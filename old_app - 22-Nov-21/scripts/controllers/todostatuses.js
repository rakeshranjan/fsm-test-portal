'use strict';

angular.module('secondarySalesApp')
    .controller('ToDoStatusesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/todo-status/create' || $location.path() === '/todo-status/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todo-status/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todo-status/create' || $location.path() === '/todo-status/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todo-status/create' || $location.path() === '/todo-status/' + $routeParams.id;
            return visible;
        };

        $scope.DisplayTodoStatus = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayTodoStatus.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
            });
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayTodoStatus.splice(indexVal, 1);
        };

        $scope.myobj = {};

        $scope.typeChange = function (index) {
            $scope.DisplayTodoStatus[index].name = '';
            $scope.DisplayTodoStatus[index].disableLang = false;
            $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
        };

        $scope.levelChange = function (index) {
            $scope.DisplayTodoStatus[index].disableLang = true;
            $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
            $scope.DisplayTodoStatus[index].disableAdd = false;
            $scope.DisplayTodoStatus[index].disableRemove = false;
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name, type) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Due Days';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.myobj.actionable == '' || $scope.myobj.actionable == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Actionable';
                document.getElementById('action').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
                $scope.DisplayTodoStatus[$scope.lastIndex].disableAdd = true;
                $scope.DisplayTodoStatus[$scope.lastIndex].disableRemove = true;
                $scope.DisplayTodoStatus[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
                $scope.DisplayTodoStatus[$scope.lastIndex].actionable = $scope.myobj.actionable;
                $scope.DisplayTodoStatus[$scope.lastIndex].languages = angular.copy($scope.languages);

//                angular.forEach($scope.languages, function (data, index) {
//                    data.inx = index + 1;
//                    $scope.DisplayTodoStatus[$scope.lastIndex]["lang" + data.inx] = data.lang;
//                    console.log($scope.DisplayTodoStatus);;
//                });

                $scope.langModel = false;
            }
        };


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/todostatuses") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('todostatuses?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (todostatus) {
            $scope.todostatuses = todostatus;
            // console.log($scope.workflows);
            angular.forEach($scope.todostatuses, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /*-------------------------------------------------------------------------------------*/

        /************* SAVE *******************************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveTodoStatus();
        };

        var saveCount = 0;

        $scope.saveTodoStatus = function () {

            if (saveCount < $scope.DisplayTodoStatus.length) {

                if ($scope.DisplayTodoStatus[saveCount].name == '' || $scope.DisplayTodoStatus[saveCount].name == null) {
                    saveCount++;
                    $scope.saveTodoStatus();
                } else {
                    Restangular.all('todostatuses').post($scope.DisplayTodoStatus[saveCount]).then(function (resp) {
//                        saveCount++;
//                        $scope.saveTodoStatus();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayTodoStatus[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayTodoStatus[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayTodoStatus[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                type: resp.type
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/todo-status-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('todostatuses').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveTodoStatus();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Todo Status Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.todostatus.lastmodifiedby = $window.sessionStorage.userId;
                $scope.todostatus.lastmodifiedtime = new Date();
                $scope.todostatus.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('todostatuses', $routeParams.id).customPUT($scope.todostatus).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('todostatuses', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/todo-status-list';
            }

        }

        $scope.gettodo = function (todotypeid) {
            return Restangular.one('todotypes', todotypeid).get().$object;
        };

        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todostatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            $scope.myobj.orderNo = $scope.todostatus.orderNo;
            $scope.myobj.actionable = $scope.todostatus.actionable;

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

             for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.todostatus.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
           angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if (data.id === lngwrkflw.language) {
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.todostatus.orderNo = $scope.myobj.orderNo;
            $scope.todostatus.actionable = $scope.myobj.actionable;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Status has been Updated!';
            Restangular.one('todostatuses', $routeParams.id).get().then(function (tdosts) {
                Restangular.all('todostatuses?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                $scope.original = tdosts;
                $scope.todostatus = Restangular.copy($scope.original);
                });
            });
        } else {
            $scope.message = 'Status has been Created!';
        }

    });
