'use strict';

angular.module('secondarySalesApp')
    .controller('TimeslotCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/

        // Restangular.all('customertimeslots?filter[where][deleteflag]=false').getList().then(function (tslots) {
        //     $scope.customertimeslots = tslots;

        //     angular.forEach($scope.customertimeslots, function (data, index) {
        //         data.index = index + 1;
        //     });
        // });

        var timeslotUrl = '';

        if ($window.sessionStorage.roleId == 2) {
            timeslotUrl = 'timeslotviews?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $window.sessionStorage.customerId;
        } else {
            timeslotUrl = 'timeslotviews?filter[where][deleteflag]=false';
        }

        Restangular.all(timeslotUrl).getList().then(function (tslots) {
            $scope.customertimeslots = tslots;

            angular.forEach($scope.customertimeslots, function (data, index) {
                data.index = index + 1;
            });
        });

        $scope.getcustomer = function (custid) {
            return Restangular.one('customers', custid).get().$object;
        };

        $scope.getcategory = function (todotypeid) {
            return Restangular.one('categories', todotypeid).get().$object;
        };

        $scope.getsubcategory = function (todostatusid) {
            return Restangular.one('subcategories', todostatusid).get().$object;
        };
    });