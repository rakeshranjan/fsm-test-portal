'use strict';

angular.module('secondarySalesApp')
  .controller('FormLevelBackendEndCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    console.log('In formlevel-backend');

    Restangular.one('rcdetailslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
      $scope.rcdetailLang = langResponse[0];
      
    });

    Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false').getList().then(function (survquest) {
      $scope.rcstatuses = survquest;
    });



  });
