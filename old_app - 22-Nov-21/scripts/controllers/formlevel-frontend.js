'use strict';

angular.module('secondarySalesApp')
  .controller('FormLevelFrontEndCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    console.log('In formlevel-frontend');

    Restangular.one('rcdetailslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
      $scope.rcdetailLang = langResponse[0];
      
    });

    Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=true').getList().then(function (survquest) {
      $scope.rcstatuses = survquest;
    });

  });
