'use strict';

angular.module('secondarySalesApp')
    .controller('learningtypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/learningtype/create' || $location.path() === '/learningtype/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/learningtype/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/learningtype/create' || $location.path() === '/learningtype/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/learningtype/create' || $location.path() === '/learningtype/' + $routeParams.id;
            return visible;
        };
        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/learningtypes") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        $scope.part = Restangular.all('learningtypes?filter[where][deleteflag]=false').getList().then(function (part) {
            $scope.learningtypes = part;
            $scope.learningtypeId = $window.sessionStorage.sales_learningtypeId;
            angular.forEach($scope.learningtypes, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        //$scope.learningtype.featured = 'no';
        Restangular.all('learningtypetypes').getList().then(function (response) {
            $scope.types = response;
        });
        $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
        $scope.getType = function (id) {
            return Restangular.one('learningtypetypes', id).get().$object;

        }
        /*-------------------------------------------------------------------------------------*/
        $scope.learningtype = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            deleteflag: false,
            featured: 'no',
            type: 1
        };
        $scope.language = {};
        $scope.statecodeDisable = false;
        $scope.membercountDisable = false;
        if ($routeParams.id) {
            console.log('i am working');
            $scope.message = 'Learning Type has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('learningtypes', $routeParams.id).get().then(function (learningtype) {
                $scope.original = learningtype;
                $scope.learningtype = Restangular.copy($scope.original);

            });
        } else {
            $scope.message = 'Learning Type has been created!';
        }

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Savelearningtype = function (clicked) {
            document.getElementById('name').style.border = "";
            if ($scope.learningtype.name == '' || $scope.learningtype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Learning Type Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
                Restangular.all('learningtypes').post($scope.learningtype).then(function (znResponse) {
                    console.log('znResponse', znResponse);
                    window.location = '/learningtype';
                });
            }
        };

        /***************************** UPDATE *******************************************/
        $scope.Updatelearningtype = function () {
            document.getElementById('name').style.border = "";
            if ($scope.learningtype.name == '' || $scope.learningtype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter learningtype Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('learningtypes', $routeParams.id).customPUT($scope.learningtype).then(function (res) {
                    //console.log('res', res.representative);
                    window.location = '/learningtype';

                });
            }
        };
        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('learningtypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



    });
