'use strict';

angular.module('secondarySalesApp')
  .controller('GroupCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
    /*********/

    $scope.showForm = function () {
      var visible = $location.path() === '/groupcreate/create' || $location.path() === '/groupcreate/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/groupcreate/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/groupcreate/create' || $location.path() === '/groupcreate/edit/' + $routeParams.id;
      return visible;
    };

    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/groupcreate/create' || $location.path() === '/groupcreate/edit/' + $routeParams.id;
      return visible;
    };

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customers?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;
    });
    
    $scope.$watch('group.customerId', function (newValue, oldValue) {
      if (newValue == '' || oldValue == undefined) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (catg) {
          $scope.categories = catg;
        });
      }
    });

    $scope.$watch('group.categoryId', function (newValue, oldValue) {
      if (newValue == '' || oldValue == undefined) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (scatg) {
          $scope.subcategories = scatg;
        });
      }
    });

    /************************************************************************************/
    $scope.group = {
      deleteFlag: false
    };

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    /****************************************** CREATE *********************************/

    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.savegroup = function () {
      document.getElementById('name').style.border = "";
      console.log($scope.customer, "message")
      if ($scope.group.groupname == '' || $scope.group.groupname == null) {
        $scope.group.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Group Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Group has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

        $scope.customerName = $scope.group.groupname;
        $scope.submitDisable = true;
        Restangular.all('groupinfo').post($scope.group).then(function () {
          window.location = '/groupcreate';
        });
        //window.location = '/customers-list';
      }
    };



    $scope.validatestring = '';
    $scope.updatecount = 0;

    $scope.Updategroup = function () {
      document.getElementById('name').style.border = "";

      if ($scope.group.groupname == '' || $scope.group.groupname == null) {
        $scope.group.groupname = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Group Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
        $scope.message = 'Group has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;

        Restangular.one('groupinfo/' + $routeParams.id).customPUT($scope.group).then(function () {
          window.location = '/groupcreate';
        });
      }
    };

    if ($routeParams.id) {
      Restangular.one('groupinfo', $routeParams.id).get().then(function (grp) {
        $scope.original = grp;
        $scope.group = Restangular.copy($scope.original);
      });
    }

  });
