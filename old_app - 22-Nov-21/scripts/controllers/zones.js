'use strict';

angular.module('secondarySalesApp')
	.controller('ZonesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/
		$scope.modalTitle = 'Thank You';

		$scope.showForm = function () {
			var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/state/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		if ($window.sessionStorage.prviousLocation != "partials/zones") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.zones = part;
			$scope.zoneId = $window.sessionStorage.sales_zoneId;
			angular.forEach($scope.zones, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.zone = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
			$scope.message = 'State has been Updated!';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('zones', $routeParams.id).get().then(function (zone) {
				$scope.original = zone;
				$scope.zone = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'State has been created!';
		}
		/************* SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.SaveZone = function (clicked) {
			document.getElementById('name').style.border = "";
			document.getElementById('code').style.border = "";
			document.getElementById('count').style.border = "";
			if ($scope.zone.name == '' || $scope.zone.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter State Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			} else if ($scope.zone.code == '' || $scope.zone.code == null) {
				document.getElementById('code').style.borderColor = "#FF0000";
				$scope.validatestring = $scope.validatestring + 'Please Enter State Code';
			} else if ($scope.zone.membercount == '' || $scope.zone.membercount == null) {
				document.getElementById('count').style.borderColor = "#FF0000";
				$scope.validatestring = $scope.validatestring + 'Please Enter State Count';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				//toaster.pop('success', "State has been created", null, null, 'trustedHtml');
				Restangular.all('zones').post($scope.zone).then(function (znResponse) {
					console.log('znResponse', znResponse);
					window.location = '/zones';
				});
			}
		};

		/***************************** UPDATE *******************************************/
		$scope.UpdateZone = function () {
			document.getElementById('name').style.border = "";
			if ($scope.zone.name == '' || $scope.zone.name == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter State Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('zones', $routeParams.id).customPUT($scope.zone).then(function () {
					//$location.path('/zones');
					window.location = '/zones';
				});
			}
		};
		/*---------------------------Delete---------------------------------------------------*/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('zones/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};



	});

/********************************* Not In Use ************************


		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		
		$scope.validatestring = '';
		/*$scope.SaveZone = function () {
			document.getElementById('name').style.border = "";
			if ($scope.zone.name == '' || $scope.zone.name == null) {
				//$scope.zone.name = null;
				$scope.validatestring = $scope.validatestring + 'Please enter state name';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.all('zones').post($scope.zone).then(function (res) {
					console.log('Zone Saved', res);
					window.location = '/zones';
				});
			}
		};*/


/****************************************************************/
