'use strict';

angular.module('secondarySalesApp')
	.controller('EditSchemeCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout) {

		if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
			window.location = "/";
		}
		console.log('$window.sessionStorage.fullName', $window.sessionStorage.UserEmployeeId);

		/* $scope.showForm = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.isCreateView = function () {
		     if ($scope.showForm()) {
		         var visible = $location.path() === '/applyforschemes/create';
		         return visible;
		     }
		 };
		 $scope.hideCreateButton = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.hideSearchFilter = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };*/
		/*$scope.schememaster = {
		    attendees: []
		};*/

		$scope.HideCreateButton = true;
		/*******************************************************************************************************************/


		$scope.states = Restangular.all('zones').getList().$object;
		$scope.genders = Restangular.all('genders').getList().$object;
		$scope.educations = Restangular.all('educations').getList().$object;
		$scope.submitschememasters = Restangular.all('schememasters').getList().$object;
		$scope.beneficiaries = Restangular.all('beneficiaries').getList().$object;
		$scope.schemestages = Restangular.all('schemestages').getList().$object;
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;

		$scope.auditlog = {
			description: 'Apply for Scheme Update',
			modifiedbyroleid: $window.sessionStorage.roleId,
			modifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date(),
			entityroleid: 44,
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId
		};

		// $scope.modalschememasters = Restangular.all('schemes?filter[where][deleteflag]=null').getList().$object;
		$scope.submittodos = Restangular.all('todos').getList().$object;

		$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null').getList().then(function (scheme) {
			$scope.printschemes = scheme;
			angular.forEach($scope.printschemes, function (member, index) {
				member.index = index;
				member.enabled = 'no';
			});
		});


		$scope.getGender = function (genderId) {
			return Restangular.one('genders', genderId).get().$object;
		};

		if ($routeParams.id) {
			Restangular.one('todos', $routeParams.id).get().then(function (todo) {
				Restangular.one('schememasters', todo.reportincidentid).get().then(function (schememaster) {
					$scope.original = schememaster;
					$scope.schememaster = Restangular.copy($scope.original);
					// $scope.schememaster.datetime = $filter('date')(schememaster.datetime, 'y-MM-dd');
					console.log('$scope.schememaster', $scope.schememaster);
					//$scope.schememaster.categ = schememaster.category.split(",");
					//$scope.schememaster.agegrp = schememaster.agegroup.split(",");
				});
			});
		}

		$scope.DeleteFlag = function (id) {
				if (confirm("Are you sure want to delete..!") == true) {
					Restangular.one('schemes/' + id).remove($scope.schememaster).then(function () {
						$route.reload();
					});

				} else {

				}

			}
			/******************************************************* Save ************************/
		$scope.Update = function () {
			$scope.todo = {
				"datetime": $scope.schememaster.datetime, //$filter('date')(new Date(), 'y-MM-dd'),
				"stage": $scope.schememaster.stage,
				"id": $routeParams.id
			};
			$scope.submitschememasters.customPUT($scope.schememaster).then(function (resp) {
				$scope.submittodos.customPUT($scope.todo).then(function () {
					console.log('$scope.schememaster', $scope.schememaster);
					console.log('$scope.todo', $scope.todo);
					window.location = '/trackapplications';
				});

			});

		};
		console.log('$window.sessionStorage.UserEmployeeId,', $window.sessionStorage.UserEmployeeId);




		//Datepicker settings start
		$scope.today = function () {
			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 7);
			//$scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
		};

		$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		$scope.dtmin = new Date();

		// Disable weekend selection
		/*  $scope.disabled = function (date, mode) {
		      return (mode === 'day' && (date.getDay() === 0));
		  };*/

		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();
		$scope.picker = {};
		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened = true;
		};


		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end

		$scope.datetimehide = false;
		$scope.reponserec = true;
		$scope.delaydis = true;
		$scope.collectedrequired = true;
		$scope.$watch('schememaster.stage', function (newValue, oldValue) {
			console.log('newValue', newValue);
			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 15);
			if (newValue === oldValue) {
				return;
			} else if (newValue === '4') {
				$scope.reponserec = false;
				$scope.delaydis = true;
				$scope.datetimehide = false;
				$scope.collectedrequired = true;
			} else if (newValue === '5') {
				$scope.delaydis = false;
				$scope.reponserec = true;
				$scope.datetimehide = false;
				$scope.collectedrequired = true;
				//$scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
			} else if (newValue === '2') {
				$scope.delaydis = true;
				$scope.reponserec = true;
				$scope.datetimehide = false;
				$scope.collectedrequired = false;
				// $scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
			} else if (newValue === '3') {
				//$scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
				$scope.datetimehide = false;
				$scope.collectedrequired = true;
			} else if (newValue === '7') {
				var sixmonth = new Date();
				sixmonth.setDate(sixmonth.getDate() + 180);
				//$scope.schememaster.datetime = $filter('date')(sixmonth, 'y-MM-dd');
				$scope.datetimehide = false;
				$scope.collectedrequired = true;
			} else if (newValue === 1) {
				//$scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
				$scope.delaydis = true;
				$scope.reponserec = true;
				$scope.collectedrequired = true;

			} else if (newValue === '6' || newValue === '8' || newValue === '10' || newValue === '9') {
				$scope.datetimehide = true;
				$scope.delaydis = true;
				$scope.reponserec = true;
				$scope.collectedrequired = true;

			} else {
				$scope.reponserec = true;
				$scope.delaydis = true;
				$scope.schememaster.responserecieve = null;
				$scope.schememaster.delay = null;
				// $scope.schememaster.datetime = '';
				$scope.datetimehide = false;
			}
		});


		$scope.rejectdis = true;
		$scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 180);
			if (newValue === oldValue) {
				return;
			} else if (newValue === 'Approved') {
				// $scope.schememaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
			} else if (newValue === 'Rejected') {
				$scope.rejectdis = false;
				//$scope.schememaster.datetime = '';
			} else {
				//  $scope.schememaster.datetime = '';
				$scope.rejectdis = true;
				//$scope.schememaster.rejection = null;
			}
		});



		/************************************************ PLHIV Modal *****************************************/
		// $scope.schememaster.attendees = [];

		$scope.$watch('schememaster.attendees', function (newValue, oldValue) {
			// console.log('watch.attendees', newValue);

			if (newValue === oldValue || newValue == undefined) {
				return;
			} else {
				if (newValue.length > oldValue.length) {
					// something was added
					var Array1 = newValue;
					var Array2 = oldValue;

					for (var i = 0; i < Array2.length; i++) {
						var arrlen = Array1.length;
						for (var j = 0; j < arrlen; j++) {
							if (Array2[i] == Array1[j]) {
								Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));

							}
						}
					}
					$scope.printschemes[Array1[0]].enabled = true;

					// do stuff
				} else if (newValue.length < oldValue.length) {
					// something was removed
					var Array1 = oldValue;
					var Array2 = newValue;

					for (var i = 0; i < Array2.length; i++) {
						var arrlen = Array1.length;
						for (var j = 0; j < arrlen; j++) {
							if (Array2[i] == Array1[j]) {
								Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
							}
						}
					}
					$scope.printschemes[Array1[0]].enabled = false;

				}
			}
		});


		$scope.showschemesModal = false;
		$scope.toggleschemesModal = function () {
			$scope.SaveScheme = function () {
				$scope.newArray = [];
				$scope.schememaster.attendees = [];
				for (var i = 0; i < $scope.printschemes.length; i++) {
					if ($scope.printschemes[i].enabled == true) {
						$scope.newArray.push($scope.printschemes[i].index);
					}
				}
				$scope.schememaster.attendees = $scope.newArray
				console.log('$scope.groupmeeting.attendees', $scope.schememaster.attendees.id);
				$scope.showschemesModal = !$scope.showschemesModal;
			};

			$scope.showschemesModal = !$scope.showschemesModal;
		};


		$scope.CancelScheme = function () {
			$scope.showschemesModal = !$scope.showschemesModal;
		};

		/************************************************ Search On Modal *************************/

		$scope.schemename = $scope.name;
		$scope.$watch('schememaster.agegrp', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
					$scope.schemes = scheme;

					angular.forEach($scope.schemes, function (member, index) {
						member.index = index;
						member.enabled = false;
					});
				});
			}
		});

		$scope.$watch('schememaster.gender', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
					$scope.schemes = scheme;

					angular.forEach($scope.schemes, function (member, index) {
						member.index = index;
						member.enabled = false;
					});
				});
			}
		});

		$scope.$watch('schememaster.stateid', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
					$scope.schemes = scheme;

					angular.forEach($scope.schemes, function (member, index) {
						member.index = index;
						member.enabled = false;
					});
				});
			}
		});



	});
