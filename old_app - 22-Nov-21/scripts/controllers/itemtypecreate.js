'use strict';

angular.module('secondarySalesApp')
    .controller('ItemtypeCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        //console.log("$routeParams.id-5", $routeParams.id)
        $scope.showForm = function () {
            var visible = $location.path() === '/itemtypecreate' || $location.path() === '/itemtype/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/itemtypecreate';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/itemtypecreate';
            return visible;
        };

        if ($location.path() === '/itemtypecreate') {
            $scope.disableDropdown = false;
        } else {
            $scope.disableDropdown = true;
        }

        /************************************************************************************/
        $scope.itemtype = {
            //organizationId: $window.sessionStorage.organizationId,
            deleteflag: false
          };

        /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/itemtypecreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
      $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
      $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
      };
  
      $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
      $scope.pageFunction = function (mypage) {
        console.log('mypage', mypage);
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
      };

      // ******************************** EOF Pagination *************************************************

      Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtype) {
        $scope.itemtypes = itmtype;
        angular.forEach($scope.itemtypes, function (member, index) {
          member.index = index + 1;
            
          Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
            $scope.customername = custmor;
            for (var j = 0; j < $scope.customername.length; j++) {
              if (member.customerId == $scope.customername[j].id) {
                member.customername = $scope.customername[j].name;
                break;
              }
            }
          });
          Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
            $scope.categoryname = categ;
            for (var j = 0; j < $scope.categoryname.length; j++) {
              if (member.categoryId == $scope.categoryname[j].id) {
                member.categoryname = $scope.categoryname[j].name;
                break;
              }
            }
          });
          Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
            $scope.subcategoryname = subcateg;
            for (var j = 0; j < $scope.subcategoryname.length; j++) {
              if (member.subcategoryId == $scope.subcategoryname[j].id) {
                member.subcategoryname = $scope.subcategoryname[j].name;
                break;
              }
            }
          });
          Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
            $scope.itemcategoryname = itmcateg;
            for (var j = 0; j < $scope.itemcategoryname.length; j++) {
              if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                member.itemcategoryname = $scope.itemcategoryname[j].name;
                break;
              }
            }
          });
           // $scope.TotalTodos = [];
           // $scope.TotalTodos.push(member);
         
        });
 
      });

    $scope.$watch('customerId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else {
        //   Restangular.all('categories').getList().then(function (cate) {
        //     $scope.categories = cate;
        //   });
  
          Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (itemcatg) {
            $scope.itemcategories = itemcatg;
  
            angular.forEach($scope.itemcategories, function (member, index) {
              member.index = index + 1;
  
              Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
                $scope.customername = custmor;
                for (var j = 0; j < $scope.customername.length; j++) {
                  if (member.customerId == $scope.customername[j].id) {
                    member.customername = $scope.customername[j].name;
                    break;
                  }
                }
              });
              Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
                $scope.categoryname = categ;
                for (var j = 0; j < $scope.categoryname.length; j++) {
                  if (member.categoryId == $scope.categoryname[j].id) {
                    member.categoryname = $scope.categoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
                $scope.subcategoryname = subcateg;
                for (var j = 0; j < $scope.subcategoryname.length; j++) {
                  if (member.subcategoryId == $scope.subcategoryname[j].id) {
                    member.subcategoryname = $scope.subcategoryname[j].name;
                    break;
                  }
                }
              });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
            });
          });
        }
      });

      $scope.$watch('categoryId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else {
          Restangular.all('subcategories?filter[where][categoryId]=' + newValue).getList().then(function (scate) {
            $scope.subcategories = scate;
          });
  
          Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId).getList().then(function (itemcatg) {
            $scope.itemcategories = itemcatg;
  
            angular.forEach($scope.itemcategories, function (member, index) {
              member.index = index + 1;
  
              Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
                $scope.customername = custmor;
                for (var j = 0; j < $scope.customername.length; j++) {
                  if (member.customerId == $scope.customername[j].id) {
                    member.customername = $scope.customername[j].name;
                    break;
                  }
                }
              });
              Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
                $scope.categoryname = categ;
                for (var j = 0; j < $scope.categoryname.length; j++) {
                  if (member.categoryId == $scope.categoryname[j].id) {
                    member.categoryname = $scope.categoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
                $scope.subcategoryname = subcateg;
                for (var j = 0; j < $scope.subcategoryname.length; j++) {
                  if (member.subcategoryId == $scope.subcategoryname[j].id) {
                    member.subcategoryname = $scope.subcategoryname[j].name;
                    break;
                  }
                }
              });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
            });
          });
        }
      });

      $scope.$watch('subCategoryId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else {
          Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + newValue).getList().then(function (itemcatg) {
            $scope.itemcategories = itemcatg;
  
            angular.forEach($scope.itemcategories, function (member, index) {
              member.index = index + 1;
  
              Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
                $scope.customername = custmor;
                for (var j = 0; j < $scope.customername.length; j++) {
                  if (member.customerId == $scope.customername[j].id) {
                    member.customername = $scope.customername[j].name;
                    break;
                  }
                }
              });
              Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
                $scope.categoryname = categ;
                for (var j = 0; j < $scope.categoryname.length; j++) {
                  if (member.categoryId == $scope.categoryname[j].id) {
                    member.categoryname = $scope.categoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
                $scope.subcategoryname = subcateg;
                for (var j = 0; j < $scope.subcategoryname.length; j++) {
                  if (member.subcategoryId == $scope.subcategoryname[j].id) {
                    member.subcategoryname = $scope.subcategoryname[j].name;
                    break;
                  }
                }
              });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
            });
          });
        }
      });

      $scope.customerId = '';
      $scope.categoryId = '';
      $scope.subcategoryId = '';


      Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });

      $scope.$watch('itemtype.customerId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          $scope.customerId = newValue;
          Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
            $scope.categories = catgresp;
          });
        }
      });
  
      $scope.$watch('itemtype.categoryId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          $scope.categoryId = newValue;
          Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
          });
        }
      });

      $scope.$watch('itemtype.subcategoryId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          $scope.subcategoryId = newValue;
          Restangular.all('itemcategories?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId).getList().then(function (itemcatg) {
            $scope.itemcategories = itemcatg;
    
          });
        }
      });

     /****************************************** CREATE *********************************/
     $scope.showValidation = false;
     $scope.toggleValidation = function () {
     $scope.showValidation = !$scope.showValidation;
     };
     $scope.validatestring = '';
     $scope.submitDisable = false;

     $scope.Save = function () {
     document.getElementById('name').style.border = "";
     //console.log($scope.itemtype, "message-282")
         if ($scope.itemtype.name == '' || $scope.itemtype.name == null) {
         $scope.itemtype.name = null;
         $scope.validatestring = $scope.validatestring + 'Please  Enter Item type';
         document.getElementById('name').style.border = "1px solid #ff0000";

         } else if ($scope.itemtype.customerId == '' || $scope.itemtype.customerId == null) {
             $scope.itemtype.customerId = null;
             $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

         } else if ($scope.itemtype.categoryId == '' || $scope.itemtype.categoryId == null) {
         $scope.itemtype.categoryId = null;
         $scope.validatestring = $scope.validatestring + 'Please  Select Category';

         } else if ($scope.itemtype.subcategoryId == '' || $scope.itemtype.subcategoryId == null) {
             $scope.itemtype.subcategoryId = null;
             $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';
 
         } else if ($scope.itemtype.itemcategoryId == '' || $scope.itemtype.itemcategoryId == null) {
            $scope.itemtype.itemcategoryId = null;
            $scope.validatestring = $scope.validatestring + 'Please  Select Itemcategory';

        }
         if ($scope.validatestring != '') {
         $scope.toggleValidation();
         $scope.validatestring1 = $scope.validatestring;
         $scope.validatestring = '';
         }
         else {
         $scope.message = 'Item type has been created!';
         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
         $scope.submitDisable = true;
         Restangular.all('itemtypes').post($scope.itemtype).then(function () {
             window.location = '/itemtype';
         });
     }
     };

     /***************************** UPDATE *******************************************/

     $scope.validatestring = '';
     $scope.updatecount = 0;

     $scope.Update = function () {
     document.getElementById('name').style.border = "";
     //console.log($scope.itemcategory, "message-323")
         if ($scope.itemtype.name == '' || $scope.itemtype.name == null) {
         $scope.itemtype.name = null;
         $scope.validatestring = $scope.validatestring + 'Please  Enter Itemtype';
         document.getElementById('name').style.border = "1px solid #ff0000";

         } else if ($scope.itemtype.customerId == '' || $scope.itemtype.customerId == null) {
             $scope.itemtype.customerId = null;
             $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

         } else if ($scope.itemtype.categoryId == '' || $scope.itemtype.categoryId == null) {
             $scope.itemtype.categoryId = null;
             $scope.validatestring = $scope.validatestring + 'Please  Select Category';

         } else if ($scope.itemtype.subcategoryId == '' || $scope.itemtype.subcategoryId == null) {
             $scope.itemtype.subcategoryId = null;
             $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';
 
         }
         if ($scope.validatestring != '') {
         $scope.toggleValidation();
         $scope.validatestring1 = $scope.validatestring;
         $scope.validatestring = '';
         }
         else {
         $scope.message = 'Itemtype has been updated!';
         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
         $scope.neWsubmitDisable = true;
         Restangular.one('itemtypes/' + $routeParams.id).customPUT($scope.itemtype).then(function () {
             window.location = '/itemtype';
             
         });
       }
     };

       if ($routeParams.id) {
         console.log("$routeParams.id", $routeParams.id)
         Restangular.one('itemtypes', $routeParams.id).get().then(function (itmtyp) {
           $scope.original = itmtyp;
           $scope.itemtype = Restangular.copy($scope.original);
         });
       }



    });