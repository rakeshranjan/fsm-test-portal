'use strict';

angular.module('secondarySalesApp')
  .controller('CategoryCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
    /*********/
    console.log("Session Storage: ", $window.sessionStorage);
    $scope.showForm = function () {
      var visible = $location.path() === '/category/create' || $location.path() === '/category/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/category/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/category/create' || $location.path() === '/category/edit/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/category/create' || $location.path() === '/category/edit/' + $routeParams.id;
      return visible;
    };

    if ($location.path() === '/category/create') {
      $scope.disableCustomer = false;
    } else {
      $scope.disableCustomer = true;
    }
    /************************************************************************************/
    $scope.category = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteFlag: false
    };
    // if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
    //     $window.sessionStorage.facility_zoneId = null;
    //     $window.sessionStorage.facility_stateId = null;
    //     $window.sessionStorage.facility_currentPage = 1;
    //     $window.sessionStorage.facility_currentPageSize = 25;
    // } else {
    //     $scope.countryId = $window.sessionStorage.facility_zoneId;
    //     $scope.stateId = $window.sessionStorage.facility_stateId;
    //     $scope.currentpage = $window.sessionStorage.facility_currentPage;
    //     $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    // }


    // if ($window.sessionStorage.prviousLocation != "partials/onetoonetype") {
    //     $window.sessionStorage.facility_zoneId = '';
    //     $window.sessionStorage.facility_stateId = '';
    //     $window.sessionStorage.facility_currentPage = 1;
    //     $scope.currentpage = 1;
    //     $window.sessionStorage.facility_currentPageSize = 25;
    //     $scope.pageSize = 25;
    // }

    // $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    // $scope.pageFunction = function (mypage) {
    //     console.log('mypage', mypage);
    //     $scope.pageSize = mypage;
    //     $window.sessionStorage.facility_currentPageSize = mypage;
    // };

    // $scope.currentpage = $window.sessionStorage.facility_currentPage;
    // $scope.PageChanged = function (newPage, oldPage) {
    //     $scope.currentpage = newPage;
    //     $window.sessionStorage.facility_currentPage = newPage;
    // };

    /************************************************/

    // $scope.DisplayOneToOneTypes = [{
    //     name: '',
    //     disableAdd: false,
    //     disableRemove: false,
    //     deleteflag: false,
    //     lastmodifiedby: $window.sessionStorage.userId,
    //     lastmodifiedrole: $window.sessionStorage.roleId,
    //     lastmodifiedtime: new Date(),
    //     createdby: $window.sessionStorage.userId,
    //     createdtime: new Date(),
    //     createdrole: $window.sessionStorage.roleId
    // }];

    // $scope.Add = function (index) {

    //     var idVal = index + 1;

    //         $scope.DisplayOneToOneTypes.push({
    //             name: '',
    //             disableAdd: false,
    //             disableRemove: false,
    //             deleteflag: false,
    //             lastmodifiedby: $window.sessionStorage.userId,
    //             lastmodifiedrole: $window.sessionStorage.roleId,
    //             lastmodifiedtime: new Date(),
    //             createdby: $window.sessionStorage.userId,
    //             createdtime: new Date(),
    //             createdrole: $window.sessionStorage.roleId
    //         });
    // };

    // $scope.Remove = function (index) {
    //     var indexVal = index - 1;
    //     $scope.DisplayOneToOneTypes.splice(indexVal, 1);
    // };

    // $scope.levelChange = function (index, name) {
    //     if (name == '' || name == null) {
    //         $scope.DisplayOneToOneTypes[index].disableAdd = false;
    //         $scope.DisplayOneToOneTypes[index].disableRemove = false;
    //     } else {
    //         $scope.DisplayOneToOneTypes[index].disableAdd = true;
    //         $scope.DisplayOneToOneTypes[index].disableRemove = true;
    //     }
    // };

    /*************************************** DELETE *******************************/

    // $scope.Delete = function (id) {
    //     $scope.item = [{
    //         deleteflag: true
    //     }]
    //     Restangular.one('onetoonetypes/' + id).customPUT($scope.item[0]).then(function () {
    //         $route.reload();
    //     });
    // }
    /********************************************** WATCH ***************************************/

    // Restangular.all('onetoonetypes?filter[where][deleteflag]=false').getList().then(function (ty) {
    //     $scope.onetoonetypes = ty;
    //     angular.forEach($scope.onetoonetypes, function (member, index) {
    //         member.index = index + 1;

    //         $scope.TotalTodos = [];
    //         $scope.TotalTodos.push(member);
    //     });
    // });

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customers?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;
    });

    /****************************************** CREATE *********************************/
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.savecategory = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.category, "message")
      if ($scope.category.name == '' || $scope.category.name == null) {
        $scope.category.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Category Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.category.customerId == '' || $scope.category.customerId == null) {
        $scope.category.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Category has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        Restangular.all('categories').post($scope.category).then(function () {
          window.location = '/category-list';
        });
      }
    };

    /***************************** UPDATE *******************************************/

    // $scope.Update = function (clicked) {

    //     if ($scope.onetoonetype.name == '' || $scope.onetoonetype.name == null) {
    //         $scope.validatestring = $scope.validatestring + 'Please Enter Question Type';
    //         document.getElementById('name').style.borderColor = "#FF0000";
    //     }
    //     if ($scope.validatestring != '') {
    //         $scope.toggleValidation();
    //         $scope.validatestring1 = $scope.validatestring;
    //         $scope.validatestring = '';
    //         //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    //     } else {

    //         $scope.onetoonetype.lastmodifiedby = $window.sessionStorage.userId;
    //         $scope.onetoonetype.lastmodifiedtime = new Date();
    //         $scope.onetoonetype.lastmodifiedrole = $window.sessionStorage.roleId;

    //         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    //         $scope.submitDisable = true;

    //         Restangular.one('onetoonetypes', $routeParams.id).customPUT($scope.onetoonetype).then(function (resp) {
    //             window.location = '/onetoone-type-list';
    //         });
    //     }
    // };

    // Restangular.one('customers', $routeParams.id).customPUT($scope.customer).then(function (resp) {
    //   window.location = '/customers-list';
    // });
    //}
    // };

    // $scope.showValidation = false;
    // $scope.toggleValidation = function () {
    //     $scope.showValidation = !$scope.showValidation;
    // };

    // if ($routeParams.id) {
    //     $scope.message = 'Type has been Updated!';
    //     $scope.statecodeDisable = true;
    //     $scope.membercountDisable = true;
    //     Restangular.one('onetoonetypes', $routeParams.id).get().then(function (onetoonetype) {
    //         $scope.original = onetoonetype;
    //         $scope.onetoonetype = Restangular.copy($scope.original);
    //     });
    // } else {
    //     $scope.message = 'Type has been created!';
    // }

    // $scope.showValidation = false;
    // $scope.toggleValidation = function () {
    //   $scope.showValidation = !$scope.showValidation;
    // };

    // if ($routeParams.id) {
    //   $scope.message = 'Customer has been Updated!';
    //   $scope.statecodeDisable = true;
    //   $scope.membercountDisable = true;
    //   Restangular.one('customers', $routeParams.id).get().then(function (custmers) {
    //     $scope.original = custmers;
    //     $scope.onetoonetype = Restangular.copy($scope.original);
    //   });
    // } else {
    //   $scope.message = 'Customer has been created!';
    // }

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Updatecategory = function () {
      document.getElementById('name').style.border = "";

      if ($scope.category.name == '' || $scope.category.name == null) {
        $scope.category.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Category Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.category.customerId == '' || $scope.category.customerId == null) {
        $scope.category.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Category has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;
        Restangular.one('categories/' + $routeParams.id).customPUT($scope.category).then(function () {
          window.location = '/category-list';
          // $location.path('/customers');
          //$route.reload();
        });
        //   $scope.customers.customPUT($scope.customer).then(function () {
        //     toaster.pop('success', 'Customer updated successfully');


        //     $location.path('/customers');
        //     // $route.reload();
        //   }, function (error) {
        //     $scope.neWsubmitDisable = false;
        //     toaster.pop('error', 'Customer already exist');
        //     $scope.customer.name = '';
        //   });
      }
    };

    if ($routeParams.id) {
      Restangular.one('categories', $routeParams.id).get().then(function (catg) {
        $scope.original = catg;
        $scope.category = Restangular.copy($scope.original);
      });
    }

  });
