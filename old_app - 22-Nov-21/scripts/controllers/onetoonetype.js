'use strict';

angular.module('secondarySalesApp')
    .controller('OneToOneTypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/onetoone-type/create' || $location.path() === '/onetoone-type/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/onetoone-type/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/onetoone-type/create' || $location.path() === '/onetoone-type/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/onetoone-type/create' || $location.path() === '/onetoone-type/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/onetoonetype") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        $scope.DisplayOneToOneTypes = [{
            name: '',
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        }];

        $scope.Add = function (index) {

            var idVal = index + 1;

                $scope.DisplayOneToOneTypes.push({
                    name: '',
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId
                });
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayOneToOneTypes.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayOneToOneTypes[index].disableAdd = false;
                $scope.DisplayOneToOneTypes[index].disableRemove = false;
            } else {
                $scope.DisplayOneToOneTypes[index].disableAdd = true;
                $scope.DisplayOneToOneTypes[index].disableRemove = true;
            }
        };

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('onetoonetypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        /********************************************** WATCH ***************************************/

        Restangular.all('onetoonetypes?filter[where][deleteflag]=false').getList().then(function (ty) {
            $scope.onetoonetypes = ty;
            angular.forEach($scope.onetoonetypes, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveType();
        };

        var saveCount = 0;

        $scope.saveType = function () {

            if (saveCount < $scope.DisplayOneToOneTypes.length) {

                if ($scope.DisplayOneToOneTypes[saveCount].name == '' || $scope.DisplayOneToOneTypes[saveCount].name == null) {
                    saveCount++;
                    $scope.saveType();
                } else {
                    Restangular.all('onetoonetypes').post($scope.DisplayOneToOneTypes[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveType();
                    });
                }

            } else {
                window.location = '/onetoone-type-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.onetoonetype.name == '' || $scope.onetoonetype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question Type';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                
                $scope.onetoonetype.lastmodifiedby = $window.sessionStorage.userId;
                $scope.onetoonetype.lastmodifiedtime = new Date();
                $scope.onetoonetype.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('onetoonetypes', $routeParams.id).customPUT($scope.onetoonetype).then(function (resp) {
                    window.location = '/onetoone-type-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Type has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('onetoonetypes', $routeParams.id).get().then(function (onetoonetype) {
                $scope.original = onetoonetype;
                $scope.onetoonetype = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Type has been created!';
        }



    });
