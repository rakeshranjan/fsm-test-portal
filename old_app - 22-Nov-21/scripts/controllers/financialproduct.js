'use strict';

angular.module('secondarySalesApp')
    .controller('FinancialProductCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/financialproduct/create' || $location.path() === '/financialproduct/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/financialproduct/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/financialproduct/create' || $location.path() === '/financialproduct/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/financialproduct/create' || $location.path() === '/financialproduct/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        if ($window.sessionStorage.prviousLocation != "partials/financialproduct") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };



        /*********/

        //$scope.submitstakeholdertypes = Restangular.all('financialproducts').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Financial Product has been Updated!';
            Restangular.one('financialproducts', $routeParams.id).get().then(function (stakeholdertype) {
                $scope.original = stakeholdertype;
                $scope.financialproduct = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Financial Product has been Created!';
        }
        $scope.searchstakeholdertype = $scope.name;

        /***************************** INDEX *******************************************/
        $scope.zn = Restangular.all('financialproducts?filter[where][deleteflag]=false').getList().then(function (zn) {
            //
            $scope.financialproducts = zn;
            // console.log('$scope.financialproducts', $scope.financialproducts);
            angular.forEach($scope.financialproducts, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.financialproduct = {
            name: '',
            deleteflag: false
        };
        /************************************ SAVE *******************************************/
        $scope.validatestring = '';
        $scope.Savestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.financialproduct.name == '' || $scope.financialproduct.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.hnname == '' || $scope.financialproduct.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.knname == '' || $scope.financialproduct.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.taname == '' || $scope.financialproduct.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.tename == '' || $scope.financialproduct.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.mrname == '' || $scope.financialproduct.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.financialproducts.post($scope.financialproduct).then(function () {
                    console.log('financial product Saved');
                    window.location = '/financialproduct';
                });
            }
        };
        /********************************* UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updatestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.financialproduct.name == '' || $scope.financialproduct.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.hnname == '' || $scope.financialproduct.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.knname == '' || $scope.financialproduct.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.taname == '' || $scope.financialproduct.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.tename == '' || $scope.financialproduct.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.financialproduct.mrname == '' || $scope.financialproduct.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial product name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.financialproducts.customPUT($scope.financialproduct).then(function () {
                    console.log('financial product Saved');
                    window.location = '/financialproduct';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        /********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('financialproducts/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
