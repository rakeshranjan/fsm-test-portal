'use strict';

angular.module('secondarySalesApp')

    .controller('MemberLanguageCtrl', function ($scope, Restangular, $route, $window, $filter, $modal) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/memberlanguage-form" || $window.sessionStorage.prviousLocation != "partials/memberlanguage") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /************************************* INDEX ***************************************************/

        Restangular.all('memberlanguages?filter[where][deleteflag]=false').getList().then(function (member) {
            $scope.memberlanguages = member;

            angular.forEach($scope.memberlanguages, function (data, index) {
                data.index = index + 1;
            });
        });

        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('memberlanguages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getLanguage = function (language) {
            return Restangular.one('languagedefinitions/findOne?filter[where][deleteflag]=false&filter[where][id]='+language).get().$object;
        };


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
