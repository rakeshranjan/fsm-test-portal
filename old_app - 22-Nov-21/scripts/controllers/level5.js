'use strict';

angular.module('secondarySalesApp')
    .controller('Level5Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/level5/create' || $location.path() === '/level5/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level5/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level5/create' || $location.path() === '/level5/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level5/create' || $location.path() === '/level5/' + $routeParams.id;
            return visible;
        };

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            if ($routeParams.id) {
                $scope.message = leveldefinition[4].name + ' has been Updated!';
            } else {
                $scope.message = leveldefinition[4].name + ' has been Created!';
            }

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/level5") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (l1) {
            $scope.levelones = l1;
        });

        $scope.$watch('level1', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwos = l2;
                });
            }
        });

        $scope.$watch('level2', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (l3) {
                    $scope.levelthrees = l3;
                });
            }
        });

        $scope.$watch('level3', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][levelthreeid]=' + newValue).getList().then(function (l4) {
                    $scope.levelfours = l4;
                });
            }
        });

        Restangular.all('levelfives?filter[where][deleteflag]=false').getList().then(function (lvl5) {
            $scope.levelfives = lvl5;

            angular.forEach($scope.levelfives, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.level1Id = '';
        $scope.level2Id = '';
        $scope.level3Id = '';
        $scope.level4Id = '';

        $scope.$watch('level1Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwolist = l2;
                });
                
                Restangular.all('levelfives?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (lvl5) {
                    $scope.levelfives = lvl5;

                    angular.forEach($scope.levelfives, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });


        $scope.$watch('level2Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (l3) {
                    $scope.levelthreelist = l3;
                });
                
                Restangular.all('levelfives?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (lvl5) {
                    $scope.levelfives = lvl5;

                    angular.forEach($scope.levelfives, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        $scope.$watch('level3Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][levelthreeid]=' + newValue).getList().then(function (l4) {
                    $scope.levelfourlist = l4;
                });
                
                Restangular.all('levelfives?filter[where][deleteflag]=false' + '&filter[where][levelthreeid]=' + newValue).getList().then(function (lvl5) {
                    $scope.levelfives = lvl5;

                    angular.forEach($scope.levelfives, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        $scope.$watch('level4Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('levelfives?filter[where][deleteflag]=false' + '&filter[where][levelfourid]=' + newValue).getList().then(function (lvl5) {
                    $scope.levelfives = lvl5;

                    angular.forEach($scope.levelfives, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        /************************ end ********************************************************/

        $scope.DisplayLevelfives = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png'
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayLevelfives.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png'
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLevelfives.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLevelfives[index].disableLang = false;
                $scope.DisplayLevelfives[index].disableAdd = false;
                $scope.DisplayLevelfives[index].disableRemove = false;
                $scope.DisplayLevelfives[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayLevelfives[index].disableLang = true;
                $scope.DisplayLevelfives[index].disableAdd = false;
                $scope.DisplayLevelfives[index].disableRemove = false;
                $scope.DisplayLevelfives[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayLevelfives[$scope.lastIndex].disableAdd = true;
            $scope.DisplayLevelfives[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayLevelfives[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayLevelfives);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('levelfives/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel5();
        };

        var saveCount = 0;

        $scope.saveLevel5 = function () {

            if (saveCount < $scope.DisplayLevelfives.length) {

                if ($scope.DisplayLevelfives[saveCount].name == '' || $scope.DisplayLevelfives[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel5();
                } else {

                    $scope.DisplayLevelfives[saveCount].leveloneid = $scope.level1;
                    $scope.DisplayLevelfives[saveCount].leveltwoid = $scope.level2;
                    $scope.DisplayLevelfives[saveCount].levelthreeid = $scope.level3;
                    $scope.DisplayLevelfives[saveCount].levelfourid = $scope.level4;

                    Restangular.all('levelfives').post($scope.DisplayLevelfives[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel5();
                    });
                }

            } else {
                window.location = '/level5-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.levelfive.name == '' || $scope.levelfive.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter' + $scope.leveldefinitions[4].name + 'Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.levelfive.lastmodifiedby = $window.sessionStorage.userId;
                $scope.levelfive.lastmodifiedtime = new Date();
                $scope.levelfive.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('levelfives', $routeParams.id).customPUT($scope.levelfive).then(function (resp) {
                    window.location = '/level5-list';
                });
            }
        };

        $scope.getLevel1 = function (leveloneid) {
            return Restangular.one('levelones', leveloneid).get().$object;
        };

        $scope.getLevel2 = function (leveltwoid) {
            return Restangular.one('leveltwos', leveltwoid).get().$object;
        };

        $scope.getLevel3 = function (levelthreeid) {
            return Restangular.one('levelthrees', levelthreeid).get().$object;
        };

        $scope.getLevel4 = function (levelfourid) {
            return Restangular.one('levelfours', levelfourid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.levelfive) {
                if ($scope.levelfive.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                        // console.log(x);

                        if ($scope.levelfive[key] != null) {
                            $scope.languages[mCount].lang = $scope.levelfive[key];
                            //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.levelfive[key] == null) {
                            // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.levelfive.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.levelfive["lang" + data.inx] = data.lang;
                // console.log($scope.leveldefinition);
            });

            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('levelfives', $routeParams.id).get().then(function (levelfive) {
                $scope.original = levelfive;
                $scope.levelfive = Restangular.copy($scope.original);
            });

            Restangular.all('leveltwos?filter[where][deleteflag]=false').getList().then(function (l2) {
                $scope.leveltwos = l2;
            });

            Restangular.all('levelthrees?filter[where][deleteflag]=false').getList().then(function (l3) {
                $scope.levelthrees = l3;
            });

            Restangular.all('levelfours?filter[where][deleteflag]=false').getList().then(function (l4) {
                $scope.levelfours = l4;
            });
        }

    });
