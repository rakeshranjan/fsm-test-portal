'use strict';

angular.module('secondarySalesApp')
    .controller('AssignitemCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (usr) {
            var splitArray = $window.sessionStorage.orgStructure.split(';');
            var splitArray1 = splitArray[splitArray.length - 1];
            var splitArray2 = splitArray1.split('-');
            var splitArray2 = splitArray2[splitArray2.length - 1];
            $scope.userDisplay = [];
            Restangular.all('users?filter[where][isPartner]=true' + '&filter[where][customerId]=' + usr.customerId + '&filter[where][subcategoryId]=' + usr.subcategoryId + '&filter[where][roleId]=' + 33).getList().then(function (userresp) {
                angular.forEach(userresp, function (resp, index) {
                    var arr = resp.orgStructure.split(';');
                    var arr1 = arr[arr.length - 1].split('-');
                    var arr2 = arr1[arr1.length - 1].split(',');
                    var filterObj = arr2.filter(function (e) {
                        return e == splitArray2;
                    });
                    if (filterObj.length > 0) {
                        $scope.userDisplay.push(resp);
                    }
                });
            });
        });

        $scope.EmployeeId = '';

        $scope.$watch('EmployeeId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('assigneditems?filter[where][partnerId]=' + newValue).getList().then(function (assign) {
                    //  Restangular.all('assigneditems?filter[where][distributionAreaId]=' + $scope.userDistAreaId + '&filter[where][warehouseid]=' + newValue + '&filter[where][warehousetype]=e').getList().then(function (assign) {
                    $scope.assigneditems = assign;
                    angular.forEach($scope.assigneditems, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.getwarehouse = function (warehouseid) {
            return Restangular.one('warehouses', warehouseid).get().$object;
        };

        $scope.getuser = function (partnerId) {
            return Restangular.one('users', partnerId).get().$object;
        };

        $scope.getitemstatus = function (itemstatusId) {
            return Restangular.one('itemstatuses', itemstatusId).get().$object;
        };

        $scope.getDistributionSubarea = function (distributionSubareaId) {
            return Restangular.one('distribution-subareas', distributionSubareaId).get().$object;
        };

        $scope.getitemdefinition = function (itemstatusId) {
            return Restangular.one('itemdefinitions', itemstatusId).get().$object;
        };

        $scope.getcustomer = function (custId) {
            return Restangular.one('customers', custId).get().$object;
        };

        $scope.getcategory = function (categoryid) {
            return Restangular.one('categories', categoryid).get().$object;
        };

        $scope.getsubcategory = function (subcategoryid) {
            return Restangular.one('subcategories', subcategoryid).get().$object;
        };
    });