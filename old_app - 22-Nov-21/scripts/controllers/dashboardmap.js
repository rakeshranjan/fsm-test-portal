'use strict';
angular.module('secondarySalesApp').controller('DashboardMapCtrl', function ($scope, $http, Restangular, $filter, $timeout, $window, $route, $modal, $fileUploader) {

  console.log("Dashboard Map");

  if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
    $scope.showAssign = false;
  } else if ($window.sessionStorage.roleId === '5') {
    $scope.showAssign = false;
  } else {
    $scope.showAssign = true;
  }
  /////////////////////////map//////////////////////////////////////
  Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    $scope.cities = city;
  });

  Restangular.all('ticket_status_role_view?[deleteflag]=false').getList().then(function (members) {
    $scope.members = members;
    console.log("members", members);

    var latitude = [];
    var longitude = [];
    var status = [];
    var approvalp = [];
    var fullname = [];
    var startdate = [];
    var enddate = [];
    var customername = [];
    var category = [];
    var subcategory = [];
    for (var j = 0; j < members.length; j++) {
      latitude[j] = members[j].latitude;
      longitude[j] = members[j].longitude;
      status[j] = members[j].todostatusId;
      approvalp[j] = members[j].approvalFlag;
      fullname[j] = members[j].fullname;
      startdate[j] = members[j].starttime;
      enddate[j] = members[j].endtime;
      category[j] = members[j].categoryname;
      subcategory[j] = members[j].subcategoryname;
      customername[j] = members[j].customername;
    }

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(21.7679, 78.8718),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker;
    var statusname = [];
    var image = [];
    var background = [];
    console.log("testing", members[1].todostatusId)
    for (var l = 0; l < members.length; l++) {
      if (status[l] == 1) {
        image[l] = {
          url: "images/piclinks/open.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "OPEN";

      } else if (status[l] == 5) {
        image[l] = {
          url: "images/piclinks/complete.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "COMPLETED";

      } else if (status[l] == 3) {
        image[l] = {
          url: "images/piclinks/assign.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "ASSIGN";

      } else if (status[l] == 4) {
        image[l] = {
          url: "images/piclinks/inprogress.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "IN PROGRESS";

      } else if (status[l] == 6) {
        image[l] = {
          url: "images/piclinks/approval.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "APPROVAL PENDING";

      } else if (status[l] == 2) {
        image[l] = {
          url: "images/piclinks/backbench.png",
          scaledSize: new google.maps.Size(50, 50), // scaled size

        };
        statusname[l] = "BACK OFFICE";

      }
    }

    var infowindow = new google.maps.InfoWindow();


    for (var i = 0; i < members.length; i++) {


      marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude[i], longitude[i]),
        map: map,
        stylers: background[i],
        icon: image[i]
      });
      google.maps.event.addListener(marker, 'click', (function (marker, i) {

        return function () {
          // infowindow.setContent(infoWindowContent1[i][0]);
          if (status[i] == 1) {
            infowindow.setContent(
              '<div style="background-color: #2f68d4;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          } else if (status[i] == 5) {
            infowindow.setContent(
              '<div style="background-color: #ff0ada;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          } else if (status[i] == 3) {
            infowindow.setContent(
              '<div style="background-color: #9d0cb0;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          } else if (status[i] == 4) {
            infowindow.setContent(
              '<div style="background-color: #ff7919;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          } else if (status[i] == 6) {
            infowindow.setContent(
              '<div style="background-color: #008080;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          } else if (status[i] == 2) {
            infowindow.setContent(
              '<div style="background-color: #800000;padding: 10px;">' +
              '<div class="info_content">' + fullname[i] + '</div>' +
              '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
              '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' + '</div>');
          }
          infowindow.open(map, marker);

        }
      })(marker, i));
    }

  });



});
