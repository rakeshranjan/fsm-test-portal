'use strict';

angular.module('secondarySalesApp')
    .controller('TransferFormCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        Restangular.all("beneficiaries?filter[where][id]=" + $routeParams.id).getList().then(function (members) {
            if (members.length > 0) {
                $scope.disableInitiateTransfer = false;
                $("#demo").collapse('show');
                $scope.beneficiary = members[0];

                Restangular.one('fieldworkers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
                    $scope.beneficiary.fieldworkername = fwget.firstname;
                });

                Restangular.one('employees', $scope.beneficiary.facility).get().then(function (employee) {
                    $scope.beneficiary.facilityName = employee.firstName;
                });

                Restangular.one('distribution-routes', $scope.beneficiary.site).get().then(function (site) {
                    $scope.beneficiary.siteName = site.name;
                });

                Restangular.one('genders', $scope.beneficiary.gender).get().then(function (gender) {
                    $scope.beneficiary.genderName = gender.name;
                });

            } else {
                $scope.validatestring = "No Record found with this AvahanId";
                $scope.showValidation = !$scope.showValidation;
                $scope.disableInitiateTransfer = true;
            }
        });

    });