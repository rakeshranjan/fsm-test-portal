'use strict';

angular.module('secondarySalesApp')
    .controller('WorkFlowCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/workflow/create' || $location.path() === '/workflow/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflow/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflow/create' || $location.path() === '/workflow/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflow/create' || $location.path() === '/workflow/' + $routeParams.id;
            return visible;
        };

        $scope.DisplayWorkflows = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayWorkflows.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
                language: 1,
                parentFlag: true
            });
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayWorkflows.splice(indexVal, 1);
        };

        $scope.myobj = {};

        $scope.levelChange = function (index) {
            $scope.DisplayWorkflows[index].disableLang = true;
            $scope.DisplayWorkflows[index].langdark = 'images/Ldark.png';
            $scope.DisplayWorkflows[index].disableAdd = false;
            $scope.DisplayWorkflows[index].disableRemove = false;
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
                $scope.DisplayWorkflows[$scope.lastIndex].disableAdd = true;
                $scope.DisplayWorkflows[$scope.lastIndex].disableRemove = true;
                $scope.DisplayWorkflows[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
                $scope.DisplayWorkflows[$scope.lastIndex].languages = angular.copy($scope.languages);

                //                angular.forEach($scope.languages, function (data, index) {
                //                    data.inx = index + 1;
                //                    $scope.DisplayWorkflows[$scope.lastIndex]["lang" + data.inx] = data.lang;
                //                    console.log($scope.DisplayWorkflows);;
                //                });
                console.log("$scope.DisplayWorkflows", $scope.DisplayWorkflows);
                $scope.langModel = false;
            }
        };


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflow") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        Restangular.all('workflows?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wflow) {
            $scope.workflows = wflow;
            // console.log($scope.workflows);
            angular.forEach($scope.workflows, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /*-------------------------------------------------------------------------------------*/

        /************* SAVE *******************************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveWorkflow();
        };

        var saveCount = 0;

        $scope.saveLangCount = 0;
        $scope.languageWorkflows = [];

        $scope.saveWorkflow = function () {

            if (saveCount < $scope.DisplayWorkflows.length) {

                if ($scope.DisplayWorkflows[saveCount].name == '' || $scope.DisplayWorkflows[saveCount].name == null) {
                    saveCount++;
                    $scope.saveWorkflow();
                } else {
                    Restangular.all('workflows').post($scope.DisplayWorkflows[saveCount]).then(function (resp) {
                        // $scope.saveWorkflow();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayWorkflows[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayWorkflows[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayWorkflows[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/workflows-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('workflows').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveWorkflow();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.workflow.name == '' || $scope.workflow.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Workflow Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.workflow.lastmodifiedby = $window.sessionStorage.userId;
                $scope.workflow.lastmodifiedtime = new Date();
                $scope.workflow.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflows', $routeParams.id).customPUT($scope.workflow).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };
    $scope.updateLangCount = 0;
    
    $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('workflows',$scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/workflows-list';
            }

        }


        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflows/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};
            $scope.myobj.orderNo = $scope.workflow.orderNo;

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';
            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.workflow.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if(data.id === lngwrkflw.language){
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.workflow.orderNo = $scope.myobj.orderNo;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Workflow has been Updated!';
            Restangular.one('workflows', $routeParams.id).get().then(function (workflow) {
                Restangular.all('workflows?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                    $scope.original = workflow;
                    $scope.workflow = Restangular.copy($scope.original);
                });
            });
        } else {
            $scope.message = 'Workflow has been Created!';
        }

    });