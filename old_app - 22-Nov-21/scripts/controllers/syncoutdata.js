'use strict';

angular.module('secondarySalesApp')
    .controller('SyncOutDataCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter) {

        $scope.firstClickCount = 0;
        $scope.secondClickCount = 0;
        $scope.thirdClickCount = 0;

        Restangular.all('fieldworkers?filter[where][id]=' + $routeParams.id).getList().then(function (fldwrkr) {
            $scope.selectedFW = fldwrkr[0];
            console.log('$scope.selectedFW',$scope.selectedFW);
        });

        Restangular.all('userlogindetails?filter[where][platform]=mobile&filter[where][operationflag]=syncout&filter[where][employeeid]=' + $routeParams.id).getList().then(function (logindetails) {
            $scope.userlogindetails = logindetails;
            if ($scope.userlogindetails.length >= 4) {
                $scope.thirdSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 1];
                $scope.secondSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 2];
                $scope.firstSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 3];
            } else if ($scope.userlogindetails.length == 3) {
                $scope.thirdSyncout = $scope.userlogindetails[2];
                $scope.secondSyncout = $scope.userlogindetails[1];
                $scope.firstSyncout = $scope.userlogindetails[0];
            } else if ($scope.userlogindetails.length == 2) {
                $scope.thirdSyncout = $scope.userlogindetails[1];
                $scope.secondSyncout = $scope.userlogindetails[0];
            } else if ($scope.userlogindetails.length == 1) {
                $scope.thirdSyncout = $scope.userlogindetails[0];
            }


            $scope.partners1 = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (resPartner1) {
                $scope.partners = resPartner1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
                $scope.onCLickThirdSyncOut($scope.thirdSyncout.datetime);
                
            });
        });

        $scope.getassignedSites = function (siteids) {
            if (siteids != null || siteids != '' || siteids != 'undefined') {
                return Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
            }
        };


        $scope.thirdSyncMembers = "";
        $scope.thirdSyncAnswers = "";
        $scope.thirdSyncSchemes = "";
        $scope.thirdSyncIncidents = "";
        $scope.thirdArraySyncMembers = [];
        $scope.thirdArraySyncAnswers = [];
        $scope.thirdArraySyncSchemes = [];
        $scope.thirdArraySyncIncidents = [];
        $scope.onCLickThirdSyncOut = function (syncouttime) {
            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {

                if ($scope.thirdClickCount == 0) {
                    $scope.thirdClickCount++;
                    Restangular.all('beneficiaries?filter[where][syncouttime]=' + syncouttime).getList().then(function (bens) {
                        $scope.beneficiaries = bens;
                       // console.log("bens",bens);
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.thirdArraySyncMembers.indexOf(member.fullname) == -1) {
                                $scope.thirdArraySyncMembers.push(member.fullname);
                                if ($scope.thirdSyncMembers != "") {
                                    $scope.thirdSyncMembers = $scope.thirdSyncMembers + "," + member.fullname;
                                } else {
                                    $scope.thirdSyncMembers = $scope.thirdSyncMembers + member.fullname;
                                }
                            }
                        });
                    });

                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                        //console.log("reports",reports);
                        $scope.reportincidents = reports;
                        angular.forEach($scope.reportincidents, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.thirdArraySyncIncidents.indexOf(member.Membername) == -1) {
                                $scope.thirdArraySyncIncidents.push(member.Membername);
                                if ($scope.thirdSyncIncidents != "") {
                                    $scope.thirdSyncIncidents = $scope.thirdSyncIncidents + "," + member.Membername;
                                } else {
                                    $scope.thirdSyncIncidents = $scope.thirdSyncIncidents + member.Membername;
                                }
                            }
                        });
                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                       // console.log("schmasters",schmasters);
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.thirdArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.thirdArraySyncSchemes.push(member.Membername);
                                if ($scope.thirdSyncSchemes != "") {
                                    $scope.thirdSyncSchemes = $scope.thirdSyncSchemes + "," + member.Membername;
                                } else {
                                    $scope.thirdSyncSchemes = $scope.thirdSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    Restangular.all('surveyanswers?filter[where][syncouttime]=' + syncouttime).getList().then(function (suranswers) {
                        $scope.surveyanswers = suranswers;
                        //console.log("suranswers",suranswers);
                        angular.forEach($scope.surveyanswers, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.thirdArraySyncAnswers.lastIndexOf(member.Membername) == -1) {
                                $scope.thirdArraySyncAnswers.push(member.Membername);
                                if ($scope.thirdSyncAnswers != "") {
                                    $scope.thirdSyncAnswers = $scope.thirdSyncAnswers + "," + member.Membername;
                                } else {
                                    $scope.thirdSyncAnswers = $scope.thirdSyncAnswers + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.thirdstakeholders = Restangular.all('stakeholders?filter[where][syncouttime]=' + syncouttime).getList().$object;
                    $scope.thirdgroupmeetings = Restangular.all('groupmeetings?filter[where][syncouttime]=' + syncouttime).getList().$object;
                    //$scope.thirdClickCount=0;
                }
            }
        };



        $scope.secondSyncMembers = "";
        $scope.secondSyncAnswers = "";
        $scope.secondSyncSchemes = "";
        $scope.secondSyncIncidents = "";
        $scope.secondArraySyncMembers = [];
        $scope.secondArraySyncAnswers = [];
        $scope.secondArraySyncSchemes = [];
        $scope.secondArraySyncIncidents = [];
        $scope.onCLickSecondSyncOut = function (syncouttime) {
            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {
                if ($scope.secondClickCount == 0) {
                    $scope.secondClickCount++;
                    Restangular.all('beneficiaries?filter[where][syncouttime]=' + syncouttime).getList().then(function (bens) {
                        $scope.beneficiaries = bens;
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.secondArraySyncMembers.indexOf(member.fullname) == -1) {
                                $scope.secondArraySyncMembers.push(member.fullname);
                                if($scope.secondSyncMembers!=""){
                                $scope.secondSyncMembers = $scope.secondSyncMembers + "," + member.fullname;
                                }else{
                                    $scope.secondSyncMembers = $scope.secondSyncMembers + member.fullname;
                                }
                            }
                        });
                    });

                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                        $scope.reportincidents = reports;
                        angular.forEach($scope.reportincidents, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.secondArraySyncIncidents.indexOf(member.Membername) == -1) {
                                $scope.secondArraySyncIncidents.push(member.Membername);
                                if($scope.secondSyncIncidents!=""){
                                $scope.secondSyncIncidents = $scope.secondSyncIncidents + "," + member.Membername;
                                }else{
                                    $scope.secondSyncIncidents = $scope.secondSyncIncidents + member.Membername;
                                }
                            }
                        });
                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.secondArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.secondArraySyncSchemes.push(member.Membername);
                                if($scope.secondSyncSchemes!=""){
                                $scope.secondSyncSchemes = $scope.secondSyncSchemes + "," + member.Membername;
                                }else{
                                    $scope.secondSyncSchemes = $scope.secondSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    Restangular.all('surveyanswers?filter[where][syncouttime]=' + syncouttime).getList().then(function (suranswers) {
                        $scope.surveyanswers = suranswers;
                        angular.forEach($scope.surveyanswers, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.secondArraySyncAnswers.lastIndexOf(member.Membername) == -1) {
                                $scope.secondArraySyncAnswers.push(member.Membername);
                                if($scope.secondSyncAnswers!=""){
                                $scope.secondSyncAnswers = $scope.secondSyncAnswers + "," + member.Membername;
                                }else{
                                    $scope.secondSyncAnswers = $scope.secondSyncAnswers + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.secondstakeholders = Restangular.all('stakeholders?filter[where][syncouttime]=' + syncouttime).getList().$object;
                    $scope.secondgroupmeetings = Restangular.all('groupmeetings?filter[where][syncouttime]=' + syncouttime).getList().$object;
                }
            }
        };


        $scope.firstSyncMembers = "";
        $scope.firstSyncAnswers = "";
        $scope.firstSyncSchemes = "";
        $scope.firstSyncIncidents = "";
        $scope.firstArraySyncMembers = [];
        $scope.firstArraySyncAnswers = [];
        $scope.firstArraySyncSchemes = [];
        $scope.firstArraySyncIncidents = [];
        $scope.onCLickFirstSyncOut = function (syncouttime) {
            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {
                if ($scope.firstClickCount == 0) {
                    $scope.firstClickCount++;
                    Restangular.all('beneficiaries?filter[where][syncouttime]=' + syncouttime).getList().then(function (bens) {
                        $scope.beneficiaries = bens;
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.firstArraySyncMembers.indexOf(member.fullname) == -1) {
                                $scope.firstArraySyncMembers.push(member.fullname);
                                if($scope.firstSyncMembers!=""){
                                $scope.firstSyncMembers = $scope.firstSyncMembers + "," + member.fullname;
                                }else{
                                    $scope.firstSyncMembers = $scope.firstSyncMembers + member.fullname;
                                }
                            }
                        });
                    });

                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                        $scope.reportincidents = reports;
                        angular.forEach($scope.reportincidents, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.firstArraySyncIncidents.indexOf(member.Membername) == -1) {
                                $scope.firstArraySyncIncidents.push(member.Membername);
                                if($scope.firstSyncIncidents!=""){
                                $scope.firstSyncIncidents = $scope.firstSyncIncidents + "," + member.Membername;
                                }else{
                                    $scope.firstSyncIncidents = $scope.firstSyncIncidents + member.Membername;
                                }
                            }
                        });
                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.firstArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.firstArraySyncSchemes.push(member.Membername);
                                if($scope.firstSyncSchemes!=""){
                                $scope.firstSyncSchemes = $scope.firstSyncSchemes + "," + member.Membername;
                                }else{
                                    $scope.firstSyncSchemes = $scope.firstSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    Restangular.all('surveyanswers?filter[where][syncouttime]=' + syncouttime).getList().then(function (suranswers) {
                        $scope.surveyanswers = suranswers;
                        angular.forEach($scope.surveyanswers, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].fullname;
                                    break;
                                }
                            }
                            if ($scope.firstArraySyncAnswers.lastIndexOf(member.Membername) == -1) {
                                $scope.firstArraySyncAnswers.push(member.Membername);
                                if($scope.firstSyncAnswers!=''){
                                $scope.firstSyncAnswers = $scope.firstSyncAnswers + "," + member.Membername;
                                }else{
                                    $scope.firstSyncAnswers = $scope.firstSyncAnswers + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.firststakeholders = Restangular.all('stakeholders?filter[where][syncouttime]=' + syncouttime).getList().$object;
                    $scope.firstgroupmeetings = Restangular.all('groupmeetings?filter[where][syncouttime]=' + syncouttime).getList().$object;
                }
            }
        };

    });