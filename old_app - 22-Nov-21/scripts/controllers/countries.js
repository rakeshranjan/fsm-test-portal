'use strict';

angular.module('secondarySalesApp')
    .controller('CountriesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/countries/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/countries") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /**********************************/


        /****************************************************************************************INDEX****************************/

        $scope.searchCon = $scope.name;

        $scope.con = Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
            $scope.countries = con;
            console.log($scope.countries);
            angular.forEach($scope.countries, function (member, index) {
                member.index = index + 1;
            });
        });
        $scope.country = {
            "deleteflag": false
        };

        /****************************************************************************CREATE************************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.country.name == '' || $scope.country.name == null) {
                $scope.country.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.countries.post($scope.country).then(function () {
                    console.log('$scope.country', $scope.country);
                    window.location = '/countries';
                });
            }
        };

        /***********************************************************************************UPDATE****************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.country.name == '' || $scope.country.name == null) {
                $scope.country.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.countries.customPUT($scope.country).then(function () {
                    console.log('$scope.country', $scope.country);
                    window.location = '/countries';
                });
            }
        };
        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



        if ($routeParams.id) {
            $scope.message = 'Countries has been Updated!';
            Restangular.one('countries', $routeParams.id).get().then(function (country) {
                $scope.original = country;
                $scope.country = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Countries has been Created!';
        }


        /**********************************************************************************************DELETE*************************/

        //        $scope.Delete = function (id) {
        //            if (confirm("Are you sure want to delete..!") == true) {
        //                Restangular.one('countries/' + id).remove($scope.country).then(function () {
        //                    $route.reload();
        //                });
        //
        //            } else {
        //
        //            }
        //
        //        }
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('countries/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
