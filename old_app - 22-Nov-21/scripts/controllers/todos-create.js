'use strict';

angular.module('secondarySalesApp')
    .controller('TodosCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';
        $scope.languageId = $window.sessionStorage.language;

        $scope.showForm = function () {
            var visible = $location.path() === '/todos/create' || $location.path() === '/todos/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todos/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todos/create' || $location.path() === '/todos/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todos/create' || $location.path() === '/todos/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/todos-form" || $window.sessionStorage.prviousLocation != "partials/todos-form") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        $scope.todolanguage = Restangular.one('todolanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;


        //        $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        $scope.todo = {
            memberid: '',
            followupdate: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };

        /************************************* INDEX ***************************************************/
if($window.sessionStorage.orgLevel==='16'){
        $scope.todoUrl = 'todos?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignedto]=0';
    }else{
        $scope.todoUrl = 'todos?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignedto]='+$window.sessionStorage.userId;
    }
        Restangular.all($scope.todoUrl).getList().then(function (tdo) {
            $scope.todos = tdo;
            angular.forEach($scope.todos, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.orgStructureSplit = $window.sessionStorage.orgStructure.split(";");
        $scope.intermediateOrgStructure = "";
        for(var i=0;i<$scope.orgStructureSplit.length-1;i++){
            if ($scope.intermediateOrgStructure === "") {
            $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
            }
            else{
                $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" +$scope.orgStructureSplit[i];
            }
        }

        if($scope.orgStructureSplit.length>0){
            $scope.orgStructureFilterArray = [];
            $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length-1].split("-")[1].split(",");
            for(var i=0;i<$scope.LocationsSplit.length;i++){
                $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure+";"+$scope.orgStructureSplit[$scope.orgStructureSplit.length-1].split("-")[0]+"-"+$scope.LocationsSplit[i]);
            }

        }
        $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[","").replace("]","");
        console.log("$scope.orgStructureFilter",JSON.stringify($scope.orgStructureFilterArray).replace("[","").replace("]",""));

    
     if($window.sessionStorage.orgLevel==='16'){
        $scope.memberUrl = 'members?filter={"where":{"and":[{"orgstructure":{"inq":['+$scope.orgStructureFilter+']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[0]}}]}}';
    }else{
        $scope.memberUrl = 'members?filter={"where":{"and":[{"orgstructure":{"inq":['+$scope.orgStructureFilter+']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":['+$window.sessionStorage.userId+']}}]}}';
    }

        Restangular.all($scope.memberUrl).getList().then(function (member) {
            $scope.members = member;
        });

        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (type) {
            $scope.todotypes = type;
            // $scope.todo.todotypeid = $scope.todo.todotypeid;
        });

        Restangular.all('todostatuses?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (tstatus) {
            $scope.todostatuses = tstatus;
            //$scope.todo.todostatusid = $scope.todo.todostatusid;
        });

        Restangular.all('users').getList().then(function (usr) {
            $scope.users = usr;
            $scope.todo.assignedto = $scope.todo.assignedto;
        });

        $scope.$watch('todo.todotypeid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {

                Restangular.one('todotypes', newValue).get().then(function (type) {
                    var dueDate = parseInt(type.due);
                    var fifteendays = new Date();
                    fifteendays.setDate(fifteendays.getDate() + dueDate);
                    $scope.todo.followupdate = fifteendays;
                });
            }
        });

        $scope.$watch('todo.memberid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {

                Restangular.one('members', newValue).get().then(function (member) {
                    $scope.todo.orgstructure = member.orgstructure;
                });
            }
        });

        $scope.validatestring = "";

        $scope.Save = function (clicked) {

            if ($scope.todo.memberid == '' || $scope.todo.memberid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.todo.todotypeid == '' || $scope.todo.todotypeid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo Type';

            } else if ($scope.todo.todostatusid == '' || $scope.todo.todostatusid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo Status';

            } else if ($scope.todo.followupdate == '' || $scope.todo.followupdate == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup Date';

            } else if ($scope.todo.assignedto == '' || $scope.todo.assignedto == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.all('todos').post($scope.todo).then(function (resp) {
                    window.location = '/todos';
                });
            }
        };

        $scope.Update = function (clicked) {

            if ($scope.todo.memberid == '' || $scope.todo.memberid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.todo.todotypeid == '' || $scope.todo.todotypeid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo Type';

            } else if ($scope.todo.todostatusid == '' || $scope.todo.todostatusid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo Status';

            } else if ($scope.todo.followupdate == '' || $scope.todo.followupdate == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup Date';

            } else if ($scope.todo.assignedto == '' || $scope.todo.assignedto == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('todos', $routeParams.id).customPUT($scope.todo).then(function (resp) {
                    window.location = '/todos';
                });
            }
        };

        if ($routeParams.id) {

            $scope.message = 'Todo has been updated!';

            Restangular.one('todos', $routeParams.id).get().then(function (td) {

                Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (type) {
                    $scope.todotypes = type;
                    Restangular.all('todostatuses?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (tstatus) {
                        $scope.todostatuses = tstatus;
                        $scope.original = td;
                        $scope.todo = Restangular.copy($scope.original);
                        $scope.todo.todotypeid = td.todotypeid;
                        $scope.todo.todostatusid = td.todostatusid;
                    });
                });


            });
        } else {
            $scope.message = 'Todo has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todos/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getmember = function (memberid) {
            return Restangular.one('members', memberid).get().$object;
        };

        $scope.gettodotype = function (todotypeid) {
            return Restangular.one('todotypes', todotypeid).get().$object;
        };

        $scope.gettodostatus = function (todostatusid) {
            return Restangular.one('todostatuses', todostatusid).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });