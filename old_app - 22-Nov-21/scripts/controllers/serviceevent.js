'use strict';

angular.module('secondarySalesApp')
    .controller('ServiceEventCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
//        if ($window.sessionStorage.roleId != 1) {
//            window.location = "/";
//        }

        $scope.showForm = function () {
            var visible = $location.path() === '/serviceevent/create' || $location.path() === '/serviceevent/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/serviceevent/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/serviceevent/create' || $location.path() === '/serviceevent/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/serviceevent/create' || $location.path() === '/serviceevent/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/serviceevent") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.serviceevents = zn;
            angular.forEach($scope.serviceevents, function (member, index) {
                member.index = index + 1;
            });
        });

        if ($routeParams.id) {
            Restangular.one('serviceevents', $routeParams.id).get().then(function (srvcevent) {
              $scope.original = srvcevent;

              $scope.serviceevent = Restangular.copy($scope.original);
      
                 
            });
          };

          $scope.serviceevent = {
            deleteflag: false,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };

          $scope.validatestring = '';
          $scope.submitDisable = false;
        
          $scope.Save = function () {
            console.log('i m inside save');
            document.getElementById('serviceeventname').style.border = "";
            document.getElementById('serviceeventorder').style.border = "";
      
            if ($scope.serviceevent.name == '' || $scope.serviceevent.name == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Service Event';
              document.getElementById('serviceeventname').style.border = "1px solid #ff0000";
      
            } else if ($scope.serviceevent.order == '' || $scope.serviceevent.order == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Order';
              document.getElementById('serviceeventorder').style.border = "1px solid #ff0000";
      
            } 
      
            if ($scope.validatestring != '') {
              $scope.toggleValidation();
              $scope.validatestring1 = $scope.validatestring;
              $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
              Restangular.all('serviceevents').post($scope.serviceevent).then(function (conResponse) {
                console.log('conResponse', conResponse);
                $scope.message = 'Service Event has been created!';
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                
                setTimeout(function () {
                  window.location = '/serviceevent';
                  // window.location = '/workflowlanguage';
                }, 350);
              });
            };
      
          };

          $scope.Update = function () {
            console.log('i m inside update');
            document.getElementById('serviceeventname').style.border = "";
            document.getElementById('serviceeventorder').style.border = "";
      
            if ($scope.serviceevent.name == '' || $scope.serviceevent.name == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Service Event';
              document.getElementById('serviceeventname').style.border = "1px solid #ff0000";
      
            } else if ($scope.serviceevent.order == '' || $scope.serviceevent.order == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Order';
              document.getElementById('serviceeventorder').style.border = "1px solid #ff0000";
      
            } 
      
            if ($scope.validatestring != '') {
              $scope.toggleValidation();
              $scope.validatestring1 = $scope.validatestring;
              $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
              Restangular.all('serviceevents/' + $routeParams.id).customPUT($scope.serviceevent).then(function (conResponse) {
                console.log('conResponse', conResponse);
                $scope.message = 'Service Event has been updated!';
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                
                setTimeout(function () {
                  window.location = '/serviceevent';
                  // window.location = '/workflowlanguage';
                }, 350);
              });
            };
      
          };

        

       


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        


        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('genders/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
