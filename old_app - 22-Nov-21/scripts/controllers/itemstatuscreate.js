'use strict';
angular.module('secondarySalesApp')
    .controller('ItemstatusCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        //console.log("Item Status")
        $scope.showForm = function () {
            var visible = $location.path() === '/itemstatuscreate' || $location.path() === '/itemstatus/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/itemstatuscreate';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/itemstatuscreate';
            return visible;
        };

        if ($location.path() === '/itemstatuscreate') {
            $scope.disableDropdown = false;
          } else {
            $scope.disableDropdown = true;
          }
          /************************************************************************************/
          $scope.itemdefinition = {
            //organizationId: $window.sessionStorage.organizationId,
            deleteflag: false
          };

          /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/itemstatuscreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
      $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
      $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
      };
  
      $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
      $scope.pageFunction = function (mypage) {
        console.log('mypage', mypage);
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
      };

      // ******************************** EOF Pagination *************************************************
      $scope.itemstatus = {
        //organizationId: $window.sessionStorage.organizationId,
        deleteflag: false
      };

      /****************************************** CREATE *********************************/
      $scope.showValidation = false;
      $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
      };
      $scope.validatestring = '';
      $scope.submitDisable = false;

      $scope.Save = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.itemcategory, "message-282")
          if ($scope.itemstatus.itemstatus == '' || $scope.itemstatus.itemstatus == null) {
          $scope.itemstatus.itemstatus = null;
          $scope.validatestring = $scope.validatestring + 'Please  Enter Item Status';
          document.getElementById('name').style.border = "1px solid #ff0000";

          } 
        //   else if ($scope.itemstatus.description == '' || $scope.itemstatus.description == null) {
        //       $scope.itemstatus.description = null;
        //       $scope.validatestring = $scope.validatestring + 'Please  Enter Item Description';

        //   } 
          if ($scope.validatestring != '') {
          $scope.toggleValidation();
          $scope.validatestring1 = $scope.validatestring;
          $scope.validatestring = '';
          }
          else {
          $scope.message = 'Itemstatus has been created!';
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
          $scope.submitDisable = true;
          Restangular.all('itemstatuses').post($scope.itemstatus).then(function () {
              window.location = '/itemstatus';
          });
      }
      };

      /***************************** UPDATE *******************************************/

     $scope.validatestring = '';
     $scope.updatecount = 0;

     $scope.Update = function () {
     document.getElementById('name').style.border = "";
     //console.log($scope.itemcategory, "message-323")
         if ($scope.itemstatus.itemstatus == '' || $scope.itemstatus.itemstatus == null) {
         $scope.itemstatus.itemstatus = null;
         $scope.validatestring = $scope.validatestring + 'Please  Enter Item Status';
         document.getElementById('name').style.border = "1px solid #ff0000";

         } else if ($scope.itemstatus.description == '' || $scope.itemstatus.description == null) {
             $scope.itemstatus.description = null;
             $scope.validatestring = $scope.validatestring + 'Please  Enter Description';

         } 
         if ($scope.validatestring != '') {
         $scope.toggleValidation();
         $scope.validatestring1 = $scope.validatestring;
         $scope.validatestring = '';
         }
         else {
         $scope.message = 'Itemstatus has been updated!';
         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
         $scope.neWsubmitDisable = true;
         Restangular.one('itemstatuses/' + $routeParams.id).customPUT($scope.itemstatus).then(function () {
             window.location = '/itemstatus';
             
         });
       }
     };

       if ($routeParams.id) {
         console.log("$routeParams.id", $routeParams.id)
         Restangular.one('itemstatuses', $routeParams.id).get().then(function (itmstat) {
           $scope.original = itmstat;
           $scope.itemstatus = Restangular.copy($scope.original);
         });
       }

});