'use strict';

angular.module('secondarySalesApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'restangular',
  'ui.select2',
  'ui.bootstrap',
  'angularFileUpload',
  'angularUtils.directives.dirPagination',
  'ngIdle',
  'toggle-switch',
  'angucomplete',
  'services.breadcrumbs',
  'ngPageTitle',
  'confirmButton',
  'toaster',
  'confirmButton2'
])
  .config(function ($routeProvider, $locationProvider, RestangularProvider, $idleProvider, $keepaliveProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/main1',
        controller: 'MainCtrl',
        label: 'Home',
        data: {
          pageTitle: 'Home'
        }
      })
      .when('/login', {
        templateUrl: 'partials/login',
        controller: 'LoginCtrl',
        label: 'Login',
        data: {
          pageTitle: 'Login'
        }
      })
      .when('/roles', {
        templateUrl: 'partials/roles',
        controller: 'RolesCtrl',
        label: 'Roles',
        data: {
          pageTitle: 'Roles'
        }
      })
      .when('/roles/create', {
        templateUrl: 'partials/roles',
        controller: 'RolesCtrl',
        label: 'Roles',
        data: {
          pageTitle: 'Roles'
        }
      })
      .when('/roles/:id', {
        templateUrl: 'partials/roles',
        controller: 'RolesCtrl',
        label: 'Roles',
        data: {
          pageTitle: 'Roles'
        }
      })
      .when('/users', {
        templateUrl: 'partials/users',
        controller: 'UsersCtrl',
        label: 'Users',
        data: {
          pageTitle: 'Users'
        }
      })
      .when('/users/create', {
        templateUrl: 'partials/users-form',
        controller: 'UsersCreateCtrl',
        label: 'Users',
        data: {
          pageTitle: 'Users'
        }
      })
      .when('/users/:id', {
        templateUrl: 'partials/users-form',
        controller: 'UsersEditCtrl',
        label: 'Users',
        data: {
          pageTitle: 'Users'
        }
      })
      .when('/groups', {
        templateUrl: 'partials/groups',
        controller: 'GroupsCtrl'
      })
      .when('/groups/create', {
        templateUrl: 'partials/groups',
        controller: 'GroupsCtrl'
      })
      .when('/groups/:id', {
        templateUrl: 'partials/groups',
        controller: 'GroupsCtrl'
      })
      .when('/customfields', {
        templateUrl: 'partials/customfields',
        controller: 'CustomFieldsCtrl'
      })
      .when('/customfields/create', {
        templateUrl: 'partials/customfields',
        controller: 'CustomFieldsCtrl'
      })
      .when('/customfields/:id', {
        templateUrl: 'partials/customfields',
        controller: 'CustomFieldsCtrl'
      })
      .when('/countries', {
        templateUrl: 'partials/countries',
        controller: 'CountriesCtrl'
      })
      .when('/countries/create', {
        templateUrl: 'partials/countries',
        controller: 'CountriesCtrl'
      })
      .when('/countries/:id', {
        templateUrl: 'partials/countries',
        controller: 'CountriesCtrl'
      })
      .when('/states', {
        templateUrl: 'partials/states',
        controller: 'StatesCtrl'
      })
      .when('/states/create', {
        templateUrl: 'partials/states',
        controller: 'StatesCtrl'
      })
      .when('/states/:id', {
        templateUrl: 'partials/states',
        controller: 'StatesCtrl'
      })
      .when('/town', {
        templateUrl: 'partials/cities',
        controller: 'CitiesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/town/create', {
        templateUrl: 'partials/cities',
        controller: 'CitiesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/town/:id', {
        templateUrl: 'partials/cities',
        controller: 'CitiesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/uploadtown', {
        templateUrl: 'partials/uploadcities',
        controller: 'CityReadXlsCtrl',
        label: 'TGeography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/state', {
        templateUrl: 'partials/zones',
        controller: 'ZonesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/state/create', {
        templateUrl: 'partials/zones',
        controller: 'ZonesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/state/:id', {
        templateUrl: 'partials/zones',
        controller: 'ZonesCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/uploadstate', {
        templateUrl: 'partials/uploadzones',
        controller: 'ZoneReadXlsCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/district', {
        templateUrl: 'partials/sales-areas',
        controller: 'SalesAreasCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }


      })
      .when('/district/create', {
        templateUrl: 'partials/sales-areas',
        controller: 'SalesAreasCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/district/:id', {
        templateUrl: 'partials/sales-areas',
        controller: 'SalesAreasCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/uploaddistrict', {
        templateUrl: 'partials/uploadsales-areas',
        controller: 'DistReadXlsCtrl',
        label: 'Geography',
        data: {
          pageTitle: 'Geography'
        }
      })
      .when('/distribution-areas', {
        templateUrl: 'partials/distribution-areas',
        controller: 'DistributionAreasCtrl'
      })
      .when('/distribution-areas/create', {
        templateUrl: 'partials/distribution-areas',
        controller: 'DistributionAreasCtrl'
      })
      .when('/distribution-areas/:id', {
        templateUrl: 'partials/distribution-areas',
        controller: 'DistributionAreasCtrl'
      })
      .when('/distribution-subareas', {
        templateUrl: 'partials/distribution-subareas',
        controller: 'DistributionSubareasCtrl'
      })
      .when('/distribution-subareas/create', {
        templateUrl: 'partials/distribution-subareas',
        controller: 'DistributionSubareasCtrl'
      })
      .when('/distribution-subareas/:id', {
        templateUrl: 'partials/distribution-subareas',
        controller: 'DistributionSubareasCtrl'
      })
      /* .when('/employees', {
           templateUrl: 'partials/employees',
           controller: 'EmployeesCtrl'
       })*/
      .when('/facility', {
        templateUrl: 'partials/employees1',
        controller: 'EmployeesCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/facility/create', {
        templateUrl: 'partials/employees-form',
        controller: 'EmployeesCreateCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/facility/:id', {
        templateUrl: 'partials/employees-form',
        controller: 'EmployeesEditCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      /************New Added*********/
      .when('/facilitytype', {
        templateUrl: 'partials/facilitytype',
        controller: 'FacilityTypeCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/facilitytype/create', {
        templateUrl: 'partials/facilitytype',
        controller: 'FacilityTypeCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/facilitytype/:id', {
        templateUrl: 'partials/facilitytype',
        controller: 'FacilityTypeCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })

      .when('/areaofoperation', {
        templateUrl: 'partials/areaofoperation',
        controller: 'AreaofOpeartionCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/areaofoperation/create', {
        templateUrl: 'partials/areaofoperation',
        controller: 'AreaofOpeartionCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/areaofoperation/:id', {
        templateUrl: 'partials/areaofoperation',
        controller: 'AreaofOpeartionCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/12astatus', {
        templateUrl: 'partials/12astatus',
        controller: '12AStatusCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/12astatus/create', {
        templateUrl: 'partials/12astatus',
        controller: '12AStatusCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/12astatus/:id', {
        templateUrl: 'partials/12astatus',
        controller: '12AStatusCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/tirating', {
        templateUrl: 'partials/tirating',
        controller: 'TiRatingCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/tirating/create', {
        templateUrl: 'partials/tirating',
        controller: 'TiRatingCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/tirating/:id', {
        templateUrl: 'partials/tirating',
        controller: 'TiRatingCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      /* .when('/80gstatus', {
           templateUrl: 'partials/80gstatus',
           controller: '80gStatusCtrl'
       })
       .when('/80gstatus/create', {
           templateUrl: 'partials/80gstatus',
           controller: '80gStatusCtrl'
       })
       .when('/80gstatus/:id', {
           templateUrl: 'partials/80gstatus',
           controller: '80gStatusCtrl'
       })
       .when('/fcrastaus', {
           templateUrl: 'partials/fcrastaus',
           controller: 'FcraStausCtrl'
       })
       .when('/fcrastaus/create', {
           templateUrl: 'partials/fcrastaus',
           controller: 'FcraStausCtrl'
       })
       .when('/fcrastaus/:id', {
           templateUrl: 'partials/fcrastaus',
           controller: 'FcraStausCtrl'
       })*/
      /*****************************/
      .when('/facilitymanager', {
        templateUrl: 'partials/co-org',
        controller: 'COgrCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/facilitymanager/create', {
        templateUrl: 'partials/co-org-form',
        controller: 'COCreateCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/facilitymanager/:id', {
        templateUrl: 'partials/co-org-form',
        controller: 'COEditCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/upload-facilitymanager', {
        templateUrl: 'partials/upload-co-org',
        controller: 'CoOrgReadXlsCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/fieldworkers', {
        templateUrl: 'partials/f-work',
        controller: 'FWCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/fieldworker/create', {
        templateUrl: 'partials/f-work-form',
        controller: 'FWCreateCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/fieldworker/:id', {
        templateUrl: 'partials/f-work-form',
        controller: 'FWEditCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      //            .when('/fieldworkers/:id', {
      //                templateUrl: 'partials/f-work-form',
      //                controller: 'FWEditCtrl',
      //                label: 'Field Work Profile Edit',
      //                data: {
      //                    pageTitle: 'Field Worker'
      //                }
      //            })
      .when('/tickets', {
        templateUrl: 'partials/beneficiaries',
        controller: 'BnCtrl',
        label: 'Tickets',
        data: {
          pageTitle: 'Tickets'
        }
      })
      .when('/tickets/create', {
        templateUrl: 'partials/beneficiaries-form',
        controller: 'BnCreateCtrl',
        label: 'Tickets Create',
        data: {
          pageTitle: 'Tickets Create'
        }
      })
      .when('/tickets/:id', {
        templateUrl: 'partials/beneficiaries-form',
        controller: 'BnEditCtrl',
        label: 'Tickets Edit',
        data: {
          pageTitle: 'Tickets Edit'
        }
      })

      /* .when('/coorgedit', {
               templateUrl: 'partials/coorgedit',
               controller: 'COOrgEditCtrl',
               label: 'Facility Profile',
               data: {
                   pageTitle: 'Facility Profile'
               }
           })*/
      .when('/profileedit/:id', {
        templateUrl: 'partials/coorgedit',
        controller: 'COOrgEditCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }

      })
      .when('/sites', {
        templateUrl: 'partials/routes',
        controller: 'RoutesCtrl',
        label: 'Sites',
        data: {
          pageTitle: 'Sites'
        }

      })
      .when('/site/create', {
        templateUrl: 'partials/routes-form',
        controller: 'RoutesCreateCtrl',
        label: 'Sites  ',
        data: {
          pageTitle: 'Sites'
        }
      })
      .when('/site/:id', {
        templateUrl: 'partials/routes-form',
        controller: 'RoutesEditCtrl',
        label: 'Sites',
        data: {
          pageTitle: 'Sites'
        }
      })
      .when('/uploadsite', {
        templateUrl: 'partials/uploadroutes',
        controller: 'RouteReadXlsCtrl',
        label: 'Sites',
        data: {
          pageTitle: 'Sites'
        }
      })
      .when('/managersite', {
        templateUrl: 'partials/routesviewlist',
        controller: 'RoutesViewListCtrl',
        label: 'Sites',
        data: {
          pageTitle: 'Sites'
        }

      })
      .when('/routesview/:id', {
        templateUrl: 'partials/routesview',
        controller: 'RoutesViewCtrl',
        label: 'Site Detials'
      })

      .when('/routelinks', {
        templateUrl: 'partials/routelinks',
        controller: 'RoutelinksCtrl'
      })
      .when('/routelinks/create', {
        templateUrl: 'partials/routelinks',
        controller: 'RoutelinksCtrl'
      })
      .when('/routelinks/:id', {
        templateUrl: 'partials/routelinks',
        controller: 'RoutelinksCtrl'
      })
      .when('/route_assignment', {
        templateUrl: 'partials/route_assignmentdisplays',
        controller: 'RouteassignmentDisplayCtrl'
      })
      .when('/route_assignment/create', {
        templateUrl: 'partials/route_assignment',
        controller: 'RouteassignmentCtrl'
      })
      .when('/route_assignment/:id', {
        templateUrl: 'partials/route_assignmentupdate',
        controller: 'RouteassignmentUpdateCtrl'
      })
      .when('/maps', {
        templateUrl: 'partials/maps',
        controller: 'TestCtrl'
      })
      .when('/maps/create', {
        templateUrl: 'partials/maps',
        controller: 'TestCtrl'
      })
      .when('/maps/:id', {
        templateUrl: 'partials/maps',
        controller: 'TestCtrl'
      })
      .when('/tourtypes', {
        templateUrl: 'partials/tourtypes',
        controller: 'TourtypeCtrl'
      })
      .when('/tourtypes/create', {
        templateUrl: 'partials/tourtypes',
        controller: 'TourtypeCtrl'
      })
      .when('/tourtypes/:id', {
        templateUrl: 'partials/tourtypes',
        controller: 'TourtypeCtrl'
      })
      .when('/assignfwtosite', {
        templateUrl: 'partials/agentroutelinks',
        controller: 'AgentRoutelinksCtrl',
        label: 'Assign FW To Site',
        data: {
          pageTitle: 'Assign FW To Site'
        }
      })
      .when('/assignfwtosite/create', {
        templateUrl: 'partials/agentroutelinks',
        controller: 'AgentRoutelinksCtrl',
        label: 'Assign FW To Site',
        data: {
          pageTitle: 'Assign FW To Site'
        }
      })
      .when('/assignfwtosite/:id', {
        templateUrl: 'partials/agentroutelinks',
        controller: 'AgentRoutelinksCtrl',
        label: 'Assign FW To Site',
        data: {
          pageTitle: 'Assign FW To Site'
        }
      })
      /*.when('/imageupload', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/create', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/:id', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })*/
      .when('/socialprotection', {
        templateUrl: 'partials/sdoctypes',
        controller: 'SDocTypCtrl',
        label: 'Apply For Document',
        data: {
          pageTitle: 'Apply For Document'
        }

      })
      .when('/socialprotection/create', {
        templateUrl: 'partials/sdoctypes',
        controller: 'SDocTypCtrl',
        label: 'Apply For Document',
        data: {
          pageTitle: 'Apply For Document'
        }
      })
      .when('/socialprotection/:id', {
        templateUrl: 'partials/sdoctypes',
        controller: 'SDocTypCtrl',
        label: 'Apply For Document',
        data: {
          pageTitle: 'Apply For Document'
        }
      })
      .when('/fdocumenttypes', {
        templateUrl: 'partials/fdoctypes',
        controller: 'FDocTypCtrl'
      })
      .when('/fdocumenttypes/create', {
        templateUrl: 'partials/fdoctypes',
        controller: 'FDocTypCtrl'
      })
      .when('/fdocumenttypes/:id', {
        templateUrl: 'partials/fdoctypes',
        controller: 'FDocTypCtrl'
      })

      //        .when('/schemes', {
      //                templateUrl: 'partials/scheme',
      //                controller: 'SchCtrl'
      //            })
      //            .when('/schemes/create', {
      //                templateUrl: 'partials/scheme',
      //                controller: 'SchCtrl'
      //            })
      //            .when('/schemes/:id', {
      //                templateUrl: 'partials/scheme',
      //                controller: 'SchCtrl'
      //            })
      .when('/gender', {
        templateUrl: 'partials/gender',
        controller: 'GenCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/gender/create', {
        templateUrl: 'partials/gender',
        controller: 'GenCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/gender/:id', {
        templateUrl: 'partials/gender',
        controller: 'GenCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/updatetype', {
        templateUrl: 'partials/updatetype',
        controller: 'UpdateTypeCtrl',
        label: 'Update Type',
        data: {
          pageTitle: 'Update Type'
        }
      })
      .when('/updatetype/create', {
        templateUrl: 'partials/updatetype',
        controller: 'UpdateTypeCtrl',
        label: 'Update Type',
        data: {
          pageTitle: 'Update Type'
        }
      })
      .when('/updatetype/:id', {
        templateUrl: 'partials/updatetype',
        controller: 'UpdateTypeCtrl',
        label: 'Update Type',
        data: {
          pageTitle: 'Update Type'
        }
      })
      .when('/serviceevent', {
        templateUrl: 'partials/serviceevent',
        controller: 'ServiceEventCtrl',
        label: 'Service Event',
        data: {
          pageTitle: 'Service Event'
        }
      })
      .when('/serviceevent/create', {
        templateUrl: 'partials/serviceevent',
        controller: 'ServiceEventCtrl',
        label: 'Service Event',
        data: {
          pageTitle: 'Service Event'
        }
      })
      .when('/serviceevent/:id', {
        templateUrl: 'partials/serviceevent',
        controller: 'ServiceEventCtrl',
        label: 'Service Event',
        data: {
          pageTitle: 'Service Event'
        }
      })
      .when('/typology', {
        templateUrl: 'partials/typology',
        controller: 'TypCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/typology/create', {
        templateUrl: 'partials/typology',
        controller: 'TypCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/typology/:id', {
        templateUrl: 'partials/typology',
        controller: 'TypCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/occupation', {
        templateUrl: 'partials/occupation',
        controller: 'OccupationCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/occupation/create', {
        templateUrl: 'partials/occupation',
        controller: 'OccupationCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/occupation/:id', {
        templateUrl: 'partials/occupation',
        controller: 'OccupationCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/education', {
        templateUrl: 'partials/education',
        controller: 'EduCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/education/create', {
        templateUrl: 'partials/education',
        controller: 'EduCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/education/:id', {
        templateUrl: 'partials/education',
        controller: 'EduCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/maritalstatus', {
        templateUrl: 'partials/maritalstatus',
        controller: 'MaryCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/maritalstatus/create', {
        templateUrl: 'partials/maritalstatus',
        controller: 'MaryCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/maritalstatus/:id', {
        templateUrl: 'partials/maritalstatus',
        controller: 'MaryCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/employmentstatuses', {
        templateUrl: 'partials/employmentstatus',
        controller: 'EmpstCtrl'
      })
      .when('/employmentstatuses/create', {
        templateUrl: 'partials/employmentstatus',
        controller: 'EmpstCtrl'
      })
      .when('/employmentstatuses/:id', {
        templateUrl: 'partials/employmentstatus',
        controller: 'EmpstCtrl'
      })
      .when('/physicalappearances', {
        templateUrl: 'partials/physicalappearance',
        controller: 'PhyApCtrl'
      })
      .when('/physicalappearances/create', {
        templateUrl: 'partials/physicalappearance',
        controller: 'PhyApCtrl'
      })
      .when('/physicalappearances/:id', {
        templateUrl: 'partials/physicalappearance',
        controller: 'PhyApCtrl'
      })
      .when('/emotionalstates', {
        templateUrl: 'partials/emotionalstate',
        controller: 'EmotionCtrl'
      })
      .when('/emotionalstates/create', {
        templateUrl: 'partials/emotionalstate',
        controller: 'EmotionCtrl'
      })
      .when('/emotionalstates/:id', {
        templateUrl: 'partials/emotionalstate',
        controller: 'EmotionCtrl'
      })

      .when('/cocategories', {
        templateUrl: 'partials/cocategories',
        controller: 'COCategoriesCtrl'
      })
      .when('/cocategories/create', {
        templateUrl: 'partials/cocategories',
        controller: 'COCategoriesCtrl'
      })
      .when('/cocategories/:id', {
        templateUrl: 'partials/cocategories',
        controller: 'COCategoriesCtrl'
      })
      .when('/reportincident', {
        templateUrl: 'partials/reportincident',
        controller: 'ReportIncidentCtrl',
        label: 'Report Incident Create',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/reportincident/:id', {
        templateUrl: 'partials/reportincident',
        controller: 'ReportIncidentEditCtrl',
        label: 'Report Incident Edit',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      //
      //        .when('/socialprotection', {
      //                templateUrl: 'partials/socialprotection',
      //                controller: 'SocialProtectionCtrl'
      //            })
      .when('/health', {
        templateUrl: 'partials/health',
        controller: 'HealthCtrl'
      })
      .when('/outreachactivity', {
        templateUrl: 'partials/outreachactivity',
        controller: 'OutReachActivityCtrl'
      })
      .when('/codashboard', {
        templateUrl: 'partials/codashboard',
        controller: 'CODashboardCtrl',
        label: 'CO Dashboard',
        data: {
          pageTitle: 'CO Dashboard'
        }
      })
      .when('/applicationstage', {
        templateUrl: 'partials/applicationstages',
        controller: 'applicationstageCtrl'
      })
      .when('/applicationstage/create', {
        templateUrl: 'partials/applicationstages',
        controller: 'applicationstageCtrl'
      })
      .when('/applicationstage/:id', {
        templateUrl: 'partials/applicationstages',
        controller: 'applicationstageCtrl'
      })
      .when('/schemestage', {
        templateUrl: 'partials/schemestage',
        controller: 'schemestageCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemestage/create', {
        templateUrl: 'partials/schemestage',
        controller: 'schemestageCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemestage/:id', {
        templateUrl: 'partials/schemestage',
        controller: 'schemestageCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/coactivity', {
        templateUrl: 'partials/coactivity',
        controller: 'BnCreateCtrl'
      })
      .when('/resetpassword', {
        templateUrl: 'partials/resetpassword',
        controller: 'ResetPswdCtrl',
        label: 'Reset Password'
      })
      .when('/schemes', {
        templateUrl: 'partials/schemes',
        controller: 'SchemeMastersCtrl',
        label: 'Scheme List',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemes/create', {
        templateUrl: 'partials/schemes',
        controller: 'SchemeMastersCtrl',
        label: 'Scheme Create',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemes/:id', {
        templateUrl: 'partials/schemes',
        controller: 'SchemeMastersCtrl',
        label: 'Scheme Edit',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/fwarchieve', {
        templateUrl: 'partials/fieldworkerarchieve',
        controller: 'FWArchieveCtrl',
        label: 'Archived Members',
        data: {
          pageTitle: 'Archived Members'
        }
      })
      .when('/coarchieve', {
        templateUrl: 'partials/coarchieve',
        controller: 'COArchieveCtrl',
        label: 'Archived Member',
        data: {
          pageTitle: 'Archived Members'
        }
      })
      .when('/mysites', {
        templateUrl: 'partials/fieldworkersites',
        controller: 'FWSitesCtrl',
        label: 'Site List',
        data: {
          pageTitle: 'Sites'
        }
      })
      .when('/groupmeetings', {
        templateUrl: 'partials/groupmeeting',
        controller: 'GroupMeetingsCtrl',
        label: 'Group Meetings',
        data: {
          pageTitle: 'Group Meetings'
        }
      })
      .when('/groupmeeting/create', {
        templateUrl: 'partials/groupmeeting',
        controller: 'GroupMeetingsCtrl',
        label: 'Group Meetings ',
        data: {
          pageTitle: 'Group Meetings'
        }
      })
      .when('/groupmeeting/:id', {
        templateUrl: 'partials/groupmeeting',
        controller: 'EditGroupMeetingsCtrl',
        label: 'Group Meetings ',
        data: {
          pageTitle: 'Group Meetings '
        }
      })
      .when('/events', {
        templateUrl: 'partials/events',
        controller: 'EventsCtrl',
        label: 'Event List',
        data: {
          pageTitle: 'Events'
        }
      })
      .when('/event/create', {
        templateUrl: 'partials/events',
        controller: 'EventsCtrl',
        label: 'Events',
        data: {
          pageTitle: 'Events'
        }
      })
      .when('/event/:id', {
        templateUrl: 'partials/events',
        controller: 'EditEventsCtrl',
        label: 'Events',
        data: {
          pageTitle: 'Events'
        }
      })

      .when('/applyforscheme', {
        templateUrl: 'partials/applyforscheme',
        controller: 'ApplyForSchemeCtrl',
        label: 'Apply For Scheme Create',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/applyforschemes/create', {
        templateUrl: 'partials/applyforscheme',
        controller: 'ApplyForSchemeCtrl',
        label: 'Apply For Scheme Create',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/applyforschemes/:id', {
        templateUrl: 'partials/applyforscheme',
        controller: 'EditSchemeCtrl',
        label: 'Apply For Scheme Edit',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/applyfordocument', {
        templateUrl: 'partials/applyfordocument',
        controller: 'ApplyForDocumentCtrl',
        label: 'Apply For Document Create',
        data: {
          pageTitle: 'Apply For Document'
        }
      })
      .when('/applyfordocuments/create', {
        templateUrl: 'partials/applyfordocument',
        controller: 'ApplyForDocumentCtrl',
        label: 'Apply For Document Create',
        data: {
          pageTitle: 'Apply For Document'
        }
      })
      .when('/applyfordocuments/:id', {
        templateUrl: 'partials/applyfordocument',
        controller: 'EditDocumentCtrl',
        label: 'Apply For Document Edit',
        data: {
          pageTitle: 'Apply For Document'
        }
      })
      .when('/onetoone/:id', {
        templateUrl: 'partials/onetoone1',
        controller: 'OneToOneCtrl',
        label: 'OnetoOne',
        data: {
          pageTitle: 'One To One'
        }
      })
      /*  .when('/surveyquestions', {
            templateUrl: 'partials/surveyquestions',
            controller: 'SurveyQuestionsCtrl'
        })
        .when('/surveyquestions/create', {
            templateUrl: 'partials/surveyquestions',
            controller: 'SurveyQuestionsCtrl'
        })
        .when('/surveyquestions/:id', {
            templateUrl: 'partials/surveyquestions',
            controller: 'SurveyQuestionsCtrl'
        })*/
      .when('/pillars', {
        templateUrl: 'partials/pillars',
        controller: 'PillarsCtrl',
        label: 'Pillars',
        data: {
          pageTitle: 'Pillars'
        }

      })
      .when('/pillars/create', {
        templateUrl: 'partials/pillars',
        controller: 'PillarsCtrl',
        label: 'Pillars',
        data: {
          pageTitle: 'Pillars'
        }
      })
      .when('/pillars/:id', {
        templateUrl: 'partials/pillars',
        controller: 'PillarsCtrl',
        label: 'Pillars',
        data: {
          pageTitle: 'Pillars'
        }
      })
      .when('/topics', {
        templateUrl: 'partials/groupmeetingtopic',
        controller: 'groupmeetingtopicCtrl',
        label: 'Group Meetings',
        data: {
          pageTitle: 'Group Meetings'
        }
      })
      .when('/topics/create', {
        templateUrl: 'partials/groupmeetingtopic',
        controller: 'groupmeetingtopicCtrl',
        label: 'Group Meetings',
        data: {
          pageTitle: 'Group Meetings'
        }
      })
      .when('/topics/:id', {
        templateUrl: 'partials/groupmeetingtopic',
        controller: 'groupmeetingtopicCtrl',
        label: 'Group Meetings',
        data: {
          pageTitle: 'Group Meetings'
        }
      })
      .when('/groupmeetingevents', {
        templateUrl: 'partials/groupmeetingevent',
        controller: 'groupmeetingeventCtrl'
      })
      .when('/groupmeetingevents/create', {
        templateUrl: 'partials/groupmeetingevent',
        controller: 'groupmeetingeventCtrl'
      })
      .when('/groupmeetingevents/:id', {
        templateUrl: 'partials/groupmeetingevent',
        controller: 'groupmeetingeventCtrl'
      })
      .when('/stakeholdermeetings', {
        templateUrl: 'partials/stakeholder',
        controller: 'StakeHolderCtrl',
        label: 'Stakeholder Meetings ',
        data: {
          pageTitle: 'Stakeholder Meetings '
        }
      })
      .when('/stakeholdermeeting/create', {
        templateUrl: 'partials/stakeholder',
        controller: 'StakeHolderCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings '
        }
      })
      .when('/stakeholdermeeting/:id', {
        templateUrl: 'partials/stakeholder',
        controller: 'EditStakeHolderCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('/eventtype', {
        templateUrl: 'partials/eventtypes',
        controller: 'EventTypesCtrl',
        label: 'Event Type',
        data: {
          pageTitle: 'Event Type'
        }
      })
      .when('/eventtype/create', {
        templateUrl: 'partials/eventtypes',
        controller: 'EventTypesCtrl',
        label: 'Event Type Create',
        data: {
          pageTitle: 'Event Type Create'
        }
      })
      .when('/eventtype/:id', {
        templateUrl: 'partials/eventtypes',
        controller: 'EventTypesCtrl',
        label: 'Event Type Edit',
        data: {
          pageTitle: 'Event Type Edit'
        }
      })
      .when('/followuptypes', {
        templateUrl: 'partials/followuptypes',
        controller: 'FollowUpTypesCtrl'
      })
      .when('/followuptypes/create', {
        templateUrl: 'partials/followuptypes',
        controller: 'FollowUpTypesCtrl'
      })
      .when('/followuptypes/:id', {
        templateUrl: 'partials/followuptypes',
        controller: 'FollowUpTypesCtrl'
      })
      .when('/bulkupdates', {
        templateUrl: 'partials/bulkupdate',
        controller: 'BulkUpCtrl',
        label: 'Bulkupdate List',
        data: {
          pageTitle: 'Bulk Updates'
        }
      })
      .when('/bulkupdates/create', {
        templateUrl: 'partials/bulkupdate',
        controller: 'BulkUpCtrl',
        label: 'Bulkupdate Create',
        data: {
          pageTitle: 'Bulk Updates'
        }
      })
      .when('/bulkupdates/:id', {
        templateUrl: 'partials/bulkupdate',
        controller: 'BulkUPEditCtrl',
        label: 'Bulkupdate Edit',
        data: {
          pageTitle: 'Bulk Updates'
        }
      })
      .when('/todofilters', {
        templateUrl: 'partials/mytodostatuses',
        controller: 'MyToDoStatusesCtrl',
        label: '1-1 ToDos',
        data: {
          pageTitle: '1-1 ToDos'
        }
      })
      .when('/todofilters/create', {
        templateUrl: 'partials/mytodostatuses',
        controller: 'MyToDoStatusesCtrl',
        label: '1-1 ToDos',
        data: {
          pageTitle: '1-1 ToDos'
        }
      })
      .when('/todofilters/:id', {
        templateUrl: 'partials/mytodostatuses',
        controller: 'MyToDoStatusesCtrl',
        label: '1-1 ToDos',
        data: {
          pageTitle: '1-1 ToDos'
        }
      })


      .when('/trackapplications', {
        templateUrl: 'partials/trackapplications',
        controller: 'TrackApplicationCtrl',
        label: 'Applications',
        data: {
          pageTitle: 'Applications'
        }
      })
      .when('/stakeholdertype', {
        templateUrl: 'partials/stakeholdertype',
        controller: 'stakeholdertypesCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('/stakeholdertype/create', {
        templateUrl: 'partials/stakeholdertype',
        controller: 'stakeholdertypesCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('/stakeholdertype/:id', {
        templateUrl: 'partials/stakeholdertype',
        controller: 'stakeholdertypesCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })

      /********new added*******/
      .when('/financialgoal', {
        templateUrl: 'partials/financialgoal',
        controller: 'FinancialGoalCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialgoal/create', {
        templateUrl: 'partials/financialgoal',
        controller: 'FinancialGoalCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialgoal/:id', {
        templateUrl: 'partials/financialgoal',
        controller: 'FinancialGoalCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialproduct', {
        templateUrl: 'partials/financialproduct',
        controller: 'FinancialProductCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialproduct/create', {
        templateUrl: 'partials/financialproduct',
        controller: 'FinancialProductCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialproduct/:id', {
        templateUrl: 'partials/financialproduct',
        controller: 'FinancialProductCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/chartdropdown', {
        templateUrl: 'partials/chartdropdown',
        controller: 'ChartDropdownCtrl',
        label: 'Chart Dropdown',
        data: {
          pageTitle: 'Chart Dropdown'
        }
      })
      .when('/chartdropdown/create', {
        templateUrl: 'partials/chartdropdown',
        controller: 'ChartDropdownCtrl',
        label: 'Chart Dropdown',
        data: {
          pageTitle: 'Chart Dropdown'
        }
      })
      .when('/chartdropdown/:id', {
        templateUrl: 'partials/chartdropdown',
        controller: 'ChartDropdownCtrl',
        label: 'Chart Dropdown',
        data: {
          pageTitle: 'Chart Dropdown'
        }
      })
      .when('/timeinyear', {
        templateUrl: 'partials/timeinyear',
        controller: 'TimeinYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/timeinyear/create', {
        templateUrl: 'partials/timeinyear',
        controller: 'TimeinYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/timeinyear/:id', {
        templateUrl: 'partials/timeinyear',
        controller: 'TimeinYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/budgetyear', {
        templateUrl: 'partials/budgetyear',
        controller: 'BudgetYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/budgetyear/create', {
        templateUrl: 'partials/budget',
        controller: 'BudgetYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/budgetyear/:id', {
        templateUrl: 'partials/budgetyear',
        controller: 'BudgetYearCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })

      .when('/loantracking', {
        templateUrl: 'partials/loantracking',
        controller: 'LoanTrackingCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/loantracking/create', {
        templateUrl: 'partials/loantracking',
        controller: 'LoanTrackingCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/loantracking/:id', {
        templateUrl: 'partials/loantracking',
        controller: 'LoanTrackingCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })


      /***********************/
      .when('/stakeholder', {
        templateUrl: 'partials/followupstack',
        controller: 'followupstacksCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/stakeholder/create', {
        templateUrl: 'partials/followupstack',
        controller: 'followupstacksCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/stakeholder/:id', {
        templateUrl: 'partials/followupstack',
        controller: 'followupstacksCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupbulkupdates', {
        templateUrl: 'partials/followupbulkupdate',
        controller: 'followupbulkupdatesCtrl'
      })
      .when('/followupbulkupdates/create', {
        templateUrl: 'partials/followupbulkupdate',
        controller: 'followupbulkupdatesCtrl'
      })
      .when('/followupbulkupdates/:id', {
        templateUrl: 'partials/followupbulkupdate',
        controller: 'followupbulkupdatesCtrl'
      })
      .when('/purposeofmeet', {
        templateUrl: 'partials/purposeofmeeting',
        controller: 'purposeofmeetingsCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/purposeofmeet/create', {
        templateUrl: 'partials/purposeofmeeting',
        controller: 'purposeofmeetingsCtrl',
        label: 'POtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/purposeofmeet/:id', {
        templateUrl: 'partials/purposeofmeeting',
        controller: 'purposeofmeetingsCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupgroup', {
        templateUrl: 'partials/followupgroup',
        controller: 'followupgroupsCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupgroup/create', {
        templateUrl: 'partials/followupgroup',
        controller: 'followupgroupsCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupgroup/:id', {
        templateUrl: 'partials/followupgroup',
        controller: 'followupgroupsCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/fwdashboard', {
        templateUrl: 'partials/fwdashboard',
        controller: 'CODashboardCtrl',
        label: 'FW Dashboard',
        data: {
          pageTitle: 'FW Dashboard'
        }
      })
      .when('/memberdatamigration', {
        templateUrl: 'partials/memberdatamigration',
        controller: 'memberdatamigrationCtrl'
      })
      .when('/uploadfacility', {
        templateUrl: 'partials/uploadfacility',
        controller: 'FacilityReadXlsCtrl',
        label: 'Facility profile',
        data: {
          pageTitle: 'Facility profile'
        }
      })
      .when('/uploadscheme', {
        templateUrl: 'partials/uploadscheme',
        controller: 'SchemeReadXlsCtrl',
        label: 'Scheme Upload',
        data: {
          pageTitle: 'Scheme Upload'
        }
      })
      .when('/uploaduser', {
        templateUrl: 'partials/uploaduser',
        controller: 'UserReadXlsCtrl',
        label: 'User Upload',
        data: {
          pageTitle: 'User Upload'
        }
      })
      .when('/uploadequipment', {
        templateUrl: 'partials/uploadequipment',
        controller: 'EquipmentReadXlsCtrl',
        label: 'Equipment Upload',
        data: {
          pageTitle: 'Equipment Upload'
        }
      })
      .when('/facilityprofile', {
        templateUrl: 'partials/facilityprofileedit',
        controller: 'FacilityProfileEditCtrl',
        label: 'Facility Profile',
        data: {
          pageTitle: 'Facility Profile'
        }
      })
      .when('/nodashboard', {
        templateUrl: 'partials/nodashboard',
        controller: 'CODashboardCtrl',
        label: 'NO Dashboard',
        data: {
          pageTitle: 'NO Dashboard'
        }
      })
      .when('/rodashboard', {
        templateUrl: 'partials/rodashboard',
        controller: 'CODashboardCtrl',
        label: 'RO Dashboard',
        data: {
          pageTitle: 'RO Dashboard'
        }
      })
      .when('/spmdashboard', {
        templateUrl: 'partials/spmdashboard',
        controller: 'CODashboardCtrl',
        label: 'SPM Dashboard',
        data: {
          pageTitle: 'SPM Dashboard'
        }
      })
      .when('/fsmentordashboard', {
        templateUrl: 'partials/fsmentordashboard',
        controller: 'CODashboardCtrl',
        label: 'FS Mentor Dashboard',
        data: {
          pageTitle: 'FS Mentor Dashboard'
        }
      })
      //            .when('/mentor', {
      //                templateUrl: 'partials/mentor',
      //                controller: 'MentorCtrl',
      //
      //            })
      //            .when('/mentor/create', {
      //                templateUrl: 'partials/mentor-form',
      //                controller: 'MentorCreateCtrl',
      //
      //
      //            })
      //            .when('/otherroles/:id', {
      //                templateUrl: 'partials/mentor-form',
      //                controller: 'MentorCreateCtrl',
      //                label: 'Other Roles',
      //                data: {
      //                    pageTitle: 'Other Roles'
      //                }
      //            })

      .when('/mentorassign', {
        templateUrl: 'partials/mentorassign',
        controller: 'MentorAssignCtrl',

      })
      .when('/mentorassign/create', {
        templateUrl: 'partials/mentorassign',
        controller: 'MentorAssignCtrl'
      })
      .when('/schemedepartment', {
        templateUrl: 'partials/schemedepartments',
        controller: 'SchemeDeptCtrl'
      })
      .when('/schemedepartment/create', {
        templateUrl: 'partials/schemedepartments',
        controller: 'SchemeDeptCtrl'
      })
      .when('/schemedepartment/:id', {
        templateUrl: 'partials/schemedepartments',
        controller: 'SchemeDeptCtrl'
      })
      .when('/agegroup', {
        templateUrl: 'partials/agegroups',
        controller: 'AgeGroupCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/agegroup/create', {
        templateUrl: 'partials/agegroups',
        controller: 'AgeGroupCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/agegroup/:id', {
        templateUrl: 'partials/agegroups',
        controller: 'AgeGroupCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/healthstatus', {
        templateUrl: 'partials/healthstatuses',
        controller: 'HealthStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }

      })
      .when('/healthstatus/create', {
        templateUrl: 'partials/healthstatuses',
        controller: 'HealthStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/healthstatus/:id', {
        templateUrl: 'partials/healthstatuses',
        controller: 'HealthStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/minoritystatus', {
        templateUrl: 'partials/minoritystatuses',
        controller: 'MinorityStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }

      })
      .when('/minoritystatus/create', {
        templateUrl: 'partials/minoritystatuses',
        controller: 'MinorityStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/minoritystatus/:id', {
        templateUrl: 'partials/minoritystatuses',
        controller: 'MinorityStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/socialstatus', {
        templateUrl: 'partials/socialstatuses',
        controller: 'SocialStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/socialstatus/create', {
        templateUrl: 'partials/socialstatuses',
        controller: 'SocialStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/socialstatus/:id', {
        templateUrl: 'partials/socialstatuses',
        controller: 'SocialStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemetypology', {
        templateUrl: 'partials/occupationstatuses',
        controller: 'OccupationStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemetypology/create', {
        templateUrl: 'partials/occupationstatuses',
        controller: 'OccupationStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemetypology/:id', {
        templateUrl: 'partials/occupationstatuses',
        controller: 'OccupationStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/locationtype', {
        templateUrl: 'partials/locationtypes',
        controller: 'LocationTypeCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/locationtype/create', {
        templateUrl: 'partials/locationtypes',
        controller: 'LocationTypeCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/locationtype/:id', {
        templateUrl: 'partials/locationtypes',
        controller: 'LocationTypeCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }

      })
      .when('/incomestatus', {
        templateUrl: 'partials/incomestatuses',
        controller: 'IncomeStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/incomestatus/create', {
        templateUrl: 'partials/incomestatuses',
        controller: 'IncomeStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/incomestatus/:id', {
        templateUrl: 'partials/incomestatuses',
        controller: 'IncomeStatusCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemecategory', {
        templateUrl: 'partials/schemecategories',
        controller: 'SchemeCategCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemecategory/create', {
        templateUrl: 'partials/schemecategories',
        controller: 'SchemeCategCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/schemecategory/:id', {
        templateUrl: 'partials/schemecategories',
        controller: 'SchemeCategCtrl',
        label: 'Apply For Scheme',
        data: {
          pageTitle: 'Apply For Scheme'
        }
      })
      .when('/otherroles', {
        templateUrl: 'partials/mentor',
        controller: 'MentorCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/otherroles/create', {
        templateUrl: 'partials/mentor-form',
        controller: 'MentorCreateCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/otherroles/:id', {
        templateUrl: 'partials/mentor-form',
        controller: 'MentorCreateCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/facilityassign', {
        templateUrl: 'partials/mentorassign',
        controller: 'MentorAssignCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/facilityassign/create', {
        templateUrl: 'partials/mentorassign',
        controller: 'MentorAssignCtrl',
        label: 'Field Workers',
        data: {
          pageTitle: 'Field Workers'
        }
      })
      .when('/upload-co-organisation', {
        templateUrl: 'partials/upload-co-org',
        controller: 'CoOrgReadXlsCtrl',

      })
      .when('/followupreportincident', {
        templateUrl: 'partials/followupreportincident',
        controller: 'followupreportincidentCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupreportincident/create', {
        templateUrl: 'partials/followupreportincident',
        controller: 'followupreportincidentCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/followupreportincident/:id', {
        templateUrl: 'partials/followupreportincident',
        controller: 'followupreportincidentCtrl',
        label: 'OtherToDos',
        data: {
          pageTitle: 'OtherToDos'
        }
      })
      .when('/documenttypes', {
        templateUrl: 'partials/documenttype',
        controller: 'documenttypeCtrl'
      })
      .when('/documenttypes/create', {
        templateUrl: 'partials/documenttype',
        controller: 'documenttypeCtrl'
      })
      .when('/documenttypes/:id', {
        templateUrl: 'partials/documenttype',
        controller: 'documenttypeCtrl'
      })
      .when('/reasonforrejection', {
        templateUrl: 'partials/reasonforrejection',
        controller: 'reasonforrejectionCtrl',
        abel: 'Reason For Rejection',
        data: {
          pageTitle: 'Reason For Rejection'
        }
      })
      .when('/reasonforrejection/:id', {
        templateUrl: 'partials/reasonforrejection',
        controller: 'reasonforrejectionCtrl',
        label: 'Reason For Rejection',
        data: {
          pageTitle: 'Reason For Rejection'
        }
      })
      .when('/reasonforrejection/create', {
        templateUrl: 'partials/reasonforrejection',
        controller: 'reasonforrejectionCtrl',
        label: 'Reason For Rejection',
        data: {
          pageTitle: 'Reason For Rejection'
        }
      })
      .when('/reasonfordelay', {
        templateUrl: 'partials/reasonfordelay',
        controller: 'reasonfordelayCtrl',
        abel: 'Reason For Delay',
        data: {
          pageTitle: 'Reason For Delay'
        }
      })
      .when('/reasonfordelay/:id', {
        templateUrl: 'partials/reasonfordelay',
        controller: 'reasonfordelayCtrl',
        label: 'Reason For Delay',
        data: {
          pageTitle: 'Reason For Delay'
        }
      })
      .when('/reasonfordelay/create', {
        templateUrl: 'partials/reasonfordelay',
        controller: 'reasonfordelayCtrl',
        label: 'Reason For Delay',
        data: {
          pageTitle: 'Reason For Delay'
        }
      })

      .when('/servityofincident', {
        templateUrl: 'partials/servityofincident',
        controller: 'servityofincidentCtrl',
        abel: 'Report Incident',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/servityofincident/:id', {
        templateUrl: 'partials/servityofincident',
        controller: 'servityofincidentCtrl',
        label: 'Report Incident ',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/servityofincident/create', {
        templateUrl: 'partials/servityofincident',
        controller: 'servityofincidentCtrl',
        label: 'Report Incident',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/currentstatus', {
        templateUrl: 'partials/currentstatusofcase',
        controller: 'currentstatusofcaseCtrl',
        abel: 'Report Incident',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/currentstatus/:id', {
        templateUrl: 'partials/currentstatusofcase',
        controller: 'currentstatusofcaseCtrl',
        label: 'Report Incident',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/currentstatus/create', {
        templateUrl: 'partials/currentstatusofcase',
        controller: 'currentstatusofcaseCtrl',
        label: 'Report Incident',
        data: {
          pageTitle: 'Report Incident'
        }
      })
      .when('/organiseby', {
        templateUrl: 'partials/organisebystakeholder',
        controller: 'organisebystakeholderCtrl',
        abel: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('/organiseby/:id', {
        templateUrl: 'partials/organisebystakeholder',
        controller: 'organisebystakeholderCtrl',
        label: 'Stakeholder Meetings ',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('organiseby/create', {
        templateUrl: 'partials/organisebystakeholder',
        controller: 'organisebystakeholderCtrl',
        label: 'Stakeholder Meetings',
        data: {
          pageTitle: 'Stakeholder Meetings'
        }
      })
      .when('/responsereceive', {
        templateUrl: 'partials/responsereceive',
        controller: 'responsereceiveCtrl',
        label: 'Response Receive',
        data: {
          pageTitle: 'Response Receive'
        }
      })
      .when('/responsereceive/:id', {
        templateUrl: 'partials/responsereceive',
        controller: 'responsereceiveCtrl',
        label: 'Response Receive',
        data: {
          pageTitle: 'Response Receive'
        }
      })
      .when('/responsereceive/create', {
        templateUrl: 'partials/responsereceive',
        controller: 'responsereceiveCtrl',
        label: 'Response Receive',
        data: {
          pageTitle: 'Response Receive'
        }
      })
      .when('/dynamicfields', {
        templateUrl: 'partials/dynamicfields',
        controller: 'DynamicFieldsCtrl',
        label: 'Dynamic Fields',
        data: {
          pageTitle: 'Dynamic Fields'
        }
      })
      .when('/onetoonequestion', {
        templateUrl: 'partials/surveyquestion-mlanguage-View',
        controller: 'SurveyQuestionsCtrl',
        label: 'One to One Question',
        data: {
          pageTitle: 'One to One Question'
        }

      })
      .when('/onetoonequestion/create', {
        templateUrl: 'partials/surveyquestion-mlanguage',
        controller: 'SurveyQuestionsCtrl',
        label: 'One to One Question Create',
        data: {
          pageTitle: 'One to One Question'
        }
      })
      .when('/onetoonequestion/:id', {
        templateUrl: 'partials/surveyquestion-mlanguage',
        controller: 'SurveyQuestionsCtrl',
        label: 'One to One Question',
        data: {
          pageTitle: 'One to One Question'
        }
      })
      /******************************
        
            .when('/consentform', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/create', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/:id', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
            *****************************/
      .when('/syncoutdata/:id', {
        templateUrl: 'partials/syncoutdata2',
        controller: 'SyncOutDataCtrl',
        label: 'Last Pushed Data',
        data: {
          pageTitle: 'Last Pushed Data'
        }
      })
      .when('/phonetype', {
        templateUrl: 'partials/phonetypes',
        controller: 'PhoneCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/phonetype/create', {
        templateUrl: 'partials/phonetypes',
        controller: 'PhoneCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/phonetype/:id', {
        templateUrl: 'partials/phonetypes',
        controller: 'PhoneCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/noofterm', {
        templateUrl: 'partials/noofterms',
        controller: 'TermCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/noofterm/create', {
        templateUrl: 'partials/noofterms',
        controller: 'TermCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/noofterm/:id', {
        templateUrl: 'partials/noofterms',
        controller: 'TermCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      /*********/
      .when('/archivemember', {
        templateUrl: 'partials/archivemember',
        controller: 'ArchiveCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/archivemember/create', {
        templateUrl: 'partials/archivemember',
        controller: 'ArchiveCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/archivemember/:id', {
        templateUrl: 'partials/archivemember',
        controller: 'ArchiveCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      /************/
      .when('/financialPlanning', {
        templateUrl: 'partials/financialPlaning',
        controller: 'fPlanCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialPlanning/create', {
        templateUrl: 'partials/financialPlaning',
        controller: 'fPlanCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      .when('/financialPlanning:/id', {
        templateUrl: 'partials/financialPlaning',
        controller: 'fPlanCtrl',
        label: 'Financial Planning',
        data: {
          pageTitle: 'Financial Planning'
        }
      })
      /*.when('/cobudgets', {
          templateUrl: 'partials/cobudgetsView',
          controller: 'cobudgetsViewCtrl',
          label: 'CO Budgets',
          data: {
              pageTitle: 'CO Budgets'
          }
      })
      .when('/cobudgets/create', {
          templateUrl: 'partials/cobudgetsView',
          controller: 'cobudgetsViewCtrl',
          label: 'CO Budgets',
          data: {
              pageTitle: 'CO Budgets'
          }
      })
      .when('/cobudgets:/id', {
          templateUrl: 'partials/cobudgetsView',
          controller: 'cobudgetsViewCtrl',
          label: 'CO Budgets',
          data: {
              pageTitle: 'CO Budgets'
          }
      })*/
      .when('/spmbudgets', {
        templateUrl: 'partials/spmbudgetsView',
        controller: 'spmbudgetsViewCtrl',
        label: 'SPM Budgets',
        data: {
          pageTitle: 'SPM Budgets'
        }
      })
      .when('/spmbudgets/create', {
        templateUrl: 'partials/spmbudgets',
        controller: 'spmbudgetsCtrl',
        label: 'SPM Budgets',
        data: {
          pageTitle: 'SPM Budgets'
        }
      })
      .when('/spmbudgets/:id', {
        templateUrl: 'partials/spmbudgets',
        controller: 'spmbudgetsCtrl',
        label: 'SPM Budgets',
        data: {
          pageTitle: 'SPM Budgets'
        }
      })

      .when('/months', {
        templateUrl: 'partials/months',
        controller: 'MonthCtrl'
      })
      .when('/months/create', {
        templateUrl: 'partials/months',
        controller: 'MonthCtrl'
      })
      .when('/months/:id', {
        templateUrl: 'partials/months',
        controller: 'MonthCtrl'
      })
      .when('/cobudget', {
        templateUrl: 'partials/cobudgetsView1',
        controller: 'cobudgetsViewNew2Ctrl',
        label: 'CO Budget',
        data: {
          pageTitle: 'CO Budget'
        }
      })
      .when('/cobudgets/create', {
        templateUrl: 'partials/cobudgetsView1',
        controller: 'cobudgetsViewNew2Ctrl',
        label: 'CO Budgets',
        data: {
          pageTitle: 'CO Budgets'
        }
      })
      .when('/cobudgets:/id', {
        templateUrl: 'partials/cobudgetsView1',
        controller: 'cobudgetsViewNew2Ctrl',
        label: 'CO Budgets',
        data: {
          pageTitle: 'CO Budgets'
        }
      })

      .when('/nobudgets', {
        templateUrl: 'partials/nobudgets',
        controller: 'nobudgetsCtrl',
        label: 'NO Budgets',
        data: {
          pageTitle: 'NO Budgets'
        }
      })
      .when('/learning', {
        templateUrl: 'partials/learning',
        controller: 'learningCtrl',
        label: 'Learning',
        data: {
          pageTitle: 'Learning'
        }
      })

      .when('/learning/create', {
        templateUrl: 'partials/learning',
        controller: 'learningCtrl',
        label: 'Learning',
        data: {
          pageTitle: 'Learning'
        }
      })

      .when('/learning/:id', {
        templateUrl: 'partials/learning',
        controller: 'learningCtrl',
        label: 'Learning',
        data: {
          pageTitle: 'Learning'
        }
      })
      .when('/learningtype', {
        templateUrl: 'partials/learningtype',
        controller: 'learningtypeCtrl',
        label: 'Learning ',
        data: {
          pageTitle: 'Learning'
        }
      })
      .when('/learningtype/create', {
        templateUrl: 'partials/learningtype',
        controller: 'learningtypeCtrl',
        label: 'Learning',
        data: {
          pageTitle: 'Learning'
        }
      })
      .when('/learningtype/:id', {
        templateUrl: 'partials/learningtype',
        controller: 'learningtypeCtrl',
        label: 'Learning',
        data: {
          pageTitle: 'Learning'
        }
      }).when('/transfer', {
        templateUrl: 'partials/transfer',
        controller: 'TransferCtrl',
        label: 'Transfer',
        data: {
          pageTitle: 'Transfer'
        }
      })
      .when('/transfer_form/:id', {
        templateUrl: 'partials/transfer_form',
        controller: 'TransferFormCtrl',
        label: 'Transfer Member Completed',
        data: {
          pageTitle: 'Transfer Member Completed'
        }
      })
      .when('/transfer_member_data/:id', {
        templateUrl: 'partials/transfer_member_data',
        controller: 'TrasnferMemberDataCtrl',
        label: 'Transferred Member Data',
        data: {
          pageTitle: 'Transferred Member Data'
        }
      })


      .when('/levels-list', {
        templateUrl: 'partials/levels',
        controller: 'LevelsCtrl',
        label: 'Levels List',
        data: {
          pageTitle: 'Levels List'
        }
      })
      .when('/level/create', {
        templateUrl: 'partials/levels',
        controller: 'LevelsCtrl',
        label: 'Levels Create',
        data: {
          pageTitle: 'Level Create'
        }
      })

      .when('/level/edit/:id', {
        templateUrl: 'partials/levels',
        controller: 'LevelsCtrl',
        label: 'Levels Edit',
        data: {
          pageTitle: 'Level Edit'
        }
      })

      .when('/languages-list', {
        templateUrl: 'partials/languages',
        controller: 'LanguagesCtrl',
        label: 'Languages List',
        data: {
          pageTitle: 'Languages List'
        }
      })
      .when('/language/create', {
        templateUrl: 'partials/languages',
        controller: 'LanguagesCtrl',
        label: 'Language Create',
        data: {
          pageTitle: 'Language Create'
        }
      })

      .when('/language/edit/:id', {
        templateUrl: 'partials/languages',
        controller: 'LanguagesCtrl',
        label: 'Language Edit',
        data: {
          pageTitle: 'Language Edit'
        }
      })

      .when('/define-languages-list', {
        templateUrl: 'partials/define-languages',
        controller: 'DefineLanguagesCtrl',
        label: 'Define Languages List',
        data: {
          pageTitle: 'Define Languages List'
        }
      })
      .when('/define-language/create', {
        templateUrl: 'partials/define-languages',
        controller: 'DefineLanguagesCtrl',
        label: 'Define Language Create',
        data: {
          pageTitle: 'Define Language Create'
        }
      })

      .when('/define-language/edit/:id', {
        templateUrl: 'partials/define-languages',
        controller: 'DefineLanguagesCtrl',
        label: 'Define Language Edit',
        data: {
          pageTitle: 'Define Language Edit'
        }
      })

      .when('/define-levels-list', {
        templateUrl: 'partials/define-levels',
        controller: 'DefineLevelsCtrl',
        label: 'Define Levels List',
        data: {
          pageTitle: 'Define Levels List'
        }
      })
      .when('/define-level/create', {
        templateUrl: 'partials/define-levels',
        controller: 'DefineLevelsCtrl',
        label: 'Define Level Create',
        data: {
          pageTitle: 'Define Level Create'
        }
      })

      .when('/define-level/edit/:id', {
        templateUrl: 'partials/define-levels',
        controller: 'DefineLevelsCtrl',
        label: 'Define Level Edit',
        data: {
          pageTitle: 'Define Level Edit'
        }
      })


      .when('/level1-list', {
        templateUrl: 'partials/level1',
        controller: 'Level1Ctrl',
        label: 'Level1 List',
        data: {
          pageTitle: 'Level1 List'
        }
      })
      .when('/level1/create', {
        templateUrl: 'partials/level1',
        controller: 'Level1Ctrl',
        label: 'Level1 Create',
        data: {
          pageTitle: 'Level1 Create'
        }
      })

      .when('/level1/edit/:id', {
        templateUrl: 'partials/level1',
        controller: 'Level1Ctrl',
        label: 'Level1 Edit',
        data: {
          pageTitle: 'Level1 Edit'
        }
      })


      .when('/level2-list', {
        templateUrl: 'partials/level2',
        controller: 'Level2Ctrl',
        label: 'Level2 List',
        data: {
          pageTitle: 'Level2 List'
        }
      })
      .when('/level2/create', {
        templateUrl: 'partials/level2',
        controller: 'Level2Ctrl',
        label: 'Level2  Create',
        data: {
          pageTitle: 'Level2 Create'
        }
      })

      .when('/level2/edit/:id', {
        templateUrl: 'partials/level2',
        controller: 'Level2Ctrl',
        label: 'Level2 Edit',
        data: {
          pageTitle: 'Level2 Edit'
        }
      })

      .when('/level3-list', {
        templateUrl: 'partials/level3',
        controller: 'Level3Ctrl',
        label: 'Level3 List',
        data: {
          pageTitle: 'Level3 List'
        }
      })
      .when('/level3/create', {
        templateUrl: 'partials/level3',
        controller: 'Level3Ctrl',
        label: 'Level3 Create',
        data: {
          pageTitle: 'Level3 Create'
        }
      })

      .when('/level3/edit/:id', {
        templateUrl: 'partials/level3',
        controller: 'Level3Ctrl',
        label: 'Level3 Edit',
        data: {
          pageTitle: 'Level3 Edit'
        }
      })

      .when('/level4-list', {
        templateUrl: 'partials/level4',
        controller: 'Level4Ctrl',
        label: 'Level4 List',
        data: {
          pageTitle: 'Level4 List'
        }
      })
      .when('/level4/create', {
        templateUrl: 'partials/level4',
        controller: 'Level4Ctrl',
        label: 'Level4 Create',
        data: {
          pageTitle: 'Level4 Create'
        }
      })

      .when('/level4/edit/:id', {
        templateUrl: 'partials/level4',
        controller: 'Level4Ctrl',
        label: 'Level4 Edit',
        data: {
          pageTitle: 'Level4 Edit'
        }
      })

      .when('/level5-list', {
        templateUrl: 'partials/level5',
        controller: 'Level5Ctrl',
        label: 'Level5 List',
        data: {
          pageTitle: 'Level5 List'
        }
      })
      .when('/level5/create', {
        templateUrl: 'partials/level5',
        controller: 'Level5Ctrl',
        label: 'Level5 Create',
        data: {
          pageTitle: 'Level5 Create'
        }
      })

      .when('/level5/edit/:id', {
        templateUrl: 'partials/level5',
        controller: 'Level5Ctrl',
        label: 'Level5 Edit',
        data: {
          pageTitle: 'Level5 Edit'
        }
      })

      .when('/workflows-list', {
        templateUrl: 'partials/workflow',
        controller: 'WorkFlowCtrl',
        label: 'Work Flow List',
        data: {
          pageTitle: 'Work Flow List'
        }
      })
      .when('/workflow/create', {
        templateUrl: 'partials/workflow',
        controller: 'WorkFlowCtrl',
        label: 'Work Flow Create',
        data: {
          pageTitle: 'Work Flow Create'
        }
      })
      .when('/workflow/edit/:id', {
        templateUrl: 'partials/workflow',
        controller: 'WorkFlowCtrl',
        label: 'Work Flow Edit',
        data: {
          pageTitle: 'Work Flow Edit'
        }
      })

      .when('/workflow-status-list', {
        templateUrl: 'partials/workflow-status',
        controller: 'WorkFlowStatusCtrl',
        label: 'Workflow Status List',
        data: {
          pageTitle: 'Workflow Status List'
        }
      })
      .when('/workflow-status/create', {
        templateUrl: 'partials/workflow-status',
        controller: 'WorkFlowStatusCtrl',
        label: 'Workflow Status Create',
        data: {
          pageTitle: 'Workflow Status Create'
        }
      })
      .when('/workflow-status/edit/:id', {
        templateUrl: 'partials/workflow-status',
        controller: 'WorkFlowStatusCtrl',
        label: 'Workflow Status Edit',
        data: {
          pageTitle: 'Workflow Status Edit'
        }
      })


      .when('/workflow-category-list', {
        templateUrl: 'partials/workflow-category',
        controller: 'WorkFlowCategoryCtrl',
        label: 'Workflow Category List',
        data: {
          pageTitle: 'Workflow Category List'
        }
      })
      .when('/workflow-category/create', {
        templateUrl: 'partials/workflow-category',
        controller: 'WorkFlowCategoryCtrl',
        label: 'Workflow Category Create',
        data: {
          pageTitle: 'Workflow Category Create'
        }
      })
      .when('/workflow-category/edit/:id', {
        templateUrl: 'partials/workflow-category',
        controller: 'WorkFlowCategoryCtrl',
        label: 'Workflow Category Edit',
        data: {
          pageTitle: 'Workflow Category Edit'
        }
      })


      .when('/workflow-priority-list', {
        templateUrl: 'partials/workflow-priority',
        controller: 'WorkFlowPriorityCtrl',
        label: 'Workflow Priority List',
        data: {
          pageTitle: 'Workflow Priority List'
        }
      })
      .when('/workflow-priority/create', {
        templateUrl: 'partials/workflow-priority',
        controller: 'WorkFlowPriorityCtrl',
        label: 'Workflow Priority Create',
        data: {
          pageTitle: 'Workflow Priority Create'
        }
      })
      .when('/workflow-priority/edit/:id', {
        templateUrl: 'partials/workflow-priority',
        controller: 'WorkFlowPriorityCtrl',
        label: 'Workflow Priority Edit',
        data: {
          pageTitle: 'Workflow Priority Edit'
        }
      })

      .when('/todo-list', {
        templateUrl: 'partials/todotype',
        controller: 'ToDoTypesCtrl',
        label: 'ToDos List',
        data: {
          pageTitle: 'ToDos List'
        }
      })
      .when('/todo/create', {
        templateUrl: 'partials/todotype',
        controller: 'ToDoTypesCtrl',
        label: 'ToDos Create',
        data: {
          pageTitle: 'ToDos Create'
        }
      })
      .when('/todo/edit/:id', {
        templateUrl: 'partials/todotype',
        controller: 'ToDoTypesCtrl',
        label: 'ToDos Edit',
        data: {
          pageTitle: 'ToDos Edit'
        }
      })
      .when('/todo-status-list', {
        templateUrl: 'partials/todostatuses',
        controller: 'ToDoStatusesCtrl',
        label: 'ToDos Status',
        data: {
          pageTitle: 'ToDos Status'
        }
      })
      .when('/todo-status/create', {
        templateUrl: 'partials/todostatuses',
        controller: 'ToDoStatusesCtrl',
        label: 'ToDos Create',
        data: {
          pageTitle: 'ToDos Create'
        }
      })
      .when('/todo-status/edit/:id', {
        templateUrl: 'partials/todostatuses',
        controller: 'ToDoStatusesCtrl',
        label: 'ToDos Edit',
        data: {
          pageTitle: 'ToDos Edit'
        }
      })

      .when('/fieldexecutiveslist-view', {
        templateUrl: 'partials/fieldexecutiveslist-view',
        controller: 'FieldexListCtrl',
        label: 'FieldexList',
        data: {
          pageTitle: 'FieldexList'
        }
      })

      .when('/customers-list', {
        templateUrl: 'partials/customerslist-view',
        controller: 'CustomersListViewCtrl',
        label: 'Customers List',
        data: {
          pageTitle: 'Customers List'
        }
      })
      .when('/customers/create', {
        templateUrl: 'partials/customers',
        controller: 'CustomersCtrl',
        label: 'Customers Create',
        data: {
          pageTitle: 'Customers Create'
        }
      })
      .when('/customers/edit/:id', {
        templateUrl: 'partials/customers',
        controller: 'CustomersCtrl',
        label: 'Customers Edit',
        data: {
          pageTitle: 'Customers Edit'
        }
      })

      .when('/groupcreate', {
        templateUrl: 'partials/groupcreate-list',
        controller: 'GroupCreateListCtrl',
        label: 'Group List',
        data: {
          pageTitle: 'Group List'
        }
      })
      .when('/groupcreate/create', {
        templateUrl: 'partials/groupcreate',
        controller: 'GroupCreateCtrl',
        label: 'Group Create',
        data: {
          pageTitle: 'Group Create'
        }
      })
      .when('/groupcreate/edit/:id', {
        templateUrl: 'partials/groupcreate',
        controller: 'GroupCreateCtrl',
        label: 'Group Edit',
        data: {
          pageTitle: 'Group Edit'
        }
      })

      .when('/category-list', {
        templateUrl: 'partials/categorylist-view',
        controller: 'CategoryListViewCtrl',
        label: 'Category List',
        data: {
          pageTitle: 'Category List'
        }
      })
      .when('/category/create', {
        templateUrl: 'partials/category',
        controller: 'CategoryCtrl',
        label: 'Category Create',
        data: {
          pageTitle: 'Category Create'
        }
      })
      .when('/category/edit/:id', {
        templateUrl: 'partials/category',
        controller: 'CategoryCtrl',
        label: 'Category Edit',
        data: {
          pageTitle: 'Category Edit'
        }
      })

      .when('/subcategory-list', {
        templateUrl: 'partials/subcategorylist-view',
        controller: 'SubCategoryListViewCtrl',
        label: 'SubCategory List',
        data: {
          pageTitle: 'SubCategory List'
        }
      })
      .when('/subcategory/create', {
        templateUrl: 'partials/subcategory',
        controller: 'SubCategoryCtrl',
        label: 'SubCategory Create',
        data: {
          pageTitle: 'SubCategory Create'
        }
      })
      .when('/subcategory/edit/:id', {
        templateUrl: 'partials/subcategory',
        controller: 'SubCategoryCtrl',
        label: 'SubCategory Edit',
        data: {
          pageTitle: 'SubCategory Edit'
        }
      })

      .when('/servicequestion-type-list', {
        templateUrl: 'partials/onetoonetype',
        controller: 'OneToOneTypeCtrl',
        label: 'Service Question Types',
        data: {
          pageTitle: 'Service Question Types'
        }
      })
      .when('/servicequestion-type/create', {
        templateUrl: 'partials/onetoonetype',
        controller: 'OneToOneTypeCtrl',
        label: 'Service Question Type Create',
        data: {
          pageTitle: 'Service Question Type Create'
        }
      })
      .when('/servicequestion-type/edit/:id', {
        templateUrl: 'partials/onetoonetype',
        controller: 'OneToOneTypeCtrl',
        label: 'Service Question Type Edit',
        data: {
          pageTitle: 'Service Question Type Edit'
        }
      })

      .when('/servicequestion-list', {
        templateUrl: 'partials/onetoonelist-view',
        controller: 'OneToOneListViewCtrl',
        label: 'Service Question List',
        data: {
          pageTitle: 'Service Question List'
        }
      })
      .when('/servicequestion/create', {
        templateUrl: 'partials/onetoonelist',
        controller: 'OneToOneListCtrl',
        label: 'Service Question Create',
        data: {
          pageTitle: 'Service Question Create'
        }
      })
      .when('/servicequestion/edit/:id', {
        templateUrl: 'partials/onetoonelist',
        controller: 'OneToOneListCtrl',
        label: 'Service Question Edit',
        data: {
          pageTitle: 'Service Question Edit'
        }
      })
      .when('/servicedetail-backend', {
        templateUrl: 'partials/servicedetail-backend',
        controller: 'ServiceDetailBackCtrl',
        label: 'Service Detail Backend',
        data: {
          pageTitle: 'Service Detail Backend'
        }
      })

      .when('/servicedetail-frontend', {
        templateUrl: 'partials/servicedetail-frontend',
        controller: 'ServiceDetailFrontCtrl',
        label: 'Service Detail Frontend',
        data: {
          pageTitle: 'Service Detail Frontend'
        }
      })

      .when('/formlevel-frontend', {
        templateUrl: 'partials/formlevel-frontend',
        controller: 'FormLevelFrontEndCtrl',
        label: 'Form Level Frontend Detail',
        data: {
          pageTitle: 'Form Level Frontend Detail'
        }
      })

      .when('/formlevel-backend', {
        templateUrl: 'partials/formlevel-backend',
        controller: 'FormLevelBackendEndCtrl',
        label: 'Form Level Backend Detail',
        data: {
          pageTitle: 'Form Level Backend Detail'
        }
      })

      .when('/serviceorder', {
        templateUrl: 'partials/serviceorder',
        controller: 'ServiceOrderCtrl',
        label: 'Service Order',
        data: {
          pageTitle: 'Service Order'
        }
      })

      .when('/workflows', {
        templateUrl: 'partials/workflow-form',
        controller: 'WorkflowCreateCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })
      .when('/workflows/create', {
        templateUrl: 'partials/workflow-form',
        controller: 'WorkflowCreateCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })
      .when('/workflows/edit/:id', {
        templateUrl: 'partials/workflow-form',
        controller: 'WorkflowCreateCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })

      .when('/todos', {
        templateUrl: 'partials/todos-form',
        controller: 'TodosCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })
      .when('/todos/create', {
        templateUrl: 'partials/todos-form',
        controller: 'TodosCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })
      .when('/todos/edit/:id', {
        templateUrl: 'partials/todos-form',
        controller: 'TodosCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })


      .when('/workflowlanguage', {
        templateUrl: 'partials/workflowlanguage',
        controller: 'WorkflowLanguageCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })
      .when('/workflowlanguage/create', {
        templateUrl: 'partials/workflowlanguage',
        controller: 'WorkflowLanguageCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })
      .when('/workflowlanguage/edit/:id', {
        templateUrl: 'partials/workflowlanguage',
        controller: 'WorkflowLanguageCtrl',
        label: 'Work Flow',
        data: {
          pageTitle: 'Work Flow'
        }
      })

      .when('/todolanguage', {
        templateUrl: 'partials/todolanguage',
        controller: 'TodoLanguageCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })
      .when('/todolanguage/create', {
        templateUrl: 'partials/todolanguage',
        controller: 'TodoLanguageCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })
      .when('/todolanguage/edit/:id', {
        templateUrl: 'partials/todolanguage',
        controller: 'TodoLanguageCtrl',
        label: 'Todo',
        data: {
          pageTitle: 'Todo'
        }
      })


      .when('/onetoonelanguage', {
        templateUrl: 'partials/onetoonelnguage',
        controller: 'OnetoOneLanguageCtrl',
        label: 'One to One',
        data: {
          pageTitle: 'One to One'
        }
      })
      .when('/onetoonelanguage/create', {
        templateUrl: 'partials/onetoonelnguage',
        controller: 'OnetoOneLanguageCtrl',
        label: 'One to One',
        data: {
          pageTitle: 'One to One'
        }
      })
      .when('/onetoonelanguage/edit/:id', {
        templateUrl: 'partials/onetoonelnguage',
        controller: 'OnetoOneLanguageCtrl',
        label: 'One to One',
        data: {
          pageTitle: 'One to One'
        }
      })
      .when('/memberlanguage', {
        templateUrl: 'partials/memberlanguage',
        controller: 'MemberLanguageCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/memberlanguage/create', {
        templateUrl: 'partials/memberlanguage-form',
        controller: 'MemberLanguageCreateCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })
      .when('/memberlanguage/:id', {
        templateUrl: 'partials/memberlanguage-form',
        controller: 'MemberLanguageEditCtrl',
        label: 'Members',
        data: {
          pageTitle: 'Members'
        }
      })

      .when('/organisationlevel', {
        templateUrl: 'partials/organisationlevels',
        controller: 'OrganisationLevelCtrl',
        label: 'Level',
        data: {
          pageTitle: 'Level'
        }
      })
      .when('/organisationlevel/create', {
        templateUrl: 'partials/organisationlevels',
        controller: 'OrganisationLevelCtrl',
        label: 'Level',
        data: {
          pageTitle: 'Level'
        }
      })
      .when('/organisationlevel/edit/:id', {
        templateUrl: 'partials/organisationlevels',
        controller: 'OrganisationLevelCtrl',
        label: 'Level',
        data: {
          pageTitle: 'Level'
        }
      })
      // .when('/formlevel/create', {
      //   templateUrl: 'partials/formlevels',
      //   controller: 'FormLevelCtrl',
      //   label: 'Form Level Create',
      //   data: {
      //     pageTitle: 'Form Level Create'
      //   }
      // })
      // .when('/formlevel/edit/:id', {
      //   templateUrl: 'partials/formlevels',
      //   controller: 'FormLevelCtrl',
      //   label: 'Form Level Edit',
      //   data: {
      //     pageTitle: 'Form Level Edit'
      //   }
      // })
      .when('/formlevel/create', {
        templateUrl: 'partials/formlevelsnew',
        controller: 'FormLevelCtrl',
        label: 'Form Level Create',
        data: {
          pageTitle: 'Form Level Create'
        }
      })
      .when('/formlevel/edit/:id', {
        templateUrl: 'partials/formlevelsnew',
        controller: 'FormLevelCtrl',
        label: 'Form Level Edit',
        data: {
          pageTitle: 'Form Level Edit'
        }
      })
      .when('/formleveldetailslist', {
        templateUrl: 'partials/formdetailslist',
        controller: 'FormLevelDetailsCtrl',
        label: 'Form Level Details',
        data: {
          pageTitle: 'Form Level Details'
        }
      })
      .when('/formlevelsummary/create', {
        templateUrl: 'partials/formlevelsummary',
        controller: 'FormLevelSummaryCtrl',
        label: 'Form Level Summary Create',
        data: {
          pageTitle: 'Form Level Summary Create'
        }
      })
      .when('/formlevelsummary/edit/:id', {
        templateUrl: 'partials/formlevelsummary',
        controller: 'FormLevelSummaryCtrl',
        label: 'Form Level Summary Edit',
        data: {
          pageTitle: 'Form Level Summary Edit'
        }
      })
      .when('/multilanguagelist', {
        templateUrl: 'partials/multilanguagelist',
        controller: 'MultiLanguageListCtrl',
        label: 'Multi Language List',
        data: {
          pageTitle: 'Multi Language List'
        }
      })
      // .when('/multilanguage/edit/:id', {
      //   templateUrl: 'partials/multilanguage',
      //   controller: 'MultiLanguageCtrl',
      //   label: 'Multi Language Edit',
      //   data: {
      //     pageTitle: 'Multi Language Edit'
      //   }
      // })
      // .when('/multilanguage/create', {
      //   templateUrl: 'partials/multilanguage',
      //   controller: 'MultiLanguageCtrl',
      //   label: 'Multi Language Create',
      //   data: {
      //     pageTitle: 'Multi Language Create'
      //   }
      // })

      .when('/multilanguage/edit/:id', {
        templateUrl: 'partials/multilanguagenew',
        controller: 'MultiLanguageNewCtrl',
        label: 'Multi Language Edit',
        data: {
          pageTitle: 'Multi Language Edit'
        }
      })
      .when('/multilanguage/create', {
        templateUrl: 'partials/multilanguagenew',
        controller: 'MultiLanguageNewCtrl',
        label: 'Multi Language Create',
        data: {
          pageTitle: 'Multi Language Create'
        }
      })
      .when('/uploadxls', {
        templateUrl: 'partials/uploadxls',
        controller: 'FormLevelRCDetailsUploadxlsCtrl',
        label: 'Form Level RC Details Upload',
        data: {
          pageTitle: 'Form Level RC Details Upload'
        }
      })
      .when('/organisationlocation', {
        templateUrl: 'partials/organisationlocations',
        controller: 'OrganisationLocationCtrl',
        label: 'Location',
        data: {
          pageTitle: 'Location'
        }
      })
      .when('/organisationlocation/create', {
        templateUrl: 'partials/organisationlocations',
        controller: 'OrganisationLocationCtrl',
        label: 'Location',
        data: {
          pageTitle: 'Location'
        }
      })
      .when('/organisationlocation/edit/:id', {
        templateUrl: 'partials/organisationlocations',
        controller: 'OrganisationLocationCtrl',
        label: 'Location',
        data: {
          pageTitle: 'Location'
        }
      })


      .when('/membercustom', {
        templateUrl: 'partials/membercustom',
        controller: 'MemberCustomCtrl',
        label: 'Member Custom Fields',
        data: {
          pageTitle: 'Member Custom Fields'
        }
      })
      .when('/membercustom/create', {
        templateUrl: 'partials/membercustom',
        controller: 'MemberCustomCtrl',
        label: 'Member Custom Fields',
        data: {
          pageTitle: 'Member Custom Fields'
        }
      })
      .when('/membercustom/edit/:id', {
        templateUrl: 'partials/membercustom',
        controller: 'MemberCustomCtrl',
        label: 'Member Custom Fields',
        data: {
          pageTitle: 'Member Custom Fields'
        }
      })
      .when('/configcustomreport', {
        templateUrl: 'partials/configcustomreport',
        controller: 'ConfigCustomReportsCtrl',
        label: 'Config Custom Report',
        data: {
          pageTitle: 'Config Custom Report'
        }
      })
      .when('/configcustomreport/create', {
        templateUrl: 'partials/configcustomreport',
        controller: 'ConfigCustomReportsCtrl',
        label: 'Config Custom Report',
        data: {
          pageTitle: 'Config Custom Report'
        }
      })
      .when('/configcustomreport/:id', {
        templateUrl: 'partials/configcustomreport',
        controller: 'ConfigCustomReportsCtrl',
        label: 'Config Custom Report',
        data: {
          pageTitle: 'Config Custom Report'
        }
      })
      .when('/assigntodo', {
        templateUrl: 'partials/assigntodo',
        controller: 'AssignTodoCtrl',
        label: 'Assign Task',
        data: {
          pageTitle: 'Assign Task'
        }
      })
      .when('/assigntickets-list', {
        templateUrl: 'partials/assignmemberslist-view',
        controller: 'AssignmembersListViewCtrl',
        label: 'Assigned Tickets List',
        data: {
          pageTitle: 'Assigned Tickets List'
        }
      })
      .when('/assignticket', {
        templateUrl: 'partials/assignmember',
        controller: 'AssignMemberCtrl',
        label: 'Assign Ticket',
        data: {
          pageTitle: 'Assign Ticket'
        }
      })

      .when('/approvetickets-list', {
        templateUrl: 'partials/approveticketslist-view',
        controller: 'ApproveticketsListViewCtrl',
        label: 'Approve Tickets List',
        data: {
          pageTitle: 'Approve Tickets List'
        }
      })
      .when('/approveticket/:id', {
        templateUrl: 'partials/approveticket',
        controller: 'ApproveticketCtrl',
        label: 'Approve Ticket',
        data: {
          pageTitle: 'Approve Ticket'
        }
      })

      .when('/dashboard', {
        templateUrl: 'partials/dashboard',
        controller: 'DashboardCtrl',
        label: 'Dashboard',
        data: {
          pageTitle: 'Dashboard'
        }
      })
      .when('/dashboardnew', {
        templateUrl: 'partials/dashboardnew',
        controller: 'DashboardNewCtrl',
        label: 'DashboardNew',
        data: {
          pageTitle: 'DashboardNew'
        }
      })
      .when('/dashboardmap', {
        templateUrl: 'partials/dashboardmap',
        controller: 'DashboardMapCtrl',
        label: 'Dashboard Map',
        data: {
          pageTitle: 'Dashboard Map'
        }
      })
      .when('/both', {
        templateUrl: 'partials/both',
        controller: 'BothCtrl',
        label: 'Both',
        data: {
          pageTitle: 'Both'
        }
      })

      .when('/roll-back', {
        templateUrl: 'partials/roll-back',
        controller: 'RollbackCtrl',
        label: 'Roll-Back',
        data: {
          pageTitle: 'Roll-Back'
        }
      })
      .when('/rollback-view/:id', {
        templateUrl: 'partials/rollback-view',
        controller: 'Rollback-viewCtrl',
        label: 'Roll Back View',
        data: {
          pageTitle: 'Roll Back View'
        }
      })
      .when('/export-details', {
        templateUrl: 'partials/export-details',
        controller: 'ExportDetailsCtrl',
        label: 'Export Details',
        data: {
          pageTitle: 'Export Details'
        }
      })
      .when('/export-details/:id', {
        templateUrl: 'partials/export-details',
        controller: 'ExportDetailsCtrl',
        label: 'Export Details View',
        data: {
          pageTitle: 'Export Details View'
        }
      })
      .when('/inventories', {
        templateUrl: 'partials/inventories',
        controller: 'InventoriesCtrl',
        label: 'Inventories',
        data: {
          pageTitle: 'Inventories'
        }
      })
      .when('/inventorycreate', {
        templateUrl: 'partials/inventorycreate',
        controller: 'InventoryCreateCtrl',
        label: 'Inventories',
        data: {
          pageTitle: 'Inventories'
        }
      })
      .when('/inventories/:id', {
        templateUrl: 'partials/inventorycreate',
        controller: 'InventoryCreateCtrl',
        label: 'Inventories',
        data: {
          pageTitle: 'Inventories'
        }
      })
      .when('/bom', {
        templateUrl: 'partials/bom',
        controller: 'BomCtrl',
        label: 'BOM',
        data: {
          pageTitle: 'BOM'
        }
      })
      .when('/purchaseorder', {
        templateUrl: 'partials/purchaseorder',
        controller: 'PurchaseorderCtrl',
        label: 'Purchaseorder',
        data: {
          pageTitle: 'Purchaseorder'
        }
      })
      .when('/purchasecreate', {
        templateUrl: 'partials/purchasecreate',
        controller: 'PurchaseCreateCtrl',
        label: 'Purchasecreate',
        data: {
          pageTitle: 'Purchasecreate'
        }
      })
      .when('/purchaseorder/:id', {
        templateUrl: 'partials/purchasecreate',
        controller: 'PurchaseCreateCtrl',
        label: 'Purchasecreate',
        data: {
          pageTitle: 'Purchasecreate'
        }
      })
      .when('/otherorder', {
        templateUrl: 'partials/otherorder',
        controller: 'OtherorderCtrl',
        label: 'Otherorder',
        data: {
          pageTitle: 'Otherorder'
        }
      })
      .when('/otherorder/:id', {
        templateUrl: 'partials/otherorder',
        controller: 'OtherorderCtrl',
        label: 'Otherorder',
        data: {
          pageTitle: 'Otherorder'
        }
      })
      .when('/goodreceipt', {
        templateUrl: 'partials/goodreceipt',
        controller: 'GoodreceiptCtrl',
        label: 'Good Receipt',
        data: {
          pageTitle: 'Good Receipt'
        }
      })
      .when('/goodscreate', {
        templateUrl: 'partials/goodscreate',
        controller: 'GoodscreateCtrl',
        label: 'Good Receipt create',
        data: {
          pageTitle: 'Good Receipt create'
        }
      })
      .when('/goodreceipt/:id', {
        templateUrl: 'partials/goodscreate',
        controller: 'GoodscreateCtrl',
        label: 'Good Receipt View',
        data: {
          pageTitle: 'Good Receipt View'
        }
      })
      .when('/stock', {
        templateUrl: 'partials/stock',
        controller: 'StockCtrl',
        label: 'Stock',
        data: {
          pageTitle: 'Stock'
        }
      })
      .when('/stockcreate', {
        templateUrl: 'partials/stockcreate',
        controller: 'StockcreateCtrl',
        label: 'Stock Create',
        data: {
          pageTitle: 'Stock Create'
        }
      })
      .when('/stock/:id', {
        templateUrl: 'partials/stock',
        controller: 'StockCtrl',
        label: 'Stock View',
        data: {
          pageTitle: 'Stock View'
        }
      })
      .when('/itemtype', {
        templateUrl: 'partials/itemtype',
        controller: 'ItemtypeCreateCtrl',
        label: 'Item Type',
        data: {
          pageTitle: 'Item Type'
        }
      })
      .when('/itemtypecreate', {
        templateUrl: 'partials/itemtypecreate',
        controller: 'ItemtypeCreateCtrl',
        label: 'Item Type Create',
        data: {
          pageTitle: 'Item Type Create'
        }
      })
      .when('/itemtype/edit/:id', {
        templateUrl: 'partials/itemtypecreate',
        controller: 'ItemtypeCreateCtrl',
        label: 'Itemtype Edit',
        data: {
          pageTitle: 'Itemtype Edit'
        }
      })
      .when('/itemcategory', {
        templateUrl: 'partials/itemcategory',
        controller: 'ItemcategoryCreate',
        label: 'Item Category',
        data: {
          pageTitle: 'Item Category'
        }
      })
      .when('/itemcategorycreate', {
        templateUrl: 'partials/itemcategorycreate',
        controller: 'ItemcategoryCreate',
        label: 'Item Category Create',
        data: {
          pageTitle: 'Item Category Create'
        }
      })
      .when('/itemcategory/edit/:id', {
        templateUrl: 'partials/itemcategorycreate',
        controller: 'ItemcategoryCreate',
        label: 'Itemcategory Edit',
        data: {
          pageTitle: 'Itemcategory Edit'
        }
      })
      .when('/itemdefinition', {
        templateUrl: 'partials/itemdefinition',
        controller: 'ItemDefinitionCtrl',
        label: 'Item Definition',
        data: {
          pageTitle: 'ItemDefinition'
        }
      })
      .when('/itemdefinitioncreate', {
        templateUrl: 'partials/itemdefinitioncreate',
        controller: 'ItemDefinitionCreateCtrl',
        label: 'Item Definition Create',
        data: {
          pageTitle: 'ItemDefinition Create'
        }
      })
      .when('/itemdefinition/edit/:id', {
        templateUrl: 'partials/itemdefinitioncreate',
        controller: 'ItemDefinitionCreateCtrl',
        label: 'Item Definition Edit',
        data: {
          pageTitle: 'ItemDefinition Edit'
        }
      })
      .when('/itemstatus', {
        templateUrl: 'partials/itemstatus',
        controller: 'ItemstatusCtrl',
        label: 'Item Status',
        data: {
          pageTitle: 'Item Status'
        }
      })
      .when('/itemstatuscreate', {
        templateUrl: 'partials/itemstatuscreate',
        controller: 'ItemstatusCreateCtrl',
        label: 'Item Status Create',
        data: {
          pageTitle: 'Item Status Create'
        }
      })
      .when('/itemstatus/edit/:id', {
        templateUrl: 'partials/itemstatuscreate',
        controller: 'ItemstatusCreateCtrl',
        label: 'Item Status Edit',
        data: {
          pageTitle: 'Item Status Edit'
        }
      })
      .when('/warehousetype', {
        templateUrl: 'partials/warehousetype',
        controller: 'WarehousetypeCtrl',
        label: 'Warehouse Type',
        data: {
          pageTitle: 'Warehouse Type'
        }
      })
      .when('/warehousetypecreate', {
        templateUrl: 'partials/warehousetypecreate',
        controller: 'WarehousetypeCreateCtrl',
        label: 'Warehouse Type Create',
        data: {
          pageTitle: 'Warehouse Type Create'
        }
      })
      .when('/warehousetype/edit/:id', {
        templateUrl: 'partials/warehousetypecreate',
        controller: 'WarehousetypeCreateCtrl',
        label: 'Warehouse Type Edit',
        data: {
          pageTitle: 'Warehouse Type Edit'
        }
      })
      .when('/warehouse', {
        templateUrl: 'partials/warehouse',
        controller: 'WarehouseCtrl',
        label: 'Warehouse ',
        data: {
          pageTitle: 'Warehouse'
        }
      })
      .when('/warehousecreate/edit/:id', {
        templateUrl: 'partials/warehousecreate',
        controller: 'WarehouseCreateCtrl',
        label: 'Warehouse Edit',
        data: {
          pageTitle: 'Warehouse'
        }
      })
      .when('/warehousecreate', {
        templateUrl: 'partials/warehousecreate',
        controller: 'WarehouseCreateCtrl',
        label: 'Warehouse Create',
        data: {
          pageTitle: 'Warehouse Create'
        }
      })
      .when('/inventoryuser', {
        templateUrl: 'partials/inventoryuser',
        controller: 'InventoryuserCtrl',
        label: 'Inventory',
        data: {
          pageTitle: 'Inventory'
        }
      })
      .when('/inventoryusercreate', {
        templateUrl: 'partials/inventoryusercreate',
        controller: 'InventoryuserCreateCtrl',
        label: 'Inventory Create',
        data: {
          pageTitle: 'Inventory Create'
        }
      })
      .when('/inventoryuser/:id', {
        templateUrl: 'partials/inventoryusercreate',
        controller: 'InventoryuserCreateCtrl',
        label: 'Inventory Create',
        data: {
          pageTitle: 'Inventory Edit'
        }
      })
      .when('/return', {
        templateUrl: 'partials/return',
        controller: 'ReturnCtrl',
        label: 'Return',
        data: {
          pageTitle: 'Return'
        }
      })
      .when('/fieldrequest', {
        templateUrl: 'partials/fieldrequest',
        controller: 'FieldrequestCtrl',
        label: 'Field Request',
        data: {
          pageTitle: 'Field Request'
        }
      })
      .when('/fieldrequest/:id', {
        templateUrl: 'partials/fieldrequest',
        controller: 'FieldrequestCtrl',
        label: 'Field Request View',
        data: {
          pageTitle: 'Field Request View'
        }
      })
      .when('/fieldrequestcreate', {
        templateUrl: 'partials/fieldrequestcreate',
        controller: 'FieldrequestCreateCtrl',
        label: 'Field Request Create',
        data: {
          pageTitle: 'Field Request Create'
        }
      })
      .when('/recipt', {
        templateUrl: 'partials/recipt',
        controller: 'ReciptCtrl',
        label: 'Recipt',
        data: {
          pageTitle: 'Recipt'
        }
      })
      .when('/recipt/:id', {
        templateUrl: 'partials/recipt',
        controller: 'ReciptCtrl',
        label: 'Recipt View',
        data: {
          pageTitle: 'Recipt View'
        }
      })
      .when('/reciptcreate', {
        templateUrl: 'partials/reciptcreate',
        controller: 'ReciptCreateCtrl',
        label: 'Recipt Create',
        data: {
          pageTitle: 'Recipt Create'
        }
      })
      .when('/assignitem', {
        templateUrl: 'partials/assignitem',
        controller: 'AssignitemCtrl',
        label: 'Assign Item',
        data: {
          pageTitle: 'Assign Item'
        }
      })
      .when('/assignitemcreate', {
        templateUrl: 'partials/assignitemcreate',
        controller: 'AssignitemCreateCtrl',
        label: 'Assign Item Create',
        data: {
          pageTitle: 'Assign Item Create'
        }
      })
      .when('/uploaditem', {
        templateUrl: 'partials/uploaditem',
        controller: 'UploaditemCtrl',
        label: 'Upload Item',
        data: {
          pageTitle: 'Upload Item'
        }
      })
      .when('/timeslot', {
        templateUrl: 'partials/timeslot',
        controller: 'TimeslotCtrl',
        label: 'Timeslot',
        data: {
          pageTitle: 'Time Slot'
        }
      })
      .when('/timeslotcreate', {
        templateUrl: 'partials/timeslotcreate',
        controller: 'TimeslotCreateCtrl',
        label: 'Timeslot Create',
        data: {
          pageTitle: 'Time Slot Create'
        }
      })
      .when('/timeslot/:id', {
        templateUrl: 'partials/timeslotcreate',
        controller: 'TimeslotCreateCtrl',
        label: 'Timeslot Edit',
        data: {
          pageTitle: 'Time Slot Edit'
        }
      })
      .when('/daily', {
        templateUrl: 'partials/daily',
        controller: 'DailyCtrl',
        label: 'Daily',
        data: {
          pageTitle: 'Daily'
        }
      })
      .when('/hourly', {
        templateUrl: 'partials/hourly',
        controller: 'HourlyCtrl',
        label: 'Hourly',
        data: {
          pageTitle: 'Hourly'
        }
      })
      .when('/task', {
        templateUrl: 'partials/export-details',
        controller: 'ExportDetailsCtrl',
        label: 'Task',
        data: {
          pageTitle: 'Task'
        }
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
    RestangularProvider.setBaseUrl('//fsm-test-api.herokuapp.com/api/v1/');
    RestangularProvider.addElementTransformer('users', true, function (user) {
      // This will add a method called login that will do a POST to the path login
      // signature is (name, operation, path, params, headers, elementToPost)

      user.addRestangularMethod('login', 'post', 'login');

      return user;
    });
    $idleProvider.idleDuration(60 * 30);
    $idleProvider.warningDuration(5);
    $keepaliveProvider.interval(10);
  })

  .factory('AnalyticsRestangular', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl('//swastianalyticstest.herokuapp.com/api/v1/');
    });
  })
  .run(function ($rootScope, $window, $location, Restangular, $idle, $modal, AnalyticsRestangular) {
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
      if (current != undefined) {
        $window.sessionStorage.prviousLocation = current.loadedTemplateUrl;
        // console.log('$window.sessionStorage.prviousLocation when current != undefined',$window.sessionStorage.prviousLocation);
      } else {
        $window.sessionStorage.prviousLocation = null;
        //console.log('$window.sessionStorage.prviousLocation when else',$window.sessionStorage.prviousLocation);
      }
      console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
      console.log('$location.path()', $location.path());
      if (!$window.sessionStorage.userId && $location.path() !== '/login') {
        $location.path('/login');
      }
      if ($location.path() == '/login') {
        $rootScope.showMe = true;
      } else {
        $rootScope.showMe = false;
      }
      Restangular.setDefaultHeaders({
        'authorization': $window.sessionStorage.accessToken
      });

      AnalyticsRestangular.setDefaultHeaders({
        'authorization': $window.sessionStorage.analyticsAccessToken
      });
    });

    $rootScope.started = false;
    $rootScope.pageSize = 25;

    function closeModals() {
      if ($rootScope.warning) {
        $rootScope.warning.close();
        $rootScope.warning = null;
      }

      if ($rootScope.timedout) {
        $rootScope.timedout.close();
        $rootScope.timedout = null;
      }
    }

    function logout() {
      // alert('alert');
      //$location.path("/login");

      //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
      Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
        $window.sessionStorage.userId = '';
        console.log('Logout');
      }).then(function (redirect) {
        $window.location.href = '/login';
        $window.location.reload();
        $idle.unwatch();
      });
    }

    $rootScope.$on('$idleStart', function () {
      closeModals();

      $rootScope.warning = $modal.open({
        templateUrl: 'partials/warning-dialog.html',
        windowClass: 'modal-danger'
      });
    });

    $rootScope.$on('$idleEnd', function () {
      closeModals();
    });

    $rootScope.$on('$idleTimeout', function () {
      closeModals();
      logout();
      $rootScope.timedout = $modal.open({
        templateUrl: 'partials/timedout-dialog.html',
        windowClass: 'modal-danger'
      });
    });

    $rootScope.start = function () {
      closeModals();
      $idle.watch();
      $rootScope.started = true;
    };
    if (!$window.sessionStorage.userId == '') {
      $rootScope.start();
    }

    $rootScope.stop = function () {
      closeModals();
      $idle.unwatch();
      $rootScope.started = false;

    };
    $rootScope.style = {
      "font-size": 14
    }
  })

  .directive('numbersOnly', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function (inputValue) {
          // this next if is necessary for when using ng-required on your input. 
          // In such cases, when a letter is typed first, this parser will be called
          // again, and the 2nd time, the value will be undefined
          if (inputValue == undefined) return ''
          var transformedInput = inputValue.replace(/[^0-9]/g, '');
          if (transformedInput != inputValue) {
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
          }

          return transformedInput;
        });
      }
    };
  })

  .controller('crumbCtrl', ['$scope', '$location', '$route', 'breadcrumbs',
    function ($scope, $location, $route, breadcrumbs) {
      $scope.location = $location;
      $scope.breadcrumbs = breadcrumbs;

    }
  ])
