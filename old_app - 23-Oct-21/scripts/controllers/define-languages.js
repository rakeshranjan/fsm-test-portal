'use strict';

angular.module('secondarySalesApp')
    .controller('DefineLanguagesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/define-language/create' || $location.path() === '/define-language/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/define-language/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/define-language/create' || $location.path() === '/define-language/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/define-language/create' || $location.path() === '/define-language/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/define-languages") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        //        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lng) {
        //            $scope.languages = lng;
        //
        //            $scope.DisplayDefineLanguages = [{
        //                language: lng[0].name,
        //                languageid: lng[0].id,
        //                name: '',
        //                disableAdd: false,
        //                disableRemove: false,
        //                deleteflag: false,
        //                lastmodifiedby: $window.sessionStorage.userId,
        //                lastmodifiedrole: $window.sessionStorage.roleId,
        //                lastmodifiedtime: new Date(),
        //                createdby: $window.sessionStorage.userId,
        //                createdtime: new Date(),
        //                createdrole: $window.sessionStorage.roleId
        //            }];
        //        });


        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langdef) {
            $scope.langdefs = langdef;

            $scope.langdefArray = [];
            var count = 0;

            if ($scope.langdefs.length == 0) {
                $scope.getLanguages();
            }

            angular.forEach($scope.langdefs, function (value, index) {
                $scope.langdefArray.push(value.languageid);
                count++;

                if (count == $scope.langdefs.length) {
                    $scope.getLanguages();
                }
            });
        });


        $scope.getLanguages = function () {

            Restangular.all('languages?filter={"where":{"and":[{"id":{"nin":[' + $scope.langdefArray + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (lng) {
                $scope.languages = lng;
                // console.log($scope.levels);

                $scope.DisplayDefineLanguages = [{
                    language: lng[0].name,
                    languageid: lng[0].id,
                    name: '',
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId
            }];
            });
        };


        $scope.Add = function (index) {

            var idVal = index + 1;

            if (idVal > $scope.languages.length) {
                return;
            } else {
                $scope.DisplayDefineLanguages.push({
                    language: $scope.languages[idVal].name,
                    languageid: $scope.languages[idVal].id,
                    name: '',
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId
                });
            }
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayDefineLanguages.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayDefineLanguages[index].disableAdd = false;
                $scope.DisplayDefineLanguages[index].disableRemove = false;
            } else {
                $scope.DisplayDefineLanguages[index].disableAdd = true;
                $scope.DisplayDefineLanguages[index].disableRemove = true;
            }
        };

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('languagedefinitions/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /********************************************** WATCH ***************************************/
        
        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lngdsply) {
            $scope.languagesdsplay = lngdsply;
        });

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (lngdef) {
            $scope.languagedefinitions = lngdef;
            angular.forEach($scope.languagedefinitions, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLangs();
        };

        var saveCount = 0;

        $scope.saveLangs = function () {

            if (saveCount < $scope.DisplayDefineLanguages.length) {

                if ($scope.DisplayDefineLanguages[saveCount].name == '' || $scope.DisplayDefineLanguages[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLangs();
                } else {
                    Restangular.all('languagedefinitions').post($scope.DisplayDefineLanguages[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLangs();
                    });
                }

            } else {
                window.location = '/define-languages-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.languagedefinition.name == '' || $scope.languagedefinition.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Language Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.languagedefinition.lastmodifiedby = $window.sessionStorage.userId;
                $scope.languagedefinition.lastmodifiedtime = new Date();
                $scope.languagedefinition.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('languagedefinitions', $routeParams.id).customPUT($scope.languagedefinition).then(function (resp) {
                    window.location = '/define-languages-list';
                });
            }
        };

        $scope.getLang = function (languageid) {
            return Restangular.one('languages', languageid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Language definition has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('languagedefinitions', $routeParams.id).get().then(function (languagedef) {
                $scope.original = languagedef;
                $scope.languagedefinition = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Language definition has been created!';
        }


    });
