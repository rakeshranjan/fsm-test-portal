'use strict';

angular.module('secondarySalesApp')
    .controller('FacilityTypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/facilitytype/create' || $location.path() === '/facilitytype/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/facilitytype/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/facilitytype/create' || $location.path() === '/facilitytype/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/facilitytype/create' || $location.path() === '/facilitytype/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/facilitytype") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Facilitytype has been Updated!';
            Restangular.one('facilitytypes', $routeParams.id).get().then(function (facilitytype) {
                //console.log('facilitytype', facilitytype);
                $scope.original = facilitytype;
                $scope.facilitytype = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Facilitytype has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('facilitytypes?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.facilitytypes = zn;
            angular.forEach($scope.facilitytypes, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.facilitytype = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.facilitytype.name == '' || $scope.facilitytype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.hnname == '' || $scope.facilitytype.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.knname == '' || $scope.facilitytype.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.taname == '' || $scope.facilitytype.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.tename == '' || $scope.facilitytype.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.mrname == '' || $scope.facilitytype.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.facilitytypes.post($scope.facilitytype).then(function () {
                    window.location = '/facilitytype';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.facilitytype.name == '' || $scope.facilitytype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.hnname == '' || $scope.facilitytype.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.knname == '' || $scope.facilitytype.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.taname == '' || $scope.facilitytype.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.tename == '' || $scope.facilitytype.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter facilitytype Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.facilitytype.mrname == '' || $scope.facilitytype.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Facilitytype Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('facilitytypes', $routeParams.id).customPUT($scope.facilitytype).then(function () {
                    console.log('facilitytype Saved');
                    window.location = '/facilitytype';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('facilitytypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
