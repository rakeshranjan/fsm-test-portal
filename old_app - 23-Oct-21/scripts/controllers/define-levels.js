'use strict';

angular.module('secondarySalesApp')
    .controller('DefineLevelsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/define-level/create' || $location.path() === '/define-level/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/define-level/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/define-level/create' || $location.path() === '/define-level/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/define-level/create' || $location.path() === '/define-level/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/cities") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (lvldfs) {
            $scope.leveldefs = lvldfs;

            $scope.leveldefArray = [];
            var count = 0;

            if ($scope.leveldefs.length == 0) {
                $scope.getLevels();
            }

            angular.forEach($scope.leveldefs, function (value, index) {
                $scope.leveldefArray.push(value.levelid);
                count++;

                if (count == $scope.leveldefs.length) {
                    $scope.getLevels();
                }
            });
        });

        $scope.getLevels = function () {

            Restangular.all('levels?filter={"where":{"and":[{"id":{"nin":[' + $scope.leveldefArray + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (lvle) {
                $scope.levels = lvle;
                // console.log($scope.levels);

                $scope.DisplayDefineLevels = [{
                    level: lvle[0].name,
                    levelid: lvle[0].id,
                    name: '',
                    disableLang: false,
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId,
                    langdark: 'images/Lgrey.png'
                  }];
            });
        };

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            if (idVal > $scope.levels.length) {
                return;
            } else {
                $scope.DisplayDefineLevels.push({
                    level: $scope.levels[idVal].name,
                    levelid: $scope.levels[idVal].id,
                    name: '',
                    disableLang: false,
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId,
                    langdark: 'images/Lgrey.png'
                });
            }
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayDefineLevels.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayDefineLevels[index].disableLang = false;
                $scope.DisplayDefineLevels[index].disableAdd = false;
                $scope.DisplayDefineLevels[index].disableRemove = false;
                $scope.DisplayDefineLevels[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayDefineLevels[index].disableLang = true;
                $scope.DisplayDefineLevels[index].disableAdd = false;
                $scope.DisplayDefineLevels[index].disableRemove = false;
                $scope.DisplayDefineLevels[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayDefineLevels[$scope.lastIndex].disableAdd = true;
            $scope.DisplayDefineLevels[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayDefineLevels[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayDefineLevels);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('leveldefinitions/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /********************************************** WATCH ***************************************/

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (lvldef) {
            $scope.leveldefinitions = lvldef;
            angular.forEach($scope.leveldefinitions, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel();
        };

        var saveCount = 0;

        $scope.saveLevel = function () {

            if (saveCount < $scope.DisplayDefineLevels.length) {

                if ($scope.DisplayDefineLevels[saveCount].name == '' || $scope.DisplayDefineLevels[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel();
                } else {
                    Restangular.all('leveldefinitions').post($scope.DisplayDefineLevels[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel();
                    });
                }

            } else {
                window.location = '/define-levels-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.leveldefinition.name == '' || $scope.leveldefinition.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Level Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.leveldefinition.lastmodifiedby = $window.sessionStorage.userId;
                $scope.leveldefinition.lastmodifiedtime = new Date();
                $scope.leveldefinition.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('leveldefinitions', $routeParams.id).customPUT($scope.leveldefinition).then(function (resp) {
                    window.location = '/define-levels-list';
                });
            }
        };

        $scope.getLevel = function (levelid) {
            return Restangular.one('levels', levelid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.leveldefinition) {
                if ($scope.leveldefinition.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                       // console.log(x);

                        if ($scope.leveldefinition[key] != null) {
                            $scope.languages[mCount].lang = $scope.leveldefinition[key];
                          //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.leveldefinition[key] == null) {
                           // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.leveldefinition.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.leveldefinition["lang" + data.inx] = data.lang;
               // console.log($scope.leveldefinition);
            });
            
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.message = 'Level definition has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('leveldefinitions', $routeParams.id).get().then(function (leveldefinition) {
                $scope.original = leveldefinition;
                $scope.leveldefinition = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Level definition has been created!';
        }

    })

    .directive('langmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
