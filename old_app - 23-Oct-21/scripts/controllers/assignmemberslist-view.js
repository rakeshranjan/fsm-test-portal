'use strict';

angular.module('secondarySalesApp')
  .controller('AssignmembersListViewCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/
    $scope.modalTitle = 'Thank You';
    $scope.disableAssign = true;

    $scope.DisplayTodoStatus = [{
      name: '',
      disableLang: false,
      disableAdd: false,
      disableRemove: false,
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      langdark: 'images/Lgrey.png'
    }];

    $scope.Add = function (index) {

      $scope.myobj = {};

      var idVal = index + 1;

      $scope.DisplayTodoStatus.push({
        name: '',
        disableLang: false,
        disableAdd: false,
        disableRemove: false,
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        langdark: 'images/Lgrey.png'
      });
    };

    $scope.Remove = function (index) {
      var indexVal = index - 1;
      $scope.DisplayTodoStatus.splice(indexVal, 1);
    };

    $scope.myobj = {};

    $scope.typeChange = function (index) {
      $scope.DisplayTodoStatus[index].name = '';
      $scope.DisplayTodoStatus[index].disableLang = false;
      $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
    };

    $scope.levelChange = function (index) {
      $scope.DisplayTodoStatus[index].disableLang = true;
      $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
      $scope.DisplayTodoStatus[index].disableAdd = false;
      $scope.DisplayTodoStatus[index].disableRemove = false;
    };

    $scope.langModel = false;

    $scope.Lang = function (index, name, type) {

      $scope.lastIndex = index;

      angular.forEach($scope.languages, function (data) {
        data.lang = name;
        // console.log(data);
      });

      $scope.langModel = true;
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/customerslist-view" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };


    $scope.getengineer = function (assignedto) {
      return Restangular.one('users', assignedto).get().$object;
    };

    $scope.getstatus = function (statusId) {
      return Restangular.one('todostatuses', statusId).get().$object;
    };

    $scope.getstate = function (stateId) {
      return Restangular.one('organisationlocations', stateId).get().$object;
    };

    $scope.getcity = function (cityId) {
    return Restangular.one('organisationlocations', cityId).get().$object;
    };
    /*********************************** INDEX *******************************************/

    // Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().then(function (srvy) {
    //     $scope.surveyquestions = srvy;

    // Restangular.all('members?filter[where][deleteflag]=false&filter[where][assignFlag]=true').getList().then(function (membr) {
    // Restangular.all('members?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][createdby]=' + $window.sessionStorage.userId).getList().then(function (membr) {
    // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true').getList().then(function (membr) {
    //console.log($window.sessionStorage.userId, $window.sessionStorage.orgStructure, "USER-ID : orgStructure -112")
    // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (membr) {
    //   //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][assignedto]=' + $window.sessionStorage.userId).getList().then(function (membr) {
    //   $scope.members = membr;
    //   console.log($scope.members, "ASSIGNED - LIST - 114")
    //   angular.forEach($scope.members, function (member, index) {
    //     member.index = index + 1;

    //     // $scope.TotalTodos = [];
    //     // $scope.TotalTodos.push(member);
    //     // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
    //     //   $scope.customername = custmor;
    //     //   for (var j = 0; j < $scope.customername.length; j++) {
    //     //     if (member.customerId == $scope.customername[j].id) {
    //     //       member.customername = $scope.customername[j].name;
    //     //       break;
    //     //     }
    //     //   }
    //     // });
    //     Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
    //       $scope.categoryname = categ;
    //       for (var j = 0; j < $scope.categoryname.length; j++) {
    //         if (member.categoryId == $scope.categoryname[j].id) {
    //           member.categoryname = $scope.categoryname[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
    //       $scope.subcategoryname = subcateg;
    //       for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //         if (member.subcategoryId == $scope.subcategoryname[j].id) {
    //           member.subcategoryname = $scope.subcategoryname[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (orglocresp) {
    //       $scope.statename = orglocresp;
    //       for (var j = 0; j < $scope.statename.length; j++) {
    //         if (member.stateId == $scope.statename[j].id) {
    //           member.statename = $scope.statename[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (orglocityresp) {
    //       $scope.cityname = orglocityresp;
    //       for (var j = 0; j < $scope.cityname.length; j++) {
    //         if (member.cityId == $scope.cityname[j].id) {
    //           member.cityname = $scope.cityname[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
    //       $scope.username = userresp;
    //       for (var j = 0; j < $scope.username.length; j++) {
    //         if (member.assignedto == $scope.username[j].id) {
    //           member.username = $scope.username[j].username;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
    //       $scope.statusname = todostatusresp;
    //       for (var j = 0; j < $scope.statusname.length; j++) {
    //         if (member.statusId == $scope.statusname[j].id) {
    //           member.statusname = $scope.statusname[j].name;
    //           break;
    //         }
    //       }
    //     });

    //   });
    //   //});

    // });


    /************************************* Spliting Orgstructure ***************************************************/
    $scope.searchbulkupdate = '';

    $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    //console.log($scope.orgStructureSplit, "$scope.orgStructureSplit-428")
    $scope.CountrySplit = $scope.orgStructureSplit[0];
    $scope.ZoneSplit = $scope.orgStructureSplit[1];
    //console.log($scope.CountrySplit, "$scope.CountrySplit-430")
    $scope.intermediateOrgStructure = "";
    for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
      if ($scope.intermediateOrgStructure === "") {
        $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
      } else {
        $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
      }
    }
    //console.log($scope.intermediateOrgStructure, "$scope.intermediateOrgStructure-436")
    //--- No need now (Let it be because LHS menu somewhere linked) - It was for single state & multi city ---------
    $scope.orgStructureFilterArray = [];
    if ($scope.orgStructureSplit.length > 0) {
      $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
      console.log($scope.LocationsSplit, "$scope.LocationsSplit-444")
      for (var i = 0; i < $scope.LocationsSplit.length; i++) {
        $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
      }

    }
    console.log($scope.orgStructureFilterArray, "$scope.orgStructureFilterArray-451")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));
    //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
    $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
    console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
    //--- End of No need now - It was for single state & multi city ---------

 //-- For Multi Zone, state & multi City array filter work Ravi ---------------------------------
 $scope.orgStructureZoneFilterArray = [];
 if ($scope.orgStructureSplit.length > 1) {
   $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
   console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
   for (var i = 0; i < $scope.ZoneSplit.length; i++) {
     $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.ZoneSplit[i]);
   }
 }

 //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
 $scope.orgStructureStateFilterArray = [];
 if ($scope.orgStructureSplit.length > 1) {
   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
 //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
  // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
  for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
   for (var i = 0; i < $scope.StatesSplit.length; i++) {
     // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
     $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.StatesSplit[i]);
   }
  }
 }

    //-- For Multi state & multi City array filter work Satyen ---------------------------------
    // $scope.orgStructureStateFilterArray = [];
    // // if ($scope.orgStructureSplit.length > 0) {
    //   if ($scope.orgStructureSplit.length > 1) {
    //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //    // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //    $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //   }
    // }
    //console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-468")
    $scope.orgStructureFinalFilterArray = [];
    if ($scope.orgStructureStateFilterArray.length > 0) {
      for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
        for (var i = 0; i < $scope.LocationsSplit.length; i++) {
          $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
        }
      }
    }
    //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", "");

    if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4 || window.sessionStorage.roleId == 8) {
      
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%').getList().then(function (membr) {

            $scope.members = membr;
             console.log($scope.members, "Country & Zone")

            angular.forEach($scope.members, function (data, index) {
              data.index = index + 1;

            });
          });
        
    }

    else if (window.sessionStorage.roleId == 5 || window.sessionStorage.roleId == 6) {
      console.log($scope.orgStructureFilter, "ORGSTRUCT-FILTER-570")
        //-- Previous code of Single state & multi city was same as was in it is commented for role == 7 and 15 -----
        console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-574")
        //var str = $scope.orgStructureFilter; //previously
        var str = $scope.orgStructureUptoStateFilter;
        var strOrgstruct = str.split('"')
        //console.log(strOrgstruct[1], "strorgstruct-578", strOrgstruct)

        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-586")

        // from here ---
        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

            $scope.membersnew = membr;
            //console.log($scope.membersnew, "TICKET-DISPLAY-595")
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.members = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-602")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-603")
             console.log($scope.members, "Senior manager")

            angular.forEach($scope.members, function (data, index) {
              data.index = index + 1;

              

              
            });
          });
        }
    } else if (window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
      var str = $scope.orgStructureFilter;
        var strOrgstruct = str.split('"');
        //console.log(strOrgstruct[1], "strorgstruct-638", strOrgstruct)
        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-651")

        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

            $scope.membersnew = membr;
            //console.log($scope.membersnew, "TICKET-DISPLAY-662")
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.members = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-669")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-670")
             console.log($scope.members, "Manager")

            angular.forEach($scope.members, function (data, index) {
              data.index = index + 1;

              
            });
          });
        }
    }

  });
