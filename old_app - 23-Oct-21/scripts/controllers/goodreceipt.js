'use strict';

angular.module('secondarySalesApp')
	.controller('GoodreceiptCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
	
        Restangular.all('soheaders?filter[where][flag]=false').getList().then(function (so) {
            $scope.soheaders = so;
            angular.forEach($scope.soheaders, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getPartner = function (partnerid) {
            return Restangular.one('partners', partnerid).get().$object;
        };

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.$watch('customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });

                Restangular.all('soheaders?filter[where][customerId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });

                Restangular.all('soheaders?filter[where][categoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('soheaders?filter[where][subcategoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

    });