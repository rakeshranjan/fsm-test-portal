'use strict';

angular.module('secondarySalesApp')
    .controller('Level3Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/level3/create' || $location.path() === '/level3/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level3/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level3/create' || $location.path() === '/level3/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level3/create' || $location.path() === '/level3/' + $routeParams.id;
            return visible;
        };

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            if ($routeParams.id) {
                $scope.message = leveldefinition[2].name + ' has been Updated!';
            } else {
                $scope.message = leveldefinition[2].name + ' has been Created!';
            }

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/level3") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (l1) {
            $scope.levelones = l1;
        });

        $scope.$watch('level1', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwos = l2;
                });
            }
        });

        Restangular.all('levelthrees?filter[where][deleteflag]=false').getList().then(function (lvl3) {
            $scope.levelthrees = lvl3;

            angular.forEach($scope.levelthrees, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.level1Id = '';
        $scope.level2Id = '';

        $scope.$watch('level1Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwolist = l2;
                });
                
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (lvl3) {
                    $scope.levelthrees = lvl3;

                    angular.forEach($scope.levelthrees, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });


        $scope.$watch('level2Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (lvl3) {
                    $scope.levelthrees = lvl3;

                    angular.forEach($scope.levelthrees, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        /************************ end ********************************************************/

        $scope.DisplayLevelthrees = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png'
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayLevelthrees.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png'
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLevelthrees.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLevelthrees[index].disableLang = false;
                $scope.DisplayLevelthrees[index].disableAdd = false;
                $scope.DisplayLevelthrees[index].disableRemove = false;
                $scope.DisplayLevelthrees[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayLevelthrees[index].disableLang = true;
                $scope.DisplayLevelthrees[index].disableAdd = false;
                $scope.DisplayLevelthrees[index].disableRemove = false;
                $scope.DisplayLevelthrees[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayLevelthrees[$scope.lastIndex].disableAdd = true;
            $scope.DisplayLevelthrees[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayLevelthrees[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayLevelthrees);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('levelthrees/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel3();
        };

        var saveCount = 0;

        $scope.saveLevel3 = function () {

            if (saveCount < $scope.DisplayLevelthrees.length) {

                if ($scope.DisplayLevelthrees[saveCount].name == '' || $scope.DisplayLevelthrees[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel3();
                } else {

                    $scope.DisplayLevelthrees[saveCount].leveloneid = $scope.level1;
                    $scope.DisplayLevelthrees[saveCount].leveltwoid = $scope.level2;

                    Restangular.all('levelthrees').post($scope.DisplayLevelthrees[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel3();
                    });
                }

            } else {
                window.location = '/level3-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.levelthree.name == '' || $scope.levelthree.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter' + $scope.leveldefinitions[2].name + 'Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.levelthree.lastmodifiedby = $window.sessionStorage.userId;
                $scope.levelthree.lastmodifiedtime = new Date();
                $scope.levelthree.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('levelthrees', $routeParams.id).customPUT($scope.levelthree).then(function (resp) {
                    window.location = '/level3-list';
                });
            }
        };

        $scope.getLevel1 = function (leveloneid) {
            return Restangular.one('levelones', leveloneid).get().$object;
        };

        $scope.getLevel2 = function (leveltwoid) {
            return Restangular.one('leveltwos', leveltwoid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.levelthree) {
                if ($scope.levelthree.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                        // console.log(x);

                        if ($scope.levelthree[key] != null) {
                            $scope.languages[mCount].lang = $scope.levelthree[key];
                            //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.levelthree[key] == null) {
                            // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.levelthree.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.levelthree["lang" + data.inx] = data.lang;
                // console.log($scope.leveldefinition);
            });

            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('levelthrees', $routeParams.id).get().then(function (levelthree) {
                $scope.original = levelthree;
                $scope.levelthree = Restangular.copy($scope.original);
            });

            Restangular.all('leveltwos?filter[where][deleteflag]=false').getList().then(function (l2) {
                $scope.leveltwos = l2;
            });
        }

    });
