'use strict';
angular.module('secondarySalesApp')
    .controller('ReportIncidentCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
        /*********/
        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
            window.location = "/";
        }
        /*********/
        $scope.auditlog = {
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 46,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId
        };
        /***********************************************************************/
        $scope.HideSubmitButton = false;
        $scope.reportincident = {
            // "referredby":false,
            "physical": false,
            "emotional": false,
            "sexual": false,
            "propertyrelated": false,
            "childrelated": false,
            "police": false,
            "goons": false,
            "clients": false,
            "partners": false,
            "husband": false,
            "serviceprovider": false,
            "familymember": false,
            "otherkp": false,
            "perpetratorother": false,
            "neighbour": false,
            "authorities": false,
            "co": false,
            "timetorespond": false,
            "resolved": false,
            "reported": false,
            "reportedpolice": false,
            "reportedngos": false,
            "reportedfriends": false,
            "reportedplv": false,
            "reportedcoteam": false,
            "reportedchampions": false,
            "reportedotherkp": false,
            "reportedlegalaid": false,
            "referredcouncelling": false,
            "referredmedicalcare": false,
            "referredcomanager": false,
            "referredplv": false,
            "referredlegalaid": false,
            "referredboardmember": false,
            "noactionreqmember": false,
            "noactiontaken": false,
            "multiplemembers": ['1', '2'],
            "severity": "Moderate",
            "currentstatus": 'Needs Follow Up',
            "stateid": $window.sessionStorage.zoneId,
            "state": $window.sessionStorage.zoneId,
            "districtid": $window.sessionStorage.salesAreaId,
            "district": $window.sessionStorage.salesAreaId,
            "coid": $window.sessionStorage.coorgId,
            "facility": $window.sessionStorage.coorgId,
            "beneficiaryid": null,
            "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
            "lastmodifiedtime": new Date(),
            "lastmodifiedbyrole": $window.sessionStorage.roleId
        };


        $scope.reportincident.follow = [];
        $scope.todo = {
            multiplemembers: [],
            facilityId: $scope.getfacilityId,
            roleId: $window.sessionStorage.roleId
        };

        /**************************************** Member *******************************************/
        //console.log('$window.sessionStorage.fullName',$window.sessionStorage.fullName);
        $scope.$watch('reportincident.severity', function (newValue, oldValue) {
            Restangular.all('servityofincidents?filter[where][deleteflag]=false' + '&filter[where][name]=' + newValue).getList().then(function (responName) {
                $scope.printdocumenttype = responName[0];
            });
        });

        $scope.UserLanguage = $window.sessionStorage.language;
        Restangular.all('servityofincidents?filter[where][deleteflag]=false').getList().then(function (responseseservity) {
            $scope.servityofincidents = responseseservity;
            $scope.reportincident.severity = 'Moderate';
        });
        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.currentstatusofcases = response;
            $scope.reportincident.currentstatus = 'Needs Follow Up';
        });


        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            $scope.memberfullname = member;
            $scope.benfid = $window.sessionStorage.fullName;
            $scope.modalInstanceLoad.close();
        });

        if ($window.sessionStorage.roleId == 5) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.reportincident.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
            });
            $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
            });

        } else {
            Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                    $scope.reportincident.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                });
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                    });
                });
            });
        }


        /************************************************************************/

        $scope.reportincident.reported = false;
        $scope.report = true;
        $scope.$watch('reportincident.reported', function (newValue, oldValue) {
            //console.log('Report Too');
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.reported === true) {
                $scope.report = false;
            } else {

                $scope.report = true;
                $scope.timetorespond = true;
                $scope.reportincident.reportedcoteam = false;
                $scope.reportincident.timetorespond = false;
            }
        });

        $scope.reportincident.reportedcoteam = false;
        $scope.timetorespond = true;
        $scope.$watch('reportincident.reportedcoteam', function (newValue, oldValue) {
            //console.log('reportincident.reportedcoteam');
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.reportedcoteam === true) {
                $scope.timetorespond = false;
            } else {
                $scope.timetorespond = true;
                $scope.reportincident.timetorespond = false;
            }
        });



        $scope.$watch('reportincident.multiplemembers', function (newValue, oldValue) {
            console.log('reportincident.multiplemembers', newValue);
        });
        $scope.member = true;
        $scope.memberone = true;
        $scope.$watch('reportincident.co', function (newValue, oldValue) {
            //console.log('reportincident.co', newValue);
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.co === false) {
                $scope.member = true;
                $scope.memberone = true;
            } else if ($scope.reportincident.co === true) {
                $scope.member = false;
                $scope.memberone = false;
                $scope.reportincident.multiplemembers.push($window.sessionStorage.fullName);

            } else {
                $scope.member = true;
                $scope.memberone = true;
            }

        });


        $scope.followupdatehide = false;
        $scope.followuphide = false;
        $scope.dateofclosure = true;
        //$scope.reportincident.currentstatus = 'Needs Follow Up';
        $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
            //$scope.reportincident.currentstatus = 'Needs Follow Up';
            if ($scope.reportincident.currentstatus == null) {
                $scope.reportincident.currentstatus = 'Needs Follow Up';
            } else {
                Restangular.all('currentstatusofcases?filter[where][deleteflag]=false' + '&filter[where][name]=' + newValue).getList().then(function (responseName) {
                    $scope.printcurrentstatuscase = responseName[0];
                });
                if (newValue === oldValue) {
                    return;
                } else if ($scope.reportincident.currentstatus === 'Needs Follow Up') {
                    $scope.dateofclosure = true;
                    $scope.followuphide = false;
                    $scope.followupdatehide = false;
                } else if ($scope.reportincident.currentstatus === 'Resolved' || $scope.reportincident.currentstatus === 'Closed') {
                    $scope.followuphide = true;
                    $scope.dateofclosure = true;
                    $scope.dateofclosure = false;
                    $scope.followupdatehide = true;
                    $scope.reportincident.dateofclosure = new Date();
                } else {
                    $scope.dateofclosure = false;
                }
            };

        });

        $scope.$watch('reportincident.noactiontaken', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.noactiontaken == true) {
                $scope.reportincident.currentstatus = 'Closed';
            } else {
                $scope.reportincident.currentstatus = 'Needs Follow Up';
            }
        });
        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.reportincident.incidentdate = new Date();
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.reportincident.followupdate = sevendays;


        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.reportincident.followupopened = true;
        };

        $scope.reportincidentfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
        /************************************************************* MOdal ******************************/
        $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
        $scope.addedtodos = [];
        $scope.CancelFollow = function () {
            $scope.reportincident.follow = $scope.oldFollowUp;
            $scope.showfollowupModal = !$scope.showfollowupModal;
        };



        $scope.todocount = 0;
        $scope.todocount1 = 0;
        $scope.SaveToDo = function () {
            $scope.addedtodocount = 0;
            $scope.addedtodocount1 = 0;

            for (var i = 0; i < $scope.addedtodos.length; i++) {
                if (i == 0) {
                    $scope.addedtodos[i].beneficiaryid = window.sessionStorage.fullName;
                } else {
                    $scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount];
                    Restangular.all('todos').post($scope.addedtodos[i]).then(function (resp) {
                        console.log('resp', resp);
                        $scope.addedtodocount++;
                        $scope.addedtodocount1++;
                        if ($scope.addedtodocount >= $scope.addedtodos.length && $scope.addedtodocount1 >= $scope.addedtodos.length) {
                            $scope.todocount++;
                            $scope.todocount1++;
                            if ($scope.todocount < $scope.attendeestodo.length && $scope.todocount1 < $scope.attendeespurpose.length) {
                                $scope.SaveToDo();
                            } else if ($scope.SaveToDo.length <= $scope.addedtodos.length) {
                                window.location = '/';
                            }
                        }
                    });
                }
            }
        };

        $scope.reportadd = 0;
        $scope.documentdataModal = false;
        $scope.CreateClicked = false;
        $scope.SaveReportIncident = function () {
            if ($scope.reportincident.physical == true || $scope.reportincident.sexual == true || $scope.reportincident.childrelated == true || $scope.reportincident.emotional == true || $scope.reportincident.propertyrelated == true) {

                if ($scope.reportincident.police == true || $scope.reportincident.clients == true || $scope.reportincident.serviceprovider == true || $scope.reportincident.otherkp == true || $scope.reportincident.neighbour == true || $scope.reportincident.goons == true || $scope.reportincident.partners == true || $scope.reportincident.familymember == true || $scope.reportincident.husband == true) {
                    if ($scope.reportincident.reported == false || $scope.reportincident.reportedpolice == true || $scope.reportincident.reportedngos == true || $scope.reportincident.reportedfriends == true || $scope.reportincident.reportedplv == true || $scope.reportincident.reportedcoteam == true || $scope.reportincident.reportedchampions == true || $scope.reportincident.reportedotherkp == true || $scope.reportincident.reportedlegalaid == true) {
                        console.log('check');
                        if ($scope.reportincident.referredcouncelling == true || $scope.reportincident.referredmedicalcare == true || $scope.reportincident.referredcomanager == true || $scope.reportincident.referredplv == true || $scope.reportincident.referredlegalaid == true || $scope.reportincident.referredboardmember == true || $scope.reportincident.noactionreqmember == true || $scope.reportincident.noactiontaken == true) {

                            if ($scope.dateofclosure == true) {
                                if ($scope.reportincident.follow.length > 0) {
                                    $scope.CreateClicked = true;
                                    $scope.reportcount = 0;
                                    for (var i = 0; i < $scope.reportincident.follow.length; i++) {
                                        if (i == 0) {
                                            $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
                                        } else {
                                            $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
                                        }
                                    }
                                    $scope.SaveIncident();
                                    console.log('Save');
                                } else {

                                    $scope.AlertMessage = $scope.followupmandatory; //'Followup is Mandatory';
                                    $scope.openOneAlert();

                                }
                            } else {
                                $scope.reportcount = 0;
                                for (var i = 0; i < $scope.reportincident.follow.length; i++) {
                                    if (i == 0) {
                                        $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
                                    } else {
                                        $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
                                    }
                                }
                                $scope.SaveIncident();
                            }
                            $scope.OK = function () {
                                $scope.documentdataModal = !$scope.documentdataModal;
                                window.location = '/';
                            };

                        } else {
                            $scope.AlertMessage = $scope.optactiontaken; //'At least one option must be selected in Action Taken and Followup';
                            $scope.openOneAlert();
                        }

                    } else {
                        $scope.AlertMessage = $scope.optreportedto; //'At least one option must be selected in Reported To';
                        $scope.openOneAlert();
                    }

                } else {
                    $scope.AlertMessage = $scope.optpreperator; //'At least one option must be selected in Perpetrator Details';
                    $scope.openOneAlert();
                }
            } else {
                $scope.AlertMessage = $scope.optincidenttype; //'At least one option must be selected in Incident Type';
                $scope.openOneAlert();
            }
        };

        $scope.newtodocount = 0;
        $scope.beneficiarydataModal = false;
        $scope.SaveIncident = function () {
            if ($scope.reportincident.co === false) {
                console.log('$scope.benfid', $scope.benfid);

                $scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
                Restangular.all('reportincidents').post($scope.reportincident).then(function (onemember) {

                    $scope.SubmittedReport = onemember;
                    console.log('SubmittedReport', $scope.SubmittedReport);
                    console.log('SubmittedReport.severity', $scope.SubmittedReport.severity);
                    $scope.auditlog.entityid = onemember.id;
                    var respdate = $filter('date')(onemember.incidentdate, 'dd-MMMM-yyyy');
                    var respdate2 = $filter('date')(onemember.followupdate, 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Report Incident Created With Following Details: ' + 'When did it happen - ' + respdate + ' , ' + 'MemberId - ' + onemember.beneficiaryid + ' , ' + 'Current Status - ' + onemember.currentstatus + ' , ' + 'Follow Up - ' + onemember.followupneeded + ' , ' + 'Date - ' + respdate2;

                    $scope.memberupdate = {
                        lastmeetingdate: new Date()
                    }
                    Restangular.all('beneficiaries/' + $window.sessionStorage.fullName).customPUT($scope.memberupdate).then(function (responsemember) {
                        console.log('responsemember', responsemember);
                        if (responsemember.stress_data == true) {
                            $scope.stressReport = {
                                stress_data: true,
                                id: onemember.id
                            }
                        } else {
                            $scope.stressReport = {
                                stress_data: false,
                                id: onemember.id
                            }
                        }
                        Restangular.all('reportincidents/' + onemember.id).customPUT($scope.stressReport).then(function (stressReport) {
                            Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                                //$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
                                $scope.documentdataModal = !$scope.documentdataModal;
                            });

                        });
                    });
                });
            } else if ($scope.reportincident.co === true) {
                if ($scope.reportincident.multiplemembers.length == 0) {
                    $scope.AlertMessage = $scope.atleastonemember;
                    $scope.openOneAlert();
                } else {
                    $scope.CreateClicked = true;
                    $scope.reportincident.beneficiaryid = $scope.reportincident.multiplemembers[$scope.reportadd];
                    Restangular.all('reportincidents').post($scope.reportincident).then(function (allmember) {
                        $scope.SubmittedReport = allmember;
                        $scope.auditlog.entityid = allmember.id;

                        var respdate = $filter('date')(allmember.incidentdate, 'dd-MMMM-yyyy');
                        var respdate2 = $filter('date')(allmember.followupdate, 'dd-MMMM-yyyy');
                        $scope.auditlog.description = 'Report Incident Created With Following Details: ' + 'When did it happen - ' + allmember.incidentdate + ' , ' + 'MemberId - ' + allmember.beneficiaryid + ' , ' + 'Current Status - ' + allmember.currentstatus + ' , ' + 'Follow Up - ' + allmember.followupneeded + ' , ' + 'Date - ' + respdate;
                        console.log('aa', $scope.reportincident.multiplemembers[$scope.reportadd])
                        $scope.memberupdate = {
                            lastmeetingdate: new Date()
                        }
                        Restangular.all('beneficiaries/' + $scope.reportincident.multiplemembers[$scope.reportadd]).customPUT($scope.memberupdate).then(function (responsemember) {
                            //console.log('responsemember', responsemember);
                            if (responsemember.stress_data == true) {
                                $scope.stressReport = {
                                    stress_data: true,
                                    id: allmember.id
                                }
                            } else {
                                $scope.stressReport = {
                                    stress_data: false,
                                    id: allmember.id
                                }
                            }

                            Restangular.all('reportincidents/' + allmember.id).customPUT($scope.stressReport).then(function (stressReport) {
                                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {

                                    $scope.reportadd++;
                                    if ($scope.reportadd < $scope.reportincident.multiplemembers.length) {
                                        $scope.SaveIncident();

                                    } else {
                                        $scope.documentdataModal = !$scope.documentdataModal;
                                    }

                                });

                            });
                        });
                    });
                }
            };

        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };
        /******************************* Language ********************************************/
        $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.whendidhappen = langResponse.whendidhappen;
            $scope.multiplepeopleeffect = langResponse.multiplepeopleeffect;
            $scope.yes = langResponse.yes;
            $scope.no = langResponse.no;
            $scope.printmembername = langResponse.membername;
            $scope.printmember = langResponse.member;
            $scope.divincidenttype = langResponse.divincidenttype;
            $scope.physical = langResponse.physical;
            $scope.sexual = langResponse.sexual;
            $scope.chieldrelated = langResponse.chieldrelated;
            $scope.emotional = langResponse.emotional;
            $scope.propertyrelated = langResponse.propertyrelated;
            $scope.severityofincident = langResponse.severityofincident;
            $scope.divperpetratordetail = langResponse.divperpetratordetail;
            $scope.police = langResponse.police;
            $scope.client = langResponse.client;
            $scope.serviceprovider = langResponse.serviceprovider;
            $scope.otherkp = langResponse.otherkp;
            $scope.neighbour = langResponse.neighbour;
            $scope.goons = langResponse.goons;
            $scope.printpartners = langResponse.partners;
            $scope.familiymember = langResponse.familiymember;
            $scope.othersexkp = langResponse.othersexkp;
            $scope.divreported = langResponse.divreported;
            $scope.reportedto = langResponse.reportedto;
            $scope.ngos = langResponse.ngos;
            $scope.friends = langResponse.friends;
            $scope.plv = langResponse.plv;
            $scope.coteam = langResponse.coteam;
            $scope.champion = langResponse.champion;
            $scope.otherkps = langResponse.otherkps;
            $scope.legalaidclinic = langResponse.legalaidclinic;
            $scope.divtimetorespond = langResponse.divtimetorespond;
            $scope.refferedcounselling = langResponse.refferedcounselling;
            $scope.refferedmedicalcare = langResponse.refferedmedicalcare;
            $scope.refferedcomanager = langResponse.refferedcomanager;
            $scope.refferedplv = langResponse.refferedplv;
            $scope.refferedaidclinic = langResponse.refferedaidclinic;
            $scope.refferedboardmember = langResponse.refferedboardmember;
            $scope.followupdate = langResponse.followupdate;
            $scope.printdateofclosure = langResponse.dateofclosure;
            $scope.create = langResponse.create;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;
            $scope.okbutton = langResponse.ok;
            $scope.divactiontaken = langResponse.divactiontaken;
            $scope.twohrs = langResponse.twohrs;
            $scope.twototwentyfourhrs = langResponse.twototwentyfourhrs;
            $scope.greatertwentyfourhrs = langResponse.greatertwentyfourhrs;
            $scope.currentstatuscase = langResponse.currentstatuscase;
            $scope.followuprequired = langResponse.followuprequired;
            $scope.headerreportincident = langResponse.headerreportincident;
            $scope.PrintHusband = langResponse.husband;
            $scope.mandatoryfield = langResponse.mandatoryfield;
            $scope.modalTitle = langResponse.createdetails;

            $scope.optactiontaken = langResponse.optactiontaken;
            $scope.optreportedto = langResponse.optreportedto;
            $scope.optpreperator = langResponse.optpreperator;
            $scope.optincidenttype = langResponse.optincidenttype;
            $scope.atleastonemember = langResponse.atleastonemember;
            $scope.followupmandatory = langResponse.followupmandatory;
            $scope.noactionreqmember = langResponse.noactionreqmember;
            $scope.noactiontaken = langResponse.noactiontaken;
        });

        $scope.membernameDisabled = true;
        /*		if ($window.sessionStorage.language == 1) {
        			$scope.modalTitle = 'Created With the Following Details';
        		} else if ($window.sessionStorage.language == 2) {
        			$scope.modalTitle = 'निम्न विवरण के साथ बनाया';
        		} else if ($window.sessionStorage.language == 3) {
        			$scope.modalTitle = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        		} else if ($window.sessionStorage.language == 4) {
        			$scope.modalTitle = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        		} else if ($window.sessionStorage.language == 5) {
        			$scope.modalTitle = 'క్రింది వివరాలను తో రూపొందించబడింది';
        		} else if ($window.sessionStorage.language == 6) {
        			$scope.modalTitle = 'ह्या माहिती प्रमाणे करनायत आलेळे आहे';
        		}*/




    })

.directive('modal2', function () {
    return {
        template: '<div class="modal fade" data-backdrop="static">' +
            '<div class="modal-dialog modal-sm">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title">{{ title1 }}</h4>' +
            '</div>' +
            '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title1 = attrs.title1;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});