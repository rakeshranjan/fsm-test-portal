'use strict';

angular.module('secondarySalesApp')
  .controller('DashboardCtrl', function ($scope, $http, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/
    $scope.modalTitle = 'Thank You';

    if ($window.sessionStorage.roleId == '1') {
      document.getElementById("demo").innerHTML = "Country";
    } else if ($window.sessionStorage.roleId == '7' || $window.sessionStorage.roleId == '15') {
      document.getElementById("demo").innerHTML = "City";
    } else if ($window.sessionStorage.roleId == '5') {
      document.getElementById("demo").innerHTML = "City";
    } else if ($window.sessionStorage.roleId == '11') {
      document.getElementById("demo").innerHTML = "Postal Code";
    }

    if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
      $scope.showAssign = false;
    } else if ($window.sessionStorage.roleId === '5') {
      $scope.showAssign = false;
    } else {
      $scope.showAssign = true;
    }

    if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {

      var vorgStructure = window.sessionStorage.orgStructure;
      var strLevelcities = vorgStructure.split(";");
      var strcities = strLevelcities[2];
      var strcitiesid = strcities.split("-")
      console.log(strcitiesid, "Cities-Id")
      Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + strcitiesid + ']}}}').getList().then(function (city) {
        $scope.cities = city;
        console.log("cut", city);
      });


      $scope.$watch('dashboard.city', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else if (newValue == 'all') {
          Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
            .getList().then(function (data3) {
              console.log("datas", data3);
              $scope.buildpiechart1(data3);

            });

        } else {
          Restangular.all('ticket_status_role_view?filter[where][cityId]=' + newValue).getList().then(function (data2) {
            console.log('data2', data);
            $scope.buildpiechart1(data2);
          });

        }
      });
      Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
        .getList().then(function (data) {

          console.log("datas11", data);
          $scope.buildpiechart1(data);
        });
    }
    if ($window.sessionStorage.roleId == 5) {

      var vorgStructure = window.sessionStorage.orgStructure;
      var strLevelcities = vorgStructure.split(";");
      var strcities = strLevelcities[2];
      var strcitiesid = strcities.split("-")
      console.log(strcitiesid, "Cities-Id")
      Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + strcitiesid + ']}}}').getList().then(function (city) {
        $scope.cities = city;
        console.log("cut", city);
      });


      $scope.$watch('dashboard.city', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else if (newValue == 'all') {
          Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
            .getList().then(function (data3) {
              console.log("datas", data3);
              $scope.buildpiechart1(data3);

            });

        } else {
          Restangular.all('ticket_status_role_view?filter[where][cityId]=' + newValue).getList().then(function (data2) {
            console.log('data2', data);
            $scope.buildpiechart1(data2);
          });

        }
      });
      Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
        .getList().then(function (data) {

          console.log("datas11", data);
          $scope.buildpiechart1(data);
        });
    } else if ($window.sessionStorage.roleId == 11) {

      var vorgStructure1 = window.sessionStorage.orgStructure;
      var strLevelcities1 = vorgStructure1.split(";");
      var strcities1 = strLevelcities1[3];
      var strcitiesid1 = strcities1.split("-");
      console.log(strcitiesid1, "Cities-Id1");
      Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + strcitiesid1 + ']}}}').getList().then(function (city) {
        $scope.cities = city;
        console.log("cut1", city);
      });


      $scope.$watch('dashboard.city', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else if (newValue == 'all') {
          Restangular.all('ticket_status_role_view?filter[where][assignFlag]=true&filter[where][assignedto]=' + window.sessionStorage.userId + '&access_token=' + window.sessionStorage.accessToken)
            .getList().then(function (data3) {

              $scope.buildpiechart1(data3);
            });

        } else {
          console.log("newValue", newValue);
          Restangular.all('ticket_status_role_view?filter[where][pincodeId]=' + newValue).getList().then(function (data2) {
            console.log('data5', data2);
            console.log('length', data2.length);

            function listProducts(prods) {
              var product_names = [];
              for (var i = 0; i < data2.length; i++) {
                if (data2[i].name == "ASSIGN" || data2[i].name == "IN PROGRESS" || data2[i].name == "COMPLETED" || data2[i].name == "APPROVAL PENDING") {
                  product_names.push({
                    todostatusId: prods[i].todostatusId,
                    name: prods[i].name,
                    total_open_count: prods[i].total_open_count,
                    stotal_backoffice_count: prods[i].total_backoffice_count,
                    total_assign_count: prods[i].total_assign_count,
                    total_inprogress_count: prods[i].total_inprogress_count,
                    total_completed_count: prods[i].total_completed_count,
                    total_approval_pending_count: prods[i].total_approval_pending_count,
                  });
                }

              }
              return product_names;
            }
            $scope.buildpiechart1(listProducts(data2));
          });

        }
      });
      Restangular.all('ticket_status_role_view?filter[where][assignFlag]=true&filter[where][assignedto]=' + window.sessionStorage.userId + '&access_token=' + window.sessionStorage.accessToken)
        .getList().then(function (data) {

          console.log("datas11", data);
          $scope.buildpiechart1(data);
        });
    } else if ($window.sessionStorage.roleId == 1) {
      Restangular.all('organisationlocations?filter[where][deleteflag]=false' + '&filter[where][level] = 36').getList().then(function (city) {
        $scope.cities = city;
        console.log("cut", city);
      });
      $http.get("//fsm-api.herokuapp.com/api/v1/ticket_status_role_view?&access_token=" + window.sessionStorage.accessToken)
        .then(function (data) {
          $scope.buildpiechart(data);
        });
      $scope.$watch('dashboard.city', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
          return;
        } else if (newValue == 'all') {
          $http.get("//fsm-api.herokuapp.com/api/v1/ticket_status_role_view?&access_token=" + window.sessionStorage.accessToken)
            .then(function (data) {
              console.log("ttt", data);
              $scope.buildpiechart(data);
            });

        } else {
          var vorgStructure2 = window.sessionStorage.orgStructure;
          var strLevelcities2 = vorgStructure2.split(";");
          var strcities2 = strLevelcities2[0];
          var strcitiesid2 = strcities2.split("-")
          console.log(strcitiesid2, "Cities-Id1")
          Restangular.all('ticket_status_role_view?filter[where][strcitiesid2]=' + newValue).getList().then(function (data1) {
            console.log('data', data);
            $scope.buildpiechart1(data1);
          });
        }
      });
    }

    $scope.buildpiechart = function (data) {
      var Gtotal_open_count = 0;
      var Gtotal_assign_count = 0;
      var Gtotal_inprogress_count = 0;
      var Gtotal_completed_count = 0;
      var Gtotal_approval_pending_count = 0;
      var Gtotal_backoffice_count = 0;

      for (var i = 0; i < data.data.length; i++) {
        if (data.data[i].todostatusId == 1 && data.data[i].id != null) {
          Gtotal_open_count = Gtotal_open_count + data.data[i].total_open_count;
          //console.log("AAAAA", Gtotal_open_count)
          //break;
        }
        console.log(Gtotal_open_count, "Gtotal_open_count")
        // $scope.name1 = data.data[0].name;
        $scope.name1 = 'OPEN';
        $scope.temp1 = Gtotal_open_count;
      }

      for (var j = 0; j < data.data.length; j++) {
        if (data.data[j].todostatusId == 3 && data.data[j].id != null) {
          Gtotal_assign_count = Gtotal_assign_count + data.data[j].total_assign_count;
          //console.log("BBBBB", Gtotal_assign_count)
          //break;
        }
        console.log(Gtotal_assign_count, "Gtotal_assign_count")
        // $scope.name2 = data.data[1].name;
        $scope.name2 = 'ASSIGN';
        $scope.temp2 = Gtotal_assign_count;
      }



      for (var x = 0; x < data.data.length; x++) {
        if (data.data[x].todostatusId == 5 && data.data[x].id != null) {
          Gtotal_completed_count = Gtotal_completed_count + data.data[x].total_completed_count;
        }
        console.log(Gtotal_completed_count, "Gtotal_completed_count")
        $scope.name3 = 'COMPLETED';
        $scope.temp3 = Gtotal_completed_count;

      }


      for (var y = 0; y < data.data.length; y++) {
        if (data.data[y].todostatusId == 6 && data.data[y].id != null) {
          Gtotal_approval_pending_count = Gtotal_approval_pending_count + data.data[y].total_approval_pending_count;
        }
        console.log(Gtotal_approval_pending_count, "Gtotal_approval_pending_count");
        $scope.name4 = 'APPROVAL PENDING';
        $scope.temp4 = Gtotal_approval_pending_count;
      }

      for (var k = 0; k < data.data.length; k++) {
        if (data.data[k].todostatusId == 4 && data.data[k].id != null) {
          Gtotal_inprogress_count = Gtotal_inprogress_count + data.data[k].total_inprogress_count;
        }
        console.log(Gtotal_inprogress_count, "Gtotal_inprogress_count")
        $scope.name5 = 'IN PROGRESS';
        $scope.temp5 = Gtotal_inprogress_count;
      }

      for (var l = 0; l < data.data.length; l++) {
        if (data.data[l].todostatusId == 2 && data.data[l].id != null) {
          Gtotal_backoffice_count = Gtotal_backoffice_count + data.data[l].total_backoffice_count;
        }
        console.log(Gtotal_backoffice_count, "Gtotal_backoffice_count")
        $scope.name6 = 'BACK OFFICE';
        $scope.temp6 = Gtotal_backoffice_count;
      }

      if (data.data == 0) {
        $scope.charts = [{
            key: 'OPEN',
            value: 0
          },
          {
            key: 'BACK OFFICE',
            value: 0
          }, {
            key: 'ASSIGNED',
            value: 0
          },
          {
            key: 'IN PROGRESS',
            value: 0
          },
          /*{ key: 'inprogress', value: 0 },*/
          {
            key: 'COMPLETED',
            value: 0
          },
          {
            key: 'APPROVAL PENDING',
            value: 0
          }
        ];
      } else {
        $scope.charts = [{
            key: $scope.name1,
            value: $scope.temp1
          },
          {
            key: $scope.name6,
            value: $scope.temp6
          },
          {
            key: $scope.name2,
            value: $scope.temp2
          },
          {
            key: $scope.name5,
            value: $scope.temp5
          },
          {
            key: $scope.name3,
            value: $scope.temp3
          },
          {
            key: $scope.name4,
            value: $scope.temp4
          }
        ];
      }
      console.log("df", $scope.charts);
      var width = 830,
        height = 450;
      var colors = d3.scaleOrdinal().domain(["COMPLETED", "ASSIGN", "OPEN", "PENDING", "INPROGRESS", "BACK OFFICE"])
        .range(["#4363d8", "#800000", "#911eb4", "#f58231", "#f032e6", "#008080"]);
      var color_hash = {
        0: ['#4363d8'],
        1: ['#800000'],
        2: ['#911eb4'],
        3: ['#f58231'],
        4: ['#f032e6'],
        5: ['#008080']
      }
      var svg = d3.select("#pie-chart").append("svg").attr("width", width)
        .attr("height", height).style("background", "white");
      //var details = [{grade: "A+", number: 20},{grade: "A", number: 36},{grade: "B", number: 43}];

      var data1 = d3.pie().sort(null).value(function (d) {
        return d.value;
      })($scope.charts);

      console.log("ght", data1);
      var segments = d3.arc().innerRadius(0)
        .outerRadius(150)
        .padAngle(0.05)
        .padRadius(50)

      var sections = svg.append("g").attr("transform", "translate(500,170)")
        .selectAll("path")
        .data(data1);
      sections.enter().append("path").attr("d", segments).attr("fill",
        function (d, i) {
          return colors(i);
        });
      var content = d3.select("g").selectAll("text").data(data1);
      content.enter().append("text").classed("inside", true).each(function (d) {
        if (d.value != 0) {
          var center = segments.centroid(d);
          d3.select(this).attr("x", center[0]).attr("y", center[1])
            .text(d.value)
            .style("fill", "white");
        }
      });
      var legend = svg.append("g")
        .attr("class", "legend")
        .attr("x", width)
        // .attr("y", 10)
        .attr("height", 50)
        .attr("width", 300)
        .attr("transform", "translate(" + (330) + ", 300)");

      legend.selectAll('g').data(data1)
        .enter()
        .append('g')
        .attr("transform", function (d, i) {
          if (i < 3) {
            var xOff = (i % 3) * 80
            var yOff = Math.floor(i / 3) * 30
            return "translate(" + xOff + "," + yOff + ")"
          } else {
            var xOff = (i % 3) * 140
            var yOff = Math.floor(i / 3) * 30
            return "translate(" + xOff + "," + yOff + ")"
          }
        })
        .each(function (d, i) {
          console.log("lll", d)
          if (i < 3) {
            var g = d3.select(this);
            g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 60)
              .attr("y", 65).style("fill", color_hash[String(i)][0]);
            g.append("text").classed("label", true).text(function (d) {
                console.log("sdfj", d);
                return d.data.key;
              })
              //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
              .attr("x", i * 60 + 20)
              .attr("y", 75).style("fill", "#000000");
          } else {
            console.log("ooo", i);
            var g = d3.select(this);
            g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 0)
              .attr("y", 65).style("fill", color_hash[String(i)][0]);
            g.append("text").classed("label", true).text(function (d) {
                console.log("sdfj", d);
                return d.data.key;
              })
              //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
              .attr("x", i * 0 + 20)
              .attr("y", 75).style("fill", "#000000");
          }
        });
    }

    $scope.buildpiechart1 = function (sdata) {
      var Gtotal_open_count = 0;
      var Gtotal_assign_count = 0;
      var Gtotal_inprogress_count = 0;
      var Gtotal_completed_count = 0;
      var Gtotal_approval_pending_count = 0;
      var Gtotal_backoffice_count = 0;

      for (var i = 0; i < sdata.length; i++) {
        if (sdata[i].todostatusId == 1) {
          Gtotal_open_count = Gtotal_open_count + sdata[i].total_open_count;
          //console.log("AAAAA", Gtotal_open_count)
          //break;
        }
        console.log(Gtotal_open_count, "Gtotal_open_count")
        // $scope.name1 = data.data[0].name;
        $scope.name1 = 'OPEN';
        $scope.temp1 = Gtotal_open_count;
      }

      for (var j = 0; j < sdata.length; j++) {
        if (sdata[j].todostatusId == 3) {
          Gtotal_assign_count = Gtotal_assign_count + sdata[j].total_assign_count;
        }
        console.log(Gtotal_assign_count, "Gtotal_assign_count")
        $scope.name2 = 'ASSIGN';
        $scope.temp2 = Gtotal_assign_count;
      }


      for (var x = 0; x < sdata.length; x++) {
        if (sdata[x].todostatusId == 5) {
          Gtotal_completed_count = Gtotal_completed_count + sdata[x].total_completed_count;
        }
        console.log(Gtotal_completed_count, "Gtotal_completed_count")
        $scope.name3 = 'COMPLETED';
        $scope.temp3 = Gtotal_completed_count;
      }

      for (var y = 0; y < sdata.length; y++) {
        if (sdata[y].todostatusId == 6) {
          Gtotal_approval_pending_count = Gtotal_approval_pending_count + sdata[y].total_approval_pending_count;
        }
        console.log(Gtotal_approval_pending_count, "Gtotal_approval_pending_count")
        $scope.name4 = 'APPROVAL PENDING';
        $scope.temp4 = Gtotal_approval_pending_count;
      }


      for (var k = 0; k < sdata.length; k++) {
        if (sdata[k].todostatusId == 4) {
          Gtotal_inprogress_count = Gtotal_inprogress_count + sdata[k].total_inprogress_count;
        }
        console.log(Gtotal_inprogress_count, "Gtotal_inprogress_count")
        $scope.name5 = 'IN PROGRESS';
        $scope.temp5 = Gtotal_inprogress_count;
      }
      for (var l = 0; l < sdata.length; l++) {
        if (sdata[l].todostatusId == 2) {
          Gtotal_backoffice_count = Gtotal_backoffice_count + sdata[l].total_backoffice_count;
        }
        console.log(Gtotal_backoffice_count, "Gtotal_backoffice_count")
        $scope.name6 = 'BACK OFFICE';
        $scope.temp6 = Gtotal_backoffice_count;
      }

      if (sdata == 0) {
        $scope.charts = [{
            key: 'OPEN',
            value: 0
          },
          {
            key: 'BACK OFFICE',
            value: 0
          }, {
            key: 'ASSIGNED',
            value: 0
          },
          {
            key: 'IN PROGRESS',
            value: 0
          },
          /*{ key: 'inprogress', value: 0 },*/
          {
            key: 'COMPLETED',
            value: 0
          },
          {
            key: 'APPROVAL PENDING',
            value: 0
          }
        ];
      } else {
        $scope.charts = [{
            key: $scope.name1,
            value: $scope.temp1
          },
          {
            key: $scope.name6,
            value: $scope.temp6
          },
          {
            key: $scope.name2,
            value: $scope.temp2
          },
          {
            key: $scope.name5,
            value: $scope.temp5
          },
          {
            key: $scope.name3,
            value: $scope.temp3
          },
          {
            key: $scope.name4,
            value: $scope.temp4
          }
        ];
      }
      console.log("df21", $scope.charts);

      /*console.log("temp1",temp); */
      var width = 830,
        height = 450;
      var colors = d3.scaleOrdinal().domain(["COMPLETED", "ASSIGN", "OPEN", "PENDING", "INPROGRESS", "BACK OFFICE"])
        .range(["#4363d8", "#800000", "#911eb4", "#f58231", "#f032e6", "#008080"]);
      var color_hash = {
        0: ['#4363d8'],
        1: ['#800000'],
        2: ['#911eb4'],
        3: ['#f58231'],
        4: ['#f032e6'],
        5: ['#008080']
      }
      var svg = d3.select("#pie-chart").append("svg").attr("width", width)
        .attr("height", height).style("background", "white");

      var data1 = d3.pie().sort(null).value(function (d) {
        return d.value;
      })($scope.charts);

      console.log("ght", data1);

      var segments = d3.arc().innerRadius(0)
        .outerRadius(150)
        .padAngle(0.05)
        .padRadius(50)
      var path = svg.append("g").attr("transform", "translate(500,170)")
        .selectAll("path")
        .data(data1);

      path.enter().append("path").attr("d", segments).attr("fill",
        function (d, i) {
          return colors(i);
        });
      var content = d3.select("g").selectAll("text").data(data1);
      content.enter().append("text").classed("inside", true).each(function (d) {
        if (d.value != 0) {
          var center = segments.centroid(d);
          d3.select(this).attr("x", center[0]).attr("y", center[1])
            .text(d.value)
            .style("fill", "white");
        }
      });
      var legend = svg.append("g")
        .attr("class", "legend")
        .attr("x", width)
        // .attr("y", 10)
        .attr("height", 50)
        .attr("width", 300)
        .attr("transform", "translate(" + (330) + ", 300)");

      legend.selectAll('g').data(data1)
        .enter()
        .append('g')
        .attr("transform", function (d, i) {
          if (i < 3) {
            var xOff = (i % 3) * 80
            var yOff = Math.floor(i / 3) * 30
            return "translate(" + xOff + "," + yOff + ")"
          } else {
            var xOff = (i % 3) * 140
            var yOff = Math.floor(i / 3) * 30
            return "translate(" + xOff + "," + yOff + ")"
          }
        })
        .each(function (d, i) {
          console.log("lll", d)
          if (i < 3) {
            var g = d3.select(this);
            g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 60)
              .attr("y", 65).style("fill", color_hash[String(i)][0]);
            g.append("text").classed("label", true).text(function (d) {
                console.log("sdfj", d);
                return d.data.key;
              })
              //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
              .attr("x", i * 60 + 20)
              .attr("y", 75).style("fill", "#000000");
          } else {
            console.log("ooo", i);
            var g = d3.select(this);
            g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 0)
              .attr("y", 65).style("fill", color_hash[String(i)][0]);
            g.append("text").classed("label", true).text(function (d) {
                console.log("sdfj", d);
                return d.data.key;
              })
              //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
              .attr("x", i * 0 + 20)
              .attr("y", 75).style("fill", "#000000");
          }
        });

    }

    $(document).ready(function () {
      $("select").click(function () {
        $("#pie-chart").empty();
      });
    });

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    // if ($window.sessionStorage.prviousLocation != "partials/onetoonelist-view" || $window.sessionStorage.prviousLocation != "partials/onetoonelist") {
    //     $window.sessionStorage.myRoute_currentPage = 1;
    //     $window.sessionStorage.myRoute_currentPagesize = 25;
    // }
    if ($window.sessionStorage.prviousLocation != "partials/dashboard" || $window.sessionStorage.prviousLocation != "partials/dashboard") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };


  });
