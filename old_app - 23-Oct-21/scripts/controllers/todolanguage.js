'use strict';

angular.module('secondarySalesApp')
    .controller('TodoLanguageCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/todolanguage/create' || $location.path() === '/todolanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todolanguage/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todolanguage/create' || $location.path() === '/todolanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todolanguage/create' || $location.path() === '/todolanguage/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/todolanguage" || $window.sessionStorage.prviousLocation != "partials/todolanguage") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /****************************************************************************/

        $scope.todo = {
            member: '',
            followUp: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };
        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });

        $scope.languageDisable = true;

       Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
           Restangular.one("roles/"+$window.sessionStorage.roleId).get().then(function(role){
             $scope.languages = langs;
               if(role.isLanguageUser){
                    $scope.todo.language = $window.sessionStorage.language;
                   $scope.languageDisable = true;
               }else{
                   $scope.languageDisable = false;
               }
           });
           
        });


        /************************************* INDEX ***************************************************/

        Restangular.all('todolanguages?filter[where][deleteflag]=false').getList().then(function (tdo) {
            $scope.todolanguages = tdo;
            angular.forEach($scope.todolanguages, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });


        $scope.validatestring = "";

        $scope.Save = function (clicked) {

            if ($scope.todo.language == '' || $scope.todo.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.todo.member == '' || $scope.todo.member == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.todo.todoLabel == '' || $scope.todo.todoLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo';

            } else if ($scope.todo.status == '' || $scope.todo.status == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select  Status';

            } else if ($scope.todo.followUp == '' || $scope.todo.followUp == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup ';

            } else if ($scope.todo.assignedTo == '' || $scope.todo.assignedTo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            } else if ($scope.todo.enableforroles == '' || $scope.todo.enableforroles == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Enable for roles';

            } else if ($scope.todo.saveLabel == '' || $scope.todo.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Save';

            } else if ($scope.todo.updateLabel == '' || $scope.todo.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Update';

            } else if ($scope.todo.cancelLabel == '' || $scope.todo.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.all('todolanguages').post($scope.todo).then(function (resp) {
                    window.location = '/todolanguage';
                });
            }
        };

        $scope.Update = function (clicked) {

            if ($scope.todo.language == '' || $scope.todo.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.todo.member == '' || $scope.todo.member == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.todo.todoLabel == '' || $scope.todo.todoLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Todo';

            } else if ($scope.todo.status == '' || $scope.todo.status == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select  Status';

            } else if ($scope.todo.followUp == '' || $scope.todo.followUp == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup ';

            } else if ($scope.todo.assignedTo == '' || $scope.todo.assignedTo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            } else if ($scope.todo.enableforroles == '' || $scope.todo.enableforroles == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Enable for roles';

            } else if ($scope.todo.saveLabel == '' || $scope.todo.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Save';

            } else if ($scope.todo.updateLabel == '' || $scope.todo.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Update';

            } else if ($scope.todo.cancelLabel == '' || $scope.todo.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('todolanguages', $routeParams.id).customPUT($scope.todo).then(function (resp) {
                    window.location = '/todolanguage';
                });
            }
        };


        if ($routeParams.id) {

            $scope.message = 'Todo has been updated!';

            Restangular.one('todolanguages', $routeParams.id).get().then(function (td) {
                $scope.original = td;
                $scope.todo = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Todo has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todolanguages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getmember = function (memberid) {
            return Restangular.one('members', memberid).get().$object;
        };

       $scope.getLanguage = function (language) {
            return Restangular.one('languagedefinitions/findOne?filter[where][deleteflag]=false&filter[where][id]='+language).get().$object;
        };

        $scope.gettodostatus = function (todostatusid) {
            return Restangular.one('todostatuses', todostatusid).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });
