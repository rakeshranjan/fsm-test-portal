'use strict';

angular.module('secondarySalesApp')
    .controller('Level2Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/level2/create' || $location.path() === '/level2/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level2/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level2/create' || $location.path() === '/level2/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level2/create' || $location.path() === '/level2/' + $routeParams.id;
            return visible;
        };

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            if ($routeParams.id) {
                $scope.message = leveldefinition[1].name + ' has been Updated!';
            } else {
                $scope.message = leveldefinition[1].name + ' has been Created!';
            }

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/level2") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (l1) {
            $scope.levelones = l1;
        });
    
    Restangular.all('leveltwos?filter[where][deleteflag]=false').getList().then(function (lvl2) {
            $scope.leveltwos = lvl2;

            angular.forEach($scope.leveltwos, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

    $scope.level1Id = '';

    $scope.$watch('level1Id', function (newValue, oldValue) {
        if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
            return;
        } else {
            Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (lvl2) {
                $scope.leveltwos = lvl2;

                angular.forEach($scope.leveltwos, function (member, index) {
                    member.index = index + 1;

                    $scope.TotalTodos = [];
                    $scope.TotalTodos.push(member);
                });
            });
        }
    });

        /************************ end ********************************************************/

        $scope.DisplayLeveltwos = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png'
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayLeveltwos.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png'
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLeveltwos.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLeveltwos[index].disableLang = false;
                $scope.DisplayLeveltwos[index].disableAdd = false;
                $scope.DisplayLeveltwos[index].disableRemove = false;
                $scope.DisplayLeveltwos[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayLeveltwos[index].disableLang = true;
                $scope.DisplayLeveltwos[index].disableAdd = false;
                $scope.DisplayLeveltwos[index].disableRemove = false;
                $scope.DisplayLeveltwos[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayLeveltwos[$scope.lastIndex].disableAdd = true;
            $scope.DisplayLeveltwos[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayLeveltwos[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayLeveltwos);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('leveltwos/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel2();
        };

        var saveCount = 0;

        $scope.saveLevel2 = function () {

            if (saveCount < $scope.DisplayLeveltwos.length) {

                if ($scope.DisplayLeveltwos[saveCount].name == '' || $scope.DisplayLeveltwos[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel2();
                } else {

                    $scope.DisplayLeveltwos[saveCount].leveloneid = $scope.level1;

                    Restangular.all('leveltwos').post($scope.DisplayLeveltwos[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel2();
                    });
                }

            } else {
                window.location = '/level2-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.leveltwo.name == '' || $scope.leveltwo.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter' + $scope.leveldefinitions[1].name + 'Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.leveltwo.lastmodifiedby = $window.sessionStorage.userId;
                $scope.leveltwo.lastmodifiedtime = new Date();
                $scope.leveltwo.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('leveltwos', $routeParams.id).customPUT($scope.leveltwo).then(function (resp) {
                    window.location = '/level2-list';
                });
            }
        };

        $scope.getLevel1 = function (leveloneid) {
            return Restangular.one('levelones', leveloneid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.leveltwo) {
                if ($scope.leveltwo.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                        // console.log(x);

                        if ($scope.leveltwo[key] != null) {
                            $scope.languages[mCount].lang = $scope.leveltwo[key];
                            //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.leveltwo[key] == null) {
                            // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.leveltwo.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.leveltwo["lang" + data.inx] = data.lang;
                // console.log($scope.leveldefinition);
            });

            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('leveltwos', $routeParams.id).get().then(function (leveltwo) {
                $scope.original = leveltwo;
                $scope.leveltwo = Restangular.copy($scope.original);
            });
        }

    });
