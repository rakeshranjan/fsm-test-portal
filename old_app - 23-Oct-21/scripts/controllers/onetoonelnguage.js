'use strict';

angular.module('secondarySalesApp')
    .controller('OnetoOneLanguageCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/onetoonelanguage/create' || $location.path() === '/onetoonelanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/onetoonelanguage/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/onetoonelanguage/create' || $location.path() === '/onetoonelanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/onetoonelanguage/create' || $location.path() === '/onetoonelanguage/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/onetoonelnguage" || $window.sessionStorage.prviousLocation != "partials/onetoonelnguage") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /****************************************************************************/

        $scope.onetoonelang = {
            member: '',
            followUp: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };
        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });

       $scope.languageDisable = true;

       Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
           Restangular.one("roles/"+$window.sessionStorage.roleId).get().then(function(role){
             $scope.languages = langs;
               if(role.isLanguageUser){
                    $scope.onetoonelang.language = $window.sessionStorage.language;
                   $scope.languageDisable = true;
               }else{
                   $scope.languageDisable = false;
               }
           });
           
        });


        /************************************* INDEX ***************************************************/

        Restangular.all('onetoonelanguages?filter[where][deleteflag]=false').getList().then(function (tdo) {
            $scope.onetoonelanguages = tdo;
            angular.forEach($scope.onetoonelanguages, function (member, index) {
                member.index = index + 1;

                //                $scope.TotalTodos = [];
                //                $scope.TotalTodos.push(member);
            });
        });


        $scope.validatestring = "";

        $scope.Save = function (clicked) {

            if ($scope.onetoonelang.language == '' || $scope.onetoonelang.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.onetoonelang.onetooneTitle == '' || $scope.onetoonelang.onetooneTitle == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select One to One';

            } else if ($scope.onetoonelang.memberName == '' || $scope.onetoonelang.memberName == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member Name';

            } else if ($scope.onetoonelang.memberID == '' || $scope.onetoonelang.memberID == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member Id';

            } else if ($scope.onetoonelang.lessDetails == '' || $scope.onetoonelang.lessDetails == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Less details';

            } else if ($scope.onetoonelang.history == '' || $scope.onetoonelang.history == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select History';

            } else if ($scope.onetoonelang.progress == '' || $scope.onetoonelang.progress == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Progress';

            } else if ($scope.onetoonelang.dateLabel == '' || $scope.onetoonelang.dateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Date';

            } else if ($scope.onetoonelang.question == '' || $scope.onetoonelang.question == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Question';

            } else if ($scope.onetoonelang.answer == '' || $scope.onetoonelang.answer == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Answer';

            } else if ($scope.onetoonelang.todo == '' || $scope.onetoonelang.todo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select ToDo';

            } else if ($scope.onetoonelang.saveLabel == '' || $scope.onetoonelang.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Save';

            } else if ($scope.onetoonelang.updateLabel == '' || $scope.onetoonelang.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Update';

            } else if ($scope.onetoonelang.cancelLabel == '' || $scope.onetoonelang.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.all('onetoonelanguages').post($scope.onetoonelang).then(function (resp) {
                    window.location = '/onetoonelanguage';
                });
            }
        };

        $scope.Update = function (clicked) {

            if ($scope.onetoonelang.language == '' || $scope.onetoonelang.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.onetoonelang.onetooneTitle == '' || $scope.onetoonelang.onetooneTitle == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select One to One';

            } else if ($scope.onetoonelang.memberName == '' || $scope.onetoonelang.memberName == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member Name';

            } else if ($scope.onetoonelang.memberID == '' || $scope.onetoonelang.memberID == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member Id';

            } else if ($scope.onetoonelang.lessDetails == '' || $scope.onetoonelang.lessDetails == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Less details';

            } else if ($scope.onetoonelang.history == '' || $scope.onetoonelang.history == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select History';

            } else if ($scope.onetoonelang.progress == '' || $scope.onetoonelang.progress == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Progress';

            } else if ($scope.onetoonelang.dateLabel == '' || $scope.onetoonelang.dateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Date';

            } else if ($scope.onetoonelang.question == '' || $scope.onetoonelang.question == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Question';

            } else if ($scope.onetoonelang.answer == '' || $scope.onetoonelang.answer == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Answer';

            } else if ($scope.onetoonelang.todo == '' || $scope.onetoonelang.todo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select ToDo';

            } else if ($scope.onetoonelang.saveLabel == '' || $scope.onetoonelang.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Save';

            } else if ($scope.onetoonelang.updateLabel == '' || $scope.onetoonelang.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Update';

            } else if ($scope.onetoonelang.cancelLabel == '' || $scope.onetoonelang.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('onetoonelanguages', $routeParams.id).customPUT($scope.onetoonelang).then(function (resp) {
                    window.location = '/onetoonelanguage';
                });
            }

        };


        if ($routeParams.id) {

            $scope.message = 'One to One has been updated!';

            Restangular.one('onetoonelanguages', $routeParams.id).get().then(function (td) {
                $scope.original = td;
                $scope.onetoonelang = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'One to One has been created!';
        }



        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('onetoonelanguages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


        $scope.getLanguage = function (language) {
            return Restangular.one('languagedefinitions/findOne?filter[where][deleteflag]=false&filter[where][id]='+language).get().$object;
        };


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });
