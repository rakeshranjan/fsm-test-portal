'use strict';

angular.module('secondarySalesApp')
    .controller('LevelsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/level/create' || $location.path() === '/level/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level/create' || $location.path() === '/level/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level/create' || $location.path() === '/level/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/cities") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        $scope.DisplayLevels = [{
            name: '',
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        }];

        $scope.Add = function (index) {

            var idVal = index + 1;

            if (idVal > 9) {
                return;
            } else {
                $scope.DisplayLevels.push({
                    name: '',
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId
                });
            }
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLevels.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLevels[index].disableAdd = false;
                $scope.DisplayLevels[index].disableRemove = false;
            } else {
                $scope.DisplayLevels[index].disableAdd = true;
                $scope.DisplayLevels[index].disableRemove = true;
            }
        };

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('levels/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        /********************************************** WATCH ***************************************/

        Restangular.all('levels?filter[where][deleteflag]=false').getList().then(function (lvl) {
            $scope.levels = lvl;
            angular.forEach($scope.levels, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel();
        };

        var saveCount = 0;

        $scope.saveLevel = function () {

            if (saveCount < $scope.DisplayLevels.length) {

                if ($scope.DisplayLevels[saveCount].name == '' || $scope.DisplayLevels[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel();
                } else {
                    Restangular.all('levels').post($scope.DisplayLevels[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel();
                    });
                }

            } else {
                window.location = '/levels-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.level.name == '' || $scope.level.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Level';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.level.lastmodifiedby = $window.sessionStorage.userId;
                $scope.level.lastmodifiedtime = new Date();
                $scope.level.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('levels', $routeParams.id).customPUT($scope.level).then(function (resp) {
                    window.location = '/levels-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Level has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('levels', $routeParams.id).get().then(function (level) {
                $scope.original = level;
                $scope.level = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Level has been created!';
        }

    });
