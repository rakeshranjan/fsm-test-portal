'use strict';

angular.module('secondarySalesApp')

.controller('ResetPswdCtrl', function ($rootScope, $scope, $http, $window, $location, Restangular, $idle, $modal) {
    $scope.user = {
        username: $window.sessionStorage.userName
    };
    $scope.login = function () {
        Restangular.all('users').login($scope.user).then(function (loginResult) {
            console.log('loginResult', loginResult);
        }).then(function () {
            if ($scope.user.newpassword === $scope.user.confirmnewpassword) {
                Restangular.all('users?filter[where][username]=' + $scope.user.username).getList().then(function (detail) {
                    if (detail.length > 0) {
                        $scope.userId = detail[0].id;

                        $scope.newdata = {
                            password: $scope.user.newpassword
                        };

                        Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
                            console.log('response', response);
                            $scope.logout();
                        });
                    }
                });
            } else {
                $scope.errormsg = 'Passwords Doesnt Match';
            }
        }, function (response) {
            console.log('response', response);

            $scope.errormsg = 'Invalid Password';

        });
    };
    $scope.logout = function () {
        //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
        Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
            $window.sessionStorage.userId = '';
            console.log('Logout');
        }).then(function (redirect) {

            $location.path("/login");
            $idle.unwatch();

        });
    };

});