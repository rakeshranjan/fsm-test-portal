'use strict';

angular.module('secondarySalesApp')
	.controller('CitiesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/town/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
			return visible;
		};
		/************************************************************************************/
		if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
			$window.sessionStorage.facility_zoneId = null;
			$window.sessionStorage.facility_stateId = null;
			$window.sessionStorage.facility_currentPage = 1;
			$window.sessionStorage.facility_currentPageSize = 25;
		} else {
			$scope.countryId = $window.sessionStorage.facility_zoneId;
			$scope.stateId = $window.sessionStorage.facility_stateId;
			$scope.currentpage = $window.sessionStorage.facility_currentPage;
			$scope.pageSize = $window.sessionStorage.facility_currentPageSize;
		}


		if ($window.sessionStorage.prviousLocation != "partials/cities") {
			$window.sessionStorage.facility_zoneId = '';
			$window.sessionStorage.facility_stateId = '';
			$window.sessionStorage.facility_currentPage = 1;
			$scope.currentpage = 1;
			$window.sessionStorage.facility_currentPageSize = 25;
			$scope.pageSize = 25;
		}

		$scope.pageSize = $window.sessionStorage.facility_currentPageSize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.facility_currentPageSize = mypage;
		};

		$scope.currentpage = $window.sessionStorage.facility_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.facility_currentPage = newPage;
		};

/************************************************/


		$scope.countryId = '';
		$scope.stateId = '';
		$scope.statesid = '';
		$scope.countiesid = '';


		$scope.$watch('countryId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$window.sessionStorage.facility_zoneId = newValue;
				$scope.displaysalesareas12 = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
					$scope.displaysalesareas = responceSt;
					$scope.stateId = $window.sessionStorage.facility_stateId;
				});

				$scope.cities1 = Restangular.all('cities?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
					$scope.DisplayCities = ctyRes1;
					angular.forEach($scope.DisplayCities, function (member, index) {
						member.index = index + 1;
					});
				});
				$scope.countiesid = +newValue;
			}
		});

		$scope.$watch('stateId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$window.sessionStorage.facility_stateId = newValue;
				$scope.cities2 = Restangular.all('cities?filter[where][district]=' + newValue + '&filter[where][state]=' + $scope.countiesid + '&filter[where][deleteflag]=false').getList().then(function (ctyRes) {
					$scope.DisplayCities = ctyRes;
					angular.forEach($scope.DisplayCities, function (member, index) {
						member.index = index + 1;
					});
				});
			}
		});

		$scope.DisplayCities = [];
		/**********************************************************************************/
		$scope.statedisable = false;
		$scope.districdisable = false;
		$scope.searchCity = $scope.name;
		$scope.getSalesArea = function (salesareaId) {
			return Restangular.one('sales-areas', salesareaId).get().$object;
		};

		$scope.getZone = function (zoneId) {
			return Restangular.one('zones', zoneId).get().$object;
		};



		/*************************************** DELETE *******************************/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('cities/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


		/********************************************** WATCH ***************************************/
		$scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.countryId = $window.sessionStorage.facility_zoneId;
		});

		$scope.$watch('city.state', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
				return;
			} else {
				$scope.salesareas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
			}
		});


		/****************************************** CREATE *********************************/

		$scope.city = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		};

		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.city.name == '' || $scope.city.name == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			} else if ($scope.city.state == '' || $scope.city.state == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter State';
			} else if ($scope.city.district == '' || $scope.city.district == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter District';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('cities').post($scope.city).then(function () {
					window.location = '/cities';
				});
			}
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/******************************************************** UPDATE ************************/
		$scope.modalTitle = 'Thank You';
		if ($routeParams.id) {
			$scope.statedisable = true;
			$scope.districdisable = true;
			$scope.message = 'City has been Updated!';
			Restangular.one('cities', $routeParams.id).get().then(function (city) {
				$scope.original = city;
				$scope.salearea = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.state + '&filter[where][deleteflag]=false').getList().then(function (salearea) {
					$scope.salesareas = salearea;
					$scope.city = Restangular.copy($scope.original);
				});
			});
		} else {
			$scope.message = 'City has been Created!';
		}


		/*
				$scope.validatestring = '';
				$scope.Update = function () {
					delete $scope.city.district;
					delete $scope.city.facility;
					delete $scope.city.site;
					delete $scope.city.state;
					$scope.submitcities.customPUT($scope.city).then(function (updateRes) {
						console.log('$scope.city', updateRes);
						window.location = '/cities';
					}, function (error) {
						console.log('error', error);
					});
				};*/
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.city.name === '' || $scope.city.name === null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.submitDisable = true;
				delete $scope.city.district;
				delete $scope.city.facility;
				delete $scope.city.site;
				delete $scope.city.state;
				Restangular.one('cities', $routeParams.id).customPUT($scope.city).then(function () {
					//$location.path('/cities');
					window.location = '/cities';
				});
			}
		};



	});
