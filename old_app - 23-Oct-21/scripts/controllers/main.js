'use strict';
angular.module('secondarySalesApp').controller('MainCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route) {

  $scope.rolesconfiguration1 = true;
  console.log("Here-In Main js-Inside")
  Restangular.one('users', $window.sessionStorage.userId).get().then(function (user) {
    $scope.username = user.username;
  });

  $scope.analyticsRedirect = function (path) {
    if ($scope.showDashboard) {
      $location.path(path);
    }
  };

  $scope.getMember = function (memberId) {
    return Restangular.one('members', memberId).get().$object;
  };
  $scope.getTodotype = function (todotypeId) {
    return Restangular.one('todotypes', todotypeId).get().$object;
  };
  $scope.getTodoStatus = function (todostatusId) {
    return Restangular.one('todostatuses', todostatusId).get().$object;
  };

  $scope.inProgressCount = 0;
  $scope.readyToAssignCount = 0;
  $scope.showDashboard = false;
  if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
    // $scope.membertodoUrl = 'membertodoview?filter[where][orgtructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=0';
    $scope.membertodoUrl = 'rcdetailtodoview?filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=0';
    $scope.showDashboard = true;
  } else if ($window.sessionStorage.roleId === '5') {
    // $scope.membertodoUrl = 'membertodoview?filter[where][orgtructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=0';
    $scope.membertodoUrl = 'rcdetailtodoview?filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=0';
    $scope.showDashboard = true;
  } else {
    // $scope.membertodoUrl = 'membertodoview?filter[where][orgtructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=' + $window.sessionStorage.userId;
    $scope.membertodoUrl = 'rcdetailtodoview?filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignTo]=' + $window.sessionStorage.userId;
    $scope.showDashboard = false;
  }
  Restangular.all($scope.membertodoUrl).getList().then(function (tdo) {
    $scope.membertodo = tdo;
    angular.forEach($scope.membertodo, function (todo, index) {
      if (+todo.todoCount === 0 && todo.assignTo === 0) {
        $scope.readyToAssignCount++;
      } else {
        $scope.inProgressCount++;
      }
    });
  });

  $scope.openTodoCount = 0;
  $scope.progressTodoCount = 0;
  if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
    $scope.todoUrl = 'todos?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignedto]=0';
  } else if ($window.sessionStorage.roleId === '5') {
    $scope.todoUrl = 'todos?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignedto]=0';
  } else {
    $scope.todoUrl = 'todos?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][assignedto]=' + $window.sessionStorage.userId;
  }
  Restangular.all($scope.todoUrl).getList().then(function (tdo) {
    $scope.todos = tdo;
    angular.forEach($scope.todos, function (todo, index) {
      if (todo.todostatusid === 1 && todo.assignedto === 0) {
        $scope.openTodoCount++;
      } else {
        $scope.progressTodoCount++;
      }
    });
  });

  $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    $scope.CountrySplit = $scope.orgStructureSplit[0];
    $scope.ZoneSplit = $scope.orgStructureSplit[1];
  $scope.intermediateOrgStructure = "";
  for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
    if ($scope.intermediateOrgStructure === "") {
      $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
    } else {
      $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
    }
  }

  $scope.orgStructureFilterArray = [];
  if ($scope.orgStructureSplit.length > 0) {
    $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
    for (var i = 0; i < $scope.LocationsSplit.length; i++) {
      $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
    }

  }
  $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
  console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));

  //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
  $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
  console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
  //--- End of No need now - It was for single state & multi city ---------

  //-- For Multi Zone, state & multi City array filter work Ravi ---------------------------------
  $scope.orgStructureZoneFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
    for (var i = 0; i < $scope.ZoneSplit.length; i++) {
      $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.ZoneSplit[i]);
    }
  }

  //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
  $scope.orgStructureStateFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
  //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    console.log($scope.StatesSplit, "$scope.StatesSplit-461")
   // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
   for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
    for (var i = 0; i < $scope.StatesSplit.length; i++) {
      // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
      $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.StatesSplit[i]);
    }
   }
  }

  //-- For Multi state & multi City array filter work Satyen ---------------------------------
  // $scope.orgStructureStateFilterArray = [];
  // if ($scope.orgStructureSplit.length > 0) {
  //   if ($scope.orgStructureSplit.length > 1) {
  //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
  // //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
  //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
  //  // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
  //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
  //     // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
  //     $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
  //   }
  // }

  
  //console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-468")
  $scope.orgStructureFinalFilterArray = [];
  if ($scope.orgStructureStateFilterArray.length > 0) {
    for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
      for (var i = 0; i < $scope.LocationsSplit.length; i++) {
        $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
      }
    }
  }
  //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
  $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", "");
  //console.log("$scope.orgStructureFinalFilterArray", JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", ""));

  //-- End of For Multi state & multi City array filter work Satyen ---------------------------------

  Restangular.all('members?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (member) {
    $scope.members = member;
    console.log($scope.members, "MEMBERS-66")
    angular.forEach($scope.members, function (member, index) {
      if (member.answeredQuestion != undefined && member.answeredQuestion != null && member.answeredQuestion != '' && member.answeredQuestion != 0) {
        //$scope.inProgressCount++;
      }
    });
  });

  //    document.getElementById('disablebulk').style.background = "none";
  document.getElementById('disablebulk').style.cursor = "not-allowed";
  $scope.HideMenu = true;
  //It was from previously - saty
  if ($window.sessionStorage.roleId == 5) {
    $scope.HideMenu = false;
    $scope.partners1 = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (resPartner1) {
      $scope.partners = resPartner1;
      angular.forEach($scope.partners, function (member, index) {
        member.index = index + 1;
      });
    });
  }
  $scope.emps = Restangular.all('employees').getList().then(function (emps) {
    $scope.employees = emps;
    $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {
      $scope.maintodotypes = todotyp;
    });
  });

  console.log('$window.sessionStorage.roleId', $window.sessionStorage.roleId);
  if ($window.sessionStorage.roleId != 1) {
    //$scope.Main_modalInstanceLoad.close();
    $scope.main_toggleLoading = function () {
      $scope.Main_modalInstanceLoad = $modal.open({
        animation: true,
        templateUrl: 'template/MainLodingModal.html',
        scope: $scope,
        backdrop: 'static',
        size: 'sm'
      });
    };
  }
  ////////////////////////////////Count/////////////////
  // if ($window.sessionStorage.roleId == 1) {
  if ($window.sessionStorage.roleId == 1) {
    Restangular.all('ticket_status_role_view?[deleteflag]=false').getList().then(function (piegresp) {
      $scope.piegresp = piegresp;
      console.log("asdf", piegresp);
      //console.log("token", $window.sessionStorage.accessToken);
      $scope.statusCount(piegresp);
    });
  } 
  else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {
    // Restangular.all('ticket_status_role_view?[deleteflag]=false').getList().then(function (piegresp) {
    Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId).getList().then(function (piegresp) {
      $scope.piegresp = piegresp;
      console.log("asdf-213", piegresp);
      //console.log("token", $window.sessionStorage.accessToken);
      $scope.statusCount(piegresp);
    });
  } 
  else if ($window.sessionStorage.roleId == 8) {
    Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (piegresp) {
      $scope.piegresp = piegresp;
      console.log("asdf", piegresp);
      //console.log("token", $window.sessionStorage.accessToken);
      $scope.statusCount(piegresp);
    });
  } 
  else if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
    // from here new code ---
    var str = $scope.orgStructureFilter;
    //console.log('orgStructureFilter', $scope.orgStructureFilter);
    var strOrgstruct = str.split('"');

    $scope.strOrgstructFinal = [];
    for (var x = 0; x < strOrgstruct.length; x++) {
      if (x % 2 != 0) {
        $scope.strOrgstructFinal.push(strOrgstruct[x]);
      }
    }
    //console.log('OrsgtructFinal:', $scope.strOrgstructFinal);
    $scope.membersFinal = [];
    for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
      // Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
      //   .getList().then(function (data3) {
      Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%&access_token=' + window.sessionStorage.accessToken)
        .getList().then(function (data3) {
          $scope.membersnew = data3;
          if ($scope.membersnew.length > 0) {
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.membersFinal.push(value);
            });
          }
          $scope.membersdata = $scope.membersFinal;
          console.log("datas", $scope.membersdata);
          $scope.statusCount($scope.membersdata);

        });
    }
  } else if ($window.sessionStorage.roleId == 5) {

    var str = $scope.orgStructureUptoStateFilter;
    var strOrgstruct = str.split('"')
    $scope.strOrgstructFinal = [];
    for (var x = 0; x < strOrgstruct.length; x++) {
      if (x % 2 != 0) {
        $scope.strOrgstructFinal.push(strOrgstruct[x]);
      }
    }
    $scope.membersFinal = [];
    for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
      // Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken)
      //   .getList().then(function (data3) {
      Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%&access_token=' + window.sessionStorage.accessToken)
        .getList().then(function (data3) {
          $scope.membersnew = data3;
          if ($scope.membersnew.length > 0) {
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.membersFinal.push(value);
            });
          }
          $scope.membersdata = $scope.membersFinal;
          console.log("datas", $scope.membersdata);
          $scope.statusCount($scope.membersdata);

        });
    }
  } else if ($window.sessionStorage.roleId == 11) {
    Restangular.all('ticket_status_role_view?filter[where][assignFlag]=true&filter[where][assignedto]=' + window.sessionStorage.userId + '&access_token=' + window.sessionStorage.accessToken)
      .getList().then(function (data4) {

        $scope.statusCount(data4);
        console.log("dat", data4);
      });
  }

  $scope.statusCount = function (piegresp) {
    console.log("piegresp-295", piegresp)
    if (piegresp.length == 0) {
      $scope.all = {
        op: 0,
        ba: 0,
        as: 0,
        in: 0,
        co: 0,
        ap: 0
      };
    } else {
      var Gtotal_open_count = 0;
      var Gtotal_backoffice_count = 0;
      var Gtotal_assign_count = 0;
      var Gtotal_inprogress_count = 0;
      var Gtotal_completed_count = 0;
      var Gtotal_approval_pending_count = 0;

      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 1) {
          Gtotal_open_count = Gtotal_open_count + piegresp[i].total_open_count;

        }
        var count1 = Gtotal_open_count;

        console.log("count", count1);
      }

      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 2) {
          Gtotal_backoffice_count = Gtotal_backoffice_count + piegresp[i].total_backoffice_count;

        }

        var count2 = Gtotal_backoffice_count;

        console.log("count2", count2);

      }

      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 3) {
          Gtotal_assign_count = Gtotal_assign_count + piegresp[i].total_assign_count;

        }
        var count3 = Gtotal_assign_count;

        console.log("count3", count3);

      }

      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 4) {
          Gtotal_inprogress_count = Gtotal_inprogress_count + piegresp[i].total_inprogress_count;

        }
        var count4 = Gtotal_inprogress_count;

        console.log("count4", count4);
      }
      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 5) {
          Gtotal_completed_count = Gtotal_completed_count + piegresp[i].total_completed_count;

        }
        var count5 = Gtotal_completed_count;

        console.log("count5", count5);
      }
      for (var i = 0; i < piegresp.length; i++) {
        if (piegresp[i].todostatusId == 6) {
          Gtotal_approval_pending_count = Gtotal_approval_pending_count + piegresp[i].total_approval_pending_count;

        }
        var count6 = Gtotal_approval_pending_count;

      }
      $scope.countall = [count1, count2, count3, count4, count5, count6];
      console.log("countall", $scope.countall);
      $scope.all = {
        op: $scope.countall[0],
        ba: $scope.countall[1],
        as: $scope.countall[2],
        in: $scope.countall[3],
        co: $scope.countall[4],
        ap: $scope.countall[5]
      };
      console.log("all-381", $scope.all);
    }
  }
  /********************************Fieldworker Todo *************************/
  /********************************** FWorkpillarId***********************
  		$scope.$watch('FWorkpillarId', function (newValue, oldValue) {
  			if (newValue === oldValue || newValue == '' || newValue == undefined) {
  				return;
  			} else {
  				$scope.TotalTodos = [];
  				if (newValue != 2) {
  					$scope.displayreportincidents = [];
  					$scope.RemainingPillars = 5;
  					$scope.zn = Restangular.all('todos?filter[where][pillarid]=' + newValue + '&filter[order]=datetime%20DESC&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=false&filter[where][roleId]=6').getList().then(function (zn) {
  						$scope.FWorktodos = zn;
  						angular.forEach($scope.FWorktodos, function (member, index) {
  							member.index = index + 1;
  							member.backgroungclr = '#000000';
  							for (var m = 0; m < $scope.partners.length; m++) {
  								if (member.beneficiaryid == $scope.partners[m].id) {
  									member.Membername = $scope.partners[m];
  									if ($scope.partners[m].deleteflag == true) {
  										member.backgroungclr = '#D1160A';
  									} else {
  										member.backgroungclr = '#000000';
  									}
  									break;
  								}
  							}

  							for (var n = 0; n < $scope.fieldworkers.length; n++) {
  								if (member.lastmodifiedby == $scope.fieldworkers[n].id) {
  									member.FieldWorkerName = $scope.fieldworkers[n];
  									break;
  								}
  							}

  							for (var o = 0; o < $scope.maintodotypes.length; o++) {
  								if (member.todotype == $scope.maintodotypes[o].id) {
  									member.ActionType = $scope.maintodotypes[o];
  									break;
  								}
  							}



  							member.datetime = member.datetime;
  							$scope.FWorkTotalTodos.push(member);

  						});
  					});
  				} else {
  					$scope.FWorktodos = [];
  					$scope.FWorkTotalTodos = [];
  					$scope.SSJPillar = 5;
  					$scope.rprt = Restangular.all('reportincidents?filter[where][deleteflag]=false&filter[where][lastmodifiedbyrole]=6' + '&filter[order]=followupdate%20DESC&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][currentstatus][like]=Needs%').getList().then(function (report) {
  						$scope.displayreportincidents = report;
  						console.log('report', report);
  						angular.forEach($scope.displayreportincidents, function (member, index) {
  							member.index = index + 1;
  							member.pillarid = 2;
  							member.todotype = 27;
  							member.datetime = member.followupdate;
  							member.backgroungclr = '#000000';
  							for (var m = 0; m < $scope.partners.length; m++) {
  								if (member.beneficiaryid == $scope.partners[m].id) {
  									member.Membername = $scope.partners[m];
  									if ($scope.partners[m].deleteflag == true) {
  										member.backgroungclr = '#D1160A';
  									} else {
  										member.backgroungclr = '#000000';
  									}
  									break;
  								}
  							}

  							for (var n = 0; n < $scope.fieldworkers.length; n++) {
  								if (member.lastmodifiedby == $scope.fieldworkers[n].id) {
  									member.FieldWorkerName = $scope.fieldworkers[n];
  									break;
  								}
  							}

  							for (var o = 0; o < $scope.maintodotypes.length; o++) {
  								if (member.todotype == $scope.maintodotypes[o].id) {
  									member.ActionType = $scope.maintodotypes[o];
  									break;
  								}
  							}
  							member.datetime = member.datetime;
  							$scope.FWorkTotalTodos.push(member);
  						});
  					});

  				}
  			}
  		});
  */
  /********************** Language ********************/
  $scope.UserLanguage = $window.sessionStorage.language;
  $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
    $rootScope.style = {
      "font-size": langResponse.fontsize,
      "font-weight": "normal"
    }
    console.log('langResponse', langResponse.searchfor);
    $scope.printheaderreportincident = langResponse.headerreportincident;
    $scope.applyforscheme = langResponse.applyforscheme;
    $scope.applyfordocument = langResponse.applyfordocument;
    $scope.bulkupdateheader = langResponse.bulkupdateheader;
    $scope.eventheader = langResponse.eventheader;
    $scope.headergroupmeeting = langResponse.headergroupmeeting;
    $scope.stakeholdermeeting = langResponse.stakeholdermeeting;
    $scope.pillar = langResponse.pillar;
    $scope.status = langResponse.status;
    $scope.printmembername = langResponse.membername;
    $scope.nickname = langResponse.nickname;
    $scope.facility = langResponse.facility;
    $scope.site = langResponse.site;
    $scope.actiontype = langResponse.actiontype;
    $scope.followupdate = langResponse.followupdate;
    $scope.action = langResponse.action;
    $scope.searchfor = langResponse.searchfor;
    $scope.hotspot = langResponse.hotspot;
    $scope.welcome = langResponse.welcome;
    $scope.newmember = langResponse.newmember;
    $scope.withmember = langResponse.withmember;
    $scope.mytodos = langResponse.mytodos;
    $scope.broadcastmessages = langResponse.broadcastmessages;
    $scope.nobroadcastmessages = langResponse.nobroadcastmessages;
    $scope.printonetoneheader = langResponse.onetoneheader;
    $scope.followup = langResponse.followup;
    $scope.followuprequired = langResponse.followuprequired;
    $scope.selectstatus = langResponse.selectstatus;
    $scope.followupdate = langResponse.followupdate;
    $scope.create = langResponse.create;
    $scope.update = langResponse.update;
    $scope.cancel = langResponse.cancel;
    $scope.savebutton = langResponse.savebutton;
    $scope.okbutton = langResponse.ok;
    $scope.addbutton = langResponse.addbutton;
    $scope.printdocument = langResponse.document;
    $scope.printscheme = langResponse.scheme;
    $scope.schemedocument = langResponse.schemedocument;
    $scope.schemeordocument = langResponse.schemeordocument;
    $scope.printstage = langResponse.stage;
    $scope.printresponsereceive = langResponse.responsereceive;
    $scope.printresponserejection = langResponse.responserejection;
    $scope.printresponsedelay = langResponse.responsedelay;
    $scope.tododisplay = langResponse.tododisplay;
    $scope.todosprint = langResponse.todosprint;
    $scope.name = langResponse.name;
    $scope.phonenumber = langResponse.phonenumber;
    //Report Incident
    $scope.whendidhappen = langResponse.whendidhappen;
    $scope.multiplepeopleeffect = langResponse.multiplepeopleeffect;
    $scope.printyes = langResponse.yes;
    $scope.printno = langResponse.no;
    $scope.membername = langResponse.membername;
    $scope.member = langResponse.member;
    $scope.divincidenttype = langResponse.divincidenttype;
    $scope.printphysical = langResponse.physical;
    $scope.sexual = langResponse.sexual;
    $scope.chieldrelated = langResponse.chieldrelated;
    $scope.emotional = langResponse.emotional;
    $scope.propertyrelated = langResponse.propertyrelated;
    $scope.severityofincident = langResponse.severityofincident;
    $scope.divperpetratordetail = langResponse.divperpetratordetail;
    $scope.police = langResponse.police;
    $scope.client = langResponse.client;
    $scope.serviceprovider = langResponse.serviceprovider;
    $scope.otherkp = langResponse.otherkp;
    $scope.neighbour = langResponse.neighbour;
    $scope.goons = langResponse.goons;
    $scope.printpartners = langResponse.partners;
    $scope.familiymember = langResponse.familiymember;
    $scope.othersexkp = langResponse.othersexkp;
    $scope.divreported = langResponse.divreported;
    $scope.reportedto = langResponse.reportedto;
    $scope.ngos = langResponse.ngos;
    $scope.friends = langResponse.friends;
    $scope.plv = langResponse.plv;
    $scope.coteam = langResponse.coteam;
    $scope.champion = langResponse.champion;
    $scope.otherkps = langResponse.otherkps;
    $scope.legalaidclinic = langResponse.legalaidclinic;
    $scope.divtimetorespond = langResponse.divtimetorespond;
    $scope.refferedcounselling = langResponse.refferedcounselling;
    $scope.refferedmedicalcare = langResponse.refferedmedicalcare;
    $scope.refferedcomanager = langResponse.refferedcomanager;
    $scope.refferedplv = langResponse.refferedplv;
    $scope.refferedaidclinic = langResponse.refferedaidclinic;
    $scope.refferedboardmember = langResponse.refferedboardmember;
    $scope.followupdate = langResponse.followupdate;
    $scope.printdateofclosure = langResponse.dateofclosure;
    $scope.divactiontaken = langResponse.divactiontaken;
    $scope.twohrs = langResponse.twohrs;
    $scope.twototwentyfourhrs = langResponse.twototwentyfourhrs;
    $scope.greatertwentyfourhrs = langResponse.greatertwentyfourhrs;
    $scope.currentstatuscase = langResponse.currentstatuscase;
    $scope.followuprequired = langResponse.followuprequired;
    $scope.printschemename = langResponse.schemename;
    $scope.printschemelocalname = langResponse.schemelocalname;
    $scope.printcategory = langResponse.category;
    $scope.printagegroup = langResponse.agegroup;
    $scope.printgender = langResponse.gender;
    $scope.printselect = langResponse.select;
    $scope.printselecttodos = langResponse.selecttodos;
    $scope.printreferenceno = langResponse.referenceno;
    $scope.printdate = langResponse.date;
    $scope.printname = langResponse.name;
    $scope.printreferencenumber = langResponse.referencenumber;
    $scope.monthtimeavailed = langResponse.monthtimeavailed;
    $scope.printpillar = langResponse.pillar;
    $scope.printnoofcondomask = langResponse.noofcondomask;
    $scope.printdateprovided = langResponse.dateprovided;
    $scope.printsaving = langResponse.saving;
    $scope.printinsurance = langResponse.insurance;
    $scope.printpension = langResponse.pension;
    $scope.printdateoflastsaving = langResponse.dateoflastsaving;
    $scope.printstate = langResponse.state;
    $scope.printmemberattendmeeting = langResponse.memberattendmeeting;
    $scope.printorganizedby = langResponse.organizedby;
    $scope.printothers = langResponse.others;
    $scope.printtopics = langResponse.topics;
    $scope.PrintOthers = langResponse.others;
    $scope.PrintHusband = langResponse.husband;
    $scope.Printfwtodos = langResponse.fwtodos;
    $scope.stakeholdertype = langResponse.stakeholdertype;
    $scope.purposeofmeeting = langResponse.purposeofmeeting;
    $scope.purposeofloan = langResponse.purposeofloan;
    $scope.amounttaken = langResponse.amounttaken;
    $scope.noofmonthrepay = langResponse.noofmonthrepay;
    $scope.preset = langResponse.reset;
    $scope.palert = langResponse.alert;
    $scope.pnotapplicable = langResponse.notapplicable;
    $scope.printavahanid = langResponse.avahanid;
    $scope.optactiontaken = langResponse.optactiontaken;
    $scope.optreportedto = langResponse.optreportedto;
    $scope.optpreperator = langResponse.optpreperator;
    $scope.optincidenttype = langResponse.optincidenttype;
    $scope.pnoactionreqmember = langResponse.noactionreqmember;
    $scope.noactiontaken = langResponse.noactiontaken;
  });
  $scope.todo = {
    status: 1,
    stateid: $window.sessionStorage.zoneId,
    state: $window.sessionStorage.zoneId,
    districtid: $window.sessionStorage.salesAreaId,
    district: $window.sessionStorage.salesAreaId,
    coid: $window.sessionStorage.coorgId,
    facility: $window.sessionStorage.coorgId,
    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
    lastmodifiedtime: new Date()
  };
  var sevendays = new Date();
  sevendays.setDate(sevendays.getDate() + 7);
  $scope.todo.datetime = sevendays;
  $scope.fs = {};
  $scope.health = {};
  $scope.sp = {};
  $scope.reportincident = {
    // "referredby":"false",
    "physical": "false",
    "emotional": "false",
    "sexual": "false",
    "propertyrelated": "false",
    "childrelated": "false", //"other": false,
    "police": "false",
    "goons": "false",
    "clients": "false",
    "partners": "false",
    "husband": "false",
    "serviceprovider": "false",
    "familymember": "false",
    "otherkp": "false",
    "perpetratorother": "false",
    "neighbour": "false",
    "authorities": "false",
    "co": "false",
    "timetorespond": "false",
    "severity": "Moderate",
    "resolved": "false",
    "reported": "false",
    "reportedpolice": "false",
    "reportedngos": "false",
    "reportedfriends": "false",
    "reportedplv": "false",
    "reportedcoteam": "false",
    "reportedchampions": "false",
    "reportedotherkp": "false",
    "reportedlegalaid": "false",
    "referredcouncelling": "false",
    "referredmedicalcare": "false",
    "referredcomanager": "false",
    "referredplv": "false",
    "referredlegalaid": "false",
    "referredboardmember": "false",
    "noactionreqmember": "false",
    "noactiontaken": "false",
    "multiplemembers": ['1', '2'],
    "stateid": $window.sessionStorage.zoneId,
    "state": $window.sessionStorage.zoneId,
    "districtid": $window.sessionStorage.salesAreaId,
    "district": $window.sessionStorage.salesAreaId,
    "coid": $window.sessionStorage.coorgId,
    "facility": $window.sessionStorage.coorgId,
    "beneficiaryid": null,
    "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
    "lastmodifiedtime": new Date(),
    "lastmodifiedbyrole": $window.sessionStorage.roleId
  };
  $scope.follow = {};
  $scope.picker = {};
  $scope.cdids = {};
  $scope.schememaster = {};
  $scope.plhiv = {};
  $scope.condom = {};
  $scope.finliteracy = {};
  $scope.finplanning = {};
  $scope.increment = {};
  $scope.beneficiarycondom = {};
  $scope.RemainingPillars = 3;
  $scope.SSJPillar = 2;
  $scope.TotalTodos = [];
  $scope.FWorkTotalTodos = [];
  //////////////////////////////////////////////////////////////////////////////////////////
  //$scope.pillars = Restangular.one('pillars').getList().$object;
  $scope.submitsurveyanswers = Restangular.all('surveyanswers');
  $scope.submittodos = Restangular.all('todos');
  $scope.submitreportincidents = Restangular.all('reportincidents');
  $scope.surveyanswer = {};
  $scope.beneficiaryupdate = {};
  $scope.todostatuses = Restangular.all('todostatuses').getList().$object;
  $scope.sourceofinfections = Restangular.all('sourceofinfections').getList().$object;

  $scope.schemestages = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().$object;
  $scope.financialgoals = Restangular.all('financialgoals').getList().$object;
  //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay').getList().$object;
  $scope.responcedreceived = Restangular.all('responcedreceived').getList().$object;
  $scope.reasonforrejections = Restangular.all('reasonforrejections').getList().$object;
  $scope.reasonfordelayed = Restangular.all('reasonfordelayed').getList().$object;
  $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
  $scope.submitdocumentmasters = Restangular.all('schememasters');

  $scope.DisableEvent = true;
  $scope.DisableOptions = true;
  $scope.DisableFollowupRI = true;
  $scope.hideSP = true;
  $scope.hideSSJ = true;
  $scope.hideFS = true;
  $scope.hideIDS = true;
  $scope.hideRejection = true;
  $scope.hideRejectRejection = true;
  $scope.hideVulnerability_Index = true;
  $scope.hideMember_Not_Met = true;
  $scope.hideDue_Overdue = true;
  $scope.hideHealth = true;
  if ($window.sessionStorage.roleId == 6) {
    $scope.DisableEvent = "";
    $scope.DisableOptions = true;
    document.getElementById('disableme').style.background = "#ccc";
    document.getElementById('disableme').style.cursor = "not-allowed";
    $scope.DashPillarID = 'due_overdue';
    $scope.hideDue_Overdue = false;
  } else {
    $scope.DisableEvent = "/events";
    $scope.DisableOptions = false;
    document.getElementById('disableme').style.background = "";
    document.getElementById('disableme').style.cursor = "";
    $scope.DashPillarID = 'health';
    $scope.hideHealth = false;
  }
  //console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
  //console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
  //console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
  //console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
  //console.log('$window.sessionStorage.Site', $window.sessionStorage.roleId);
  //$scope.loginRoleId = $window.sessionStorage.roleId;
  /******************************* Welcome User Name *************************/
  Restangular.one('users', $window.sessionStorage.userId).get().then(function (user) {
    if ($window.sessionStorage.roleId == 5) {
      $scope.user = Restangular.one('comembers', user.employeeid).get().$object;
      //console.log('$scope.user', user.employeeid);
    } else if ($window.sessionStorage.roleId == 6) {
      $scope.user = Restangular.one('fieldworkers', user.employeeid).get().$object;
    }
  });
  /********************************************************* INDEX *******************************************/
  $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
  $scope.stakeholdertypes = Restangular.all('stakeholdertypes').getList().$object;
  $scope.events = Restangular.all('events?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][deleteflag]=false').getList().$object;
  $scope.purposeofmeetings = Restangular.all('purposeofmeetings').getList().$object;
  //$scope.todopurposes = Restangular.all('todopurposes').getList().$object;
  $scope.submittodo = Restangular.all('todos');
  $scope.searchbulkupdate = '';

  //$scope.statusDate = 'due';
  $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');

  /********************/
  $scope.currentPage = 1;
  $scope.pageSize = 20;
  $scope.todos = [];
  $scope.FWorktodos = [];
  //It was from previously - saty
  if ($window.sessionStorage.roleId == 5) {
    $scope.eventhide = false;
  } else {
    $scope.eventhide = true;
  }
  //$scope.loading2 = true;
  $scope.getSite = function (site) {
    return Restangular.one('distribution-routes', site).get().$object;
  };
  $scope.getCoMember = function (site) {
    return Restangular.one('employees', site).get().$object;
  };
  $scope.getMemberName = function (beneficiaryid) {
    return Restangular.one('beneficiaries', beneficiaryid).get().$object;
  };
  $scope.getActionType = function (followuprequired) {
    return Restangular.one('todotypes', followuprequired).get().$object;
  };
  $scope.showfollowupModal = false;
  $scope.toggleschemesModal = function (id) {
    Restangular.one('todos/' + id).get().then(function (event) {
      ////console.log('event', event);
      $scope.original = event;
      $scope.todo = Restangular.copy($scope.original);
      $scope.todo.datetime = event.datetime;
      $scope.todo.purpose = event.purpose.split(",");
    });
    $scope.SavetodoFollow = function () {
      $scope.submittodo.customPUT($scope.todo, $scope.todo.id).then(function (response) {
        // //console.log('Save ToDo', response);
        $route.reload();
      });
      $scope.showfollowupModal = !$scope.showfollowupModal;
    }
    $scope.showfollowupModal = !$scope.showfollowupModal;
  };
  $scope.CancelFollow = function () {
    $scope.showfollowupModal = !$scope.showfollowupModal;
  };
  /*********************** Date Picker Start *******************/
  $scope.followupdt = $filter('date')(new Date(), 'dd-MMMM-yyyy');
  var newdate = new Date();
  $scope.today = function () {
    $scope.dtmax = new Date();
    /*
    newdate.setDate(newdate.getDate() + 7);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();
    $scope.followupdt = new Date(mm + '/' + dd + '/' + y);
    */
  };
  $scope.today();
  $scope.showWeeks = true;
  $scope.toggleWeeks = function () {
    $scope.showWeeks = !$scope.showWeeks;
  };
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.toggleMin = function () {
    $scope.minDate = ($scope.minDate) ? null : new Date();
  };
  $scope.toggleMin();
  $scope.toggleMin();
  $scope.follow = {};
  $scope.open = function ($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepickerfollowup' + index).focus();
    });
    // $scope.opened = true;
    $scope.follow.followupopened = true;
  };
  $scope.dateOptions = {
    'year-format': 'yy',
    'starting-day': 1
  };
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
  $scope.format = $scope.formats[0];
  //Datepicker settings end 
  $scope.statusDate = {};
  $scope.FWorkstatusDate = {};
  $scope.pillars = Restangular.one('pillars?filter[order]=order%20ASC').getList().$object;
  $scope.duetodo = Restangular.one('duetodostatuses?filter[where][deleteflag]=false').getList().then(function (responseDue) {
    $scope.duetodostatuses = responseDue;
    angular.forEach($scope.duetodostatuses, function (member, index) {
      if ($scope.UserLanguage == 1) {
        member.displayname = member.name
      } else if ($scope.UserLanguage == 2) {
        member.displayname = member.hnname
      } else if ($scope.UserLanguage == 3) {
        member.displayname = member.knname
      } else if ($scope.UserLanguage == 4) {
        member.displayname = member.taname
      } else if ($scope.UserLanguage == 5) {
        member.displayname = member.tename
      } else if ($scope.UserLanguage == 6) {
        member.displayname = member.mrname
      }
    });
    $scope.statusDate.name = 'Due';
    $scope.FWorkstatusDate.fwname = 'Due';
  });
  $scope.openOnetoOne = function () {
    console.log('openOnetoOne');
    $scope.modalInstance1 = $modal.open({
      animation: true,
      templateUrl: 'template/OnetoOneMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.okOnetoOne = function (fullname) {
    $scope.modalInstance1.close();
    var fullname = fullname;
    $rootScope.fullname = $window.sessionStorage.fullName = fullname;
    //console.log('$rootScope.okOnetoOne', $rootScope.fullname);
    // window.location = "/onetoone/" + partnerid;
    $location.path("/onetoone/" + fullname);
  };
  $scope.cancelOnetoOne = function () {
    $scope.modalInstance1.close();
  };
  $scope.openReportIncident = function () {
    console.log('openReportIncident');
    $scope.modalInstanceReport = $modal.open({
      animation: true,
      templateUrl: 'template/ReportIncidentMain.html',
      scope: $scope,
      backdrop: 'static'
    });
    //$scope.modalInstanceLoad.close();
  };
  $scope.okReportIncident = function (fullname) {
    $scope.modalInstanceReport.close();
    var fullname = fullname;
    $rootScope.fullname = $window.sessionStorage.fullName = fullname;
    ////console.log('$rootScope.okReportIncident', $rootScope.fullname);
    $location.path("/reportincident");
  };
  $scope.cancelReportIncident = function () {
    $scope.modalInstanceReport.close();
  };
  $scope.openApplyforScheme = function () {
    $scope.modalInstance1 = $modal.open({
      animation: true,
      templateUrl: 'template/ApplyforScheme.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.okApplyforScheme = function (fullname) {
    $scope.modalInstance1.close();
    var fullname = fullname;
    $rootScope.fullname = $window.sessionStorage.fullName = fullname;
    console.log('$rootScope.okOnetooOne', $rootScope.fullname);
    $location.path("/applyforschemes");
    $route.reload();
  };
  $scope.cancelApplyforScheme = function () {
    $scope.modalInstance1.close();
  };
  $scope.openApplyforDocument = function () {
    $scope.modalInstance1 = $modal.open({
      animation: true,
      templateUrl: 'template/ApplyforDocument.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.okApplyforDocument = function (fullname) {
    $scope.modalInstance1.close();
    var fullname = fullname;
    $rootScope.fullname = $window.sessionStorage.fullName = fullname;
    console.log('$rootScope.okOnetooOne', $rootScope.fullname);
    $location.path("/applyfordocuments");
    $route.reload();
  };
  $scope.cancelApplyforDocument = function () {
    $scope.modalInstance1.close();
  };
  $scope.inactivityTime = function () {
    var t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onkeypress = resetTimer;

    function logout() {
      // alert('alert');
      $location.path("/login");
      //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
      Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
        $window.sessionStorage.userId = '';
        //console.log('Logout');
      }).then(function (redirect) {
        $window.location.href = 'partials/login';
        $window.location.reload();
      });
    }

    function resetTimer() {
      clearTimeout(t);
      t = setTimeout(logout, 60 * 60 * 1000)
      // 1000 milisec = 1 sec
    }
  };
  // $scope.inactivityTime();
  $idle.watch();
  //Edit OnetoOne////////////
  $scope.EditoneToOne = function (todotype, id, data, pillarid, beneficiaryid) {
    $scope.schememaster = {};
    $scope.schememaster.stage = '';
    $scope.schememaster.purposeofloan = '';
    $scope.schememaster.amount = '';
    $scope.schememaster.noofmonthrepay = '';
    console.log('EditoneToOne', todotype, id, data, pillarid, beneficiaryid);
    if (pillarid == 1) {
      $scope.Pillar = 'SP';
      if (data.questionid != null) {
        $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
        $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;
      } else {
        $scope.UpdateQuestion = 'Scheme/Document';
        $scope.UpdateDisplayClickOption = '';
      }
    } else if (pillarid == 2) {
      $scope.Pillar = 'SSJ';
      if (data.questionid != null) {
        $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
        $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;
      } else {
        $scope.UpdateQuestion = 'Report Incident';
        $scope.UpdateDisplayClickOption = '';
      }
    } else if (pillarid == 3) {
      $scope.Pillar = 'FS';
      $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
      $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;
    } else if (pillarid == 4) {
      $scope.Pillar = 'IDS';
      $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
      $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;
    } else if (pillarid == 5) {
      $scope.Pillar = 'HEALTH';
      $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
      $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;
    }
    if (pillarid == 5) {
      $scope.hideUpdateTodo = false;
      $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
        $scope.todotypes = todotyes;
        $scope.todo = data;
        $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
          $scope.MemberName = memname;
        });
        $scope.openToDo();
      });
    }
    if (pillarid == 4 && todotype != null) {
      $scope.hideUpdateTodo = false;
      $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
        $scope.todotypes = todotyes;
        $scope.todo = data;
        if (todotype != 35) {
          $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
            $scope.MemberName = memname;
          });
          $scope.openToDo();
        } else {
          $scope.openToDoPhoneNameMain();
        }
      });
    }
    if (pillarid == 2) {
      //console.log('pillarid',pillarid);
      $scope.hideUpdateRI = false;
      if (data.id != undefined && data.id != '' && data.id != null) {
        $scope.report = Restangular.one('reportincidents', data.id).get().then(function (report) {
          console.log('report2', report);
          console.log('report.reportedcoteam', report.reportedcoteam);
          $scope.reportincident = report;
          if (report.followupneeded != null) {
            $scope.reportincident.follow = report.followupneeded.split(',');
          } else {
            $scope.reportincident.follow = [];
          }
          $scope.reportincident.multiplemembers = [];
          $scope.reportincident.multiplemembers.push(report.beneficiaryid);
          $scope.todo = data;
          $scope.openRI('lg');
        });
      } else {
        $scope.todo = data;
        $scope.openRI('lg');
      }
    }
    if (pillarid == 1) {
      $scope.hideUpdateAW = false;
      if (data.beneficiaryid != undefined && data.beneficiaryid != '' && data.beneficiaryid != null) {
        $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
          $scope.MemberName = memname;
        });
      }
      if (data.documentflag == true) {
        Restangular.one('documenttypes', data.documentid).get().then(function (doc) {
          $scope.SchemeOrDocumentname = doc.name;
        });
        if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
          Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {
            console.log('report', report);
            $scope.schemstage = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().then(function (schemstage) {
              $scope.schemestages = schemstage;
              angular.forEach($scope.schemestages, function (member, index) {
                member.index = index;
                if (report.stage == member.id) {
                  $scope.stageLevel = member.level;
                }
              });
              if (report.stage == 4 && report.responserecieve === 'Rejected') {
                $scope.reponserec = false;
                $scope.rejectdis = false;
                angular.forEach($scope.schemestages, function (member, index) {
                  member.index = index;
                  if (member.id == 4) {
                    member.enabled = false;
                  } else {
                    member.enabled = true
                  }
                });
              } else {
                angular.forEach($scope.schemestages, function (member, index) {
                  member.index = index;
                  if ($scope.stageLevel < member.level) {
                    member.enabled = false;
                  } else if (member.id == report.stage && $scope.stageLevel == member.level) {
                    member.enabled = false;
                  } else {
                    member.enabled = true
                  }
                });
              }
            });
            ////console.log('report',report);
            $scope.datetimehide = false;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster = report;
            console.log('$scope.schememaster', $scope.schememaster);
            $scope.todo = data;
            $scope.openAW('lg');
          });
        }
      } else {
        Restangular.one('schemes', data.documentid).get().then(function (sch) {
          $scope.SchemeOrDocumentname = sch.name;
          var Array = [];
          var Array2 = [];
          Array = sch.category;
          Array2 = Array.split(',');
          //console.log('Array2', Array2.length);
          for (var i = 0; i < Array2.length; i++) {
            //console.log('Array.length',Array.length);
            //console.log('Array', Array2[i]);
            if (Array2[i] == 15) {
              $scope.loanapplied = true;
              //   $scope.amountMonthrepay = false;
              //   $scope.noofmonthrepay_Hide = false;
              //console.log('Loan Applied');
              console.log('$scope.loanapplied', $scope.loanapplied);
            } else {
              $scope.loanapplied = false;
              //  console.log('Not Applied');
            }
          }
        });
        if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
          Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {
            console.log('report.amount', report.amount);
            $scope.myamountvalue = report.amount;
            $scope.installmentAmount = report.amount / report.noofmonthrepay;
            $scope.remainingamount = report.amount - report.paidamount;
            $scope.emipaid = $scope.remainingamount / $scope.installmentAmount;
            $scope.monthtopay = report.paidamount / $scope.installmentAmount;
            if (report.amount == null || report.amount == '' || report.noofmonthrepay == null || report.noofmonthrepay == '') {
              $scope.amountMonthrepay = false;
            } else {
              $scope.amountMonthrepay = true;
            }
            $scope.schemstage = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().then(function (schemstage) {
              $scope.schemestages = schemstage;
              angular.forEach($scope.schemestages, function (member, index) {
                member.index = index;
                if (report.stage == member.id) {
                  $scope.stageLevel = member.level;
                }
              });
              if (report.stage == 4 && report.responserecieve === 'Rejected') {
                $scope.reponserec = false;
                $scope.rejectdis = false;
                angular.forEach($scope.schemestages, function (member, index) {
                  member.index = index;
                  if (member.id == 4) {
                    member.enabled = false;
                  } else {
                    member.enabled = true
                  }
                });
              } else {
                angular.forEach($scope.schemestages, function (member, index) {
                  member.index = index;
                  if ($scope.stageLevel < member.level) {
                    member.enabled = false;
                  } else if (member.id == report.stage && $scope.stageLevel == member.level) {
                    member.enabled = false;
                  } else {
                    member.enabled = true
                  }
                });
              }
            });
            $scope.datetimehide = false;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            ////console.log('report',report);
            $scope.schememaster = report;
            console.log('$scope.schememaster', $scope.schememaster);
            $scope.todo = data;
            $scope.openAW('lg');
          });
        }
      }
    }
    if (pillarid == 3) {
      $scope.hideUpdateTodo = false;
      $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
        $scope.todotypes = todotyes;
        $scope.todo = data;
        $scope.openToDo();
      });
    }
    if (todotype == 33) {
      $scope.hideUpdateTodo = false;
      //$scope.todotyes = Restangular.all('todotypes?filter[where][pillarid][nlike]=null%').getList().then(function (todotyes) {
      $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
        $scope.todotypes = todotyes;
        $scope.todo = data;
        if (data.beneficiaryid != undefined && data.beneficiaryid != '' && data.beneficiaryid != null) {
          $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
            $scope.MemberName = memname;
          });
        }
        $scope.openToDoFollow();
      });
    }
    if (todotype == 32) {
      $scope.hideUpdateTodo = false;
      $scope.FollowUpMaintodostatuses1 = Restangular.all('todostatuses?filter={"where": {"id": {"inq": [1, 3]}}}').getList().then(function (Tst) {
        $scope.FollowUpMaintodostatuses = Tst;
        //console.log('FollowUpMaintodostatuses', Tst);
      });
      $scope.followupgrs = Restangular.all('followupgroups').getList().then(function (todotyes) {
        $scope.followupgroups = todotyes;
        $scope.todo = data;
        if (data.beneficiaryid != undefined && data.beneficiaryid != '' && data.beneficiaryid != null) {
          $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
            $scope.MemberName = memname;
          });
        }
        $scope.openToDoFollow();
      });
    }
    if (todotype == 31) {
      $scope.hideUpdateTodo = false;
      $scope.Stname = Restangular.one('stakeholdertypes', data.stakeholdertype).get().then(function (st) {
        $scope.stakeholdertypeName = st;
      });
      $scope.pp = Restangular.one('purposeofmeetings', data.purpose).get().then(function (st) {
        $scope.purposeofmeetingName = st;
      });
      $scope.FollowUpMaintodostatuses1 = Restangular.all('todostatuses?filter={"where": {"id": {"inq": [1, 3]}}}').getList().then(function (Tsts) {
        $scope.FollowUpMaintodostatuses = Tsts;
      });
      $scope.followupgrs = Restangular.all('followupstacks').getList().then(function (todotyes) {
        $scope.followupgroups = todotyes;
        $scope.todo = data;
        if (data.beneficiaryid != undefined && data.beneficiaryid != '' && data.beneficiaryid != null) {
          $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
            $scope.MemberName = memname;
          });
        }
        $scope.openToDoFollow();
      });
    }
  };
  ///////////////////////////////Edit One to One Close/////////////////////////////////////////////
  $scope.openGroupMeeting = function () {
    $scope.modalGroupMeeting = $modal.open({
      animation: true,
      templateUrl: 'template/GroupMeetingMain.html',
      scope: $scope,
      backdrop: 'static',
      size: 'lg'
    });
  };
  $scope.okGroupMeeting = function () {
    $scope.modalGroupMeeting.close();
  };
  $scope.openFs = function () {
    $scope.modalFS = $modal.open({
      animation: true,
      templateUrl: 'template/fsMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveFS = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    $scope.modalFS.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okFs = function () {
    $scope.modalFS.close();
  };
  $scope.openHealth = function () {
    $scope.modalHealth = $modal.open({
      animation: true,
      templateUrl: 'template/healthMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveHealth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    $scope.modalHealth.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okHealth = function () {
    $scope.modalHealth.close();
  };
  $scope.openSp = function () {
    $scope.modalSP = $modal.open({
      animation: true,
      templateUrl: 'template/spMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveSP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    $scope.modalSP.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okSp = function () {
    $scope.modalSP.close();
  };
  $scope.openToDo = function () {
    $scope.modalToDo = $modal.open({
      animation: true,
      templateUrl: 'template/ToDoMain.html',
      scope: $scope,
      backdrop: 'static',
      keyboard: false
    });
  };
  $scope.openToDoPhoneNameMain = function () {
    $scope.modalToDo = $modal.open({
      animation: true,
      templateUrl: 'template/ToDoPhoneNameMain.html',
      scope: $scope,
      backdrop: 'static',
      keyboard: false
    });
  };
  $scope.openToDoFollow = function () {
    $scope.modalToDo = $modal.open({
      animation: true,
      templateUrl: 'template/FollowUpMain.html',
      scope: $scope,
      backdrop: 'static',
      keyboard: false
    });
  };
  $scope.SaveToDo = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.todo.pillarid = pillarid;
    $scope.todo.beneficiaryid = beneficiaryid;
    $scope.todo.fieldworkerid = $window.sessionStorage.UserEmployeeId;
    $scope.todo.facility = $window.sessionStorage.coorgId;
    $scope.todo.district = $window.sessionStorage.salesAreaId;
    $scope.todo.state = $window.sessionStorage.zoneId;
    $scope.todo.questionid = questionid;
    $scope.submittodos.post($scope.todo).then(function (resp) {
      $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      $scope.todo = {
        status: 1,
        stateid: $window.sessionStorage.zoneId,
        state: $window.sessionStorage.zoneId,
        districtid: $window.sessionStorage.salesAreaId,
        district: $window.sessionStorage.salesAreaId,
        coid: $window.sessionStorage.coorgId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date()
      };
      var sevendays = new Date();
      sevendays.setDate(sevendays.getDate() + 7);
      $scope.todo.datetime = sevendays;
    }, function (error) {
      //console.log('error', error);
    });
    $scope.modalToDo.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okToDo = function () {
    $scope.modalToDo.close();
  };
  $scope.openReportFollowup = function () {
    $scope.modalReportFollowup = $modal.open({
      animation: true,
      templateUrl: 'template/ReportFollowUpMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveReportFollowup = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.modalReportFollowup.close();
  };
  $scope.okReportFollowup = function () {
    $scope.modalReportFollowup.close();
  };
  $scope.phonename = {};
  $scope.multiplephonename = [];
  $scope.openPhoneNameMain = function () {
    $scope.modalPhoneNameMain = $modal.open({
      animation: true,
      templateUrl: 'template/PhoneNameMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.okPhoneNameMain = function () {
    $scope.modalPhoneNameMain.close();
  };
  $scope.AddPhoneNameMain = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    if ($scope.phonename.name != undefined && $scope.phonename.phone != undefined) {
      if ($scope.phonename.phone.length == 10) {
        $scope.todophonename = {};
        $scope.todophonename.pillarid = pillarid;
        $scope.todophonename.beneficiaryid = beneficiaryid;
        $scope.todophonename.fieldworkerid = $window.sessionStorage.UserEmployeeId;
        $scope.todophonename.facility = $window.sessionStorage.coorgId;
        $scope.todophonename.district = $window.sessionStorage.salesAreaId;
        $scope.todophonename.state = $window.sessionStorage.zoneId;
        $scope.todophonename.questionid = questionid;
        $scope.todophonename.name = $scope.phonename.name;
        $scope.todophonename.phone = $scope.phonename.phone;
        $scope.todophonename.datetime = new Date();
        $scope.todophonename.status = 1;
        $scope.todophonename.questionid = questionid;
        $scope.todophonename.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todophonename.lastmodifiedtime = new Date();
        $scope.multiplephonename.push($scope.todophonename);
        $scope.phonename = {};
      } else {
        alert('Enter 10 digit mobile number');
      }
    }
    //$scope.modalPhoneName.close();
  };
  $scope.SavePhoneNameMain = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    if ($scope.phonename.name != undefined && $scope.phonename.phone != undefined) {
      if ($scope.phonename.phone.length == 10) {
        $scope.todophonename = {};
        $scope.todophonename.pillarid = pillarid;
        $scope.todophonename.beneficiaryid = beneficiaryid;
        $scope.todophonename.fieldworkerid = $window.sessionStorage.UserEmployeeId;
        $scope.todophonename.facility = $window.sessionStorage.coorgId;
        $scope.todophonename.district = $window.sessionStorage.salesAreaId;
        $scope.todophonename.state = $window.sessionStorage.zoneId;
        $scope.todophonename.questionid = questionid;
        $scope.todophonename.name = $scope.phonename.name;
        $scope.todophonename.phone = $scope.phonename.phone;
        $scope.todophonename.datetime = new Date();
        $scope.todophonename.status = 1;
        $scope.todophonename.questionid = questionid;
        $scope.todophonename.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todophonename.lastmodifiedtime = new Date();
        $scope.multiplephonename.push($scope.todophonename);
        $scope.phonename = {};
      } else {
        alert('Enter 10 digit mobile number');
      }
    }
    var phonenamecount = 0;
    for (var i = 0; i < $scope.multiplephonename.length; i++) {
      $scope.submittodos.post($scope.multiplephonename[i]).then(function (resp) {
        phonenamecount++;
        if (phonenamecount >= $scope.multiplephonename.length) {
          //console.log('phonename', 'phonename');
          $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
          $scope.multiplephonename = [];
          $scope.modalPhoneNameMain.close();
          if (pillarid == 1) {
            $scope.spnextquestion();
          } else if (pillarid == 2) {
            $scope.ssjnextquestion();
          } else if (pillarid == 3) {
            $scope.fsnextquestion();
          } else if (pillarid == 4) {
            $scope.idsnextquestion();
          } else if (pillarid == 5) {
            $scope.healthnextquestion();
          }
        }
      });
    }
  };
  $scope.openRI = function (size) {
    $scope.modalRI = $modal.open({
      animation: true,
      templateUrl: 'template/ReportIncidentUpdate.html',
      scope: $scope,
      backdrop: 'static',
      size: size,
    });
  };
  $scope.openRIOthers = function (size) {
    $scope.OthersReport = true;
    $scope.reportincident.multiplemembers = [];
    $scope.modalRI = $modal.open({
      animation: true,
      templateUrl: 'template/ReportIncidentOthersMain.html',
      scope: $scope,
      backdrop: 'static',
      size: size,
    });
  };
  $scope.reportincident.follow = [];
  $scope.addedtodos = [];
  $scope.CancelFollow = function () {
    $scope.reportincident.follow = $scope.oldFollowUp;
    ////console.log('$scope.groupmeeting.follow', $scope.groupmeeting.follow);
    $scope.okReportFollowup();
  };
  $scope.SaveRI = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.reportincident.beneficiaryid = beneficiaryid;
    $scope.reportincident.fieldworkerid = $window.sessionStorage.UserEmployeeId;
    $scope.reportincident.coid = $window.sessionStorage.coorgId;
    $scope.reportincident.districtid = $window.sessionStorage.salesAreaId;
    $scope.reportincident.stateid = $window.sessionStorage.zoneId;
    $scope.reportincident.questionid = questionid;
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    if ($scope.dateofclosure == true) {
      if ($scope.reportincident.follow.length > 0) {
        $scope.reportcount = 0;
        //$scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
        //$scope.reportincidents.post($scope.reportincident).then(function (onemember) {
        for (var i = 0; i < $scope.reportincident.follow.length; i++) {
          if (i == 0) {
            $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
          } else {
            $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
          }
        }
        $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        //console.log('Save');
      } else {
        alert('folllowup is mandatory');
      }
    } else {
      $scope.reportcount = 0;
      //$scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
      //$scope.reportincidents.post($scope.reportincident).then(function (onemember) {
      for (var i = 0; i < $scope.reportincident.follow.length; i++) {
        if (i == 0) {
          $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
        } else {
          $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
        }
      }
      $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      //console.log('Save');
    }
  };
  $scope.okRI = function () {
    $scope.modalRI.close();
  };
  $scope.newtodocount = 0;
  $scope.reportadd = 0;
  $scope.SaveIncident = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    if ($scope.reportincident.co === 'no') {
      //console.log('$scope.benfid', $scope.benfid);
      $scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
      $scope.submitreportincidents.post($scope.reportincident).then(function (onemember) {
        //console.log('OneReport', $scope.onemember);
        $scope.reportincident = {
          // "referredby":"false",
          "physical": "false",
          "emotional": "false",
          "sexual": "false",
          "propertyrelated": "false",
          "childrelated": "false", //"other": false,
          "police": "false",
          "goons": "false",
          "clients": "false",
          "partners": "false",
          "husband": "false",
          "serviceprovider": "false",
          "familymember": "false",
          "otherkp": "false",
          "perpetratorother": "false",
          "neighbour": "false",
          "authorities": "false",
          "co": "false",
          "timetorespond": "false",
          "severity": "Moderate",
          "resolved": "false",
          "reported": "false",
          "reportedpolice": "false",
          "reportedngos": "false",
          "reportedfriends": "false",
          "reportedplv": "false",
          "reportedcoteam": "false",
          "reportedchampions": "false",
          "reportedotherkp": "false",
          "reportedlegalaid": "false",
          "referredcouncelling": "false",
          "referredmedicalcare": "false",
          "referredcomanager": "false",
          "referredplv": "false",
          "referredlegalaid": "false",
          "referredboardmember": "false",
          "noactionreqmember": "false",
          "noactiontaken": "false",

          "multiplemembers": ['1', '2'],
          "stateid": $window.sessionStorage.zoneId,
          "state": $window.sessionStorage.zoneId,
          "districtid": $window.sessionStorage.salesAreaId,
          "district": $window.sessionStorage.salesAreaId,
          "coid": $window.sessionStorage.coorgId,
          "facility": $window.sessionStorage.coorgId,
          "beneficiaryid": null,
          "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
          "lastmodifiedtime": new Date(),
          "lastmodifiedbyrole": $window.sessionStorage.roleId
        };
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.reportincident.followupdate = sevendays;
        $scope.modalRI.close();
        if (pillarid == 1) {
          $scope.spnextquestion();
        } else if (pillarid == 2) {
          $scope.ssjnextquestion();
        } else if (pillarid == 3) {
          $scope.fsnextquestion();
        } else if (pillarid == 4) {
          $scope.idsnextquestion();
        } else if (pillarid == 5) {
          $scope.healthnextquestion();
        }

      });
    } else if ($scope.reportincident.co === true) {
      $scope.reportincident.beneficiaryid = $scope.reportincident.multiplemembers[$scope.reportadd];
      $scope.submitreportincidents.post($scope.reportincident).then(function (allmember) {
        $scope.reportadd++;
        if ($scope.reportadd < $scope.reportincident.multiplemembers.length) {
          $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        } else {
          $scope.reportincident = {
            // "referredby":"false",
            "physical": "false",
            "emotional": "false",
            "sexual": "false",
            "propertyrelated": "false",
            "childrelated": "false", //"other": false,
            "police": "false",
            "goons": "false",
            "clients": "false",
            "partners": "false",
            "husband": "false",
            "serviceprovider": "false",
            "familymember": "false",
            "otherkp": "false",
            "perpetratorother": "false",
            "neighbour": "false",
            "authorities": "false",
            "co": "false",
            "timetorespond": "false",
            "severity": "Moderate",
            "resolved": "false",
            "reported": "false",
            "reportedpolice": "false",
            "reportedngos": "false",
            "reportedfriends": "false",
            "reportedplv": "false",
            "reportedcoteam": "false",
            "reportedchampions": "false",
            "reportedotherkp": "false",
            "reportedlegalaid": "false",
            "referredcouncelling": "false",
            "referredmedicalcare": "false",
            "referredcomanager": "false",
            "referredplv": "false",
            "referredlegalaid": "false",
            "referredboardmember": "false",
            "noactionreqmember": "false",
            "noactiontaken": "false",

            "multiplemembers": ['1', '2'],
            "stateid": $window.sessionStorage.zoneId,
            "state": $window.sessionStorage.zoneId,
            "districtid": $window.sessionStorage.salesAreaId,
            "district": $window.sessionStorage.salesAreaId,
            "coid": $window.sessionStorage.coorgId,
            "facility": $window.sessionStorage.coorgId,
            "beneficiaryid": null,
            "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
            "lastmodifiedtime": new Date(),
            "lastmodifiedbyrole": $window.sessionStorage.roleId
          };
          var sevendays = new Date();
          sevendays.setDate(sevendays.getDate() + 7);
          $scope.reportincident.followupdate = sevendays;
          $scope.modalRI.close();
          if (pillarid == 1) {
            $scope.spnextquestion();
          } else if (pillarid == 2) {
            $scope.ssjnextquestion();
          } else if (pillarid == 3) {
            $scope.fsnextquestion();
          } else if (pillarid == 4) {
            $scope.idsnextquestion();
          } else if (pillarid == 5) {
            $scope.healthnextquestion();
          }
        }

      });
    }
    //});
  };
  $scope.openCL = function () {
    $scope.modalCL = $modal.open({
      animation: true,
      templateUrl: 'template/ChampionsListMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveCL = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    $scope.modalCL.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okCL = function () {
    $scope.modalCL.close();
  };
  $scope.openDP = function () {
    $scope.modalDP = $modal.open({
      animation: true,
      templateUrl: 'template/DatePickerMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.beneficiarylastdate = {};
  $scope.SaveDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    if (questionid == 3) {
      $scope.beneficiarylastdate.monthoflasthivtest = $scope.picker.selectdate;
    } else {
      $scope.beneficiarylastdate.monthoflaststitest = $scope.picker.selectdate;
    }
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarylastdate).then(function (respo) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      });
    }
    $scope.modalDP.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okDP = function () {
    $scope.modalDP.close();
  };
  $scope.openIDP = function () {
    $scope.modalIDP = $modal.open({
      animation: true,
      templateUrl: 'template/IncrementDatePickerMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.beneficiaryincrement = {};
  $scope.SaveIDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    // //console.log('Button Clicked', beneficiaryid + '::' + pillarid + ',questionid::' + questionid + ',answer::' + answer + ',modifieddate::' + modifieddate + ',modifiedby::' + modifiedby + ',serialno::' + serialno + ',yesincrement::' + yesincrement + ',noincrement::' + noincrement);
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
        var d1 = Date.parse($scope.increment.selectdate);
        var d2 = Date.parse($scope.DisplayBeneficiary.monthoflastsaving);
        // //console.log('d1', d1);
        // //console.log('d2', d2);
        if (yesincrement == 'savings') {
          $scope.beneficiaryincrement.noofsavingaccounts = +kp.noofsavingaccounts + 1;
          if (d1 > d2) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
          }
        } else if (yesincrement == 'investmentproducts') {
          $scope.beneficiaryincrement.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
          if (d1 > d2) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
          }
        } else if (yesincrement == 'savingsources') {
          $scope.beneficiaryincrement.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
          if (d1 > d2) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
          }
        } else if (yesincrement == 'insuranceproducts') {
          $scope.beneficiaryincrement.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
          if (d1 > d2) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
          }
        } else if (yesincrement == 'reciept') {
          $scope.beneficiaryincrement.paidmember = true;
          if (d1 > d2) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
          } else {
            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
          }
        }
        ////console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
        if ($routeParams.id != undefined) {
          Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrement).then(function (respo) {
            $scope.beneficiaryincrement = {};
            $scope.increment = {};
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
          }, function (error) {
            //console.log('error', error);
          });
        }
      });
    }
    $scope.modalIDP.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okIDP = function () {
    $scope.modalIDP.close();
  };
  $scope.openCDIDS = function () {
    $scope.modalCDIDS = $modal.open({
      animation: true,
      templateUrl: 'template/CDIDSMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveCDIDS = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
    $scope.availeddate = $scope.cdids.availeddate;
    $scope.referenceno = $scope.cdids.referenceno;
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, $scope.availeddate, $scope.referenceno);
    $scope.modalCDIDS.close();
    if (pillarid == 1) {
      $scope.spquestioncount++;
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjquestioncount;
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsquestioncount;
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsquestioncount;
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthquestioncount;
      $scope.healthnextquestion();
    }
  };
  $scope.okCDIDS = function () {
    $scope.modalCDIDS.close();
  };
  $scope.openAW = function (size) {
    $scope.modalAW = $modal.open({
      animation: true,
      templateUrl: 'template/ApplicationWorkflowMain.html',
      scope: $scope,
      backdrop: 'static',
      size: size
    });
  };
  $scope.SaveAW = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
    if (answer == 'yes') {
      $scope.todo.documentid = yesdocumentid;
      $scope.todo.documentflag = yesdocumentflag;
      $scope.schememaster.documentflag = yesdocumentflag;
      $scope.schememaster.schemeId = yesdocumentid;
      if (yesdocumentflag == 'yes') {
        $scope.todo.todotype = 29;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      } else {
        $scope.todo.todotype = 28;
        Restangular.one('schemes?filter[state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + yesdocumentid).get().then(function (schemes) {
          if (schemes.length > 0) {
            $scope.todo.documentid = schemes[0].id;
            $scope.schememaster.schemeId = schemes[0].id;
            $scope.todo.pillarid = pillarid;
            $scope.todo.questionid = questionid;
            $scope.todo.beneficiaryid = beneficiaryid;
            $scope.todo.datetime = $scope.schememaster.datetime;
            $scope.todo.stage = $scope.schememaster.stage;
            $scope.todo.facility = $window.sessionStorage.coorgId;
            $scope.todo.district = $window.sessionStorage.salesAreaId;
            $scope.todo.state = $window.sessionStorage.zoneId;
            $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
            $scope.todo.lastmodifiedtime = new Date();
            $scope.todo.site = $scope.DisplayBeneficiary.site;
            $scope.schememaster.memberId = beneficiaryid;
            $scope.schememaster.facility = $window.sessionStorage.coorgId;
            $scope.schememaster.district = $window.sessionStorage.salesAreaId;
            $scope.schememaster.state = $window.sessionStorage.zoneId;
            $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
            $scope.schememaster.lastmodifiedtime = new Date();
            $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
              //console.log('$scope.documentmasters', $scope.schememaster);
              $scope.todo.reportincidentid = resp.id;
              $scope.submittodos.post($scope.todo).then(function () {
                //console.log('$scope.todo', $scope.todo);
                $scope.SchemeOrDocumentname = null;
                $scope.schememaster.stage = null;
              });
            });
          }
        });
      }
    } else {
      $scope.todo.documentid = nodocumentid;
      $scope.todo.documentflag = nodocumentflag;
      $scope.schememaster.documentflag = nodocumentflag;
      $scope.schememaster.schemeId = nodocumentid;
      if (yesdocumentflag == 'yes') {
        $scope.todo.todotype = 29;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      } else {
        $scope.todo.todotype = 28;
        Restangular.one('schemes?filter[state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + nodocumentid).get().then(function (schemes) {
          if (schemes.length > 0) {
            $scope.todo.documentid = schemes[0].id;
            $scope.schememaster.schemeId = schemes[0].id;
            $scope.todo.pillarid = pillarid;
            $scope.todo.questionid = questionid;
            $scope.todo.beneficiaryid = beneficiaryid;
            $scope.todo.datetime = $scope.schememaster.datetime;
            $scope.todo.stage = $scope.schememaster.stage;
            $scope.todo.facility = $window.sessionStorage.coorgId;
            $scope.todo.district = $window.sessionStorage.salesAreaId;
            $scope.todo.state = $window.sessionStorage.zoneId;
            $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
            $scope.todo.lastmodifiedtime = new Date();
            $scope.todo.site = $scope.DisplayBeneficiary.site;
            $scope.schememaster.memberId = beneficiaryid;
            $scope.schememaster.facility = $window.sessionStorage.coorgId;
            $scope.schememaster.district = $window.sessionStorage.salesAreaId;
            $scope.schememaster.state = $window.sessionStorage.zoneId;
            $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
            $scope.schememaster.lastmodifiedtime = new Date();
            $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
              //console.log('$scope.documentmasters', $scope.schememaster);
              $scope.todo.reportincidentid = resp.id;
              $scope.submittodos.post($scope.todo).then(function () {
                //console.log('$scope.todo', $scope.todo);
                $scope.SchemeOrDocumentname = null;
                $scope.schememaster.stage = null;
              });
            });
          }
        });
      }
    }
    $scope.modalAW.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okAW = function () {
    $scope.modalAW.close();
    // $route.reload();
  };
  $scope.openAWBoth = function (size) {
    $scope.schememaster.memberId = $scope.DisplayBeneficiary.id;
    $scope.modalAWBoth = $modal.open({
      animation: true,
      templateUrl: 'template/ApplicationWorkflowAllMain.html',
      scope: $scope,
      backdrop: 'static',
      size: size
    });
  };
  $scope.SaveAWBoth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
    if (answer == 'yes') {
      $scope.todo.documentid = $scope.schememaster.schemeId;
      $scope.todo.documentflag = $scope.schememaster.documentflag;
      if (yesdocumentflag == 'yes') {
        $scope.todo.todotype = 29;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      } else {
        $scope.todo.todotype = 28;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      }
    } else {
      $scope.todo.documentid = $scope.schememaster.schemeId;
      $scope.todo.documentflag = $scope.schememaster.documentflag;
      if (yesdocumentflag == 'yes') {
        $scope.todo.todotype = 29;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      } else {
        $scope.todo.todotype = 28;
        $scope.todo.pillarid = pillarid;
        $scope.todo.questionid = questionid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.datetime = $scope.schememaster.datetime;
        $scope.todo.stage = $scope.schememaster.stage;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.todo.lastmodifiedtime = new Date();
        $scope.todo.site = $scope.DisplayBeneficiary.site;
        $scope.schememaster.memberId = beneficiaryid;
        $scope.schememaster.facility = $window.sessionStorage.coorgId;
        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
        $scope.schememaster.state = $window.sessionStorage.zoneId;
        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
        $scope.schememaster.lastmodifiedtime = new Date();
        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
          //console.log('$scope.documentmasters', $scope.schememaster);
          $scope.todo.reportincidentid = resp.id;
          $scope.submittodos.post($scope.todo).then(function () {
            //console.log('$scope.todo', $scope.todo);
            $scope.SchemeOrDocumentname = null;
            $scope.schememaster.stage = null;
          });
        });
      }
    }
    $scope.modalAWBoth.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okAWBoth = function () {
    $scope.modalAWBoth.close();
    //$route.reload();
  };
  $scope.openPLHIV = function () {
    $scope.plhiv.source = $scope.DisplayBeneficiary.sourceofinfection;
    $scope.plhiv.month = $scope.DisplayBeneficiary.monthofdetection;
    $scope.modalPLHIV = $modal.open({
      animation: true,
      templateUrl: 'template/PLHIVMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.beneficiaryplhiv = {};
  $scope.SavePLHIV = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.beneficiaryplhiv.plhiv = true;
    $scope.beneficiaryplhiv.sourceofinfection = $scope.plhiv.source;
    $scope.beneficiaryplhiv.monthofdetection = $scope.plhiv.month;
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryplhiv).then(function (respo) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      });
    }
    $scope.modalPLHIV.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okPLHIV = function () {
    $scope.modalPLHIV.close();
  };
  $scope.openCD = function () {
    if ($scope.DisplayBeneficiary.condomsasked != null) {
      var CondomsAsked = $scope.DisplayBeneficiary.condomsasked.split(',');
      var CondomsProvided = $scope.DisplayBeneficiary.condomsprovided.split(',');
      if (CondomsAsked.length > 0) {
        var AskedNum = CondomsAsked[CondomsAsked.length - 1].split(':');
        var ProvidedNum = CondomsProvided[CondomsProvided.length - 1].split(':');
        $scope.condom.asked = AskedNum[AskedNum.length - 1];
        $scope.condom.provided = ProvidedNum[ProvidedNum.length - 1];
      }
    }
    $scope.modalCD = $modal.open({
      animation: true,
      templateUrl: 'template/condomdetailsMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveCD = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.askeddate = $filter('date')($scope.condom.dateasked, 'dd/MM/yyyy');
    if ($scope.DisplayBeneficiary.condomsasked != null) {
      $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
      $scope.beneficiarycondom.condomsasked = $scope.DisplayBeneficiary.condomsasked + ',' + $scope.askeddate + ':' + $scope.condom.asked;
      $scope.beneficiarycondom.condomsprovided = $scope.DisplayBeneficiary.condomsprovided + ',' + $scope.askeddate + ':' + $scope.condom.provided;
    } else {
      $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
      $scope.beneficiarycondom.condomsasked = $scope.askeddate + ':' + $scope.condom.asked;
      $scope.beneficiarycondom.condomsprovided = $scope.askeddate + ':' + $scope.condom.provided;
    }
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarycondom).then(function (respo) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      });
    }
    $scope.modalCD.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okCD = function () {
    $scope.modalCD.close();
  };
  $scope.openFL = function () {
    $scope.finliteracy.savings = $scope.DisplayBeneficiary.savings;
    $scope.finliteracy.insurance = $scope.DisplayBeneficiary.insurance;
    $scope.finliteracy.pension = $scope.DisplayBeneficiary.pension;
    $scope.finliteracy.credit = $scope.DisplayBeneficiary.credit;
    $scope.finliteracy.remitance = $scope.DisplayBeneficiary.remitance;
    $scope.modalFL = $modal.open({
      animation: true,
      templateUrl: 'template/FinancialLiteracyMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.beneficiaryfl = {};
  $scope.SaveFL = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.beneficiaryfl.savings = $scope.finliteracy.savings;
    $scope.beneficiaryfl.insurance = $scope.finliteracy.insurance;
    $scope.beneficiaryfl.pension = $scope.finliteracy.pension;
    $scope.beneficiaryfl.credit = $scope.finliteracy.credit;
    $scope.beneficiaryfl.remitance = $scope.finliteracy.remitance;
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryfl).then(function (respo) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
      });
    }
    $scope.modalFL.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okFL = function () {
    $scope.modalFL.close();
  };
  $scope.openFP = function () {
    $scope.finplanning.savings = $scope.DisplayBeneficiary.finplansavings;
    $scope.finplanning.insurance = $scope.DisplayBeneficiary.finplaninsurance;
    $scope.finplanning.pension = $scope.DisplayBeneficiary.finplanpension;
    $scope.finplanning.credit = $scope.DisplayBeneficiary.finplancredit;
    $scope.finplanning.remitance = $scope.DisplayBeneficiary.finplanremitance;
    $scope.modalFP = $modal.open({
      animation: true,
      templateUrl: 'template/FinancialPlanningMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.openINFO = function () {
    $scope.modalINFO = $modal.open({
      animation: true,
      templateUrl: 'template/ProvideInfoMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.SaveINFO = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
    $scope.modalINFO.close();
    if (pillarid == 1) {
      $scope.spnextquestion();
    } else if (pillarid == 2) {
      $scope.ssjnextquestion();
    } else if (pillarid == 3) {
      $scope.fsnextquestion();
    } else if (pillarid == 4) {
      $scope.idsnextquestion();
    } else if (pillarid == 5) {
      $scope.healthnextquestion();
    }
  };
  $scope.okINFO = function () {
    $scope.modalINFO.close();
  };
  $scope.openPillarCompleted = function () {
    $scope.modalPillarCompleted = $modal.open({
      animation: true,
      templateUrl: 'template/PillarCompletedMain.html',
      scope: $scope,
      backdrop: 'static'
    });
  };
  $scope.PillarCompleted = {
    completedpillar: 0,
    completedquestion: 0
  }
  $scope.SavePillarCompleted = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
    if ($routeParams.id != undefined) {
      Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.PillarCompleted).then(function (respo) {
        $scope.modalPillarCompleted.close();
        //$route.reload();
        window.location = "/onetoone/" + $routeParams.id;
      });
    }
  };
  $scope.okPillarCompleted = function () {
    $scope.modalPillarCompleted.close();
    window.location = "/";
  };
  $scope.updateToDo = function () {
    /*$scope.memberupdate = {
    			lastmeetingdate: new Date()
    	}
    	Restangular.all('beneficiaries/' + $scope.todo.beneficiaryid).customPUT($scope.memberupdate).then(function (responsemember) {
    	});*/
    $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
      $scope.todo = {
        status: 1
      };
      var sevendays = new Date();
      sevendays.setDate(sevendays.getDate() + 7);
      $scope.todo.datetime = sevendays;
      console.log('$scope.todo.datetime', $scope.todo.datetime);
      $scope.modalToDo.close();
      $scope.hideUpdateTodo = true;
    }, function (error) {
      //console.log('error', error);
    });
  };
  $scope.openOneAlert = function () {
    $scope.modalOneAlert = $modal.open({
      animation: true,
      templateUrl: 'template/AlertModal.html',
      scope: $scope,
      backdrop: 'static',
      keyboard: false,
      size: 'sm',
      windowClass: 'modal-danger'
    });
  };
  $scope.okAlert = function () {
    $scope.modalOneAlert.close();
  };
  $scope.updateRI = function () {
    if ($scope.reportincident.physical == true || $scope.reportincident.sexual == true || $scope.reportincident.childrelated == true || $scope.reportincident.emotional == true || $scope.reportincident.propertyrelated == true) {
      if ($scope.reportincident.police == true || $scope.reportincident.clients == true || $scope.reportincident.serviceprovider == true || $scope.reportincident.otherkp == true || $scope.reportincident.neighbour == true || $scope.reportincident.goons == true || $scope.reportincident.partners == true || $scope.reportincident.familymember == true || $scope.reportincident.husband == true) {
        if ($scope.reportincident.referredcouncelling == true || $scope.reportincident.referredmedicalcare == true || $scope.reportincident.referredcomanager == true || $scope.reportincident.referredplv == true || $scope.reportincident.referredlegalaid == true || $scope.reportincident.referredboardmember == true || $scope.reportincident.noactionreqmember == true || $scope.reportincident.noactiontaken == true) {
          $scope.updateREPORTINC();
        } else {
          $scope.AlertMessage = $scope.optactiontaken; //'At least one option must be selected in Action Taken and Followup';
          $scope.openOneAlert();
        }
      } else {
        $scope.AlertMessage = $scope.optpreperator; //'At least one option must be selected in  Perpetrator Details';
        $scope.openOneAlert();
      }
    } else {
      $scope.AlertMessage = $scope.optincidenttype; //'At least one option must be selected in Incident Type';
      $scope.openOneAlert();
    }
  };
  $scope.updateREPORTINC = function () {
    /*$scope.memberupdate = {
    	lastmeetingdate: new Date()
    }
    Restangular.all('beneficiaries/' + $scope.reportincident.multiplemembers).customPUT($scope.memberupdate).then(function (responsemember) {
    	//console.log('member update', responsemember);
    });*/
    $scope.submitreportincidents.customPUT($scope.reportincident, $scope.reportincident.id).then(function (resp) {
      console.log('submitreportincidents', resp);
      $scope.reportincident = {
        // "referredby":"false",
        "physical": "false",
        "emotional": "false",
        "sexual": "false",
        "propertyrelated": "false",
        "childrelated": "false", //"other": false,
        "police": "false",
        "goons": "false",
        "clients": "false",
        "partners": "false",
        "husband": "false",
        "serviceprovider": "false",
        "familymember": "false",
        "otherkp": "false",
        "perpetratorother": "false",
        "neighbour": "false",
        "authorities": "false",
        "co": "false",
        "timetorespond": "false",
        "severity": "Moderate",
        "resolved": "false",
        "reported": "false",
        "reportedpolice": "false",
        "reportedngos": "false",
        "reportedfriends": "false",
        "reportedplv": "false",
        "reportedcoteam": "false",
        "reportedchampions": "false",
        "reportedotherkp": "false",
        "reportedlegalaid": "false",
        "referredcouncelling": "false",
        "referredmedicalcare": "false",
        "referredcomanager": "false",
        "referredplv": "false",
        "referredlegalaid": "false",
        "referredboardmember": "false",
        "noactionreqmember": "false",
        "noactiontaken": "false",
        "multiplemembers": ['1', '2'],
        "stateid": $window.sessionStorage.zoneId,
        "state": $window.sessionStorage.zoneId,
        "districtid": $window.sessionStorage.salesAreaId,
        "district": $window.sessionStorage.salesAreaId,
        "coid": $window.sessionStorage.coorgId,
        "facility": $window.sessionStorage.coorgId,
        "beneficiaryid": null,
        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
        "lastmodifiedtime": new Date(),
        "lastmodifiedbyrole": $window.sessionStorage.roleId
      };
      $scope.todo.datetime = resp.followupdate;
      var sevendays = new Date();
      sevendays.setDate(sevendays.getDate() + 7);
      $scope.todo.datetime = resp.followupdate;
      $scope.hideUpdateRI = true;
      $scope.okRI();
      /*$scope.submittodos.customPUT($scope.todo).then(function (resp) {
          $scope.todo = {
              status: 1
          };
          var sevendays = new Date();
          sevendays.setDate(sevendays.getDate() + 7);
          $scope.todo.datetime = sevendays;
          $scope.hideUpdateRI = true;
          $scope.okRI();
      }, function (error) {
          //console.log('error', error);
      });*/
    }, function (error) {
      //console.log('error', error);
    });
  };
  $scope.updateAW = function () {
    /*$scope.memberupdate = {
    	lastmeetingdate: new Date()
    }
    Restangular.all('beneficiaries/' + $scope.schememaster.memberId).customPUT($scope.memberupdate).then(function (responsemember) {
    	console.log('member update', responsemember);
    });*/
    //        console.log('studcheck',$scope.studcheck);
    //        console.log('$scope.installmentAmount',$scope.installmentAmount);
    //        console.log('$scope.loanapplied',$scope.loanapplied);
    //        console.log('$scope.schememaster.stage',$scope.schememaster.stage);
    //         console.log('$scope.studcheck.length',$scope.studcheck.length);
    $scope.paidamounts = $scope.installmentAmount * $scope.studcheck.length;
    //console.log('$scope.paidamounts', $scope.paidamounts);
    console.log('$scope.schememaster.paidamount', $scope.schememaster.paidamount);
    console.log('$scope.schememaster.amount', $scope.schememaster.amount);
    if ($scope.loanapplied == true && $scope.schememaster.stage == 7) {
      $scope.schememaster.paidamount = $scope.installmentAmount * $scope.studcheck.length;
    }
    if ($scope.schememaster.stage == "8") {
      $scope.schememaster.stage = "3";
      $scope.todo.status = 1;
    } else if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
      $scope.todo.status = 3;
    } else if ($scope.schememaster.stage == 9) { //|| $scope.schememaster.stage == 7) {
      $scope.todo.status = 3;
    } else if ($scope.schememaster.paidamount == $scope.schememaster.amount) {
      $scope.todo.status = 3;
    } else if ($scope.schememaster.purposeofloan != 5) {
      $scope.todo.status = 3;
      console.log('$scope.todo.status2', $scope.todo.status);
    } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
      $scope.todo.status = 3;
      console.log('$scope.todo.status1', $scope.todo.status);
    } else if ($scope.paidamounts == $scope.schememaster.amount) {
      $scope.todo.status = 3;
      console.log('$scope.todo.status3', $scope.todo.status);
    } else {
      $scope.todo.status = 1;
    }
    $scope.submitdocumentmasters.customPUT($scope.schememaster, $scope.schememaster.id).then(function (resp) {
      $scope.todo.reportincidentid = resp.id;
      $scope.todo.datetime = resp.datetime;
      $scope.todo.stage = resp.stage;
      console.log('schememaster', $scope.schememaster)
      $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function () {
        console.log('$scope.todo', $scope.todo);
        $scope.okAW();
      });
    });
  };
  $scope.surveyanswerrefer = {};
  $scope.UpdateCDIDS = function () {
    $scope.surveyanswerrefer.id = $scope.cdids.suransid;
    $scope.surveyanswerrefer.availeddate = $scope.cdids.availeddate;
    $scope.surveyanswerrefer.referenceno = $scope.cdids.referenceno;
    $scope.submitsurveyanswers.customPUT($scope.surveyanswerrefer, $scope.surveyanswerrefer.id).then(function () {
      $scope.okCDIDS();
      $scope.hideUpdateCDIDS = true;
      $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
    });
  };
  $scope.UpdateDocumentReference = function (surveyanswerid) {
    $scope.hideUpdateCDIDS = false;
    if (surveyanswerid != undefined && surveyanswerid != '' && surveyanswerid != null) {
      $scope.surveyanswer = Restangular.one('surveyanswers/findOne?filter[where][id]=' + surveyanswerid).get().then(function (surans) {
        ////console.log('surans', surans);
        $scope.cdids.availeddate = surans.availeddate;
        $scope.cdids.referenceno = surans.referenceno;
        $scope.cdids.suransid = surans.id;
        $scope.openCDIDS();
      });
    } else {
      $scope.openCDIDS();
    }
  };
  $scope.member = true;
  $scope.memberone = true;
  $scope.$watch('reportincident.co', function (newValue, oldValue) {
    ////console.log('reportincident.co', newValue);
    if (newValue === oldValue) {
      return;
    } else if ($scope.reportincident.co === false) {
      $scope.member = true;
      $scope.memberone = true;
    } else if ($scope.reportincident.co === true) {
      $scope.member = false;
      $scope.memberone = false;
      if ($scope.OthersReport == false) {
        $scope.reportincident.multiplemembers.push($routeParams.id);
        //console.log('$scope.reportincident.multiplemembers', $scope.reportincident.multiplemembers);
      }
    } else {
      $scope.member = true;
      $scope.memberone = true;
    }
  });
  $scope.reportincident.reported = false;
  $scope.report = true;
  $scope.$watch('reportincident.reported', function (newValue, oldValue) {
    //console.log('Report Too');
    if (newValue === oldValue) {
      return;
    } else if ($scope.reportincident.reported === true) {
      $scope.report = false;
    } else {
      $scope.report = true;
      $scope.timetorespond = true;
      $scope.reportincident.reportedcoteam = false;
      $scope.reportincident.timetorespond = false;
    }
  });
  $scope.reportincident.reportedcoteam = false;
  $scope.timetorespond = true;
  $scope.$watch('reportincident.reportedcoteam', function (newValue, oldValue) {
    //console.log('reportincident.reportedcoteam');
    if (newValue === oldValue) {
      return;
    } else if ($scope.reportincident.reportedcoteam === true) {
      $scope.timetorespond = false;
    } else {
      $scope.timetorespond = true;
      $scope.reportincident.timetorespond = false;
    }
  });
  $scope.followupdatehide = false;
  $scope.followuphide = false;
  $scope.dateofclosure = true;
  $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
    if ($scope.reportincident.currentstatus == null) {
      $scope.reportincident.currentstatus = 'Needs Follow Up';
    } else {
      if (newValue === oldValue) {
        return;
      } else if ($scope.reportincident.currentstatus === 'Needs Follow Up') {
        $scope.dateofclosure = true;
        $scope.followuphide = false;
        $scope.followupdatehide = false;
      } else if ($scope.reportincident.currentstatus === 'Resolved' || $scope.reportincident.currentstatus === 'Closed') {
        $scope.followuphide = true;
        $scope.dateofclosure = true;
        $scope.dateofclosure = false;
        $scope.followupdatehide = true;
        $scope.reportincident.dateofclosure = new Date();
      } else {
        $scope.dateofclosure = false;
      }
    };
  });

  $scope.$watch('reportincident.noactiontaken', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    } else if ($scope.reportincident.noactiontaken == true) {
      $scope.reportincident.currentstatus = 'Closed';
    } else {
      $scope.reportincident.currentstatus = 'Needs Follow Up';
    }
  });
  /*******************Loan Changes **********/
  $scope.noofmonthrepay_Hide = false;
  $scope.$watch('schememaster.purposeofloan', function (newValue, oldValue) {
    console.log('newValue', newValue);
    $scope.schememaster.amount = $scope.myamountvalue;
    if (newValue === oldValue) {
      return;
    } else if (newValue != 5) {
      $scope.schememaster.amount = $scope.myamountvalue;
      console.log('$scope.myamountvalue', $scope.myamountvalue);
      // $scope.currMonthyear = '';  
      $scope.hidecheckbox = true;
      $scope.noofmonthrepay_Hide = true;
    } else if (newValue == 5 && $scope.schememaster.amount == '') {
      $scope.schememaster.amount = $scope.myamountvalue;
      //   $scope.currMonthyear = '';
      $scope.purposeofloanhide = false;
      //  $scope.amountMonthrepay = true;
      $scope.hidecheckbox = false;
      //   $scope.noofmonthrepay_Hide = false;
    } else if (newValue == 5 && $scope.schememaster.amount != '') {
      $scope.noofmonthrepay_Hide = false;
      $scope.hidecheckbox = false;
    } else if (newValue == '' || newValue == null) {
      $scope.noofmonthrepay_Hide = true;
      $scope.amountMonthrepay = false;
      $scope.purposeofloanhide = false;
      $scope.currMonthyear = '';
      $scope.hidecheckbox = true;
    }
    $scope.current_LoanPurpose = newValue;
  });
  /* $scope.$watch('schememaster.purposeofloan', function (newValue, oldValue) {
       if (newValue === oldValue) {
           return;
       }
       else if ($scope.current_LoanPurpose != 5) {
           // $scope.current_LoanPurpose = newValue;
           $scope.noofmonthrepay_Hide = true;
           //$scope.schememaster.amount = '';
           $scope.currMonthyear = '';
       }
   });*/
  $scope.$watch('schememaster.amount', function (newValue, oldValue) {
    //  console.log('$scope.current_LoanPurpose',$scope.current_LoanPurpose);
    console.log('$scope.amount', newValue);
    if (newValue === oldValue) {
      return;
    } else if ($scope.current_LoanPurpose == 5 && newValue >= 10000) {
      $scope.noofmonthrepay_Hide = false;
      console.log('$scope.amount', newValue);
    } else {
      $scope.noofmonthrepay_Hide = true;
    }
  });
  $scope.studcheck = [];
  $scope.$watch('schememaster.noofmonthrepay', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    } else {
      $scope.studcheck = [];
      $scope.currMonthyear = [];
      for (var m = 1; m <= newValue; m++) {
        $scope.monthyear = new Date();
        $scope.monthyear.setDate($scope.monthyear.getDate() + 30 * m)
        $scope.monthyear = $filter('date')($scope.monthyear, 'MMMM-yyyy');
        //console.log('MonthYear',$scope.monthyear);
        $scope.currMonthyear.push($scope.monthyear);
        //console.log('$scope.currMonthyear', $scope.currMonthyear);
        //console.log('$scope.currMonthyear.monthtopay',$scope.monthtopay);
        //console.log('$scope.studcheck',$scope.studcheck.length)
        //sumanchanges
      }
      for (var k = 0; k < $scope.monthtopay; k++) {
        $scope.studcheck.push(true);
      }
    };
  });
  $scope.MonthRepay = function (monthrepay) {
    $scope.installment = $scope.schememaster.amount / monthrepay;
    console.log('$scope.installment', $scope.installment);
  }
  $scope.loanapplied = false;
  $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
    if (newValue == oldValue || newValue == undefined) {
      return;
    } else {
      //$scope.schememaster.stage = '';
      Restangular.one('schemes', newValue).get().then(function (SchemeMaster) {
        //  console.log('SchemeMaster', SchemeMaster);
        var Array = [];
        var Array2 = [];
        Array = SchemeMaster.category;
        Array2 = Array.split(',');
        console.log('Array2', Array2.length);
        for (var i = 0; i < Array2.length; i++) {
          console.log('Array.length', Array.length);
          console.log('Array', Array2[i]);
          if (Array2[i] == 15) {
            $scope.loanapplied = true;
            console.log('$scope.loanapplied', $scope.loanapplied);
          } else {
            $scope.loanapplied = false;
            //  console.log('Not Applied');
          }
        }
      });
    }
  });

  $scope.hide_Todo_Edit_Button = false;
  $scope.$watch('todo.status', function (newValue, oldValue) {
    if (newValue == oldValue || newValue == undefined) {
      return;
    } else if (newValue == 4 || newValue == 3) {
      //console.log('newValue status', newValue);
      $scope.hide_Todo_Edit_Button = true;
    } else {
      $scope.hide_Todo_Edit_Button = false;
    }
  });

  $scope.TestamountTaken = function () {
    $scope.schememaster.noofmonthrepay = '';
  }
  $scope.datetimehide = false;
  $scope.reponserec = true;
  $scope.delaydis = true;
  $scope.collectedrequired = true;
  $scope.purposeofloanhide = true;
  $scope.$watch('schememaster.stage', function (newValue, oldValue) {
    console.log('schememaster.stage', newValue);
    var fifteendays = new Date();
    fifteendays.setDate(fifteendays.getDate() + 15);
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    //if (newValue === oldValue || newValue == '' || newValue == undefined || oldValue == undefined) {
    if (newValue === oldValue) {
      return;
    } else if (newValue === '4') {
      $scope.reponserec = false;
      $scope.delaydis = true;
      $scope.datetimehide = false;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.purposeofloanhide = true;
      $scope.datetimehide = true;
    } else if (newValue === '5') {
      $scope.delaydis = false;
      $scope.reponserec = true;
      $scope.datetimehide = true;
      $scope.collectedrequired = false;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.datetime = fifteendays;
      $scope.purposeofloanhide = true;
    } else if (newValue === '2') {
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.datetimehide = true;
      $scope.collectedrequired = false;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.datetime = fifteendays;
      $scope.purposeofloanhide = true;
    } else if (newValue === '3') {
      $scope.schememaster.datetime = fifteendays;
      //console.log('$scope.schememaster.datetime3', $scope.schememaster.datetime);
      $scope.datetimehide = true;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.reponserec = true;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.schememaster.responserecieve = null;
      $scope.purposeofloanhide = true;
    } else if (newValue === '7') {
      var sixmonth = new Date();
      sixmonth.setDate(sixmonth.getDate() + 180);
      $scope.schememaster.datetime = new Date();
      $scope.datetimehide = false;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.schememaster.responserecieve = null;
      console.log('$scope.loanapplied', $scope.loanapplied);
      if ($scope.loanapplied == true && newValue === '7') {
        $scope.purposeofloanhide = false;
        //   $scope.amountMonthrepay = false;
        //    $scope.noofmonthrepay_Hide = true;
        $scope.datetimehide = false;
        var onemonth = new Date();
        onemonth.setDate(onemonth.getDate() + 30);
        $scope.schememaster.datetime = onemonth;
      }
    } else if (newValue === '1') {
      $scope.schememaster.datetime = fifteendays;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.purposeofloanhide = true;
      $scope.datetimehide = true;
    } else if (newValue === '8') {
      $scope.datetimehide = true;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.datetime = sevendays;
      $scope.purposeofloanhide = true;
    } else if (newValue === '6' || newValue === '9') {
      $scope.datetimehide = true;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.purposeofloanhide = true;
    } else if (newValue === '9') {
      $scope.datetimehide = false;
      $scope.delaydis = true;
      $scope.reponserec = true;
      $scope.collectedrequired = true;
      $scope.rejectdis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.datetime = new Date();
      $scope.purposeofloanhide = true;
    } else {
      $scope.reponserec = true;
      $scope.delaydis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.delay = null;
      $scope.schememaster.datetime = $scope.schememaster.datetime;
      $scope.datetimehide = true;
      $scope.rejectdis = true;
      $scope.purposeofloanhide = true;
    }
  });
  $scope.rejectdis = true;
  $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 180);
    if (newValue === oldValue) {
      return;
    } else if (newValue === null) {
      return;
    } else if (newValue === 'Approved') {
      $scope.schememaster.datetime = sevendays;
      console.log('$scope.schememaster.datetime', $scope.schememaster.datetime);
    } else if (newValue === 'Rejected') {
      $scope.rejectdis = false;
      $scope.schememaster.datetime = new Date(); //$scope.schememaster.datetime;
      $scope.datetimehide = true;
    } else {
      $scope.schememaster.datetime = $scope.schememaster.datetime;
      $scope.rejectdis = true;
      $scope.schememaster.rejection = null;
      $scope.reponserec = true;
      $scope.delaydis = true;
      $scope.schememaster.responserecieve = null;
      $scope.schememaster.delay = null;
      $scope.datetimehide = false;
    }
  });
  /********************************************************************************************/
  $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false').getList().then(function (scheme) {
    $scope.printschemes = scheme;
    angular.forEach($scope.printschemes, function (member, index) {
      member.index = index;
      member.enabled = false;
    });
  });
  $scope.docm = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (docmt) {
    $scope.printdocuments = docmt;
    angular.forEach($scope.printdocuments, function (member, index) {
      member.index = index;
      member.enabled = false;
    });
  });
  $scope.getGender = function (genderId) {
    return Restangular.one('genders', genderId).get().$object;
  };
  $scope.schememaster = {
    attendees: []
  };
  /* $scope.rejectdis = true;
   $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
       if (newValue === oldValue) {
           return;
       } else if (newValue === 'Rejected') {
           $scope.rejectdis = false;
       } else {
           $scope.rejectdis = true;
           $scope.schememaster.rejection = null;
       }
   });*/
  $scope.schememaster.attendees = [];
  $scope.$watch('schememaster.attendees', function (newValue, oldValue) {
    // //console.log('watch.attendees', newValue);
    if (newValue === oldValue || newValue == undefined) {
      return;
    } else {
      if (newValue.length > oldValue.length) {
        // something was added
        var Array1 = newValue;
        var Array2 = oldValue;
        for (var i = 0; i < Array2.length; i++) {
          var arrlen = Array1.length;
          for (var j = 0; j < arrlen; j++) {
            if (Array2[i] == Array1[j]) {
              Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
            }
          }
        }
        $scope.printschemes[Array1[0]].enabled = true;
        // do stuff
      } else if (newValue.length < oldValue.length) {
        // something was removed
        var Array1 = oldValue;
        var Array2 = newValue;
        for (var i = 0; i < Array2.length; i++) {
          var arrlen = Array1.length;
          for (var j = 0; j < arrlen; j++) {
            if (Array2[i] == Array1[j]) {
              Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
            }
          }
        }
        $scope.printschemes[Array1[0]].enabled = false;
      }
    }
  });
  /************************************************ Search On Modal *************************/
  $scope.schemename = $scope.name;
  $scope.$watch('schememaster.agegrp', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    } else {
      $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
        $scope.schemes = scheme;
        angular.forEach($scope.schemes, function (member, index) {
          member.index = index;
          member.enabled = false;
        });
      });
    }
  });
  $scope.$watch('schememaster.gender', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    } else {
      $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
        $scope.schemes = scheme;
        angular.forEach($scope.schemes, function (member, index) {
          member.index = index;
          member.enabled = false;
        });
      });
    }
  });
  $scope.$watch('schememaster.stateid', function (newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    } else {
      $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
        $scope.schemes = scheme;
        angular.forEach($scope.schemes, function (member, index) {
          member.index = index;
          member.enabled = false;
        });
      });
    }
  });
  /*
  $scope.openAW = function (size) {
  			$scope.modalAW = $modal.open({
  				animation: true,
  				templateUrl: 'template/ApplicationWorkflow.html',
  				scope: $scope,
  				backdrop: 'static',
  				size: size

  			});
  		};
  	*/
  $scope.openSchemeFlow = function (size) {
    $scope.modalSchemeFlow = $modal.open({
      animation: true,
      templateUrl: 'template/showschemesModalMain.html',
      scope: $scope,
      backdrop: 'static',
      size: size
    });
  };
  $scope.SaveSchemeFlow = function () {
    $scope.newArray = [];
    $scope.schememaster.attendees = [];
    for (var i = 0; i < $scope.printschemes.length; i++) {
      if ($scope.printschemes[i].enabled == true) {
        $scope.newArray.push($scope.printschemes[i].index);
      }
    }
    $scope.schememaster.attendees = $scope.newArray
    $scope.modalSchemeFlow.close();
  };
  $scope.okSchemeFlow = function () {
    $scope.modalSchemeFlow.close();
  };
  $scope.showschemesModal = false;
  $scope.toggleschemesModal = function () {
    $scope.SaveScheme = function () {
      $scope.newArray = [];
      $scope.schememaster.attendees = [];
      for (var i = 0; i < $scope.printschemes.length; i++) {
        if ($scope.printschemes[i].enabled == true) {
          $scope.newArray.push($scope.printschemes[i].index, $scope.printdocuments[j].index);
        }
      }
      $scope.schememaster.attendees = $scope.newArray
      //console.log('$scope.groupmeeting.attendees', $scope.schememaster.attendees.id);
      $scope.showschemesModal = !$scope.showschemesModal;
    };
    $scope.showschemesModal = !$scope.showschemesModal;
  };
  $scope.CancelScheme = function () {
    $scope.showschemesModal = !$scope.showschemesModal;
  };
  $scope.followupgroups = Restangular.all('followupgroups').getList().$object;
  $scope.submitgroupmeetings = Restangular.all('groupmeetings');
  $scope.fieldworkers4 = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
    $scope.fieldworkers = fwrResponse;
  });
  //$scope.fieldworkers = Restangular.all('fieldworkers').getList().$object;
  $scope.comembers = Restangular.all('comembers');
  $scope.applicastages = Restangular.all('applicationstages').getList().then(function (appstage) {
    $scope.applicationstages = appstage;
    angular.forEach($scope.applicationstages, function (member, index) {
      member.index = index + 1;
      member.enabled = false;
    });
  });
  $scope.getCO = function (partnerId) {
    return Restangular.one('comembers', partnerId).get().$object;
    //return Restangular.one('fieldworkers', partnerId).get().$object;
  };
  /*
  	$scope.getFW = function (partnerId) {
  		return Restangular.one('fieldworkers', partnerId).get().$object;
  	};*/
  $scope.getGroupMeetingTopics = function (partnerId) {
    return Restangular.all('groupmeetingtopics?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
  };
  $scope.getPilar = function (id) {
    return Restangular.all('pillars?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
  };
  /***********************************************************************************************/
  $scope.documenthide = true;
  $scope.documenthide = true;
  $scope.$watch('groupmeeting.organised', function (newValue, oldValue) {
    ////console.log('groupmeeting.organisedby', newValue);
    if (newValue === oldValue) {
      return;
    } else {
      $scope.groupmeeting.organisedby = newValue;
      if (newValue == 'Others') {
        $scope.documenthide = false;
        $scope.groupmeeting.organisedby = null;
      } else {
        $scope.documenthide = true;
      }
    }
  });
  /****************************************************************************************/
  $scope.groupcreate = false;
  var booleanValue = true;
  $scope.organisedbychange = true;
  $scope.Change = function () {
    if (booleanValue == false) {
      $scope.organisedbychange = true;
      $scope.groupcreate = true;
    } else {
      $scope.organisedbychange = false;
      $scope.groupcreate = true;
    }
  };
  $scope.groupmeeting = {
    attendees: [],
    follow: [],
    state: $window.sessionStorage.zoneId,
    district: $window.sessionStorage.salesAreaId,
    facility: $window.sessionStorage.coorgId,
    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
    lastmodifiedtime: new Date()
  };
  $scope.$watch('groupmeeting.attendees', function (newValue, oldValue) {
    // //console.log('watch.attendees', newValue);
    if (newValue === oldValue) {
      return;
    } else {
      $scope.attendeestodo = newValue;
      //console.log('$scope.attendeestodo', $scope.attendeestodo);
      //console.log('$scope.attendeestodo', $scope.attendeestodo);
      if (newValue.length > oldValue.length) {
        // something was added
        var Array1 = newValue;
        var Array2 = oldValue;
        for (var i = 0; i < Array2.length; i++) {
          var arrlen = Array1.length;
          for (var j = 0; j < arrlen; j++) {
            if (Array2[i] == Array1[j]) {
              Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
            }
          }
        }
        for (var i = 0; i < $scope.partners.length; i++) {
          if ($scope.partners[i].id == Array1[0]) {
            $scope.partners[i].enabled = true;
          }
        }
        // do stuff
      } else if (newValue.length < oldValue.length) {
        // something was removed
        var Array1 = oldValue;
        var Array2 = newValue;
        for (var i = 0; i < Array2.length; i++) {
          var arrlen = Array1.length;
          for (var j = 0; j < arrlen; j++) {
            if (Array2[i] == Array1[j]) {
              Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
            }
          }
        }
        for (var i = 0; i < $scope.partners.length; i++) {
          if ($scope.partners[i].id == Array1[0]) {
            $scope.partners[i].enabled = false;
          }
        }
        //$scope.partners[Array1[0]].enabled = false;
      }
    }
  });
  $scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
    $scope.zipmasters = zipmaster;
    angular.forEach($scope.zipmasters, function (member, index) {
      member.total = null;
      member.loader = null;
    });
  });
  $scope.checkbox = [];
  $scope.eventcheckbox = [];
  $scope.mobilenumbers = [];
  $scope.GetMobNumbers = function (index, zipcode, event) {
    ////console.log($scope.zipmasters[index]);
    if ($scope.zipmasters[index].enabled == true) {
      $scope.zipmasters[index].loader = 'images/loginloader.gif';
      //   //console.log($scope.checkbox[index], zipcode);
      $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
        $scope.prospects = surveyquestions;
        $scope.zipmasters[index].total = surveyquestions.length;
        $scope.zipmasters[index].loader = null;
        for (var i = 0; i < surveyquestions.length; i++) {
          surveyquestions[i].enabled = false;
          $scope.mobilenumbers.push(surveyquestions[i]);
        };
      });
    } else {
      $scope.zipmasters[index].total = null;
      $scope.zipmasters[index].loader = 'images/loginloader.gif';
      $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
        $scope.zipmasters[index].loader = null;
        for (var i = 0; i < surveyquestions.length; i++) {
          $scope.mobilenumbers.splice($scope.mobilenumbers.indexOf(surveyquestions[i].name), 1);
        };
      });
    }
  }
  $scope.Updategroupmeeting = function () {
    $scope.groupmeeting.attendeeslist = null;
    $scope.groupmeeting.followup = null;
    $scope.groupmeeting.pillarid = null;
    $scope.groupmeeting.topicsdiscussed = null;
    //if (document.getElementsByClassName('messageCheckbox').length > 0) {
    //$scope.validatestring = $scope.validatestring + 'Plese Select a Pillar';
    var checkedValue = null;
    var inputElements1 = document.getElementsByClassName('messageCheckbox');
    for (var i = 0; inputElements1[i]; ++i) {
      if (inputElements1[i].checked) {
        if (checkedValue == null) {
          checkedValue = inputElements1[i].value;
        } else {
          checkedValue = checkedValue + ',' + inputElements1[i].value;
        }
      }
    };
    $scope.groupmeeting.pillarid = checkedValue;
    var eventcheckedValue = null;
    var inputElements2 = document.getElementsByClassName('eventCheckbox');
    for (var i = 0; inputElements2[i]; ++i) {
      if (inputElements2[i].checked) {
        if (eventcheckedValue == null) {
          eventcheckedValue = inputElements2[i].value;
        } else {
          eventcheckedValue = eventcheckedValue + ',' + inputElements2[i].value;
        }
      }
    }
    $scope.groupmeeting.topicsdiscussed = eventcheckedValue;
    var checkedValue1 = null;
    var inputElements3 = document.getElementsByClassName('attendcheckbox');
    for (var i = 0; inputElements3[i]; ++i) {
      if (inputElements3[i].checked) {
        if (checkedValue1 == null) {
          checkedValue1 = inputElements3[i].value;
        } else {
          checkedValue1 = checkedValue1 + ',' + inputElements3[i].value;
        }
      }
    }
    $scope.groupmeeting.attendeeslist = checkedValue1;
    if ($scope.groupmeeting.follow != 'None') {
      //	$scope.groupmeeting.followup = null;
      //}	else {
      for (var i = 0; i < $scope.groupmeeting.follow.length; i++) {
        if (i == 0) {
          $scope.groupmeeting.followup = $scope.groupmeeting.follow[i];
        } else {
          $scope.groupmeeting.followup = $scope.groupmeeting.followup + ',' + $scope.groupmeeting.follow[i];
        }
      }
    } else {
      $scope.groupmeeting.followup = null;
    }
    if ($scope.groupmeeting.name == '' || $scope.groupmeeting.name == null) {
      $scope.validatestring = $scope.validatestring + 'Plese Enter Group Meeting Name';
    } else if (checkedValue === null) {
      $scope.validatestring = $scope.validatestring + 'Plese Select Pillars';
    } else if (eventcheckedValue === null) {
      $scope.validatestring = $scope.validatestring + 'Plese Select Topics';
    } else if ($scope.groupmeeting.organised == '' || $scope.groupmeeting.organised == null) {
      $scope.validatestring = $scope.validatestring + 'Plese Select a Organised By';
    }
    if ($scope.validatestring != '') {
      $scope.toggleValidation();
      $scope.validatestring1 = $scope.validatestring;
      $scope.validatestring = '';
    } else {
      $scope.submitgroupmeetings.customPUT($scope.groupmeeting, $scope.groupmeeting.id).then(function () {
        //console.log('groupmeeting Saved', $scope.groupmeeting);
        $scope.okGroupMeeting();
      });
    };
  };
  //Datepicker settings start
  $scope.today = function () {
    $scope.dt = new Date();
    //$scope.todo.datetime = $filter('date')(new Date(), 'y-MM-dd');
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    $scope.reportincident.followupdate = sevendays;
    $scope.todo.datetime = sevendays;
  };
  // var nextmonth = new Date();
  // nextmonth.setMonth(sevendays.getMonth + 1);
  // $scope.cdids.maxdate = nextmonth;
  var oneday = new Date();
  oneday.setDate(oneday.getDate() + 1);
  $scope.reportincidentdtmin = new Date();
  $scope.today();
  $scope.condom.dt = new Date();
  $scope.picker.dt = new Date();
  $scope.picker.selectdate = new Date();
  $scope.condom.dateasked = new Date();
  $scope.plhiv.month = new Date();
  $scope.plhiv.maxdate = new Date();
  $scope.cdids.availeddate = new Date();
  $scope.cdidsmaxdate = new Date();
  $scope.schememaster.datetime = new Date();
  $scope.increment.selectdate = new Date();
  $scope.reportincident.incidentdate = new Date();
  $scope.reportincident.dtmax = new Date();
  $scope.condom.dateasked = new Date();
  $scope.todomin = new Date();
  $scope.schemedocmaxdt = new Date();
  $scope.showWeeks = true;
  $scope.toggleWeeks = function () {
    $scope.showWeeks = !$scope.showWeeks;
  };
  $scope.clear = function () {
    $scope.dt = null;
  };
  // Disable weekend selection
  $scope.disabled = function (date, mode) {
    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
  };
  $scope.toggleMin = function () {
    $scope.minDate = ($scope.minDate) ? null : new Date();
  };
  $scope.toggleMin();
  $scope.todoopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.todo.opened = true;
  };
  $scope.fsopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.fs.opened = true;
  };
  $scope.healthopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.health.opened = true;
  };
  $scope.spopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.sp.opened = true;
  };
  $scope.reportopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.reportincident.opened = true;
  };
  $scope.followopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.follow.opened = true;
  };
  $scope.pickeropen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.picker.opened = true;
  };
  $scope.cdidsopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.cdids.opened = true;
  };
  $scope.workflowopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.schememaster.opened = true;
  };
  $scope.plhivopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.plhiv.opened = true;
  };
  $scope.condomopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.condom.opened = true;
  };
  $scope.incrementopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.increment.opened = true;
  };
  $scope.folowupdateopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.schememaster.opened = true;
  };
  $scope.reportincidentopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.reportincident.opened = true;
  };
  $scope.reportincidentfollowupopen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.reportincident.followupdatepick = true;
  };
  $scope.reportincidentcloser = {};
  $scope.reportincidentcloseropen = function ($event, index) {
    //$event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    $scope.reportincidentcloser.closedate = true;
  };
  $scope.dateOptions = {
    'year-format': 'yy',
    'starting-day': 1
  };
  $scope.monthOptions = {
    formatYear: 'yyyy',
    startingDay: 1,
    minMode: 'month'
  };
  $scope.mode = 'month';
  $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
  $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.monthformat = $scope.monthformats[0];
  //Datepicker settings end
  //////////////////////////////////////////////////////////Analytics Start////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////Analytics End////////////////////////////////////////////////////////
  console.log("Dashboard Map");

  if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
    $scope.showAssign = false;
  } else if ($window.sessionStorage.roleId === '5') {
    $scope.showAssign = false;
  } else {
    $scope.showAssign = true;
  }
  /////////////////////////map//////////////////////////////////////
  // if ($window.sessionStorage.roleId == 1) {
    if ($window.sessionStorage.roleId == 1) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });

    Restangular.all('ticket_status_role_view?[deleteflag]=false').getList().then(function (members) {
      $scope.members = members;
      console.log("members-3630", members);

      var latitude = [];
      var longitude = [];
      var status = [];
      var approvalp = [];
      var fullname = [];
      var startdate = [];
      var enddate = [];
      var customername = [];
      var category = [];
      var subcategory = [];
      var eta = [];
      var formatedDate = [];
      var formatedDate1 = [];
      var formatedDate2 = [];
      for (var j = 0; j < members.length; j++) {
        latitude[j] = members[j].latitude;
        longitude[j] = members[j].longitude;
        status[j] = members[j].todostatusId;
        approvalp[j] = members[j].approvalFlag;
        fullname[j] = members[j].fullname;
        formatedDate1[j] = members[j].starttime;

        if (formatedDate1[j] == null) {
          startdate[j] = formatedDate1[j]
        } else {
          startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        formatedDate2[j] = members[j].endtime;

        if (formatedDate2[j] == null) {
          enddate[j] = formatedDate2[j]
        } else {
          enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        category[j] = members[j].categoryname;
        subcategory[j] = members[j].subcategoryname;
        customername[j] = members[j].rccustomername;
        formatedDate[j] = members[j].etatime;
        if (formatedDate[j] == null) {
          eta[j] = formatedDate[j]
        } else {
          eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

      }
      console.log('tst', formatedDate[0])
      // if(eta[j] = members[j].etatime == null){
      //   eta[j] = members[j].etatime;
      // } else {
      //   eta[j] = members[j].etatime.toString("MM/dd/yyyy HH:mm:ss");
      // }
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: new google.maps.LatLng(21.7679, 78.8718),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var marker;
      var statusname = [];
      var image = [];
      var background = [];

      for (var l = 0; l < members.length; l++) {
        if (status[l] == 1) {
          image[l] = {
            url: "images/piclinks/open.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "OPEN";

        } else if (status[l] == 5) {
          image[l] = {
            url: "images/piclinks/complete.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "COMPLETED";

        } else if (status[l] == 3) {
          image[l] = {
            url: "images/piclinks/assign.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "ASSIGN";

        } else if (status[l] == 4) {
          image[l] = {
            url: "images/piclinks/inprogress.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "IN PROGRESS";

        } else if (status[l] == 6) {
          image[l] = {
            url: "images/piclinks/approval.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "APPROVAL PENDING";

        } else if (status[l] == 2) {
          image[l] = {
            url: "images/piclinks/backbench.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "BACK OFFICE";

        }
      }

      var infowindow = new google.maps.InfoWindow();


      for (var i = 0; i < members.length; i++) {


        marker = new google.maps.Marker({
          position: new google.maps.LatLng(latitude[i], longitude[i]),
          map: map,
          stylers: background[i],
          icon: image[i]
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            // infowindow.setContent(infoWindowContent1[i][0]);
            if (status[i] == 1) {
              infowindow.setContent(
                '<div style="background-color: #2f68d4;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 5) {
              infowindow.setContent(
                '<div style="background-color: #ff0ada;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 3) {
              infowindow.setContent(
                '<div style="background-color: #9d0cb0;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 4) {
              infowindow.setContent(
                '<div style="background-color: #ff7919;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 6) {
              infowindow.setContent(
                '<div style="background-color: #008080;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 2) {
              infowindow.setContent(
                '<div style="background-color: #800000;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            }
            infowindow.open(map, marker);

          }
        })(marker, i));
      }

    });
  } 
  else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });

    Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId).getList().then(function (members) {
      $scope.members = members;
      console.log("members-3846", members);

      var latitude = [];
      var longitude = [];
      var status = [];
      var approvalp = [];
      var fullname = [];
      var startdate = [];
      var enddate = [];
      var customername = [];
      var category = [];
      var subcategory = [];
      var eta = [];
      var formatedDate = [];
      var formatedDate1 = [];
      var formatedDate2 = [];
      for (var j = 0; j < members.length; j++) {
        latitude[j] = members[j].latitude;
        longitude[j] = members[j].longitude;
        status[j] = members[j].todostatusId;
        approvalp[j] = members[j].approvalFlag;
        fullname[j] = members[j].fullname;
        formatedDate1[j] = members[j].starttime;

        if (formatedDate1[j] == null) {
          startdate[j] = formatedDate1[j]
        } else {
          startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        formatedDate2[j] = members[j].endtime;

        if (formatedDate2[j] == null) {
          enddate[j] = formatedDate2[j]
        } else {
          enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        category[j] = members[j].categoryname;
        subcategory[j] = members[j].subcategoryname;
        customername[j] = members[j].rccustomername;
        formatedDate[j] = members[j].etatime;
        if (formatedDate[j] == null) {
          eta[j] = formatedDate[j]
        } else {
          eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

      }
      console.log('tst', formatedDate[0])
      // if(eta[j] = members[j].etatime == null){
      //   eta[j] = members[j].etatime;
      // } else {
      //   eta[j] = members[j].etatime.toString("MM/dd/yyyy HH:mm:ss");
      // }
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: new google.maps.LatLng(21.7679, 78.8718),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var marker;
      var statusname = [];
      var image = [];
      var background = [];

      for (var l = 0; l < members.length; l++) {
        if (status[l] == 1) {
          image[l] = {
            url: "images/piclinks/open.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "OPEN";

        } else if (status[l] == 5) {
          image[l] = {
            url: "images/piclinks/complete.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "COMPLETED";

        } else if (status[l] == 3) {
          image[l] = {
            url: "images/piclinks/assign.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "ASSIGN";

        } else if (status[l] == 4) {
          image[l] = {
            url: "images/piclinks/inprogress.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "IN PROGRESS";

        } else if (status[l] == 6) {
          image[l] = {
            url: "images/piclinks/approval.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "APPROVAL PENDING";

        } else if (status[l] == 2) {
          image[l] = {
            url: "images/piclinks/backbench.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "BACK OFFICE";

        }
      }

      var infowindow = new google.maps.InfoWindow();


      for (var i = 0; i < members.length; i++) {


        marker = new google.maps.Marker({
          position: new google.maps.LatLng(latitude[i], longitude[i]),
          map: map,
          stylers: background[i],
          icon: image[i]
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            // infowindow.setContent(infoWindowContent1[i][0]);
            if (status[i] == 1) {
              infowindow.setContent(
                '<div style="background-color: #2f68d4;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 5) {
              infowindow.setContent(
                '<div style="background-color: #ff0ada;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 3) {
              infowindow.setContent(
                '<div style="background-color: #9d0cb0;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 4) {
              infowindow.setContent(
                '<div style="background-color: #ff7919;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 6) {
              infowindow.setContent(
                '<div style="background-color: #008080;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 2) {
              infowindow.setContent(
                '<div style="background-color: #800000;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            }
            infowindow.open(map, marker);

          }
        })(marker, i));
      }

    });
  }

  if ($window.sessionStorage.roleId == 8) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });

    Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
      $scope.members = members;
      console.log("members-3574", members);

      var latitude = [];
      var longitude = [];
      var status = [];
      var approvalp = [];
      var fullname = [];
      var startdate = [];
      var enddate = [];
      var customername = [];
      var category = [];
      var subcategory = [];
      var eta = [];
      var formatedDate = [];
      var formatedDate1 = [];
      var formatedDate2 = [];
      for (var j = 0; j < members.length; j++) {
        latitude[j] = members[j].latitude;
        longitude[j] = members[j].longitude;
        status[j] = members[j].todostatusId;
        approvalp[j] = members[j].approvalFlag;
        fullname[j] = members[j].fullname;
        formatedDate1[j] = members[j].starttime;

        if (formatedDate1[j] == null) {
          startdate[j] = formatedDate1[j]
        } else {
          startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        formatedDate2[j] = members[j].endtime;

        if (formatedDate2[j] == null) {
          enddate[j] = formatedDate2[j]
        } else {
          enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        category[j] = members[j].categoryname;
        subcategory[j] = members[j].subcategoryname;
        customername[j] = members[j].rccustomername;
        formatedDate[j] = members[j].etatime;
        if (formatedDate[j] == null) {
          eta[j] = formatedDate[j]
        } else {
          eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

      }
      console.log('tst', formatedDate[0])
      // if(eta[j] = members[j].etatime == null){
      //   eta[j] = members[j].etatime;
      // } else {
      //   eta[j] = members[j].etatime.toString("MM/dd/yyyy HH:mm:ss");
      // }
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: new google.maps.LatLng(21.7679, 78.8718),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var marker;
      var statusname = [];
      var image = [];
      var background = [];

      for (var l = 0; l < members.length; l++) {
        if (status[l] == 1) {
          image[l] = {
            url: "images/piclinks/open.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "OPEN";

        } else if (status[l] == 5) {
          image[l] = {
            url: "images/piclinks/complete.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "COMPLETED";

        } else if (status[l] == 3) {
          image[l] = {
            url: "images/piclinks/assign.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "ASSIGN";

        } else if (status[l] == 4) {
          image[l] = {
            url: "images/piclinks/inprogress.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "IN PROGRESS";

        } else if (status[l] == 6) {
          image[l] = {
            url: "images/piclinks/approval.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "APPROVAL PENDING";

        } else if (status[l] == 2) {
          image[l] = {
            url: "images/piclinks/backbench.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "BACK OFFICE";

        }
      }

      var infowindow = new google.maps.InfoWindow();


      for (var i = 0; i < members.length; i++) {


        marker = new google.maps.Marker({
          position: new google.maps.LatLng(latitude[i], longitude[i]),
          map: map,
          stylers: background[i],
          icon: image[i]
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            // infowindow.setContent(infoWindowContent1[i][0]);
            if (status[i] == 1) {
              infowindow.setContent(
                '<div style="background-color: #2f68d4;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 5) {
              infowindow.setContent(
                '<div style="background-color: #ff0ada;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 3) {
              infowindow.setContent(
                '<div style="background-color: #9d0cb0;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 4) {
              infowindow.setContent(
                '<div style="background-color: #ff7919;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 6) {
              infowindow.setContent(
                '<div style="background-color: #008080;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 2) {
              infowindow.setContent(
                '<div style="background-color: #800000;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            }
            infowindow.open(map, marker);

          }
        })(marker, i));
      }

    });
  }
  if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });
    console.log("Here-3787")
    //console.log($window.sessionStorage.orgStructure, "$window.sessionStorage.orgStructure-3787")
    // from here new code ---
    var str = $scope.orgStructureFilter;
    var strOrgstruct = str.split('"');
    //console.log(strOrgstruct[1], "strorgstruct-638", strOrgstruct)
    $scope.strOrgstructFinal = [];
    for (var x = 0; x < strOrgstruct.length; x++) {
      if (x % 2 != 0) {
        $scope.strOrgstructFinal.push(strOrgstruct[x]);
      }
    }
    console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-3799")
    // $scope.count = 0;
    // $scope.membersFinal = [];
    $scope.membersFinalMgr = [];
    console.log($scope.membersFinalMgr, "$scope.membersFinal-3803")
    $scope.membersnewmgr = '';
    for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
      // Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
      Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
        $scope.membersnewmgr = members;
        // console.log("members-3806", members);
        console.log($scope.membersnewmgr.length, $scope.membersnewmgr, "members-3809");

        if ($scope.membersnewmgr.length > 0) {
          angular.forEach($scope.membersnewmgr, function (value, index) {
            $scope.membersFinalMgr.push(value);
          });
        }
        $scope.members = $scope.membersFinalMgr;
        console.log($scope.members, $scope.membersFinalMgr.length, "$scope.members-3814")
        // $scope.membersmgrdata = $scope.membersFinal;
        // console.log("membersmgrdata-3814", $scope.membersmgrdata);
        // $scope.statusCount($scope.membersmgrdata);
        // $scope.membersdata = $scope.membersFinal;
        // console.log("datas", $scope.membersdata);
        // $scope.statusCount($scope.membersdata);

        // angular.forEach($scope.members, function (member, index) {
        //   member.index = index + 1;

        var latitude = [];
        var longitude = [];
        var status = [];
        var approvalp = [];
        var fullname = [];
        var startdate = [];
        var enddate = [];
        var customername = [];
        var category = [];
        var subcategory = [];
        var eta = [];
        var formatedDate = [];
        var formatedDate1 = [];
        var formatedDate2 = [];
        console.log($scope.membersFinalMgr.length, "MSG-3834")
        for (var j = 0; j < $scope.members.length; j++) {
          latitude[j] = $scope.members[j].latitude;
          longitude[j] = $scope.members[j].longitude;
          status[j] = $scope.members[j].todostatusId;
          approvalp[j] = $scope.members[j].approvalFlag;
          fullname[j] = $scope.members[j].fullname;
          formatedDate1[j] = $scope.members[j].starttime;

          if (formatedDate1[j] == null) {
            startdate[j] = formatedDate1[j]
          } else {
            startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

          formatedDate2[j] = $scope.members[j].endtime;

          if (formatedDate2[j] == null) {
            enddate[j] = formatedDate2[j]
          } else {
            enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

          category[j] = $scope.members[j].categoryname;
          subcategory[j] = $scope.members[j].subcategoryname;
          customername[j] = $scope.members[j].rccustomername;
          formatedDate[j] = $scope.members[j].etatime;
          if (formatedDate[j] == null) {
            eta[j] = formatedDate[j]
          } else {
            eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

        }

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: new google.maps.LatLng(21.7679, 78.8718),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker;
        var statusname = [];
        var image = [];
        var background = [];

        for (var l = 0; l < $scope.members.length; l++) {
          if (status[l] == 1) {
            image[l] = {
              url: "images/piclinks/open.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "OPEN";

          } else if (status[l] == 5) {
            image[l] = {
              url: "images/piclinks/complete.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "COMPLETED";

          } else if (status[l] == 3) {
            image[l] = {
              url: "images/piclinks/assign.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "ASSIGN";

          } else if (status[l] == 4) {
            image[l] = {
              url: "images/piclinks/inprogress.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "IN PROGRESS";

          } else if (status[l] == 6) {
            image[l] = {
              url: "images/piclinks/approval.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "APPROVAL PENDING";

          } else if (status[l] == 2) {
            image[l] = {
              url: "images/piclinks/backbench.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "BACK OFFICE";

          }
        }

        var infowindow = new google.maps.InfoWindow();


        for (var i = 0; i < $scope.members.length; i++) {


          marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude[i], longitude[i]),
            map: map,
            stylers: background[i],
            icon: image[i]
          });
          google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
              // infowindow.setContent(infoWindowContent1[i][0]);
              if (status[i] == 1) {
                infowindow.setContent(
                  '<div style="background-color: #2f68d4;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 5) {
                infowindow.setContent(
                  '<div style="background-color: #ff0ada;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 3) {
                infowindow.setContent(
                  '<div style="background-color: #9d0cb0;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 4) {
                infowindow.setContent(
                  '<div style="background-color: #ff7919;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 6) {
                infowindow.setContent(
                  '<div style="background-color: #008080;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 2) {
                infowindow.setContent(
                  '<div style="background-color: #800000;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              }
              infowindow.open(map, marker);

            }
          })(marker, i));
        }
        //});
      });
    }


  }

  if ($window.sessionStorage.roleId == 5) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });
    //console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-4222")
    //var str = $scope.orgStructureFilter; //previously
    var str = $scope.orgStructureUptoStateFilter;
    var strOrgstruct = str.split('"')
    //console.log(strOrgstruct[1], "strorgstruct-578", strOrgstruct)

    $scope.strOrgstructFinal = [];
    for (var x = 0; x < strOrgstruct.length; x++) {
      if (x % 2 != 0) {
        $scope.strOrgstructFinal.push(strOrgstruct[x]);
      }
    }
    console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-4045")

    // from here ---
    // $scope.count = 0;
    // $scope.membersFinal = [];
    $scope.membersFinalSrMgr = [];
    $scope.membersnewsrmgr = '';
    for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
      //Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
      Restangular.all('ticket_status_role_view?[deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
        $scope.membersnewsrmgr = members;
        // console.log("members-4047", members);
        if ($scope.membersnewsrmgr.length > 0) {
          angular.forEach($scope.membersnewsrmgr, function (value, index) {
            $scope.membersFinalSrMgr.push(value);
          });
        }
        $scope.members = $scope.membersFinalSrMgr;
        console.log("members-4054", $scope.members);

        var latitude = [];
        var longitude = [];
        var status = [];
        var approvalp = [];
        var fullname = [];
        var startdate = [];
        var enddate = [];
        var customername = [];
        var category = [];
        var subcategory = [];
        var eta = [];
        var formatedDate = [];
        var formatedDate1 = [];
        var formatedDate2 = [];
        console.log('tred121', $scope.members.length)
      
        for (var j = 0; j < $scope.members.length; j++) {
          latitude[j] = $scope.members[j].latitude;
          console.log('tred', latitude[j])
          longitude[j] = $scope.members[j].longitude;
          status[j] = $scope.members[j].todostatusId;
          approvalp[j] = $scope.members[j].approvalFlag;
          fullname[j] = $scope.members[j].fullname;
          formatedDate1[j] = $scope.members[j].starttime;

          if (formatedDate1[j] == null) {
            startdate[j] = formatedDate1[j]
          } else {
            startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

          formatedDate2[j] = $scope.members[j].endtime;

          if (formatedDate2[j] == null) {
            enddate[j] = formatedDate2[j]
          } else {
            enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

          category[j] = $scope.members[j].categoryname;
          subcategory[j] = $scope.members[j].subcategoryname;
          customername[j] = $scope.members[j].rccustomername;
          formatedDate[j] = $scope.members[j].etatime;
          if (formatedDate[j] == null) {
            eta[j] = formatedDate[j]
          } else {
            eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
          }

        }

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: new google.maps.LatLng(21.7679, 78.8718),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker;
        var statusname = [];
        var image = [];
        var background = [];

        for (var l = 0; l < $scope.members.length; l++) {
          if (status[l] == 1) {
            image[l] = {
              url: "images/piclinks/open.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "OPEN";

          } else if (status[l] == 5) {
            image[l] = {
              url: "images/piclinks/complete.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "COMPLETED";

          } else if (status[l] == 3) {
            image[l] = {
              url: "images/piclinks/assign.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "ASSIGN";

          } else if (status[l] == 4) {
            image[l] = {
              url: "images/piclinks/inprogress.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "IN PROGRESS";

          } else if (status[l] == 6) {
            image[l] = {
              url: "images/piclinks/approval.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "APPROVAL PENDING";

          } else if (status[l] == 2) {
            image[l] = {
              url: "images/piclinks/backbench.png",
              scaledSize: new google.maps.Size(50, 50), // scaled size

            };
            statusname[l] = "BACK OFFICE";

          }
        }

        var infowindow = new google.maps.InfoWindow();


        for (var i = 0; i < $scope.members.length; i++) {

          console.log('tred1212', $scope.members.length)
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude[i], longitude[i]),
            map: map,
            stylers: background[i],
            icon: image[i]
          });
          google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
              // infowindow.setContent(infoWindowContent1[i][0]);
              if (status[i] == 1) {
                infowindow.setContent(
                  '<div style="background-color: #2f68d4;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 5) {
                infowindow.setContent(
                  '<div style="background-color: #ff0ada;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 3) {
                infowindow.setContent(
                  '<div style="background-color: #9d0cb0;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 4) {
                infowindow.setContent(
                  '<div style="background-color: #ff7919;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 6) {
                infowindow.setContent(
                  '<div style="background-color: #008080;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              } else if (status[i] == 2) {
                infowindow.setContent(
                  '<div style="background-color: #800000;padding: 10px;">' +
                  '<div class="info_content">' + fullname[i] + '</div>' +
                  '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                  '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
              }
              infowindow.open(map, marker);

            }
          })(marker, i));
        }

      });

    }


  }

  if ($window.sessionStorage.roleId == 11) {
    // Restangular.all('organisationlocations?filter[where][parent]=null&filter[where][deleteflag]=false').getList().then(function (city) {
    //   $scope.cities = city;
    // });

    Restangular.all('ticket_status_role_view?filter[where][assignFlag]=true&filter[where][assignedto]=' + window.sessionStorage.userId + '&access_token=' + window.sessionStorage.accessToken).getList().then(function (members) {
      $scope.members = members;
      console.log("members1", members);

      var latitude = [];
      var longitude = [];
      var status = [];
      var approvalp = [];
      var fullname = [];
      var startdate = [];
      var enddate = [];
      var customername = [];
      var category = [];
      var subcategory = [];
      var eta = [];
      var formatedDate = [];
      var formatedDate1 = [];
      var formatedDate2 = [];
      for (var j = 0; j < members.length; j++) {
        latitude[j] = members[j].latitude;
        longitude[j] = members[j].longitude;
        status[j] = members[j].todostatusId;
        approvalp[j] = members[j].approvalFlag;
        fullname[j] = members[j].fullname;
        formatedDate1[j] = members[j].starttime;

        if (formatedDate1[j] == null) {
          startdate[j] = formatedDate1[j]
        } else {
          startdate[j] = dayjs(formatedDate1[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        formatedDate2[j] = members[j].endtime;

        if (formatedDate2[j] == null) {
          enddate[j] = formatedDate2[j]
        } else {
          enddate[j] = dayjs(formatedDate2[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

        category[j] = members[j].categoryname;
        subcategory[j] = members[j].subcategoryname;
        customername[j] = members[j].rccustomername;
        formatedDate[j] = members[j].etatime;
        if (formatedDate[j] == null) {
          eta[j] = formatedDate[j]
        } else {
          eta[j] = dayjs(formatedDate[j]).format("YYYY-MMM-DD HH:mm:ss a")
        }

      }

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: new google.maps.LatLng(21.7679, 78.8718),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var marker;
      var statusname = [];
      var image = [];
      var background = [];

      for (var l = 0; l < members.length; l++) {
        if (status[l] == 1) {
          image[l] = {
            url: "images/piclinks/open.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "OPEN";

        } else if (status[l] == 5) {
          image[l] = {
            url: "images/piclinks/complete.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "COMPLETED";

        } else if (status[l] == 3) {
          image[l] = {
            url: "images/piclinks/assign.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "ASSIGN";

        } else if (status[l] == 4) {
          image[l] = {
            url: "images/piclinks/inprogress.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "IN PROGRESS";

        } else if (status[l] == 6) {
          image[l] = {
            url: "images/piclinks/approval.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "APPROVAL PENDING";

        } else if (status[l] == 2) {
          image[l] = {
            url: "images/piclinks/backbench.png",
            scaledSize: new google.maps.Size(50, 50), // scaled size

          };
          statusname[l] = "BACK OFFICE";

        }
      }

      var infowindow = new google.maps.InfoWindow();


      for (var i = 0; i < members.length; i++) {


        marker = new google.maps.Marker({
          position: new google.maps.LatLng(latitude[i], longitude[i]),
          map: map,
          stylers: background[i],
          icon: image[i]
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            // infowindow.setContent(infoWindowContent1[i][0]);
            if (status[i] == 1) {
              infowindow.setContent(
                '<div style="background-color: #2f68d4;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 5) {
              infowindow.setContent(
                '<div style="background-color: #ff0ada;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 3) {
              infowindow.setContent(
                '<div style="background-color: #9d0cb0;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 4) {
              infowindow.setContent(
                '<div style="background-color: #ff7919;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 6) {
              infowindow.setContent(
                '<div style="background-color: #008080;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            } else if (status[i] == 2) {
              infowindow.setContent(
                '<div style="background-color: #800000;padding: 10px;">' +
                '<div class="info_content">' + fullname[i] + '</div>' +
                '<div class="info_content">' + '<p>Customer:' + customername[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Category:' + category[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Subcategory:' + subcategory[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Status:' + statusname[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>Start Date:' + startdate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>End Date:' + enddate[i] + '</p>' + '</div>' +
                '<div class="info_content">' + '<p>ETA:' + eta[i] + '</p>' + '</div>' + '</div>');
            }
            infowindow.open(map, marker);

          }
        })(marker, i));
      }

    });
  }

});
//$scope.user = Restangular.one('users', 1).getList().$object;
/* var redirectTimeout;

var redirect = function() {
    $location.path("/login");
    
    //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
    Restangular.one('users/logout?access_token='+$window.sessionStorage.accessToken).post().then(function(logout){
        $window.sessionStorage.userId='';
		        //console.log('Logout');
		      });
}    

$timeout.cancel(redirectTimeout);

redirectTimeout = $timeout(function() {
    var timeoutTime = 30*60*1000 // 30 minutes
    redirectTimeout = $timeout(redirect, timeoutTime);
});*/
////////////////////////////////////////////auto=logout end////////////////////////////////////////////
