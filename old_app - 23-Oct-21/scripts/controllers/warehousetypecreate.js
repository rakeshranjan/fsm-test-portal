'use strict';
angular.module('secondarySalesApp')
    .controller('WarehousetypeCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        //console.log("$routeParams.id-5", $routeParams.id)
        $scope.showForm = function () {
            var visible = $location.path() === '/warehousetypecreate' || $location.path() === '/warehousetype/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/warehousetypecreate';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/warehousetypecreate';
            return visible;
        };

        if ($location.path() === '/warehousetypecreate') {
            $scope.disableDropdown = false;
          } else {
            $scope.disableDropdown = true;
          }
          /************************************************************************************/
          $scope.itemcategory = {
            //organizationId: $window.sessionStorage.organizationId,
            deleteflag: false
          };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        if ($window.sessionStorage.prviousLocation != "partials/warehousetypecreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // ******************************** EOF Pagination *************************************************

        /****************************************** CREATE *********************************/
      $scope.showValidation = false;
      $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
      };
      $scope.validatestring = '';
      $scope.submitDisable = false;

      $scope.Save = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.itemcategory, "message-282")
          if ($scope.warehousetype.warehousetype == '' || $scope.warehousetype.warehousetype == null) {
          $scope.warehousetype.warehousetype = null;
          $scope.validatestring = $scope.validatestring + 'Please  Enter Warehouse type';
          document.getElementById('name').style.border = "1px solid #ff0000";

          } 
        //   else if ($scope.itemstatus.description == '' || $scope.itemstatus.description == null) {
        //       $scope.itemstatus.description = null;
        //       $scope.validatestring = $scope.validatestring + 'Please  Enter Item Description';

        //   } 
          if ($scope.validatestring != '') {
          $scope.toggleValidation();
          $scope.validatestring1 = $scope.validatestring;
          $scope.validatestring = '';
          }
          else {
          $scope.message = 'Warehouse type has been created!';
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
          $scope.submitDisable = true;
          Restangular.all('warehousetypes').post($scope.warehousetype).then(function () {
              window.location = '/warehousetype';
          });
      }
      };

      /***************************** UPDATE *******************************************/

     $scope.validatestring = '';
     $scope.updatecount = 0;

     $scope.Update = function () {
     document.getElementById('name').style.border = "";
     //console.log($scope.itemcategory, "message-323")
         if ($scope.warehousetype.warehousetype == '' || $scope.warehousetype.warehousetype == null) {
         $scope.warehousetype.warehousetype = null;
         $scope.validatestring = $scope.validatestring + 'Please  Enter Warehouse Type';
         document.getElementById('name').style.border = "1px solid #ff0000";

         } 
         if ($scope.validatestring != '') {
         $scope.toggleValidation();
         $scope.validatestring1 = $scope.validatestring;
         $scope.validatestring = '';
         }
         else {
         $scope.message = 'Warehouse Type has been updated!';
         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
         $scope.neWsubmitDisable = true;
         Restangular.one('warehousetypes/' + $routeParams.id).customPUT($scope.warehousetype).then(function () {
             window.location = '/warehousetype';
             
         });
       }
     };

       if ($routeParams.id) {
         console.log("$routeParams.id", $routeParams.id)
         Restangular.one('warehousetypes', $routeParams.id).get().then(function (whrstyp) {
           $scope.original = whrstyp;
           $scope.warehousetype = Restangular.copy($scope.original);
         });
       }

});