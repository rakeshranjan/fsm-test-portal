'use strict';

angular.module('secondarySalesApp')
    .controller('SurveyQuestionsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/onetoonequestion/create' || $location.path() === '/onetoonequestion/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/onetoonequestion/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/onetoonequestion/create';
            return visible;
        };

        /*$scope.distributionArea = {
                zoneId: ''
            };*/

        /*  $scope.mybool = 'false';
          console.log('$scope.mybool', $scope.mybool);*/

        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View" && $window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }

        /*************************************************************************************************/

        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/

        $scope.submitsurveyquestns = Restangular.all('surveyquestions').getList().$object;


        $scope.part = Restangular.all('pillars?filter[where][deleteflag]=false' + '&filter[order]=order%20ASC').getList().then(function (part) {
            $scope.pillars = part;
            $scope.pillarid = $window.sessionStorage.myRoute;

            // console.log('$scope.pillarid', $scope.pillarid);

        });

        // $scope.salesAreas = Restangular.all('sales-areas').getList().$object;

        $scope.NotSubQuestion = true;
        $scope.NotSkipQuestion = true;
        $scope.YesNotSkipQuestion = true;
        $scope.NoNotSkipQuestion = true;
        $scope.NotCountedCheckbox = true;
        $scope.NotRange = true;
        $scope.NotScheme = true;

        $scope.$watch('surveyquestion.pillarid', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
                /* } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                    return;*/
            } else {
                $window.sessionStorage.myRoute = newValue;
                //$scope.todotypes = Restangular.all('todotypes?filter[where][pillarid]=' + newValue).getList().$object;
                //$scope.surveyquestion.yestodotype = $scope.original.yestodotype;
                Restangular.all('todotypes?filter[where][pillarid]=' + newValue).getList().then(function (Resp) {
                    $scope.todotypes = Resp;
                });
            }
        });

        $scope.$watch('surveyquestion.questiontype', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'sq') {
                $scope.NotSubQuestion = false;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'skp') {
                $scope.NotSkipQuestion = false;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'cc') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = false;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'rt') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = false;
                $scope.NotScheme = true;
            } else if (newValue == 'scheme') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = false;
            } else {
                $scope.NotSubQuestion = true;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            }
        });

        $scope.$watch('surveyquestion.yesaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.YesPopup = false;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = false;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'increment') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = false;
            } else {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });
        $scope.$watch('surveyquestion.noaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.NoPopup = false;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = false;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'increment') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = false;
            } else {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            }
        });


        $scope.YesTodo = true;
        $scope.YesDocument = true;
        $scope.NoTodo = true;
        $scope.NoDocument = true;
        $scope.$watch('surveyquestion.yespopup', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.YesTodo = false;
                $scope.YesDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;

            } else if (newValue == 'applicationflow') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;
            } else {
                $scope.YesTodo = true;
                $scope.YesDocument = true;
            }
            $scope.bool = $scope.original.yesdocumentflag;
            //console.log('$scope.bool', $scope.bool);
            $scope.surveyquestion.yesdocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.yesdocumentflag', $scope.surveyquestion.yesdocumentflag);
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });

        $scope.$watch('surveyquestion.nopopup', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.NoTodo = false;
                $scope.NoDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else if (newValue == 'applicationflow') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else {
                $scope.NoTodo = true;
                $scope.NoDocument = true;
            }
            $scope.bool = $scope.original.nodocumentflag;
            $scope.surveyquestion.nodocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.nodocumentflag', $scope.surveyquestion.nodocumentflag);
        });

        // $scope.yesdocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('surveyquestion.yesdocumentflag', function (newValue, oldValue) {
            //console.log('oldValue', oldValue);
            console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == 'true' || newValue == true) {
                    Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (response) {
                        $scope.yesdocumentsorschemes = response;
                        // $scope.surveyquestion = Restangular.copy($scope.original);
                        $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
                    });;
                    // console.log('$scope.yesdocumentsorschemes', $scope.yesdocumentsorschemes);
                } else {
                    $scope.yesdocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                    }, {
                        id: 2,
                        name: 'Two'
                    }, {
                        id: 3,
                        name: 'Three'
                    }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                    }];
                }
                //$scope.surveyquestion = Restangular.copy($scope.original);
                $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);
        });

        $scope.$watch('surveyquestion.nodocumentflag', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == true || newValue == 'true') {
                    $scope.nodocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;
                } else {
                    $scope.nodocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                        }, {
                        id: 2,
                        name: 'Two'
                        }, {
                        id: 3,
                        name: 'Three'
                        }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                        }];
                }
            }
        });


        $scope.getSurveyType = function (surveytypeid) {
            return Restangular.one('surveytypes', surveytypeid).get().$object;
        };

        $scope.getPilllar = function (pillarid) {
            return Restangular.one('pillars', pillarid).get().$object;
        };
        /*********/

        //$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;

        /* $scope.surveytypeid = '';
         $scope.surveysubcategoryid = '';
         $scope.subcategoryid = '';
         $scope.surveyid = '';
         $scope.surveyquestions = {};*/
        $scope.pillarid = '';
        $scope.$watch('pillarid', function (newValue, oldValue) {
            //$scope.callSurvey = true;
            // console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.pillarid = +newValue;
                $window.sessionStorage.myRoute = newValue;

                // $scope.surveyquestions = Restangular.all('surveyquestions?filter[where][pillarid]=' + newValue + '&filter[order][0]=pillarid ASC&filter[order][1]=serialno ASC').getList().$object;

                $scope.SalRes = Restangular.all('surveyquestions?filter[where][pillarid]=' + newValue + '&filter[order][0]=pillarid ASC&filter[order][1]=serialno ASC').getList().then(function (SalRes) {
                    $scope.surveyquestions = SalRes;
                    angular.forEach($scope.surveyquestions, function (member, index) {
                        member.index = index + 1;

                    });
                });
            }
        });

        $scope.sub_surveyquestions = [];
        $scope.$watch('taskbreakup', function (newValue, oldValue) {
            if (newValue > 20) {
                $scope.taskbreakup = '';
                alert('Task Breakup Cannot Exceed 20');
            }
            if (newValue <= 20) {
                $scope.sub_surveyquestions = [];
                for (var i = 0; i < newValue; i++) {

                    $scope.sub_surveyquestions.push({
                        "question": null,
                        "gujrathi": null,
                        "surveytypeid": null,
                        "surveysubcategoryid": null,
                        "questiontype": null,
                        "noofdropdown": null,
                        "answers": null,
                        "questionid": null,
                        "questionno": null
                    });

                }

            }
        });

        /*$scope.$scope.surveyquestion = {
        	hnquestion: ''
        };*/
        $scope.validatestring = '';
        $scope.addRouteLinks = function (value) {
            //console.log('length', $scope.sub_surveyquestions.length);
            if ($scope.surveyquestion.pillarid == '' || $scope.surveyquestion.pillarid == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a pillar';
            } else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter question';
            } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter answer options';
            } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
            } else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                $scope.validatestring = $scope.validatestring + 'Please select action on yes';
            } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                $scope.validatestring = $scope.validatestring + 'Please select action on No';
            } else if ($scope.surveyquestion.hnquestion == '' || $scope.surveyquestion.hnquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Hindi';
            } else if ($scope.surveyquestion.hnansweroptions == '' || $scope.surveyquestion.hnansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.hnansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
            } else if ($scope.surveyquestion.kaquestion == '' || $scope.surveyquestion.kaquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Kannada';
            } else if ($scope.surveyquestion.kaansweroptions == '' || $scope.surveyquestion.kaansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.kaansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';

            } else if ($scope.surveyquestion.taquestion == '' || $scope.surveyquestion.taquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Tamil';
            } else if ($scope.surveyquestion.taansweroptions == '' || $scope.surveyquestion.taansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.taansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';

            } else if ($scope.surveyquestion.tequestion == '' || $scope.surveyquestion.tequestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Telugu';
            } else if ($scope.surveyquestion.teansweroptions == '' || $scope.surveyquestion.teansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.teansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';

            } else if ($scope.surveyquestion.maquestion == '' || $scope.surveyquestion.maquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Marathi';
            } else if ($scope.surveyquestion.maansweroptions == '' || $scope.surveyquestion.maansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.maansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                var count = 0;
                if ($scope.sub_surveyquestions.length == 0) {
                    $scope.submitsurveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                        console.log('main question saved', surveyresponse);
                        window.location = "/surveyquestions";
                    });
                } else {
                    $scope.submitsurveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                        console.log('main question saved', surveyresponse);
                        for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
                            $scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
                            $scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
                            $scope.sub_surveyquestions[i].questionid = surveyresponse.id;
                            $scope.submitsurveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
                                console.log('sub question saved', response);
                                count++;
                                if (count == $scope.sub_surveyquestions.length) {
                                    //$route.reload();
                                    window.location = "/surveyquestions";
                                }
                            });
                        }
                    });
                }
            };
        };

        /*
        		$scope.updateSurveyQuestion = function (value) {
        			console.log('length', $scope.sub_surveyquestions.length);
        			var count = 0;
        			if ($scope.sub_surveyquestions.length == 0) {
        				$scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
        					console.log('main question saved', surveyresponse);
        					window.location = "/surveyquestions";
        				});
        			} else {
        				$scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
        					console.log('main question saved', surveyresponse);
        					for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
        						$scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
        						$scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
        						$scope.sub_surveyquestions[i].questionid = surveyresponse.id;
        						$scope.submitsurveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
        							console.log('sub question saved', response);
        							count++;
        							if (count == $scope.sub_surveyquestions.length) {
        								//$route.reload();
        								window.location = "/surveyquestions";
        							}
        						});
        					}
        				});
        			}
        		};*/
        /*	$scope.updateSurveyQuestion = function (value) {
			console.log('length', $scope.sub_surveyquestions.length);
			var count = 0;
			if ($scope.sub_surveyquestions.length == 0) {
				$scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
					console.log('main question saved', surveyresponse);
					window.location = "/surveyquestions";
				});
			} else {
				$scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
					console.log('main question saved', surveyresponse);
					for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
						$scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
						$scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
						$scope.sub_surveyquestions[i].questionid = surveyresponse.id;
						$scope.submitsurveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
							console.log('sub question saved', response);
							count++;
							if (count == $scope.sub_surveyquestions.length) {
								//$route.reload();
								window.location = "/surveyquestions";
							}
						});
					}
				});
			}
		};*/

        $scope.updateSurveyQuestion = function (value) {
            if ($scope.surveyquestion.pillarid == '' || $scope.surveyquestion.pillarid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select a pillar';
            } else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question';
            } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            } else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select action on yes';
            } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select action on No';
            } else if ($scope.surveyquestion.hnquestion == '' || $scope.surveyquestion.hnquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Hindi';
            } else if ($scope.surveyquestion.hnansweroptions == '' || $scope.surveyquestion.hnansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.hnansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            } else if ($scope.surveyquestion.kaquestion == '' || $scope.surveyquestion.kaquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Kannada';
            } else if ($scope.surveyquestion.kaansweroptions == '' || $scope.surveyquestion.kaansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.kaansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            } else if ($scope.surveyquestion.taquestion == '' || $scope.surveyquestion.taquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Tamil';
            } else if ($scope.surveyquestion.taansweroptions == '' || $scope.surveyquestion.taansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.taansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            } else if ($scope.surveyquestion.tequestion == '' || $scope.surveyquestion.tequestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Telugu';
            } else if ($scope.surveyquestion.teansweroptions == '' || $scope.surveyquestion.teansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.teansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            } else if ($scope.surveyquestion.maquestion == '' || $scope.surveyquestion.maquestion == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Question in Marathi';
            } else if ($scope.surveyquestion.maansweroptions == '' || $scope.surveyquestion.maansweroptions == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Answer Options';
            } else if ($scope.surveyquestion.maansweroptions.indexOf(',') == -1) {
                $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes ,No';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                var count = 0;
                console.log('$scope.sub_surveyquestions.length', $scope.sub_surveyquestions.length);
                /*if ($scope.sub_surveyquestions.length == 0) {*/
                $scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
                    console.log('main question saved', surveyresponse);
                    window.location = "/surveyquestionmlanguage";
                });
                /* } else {
                           $scope.surveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
                                console.log('main question saved', surveyresponse);
                                window.location = "/surveyquestionmlanguage";
                                for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
                                    $scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
                                    $scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
                                    $scope.sub_surveyquestions[i].questionid = surveyresponse.id;
                                    $scope.surveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
                                        console.log('sub question saved', response);
                                        count++;
                                        if (count == $scope.sub_surveyquestions.length) {
                                            //$route.reload();
                                            window.location = "/surveyquestionmlanguage";
                                        }
                                    });
                                }

                            });*/
            };
            //};
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Survey question has been Updated!';
            Restangular.one('surveyquestions', $routeParams.id).get().then(function (surveyquestion) {
                $scope.original = surveyquestion;
                $scope.surveyquestion = Restangular.copy($scope.original);
                console.log('$scope.surveyquestion', $scope.surveyquestion);
                /* $scope.yesdocFlag = $scope.original.yesdocumentid;
                 console.log('$scope.yesdocFlag', $scope.yesdocFlag);*/
                /************new changes***************
                $scope.dcoschme = Restangular.all('documenttypes?filter[where][deleteflag]=' + 'false').getList().then(function (dcoschme) {
                    $scope.yesdocumentsorschemes = dcoschme;
                    console.log('$scope.yesdocumentsorschemes', $scope.yesdocumentsorschemes);

                });
                 $scope.surveyquestion.yesdocumentflag = $scope.original.yesdocumentflag;
                 console.log('$scope.surveyquestion.yesdocumentflag', $scope.surveyquestion.yesdocumentflag);*/

                /*******************************************/

            });
        } else {
            $scope.message = 'Survey question has been Created!';
        }
    });
