'use strict';

angular.module('secondarySalesApp')
  .controller('ApproveticketsListViewCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    console.log("Approve Ticket List View");
    $scope.modalTitle = 'Thank You';

    $scope.DisplayTodoStatus = [{
      name: '',
      disableLang: false,
      disableAdd: false,
      disableRemove: false,
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      langdark: 'images/Lgrey.png'
    }];

    $scope.Add = function (index) {

      $scope.myobj = {};

      var idVal = index + 1;

      $scope.DisplayTodoStatus.push({
        name: '',
        disableLang: false,
        disableAdd: false,
        disableRemove: false,
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        langdark: 'images/Lgrey.png'
      });
    };

    $scope.Remove = function (index) {
      var indexVal = index - 1;
      $scope.DisplayTodoStatus.splice(indexVal, 1);
    };

    $scope.myobj = {};

    $scope.typeChange = function (index) {
      $scope.DisplayTodoStatus[index].name = '';
      $scope.DisplayTodoStatus[index].disableLang = false;
      $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
    };

    $scope.levelChange = function (index) {
      $scope.DisplayTodoStatus[index].disableLang = true;
      $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
      $scope.DisplayTodoStatus[index].disableAdd = false;
      $scope.DisplayTodoStatus[index].disableRemove = false;
    };

    $scope.langModel = false;

    $scope.Lang = function (index, name, type) {

      $scope.lastIndex = index;

      angular.forEach($scope.languages, function (data) {
        data.lang = name;
        // console.log(data);
      });

      $scope.langModel = true;
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/customerslist-view" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    
    // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true').getList().then(function (membr) {
      Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (membr) {
      $scope.members = membr;
      angular.forEach($scope.members, function (member, index) {
        member.index = index + 1;

        Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
          $scope.categoryname = categ;
          for (var j = 0; j < $scope.categoryname.length; j++) {
            if (member.categoryId == $scope.categoryname[j].id) {
              member.categoryname = $scope.categoryname[j].name;
              break;
            }
          }
        });
        Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
          $scope.subcategoryname = subcateg;
          for (var j = 0; j < $scope.subcategoryname.length; j++) {
            if (member.subcategoryId == $scope.subcategoryname[j].id) {
              member.subcategoryname = $scope.subcategoryname[j].name;
              break;
            }
          }
        });
        Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
          $scope.username = userresp;
          for (var j = 0; j < $scope.username.length; j++) {
            if (member.assignedto == $scope.username[j].id) {
              member.username = $scope.username[j].username;
              break;
            }
          }
        });
        Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
          $scope.statusname = todostatusresp;
          for (var j = 0; j < $scope.statusname.length; j++) {
            if (member.statusId == $scope.statusname[j].id) {
              member.statusname = $scope.statusname[j].name;
              break;
            }
          }
        });

      });
      

    });


  });
