'use strict';

angular.module('secondarySalesApp')
    .controller('GoodscreateCtrl', function ($scope, $rootScope, $filter, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        $scope.soheaderr = {
            fromdepotdist: '',
            sodate: $filter('date')(new Date(), 'd/M/yyyy'),
            flag: false
        };

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catgresp) {
            $scope.categories = catgresp;
        });

        Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
        });

        Restangular.all('poheaders?filter[where][flag]=false' + '&filter[where][transferflag]=false' + '&filter[where][receivedflag]=false').getList().then(function (po) {
            $scope.poheaders = po;
        });

        Restangular.all('itemdefinitions').getList().then(function (itms) {
            $scope.itemdefsdsplay = itms;
        });

        $scope.$watch('soheaderr.pono', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null) {
                return;
            } else {
                Restangular.all('potrailers?filter[where][poheaderId]=' + newValue).getList().then(function (potrailer) {
                    $scope.soheaderArray = potrailer;

                    $scope.totalamount = '0';

                    angular.forEach($scope.soheaderArray, function (value, index) {
                        delete value.id;
                        $scope.totalamount = parseInt($scope.totalamount) + parseInt(value.totalamount);

                        var data = $scope.itemdefsdsplay.filter(function (arr) {
                            return arr.id == value.itemcode
                        })[0];

                        if (data != undefined) {
                            value.itemname = data.itemcode;
                            value.name = data.id;
                        }
                    });
                    $scope.soheaderr.totalamount = $scope.totalamount;
                });

                Restangular.one('poheaders', newValue).get().then(function (poheader) {
                    $scope.soheaderr.fromdepotdist = poheader.fromdistret;
                    $scope.soheaderr.todistret = poheader.todepotdist;
                    $scope.soheaderr.toaddress = poheader.toaddress;
                    $scope.soheaderr.ponumber = poheader.ponumber;
                    $scope.soheaderr.state = poheader.stae;
                    $scope.soheaderr.city = poheader.city;
                    $scope.soheaderr.pin = poheader.pin;
                    $scope.soheaderr.discount = poheader.discount;
                    $scope.soheaderr.tax = poheader.tax;
                    $scope.soheaderr.netamount = poheader.netamount;
                    $scope.soheaderr.login = poheader.login;
                    $scope.mypoheaderid = poheader.id;

                    $scope.soheaderr.customerId = poheader.customerId;
                    $scope.soheaderr.categoryId = poheader.categoryId;
                    $scope.soheaderr.subcategoryId = poheader.subcategoryId;

                    var b = poheader.ponumber.replace(/[A-Z]/g, '');
                    $scope.soheaderr.sonumber = 'SO' + b;

                    Restangular.one('warehouses', poheader.fromdistret).get().then(function (whs) {
                        $scope.lwhCode = whs.warehousename;

                        Restangular.one('partners', poheader.todepotdist).get().then(function (mwhs) {
                            $scope.vdrCode = mwhs.partnerCode;
                        });
                    });
                });
            }
        });

        $scope.removeRouteLinkItem = function (index) {
            var item = $scope.soheaderArray[index];
            $scope.soheaderArray.splice(index, 1);
            //console.log('rem obj', JSON.stringify(item));
            if (item && item.id !== undefined) {
                //  console.log('existing');
                Restangular.one('sotrailers', item.id).get().then(function (salesorder) {
                    salesorder.remove();
                });
            } else {
                // console.log('does not exist');
            }
        };

        $scope.lowChanged = function (scope, index) {
            // auto filling
            if (parseInt($scope.soheaderArray[index].quantityaccepted) > parseInt($scope.soheaderArray[index].quantity)) {
                $scope.soheaderArray[index].quantityaccepted = '';
            }
        };

        /** save so ***/

        $scope.poheaderupdate = {
            receivedflag: true
        };

        $scope.inheader = {};

        $scope.Save = function () {
            $scope.soheaderr.flag = false;
            $scope.soheaderr.approvedflag = true;

            Restangular.all('soheaders').post($scope.soheaderr).then(function (res) {
                Restangular.one('poheaders', $scope.mypoheaderid).customPUT($scope.poheaderupdate).then(function () {
                    $scope.saveCount = 0;
                    $scope.saveItems(res.id);
                });
            });
        };

        $scope.saveCount = 0;

        $scope.saveItems = function (headerId) {
            if ($scope.saveCount < $scope.soheaderArray.length) {
                $scope.soheaderArray[$scope.saveCount].sonumber = $scope.soheaderr.sonumber;
                $scope.soheaderArray[$scope.saveCount].soheaderid = headerId;
                $scope.soheaderArray[$scope.saveCount].customerId = $scope.soheaderr.customerId;
                $scope.soheaderArray[$scope.saveCount].categoryId = $scope.soheaderr.categoryId;
                $scope.soheaderArray[$scope.saveCount].subcategoryId = $scope.soheaderr.subcategoryId;

                Restangular.all('sotrailers').post($scope.soheaderArray[$scope.saveCount]).then(function (resp) {
                    $scope.inheader.itemstatusId = 3;
                    $scope.inheader.quantityinput = resp.quantityaccepted;
                    $scope.inheader.quantitytransfer = 0;
                    $scope.inheader.quantityreturn = 0;
                    $scope.inheader.customerId = $scope.soheaderr.customerId;
                    $scope.inheader.categoryId = $scope.soheaderr.categoryId;
                    $scope.inheader.subcategoryId = $scope.soheaderr.subcategoryId;
                    $scope.inheader.soheaderid = headerId;
                    $scope.inheader.sotrailerid = resp.id;
                    $scope.inheader.itemdefinitionId = resp.name;
                    $scope.inheader.warehouseId = $scope.soheaderr.fromdepotdist;

                    Restangular.all('inventories').post($scope.inheader).then(function (resp) {
                        $scope.saveCount++;
                        $scope.saveItems(headerId);
                    });
                });
            } else {
                window.location = '/goodreceipt';
            }
        };

        $scope.showViewForm = false;

        if ($routeParams.id) {
            $scope.showViewForm = true;

            Restangular.all('soheaders?filter[where][id]=' + $routeParams.id).getList().then(function (so) {
                $scope.soheadersList = so;
                angular.forEach($scope.soheadersList, function (member, index) {
                    member.index = index + 1;
                });
            });

            Restangular.all('sotrailers?filter[where][soheaderid]=' + $routeParams.id).getList().then(function (sotrailer) {
                $scope.sotrailersList = sotrailer;
                angular.forEach($scope.sotrailersList, function (member, index) {
                    member.index = index + 1;
                });
            });
        }

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getPartner = function (partnerid) {
            return Restangular.one('partners', partnerid).get().$object;
        };

        $scope.getItem = function (itemid) {
            return Restangular.one('itemdefinitions', itemid).get().$object;
        };
    });