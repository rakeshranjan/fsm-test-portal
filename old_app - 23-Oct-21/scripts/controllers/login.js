'use strict';
angular.module('secondarySalesApp').directive('loading', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loading" style="color:black;"><img src="images/ajax_loader.gif" width="20" height="20"  class="loadingwidth" /> <br/>LOADING...</div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val) $(element).show();
                else $(element).hide();
            });
        }
    }
}).directive('loading1', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loading" style="color:#333;"><img src="images/loader.gif" width="20" height="20" class="loadingwidth" /> </div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val) $(element).show();
                else $(element).hide();
            });
        }
    }
}).controller('LoginCtrl', function ($rootScope, $scope, $http, $window, $location, Restangular, $idle, $modal, AnalyticsRestangular, $filter) {
    //$scope.FilterDate = "2016-09-14T09:59:43.043Z";
    $scope.login = function () {
        $scope.loading = true;
        $scope.errormsg = 'hide';
        Restangular.one('users', 1);
        console.log('login');
        Restangular.all('users').login($scope.user).then(function (loginResult) {
            var session = $window.sessionStorage.loginResult = angular.toJson(loginResult);
            var sessionObj = angular.fromJson(session);
            $rootScope.accessToken = $window.sessionStorage.accessToken = sessionObj.id;
            $rootScope.currentUserId = $window.sessionStorage.userId = sessionObj.userId;
            $rootScope.currentUserTtl = $window.sessionStorage.userTtl = sessionObj.ttl;
        }).then(function () {
            Restangular.one('users/' + $window.sessionStorage.userId + '?access_token=' + $window.sessionStorage.accessToken).get().then(function (customer) {
                    var name = customer.username;
                    var roleid = customer.roleId;
                    var zoneId = customer.zoneId;
                    var salesAreaId = customer.salesAreaId;
                    var coorgId = customer.coorgId;
                    var groupId = customer.groupId;
                    var EmployeeId = customer.employeeid;
                    var language = customer.language;
                    var deleteFlag = customer.deleteflag;
                    var orgStructure = customer.orgStructure;
                    var customerId = customer.customerId;
                    var orgLevel = customer.level;
                    if (EmployeeId == null) {
                        EmployeeId = 0
                    }
                    $rootScope.currentUserName = $window.sessionStorage.userName = name;
                    $rootScope.roleId = $window.sessionStorage.roleId = roleid;
                    $rootScope.zoneId = $window.sessionStorage.zoneId = zoneId;
                    $rootScope.salesAreaId = $window.sessionStorage.salesAreaId = salesAreaId;
                    $rootScope.coorgId = $window.sessionStorage.coorgId = coorgId;
                    $rootScope.groupId = $window.sessionStorage.groupId = groupId;
                    $rootScope.customerId = $window.sessionStorage.customerId = customerId;
                    $rootScope.UserEmployeeId = $window.sessionStorage.UserEmployeeId = EmployeeId;
                    $rootScope.DeleteFlag = $window.sessionStorage.DeleteFlag = deleteFlag;
                    $rootScope.orgLevel = $window.sessionStorage.orgLevel = orgLevel;
                $rootScope.orgStructure = $window.sessionStorage.orgStructure =orgStructure;
                $rootScope.isPartner = $window.sessionStorage.isPartner = customer.isPartner;
                    if (customer.language == null || customer.language === undefined || customer.language === '') {
                        $rootScope.language = $window.sessionStorage.language = 1;
                    } else {
                        $rootScope.language = $window.sessionStorage.language = language;
                    }
                }).then(function () {
                   
                        window.location = "/";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                });
            //$scope.loading = false;
        }, function (response) {
            console.log('response', response);
            //alert(response.statusText);
            //$scope.errormsg = 'Invalid Username or Password';
            $scope.errormsg = 'show';
            $scope.loading = false;
        });
    };
    $scope.logout = function () {
        //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
        Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
            $window.sessionStorage.userId = '';
            console.log('Logout');
        }).then(function (redirect) {
            window.location = "/login";
            alert('There is No Access for this User');
            $idle.unwatch();
        });
    };
    $scope.showModal1 = function () {
        $scope.HideSearch = true;
        $scope.HideError = true;
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/forgotpassword.html',
            scope: $scope
        });
    };
    $scope.Brodcast = function () {
        console.log('Brodcast');
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/Brodcast_Message.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg'
        });
    };
    $scope.getEvent = function () {}
    $scope.ok1 = function () {
        $scope.modalInstance1.close();
    };
    $scope.OKBDM = function () {
        $scope.modalInstance1.close();
        window.location = "/";
    }
    $scope.data = {};
    $scope.GetUserId = function () {
        $scope.HideError = true;
        $scope.HideUser = $scope.data.forgotusername;
        Restangular.all('users?filter[where][username]=' + $scope.data.forgotusername).getList().then(function (detail) {
            if (detail.length > 0) {
                $scope.HideSearch = false;
                $scope.data.usermobile = detail[0].mobile;
                $scope.data.useremail = detail[0].email;
                $scope.data.userfirstname = detail[0].firstname;
                $scope.userId = detail[0].id;
            } else {
                $scope.data.forgotusername = '';
                $scope.HideError = false;
            }
        });
    };
    $scope.ResetPassword = function () {
        var randompswd = Math.floor(Math.random() * (666666 - 111111 + 111111)) + 111111;
        $scope.newdata = {
            password: $scope.stringGen(6)
        };
        console.log($scope.data.usermobile);
        if ($scope.data.usermobile != null && $scope.data.usermobile != '') {
            Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
                console.log('$scope.newdata', $scope.newdata);
                console.log('response', response);
                $http.post("http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=socialprotection&passwd=spsw@sti1&mobilenumber=91" + $scope.data.usermobile + "&message=Your new password is " + $scope.newdata.password + "&sid=919164022220&mtype=N&DR=Y").success(function (data, status) {
                    // console.log('status', status);
                });
                if ($scope.data.useremail != null) {
                    $scope.SendMail($scope.newdata.password, $scope.data.useremail, $scope.data.userfirstname);
                }
                $scope.ok1();
            });
        } else if ($scope.data.useremail != null) {
            $scope.SendMail($scope.newdata.password, $scope.data.useremail, $scope.data.userfirstname);
            $scope.ok1();
        } else {
            $scope.data.usermobile = 'Invalid Mobile Number';
        }
    };
    $scope.stringGen = function (len) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    $scope.SendMail = function (newpassword, email, name) {
        var mailJSON = {
            "key": "so5UoRRwxBY6vS_iIDz-GQ",
            "message": {
                "html": "<!DOCTYPE html><html><body><h2>Your New Password is " + newpassword + "</h2></body></html>",
                "text": "New Password",
                "subject": "New Password",
                "from_email": "hunterabhi246@gmail.com",
                "from_name": "Suren",
                "to": [
                    {
                        "email": email,
                        "name": name,
                        "type": "to"
            }
        ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null
            },
            "async": false,
            "ip_pool": "Main Pool"
        };
        var apiURL = "https://mandrillapp.com/api/1.0/messages/send.json";
        $http.post(apiURL, mailJSON).
        success(function (data, status, headers, config) {
            console.log('successful email send.');
            $scope.form = {};
            console.log('successful email send.');
            console.log('status: ' + status);
            console.log('data: ' + JSON.stringify(data));
            console.log('headers: ' + JSON.stringify(headers));
            console.log('config: ' + config);
        }).error(function (data, status, headers, config) {
            console.log('error sending email.');
            console.log('status: ' + status);
            console.log('data: ' + JSON.stringify(data));
        });
    }
}).directive('notification', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        controller: ['$scope', function ($scope) {
            $scope.notification = {
                status: 'hide',
                type: 'danger',
                message: 'Invalid Username or Password!!!'
            };
    }],
        link: function (scope, elem, attrs) {
            // watch for changes
            attrs.$observe('notification', function (value) {
                if (value === 'show') {
                    // shows alert
                    $(elem).slideDown();
                    // and after 3secs
                    $timeout(function () {
                        // hide it
                        $(elem).slideUp();
                        // and update the show property
                        scope.notification.status = 'hide';
                    }, 1000);
                }
            });
        }
    };
  }]);