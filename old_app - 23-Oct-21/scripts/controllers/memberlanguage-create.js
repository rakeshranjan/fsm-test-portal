'use strict';
angular.module('secondarySalesApp').controller('MemberLanguageCreateCtrl', function ($scope, Restangular, $filter, $timeout, $window, $route, $modal) {
    $scope.hideSave = true;
    $scope.hideUpdate = false;

    $scope.member = {
        fullname: '',
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        deleteflag: false,
        enableforroles: []
    };

    Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
        $scope.roles = role;
    });

    $scope.languageDisable = true;

       Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
           Restangular.one("roles/"+$window.sessionStorage.roleId).get().then(function(role){
             $scope.languages = langs;
               if(role.isLanguageUser){
                    $scope.member.language = $window.sessionStorage.language;
                   $scope.languageDisable = true;
               }else{
                   $scope.languageDisable = false;
               }
           });
           
        });

    /********************** Save Function *******************************/

    $scope.validatestring = "";

    $scope.Save = function (clicked) {
        if ($scope.member.language == '' || $scope.member.language == null) {
            $scope.validatestring = $scope.validatestring + 'Please Select Language';

        } else if ($scope.member.fullname == '' || $scope.member.fullname == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Full name';

        } else if ($scope.member.memberId == '' || $scope.member.memberId == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Member ID';

        } else if ($scope.member.dob == '' || $scope.member.dob == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter DOB';

        } else if ($scope.member.gender == '' || $scope.member.gender == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Gender';

        } else if ($scope.member.occupation == '' || $scope.member.occupation == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Occupation';

        } else if ($scope.member.education == '' || $scope.member.education == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Education';

        } else if ($scope.member.enableforroles == '' || $scope.member.enableforroles == null) {
            $scope.validatestring = $scope.validatestring + 'Please Select Enable for Roles';

        } else if ($scope.member.address == '' || $scope.member.address == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Address';

        } else if ($scope.member.age == '' || $scope.member.age == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Age';

        } else if ($scope.member.latitude == '' || $scope.member.latitude == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Latitude ';

        } else if ($scope.member.longitude == '' || $scope.member.longitude == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Longitude';

        } else if ($scope.member.stress == '' || $scope.member.stress == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Stress';

        } else if ($scope.member.saveLabel == '' || $scope.member.saveLabel == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Save';

        } else if ($scope.member.updateLabel == '' || $scope.member.updateLabel == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Update';

        } else if ($scope.member.cancelLabel == '' || $scope.member.cancelLabel == null) {
            $scope.validatestring = $scope.validatestring + 'Please Enter Cancel';

        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';

        } else {


            $scope.message = 'Member Language has been created!';

            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;

            Restangular.all('memberlanguages').post($scope.member).then(function (resp) {
                window.location = '/memberlanguage';
            });
        }
    };

    /*********************** end **************************************/

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
});
