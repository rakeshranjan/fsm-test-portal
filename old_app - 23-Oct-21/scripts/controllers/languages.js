'use strict';

angular.module('secondarySalesApp')
    .controller('LanguagesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/language/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/languages") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        $scope.DisplayLanguages = [{
            name: '',
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        }];

        $scope.Add = function (index) {

            var idVal = index + 1;

            if (idVal > 9) {
                return;
            } else {
                $scope.DisplayLanguages.push({
                    name: '',
                    disableAdd: false,
                    disableRemove: false,
                    deleteflag: false,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedrole: $window.sessionStorage.roleId,
                    lastmodifiedtime: new Date(),
                    createdby: $window.sessionStorage.userId,
                    createdtime: new Date(),
                    createdrole: $window.sessionStorage.roleId
                });
            }
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLanguages.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLanguages[index].disableAdd = false;
                $scope.DisplayLanguages[index].disableRemove = false;
            } else {
                $scope.DisplayLanguages[index].disableAdd = true;
                $scope.DisplayLanguages[index].disableRemove = true;
            }
        };

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('languages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        /********************************************** WATCH ***************************************/
        
        $scope.TotalTodos = [];

        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lng) {
            $scope.languages = lng;
            angular.forEach($scope.languages, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos.push(member);
            });
        });

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLanguage();
        };

        var saveCount = 0;

        $scope.saveLanguage = function () {

            if (saveCount < $scope.DisplayLanguages.length) {

                if ($scope.DisplayLanguages[saveCount].name == '' || $scope.DisplayLanguages[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLanguage();
                } else {
                    Restangular.all('languages').post($scope.DisplayLanguages[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLanguage();
                    });
                }

            } else {
                window.location = '/languages-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.language.name == '' || $scope.language.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Language';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                
                $scope.language.lastmodifiedby = $window.sessionStorage.userId;
                $scope.language.lastmodifiedtime = new Date();
                $scope.language.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('languages', $routeParams.id).customPUT($scope.language).then(function (resp) {
                    window.location = '/languages-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Language has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('languages', $routeParams.id).get().then(function (language) {
                $scope.original = language;
                $scope.language = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Language has been created!';
        }



    });
