'use strict';

angular.module('secondarySalesApp')
  .controller('MultiLanguageCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    console.log("Multi Language");
    // $scope.languagedefinitions = Restangular.all('languagedefinitions?filter[where][deleteFlag]=false').getList().$object;

    // $scope.$watch('langrcdetail.language', function (newValue, oldValue) {
    //   if (newValue === oldValue || newValue == '') {
    //     return;
    //   } else if ($routeParams.id) {
    //     return;
    //   } else {

    //     Restangular.all('languagedefinitions?filter[where][language]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (response) {
    //       console.log('response', response);
    //       if (response.length == 0) {
    //         $scope.HideCreateButton = true;
    //       } else {
    //         $scope.checkmodal = true;

    //       }
    //     });
    //   }
    // });

    $scope.showForm = function () {
      var visible = $location.path() === '/multilanguage/create' || $location.path() === '/multilanguage/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/multilanguage/create';
        return visible;
      }
    };

    $scope.langrcdetail = {
      deleteFlag: false
    };

    $scope.modalTitle = 'Thank You';
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';

    $scope.Save = function () {
      console.log('i m inside save');
      document.getElementById('ser').style.border = "";
      document.getElementById('rcstatus').style.border = "";
      document.getElementById('customer_name').style.border = "";

      if ($scope.langrcdetail.serno == '' || $scope.langrcdetail.serno == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter SER';
        document.getElementById('ser').style.border = "1px solid #ff0000";

      } else if ($scope.langrcdetail.rcstatus == '' || $scope.langrcdetail.rcstatus == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter RC Status';
        document.getElementById('rcstatus').style.border = "1px solid #ff0000";

      } else if ($scope.langrcdetail.customername == '' || $scope.langrcdetail.customername == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('customer_name').style.border = "1px solid #ff0000";

      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {

        $scope.langrcdetail.languageId = 1;
        $scope.langrcdetail.deleteflag = false;

        Restangular.all('rcdetailslanguages').post($scope.langrcdetail).then(function (conResponse) {
          console.log('conResponse', conResponse);
          $scope.message = 'Form Level has been created!';
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
          //toaster.pop('success', 'Language RC detail list created successfully');
          $scope.disableCreate = true;
          setTimeout(function () {
            window.location = '/multilanguagelist';
            // window.location = '/workflowlanguage';
          }, 350);
        });
      };

    };
    

    

    $scope.Update = function () {
      console.log('i m in update');

      document.getElementById('ser').style.border = "";
      document.getElementById('rcstatus').style.border = "";
      document.getElementById('customer_name').style.border = "";

      if ($scope.langrcdetail.serno == '' || $scope.langrcdetail.serno == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter SER';
        document.getElementById('ser').style.border = "1px solid #ff0000";

      } else if ($scope.langrcdetail.rcstatus == '' || $scope.langrcdetail.rcstatus == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter RC Status';
        document.getElementById('rcstatus').style.border = "1px solid #ff0000";

      } else if ($scope.langrcdetail.customername == '' || $scope.langrcdetail.customername == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('customer_name').style.border = "1px solid #ff0000";

      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
      Restangular.one('rcdetailslanguages', $routeParams.id).customPUT($scope.langrcdetail).then(function (conResponse) {
        $scope.message = 'Form Level has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        console.log('conResponse', conResponse);
       // toaster.pop('success', 'Language Lead List updated successfully');
        $scope.disableUpdate = true;
        setTimeout(function () {
          window.location = '/multilanguagelist';
        }, 350);
      });
    }
    };

    if ($routeParams.id) {
      $scope.HideCreateButton = false;
      $scope.langdisable = true;
      //           $scope.message = 'Form Langauge has been Updated!';
      Restangular.one('rcdetailslanguages', $routeParams.id).get().then(function (langrcdetail) {

        $scope.original = langrcdetail;
        $scope.langrcdetail = Restangular.copy($scope.original);
        console.log('$scope.todo', $scope.langrcdetail);

      });
    } 

  });
