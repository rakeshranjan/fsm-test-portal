'use strict';

angular.module('secondarySalesApp')
  .controller('CustomersCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
    /*********/

    $scope.showForm = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/customers/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };

    
    /************************************************************************************/
    $scope.customer = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteFlag: false
    };

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
   
    /****************************************** CREATE *********************************/

    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.savecustomer = function () {
      document.getElementById('name').style.border = "";
      console.log($scope.customer, "message")
      if ($scope.customer.name == '' || $scope.customer.name == null) {
        $scope.customer.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } 
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Customer has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

        $scope.customerName = $scope.customer.name;
        $scope.submitDisable = true;
        Restangular.all('customers').post($scope.customer).then(function () {
          window.location = '/customers-list';
        });
        //window.location = '/customers-list';
      }
    };

    

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Updatecustomer = function () {
      document.getElementById('name').style.border = "";

      if ($scope.customer.name == '' || $scope.customer.name == null) {
        $scope.customer.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } 
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
        $scope.message = 'Customer has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      $scope.neWsubmitDisable = true;

      Restangular.one('customers/' + $routeParams.id).customPUT($scope.customer).then(function () {
        window.location = '/customers-list';
       
      });
    }
    };

    if ($routeParams.id) {
      Restangular.one('customers', $routeParams.id).get().then(function (custom) {
        $scope.original = custom;
        $scope.customer = Restangular.copy($scope.original);
      });
    }

  });
