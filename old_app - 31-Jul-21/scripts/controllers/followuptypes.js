'use strict';

angular.module('secondarySalesApp')
	.controller('FollowUpTypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/followuptypes/create' || $location.path() === '/followuptypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/followuptypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/followuptypes/create' || $location.path() === '/followuptypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/followuptypes/create' || $location.path() === '/followuptypes/' + $routeParams.id;
			return visible;
		};


		/*********/

		//  $scope.followuptypes = Restangular.all('followuptypes').getList().$object;

		if ($routeParams.id) {
			Restangular.one('followuptypes', $routeParams.id).get().then(function (followuptype) {
				$scope.original = followuptype;
				$scope.followuptype = Restangular.copy($scope.original);
			});
		}
		$scope.searchFollowUpType = $scope.name;
		$scope.pillars = Restangular.all('pillars').getList().$object;

		/************************************************************************** INDEX *******************************************/
		$scope.zn = Restangular.all('followuptypes').getList().then(function (zn) {
			$scope.followuptypes = zn;
			angular.forEach($scope.followuptypes, function (member, index) {
				member.index = index + 1;
			});
		});

		/*************************************************************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.SaveFollowUpType = function () {

			$scope.followuptypes.post($scope.followuptype).then(function () {
				console.log('FollowUpType Saved');
				window.location = '/followuptypes';
			});
		};
		/*************************************************************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.UpdateFollowUpType = function () {
			document.getElementById('name').style.border = "";
			if ($scope.followuptype.name == '' || $scope.followuptype.name == null) {
				$scope.followuptype.name = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your followuptype name';
				document.getElementById('name').style.border = "1px solid #ff0000";

			}
			if ($scope.validatestring != '') {
				alert($scope.validatestring);
				$scope.validatestring = '';
			} else {
				$scope.followuptypes.customPUT($scope.followuptype).then(function () {
					console.log('FollowUpType Saved');
					window.location = '/followuptypes';
				});
			}


		};
		/*************************************************************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				Restangular.one('followuptypes/' + id).remove($scope.followuptype).then(function () {
					$route.reload();
				});

			} else {

			}

		}


		$scope.getPillar = function (pillarId) {
			return Restangular.one('pillars', pillarId).get().$object;
		};

	});
