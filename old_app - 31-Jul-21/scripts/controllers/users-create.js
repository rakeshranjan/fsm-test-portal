'use strict';

angular.module('secondarySalesApp')
  .controller('UsersCreateCtrl', function ($scope, Restangular, $http, $window, $route, $filter, AnalyticsRestangular) {
    //        if ($window.sessionStorage.roleId != 1) {
    //            window.location = "/";
    //        }
    $scope.heading = 'User Create';
    $scope.Saved = false;
    $scope.Updated = true;
    $scope.roleDisable = false;
    $scope.userDisable = false;
    $scope.usernameDisable = false;
    $scope.user = {
      flag: true,
      email: null,
      zoneId: null,
      salesAreaId: null,
      coorgId: null,
      lastmodifiedtime: new Date(),
      lastmodifiedby: $window.sessionStorage.UserEmployeeId,
      deleteflag: false
    };
    $scope.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
    $scope.organisationlevels = [];

    //console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
    //console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
    //console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
    //console.log('$window.sessionStorage.LastModifyBy', $scope.lastmodifiedby);
    //console.log('$window.sessionStorage.Site', $window.sessionStorage.UserEmployeeId);
    /*
		"state": null,
		"district": null,
		"site": null,
		"facility": null,
		"lastmodifiedby": null,
		"lastmodifiedtime": null
*/
    //        Restangular.all('languages?filter[where][deleteflag]=null').getList().then(function (lRas) {
    //            $scope.languages = lRas;
    //            $scope.user.language = 1;
    //        });


    /*toggle*/
    $scope.init = function () {
      $scope.user.isPartner = true;
    }

    $scope.changeStatus = function () {
      $scope.user.isPartner = !$scope.user.isPartner;
      if ($scope.user.isPartner == true) {
        $scope.statusnew = "yes";
        console.log("in if ");
        console.log("$scope.statusnew ", $scope.statusnew);
      } else {
        $scope.statusnew = "no";
        console.log("in else ");
        console.log("$scope.statusnew ", $scope.statusnew);
      }
    }
    $scope.languages = Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().$object;
    $scope.groups = Restangular.all('groups?filter[where][deleteflag]=null').getList().$object;
    $scope.departments = Restangular.all('departments?filter[where][deleteflag]=null').getList().$object;
    $scope.ims = Restangular.all('ims?filter[where][deleteflag]=null').getList().$object;
    $scope.admins = Restangular.all('employees?filter[where][groupId]=1&filter[where][deleteflag]=false').getList().$object;
    //$scope.spms = Restangular.all('employees?filter[where][groupId]=3&filter[where][deleteflag]=false').getList().$object;
    //$scope.rios = Restangular.all('employees?filter[where][groupId]=4&filter[where][deleteflag]=null').getList().$object;
    //$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][groupId]=4').getList().$object;

    $scope.fieldworkers = Restangular.all('fieldworkers?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;


    $scope.comembers = Restangular.all('comembers?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;
    $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

    $scope.mentors = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=17').getList().$object;

    $scope.spms = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=3').getList().$object;

    $scope.rios = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=4').getList().$object;

    $scope.nos = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=16').getList().$object;

    $scope.employees = Restangular.all('employees?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

    $scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;

    //$scope.salesAreas = Restangular.all('sales-areas?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=null').getList().$object;

    //$scope.zones = Restangular.all('zones').getList().$object;

    //$scope.employees = Restangular.all('employees').getList().$object;
    //$scope.comembers = Restangular.all('comembers').getList().$object;
    //$scope.languagemasters = Restangular.all('languagemasters').getList().$object;

    /************************************************** SAVE *******************************/
    $scope.showUniqueValidation = false;
    $scope.createduser = {
      "usercreated": true
    }
    $scope.validatestring = '';
    $scope.Save = function () {

      //document.getElementById('password').style.border = "";
      //document.getElementById('email').style.border = "";
      if ($scope.user.roleId == '' || $scope.user.roleId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Your Role';
      } else if ($scope.user.language == '' || $scope.user.language == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Language';
      } else if ($scope.user.username == '' || $scope.user.username == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter User Name';
      } else if ($scope.user.countryCode == '' || $scope.user.countryCode == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Country Code';
      } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Phone No';
      } else if ($scope.user.mobile.length != 10) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Valid Phone No';
      } else if ($scope.user.email == '' || $scope.user.email == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
        // document.getElementById('email').style.borderColor = "#FF0000";
      } else if ($scope.user.latitude == '' || $scope.user.latitude == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Latitude';
      } else if ($scope.user.longitude == '' || $scope.user.longitude == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Longitude';
      } else if ($scope.user.password == '' || $scope.user.password == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
        // document.getElementById('password').style.borderColor = "#FF0000";
      } else if ($scope.user.confirm == '' || $scope.user.confirm == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Your Confirm';
        // document.getElementById('password').style.borderColor = "#FF0000";
      } else if ($scope.user.password != $scope.user.confirm) {
        $scope.validatestring = $scope.validatestring + 'Password is not matching';
      }


      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring2 = $scope.validatestring;
        $scope.validatestring = '';
      } else {

        $scope.orgstructure = "";
        for (var i = 0; i < $scope.organisationlevels.length; i++) {
          if ($scope.orgstructure === "") {
            $scope.orgstructure = $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          } else {
            $scope.orgstructure = $scope.orgstructure + ";" + $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          }
        }
        $scope.user.orgStructure = $scope.orgstructure;
        if($window.sessionStorage.isPartner === 'true'){
          $scope.user.managerId = $window.sessionStorage.userId;
        }else{
          $scope.user.managerId = 0;
        }
        console.log("$scope.user", $scope.user.orgStructure);
        console.log("$scope.user", $scope.user);
        //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.users.post($scope.user).then(function (PostResponce) {
            window.location = '/users';
          },
          function (response) {
            //alert(error.data.error.message);
            $scope.showValidation = !$scope.showValidation;
            //       if (response.data.error.constraint == "uniqueness") {
            // 	$scope.validatestring1 = 'Email' + ' ' + $scope.user.email + ' ' + 'Already Exists';
            // } else {
            // 	$scope.validatestring1 = response.data.error.detail;
            // }
            $scope.validatestring1 = 'Email ID Already Exists';
            //console.error('console.error', response);
          });
      };
    };

    $scope.Cancel = function () {
      window.location = '/users';
    };

    $scope.modalTitle = 'Thank You';
    $scope.message = 'User has been Created!';
    $scope.showValidation = false;
    /*$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};*/

    $scope.toggleValidation = function () {
      $scope.showUniqueValidation = !$scope.showUniqueValidation;
    };

    /******************************************** WATCH *************************/

    $scope.$watch('user.usrName', function (newValue, oldValue) {
      //console.log('usrName', newValue);


      if (newValue === oldValue || newValue == '' || newValue == undefined) {
        return;
      } else {
        $scope.newUser = JSON.parse(newValue);
        if ($scope.user.roleId == 1) {
          $scope.user.username = $scope.newUser.salesCode.toLowerCase();
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;
          $scope.user.email = $scope.newUser.email;
        }

        if ($scope.user.roleId == 5 || $scope.user.roleId == 15) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.helpline;
          //$scope.statename = $scope.newUser.state;
          $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.state).get().then(function (responseZone) {
            $scope.statename = responseZone[0].name;
            $scope.user.zoneId = responseZone[0].id;
            //			console.log('responseZone.name', $scope.user.zoneId);
          });

          $scope.salesAreas = Restangular.one('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.district).get().then(function (responsesalesAreas) {
            $scope.districtname = responsesalesAreas[0].name;
            $scope.user.salesAreaId = responsesalesAreas[0].id;
            //			console.log('responseZone.salesAreaId', $scope.user.salesAreaId);
          });

          $scope.Facility = Restangular.one('employees?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.facility).get().then(function (responseFacility) {
            $scope.Facilityname = responseFacility[0].salesCode;
            $scope.user.coorgId = responseFacility[0].id;
            //			console.log('responseZone.salesAreaId', $scope.user.coorgId);
          });

        }

        if ($scope.user.roleId == 15) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.helpline;
        }


        if ($scope.user.roleId == 3) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;
          //$scope.user.zoneId = $scope.newUser.state;
          console.log(' $scope.newUser.state', $scope.newUser.state);
          $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.state).get().then(function (responseZone) {
            $scope.statename = responseZone[0].name;
            $scope.user.zoneId = responseZone[0].id;
          });
        }


        if ($scope.user.roleId == 4) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;
          //$scope.user.zoneId = $scope.newUser.state;
          console.log(' $scope.newUser.state', $scope.newUser.state);
          $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.state).get().then(function (responseZone) {
            $scope.statename = responseZone[0].name;
            $scope.user.zoneId = responseZone[0].id;
          });
        }


        if ($scope.user.roleId == 16) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;

        }


        if ($scope.user.roleId == 17) {
          $scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;
          //$scope.user.zoneId = $scope.newUser.state;
          console.log(' $scope.newUser.state', $scope.newUser.state);
          $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.state).get().then(function (responseZone) {
            $scope.statename = responseZone[0].name;
            $scope.user.zoneId = responseZone[0].id;
          });
        }

        if ($scope.user.roleId == 6) {
          $scope.user.username = $scope.newUser.fwcode.toLowerCase();
          $scope.user.employeeid = $scope.newUser.id;
          $scope.user.mobile = $scope.newUser.mobile;
          $scope.user.email = $scope.newUser.email;

          $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.state).get().then(function (responseZone) {
            $scope.statename = responseZone[0].name;
            $scope.user.zoneId = responseZone[0].id;
            console.log('responseZone.name', $scope.user.zoneId);
          });

          $scope.salesAreas = Restangular.one('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.district).get().then(function (responsesalesAreas) {
            $scope.districtname = responsesalesAreas[0].name;
            $scope.user.salesAreaId = responsesalesAreas[0].id;
            console.log('responseZone.salesAreaId', $scope.user.salesAreaId);
          });

          $scope.Facility = Restangular.one('employees?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.facility).get().then(function (responseFacility) {
            $scope.Facilityname = responseFacility[0].salesCode;
            $scope.user.coorgId = responseFacility[0].id;
            console.log('responseZone.salesAreaId', $scope.user.coorgId);
          });
        }
      }
    });


    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });
    $scope.$watch('user.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('user.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    $scope.hideState = false;
    $scope.hideDistrict = false;
    $scope.hideCo = false;
    $scope.isPartner = $window.sessionStorage.isPartner;
    $scope.$watch('user.roleId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '' || newValue == undefined) {
        return;
      } else {
        
        if($window.sessionStorage.isPartner === 'true'){
        Restangular.one('roles/findOne?filter[where][id]=' + newValue).get().then(function (role) {
          $scope.user.level = role.level;
          Restangular.all('organisationlocations?filter[where][level]=' + role.level).getList().then(function (locations) {
            $scope.locations = locations;
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + role.level).get().then(function (orglvl) {
              Restangular.all('organisationlevels?filter[where][slno][lte]=' + orglvl.slno + '&filter[where][deleteflag]=false&filter[where][languageparent]=true' + '&filter[order]=slno ASC').getList().then(function (organisationlevels) {
                $scope.organisationlevels = organisationlevels;
                $scope.orgstructure = $window.sessionStorage.orgStructure.split(";");

                for (var i = 0; i < $scope.organisationlevels.length; i++) {
                  for (var j = 0; j < $scope.orgstructure.length; j++) {
                    $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
                    if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
                      $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1].split(",");
                    }
                  }
                }

                // console.log("$scope.organisationlevels", $scope.organisationlevels);

                Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                  $scope.organisationlocations = organisationlocations;

                  angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                    organisationlevel.index = index;
                    organisationlevel.organisationlocations = organisationlocations;
                  });
                });
              });
            });
          });
        });
      }
        else{
          Restangular.one('roles/findOne?filter[where][id]=' + newValue).get().then(function (role) {
            console.log(newValue, "newValue-314")
            $scope.user.level = role.level;
            console.log($scope.user.level, "USER LEVEL")
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + role.level).get().then(function (orglvl) {
  
              Restangular.all('organisationlevels?filter[where][slno][lte]=' + orglvl.slno + '&filter[where][languageparent]=true&filter[where][deleteflag]=false' + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
                $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + ogrlevls[0].id).getList().then(function (ogrlocs) {
                  $scope.organisationlevels = ogrlevls;
                  console.log($scope.organisationlevels, "$scope.organisationlevels-321-User related")
                  angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                    if (organisationlevel.id === ogrlevls[0].id) {
                      organisationlevel.organisationlocations = ogrlocs;
                    }
                  });
                });
              });
            });
  
  
          });
        }
      }
    });

    $scope.orgLevelChange = function (index, level) {
      //console.log(index, level, level.length, "Msg-338")
      $scope.organisationlocations = Restangular.all('organisationlocations?filter={"where":{"and":[{"parent":{"inq":[' + level + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (ogrlocs) {
        console.log(index, $scope.organisationlevels.length, level.length, "index-scope.organisationlevels.length-level.length")
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocs;
        }
      });
      //}
    };

    $scope.cofw = {
      data: 'co'
    };
    /*
		$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
		$scope.$watch('cofw.data', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				console.log(newValue);
				if (newValue == 'co') {
					$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
				} else if (newValue == 'fw') {
					$scope.partners = Restangular.all('partners?filter[where][groupId]=9').getList().$object;
				}
			}
		});
*/
    $scope.$watch('user.zoneId', function (newValue, oldValue) {
      if (newValue === oldValue | newValue == '') {
        return;
      } else {
        //$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
        $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
      }
    });

    $scope.$watch('user.salesAreaId', function (newValue, oldValue) {
      if (newValue === oldValue | newValue == '') {
        return;
      } else {
        //$scope.copartners = Restangular.all('employees?filter[where][groupId]=5' + '&filter[where][deleteflag]=false').getList().$object;
        //filter[where][stateId]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&
        $scope.copartners = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;
      }
    });


    $scope.users = Restangular.all('users').getList().$object;
    $scope.analyticsusers = AnalyticsRestangular.all('users').getList().$object;
    //$scope.employees = Restangular.all('employees').getList().$object;
    $scope.groups = Restangular.all('groups').getList().$object;
           if ($window.sessionStorage.roleId === '1') {
            $scope.roles = Restangular.all('roles?filter={"where": {"type": {"inq": ["null", "admin", "others", "partner"]}}}').getList().$object;
           }else if ($window.sessionStorage.isPartner === 'true') {
            $scope.roles = Restangular.all('roles?filter={"where": {"type": {"inq": ["partner"]}}}').getList().$object;
           }else{
            $scope.roles = Restangular.all('roles').getList().$object;
           }



    //$scope.salesAreas = Restangular.all('sales-areas').getList().$object;
    $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
    $scope.distributionSubareas = Restangular.all('distribution-subareas').getList().$object;



    $scope.$watch('user.distributionAreaId', function (newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      } else {
        $scope.user.organizationId = newValue;
      }
    });

    $scope.$watch('user.salesAreaId', function (newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      } else {
        $scope.user.organizationId = newValue;
      }
    });

  });
