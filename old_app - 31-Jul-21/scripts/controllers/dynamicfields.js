'use strict';

angular.module('secondarySalesApp')
    .controller('DynamicFieldsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        $scope.isCreateSave = true;
        $scope.heading = 'Dynamic Fields';

        $scope.dynamicfield = {
            languageId: 1
        };

        $scope.submitdynamicfields = Restangular.all('dynamicfields').getList().$object;

        $scope.SaveDynamicField = function () {
            $scope.submitdynamicfields.customPUT($scope.dynamicfield).then(function (response) {
                    console.log('Response', response);
                    window.location = '/';
            });
        };

        Restangular.one('dynamicfields', 1).get().then(function (zone) {
            $scope.original = zone;
            $scope.dynamicfield = Restangular.copy($scope.original);
                $scope.modalInstanceLoad.close();
        });

        $scope.LanguageName = 'Dynamic Fields';
        $scope.Language = 'Dynamic Fields';
    });