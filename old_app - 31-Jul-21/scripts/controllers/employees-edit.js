'use strict';

angular.module('secondarySalesApp')
	.controller('EmployeesEditCtrl', function ($scope, Restangular, $routeParams, $window, $filter, $timeout, $location) {

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
		$scope.modalTitle = 'Thank You';
		$scope.message = 'Facility has been updated'
		$scope.heading = 'Facility Update';
		$scope.Created = true;
		$scope.Updated = false;
		$scope.fwcodedisable = true;
		$scope.fwnamedisable = true;
		$scope.fwstatedisable = true;
		$scope.fwdistrictdisable = true;
		$scope.tiRatingDisable = true;
		$scope.tiNonRatingDisable = true;


		$scope.employee = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		};

		$scope.states = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
		$scope.typologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;
		$scope.areaofoperations = Restangular.all('areaofoperations?filter[where][deleteflag]=false').getList().$object;
		$scope.employeestatuses = Restangular.all('employeestatuses?filter[where][deleteflag]=false').getList().$object;
		$scope.facilitytypes = Restangular.all('facilitytypes?filter[where][deleteflag]=false').getList().$object;
		$scope.tiratings = Restangular.all('tiratings').getList().$object;

		/*$scope.Rating = function (no) {
			$scope.tifacilities = [];
			for (var i = 0; i < no; i++) {
				$scope.tifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});
			}
		}

		$scope.ProjectImp = function (noo) {
			$scope.nontifacilities = [];
			for (var i = 0; i < noo; i++) {
				$scope.nontifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});
			}
		}*/

		$scope.tifacilitiesAdd = false;
		$scope.Rating = function (no) {
			$scope.tifacilitiesAdd = true;
			$scope.tifacilities = [];
			for (var i = 0; i < no; i++) {
				$scope.tifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});
			}
			$scope.CurrentRating = no;
		}

		$scope.nontifacilitiesAdd = false;
		$scope.currentChangenonti = 0;
		$scope.ProjectImp = function (noo) {
			$scope.nontifacilitiesAdd = true;
			$scope.nontifacilities = [];
			for (var i = 0; i < noo; i++) {
				$scope.nontifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});
			}
		}


		$scope.tiratingAdd = false;
		$scope.nontiratingAdd = false;
		$scope.TIRATING = function (no) {
			$scope.tiratingAdd = true;
		}
		$scope.NONTIRATING = function (no) {
			$scope.nontiratingAdd = true;
		}


		if ($routeParams.id) {
			Restangular.one('employees', $routeParams.id).get().then(function (employee) {
				$scope.original = employee;
				$scope.salearea = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.stateId + '&filter[where][deleteflag]=false').getList().then(function (salearea) {
					$scope.districts = salearea;
					$scope.employee = Restangular.copy($scope.original);
					$scope.oldValueTIRating = parseInt(employee.nooftis);
					$scope.oldValueNonTIRating = parseInt(employee.noofnonti);
					$scope.empnonto = $scope.employee.noofnonti;

					if (employee.typology != null) {
						$scope.employee.typolog = employee.typology.split(",");
					} else {
						$scope.employee.typolog = [];
					}

					Restangular.one('tifacilities?filter[where][facilityid]=' + $routeParams.id + '&filter[where][deleteflag]=false').get().then(function (tt) {
						$scope.tifacilities = tt;
						$scope.currentTI = tt;
					});
					Restangular.one('nontifacilities?filter[where][facilityid]=' + $routeParams.id + '&filter[where][deleteflag]=false').get().then(function (ttt) {
						$scope.nontifacilities = ttt;
						$scope.currentNONTI = ttt;
					});
				});
			});
		}


		$scope.$watch('employee.stateId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
				return;
			} else {
				$scope.districts = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
			}
		});


		
		$scope.employee = {};
		/*********************** UPDATE **********************************************************/
		$scope.showValidation = false;
		$scope.validatestring = '';
		$scope.Update = function () {
			var re = /\S+@\S+\.\S+/;
			document.getElementById('salesCode').style.border = "";

			$scope.totalnooftis = $scope.oldValueTIRating + parseInt($scope.employee.nooftis)
			$scope.totalnoofnontis = $scope.oldValueNonTIRating + parseInt($scope.employee.noofnonti)
				//console.log('$scope.totalnooftis',$scope.totalnoofnontis);


			$scope.employee.nooftis = $scope.totalnooftis;
			$scope.employee.noofnonti = $scope.totalnoofnontis;
			 if ($scope.employee.salesCode == '' || $scope.employee.salesCode == null) {
				$scope.employee.salesCode = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your sales code';
				//document.getElementById('salesCode').style.border = "1px solid #ff0000";
				 $scope.TabName =  'BASIC';
				 
			} else if ($scope.employee.facilitytype == '' || $scope.employee.facilitytype == null) {
				$scope.validatestring = $scope.validatestring + 'Please select Facility Type';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.areaofoperation == '' || $scope.employee.areaofoperation == null) {
				$scope.validatestring = $scope.validatestring + 'Please select Area of Operation';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.nooftowncover == '' || $scope.employee.nooftowncover == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter No of Towns Covered';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofmandal == '' || $scope.employee.noofmandal == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter No of Mandals/Talukas/Blocks Covered';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofincome == '' || $scope.employee.noofincome == null) {
				$scope.validatestring = $scope.validatestring + 'No of Income Generating Activities Undertaken by the CO';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.nooftis == '' || $scope.employee.nooftis == null || $scope.employee.nooftis == NaN || $scope.employee.nooftis == 0) {
				$scope.validatestring = $scope.validatestring + 'No of TIs Implemented by the facility';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofnonti == '' || $scope.employee.noofnonti == null || $scope.employee.noofnonti == 0) {
				$scope.validatestring = $scope.validatestring + 'No of Non TI and Non Avahan Projects Implemented by the CO';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.registrationno == '' || $scope.employee.registrationno == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.registrationdate == '' || $scope.employee.registrationdate == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Renewal Date';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.pancard == '' || $scope.employee.pancard == null) {
				$scope.validatestring = $scope.validatestring + 'Enter PAN Card Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.astatus == '' || $scope.employee.astatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select 12 A Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.aregistrationno == '' || $scope.employee.aregistrationno == null && $scope.employee.astatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.gstatus == '' || $scope.employee.gstatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select 80 G Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.gregistrationno == '' || $scope.employee.gregistrationno == null && $scope.employee.gstatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.frcastatus == '' || $scope.employee.frcastatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select FCRA Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.frcaregistrationno == '' || $scope.employee.frcaregistrationno == null && $scope.employee.frcastatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.dateoflastabgm == '' || $scope.employee.dateoflastabgm == null) {
				$scope.validatestring = $scope.validatestring + 'Select Date of Last AGBM';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.registrationrenewaldate == '' || $scope.employee.registrationrenewaldate == null) {
				$scope.validatestring = $scope.validatestring + 'Select Registration Renewal Date';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.externalaudit == '' || $scope.employee.externalaudit == null) {
				$scope.validatestring = $scope.validatestring + 'Select External Audit Completed';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.auditreport == '' || $scope.employee.auditreport == null) {
				$scope.validatestring = $scope.validatestring + 'Select Audit Report Submitted';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.actionplan == '' || $scope.employee.actionplan == null) {
				$scope.validatestring = $scope.validatestring + 'Select Board has discussed Audit Report and prepared Action plan';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.electiondate == '' || $scope.employee.electiondate == null) {
				$scope.validatestring = $scope.validatestring + 'Select Date of Last Elections';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.electionbody == '' || $scope.employee.electionbody == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Total No.of Members in the Elected Body( No of board members)';
				$scope.TabName =  'STATUTORY';
			}
			if ($scope.employee.typolog != '' || $scope.employee.typolog != null || $scope.employee.typolog != undefined)
				for (var i = 0; i < $scope.employee.typolog.length; i++) {
					if (i == 0) {
						$scope.employee.typology = $scope.employee.typolog[i];
					} else {
						$scope.employee.typology = $scope.employee.typology + ',' +
							$scope.employee.typolog[i];
					}
				} else {
					$scope.employee.typology = null;
				}

			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				Restangular.one('employees', $routeParams.id).customPUT($scope.employee).then(function (response) {
					console.log('$scope.employee', $scope.employee);
					if ($scope.tifacilitiesAdd == true) {
						//console.log('Call Add Function');
						$scope.SaveTISIMP(response.stateId, response.district, response.id);
					} else {
						//console.log('Call Update Function');
						$scope.UpdateTISIMP(response.stateId, response.district);
					}

					if ($scope.nontifacilitiesAdd == true) {
						//console.log('Call NON Add Function');
						$scope.SaveNONTISIMP(response.stateId, response.district, response.id);
					} else {
						//console.log('Call NON Update Function');
						$scope.UpdateNONTISIMP(response.stateId, response.district);
					}


					//$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
					//window.location = '/employees';
				}, function (response) {
					//alert(response.data.error.detail);
					if (response.data.error.constraint == "unique_salescode") {
						$scope.validatestring2 = 'Facility Code' + ' ' + $scope.employee.salesCode + ' ' + 'Already Exists';
					} else {
						$scope.validatestring2 = response.data.error.detail;
					}
					console.error(response);
				});
			}
		};

		/*	
			$scope.UpdateTISIMP = function (st, dt, id) {
				for (var i = 0; i < $scope.tifacilities.length; i++) {
					$scope.item = {
						district: dt,
						rating: $scope.tifacilities[i].rating
					}
					Restangular.one('tifacilities/' + $scope.tifacilities[i].id).customPUT($scope.item).then(function (response) {
						console.log('update tifacilities', response);
					});
				};

			};

			$scope.count = 0;
			$scope.UpdateNONTISIMP = function (st, dt, id) {
				$scope.UpdateTISIMP();
				for (var i = 0; i < $scope.nontifacilities.length; i++) {
					$scope.item = {
						district: dt,
						rating: $scope.nontifacilities[i].rating
					}
					Restangular.one('nontifacilities/' + $scope.nontifacilities[i].id).customPUT($scope.item).then(function (response) {
						console.log('update nontifacilities', response);
					});
					$scope.count++;
					if ($scope.count == $scope.nontifacilities.length) {
						$location.path('/employees');
					}
				};

			};
			$scope.SaveTISIMP = function (st, dt, id) {
				for (var i = 0; i < $scope.tifacilities.length; i++) {
					$scope.item = {
						facilityid: id,
						state: st,
						district: dt,
						rating: $scope.tifacilities[i].rating,
						deleteflag: false
					}
					Restangular.all('tifacilities').post($scope.item).then(function (resp) {
						console.log('tifacilities', resp);
					});
				};

			};
		*/

		/*********************** UPDATE **********************************************************/
		$scope.showValidation = false;
		//  $scope.validatestring = '';

		$scope.UpdateTISIMP = function (st, dt, id) {
			for (var i = 0; i < $scope.tifacilities.length; i++) {
				$scope.item = {
					district: dt,
					rating: $scope.tifacilities[i].rating
				}
				Restangular.one('tifacilities/' + $scope.tifacilities[i].id).customPUT($scope.item).then(function (response) {
					//console.log('update tifacilities', response);
					$location.path('/employees');
				});

			};

		};

		$scope.count = 0;
		$scope.UpdateNONTISIMP = function (st, dt, id) {
			//$scope.UpdateTISIMP();
			for (var i = 0; i < $scope.nontifacilities.length; i++) {
				$scope.item = {
					district: dt,
					rating: $scope.nontifacilities[i].rating
				}
				Restangular.one('nontifacilities/' + $scope.nontifacilities[i].id).customPUT($scope.item).then(function (response) {
					//console.log('update nontifacilities', response);
				});
				$scope.count++;
				if ($scope.count == $scope.tifacilities.length) {
					$location.path('/employees');
				}
			};

		};
		$scope.SaveTISIMP = function (st, dt, id) {
			for (var i = 0; i < $scope.tifacilities.length; i++) {
				$scope.item = {
					facilityid: id,
					state: st,
					district: dt,
					rating: $scope.tifacilities[i].rating,
					deleteflag: false
				}
				Restangular.all('tifacilities').post($scope.item).then(function (resp) {
					//console.log('tifacilities', resp);
					$location.path('/employees');
				});
			};

		};


		$scope.count = 0;
		$scope.SaveNONTISIMP = function (st, dt, id) {

			for (var i = 0; i < $scope.nontifacilities.length; i++) {
				$scope.itemnonti = {
					facilityid: id,
					state: st,
					district: dt,
					rating: $scope.nontifacilities[i].rating,
					deleteflag: false
				}
				Restangular.all('nontifacilities').post($scope.itemnonti).then(function (resp) {
					//console.log('nontifacilities', resp);
					$location.path('/employees');
				});
				$scope.count++;
				//console.log('$scope.coun++', $scope.coun);
				if ($scope.count == $scope.nontifacilities.length) {
					$location.path('/employees');
				}
			};
		};

		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};


		//Datepicker settings start
		$scope.today = function () {
			$scope.dt = $filter('date')(new Date(), 'y-MM-dd');
		};
		$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		$scope.dtmax = new Date();
		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();
		$scope.picker = {};
		$scope.RegistrationRenewal = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.RegistrationRenewal.opened = true;
		};

		$scope.RegistrationRenewal2 = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.RegistrationRenewal2.opened = true;
		};

		$scope.LastAGBM = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.LastAGBM.opened = true;
		};

		$scope.LastElection = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.LastElection.opened = true;
		};


		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end
		$scope.aRegistration = false;
		$scope.gRegistration = false;
		$scope.fRegistration = false;
		$scope.$watch('employee.astatus', function (newValue, oldValue) {
			//console.log('astatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.aRegistration = true;
			} else {
				$scope.aRegistration = false;
			}
		});

		$scope.$watch('employee.gstatus', function (newValue, oldValue) {
			//console.log('gstatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.gRegistration = true;
			} else {
				$scope.gRegistration = false;
			}
		});

		$scope.$watch('employee.frcastatus', function (newValue, oldValue) {
			//console.log('fcrastatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.fRegistration = true;
			} else {
				$scope.fRegistration = false;
			}
		});
	});
