'use strict';

angular.module('secondarySalesApp')
  .controller('NavbarCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $idle, $modal, $route, $rootScope) {


    if ($window.sessionStorage.userId) {
      $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
      console.log("Admin->", $scope.roles);
    }
    console.log("$window.sessionStorage.orgLevel", $window.sessionStorage.orgLevel);
    console.log("Here-In navbar js-Inside")

    // if ($window.sessionStorage.roleId === '1' || $window.sessionStorage.roleId === '2' ) {
    //   $scope.hideFieldMenu = true;
    // }
    // $scope.hideFieldMenu = false;
    if ($window.sessionStorage.roleId === '2' || $window.sessionStorage.roleId === '3' || $window.sessionStorage.roleId === '4' || $window.sessionStorage.roleId === '5' || $window.sessionStorage.roleId === '6' || $window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '8') {
      $scope.showAssign = true;
    } else {
      $scope.showAssign = false;
    }
    // if ($window.sessionStorage.roleId === '5') {
    //   $scope.showNewAssign = true;
    // }
    // } else {
    //   $scope.showAssign = false;
    // }
    /****   for sidebar menu icon change *****/
    $scope.imageHome = "images/piclinks/home-blue.png";
    $scope.imageGeneral = "images/piclinks/facility-grey.png";
    $scope.imageFW = "images/piclinks/adminicon_grey.png";
    $scope.imageFacility = "images/piclinks/anlytics-grey.png";
    $scope.imageField = "images/piclinks/anlytics-grey.png";
    $scope.imageDashboard = "images/piclinks/language-grey.png";
    $scope.imageScm = "images/piclinks/scm.png";
    $scope.imageScmTwo = "images/piclinks/scmtwo.png";
    $scope.imageDashboardNew = "images/piclinks/dashboard.png";
    $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
    $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
    $scope.imageNOBudget = "images/piclinks/budgetdark.png";

    $scope.colorHome = "4px solid #004891";
    $scope.colorGeneral = "4px solid #ffffff";
    $scope.colorFW = "4px solid #ffffff";
    $scope.colorFacility = "4px solid #ffffff";
    $scope.colorField = "4px solid #ffffff";
    $scope.colorDashboard = "4px solid #ffffff";
    $scope.colorScm = "4px solid #ffffff";
    $scope.colorScmTwo = "4px solid #ffffff";
    $scope.colorDashboardNew = "4px solid #ffffff";
    $scope.colorDashboardLatest = "4px solid #ffffff";
    $scope.colorSPMBudget = "4px solid #ffffff";
    $scope.colorNOBudget = "4px solid #ffffff";

    $scope.clickHome = function () {
      $scope.imageHome = "images/piclinks/home-blue.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png"; 
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #004891";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";
      $location.path("/");

    };

    $scope.clickSPMBudget = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-blue.png";
      $scope.imageFW = "images/piclinks/admindark.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetwhite.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #004891";
      $scope.colorNOBudget = "4px solid #ffffff";
      $location.path("/");

    };

    $scope.clickNOBudget = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-blue.png";
      $scope.imageFW = "images/piclinks/admindark.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetwhite.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #004891";
      $location.path("/");

    };

    $rootScope.clickDashboard = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-blue.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #004891";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";

      if ($scope.roles.dashboardfacility) {
        $location.path("/");
      } else if ($scope.roles.nobudget) {
        $location.path("/");
      } else if ($scope.roles.rodashboard) {
        $location.path("/");
      } else if ($scope.roles.sodashboard) {
        $location.path("/");
      } else if ($scope.roles.fsmentordashboard) {
        $location.path("/");
      }

    };


    $rootScope.clickDashboardLatest = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-blue.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard-blue.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #004891";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";

      if ($scope.roles.dashboardfacility) {
        $location.path("/");
      } else if ($scope.roles.nobudget) {
        $location.path("/");
      } else if ($scope.roles.rodashboard) {
        $location.path("/");
      } else if ($scope.roles.sodashboard) {
        $location.path("/");
      } else if ($scope.roles.fsmentordashboard) {
        $location.path("/");
      }

    };

    $rootScope.clickDashboardNew = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard-blue.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #004891";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";

      if ($scope.roles.dashboardfacility) {
        $location.path("/");
      } else if ($scope.roles.nobudget) {
        $location.path("/");
      } else if ($scope.roles.rodashboard) {
        $location.path("/");
      } else if ($scope.roles.sodashboard) {
        $location.path("/");
      } else if ($scope.roles.fsmentordashboard) {
        $location.path("/");
      }

    };


    $scope.clickGeneral = function () {
      //            $scope.imageHome = "images/piclinks/homedark.png";
      //            $scope.imageGeneral = "images/piclinks/facilitywhite.png";
      //            $scope.imageFW = "images/piclinks/admindark.png";
      //            $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-blue.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #004891";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";
    };

    $scope.clickFW = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_blue.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #004891";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";
    };

    $scope.clickFacility = function () {
      //            $scope.imageHome = "images/piclinks/homedark.png";
      //            $scope.imageGeneral = "images/piclinks/facility-blue.png";
      //            $scope.imageFW = "images/piclinks/admindark.png";
      //            $scope.imageFacility = "images/piclinks/anlytics-blue.png";
      $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-blue.png";
      $scope.imageField = "images/piclinks/anlytics-grey.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #004891";
      $scope.colorField = "4px solid #ffffff";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";
    };
    

    $scope.clickField = function () {
    $scope.imageHome = "images/piclinks/home-gray.png";
      $scope.imageGeneral = "images/piclinks/facility-grey.png";
      $scope.imageFW = "images/piclinks/adminicon_grey.png";
      $scope.imageFacility = "images/piclinks/anlytics-grey.png";
      $scope.imageField = "images/piclinks/anlytics-blue.png";
      $scope.imageDashboard = "images/piclinks/language-grey.png";
      $scope.imageScm = "images/piclinks/scm.png";
      $scope.imageScmTwo = "images/piclinks/scmtwo.png";
      $scope.imageDashboardNew = "images/piclinks/dashboard.png";
      $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
      $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
      $scope.imageNOBudget = "images/piclinks/budgetdark.png";

      $scope.colorHome = "4px solid #ffffff";
      $scope.colorGeneral = "4px solid #ffffff";
      $scope.colorFW = "4px solid #ffffff";
      $scope.colorFacility = "4px solid #ffffff";
      $scope.colorField = "4px solid #004891";
      $scope.colorDashboard = "4px solid #ffffff";
      $scope.colorScm = "4px solid #ffffff";
      $scope.colorScmTwo = "4px solid #ffffff";
      $scope.colorDashboardNew = "4px solid #ffffff";
      $scope.colorDashboardLatest = "4px solid #ffffff";
      $scope.colorSPMBudget = "4px solid #ffffff";
      $scope.colorNOBudget = "4px solid #ffffff";
    };
    $scope.clickScm = function () {
      $scope.imageHome = "images/piclinks/home-gray.png";
        $scope.imageGeneral = "images/piclinks/facility-grey.png";
        $scope.imageFW = "images/piclinks/adminicon_grey.png";
        $scope.imageFacility = "images/piclinks/anlytics-grey.png";
        $scope.imageField = "images/piclinks/anlytics-grey.png";
        $scope.imageDashboard = "images/piclinks/language-grey.png";
        $scope.imageScm = "images/piclinks/scm-purple.png";
        $scope.imageScmTwo = "images/piclinks/scmtwo.png";
        $scope.imageDashboardNew = "images/piclinks/dashboard.png";
        $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
        $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
        $scope.imageNOBudget = "images/piclinks/budgetdark.png";
  
        $scope.colorHome = "4px solid #ffffff";
        $scope.colorGeneral = "4px solid #ffffff";
        $scope.colorFW = "4px solid #ffffff";
        $scope.colorFacility = "4px solid #ffffff";
        $scope.colorField = "4px solid #ffffff";
        $scope.colorDashboard = "4px solid #ffffff";
        $scope.colorScm = "4px solid #004891";
        $scope.colorScmTwo = "4px solid #ffffff";
        $scope.colorDashboardNew = "4px solid #ffffff";
        $scope.colorDashboardLatest = "4px solid #ffffff";
        $scope.colorSPMBudget = "4px solid #ffffff";
        $scope.colorNOBudget = "4px solid #ffffff";
      };
      $scope.clickScmTwo = function () {
        $scope.imageHome = "images/piclinks/home-gray.png";
          $scope.imageGeneral = "images/piclinks/facility-grey.png";
          $scope.imageFW = "images/piclinks/adminicon_grey.png";
          $scope.imageFacility = "images/piclinks/anlytics-grey.png";
          $scope.imageField = "images/piclinks/anlytics-grey.png";
          $scope.imageDashboard = "images/piclinks/language-grey.png";
          $scope.imageScm = "images/piclinks/scm.png";
          $scope.imageScmTwo = "images/piclinks/scmactive.png";
          $scope.imageDashboardNew = "images/piclinks/dashboard.png";
          $scope.imageDashboardLatest = "images/piclinks/dashboard.png";
          $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
          $scope.imageNOBudget = "images/piclinks/budgetdark.png";
    
          $scope.colorHome = "4px solid #ffffff";
          $scope.colorGeneral = "4px solid #ffffff";
          $scope.colorFW = "4px solid #ffffff";
          $scope.colorFacility = "4px solid #ffffff";
          $scope.colorField = "4px solid #ffffff";
          $scope.colorDashboard = "4px solid #ffffff";
          $scope.colorScm = "4px solid #ffffff";
          $scope.colorScmTwo = "4px solid #004891";
          $scope.colorDashboardNew = "4px solid #ffffff";
          $scope.colorDashboardLatest = "4px solid #ffffff";
          $scope.colorSPMBudget = "4px solid #ffffff";
          $scope.colorNOBudget = "4px solid #ffffff";
        };

    /**** Above code  for sidebar menu icon change *****/

    Restangular.one('users?filter[where][username]=' + $window.sessionStorage.userName).get().then(function (user) {
      $scope.user = user[0];
      console.log("User for Country Level-", $scope.user, user);
    });

    Restangular.one('roles?filter[where][id]=' + $window.sessionStorage.roleId).get().then(function (role) {
      $scope.role = role[0];
    });

    $scope.rolesconfiguration = true;
    $scope.rolesconfiguration1 = true;
    if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 5 || $window.sessionStorage.roleId == 6 || $window.sessionStorage.roleId == 7 || $window.sessionStorage.roleId == 8 || window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 10 || $window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 13 || $window.sessionStorage.roleId == 14) {
      $scope.rolesconfiguration = false;
    } else {
      $scope.rolesconfiguration1 = false;
    }
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.KnowledgeBaseName = "Knowledge Base";
    if ($scope.UserLanguage == 1) {
      $scope.KnowledgeBaseName = "Knowledge Base";
    } else if ($scope.UserLanguage == 2) {
      $scope.KnowledgeBaseName = "ज्ञानधार";
    } else if ($scope.UserLanguage == 3) {
      $scope.KnowledgeBaseName = "ಜ್ಞಾನದ ತಳಹದಿ";
    } else if ($scope.UserLanguage == 4) {
      $scope.KnowledgeBaseName = "அறிவு சார்ந்த";
    } else if ($scope.UserLanguage == 5) {
      $scope.KnowledgeBaseName = "నాలెడ్జ్ బేస్";
    } else if ($scope.UserLanguage == 6) {
      $scope.KnowledgeBaseName = "पायाभूत माहिती";
    }

    /*
		       $scope.menu = [{
		         'title': 'Home',
		         'route': '/?(navbar)?',
		         'link': '/'
		       },
		       {
		         'title': 'hh',
		         'route': '/hh',
		         'link': '/hh'
		       }];
		       */
    /*************************************************************************************
			if ($window.sessionStorage.roleId == 5) {
				Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.partners1 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facilityId]=' + $scope.comemberid + '&filter={"where":{"lastmodifiedbyrole":{"inq":[5,6]}}}').getList().then(function (resPartner1) {
						$scope.partners = resPartner1;
					});
				});

			} else {
				$scope.partners2 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][lastmodifiedbyrole]=6').getList().then(function (resPartner2) {
					$scope.partners = resPartner2;
				});
			}
		/**************************************** Member *******************************************/

    if ($window.sessionStorage.roleId == 5) {
      $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
        $scope.partners = part1;
        angular.forEach($scope.partners, function (member, index) {
          member.index = index + 1;
        });
      });
    } else {
      Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
        $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
          $scope.partners = part2;
          angular.forEach($scope.partners, function (member, index) {
            member.index = index + 1;

          });
        });
      });
    }

    /***********************************************************************/

    $scope.searchbulkupdate = '';

    $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    console.log($scope.orgStructureSplit, "$scope.orgStructureSplit-428")
    $scope.CountrySplit = $scope.orgStructureSplit[0];
    $scope.ZoneSplit = $scope.orgStructureSplit[1];
    console.log($scope.CountrySplit, "$scope.CountrySplit-430")
    console.log($scope.ZoneSplit, "$scope.ZoneSplit-430")
    $scope.intermediateOrgStructure = "";
    for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
      if ($scope.intermediateOrgStructure === "") {
        $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
      } else {
        $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
      }
    }
    //console.log($scope.intermediateOrgStructure, "$scope.intermediateOrgStructure-436")
    //--- No need now (Let it be because LHS menu somewhere linked) - It was for single state & multi city ---------
    $scope.orgStructureFilterArray = [];
    if ($scope.orgStructureSplit.length > 0) {
      $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
      console.log($scope.LocationsSplit, "$scope.LocationsSplit-444")
      for (var i = 0; i < $scope.LocationsSplit.length; i++) {
        $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
      }

    }
    console.log($scope.orgStructureFilterArray, "$scope.orgStructureFilterArray-451")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));
    //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
    $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
    console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
    //--- End of No need now - It was for single state & multi city ---------

 //-- For Multi Zone, state & multi City array filter work Ravi ---------------------------------
 $scope.orgStructureZoneFilterArray = [];
 if ($scope.orgStructureSplit.length > 1) {
   $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
   console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
   for (var i = 0; i < $scope.ZoneSplit.length; i++) {
     $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.ZoneSplit[i]);
   }
 }

 //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
 $scope.orgStructureStateFilterArray = [];
 if ($scope.orgStructureSplit.length > 1) {
   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
 //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
  // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
  for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
   for (var i = 0; i < $scope.StatesSplit.length; i++) {
     // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
     $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.StatesSplit[i]);
   }
  }
 }

    //-- For Multi state & multi City array filter work Satyen ---------------------------------
    // $scope.orgStructureStateFilterArray = [];
    // // if ($scope.orgStructureSplit.length > 0) {
    // if ($scope.orgStructureSplit.length > 1) {
    //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //     // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //     $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //   }
    // }
    console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-Testing-468")
    $scope.orgStructureFinalFilterArray = [];
    if ($scope.orgStructureStateFilterArray.length > 0) {
      for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
        for (var i = 0; i < $scope.LocationsSplit.length; i++) {
          $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
        }
      }
    }
    //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    //console.log("$scope.orgStructureFinalFilterArray", JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", ""));

    //-- End of For Multi state & multi City array filter work Satyen ---------------------------------

    if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
      // console.log("Here-In navbar js-Inside-297")
      // $scope.memberUrl = 'members?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[0]}}]}}';
      // $scope.memberUrl = 'rcdetails?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[0]}}]}}';
      $scope.memberUrl = 'rcdetails?filter[where][orgtructure][like]=%' + $scope.orgStructureFilter + '%&filter[where][deleteflag]=false&filter[where][customerId]='+$window.sessionStorage.customerId;
    } else if ($window.sessionStorage.roleId === '5' || $window.sessionStorage.roleId === '6') {
      // console.log("Here-In navbar js-Inside-297")
      // $scope.memberUrl = 'members?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[0]}}]}}';
      $scope.memberUrl = 'rcdetails?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[0]}}]}}';
    } else {
      // $scope.memberUrl = 'members?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';
      $scope.memberUrl = 'rcdetails?filter={"where":{"and":[{"orgstructure":{"inq":[' + $scope.orgStructureFilter + ']}},{"deleteflag":{"inq":[false]}},{"assignedto":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';
    }
    console.log($scope.memberUrl, "$scope.memberUrl")

    Restangular.all($scope.memberUrl).getList().then(function (member) {
      $scope.members = member;
      console.log($scope.members, "Member-Here at 305")
      angular.forEach($scope.members, function (member, index) {
        member.index = index + 1;
      });
    });

    $scope.openOnetoOne = function () {
      console.log("asdt");

      $scope.modalInstance1 = $modal.open({
        animation: true,
        templateUrl: 'template/OnetooOne.html',
        scope: $scope,
        backdrop: 'static'

      });
      console.log("window.sessionStorage.roleId-523", window.sessionStorage.roleId);
      console.log("window.sessionStorage.orgStructure-524", window.sessionStorage.orgStructure);
      // Restangular.all('members?filter[where][deleteflag]=false').getList().then(function (membr) {
      if (window.sessionStorage.roleId == 11 || window.sessionStorage.roleId == 33) {

        // Restangular.all('members?filter[where][deleteflag]=false&filter[where][assignedto]=' + window.sessionStorage.userId).getList().then(function (membr) {
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + window.sessionStorage.userId).getList().then(function (membr) {
          $scope.members = membr;
          console.log($scope.members, "TICKET-DISPLAY-468")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
            // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
            //   $scope.customername = custmor;
            //   for (var j = 0; j < $scope.customername.length; j++) {
            //     if (member.customerId == $scope.customername[j].id) {
            //       member.customername = $scope.customername[j].name;
            //       break;
            //     }
            //   }
            // });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
              $scope.username = userresp;
              for (var j = 0; j < $scope.username.length; j++) {
                if (member.assignedto == $scope.username[j].id) {
                  member.username = $scope.username[j].username;
                  break;
                }
              }
            });
            Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
              $scope.statusname = todostatusresp;
              for (var j = 0; j < $scope.statusname.length; j++) {
                if (member.statusId == $scope.statusname[j].id) {
                  member.statusname = $scope.statusname[j].name;
                  break;
                }
              }
            });
            Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
              $scope.updatetypename = updatetyperesp;
              for (var j = 0; j < $scope.updatetypename.length; j++) {
                if (member.producttypeid == $scope.updatetypename[j].id) {
                  member.updatetypename = $scope.updatetypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
              $scope.serviceeventname = serviceeventresp;
              for (var j = 0; j < $scope.serviceeventname.length; j++) {
                if (member.serviceeventid == $scope.serviceeventname[j].id) {
                  member.serviceeventname = $scope.serviceeventname[j].name;
                  break;
                }
              }
            });
          });
        });

      } else if (window.sessionStorage.roleId == 5 || $window.sessionStorage.roleId == 6) {
        console.log($scope.orgStructureFilter, "ORGSTRUCT-FILTER-570-State Level")
        //-- Previous code of Single state & multi city was same as was in it is commented for role == 7 and 15 -----
        console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-574")
        //var str = $scope.orgStructureFilter; //previously
        var str = $scope.orgStructureUptoStateFilter;
        var strOrgstruct = str.split('"')
        //console.log(strOrgstruct[1], "strorgstruct-578", strOrgstruct)

        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-586-State Level")
        // from here ---
        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

            $scope.membersnew = membr;
            //console.log($scope.membersnew, "TICKET-DISPLAY-595")
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.members = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-602")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-603")
            // console.log($scope.members, "$scope.members-604")

            angular.forEach($scope.members, function (member, index) {
              member.index = index + 1;
              // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              //   $scope.customername = custmor;
              //   for (var j = 0; j < $scope.customername.length; j++) {
              //     if (member.customerId == $scope.customername[j].id) {
              //       member.customername = $scope.customername[j].name;
              //       break;
              //     }
              //   }
              // });
              Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
                $scope.categoryname = categ;
                for (var j = 0; j < $scope.categoryname.length; j++) {
                  if (member.categoryId == $scope.categoryname[j].id) {
                    member.categoryname = $scope.categoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
                $scope.subcategoryname = subcateg;
                for (var j = 0; j < $scope.subcategoryname.length; j++) {
                  if (member.subcategoryId == $scope.subcategoryname[j].id) {
                    member.subcategoryname = $scope.subcategoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
                $scope.username = userresp;
                for (var j = 0; j < $scope.username.length; j++) {
                  if (member.assignedto == $scope.username[j].id) {
                    member.username = $scope.username[j].username;
                    break;
                  }
                }
              });
              Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
                $scope.statusname = todostatusresp;
                for (var j = 0; j < $scope.statusname.length; j++) {
                  if (member.statusId == $scope.statusname[j].id) {
                    member.statusname = $scope.statusname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
                $scope.updatetypename = updatetyperesp;
                for (var j = 0; j < $scope.updatetypename.length; j++) {
                  if (member.producttypeid == $scope.updatetypename[j].id) {
                    member.updatetypename = $scope.updatetypename[j].name;
                    break;
                  }
                }
              });
              Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
                $scope.serviceeventname = serviceeventresp;
                for (var j = 0; j < $scope.serviceeventname.length; j++) {
                  if (member.serviceeventid == $scope.serviceeventname[j].id) {
                    member.serviceeventname = $scope.serviceeventname[j].name;
                    break;
                  }
                }
              });
            });
          });

        }

      } else if (window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
        console.log("Inside Role - 7");
        console.log($scope.orgStructureFilter, "ORGSTRUCT-FILTER-523")

        //-- Previous code of Single state & multi city commented ---------------------------
        // var str = $scope.orgStructureFilter;
        // var strOrgstruct = str.split('"')
        // //console.log(strOrgstruct[1], "strorgstruct", strOrgstruct)
        // //  Restangular.all('members?filter[where][deleteflag]=false&filter[where][assignFlag]=true&filter[where][completedFlag]=false&filter[where][createdby]=' + window.sessionStorage.userId).getList().then(function (membr) {
        // // Restangular.all('members?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][createdby]=' + window.sessionStorage.userId).getList().then(function (membr) {

        // // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false').getList().then(function (membr) {
        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + strOrgstruct[1] + '%').getList().then(function (membr) {
        //   $scope.members = membr;
        //   console.log($scope.members, "TICKET-DISPLAY-530")
        //   angular.forEach($scope.members, function (member, index) {
        //     member.index = index + 1;
        //     // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
        //     //   $scope.customername = custmor;
        //     //   for (var j = 0; j < $scope.customername.length; j++) {
        //     //     if (member.customerId == $scope.customername[j].id) {
        //     //       member.customername = $scope.customername[j].name;
        //     //       break;
        //     //     }
        //     //   }
        //     // });
        //     Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
        //       $scope.categoryname = categ;
        //       for (var j = 0; j < $scope.categoryname.length; j++) {
        //         if (member.categoryId == $scope.categoryname[j].id) {
        //           member.categoryname = $scope.categoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
        //       $scope.subcategoryname = subcateg;
        //       for (var j = 0; j < $scope.subcategoryname.length; j++) {
        //         if (member.subcategoryId == $scope.subcategoryname[j].id) {
        //           member.subcategoryname = $scope.subcategoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
        //       $scope.username = userresp;
        //       for (var j = 0; j < $scope.username.length; j++) {
        //         if (member.assignedto == $scope.username[j].id) {
        //           member.username = $scope.username[j].username;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
        //       $scope.statusname = todostatusresp;
        //       for (var j = 0; j < $scope.statusname.length; j++) {
        //         if (member.statusId == $scope.statusname[j].id) {
        //           member.statusname = $scope.statusname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //   });
        // });
        //-- End of Previous code of Single state & multi city commented ---------------------------

        // from here new code ---
        console.log("OrgstructFilter - City Level", $scope.orgStructureFilter);
        var str = $scope.orgStructureFilter;
        var strOrgstruct = str.split('"');
        //console.log(strOrgstruct[1], "strorgstruct-638", strOrgstruct)
        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-651-City Level")

        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

            $scope.membersnew = membr;
            //console.log($scope.membersnew, "TICKET-DISPLAY-662")
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.members = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-669")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-670")
            // console.log($scope.members, "$scope.members-760")

            angular.forEach($scope.members, function (member, index) {
              member.index = index + 1;
              // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              //   $scope.customername = custmor;
              //   for (var j = 0; j < $scope.customername.length; j++) {
              //     if (member.customerId == $scope.customername[j].id) {
              //       member.customername = $scope.customername[j].name;
              //       break;
              //     }
              //   }
              // });
              Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
                $scope.categoryname = categ;
                for (var j = 0; j < $scope.categoryname.length; j++) {
                  if (member.categoryId == $scope.categoryname[j].id) {
                    member.categoryname = $scope.categoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
                $scope.subcategoryname = subcateg;
                for (var j = 0; j < $scope.subcategoryname.length; j++) {
                  if (member.subcategoryId == $scope.subcategoryname[j].id) {
                    member.subcategoryname = $scope.subcategoryname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
                $scope.username = userresp;
                for (var j = 0; j < $scope.username.length; j++) {
                  if (member.assignedto == $scope.username[j].id) {
                    member.username = $scope.username[j].username;
                    break;
                  }
                }
              });
              Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
                $scope.statusname = todostatusresp;
                for (var j = 0; j < $scope.statusname.length; j++) {
                  if (member.statusId == $scope.statusname[j].id) {
                    member.statusname = $scope.statusname[j].name;
                    break;
                  }
                }
              });
              Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
                $scope.updatetypename = updatetyperesp;
                for (var j = 0; j < $scope.updatetypename.length; j++) {
                  if (member.producttypeid == $scope.updatetypename[j].id) {
                    member.updatetypename = $scope.updatetypename[j].name;
                    break;
                  }
                }
              });
              Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
                $scope.serviceeventname = serviceeventresp;
                for (var j = 0; j < $scope.serviceeventname.length; j++) {
                  if (member.serviceeventid == $scope.serviceeventname[j].id) {
                    member.serviceeventname = $scope.serviceeventname[j].name;
                    break;
                  }
                }
              });
            });
          });

        }

      } else if (window.sessionStorage.roleId == 8 || window.sessionStorage.roleId == 2) {
        // else if (window.sessionStorage.roleId == 8 || window.sessionStorage.roleId == 2 || window.sesionStorage.roleId == 3 || window.sessionStorage.roleId == 4) {
        console.log("Inside Zone Manager", window.sessionStorage.orgStructure);
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%').getList().then(function (membr) {
          console.log("Inside Zone Manager=> Tickets", membr);
          $scope.members = membr;
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;

            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
              $scope.username = userresp;
              for (var j = 0; j < $scope.username.length; j++) {
                if (member.assignedto == $scope.username[j].id) {
                  member.username = $scope.username[j].username;
                  break;
                }
              }
            });
            Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
              $scope.statusname = todostatusresp;
              for (var j = 0; j < $scope.statusname.length; j++) {
                if (member.statusId == $scope.statusname[j].id) {
                  member.statusname = $scope.statusname[j].name;
                  break;
                }
              }
            });
            Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
              $scope.updatetypename = updatetyperesp;
              for (var j = 0; j < $scope.updatetypename.length; j++) {
                if (member.producttypeid == $scope.updatetypename[j].id) {
                  member.updatetypename = $scope.updatetypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
              $scope.serviceeventname = serviceeventresp;
              for (var j = 0; j < $scope.serviceeventname.length; j++) {
                if (member.serviceeventid == $scope.serviceeventname[j].id) {
                  member.serviceeventname = $scope.serviceeventname[j].name;
                  break;
                }
              }
            });
          });
        });

      } else if (window.sessionStorage.roleId == 3) {
        console.log("Inside Zone Manager", window.sessionStorage.orgStructure);
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%').getList().then(function (membr) {
          console.log("Inside Zone Manager=> Tickets", membr);
          $scope.members = membr;
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;

            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
              $scope.username = userresp;
              for (var j = 0; j < $scope.username.length; j++) {
                if (member.assignedto == $scope.username[j].id) {
                  member.username = $scope.username[j].username;
                  break;
                }
              }
            });
            Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
              $scope.statusname = todostatusresp;
              for (var j = 0; j < $scope.statusname.length; j++) {
                if (member.statusId == $scope.statusname[j].id) {
                  member.statusname = $scope.statusname[j].name;
                  break;
                }
              }
            });
            Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
              $scope.updatetypename = updatetyperesp;
              for (var j = 0; j < $scope.updatetypename.length; j++) {
                if (member.producttypeid == $scope.updatetypename[j].id) {
                  member.updatetypename = $scope.updatetypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
              $scope.serviceeventname = serviceeventresp;
              for (var j = 0; j < $scope.serviceeventname.length; j++) {
                if (member.serviceeventid == $scope.serviceeventname[j].id) {
                  member.serviceeventname = $scope.serviceeventname[j].name;
                  break;
                }
              }
            });
          });
        });

      } else if (window.sessionStorage.roleId == 4) {
        console.log("Inside Zone Manager", window.sessionStorage.orgStructure);
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%').getList().then(function (membr) {
          console.log("Inside Zone Manager=> Tickets", membr);
          $scope.members = membr;
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;

            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (userresp) {
              $scope.username = userresp;
              for (var j = 0; j < $scope.username.length; j++) {
                if (member.assignedto == $scope.username[j].id) {
                  member.username = $scope.username[j].username;
                  break;
                }
              }
            });
            Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {
              $scope.statusname = todostatusresp;
              for (var j = 0; j < $scope.statusname.length; j++) {
                if (member.statusId == $scope.statusname[j].id) {
                  member.statusname = $scope.statusname[j].name;
                  break;
                }
              }
            });
            Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
              $scope.updatetypename = updatetyperesp;
              for (var j = 0; j < $scope.updatetypename.length; j++) {
                if (member.producttypeid == $scope.updatetypename[j].id) {
                  member.updatetypename = $scope.updatetypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
              $scope.serviceeventname = serviceeventresp;
              for (var j = 0; j < $scope.serviceeventname.length; j++) {
                if (member.serviceeventid == $scope.serviceeventname[j].id) {
                  member.serviceeventname = $scope.serviceeventname[j].name;
                  break;
                }
              }
            });
          });
        });

      }


    };

    $scope.okOnetooOne = function (fullname) {
      $scope.modalInstance1.close();

      var fullname = fullname;
      $rootScope.fullname = $window.sessionStorage.fullName = fullname;
      console.log('$rootScope.okOnetooOne', $rootScope.fullname);
      $location.path("/onetoone/" + fullname);
      $route.reload();
    };

    $scope.cancelOnetooOne = function () {
      $scope.modalInstance1.close();
    };

    $scope.loading = true;
    //$scope.LoadingMOdal = false;
    $scope.toggleLoading = function () {
      $scope.modalInstanceLoad = $modal.open({
        animation: true,
        templateUrl: 'template/LodingModal.html',
        scope: $scope,
        backdrop: 'static',
        size: 'sm'

      });
      //$scope.LoadingMOdal = !$scope.LoadingMOdal;
    };



    $scope.openMainReportIncident = function () {
      $scope.modalMainReport = $modal.open({
        animation: true,
        templateUrl: 'template/MainReportIncident.html',
        scope: $scope,
        backdrop: 'static'

      });

    };

    $scope.okMainReportIncident = function (fullname) {
      $scope.modalMainReport.close();

      var fullname = fullname;
      $rootScope.fullname = $window.sessionStorage.fullName = fullname;
      console.log('$rootScope.okReportIncident', $rootScope.fullname);
      $location.path("/reportincident");
      $route.reload();
    };

    $scope.cancelMainReportIncident = function () {
      $scope.modalMainReport.close();
    };
    /***********************************************/
    $scope.openApplyforScheme = function () {
      $scope.modalInstance1 = $modal.open({
        animation: true,
        templateUrl: 'template/ApplyforScheme.html',
        scope: $scope,
        backdrop: 'static'

      });
    };

    $scope.okApplyforScheme = function (fullname) {
      $scope.modalInstance1.close();

      var fullname = fullname;
      $rootScope.fullname = $window.sessionStorage.fullName = fullname;
      console.log('$rootScope.okOnetooOne', $rootScope.fullname);
      $location.path("/applyforscheme");
      $route.reload();
    };

    $scope.cancelApplyforScheme = function () {
      $scope.modalInstance1.close();
    };

    $scope.openApplyforDocument = function () {
      $scope.modalInstance1 = $modal.open({
        animation: true,
        templateUrl: 'template/ApplyforDocument.html',
        scope: $scope,
        backdrop: 'static'

      });
    };

    $scope.okApplyforDocument = function (fullname) {
      $scope.modalInstance1.close();

      var fullname = fullname;
      $rootScope.fullname = $window.sessionStorage.fullName = fullname;
      console.log('$rootScope.okOnetooOne', $rootScope.fullname);
      $location.path("/applyfordocument");
      $route.reload();
    };

    $scope.cancelApplyforDocument = function () {
      $scope.modalInstance1.close();
    };


    $scope.openfPlaning = function () {
      $scope.modalInstance1 = $modal.open({
        animation: true,
        templateUrl: 'template/fPlaning.html',
        scope: $scope,
        backdrop: 'static'

      });
    };

    $scope.okfPlaning = function (fullname) {
      $scope.modalInstance1.close();
      var fullname = fullname;
      $rootScope.fullname = $window.sessionStorage.fullName = fullname;
      console.log('$rootScope.fullname', $rootScope.fullname);
      $location.path("/financialPlanning");
      $route.reload();
    };

    $scope.cancelfPlaning = function () {
      $scope.modalInstance1.close();
    };
    /****************************************/

    $scope.menu = [{
        'title': 'Home',
        'link': '/'
      },
      {
        'title': 'States',
        'link': '/states'
      },
      {
        'title': 'Contact',
        'link': '#'
      }
    ];

    $scope.fwprofileedit = true;
    $scope.coprofileedit = true;
    if ($window.sessionStorage.roleId == 5) {
      $scope.coprofileedit = false;
      $scope.UserId = $window.sessionStorage.UserEmployeeId;
    } else {
      //$scope.UserId = $window.sessionStorage.UserEmployeeId;
      $scope.fwprofileedit = false;
      $scope.UserId = $window.sessionStorage.UserEmployeeId;
    }

    console.log($window.sessionStorage.userId + 'sddsf');
    $scope.showNav = function () {
      //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
      if ($window.sessionStorage.userId) {
        return true;
      }
    };
    $scope.isActive = function (route) {
      return route === $location.path();
    };

    $scope.getClass = function (path) {
      if ($location.path().substr(0, path.length) === path) {
        return 'active';
      } else {
        return '';
      }
    }

    $scope.logout = function () {
      //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
      Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
        $window.sessionStorage.userId = '';
        console.log('Logout');
      }).then(function (redirect) {

        $location.path("/login");
        $idle.unwatch();

      });
    };


    /*************************** Language *******************************/
    $scope.languages = Restangular.all('languages').getList().$object;



  });
