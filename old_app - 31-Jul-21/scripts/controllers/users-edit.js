'use strict';

angular.module('secondarySalesApp')
  .controller('UsersEditCtrl', function ($scope, Restangular, $routeParams, $window, AnalyticsRestangular) {
    //        if ($window.sessionStorage.roleId != 1) {
    //            window.location = "/";
    //            $scope.usernameDisable = true;
    //        } else {
    //            $scope.usernameDisable = false;
    //        }
    $scope.heading = 'User Update';
    $scope.Saved = true;
    $scope.Updated = false;
    $scope.roleDisable = true;
    $scope.userDisable = true;
    $scope.isPartner = $window.sessionStorage.isPartner;
    $scope.languages = Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().$object;

    $scope.groups = Restangular.all('groups').getList().$object;
    //$scope.departments = Restangular.all('departments').getList().$object;
    //$scope.ims = Restangular.all('ims').getList().$object;
    if ($window.sessionStorage.roleId === '1') {
      $scope.roles = Restangular.all('roles?filter={"where": {"type": {"inq": ["null", "admin", "others", "partner"]}}}').getList().$object;
     }else if ($window.sessionStorage.isPartner === 'true') {
      $scope.roles = Restangular.all('roles?filter={"where": {"type": {"inq": ["partner"]}}}').getList().$object;
     }else{
      $scope.roles = Restangular.all('roles').getList().$object;
     }
    $scope.users = Restangular.all('users').getList().$object;
    $scope.employees = Restangular.all('employees').getList().$object;
    $scope.zones = Restangular.all('zones').getList().$object;
    //$scope.roles = Restangular.all('roles').getList().$object;
    $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
    $scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
    $scope.countries = Restangular.all('countries').getList().$object;
    /*toggle*/
    $scope.init = function () {
      $scope.user.isPartner = true;
    }

    $scope.changeStatus = function () {
      $scope.user.isPartner = !$scope.user.isPartner;
      if ($scope.user.isPartner == true) {
        $scope.statusnew = "yes";
        console.log("in if ");
        console.log("$scope.statusnew ", $scope.statusnew);
      } else {
        $scope.statusnew = "no";
        console.log("in else ");
        console.log("$scope.statusnew ", $scope.statusnew);
      }
    }

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });
    $scope.$watch('user.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('user.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });




    $scope.user = {};
    //        if ($window.sessionStorage.roleId != 1) {
    //            window.location = "/";
    //        }

    if ($routeParams.id) {
      Restangular.one('users', $routeParams.id).get().then(function (user) {
        $scope.original = user;
        $scope.user = Restangular.copy($scope.original);

        Restangular.one('roles/findOne?filter[where][id]=' + user.roleId).get().then(function (role) {
          $scope.user.level = role.level;
          Restangular.all('organisationlocations?filter[where][level]=' + role.level).getList().then(function (locations) {
            $scope.locations = locations;
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + role.level).get().then(function (orglvl) {
              Restangular.all('organisationlevels?filter[where][slno][lte]=' + orglvl.slno + '&filter[where][deleteflag]=false&filter[where][languageparent]=true' + '&filter[order]=slno ASC').getList().then(function (organisationlevels) {
                $scope.organisationlevels = organisationlevels;
                $scope.orgstructure = user.orgStructure.split(";");

                for (var i = 0; i < $scope.organisationlevels.length; i++) {
                  for (var j = 0; j < $scope.orgstructure.length; j++) {
                    $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
                    if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
                      $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1].split(",");
                    }
                  }
                }

                // console.log("$scope.organisationlevels", $scope.organisationlevels);

                Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                  $scope.organisationlocations = organisationlocations;

                  angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                    organisationlevel.index = index;
                    var orgLocations = [];
                    for(var ol=0;ol<organisationlocations.length;ol++){
                      if(organisationlocations[ol].level === organisationlevel.id){
                        orgLocations.push(organisationlocations[ol]);
                      }
                    }
                    organisationlevel.organisationlocations = orgLocations;
                  });
                });
              });
            });
          });
        });




      });
    };

    $scope.Cancel = function () {
      window.location = '/users';
    };

    $scope.hideState = false;
    $scope.hideDistrict = false;
    $scope.hideCo = false;
    $scope.$watch('user.roleId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '' || newValue == undefined) {
        return;
      } else {
        //                Restangular.one('roles/findOne?filter[where][id]=' + newValue).get().then(function (role) {
        //                    $scope.user.level = role.level;
        //
        //                    Restangular.all('organisationlevels?filter[where][id][lte]=' + role.level).getList().then(function (ogrlevls) {
        //                        $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + ogrlevls[0].id).getList().then(function (ogrlocs) {
        //                            $scope.organisationlevels = ogrlevls;
        //                            angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
        //                                if (organisationlevel.id === ogrlevls[0].id) {
        //                                    organisationlevel.organisationlocations = ogrlocs;
        //                                }
        //                            });
        //                        });
        //                    });
        //                });
      }
    });

    $scope.orgLevelChange = function (index, level) {
      //console.log(index, level, level.length, "Msg-338")
      $scope.organisationlocations = Restangular.all('organisationlocations?filter={"where":{"and":[{"parent":{"inq":[' + level + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (ogrlocs) {
        console.log(index, $scope.organisationlevels.length, level.length, "index-scope.organisationlevels.length-level.length")
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocs;
        }
      });
      //}
    };



    /********************************************** UPDATE ****************************/

    Restangular.all('users?filter[where][id]=' + $routeParams.id).getList().then(function (submitcomem) {
      $scope.getUserId = submitcomem[0].employeeid;
    });

    $scope.comember = {};
    $scope.fwworker = {};
    $scope.validatestring = '';
    $scope.Update = function () {
      //document.getElementById('Username').style.border = "";
      if ($scope.user.roleId == '' || $scope.user.roleId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Your Role';
        //} else if ($scope.user.usrName == '' || $scope.user.usrName == null) {
        //	$scope.validatestring = $scope.validatestring + 'Please Select Your User';
      } else if ($scope.user.username == '' || $scope.user.username == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter User Name';
        document.getElementById('Username').style.borderColor = "#FF0000";
      } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Phone No';
      } else if ($scope.user.mobile.length != 10) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Valid Phone No';
      } else if ($scope.user.email == '' || $scope.user.email == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
        document.getElementById('email').style.borderColor = "#FF0000";
        //} else if ($scope.user.password == '' || $scope.user.password == null) {
        //	$scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
        //	document.getElementById('password').style.borderColor = "#FF0000";
      } else if ($scope.user.language == '' || $scope.user.language == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring2 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
        // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.orgstructure = "";
        for (var i = 0; i < $scope.organisationlevels.length; i++) {
          if ($scope.orgstructure === "") {
            $scope.orgstructure = $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          } else {
            $scope.orgstructure = $scope.orgstructure + ";" + $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          }
        }
        $scope.user.orgStructure = $scope.orgstructure;
        Restangular.one('users/' + $routeParams.id).customPUT($scope.user).then(function (subResponse) {
          console.log('subResponse', subResponse);
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
          window.location = '/users';

          //                    AnalyticsRestangular.one('users/' + $routeParams.id).customPUT($scope.user).then(function (analyticssubResponse) {
          //                        if (subResponse.roleId == 5) {
          //                            $scope.comember.helpline = subResponse.mobile;
          //                            Restangular.one('comembers/' + $scope.getUserId).customPUT($scope.comember).then(function (submitcomember) {
          //                                console.log('submitcomember', submitcomember);
          //                                window.location = '/users';
          //                            });
          //                        } else if (subResponse.roleId == 6) {
          //                            $scope.fwworker.mobile = subResponse.mobile;
          //                            Restangular.one('fieldworkers/' + $scope.getUserId).customPUT($scope.fwworker).then(function (submitfwer) {
          //                                console.log('submitfwer', submitfwer);
          //                                window.location = '/users';
          //                            });
          //                        } else {
          //                            window.location = '/users';
          //                        }
          //                    });
        }, function (error) {
          //console.log('error', error);
          $scope.showValidation = !$scope.showValidation;
          /*if (response.data.error.constraint == "uniqueness") {
          	$scope.validatestring1 = 'Email' + ' ' + $scope.user.email + ' ' + 'Already Exists';
          } else {
          	$scope.validatestring1 = response.data.error.detail;
          }*/
          $scope.validatestring1 = 'Username/Email Already Exists';
          //console.error('console.error', response);
        });
      };
    };

    $scope.modalTitle = 'Thank You';
    $scope.message = 'User has been Updated!';
    $scope.showValidation = false;
    $scope.showUniqueValidation = false;
    $scope.toggleValidation = function () {
      $scope.showUniqueValidation = !$scope.showUniqueValidation;
    };


  });
