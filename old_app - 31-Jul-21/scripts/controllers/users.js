'use strict';

angular.module('secondarySalesApp')
  .controller('UsersCtrl', function ($scope, Restangular, $http, $window, $route, $filter) {

    //        if ($window.sessionStorage.roleId != 1) {
    //            window.location = "/";
    //        }

    if ($window.sessionStorage.user_zoneId == null || $window.sessionStorage.user_zoneId == undefined || $window.sessionStorage.user_facilityId == null || $window.sessionStorage.user_facilityId == undefined) {

      $window.sessionStorage.user_zoneId = null;
      $window.sessionStorage.user_facilityId = null;
      $window.sessionStorage.user_currentPage = 1;
      $window.sessionStorage.user_currentPagesize = 25;
    } else {
      $scope.countryId = $window.sessionStorage.user_zoneId;
      $scope.failityId = $window.sessionStorage.user_facilityId;
      $scope.currentpage = $window.sessionStorage.user_currentPage;
      $scope.pageSize = $window.sessionStorage.user_currentPagesize;
    }


    if ($window.sessionStorage.prviousLocation != "partials/users-form") {
      $window.sessionStorage.user_zoneId = '';
      $window.sessionStorage.user_facilityId = '';
      $window.sessionStorage.user_currentPage = 1;
      $window.sessionStorage.user_currentPagesize = 25;
      //$scope.currentpage = 1;
      //$scope.pageSize = 25;

      //$scope.tds = Restangular.all('users?filter[where][roleId]=1&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (response) {

      //$scope.tds = Restangular.all('users?filter={"where":{"and":[{"roleId":{"inq":[16]}},{"id":{"inq":[' +$window.sessionStorage.userId+']}}]}}').getList().then(function (response) {	
      Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
        $scope.organisationlocations = organisationlocations;
        console.log($scope.organisationlocations, "$scope.organisationlocations-member-37");
        var userUrl = '';
        if($window.sessionStorage.isPartner === 'true'){
          userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId;
        }else{
          userUrl = 'users?filter[where][managerId]=0'
        }
        $scope.led = Restangular.all(userUrl).getList().then(function (response) {

          //$scope.tds = Restangular.all('users?filter={"where":{"and":[{"roleId":{"inq":[16]}},{"id":{"inq":[1249]}}]}}').getList().then(function (response) {

          console.log('response', response);
          $scope.users = response;
          angular.forEach($scope.users, function (member, index) {
            member.index = index + 1;
            if (member.deleteflag === 'true') {
              member.backgroundColor = "#ff0000"
            } else {
              member.backgroundColor = "#000000"
            }

            if (member.orgStructure != null) {
              var array = member.orgStructure.split(';');
              console.log(array, array.length, "MSG-75")

              member.pinList = [];
              member.cityList = [];
              member.stateList = [];
              member.zoneList = [];

              if (array.length == 5) {
                var pinarray = array[array.length - 1].split('-')[1].split(',');
                var cityarray = array[array.length - 2].split('-')[1].split(',');
                var statearray = array[array.length - 3].split('-')[1].split(',');
                var zonearray = array[array.length - 4].split('-')[1].split(',');
                
                //member.pinList = pinarray.split(',');
                if (pinarray.length > 1) {
                  //member.pinList = pinarray.split(',');
                  for (var p = 0; p <= pinarray.length - 1; p++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + pinarray[p] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.pinnumber = orglocResponse[0].name;
                      member.pinList.push(member.pinnumber);
                    });
                    //member.pinList.push(pinarray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + pinarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.pinnumber = orglocResponse[0].name;
                    member.pinList.push(member.pinnumber);
                  });
                  //member.pinList = pinarray[0];
                }
                //member.cityList = cityarray.split(',');
                if (cityarray.length > 1) {
                  //member.cityList = cityarray.split(',');
                  for (var c = 0; c <= cityarray.length - 1; c++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.cityname = orglocResponse[0].name;
                      member.cityList.push(member.cityname);
                    });
                    //member.cityList.push(cityarray[c]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.cityname = orglocResponse[0].name;
                    member.cityList.push(member.cityname);
                  });
                  //member.cityList = cityarray[0];
                }
                //member.stateList = statearray.split(',');
                if (statearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= statearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.statename = orglocResponse[0].name;
                      member.stateList.push(member.statename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList = statearray[0];
                }

                if (zonearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= zonearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.zonename = orglocResponse[0].name;
                      member.zoneList.push(member.zonename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList = statearray[0];
                }
                console.log(member.pinList, "member.pinList-82")
                console.log(member.cityList, "member.cityList-83")
                console.log(member.stateList, "member.stateList-84")
                console.log(member.zoneList, "member.zoneList-84")
              }

            else if (array.length == 4) {
                // var pinarray = array[array.length - 1].split('-')[1].split(',');
                // var cityarray = array[array.length - 2].split('-')[1].split(',');
                // var statearray = array[array.length - 3].split('-')[1].split(',');
                
                var cityarray = array[array.length - 1].split('-')[1].split(',');
                var statearray = array[array.length - 2].split('-')[1].split(',');
                var zonearray = array[array.length - 3].split('-')[1].split(',');
                
                //member.pinList = pinarray.split(',');
                // if (pinarray.length > 1) {
                //   //member.pinList = pinarray.split(',');
                //   for (var p = 0; p <= pinarray.length - 1; p++) {
                //     Restangular.one('organisationlocations?filter[where][id]=' + pinarray[p] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //       //console.log(orglocResponse, "orglocResponse-124")
                //       member.pinnumber = orglocResponse[0].name;
                //       member.pinList.push(member.pinnumber);
                //     });
                //     //member.pinList.push(pinarray[k]);
                //   }
                // } else {
                //   Restangular.one('organisationlocations?filter[where][id]=' + pinarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //     //console.log(orglocResponse, "orglocResponse-124")
                //     member.pinnumber = orglocResponse[0].name;
                //     member.pinList.push(member.pinnumber);
                //   });
                //   //member.pinList = pinarray[0];
                // }
                //member.cityList = cityarray.split(',');
                if (cityarray.length > 1) {
                  //member.cityList = cityarray.split(',');
                  for (var c = 0; c <= cityarray.length - 1; c++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.cityname = orglocResponse[0].name;
                      member.cityList.push(member.cityname);
                    });
                    //member.cityList.push(cityarray[c]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.cityname = orglocResponse[0].name;
                    member.cityList.push(member.cityname);
                  });
                  //member.cityList = cityarray[0];
                }
                //member.stateList = statearray.split(',');
                if (statearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= statearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.statename = orglocResponse[0].name;
                      member.stateList.push(member.statename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList = statearray[0];
                }

                if (zonearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= zonearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.zonename = orglocResponse[0].name;
                      member.zoneList.push(member.zonename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList = statearray[0];
                }
               // console.log(member.pinList, "member.pinList-82")
                console.log(member.cityList, "member.cityList-83")
                console.log(member.stateList, "member.stateList-84")
                console.log(member.zoneList, "member.zoneList-84")
                
              } else if (array.length == 3) {
                // var cityarray = array[array.length - 1].split('-')[1].split(',');
                // var statearray = array[array.length - 2].split('-')[1].split(',');
                
                var statearray = array[array.length - 1].split('-')[1].split(',');
                var zonearray = array[array.length - 2].split('-')[1].split(',');
                //member.cityList = cityarray.split(',');
                // if (cityarray.length > 1) {
                //   // member.cityList = cityarray.split(',');
                //   for (var c = 0; c <= cityarray.length - 1; c++) {
                //     Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //       //console.log(orglocResponse, "orglocResponse-124")
                //       member.cityname = orglocResponse[0].name;
                //       member.cityList.push(member.cityname);
                //     });
                //     //member.cityList.push(cityarray[c]);
                //   }
                // } else {
                //   Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //     //console.log(orglocResponse, "orglocResponse-124")
                //     member.cityname = orglocResponse[0].name;
                //     member.cityList.push(member.cityname);
                //   });
                //   //member.cityList = cityarray[0];
                // }
                //member.stateList = statearray.split(',');
                if (statearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= statearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.statename = orglocResponse[0].name;
                      member.stateList.push(member.statename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList = statearray[0];
                }

                if (zonearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= zonearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.zonename = orglocResponse[0].name;
                      member.zoneList.push(member.zonename);
                    });
                    //member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList = statearray[0];
                }
                console.log(member.cityList, "member.cityList-100")
                console.log(member.stateList, "member.stateList-101")
              } else if (array.length == 2) {
               // var statearray = array[array.length - 1].split('-')[1].split(',');
               var zonearray = array[array.length - 1].split('-')[1].split(',');
                console.log(zonearray, "statearray-104")
                // if (statearray.length > 1) {
                //   //member.stateList = statearray.split(',');
                //   for (var k = 0; k <= statearray.length - 1; k++) {
                //     Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //       //console.log(orglocResponse, "orglocResponse-124")
                //       member.statename = orglocResponse[0].name;
                //       member.stateList.push(member.statename);
                //     });
                //     // member.stateList.push(statearray[k]);
                //   }
                // } else {
                //   Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                //     //console.log(orglocResponse, "orglocResponse-124")
                //     member.statename = orglocResponse[0].name;
                //     member.stateList.push(member.statename);
                //   });
                //   //member.stateList = statearray[0];
                // }
                if (zonearray.length > 1) {
                  //member.stateList = statearray.split(',');
                  for (var k = 0; k <= zonearray.length - 1; k++) {
                    Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                      //console.log(orglocResponse, "orglocResponse-124")
                      member.zonename = orglocResponse[0].name;
                      member.zoneList.push(member.zonename);
                    });
                    // member.stateList.push(statearray[k]);
                  }
                } else {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList = statearray[0];
                }
                console.log(member.zoneList, "member.zoneList-110")
              }
              // var subarray = array[array.length - 1].split('-')[1].split(',');
              // console.log(subarray, "SUB-ARRAY")

              // if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
              //   member.uploadedfilesList = [];
              // } else 
              //{
              // member.uploadedfilesList = member.uploadedfiles.split(',');
              // member.stateList = member.statearray.split(',');
              //}

            }

            //---- Initially working code -----------------------------------------
            // if (member.orgStructure != null) {
            //   member.locationname = "";
            //   var array = member.orgStructure.split(';');
            //   var subarray = array[array.length - 1].split('-')[1].split(',');
            //   for (var i = 0; i < $scope.organisationlocations.length; i++) {
            //     for (var j = 0; j < subarray.length; j++) {
            //       if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
            //         if (member.locationname === "") {
            //           member.locationname = $scope.organisationlocations[i].name;
            //         } else {
            //           member.locationname = member.locationname + "," + $scope.organisationlocations[i].name;
            //         }
            //       }
            //     }
            //   }
            //   member.location = array[array.length - 1].split('-')[1];
            // }
            //---- End of Initially working code -----------------------------------------

            // //---- Later Modified by me saty for multiple pincode display ----------------------
            // if (member.orgStructure != null) {
            //   //member.locationnamestate = "";
            //   member.locationname = "";
            //   member.locationnamepincode = "";
            //   console.log(member.orgStructure, "orgStructure-72")
            //   var array = member.orgStructure.split(';');
            //   console.log(array, array.length, "MSG-75")
            //   var subarray = array[array.length - 1].split('-')[1].split(',');
            //   console.log(subarray, "SUB-ARRAY")
            //   for (var i = 0; i < $scope.organisationlocations.length; i++) {
            //     for (var j = 0; j < subarray.length; j++) {
            //       if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
            //         if ($scope.organisationlocations[i].parent == null) {
            //           if (member.locationname === "") {
            //             member.locationname = $scope.organisationlocations[i].name;
            //           } else {
            //             member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
            //           }
            //         } else if ($scope.organisationlocations[i].parent != null) {
            //           Restangular.one('organisationlocations?filter[where][id]=' + $scope.organisationlocations[i].parent + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
            //             //console.log(orglocResponse, "orglocResponse-89")
            //             member.locationname = orglocResponse[0].name;
            //           });
            //           if (member.locationnamepincode === "") {
            //             member.locationnamepincode = $scope.organisationlocations[i].name;
            //           } else {
            //             member.locationnamepincode = member.locationnamepincode + ", " + $scope.organisationlocations[i].name;
            //           }
            //         }
            //       }
            //     }
            //   }
            //   member.location = array[array.length - 1].split('-')[1];
            // }
            // //---- End of Later Modified by me saty for multiple pincode display ----------------------




          });
        });

      });
    }

    $scope.currentpage = $window.sessionStorage.user_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.user_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.user_currentPagesize;
    $scope.pageFunction = function (mypage) {
      $scope.pageSize = mypage;
      $window.sessionStorage.user_currentPagesize = mypage;
    };

    Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
      $scope.zones = znes;
      $scope.countryId = $window.sessionStorage.user_zoneId;
    });


    $scope.getRole = function (stakeholdertype) {
      return Restangular.one('roles', stakeholdertype).get().$object;
    };
    $scope.getLanguage = function (language) {
      return Restangular.one('languagedefinitions/findOne?filter[where][id]=' + language).get().$object;
    };
    $scope.getLocation = function (location) {
      return Restangular.one('organisationlocations', location).get().$object;
    };
    $scope.getLevel = function (level) {
      return Restangular.one('organisationlevels', level).get().$object;
    };
    $scope.getCustomer = function (customer) {
      return Restangular.one('customers', customer).get().$object;
    };

    /*****************************************************************	


			$scope.user = {
				flag: true,
				email: '',
				zoneId: '',
				salesAreaId: '',
				coorgId: '',
				lastmodifiedtime: $filter('date')(new Date(), 'y-MM-dd'),
				lastmodifiedby: $window.sessionStorage.UserEmployeeId
				
			};
			$scope.lastmodifiedby = $scope.user.usrName;

			console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
			console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
			console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
			console.log('$window.sessionStorage.LastModifyBy', $scope.lastmodifiedby);
			console.log('$window.sessionStorage.Site', $window.sessionStorage.UserEmployeeId);
			
			$scope.getRole = function (stakeholdertype) {
				return Restangular.one('roles', stakeholdertype).get().$object;
			};


			$scope.groups = Restangular.all('groups?filter[where][deleteflag]=null').getList().$object;
			$scope.departments = Restangular.all('departments?filter[where][deleteflag]=null').getList().$object;
			$scope.ims = Restangular.all('ims?filter[where][deleteflag]=null').getList().$object;
			$scope.admins = Restangular.all('employees?filter[where][groupId]=1&filter[where][deleteflag]=null').getList().$object;
			$scope.spms = Restangular.all('employees?filter[where][groupId]=3&filter[where][deleteflag]=null').getList().$object;
			$scope.rios = Restangular.all('employees?filter[where][groupId]=4&filter[where][deleteflag]=null').getList().$object;
			//$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][groupId]=4').getList().$object;

			$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;


			$scope.comembers = Restangular.all('comembers?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;

			$scope.employees = Restangular.all('employees?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;

			//$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;

			//$scope.salesAreas = Restangular.all('sales-areas?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=null').getList().$object;

			//$scope.zones = Restangular.all('zones').getList().$object;

			//$scope.employees = Restangular.all('employees').getList().$object;
			//$scope.comembers = Restangular.all('comembers').getList().$object;
			//$scope.languagemasters = Restangular.all('languagemasters').getList().$object;

			/************************************************ SAVE *******************************
			$scope.createduser = {
				"usercreated": 'yes'
			}
			$scope.Save = function () {
				$scope.users.post($scope.user).then(function () {
					console.log('$scope.user', $scope.user);
					// window.location = '/users';

					if ($scope.user.roleId == 1 || $scope.user.roleId == 3 || $scope.user.roleId == 4) {
						Restangular.one('employees/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
							window.location = '/users';
						});
					}

					if ($scope.user.roleId == 5) {
						Restangular.one('comembers/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
							window.location = '/users';
						});
					}

					if ($scope.user.roleId == 6) {
						Restangular.one('fieldworkers/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
							window.location = '/users';
						});
					}
				}, function (error) {
					console.log('error', error);
				});
			};


			/******************************************** WATCH *************************

			$scope.$watch('user.usrName', function (newValue, oldValue) {
				if (newValue === oldValue || newValue == '' || newValue == undefined) {
					return;
				} else {
					$scope.newUser = JSON.parse(newValue);
					if ($scope.user.roleId == 1 || $scope.user.roleId == 3 || $scope.user.roleId == 4) {
						$scope.user.username = $scope.newUser.salesCode.toLowerCase();
						$scope.user.employeeid = $scope.newUser.id;
						$scope.user.mobile = $scope.newUser.mobile;
						$scope.user.email = $scope.newUser.email;
					}

					if ($scope.user.roleId == 5) {
						$scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
						$scope.user.employeeid = $scope.newUser.id;
						$scope.user.mobile = $scope.newUser.helpline;
					}

					if ($scope.user.roleId == 6) {
						$scope.user.username = $scope.newUser.fwcode.toLowerCase();
						$scope.user.employeeid = $scope.newUser.id;
						$scope.user.mobile = $scope.newUser.mobile;
						$scope.user.email = $scope.newUser.email;
					}
				}
			});

			$scope.hideState = false;
			$scope.hideDistrict = false;
			$scope.hideCo = false;
			$scope.$watch('user.roleId', function (newValue, oldValue) {
				if (newValue === oldValue || newValue == '' || newValue == undefined) {
					return;
				} else {
					$scope.user.username = null;
					$scope.user.employeeid = null;
					if (newValue == 1) {
						$scope.hideState = true;
						$scope.hideDistrict = true;
						$scope.hideCo = true;
					} else if (newValue == 3) {
						$scope.hideState = false;
						$scope.hideDistrict = true;
						$scope.hideCo = true;
					} else if (newValue == 4) {
						$scope.hideState = false;
						$scope.hideDistrict = true;
						$scope.hideCo = true;
					} else if (newValue == 5) {
						$scope.hideState = false;
						$scope.hideDistrict = false;
						$scope.hideCo = false;
					} else if (newValue == 6) {
						$scope.hideState = false;
						$scope.hideDistrict = false;
						$scope.hideCo = false;
					}
				}
			});


			$scope.cofw = {
				data: 'co'
			};
			$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
			$scope.$watch('cofw.data', function (newValue, oldValue) {
				if (newValue === oldValue) {
					return;
				} else {
					console.log(newValue);
					if (newValue == 'co') {
						$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
					} else if (newValue == 'fw') {
						$scope.partners = Restangular.all('partners?filter[where][groupId]=9').getList().$object;
					}
				}
			});

			$scope.$watch('user.zoneId', function (newValue, oldValue) {
				if (newValue === oldValue | newValue == '') {
					return;
				} else {
					//$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
					$scope.salesAreas = Restangular.all('sales-areas?filter[where][groupId]=5' + '&filter[where][deleteflag]=null').getList().$object;
				}
			});

			$scope.$watch('user.salesAreaId', function (newValue, oldValue) {
				if (newValue === oldValue | newValue == '') {
					return;
				} else {
					//$scope.copartners = Restangular.all('partners?filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=8').getList().$object;
					//$scope.copartners = Restangular.all('employees?filter[where][groupId]=5').getList().$object;
					$scope.copartners = Restangular.all('employees?filter[where][groupId]=5' + '&filter[where][deleteflag]=null').getList().$object;


				}
			});


			//$scope.users = Restangular.all('users').getList().$object;



			//$scope.employees = Restangular.all('employees').getList().$object;
			$scope.groups = Restangular.all('groups').getList().$object;
			$scope.roles = Restangular.all('roles').getList().$object;



			//$scope.salesAreas = Restangular.all('sales-areas').getList().$object;
			$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
			$scope.distributionSubareas = Restangular.all('distribution-subareas').getList().$object;



			$scope.$watch('user.distributionAreaId', function (newValue, oldValue) {
				if (newValue === oldValue) {
					return;
				} else {
					$scope.user.organizationId = newValue;
				}
			});

			$scope.$watch('user.salesAreaId', function (newValue, oldValue) {
				if (newValue === oldValue) {
					return;
				} else {
					$scope.user.organizationId = newValue;
				}
			});


			/**************************************CALLING FUNCTION**********************/
    $scope.countryId = '';
    $scope.stateId = '';
    $scope.statesid = '';
    $scope.countiesid = '';

    $scope.$watch('countryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        $window.sessionStorage.user_zoneId = newValue;
        $scope.displaysalesareas12 = Restangular.all('employees?filter[where][stateId]=' + newValue).getList().then(function (responceSt) {
          $scope.facilities = responceSt;
          $scope.failityId = $window.sessionStorage.user_facilityId;
        });

        var userUrl = '';
        if($window.sessionStorage.isPartner === 'true'){
          userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId+'&filter[where][zoneId]=' + newValue
        }else{
          userUrl = 'users?filter[where][managerId]=0&filter[where][zoneId]=' + newValue
        }
        $scope.cities = Restangular.all(userUrl).getList().then(function (ctyRes) {
          $scope.users = ctyRes;
          angular.forEach($scope.users, function (member, index) {
            member.index = index + 1;
            if (member.deleteflag === 'true') {
              member.backgroundColor = "#ff0000"
            } else {
              member.backgroundColor = "#000000"
            }
          });
        });
        $scope.countiesid = +newValue;
      }
    });

    $scope.$watch('failityId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        $window.sessionStorage.user_facilityId = newValue;
        var userUrl = '';
        if($window.sessionStorage.isPartner === 'true'){
          userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId+'&filter[where][coorgId]=' + newValue + '&filter[where][zoneId]=' + $scope.countiesid
        }else{
          userUrl = 'users?filter[where][managerId]=0&filter[where][coorgId]=' + newValue + '&filter[where][zoneId]=' + $scope.countiesid
        }
        $scope.users = Restangular.all(userUrl).getList().then(function (ctyRes) {
          $scope.users = ctyRes;
          angular.forEach($scope.users, function (member, index) {
            member.index = index + 1;
            if (member.deleteflag === 'true') {
              member.backgroundColor = "#ff0000"
            } else {
              member.backgroundColor = "#000000"
            }
          });
        });
      }
    });

    $scope.updateUsrFlag = function (id) {
      $scope.item = [{
        deleteflag: true
      }]
      Restangular.one('users/' + id).customPUT($scope.item[0]).then(function () {
        $route.reload();
      });
    }

    $scope.unArchive = function (id) {
      $scope.itemunarchive = [{
        deleteflag: false
      }]
      Restangular.one('users/' + id).customPUT($scope.itemunarchive[0]).then(function () {
        $route.reload();
      });
    }

  });
