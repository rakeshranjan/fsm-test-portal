'use strict';

angular.module('secondarySalesApp')
  .controller('FormLevelCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    $scope.disableMenu = true;

    $scope.currentRoleId = $window.sessionStorage.roleId;

    if (window.sessionStorage.currenURLParamId == undefined) {
      window.sessionStorage.LastLocation = $location.absUrl();
      var currentURL = window.sessionStorage.LastLocation.split('/');
      var currenURLParamId = currentURL[5];
      //  $routeParams.id = currenURLParamId;
      window.sessionStorage.currenURLParamId = currenURLParamId;
    } else if (window.sessionStorage.currenURLParamId != null) {
      window.sessionStorage.LastLocation = $location.absUrl();

      if (window.sessionStorage.prviousLocation != 'partials/formlevelsummary') {
        var currentURL = window.sessionStorage.LastLocation.split('/');
        var currenURLParamId = currentURL[5];
        window.sessionStorage.currenURLParamId = currenURLParamId;

        // $routeParams.id = window.sessionStorage.currenURLParamId;
      } else {
        // $routeParams.id = window.sessionStorage.currenURLParamId;
      }
      if (window.sessionStorage.prviousLocation != 'partials/formdetailslist') {
        $scope.submitDisable = true;
      }
    }

    $scope.disableLocation = false;

    if (window.sessionStorage.roleId == 1) {
      $scope.hideMenu = true;
    } else {
      $scope.hideMenu = false;
    }

    $scope.carriedCount = 0;

    if (window.sessionStorage.roleId == 1) {
      $scope.updateDisable = true;
    } else if (window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
      $scope.updateDisable = false;
      window.sessionStorage.previousLocationCarried = window.sessionStorage.prviousLocation;
      if (window.sessionStorage.previousLocationCarried == "partials/serviceorder") {
        $scope.carriedCount++;
        window.sessionStorage.carryCount = $scope.carriedCount;
      }
    } else if (window.sessionStorage.roleId == 5) {
      $scope.updateDisable = false;
    }

    if ($routeParams.id) {
      $scope.HideCreateButton = false;
      $scope.langdisable = true;
      $scope.disableLocation = true;

      Restangular.one('rcdetails', $routeParams.id).get().then(function (langrcdetail) {
        $scope.original = langrcdetail;
        $scope.langrcdetail = Restangular.copy($scope.original);
        // console.log('$scope.langrcdetail', $scope.langrcdetail);

        Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().then(function (organisationlevels) {
          $scope.organisationlevels = organisationlevels;
          $scope.orgstructure = langrcdetail.orgstructure.split(";");

          for (var i = 0; i < $scope.organisationlevels.length; i++) {
            for (var j = 0; j < $scope.orgstructure.length; j++) {
              $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
              if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
                $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1];
              }
            }
          }

          Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
            $scope.organisationlocations = organisationlocations;

            angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
              organisationlevel.index = index;
              organisationlevel.organisationlocations = organisationlocations;
            });

            Restangular.one('users?filter[where][id]=' + $scope.langrcdetail.assignedto).getList().then(function (userresp) {
              $scope.users = userresp;
            });
            Restangular.one('organisationlocations/findOne?filter[where][id]=' + $scope.langrcdetail.countryId).get().then(function (countryresp) {
              $scope.countries = countryresp;
            });
            Restangular.one('organisationlocations?filter[where][id]=' + $scope.langrcdetail.stateId).getList().then(function (stateresp) {
              $scope.states = stateresp;
            });
            Restangular.one('organisationlocations?filter[where][id]=' + $scope.langrcdetail.cityId).getList().then(function (cityresp) {
              $scope.cities = cityresp;
            });
            Restangular.one('organisationlocations/findOne?filter[where][id]=' + $scope.langrcdetail.pincodeId).get().then(function (pincoderesp) {
              $scope.pincodes = pincoderesp;
            });
            Restangular.all('surveyanswers?filter[where][memberid]=' + $scope.langrcdetail.id).getList().then(function (survans) {
              $scope.surveyanswers = survans;
              var lastquest = $scope.surveyanswers.length - 1;
              if (lastquest < 0) {
                $scope.dellremarks = null;
              } else {
                $scope.dellremarks = $scope.surveyanswers[lastquest].remark;
              }
            });

            if (langrcdetail.rcCode != null) {
              Restangular.one('surveyquestions?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.langrcdetail.rcCode).get().then(function (squas) {
                $scope.healthsurveyquestion = squas[0];
              });
            } else {
              Restangular.one('surveyquestions?filter[where][deleteflag]=false&filter[where][rcCode]=' + 9).get().then(function (squas) {
                $scope.healthsurveyquestion = squas[0];
              });
            }
          });
        });
      });
    }

    $scope.cancelClick = function () {
      if (window.sessionStorage.prviousLocation == "partials/serviceorder") {
        $location.path('/serviceorder');
      } else if (window.sessionStorage.prviousLocation == "partials/beneficiaries") {
        $location.path('/tickets');
      } else {
        if (window.sessionStorage.roleId == 1) {
          $location.path('/formleveldetailslist');
        } else {
          if (window.sessionStorage.carryCount > 0) {
            $location.path('/serviceorder');
          } else {
            $location.path('/tickets');
          }
        }
      }
    };

    Restangular.one('rcdetailslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
      $scope.rcdetailLang = langResponse[0];
      if (window.sessionStorage.roleId == 1) {
        $scope.hideMenu = true;
      } else {
        $scope.hideMenu = false;
      }
    });

    $scope.showForm = function () {
      var visible = $location.path() === '/formlevel/create' || $location.path() === '/formlevel/edit/' + $routeParams.id;
      return visible;
    };

    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/formlevel/create';
        return visible;
      }
    };

    Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + ogrlevls[0].id).getList().then(function (ogrlocs) {
        $scope.organisationlevels = ogrlevls;
        angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
          if (organisationlevel.id === ogrlevls[0].id) {
            organisationlevel.organisationlocations = ogrlocs;
          }
        });
      });
    });

    $scope.orgLevelChange = function (index, level) {
      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
        }
      });
    };

    $scope.organisationlevels = [];

    if (window.sessionStorage.roleId == 1) {
      Restangular.all('customers?filter[where][deleteFlag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });
    } else if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4) {
      Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + window.sessionStorage.customerId).getList().then(function (cust) {
        $scope.customers = cust;
      });
    }

    Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (stat) {
      $scope.statuses = stat;
    });

    Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 102 + '&filter[where][parent]=' + 2058).getList().then(function (stateResp) {
      $scope.states = stateResp;

    });

    $scope.$watch('langrcdetail.stateId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        $scope.stateId = '';
        $scope.cityId = '';

        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 105 + '&filter[where][parent]=' + newValue).getList().then(function (cityResp) {
          $scope.cities = cityResp;

        });
      }
    });

    $scope.custId = "";
    $scope.catgId = "";
    $scope.subcatgId = "";

    $scope.$watch('langrcdetail.customerId', function (newValue, oldValue) {
      $scope.custId = newValue;
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('langrcdetail.categoryId', function (newValue, oldValue) {
      $scope.catgId = newValue;
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    $scope.$watch('langrcdetail.subcategoryId', function (newValue, oldValue) {
      $scope.subcatgId = newValue;
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
      }
    });

    $scope.$watch('langrcdetail.cityId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('users?filter[where][managerId]=0&filter[where][isPartner]=true' + '&filter[where][customerId]=' + $scope.custId + '&filter[where][categoryId]=' + $scope.catgId + '&filter[where][subcategoryId]=' + $scope.subcatgId + '&filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (userresp) {
          $scope.users = userresp;
        });
      }
    });

    $scope.myAddressFunction = function () {
      var address = $scope.langrcdetail.serviceaddress1;
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'address': address
      }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var location = results[0].geometry.location;
          $scope.langrcdetail.latitude = location.lat();
          $scope.langrcdetail.longitude = location.lng();
        } else {
          $scope.langrcdetail.latitude = null;
          $scope.langrcdetail.longitude = null;
        }
      });
    }

    $scope.modalTitle = 'Thank You';

    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    $scope.validatestring = '';

    $scope.Save = function () {

      document.getElementById('dispatchno').style.border = "";

      if ($scope.langrcdetail.dispatchno == '' || $scope.langrcdetail.dispatchno == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter dispatchno';
        document.getElementById('dispatchno').style.border = "1px solid #ff0000";
      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {

        $scope.orgstructure = "";
        for (var i = 0; i < $scope.organisationlevels.length; i++) {
          if ($scope.orgstructure === "") {
            $scope.orgstructure = $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          } else {
            $scope.orgstructure = $scope.orgstructure + ";" + $scope.organisationlevels[i].id + "-" + $scope.organisationlevels[i].locationid;
          }
        }

        $scope.langrcdetail.orgstructure = $scope.orgstructure;

        $scope.langrcdetail.statusId = 1;
        $scope.langrcdetail.nextquestionId = 1;
        $scope.langrcdetail.rollbackStatus = 'N';
        $scope.langrcdetail.deleteflag = false;

        Restangular.all('rcdetails').post($scope.langrcdetail).then(function (conResponse) {
          $scope.message = ' Ticket has been created!';
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

          if ($scope.currentRoleId == 2) {
            setTimeout(function () {
              window.location = '/tickets';
            }, 350);
          } else {
            if (window.sessionStorage.prviousLocation == "partials/serviceorder") {
              setTimeout(function () {
                window.location = '/serviceorder';
              }, 350);
            } else if (window.sessionStorage.prviousLocation == "partials/beneficiaries") {
              setTimeout(function () {
                window.location = '/tickets';
              }, 350);
            } else {
              setTimeout(function () {
                window.location = '/formleveldetailslist';
              }, 350);
            }
          }

          // setTimeout(function () {
          //     window.location = '/formleveldetailslist';
          // }, 350);
        });
      };

    };

    $scope.submitDisable = false;

    $scope.Update = function () {

      document.getElementById('dispatchno').style.border = "";

      if ($scope.langrcdetail.dispatchno == '' || $scope.langrcdetail.dispatchno == null) {
        $scope.validatestring = $scope.validatestring + 'Please  Enter dispatchno';
        document.getElementById('dispatchno').style.border = "1px solid #ff0000";

      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
        $scope.langrcdetail.deleteflag = false;
        
        Restangular.one('rcdetails', $routeParams.id).customPUT($scope.langrcdetail).then(function (conResponse) {
          $scope.message = 'Form Level has been updated!';
          $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

          $scope.disableUpdate = true;

          if ($scope.currentRoleId == 2) {
            setTimeout(function () {
              window.location = '/tickets';
            }, 350);
          } else {

            if (window.sessionStorage.prviousLocation == "partials/serviceorder") {
              setTimeout(function () {
                window.location = '/serviceorder';
              }, 350);
            } else if (window.sessionStorage.prviousLocation == "partials/beneficiaries") {
              setTimeout(function () {
                window.location = '/tickets';
              }, 350);
            } else {
              setTimeout(function () {
                window.location = '/formleveldetailslist';
              }, 350);
            }
          }
        });
      }
    };

    $scope.LocateMe = function () {
      $scope.mapdataModal = true;

      var map = new google.maps.Map(document.getElementById('mapCanvas'), {
        zoom: 4,

        center: new google.maps.LatLng(12.9538477, 77.3507369),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      });
      var marker, i;

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(12.9538477, 77.3507369),
        map: map,
        html: ''
      });

      $scope.toggleMapModal();
    };

    $scope.toggleMapModal = function () {
      $scope.mapcount = 0;

      ///////////////////////////////////////////////////////MAP//////////////////////////

      var geocoder = new google.maps.Geocoder();

      function geocodePosition(pos) {
        geocoder.geocode({
          latLng: pos
        }, function (responses) {
          if (responses && responses.length > 0) {
            updateMarkerAddress(responses[0].formatted_address);
          } else {
            updateMarkerAddress('Cannot determine address at this location.');
          }
        });
      }

      function updateMarkerStatus(str) {
        document.getElementById('markerStatus').innerHTML = str;
      }

      function updateMarkerPosition(latLng) {
        $scope.langrcdetail.latitude = latLng.lat();
        $scope.langrcdetail.longitude = latLng.lng();

        document.getElementById('info').innerHTML = [
          latLng.lat(),
          latLng.lng()
        ].join(', ');
      }

      function updateMarkerAddress(str) {
        document.getElementById('mapaddress').innerHTML = str;
      }
      var map;

      function initialize() {

        $scope.latitude = 12.9538477;
        $scope.longitude = 77.3507369;
        navigator.geolocation.getCurrentPosition(function (location) {
          $scope.latitude = location.coords.latitude;
          $scope.longitude = location.coords.longitude;

          var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
          map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 4,
            center: new google.maps.LatLng($scope.latitude, $scope.longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
          });
          var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: true
          });

          // Update current position info.
          updateMarkerPosition(latLng);
          geocodePosition(latLng);

          // Add dragging event listeners.
          google.maps.event.addListener(marker, 'dragstart', function () {
            updateMarkerAddress('Dragging...');
          });

          google.maps.event.addListener(marker, 'drag', function () {
            updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
          });

          google.maps.event.addListener(marker, 'dragend', function () {
            updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());
          });
        });


      }

      // Onload handler to fire off the app.
      //google.maps.event.addDomListener(window, 'load', initialize);
      initialize();

      window.setTimeout(function () {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
        map.setZoom(10);
      }, 1000);


      $scope.SaveMap = function () {
        $scope.showMapModal = !$scope.showMapModal;
      };

      $scope.showMapModal = !$scope.showMapModal;
    };
  });
