'use strict';

angular.module('secondarySalesApp')
    .controller('WorkflowCreateCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';
        $scope.languageId = $window.sessionStorage.language;

        $scope.showForm = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflows/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflow-form" || $window.sessionStorage.prviousLocation != "partials/workflow-form") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        $scope.workflowlanguage = Restangular.one('workflowlanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;

        $scope.workflow = {
            workflowid: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };

        /************************************* INDEX ***************************************************/

        Restangular.all('workflowheaders?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%'+$window.sessionStorage.orgStructure+'%').getList().then(function (wflow) {
            $scope.workflowheaders = wflow;
            angular.forEach($scope.workflowheaders, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });
        $scope.orgStructureSplit = $window.sessionStorage.orgStructure.split(";");
        $scope.intermediateOrgStructure = "";
        for(var i=0;i<$scope.orgStructureSplit.length-1;i++){
            if ($scope.intermediateOrgStructure === "") {
            $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
            }
            else{
                $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" +$scope.orgStructureSplit[i];
            }
        }

        if($scope.orgStructureSplit.length>0){
            $scope.orgStructureFilterArray = [];
            $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length-1].split("-")[1].split(",");
            for(var i=0;i<$scope.LocationsSplit.length;i++){
                $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure+";"+$scope.orgStructureSplit[$scope.orgStructureSplit.length-1].split("-")[0]+"-"+$scope.LocationsSplit[i]);
            }

        }
        $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[","").replace("]","");
        console.log("$scope.orgStructureFilter",JSON.stringify($scope.orgStructureFilterArray).replace("[","").replace("]",""));

        Restangular.all('members?filter={"where":{"and":[{"orgstructure":{"inq":['+$scope.orgStructureFilter+']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (member) {
            $scope.members = member;
        });

        $scope.workflows = Restangular.all('workflows?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language).getList().$object;

        $scope.users = Restangular.all('users?filter[where][deleteflag]=false&filter[where][orgStructure][like]=%'+$window.sessionStorage.orgStructure+'%').getList().$object;

        $scope.$watch('workflow.workflowid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {

                Restangular.all('workflowstatuses?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language + '&filter[where][workflowid]=' + newValue).getList().then(function (wfl) {
                    $scope.workflowstatuses = wfl;
                    $scope.workflow.workflowstatusid = $scope.workflow.workflowstatusid;
                });

                Restangular.all('workflowcategories?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language + '&filter[where][workflowid]=' + newValue).getList().then(function (wflc) {
                    $scope.workflowcategories = wflc;
                    $scope.workflow.workflowcategoryid = $scope.workflow.workflowcategoryid;
                });

                Restangular.all('workflowpriorities?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language + '&filter[where][workflowid]=' + newValue).getList().then(function (wflp) {
                    $scope.workflowpriorities = wflp;
                    $scope.workflow.workflowpriorityid = $scope.workflow.workflowpriorityid;
                });
            }
        });

        $scope.$watch('workflow.workflowstatusid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {

                Restangular.one('workflowstatuses', newValue).get().then(function (status) {
                    var dueDate = parseInt(status.due);
                    var fifteendays = new Date();
                    fifteendays.setDate(fifteendays.getDate() + dueDate);
                    $scope.workflow.followupdate = fifteendays;
                });
            }
        });

        $scope.$watch('workflow.memberid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {

                Restangular.one('members', newValue).get().then(function (member) {
                   $scope.workflow.orgstructure = member.orgstructure;
                });
            }
        });

        $scope.validatestring = "";

        $scope.Save = function (clicked) {

            if ($scope.workflow.memberid == '' || $scope.workflow.memberid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.workflow.assignedto == '' || $scope.workflow.assignedto == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            } else if ($scope.workflow.workflowid == '' || $scope.workflow.workflowid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow';

            } else if ($scope.workflow.workflowstatusid == '' || $scope.workflow.workflowstatusid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow Status';

            } else if ($scope.workflow.workflowcategoryid == '' || $scope.workflow.workflowcategoryid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow Category';

            } else if ($scope.workflow.workflowpriorityid == '' || $scope.workflow.workflowpriorityid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Priority';

            } else if ($scope.workflow.followupdate == '' || $scope.workflow.followupdate == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup Date';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.all('workflowheaders').post($scope.workflow).then(function (resp) {
                    window.location = '/workflows';
                });
            }
        };

        $scope.Update = function (clicked) {

            if ($scope.workflow.memberid == '' || $scope.workflow.memberid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Member';

            } else if ($scope.workflow.assignedto == '' || $scope.workflow.assignedto == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            } else if ($scope.workflow.workflowid == '' || $scope.workflow.workflowid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow';

            } else if ($scope.workflow.workflowstatusid == '' || $scope.workflow.workflowstatusid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow Status';

            } else if ($scope.workflow.workflowcategoryid == '' || $scope.workflow.workflowcategoryid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow Category';

            } else if ($scope.workflow.workflowpriorityid == '' || $scope.workflow.workflowpriorityid == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Workflow Priority';

            } else if ($scope.workflow.followupdate == '' || $scope.workflow.followupdate == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Followup Date';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflowheaders', $routeParams.id).customPUT($scope.workflow).then(function (resp) {
                    window.location = '/workflows';
                });
            }
        };

        if ($routeParams.id) {

            $scope.message = 'Workflow has been updated!';

            Restangular.one('workflowheaders', $routeParams.id).get().then(function (wflow) {
                $scope.original = wflow;
                $scope.workflow = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Workflow has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflowheaders/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getmember = function (memberid) {
            return Restangular.one('members', memberid).get().$object;
        };

        $scope.getworkflow = function (workflowid) {
            return Restangular.one('workflows', workflowid).get().$object;
        };

        $scope.getcategory = function (workflowcategoryid) {
            return Restangular.one('workflowcategories', workflowcategoryid).get().$object;
        };

        $scope.getstatus = function (workflowstatusid) {
            return Restangular.one('workflowstatuses', workflowstatusid).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });
