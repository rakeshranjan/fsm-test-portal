'use strict';

angular.module('secondarySalesApp')
  .controller('MultiLanguageListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    console.log("MultiLanguageList");
    // $scope.rcdetailslanguages = Restangular.all('rcdetailslanguages?filter[where][deleteflag]=false').getList().$object;
    Restangular.all('rcdetailslanguages?filter[where][deleteflag]=false').getList().then(function (rclangdetailresp) {
      $scope.rcdetailslanguages = rclangdetailresp;
      console.log($scope.rcdetailslanguages, "RC-LANG-RESP-10")

      Restangular.all('languagedefinitions?filter[where][deleteFlag]=false').getList().then(function (reslang) {
        $scope.languageDisplays = reslang;
        console.log($scope.languageDisplays, "LANG-DISP-14")

        angular.forEach($scope.rcdetailslanguages, function (member, index) {
          member.index = index + 1;

          for (var a = 0; a < $scope.languageDisplays.length; a++) {
            if (member.languageId == $scope.languageDisplays[a].id) {
              member.languageName = $scope.languageDisplays[a].name;
              break;
            }
          }
          //$scope.TotalData = member;
          // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
        });
      });

    });


  });
