'use strict';
angular.module('secondarySalesApp').controller('OneToOneCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal, $filter, $fileUploader, $http) {


  // var slider = document.getElementById("myRange");
  // var output = document.getElementById("demo");
  // output.innerHTML = slider.value;

  // slider.oninput = function () {
  //     output.innerHTML = this.value;
  // }
  /*********/
  // console.log($routeParams.id, "member-id-13")
  /////// ROLLBACK FUNCTION ////////////////////////////////////
  $scope.hideETA = true;
  $scope.hideSumETA = true;
  $scope.hideEngineer = true;
  $scope.hideCallclosed = true;
  $scope.hideAssigntkt = true;

  $scope.selectedFile = null;

  $scope.updatetypes = Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().$object;

  $scope.serviceevents = Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().$object;

  // Restangular.all('users?filter[where][isPartner]=true').getList().then(function (asps) {
  //   $scope.asps = asps;
  // });


  $scope.fileSelected = function (element) {
    $scope.hideuploder = false;
    var myFileSelected = element.files[0];
    $scope.selectedFile = myFileSelected;
    //console.log('myFileSelected', myFileSelected);
  };

  $scope.rollbackAns = function () {
    $scope.disablerollback = true;
    $scope.submitDisable = true;
    //console.log("Rollback Function: ");
    // Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id).getList().then(function (sansw) {
    Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + $routeParams.id).getList().then(function (sansw) {
      $scope.RollbackAnswers = sansw;
      console.log("Last Answered QuestionID", $scope.RollbackAnswers[sansw.length - 1].questionid);
      $scope.tktansw = {
        id: $scope.RollbackAnswers[sansw.length - 1].id,
        rollbackRemark: 'Requested',
        rollbackRequestTime: new Date(),
        rollbackRoleId: $window.sessionStorage.roleId,
        loginUserId: $window.sessionStorage.userId,
        rollbackstatusId: 1
      };
      Restangular.one('surveyanswers', $scope.RollbackAnswers[sansw.length - 1].id).customPUT($scope.tktansw).then(function (resAns) {


      });

      $scope.member = {
        id: $routeParams.id,
        rollbackFlag: true,
        rollbackStatus: 'R',
        lastloginUserId: $window.sessionStorage.userId
      };
      Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function (resp) {

        window.location = '/';
      });
    });
  };
  ////// Rollback History /////////////////////////////

  $scope.openrollbackHistory = function () {

    //$scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
    $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][rollbackRemark]=Requested&filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
      $scope.HealthAnswers = HRes;
      //console.log($scope.HealthAnswers, "ANS-35");
      angular.forEach($scope.HealthAnswers, function (member, index) {
        member.index = index + 1;
        if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
          member.uploadedfilesList = [];
        } else {
          member.uploadedfilesList = member.uploadedfiles.split(',');
          // console.log(member.uploadedfilesList, "member.uploadedfilesList-71")
        }
      });
    });

    $scope.modalPrevAns = $modal.open({
      animation: true,
      templateUrl: 'template/Rollbackanswers.html',
      scope: $scope,
      backdrop: 'static',
      size: 'lg'

    });
  };

  $scope.setNewETATime = function () {
    //console.log('HERE-83');
    console.log($scope.langrcdetail.etadate, "MSG-84")
    // $scope.langrcdetail = {
    //   etadate: defaultETdate
    // };
    // //$scope.langrcdetail.etadate = defaultETdate;
    // console.log($scope.langrcdetail.etadate, "MSG-1398")
  };

  $scope.setNewsumETATime = function () {
    //console.log('HERE-93');
    console.log($scope.langrcdetail.etadate, "MSG-94")
    // $scope.langrcdetail = {
    //   etadate: defaultETdate
    // };
    // //$scope.langrcdetail.etadate = defaultETdate;
    // console.log($scope.langrcdetail.etadate, "MSG-1398")
  };

  ////////////////////////////////////PREV ANSWERS////////////////////////////

  $scope.openPrevAns = function () {
    if ($window.sessionStorage.language == 1) {
      $scope.printNotapplicable = 'Not Applicable';
    } else if ($window.sessionStorage.language == 2) {
      $scope.printNotapplicable = 'लागू नहीं';
    } else if ($window.sessionStorage.language == 3) {
      $scope.printNotapplicable = 'ಅನ್ವಯಿಸುವುದಿಲ್ಲ';
    } else if ($window.sessionStorage.language == 4) {
      $scope.printNotapplicable = 'பொருந்தாது';
    } else if ($window.sessionStorage.language == 5) {
      $scope.printNotapplicable = 'వర్తించదు';
    } else if ($window.sessionStorage.language == 6) {
      $scope.printNotapplicable = 'लागू नाही';
    }

    $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
      // $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
      $scope.HealthAnswers = HRes;
      //console.log($scope.HealthAnswers, "ANS-35");
      angular.forEach($scope.HealthAnswers, function (member, index) {
        member.index = index + 1;
        if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
          member.uploadedfilesList = [];
        } else {
          member.uploadedfilesList = member.uploadedfiles.split(',');
          console.log(member.uploadedfilesList, "member.uploadedfilesList-133")
        }
      });
    });

    $scope.modalPrevAns = $modal.open({
      animation: true,
      templateUrl: 'template/Previousanswers.html',
      scope: $scope,
      backdrop: 'static',
      size: 'lg'

    });
  };

  $scope.okPrevAns = function () {
    $scope.modalPrevAns.close();
  };

  var date = new Date();
  $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  //console.log($scope.FromDate, "CURRENT DATE - 36")
  var startdate = $scope.FromDate + "T00:00:00";
  $scope.mindate = startdate;

  $scope.openSummaryAns = function () {

    Restangular.one('rcdetails', $routeParams.id).get().then(function (langrcdetail) {

      Restangular.one('userviews?filter[where][managerId][gt]=0&filter[where][orgStructure][like]=%' + langrcdetail.pincodeId + '%').get().then(function (fes) {
        $scope.users = fes;
      });
      $scope.original = langrcdetail;
      $scope.langrcdetail = Restangular.copy($scope.original);
      //   console.log('$scope.todo', $scope.langrcdetail);

      // if($scope.langrcdetail.rollbackStatus == 'R') {
      //     $scope.submitDisable = true;
      //     $scope.disablerollback = true;
      // } else {
      //     $scope.submitDisable = false;
      //     $scope.disablerollback = false;
      // }

    });

    $scope.validatestring = "";
    $scope.todoToCreateArray = [];

    //$scope.$parent.etadate = new Date();

    // $scope.openstartdate = function ($event, index) {
    //     $event.preventDefault();
    //     $event.stopPropagation();
    //     $timeout(function () {
    //       $('#datepickerstartdate' + index).focus();
    //     });
    //     $scope.picker.startdateopened = true;
    //   };

    $scope.SubmitAnswer = function () {
      //console.log("SubmitAnswer");
      $scope.message = 'Answer has been saved!';
      $scope.modalTitle = 'Thank you!!!';

      //console.log($scope.$parent.etadate, "SELECTED ETA DATE")

      $scope.answer = "";
      if ($scope.healthsurveyquestion.questiontype == 2) {
        angular.forEach($scope.myOptionsArray, function (data) {
          if (data.answer == true) {
            if ($scope.answer == "") {
              $scope.answer = data.value;
            } else {
              $scope.answer = $scope.answer + data.value;
            }
          }
        });
        $scope.onetoonesum.answer = $scope.answer;
      }

      // if ($scope.onetoonesum.answer == '' || $scope.onetoonesum.answer == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Select Answer';
      // } else if ($scope.selectedRCcodeToPush[0] == 11 || $scope.selectedRCcodeToPush[0] == 16) {
      //   if ($scope.etadate == '' || $scope.etadate == null) {
      //     $scope.validatestring = $scope.validatestring + 'Please Select ETA Time';
      //   }
      // }
      document.getElementById('datepickerSumETAdate').style.border = "";
      document.getElementById('city').style.border = "";
      document.getElementById('user').style.border = "";
      document.getElementById('datepickerstartdate').style.border = "";
      document.getElementById('datepickerenddate').style.border = "";
      if ($scope.onetoonesum.answer == '' || $scope.onetoonesum.answer == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Answer';
      } else if ($scope.selectedRCcodeToPush[0] == 11 || $scope.selectedRCcodeToPush[0] == 16) {
        //console.log("Here-217", $scope.etadate)
        if ($scope.langrcdetail.etadate != undefined) {
          //console.log($scope.langrcdetail.etadate, "Here-1021")
        }
        if ($scope.langrcdetail.etadate == '' || $scope.langrcdetail.etadate == null) {
          //$scope.enddate = null;
          $scope.langrcdetail.etadate = null;
          $scope.validatestring = $scope.validatestring + 'Please Select ETA Time';
          document.getElementById('datepickerSumETAdate').style.border = "1px solid #ff0000";
        }
      } else if ($scope.selectedRCcodeToPush[0] == 10) {

        if ($scope.langrcdetail.city == '' || $scope.langrcdetail.city == null || $scope.langrcdetail.city == undefined) {
          $scope.langrcdetail.city = null;
          $scope.validatestring = $scope.validatestring + 'Please Select City';
          document.getElementById('city').style.border = "1px solid #ff0000";
        } else if ($scope.langrcdetail.user == '' || $scope.langrcdetail.user == null || $scope.langrcdetail.user == undefined) {
          $scope.langrcdetail.user = null;
          $scope.validatestring = $scope.validatestring + 'Please Select User';
          document.getElementById('user').style.border = "1px solid #ff0000";
        } else if ($scope.langrcdetail.startdate == '' || $scope.langrcdetail.startdate == null) {
          //toaster.pop('error', 'please select Start Date');
          $scope.langrcdetail.startdate = null;
          $scope.validatestring = $scope.validatestring + 'Please Select Start Date';
          document.getElementById('datepickerstartdate').style.border = "1px solid #ff0000";
        } else if ($scope.langrcdetail.enddate == '' || $scope.langrcdetail.enddate == null) {
          //toaster.pop('error', 'please select End Date');
          $scope.langrcdetail.enddate = null;
          $scope.validatestring = $scope.validatestring + 'Please Select End Date';
          document.getElementById('datepickerenddate').style.border = "1px solid #ff0000";
          //console.log($scope.$parent.startdate, "START-TIME")
          //   console.log($scope.startdate, "--", starttime, $scope.$parent.startdate, "START-TIME")
        }
      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {
        // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        $scope.onetoonesum.questionid = $scope.healthsurveyquestion.id;
        //added by me
        $scope.onetoonesum.deleteflag = false;

        // console.log($scope.onetoone.answer, "THE ANSWER-434")
        // console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE, selectedRCcodeToPush[0]-435")
        // console.log($scope.healthsurveyquestion.serialno, "serialno-436")
        // console.log($scope.healthsurveyquestion.questiontype, "questiontype-437")
        // console.log($scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0], "Outside-Pushed-Selected-serialno-341")
        // if ($scope.selectedSerialNoToPush[0] == undefined) {
        //   $scope.selectedSerialNoToPush.push(0);
        //   console.log($scope.selectedSerialNoToPush[0], "VALUE-NOW-440")
        // }
        if ($scope.selectedSerialNoToPush[0] != undefined) {
          Restangular.all('surveyanswers').post($scope.onetoonesum).then(function (resp) {

            if ($scope.healthsurveyquestion.skipquestion === true && $scope.onetoonesum.answer.toString().split(",").indexOf($scope.healthsurveyquestion.skipquestionvalue) > -1) {
              // Restangular.one('surveyquestions', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
              // console.log("IF-448")
              // Restangular.one('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId, $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
              Restangular.one('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]}}', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
                //Restangular.one('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
                for (var i = 0; i < $scope.todotypes.length; i++) {
                  if ($scope.todotypes[i].toInitial === true) {
                    $scope.member = {
                      id: $routeParams.id,
                      answeredQuestion: 0,
                      assignedto: 0,
                      etatime: $scope.langrcdetail.etadate,
                      lastmodifiedby: $window.sessionStorage.userId,
                      lastmodifiedtime: new Date(),
                      lastmodifiedrole: $window.sessionStorage.roleId
                    };
                    break;
                  } else {
                    $scope.member = {
                      id: $routeParams.id,
                      answeredQuestion: +skpqustn.serialno - 1,
                      assignedto: 0,
                      rcCode: skpqustn.rcCode,
                      etatime: $scope.langrcdetail.etadate,
                      lastmodifiedby: $window.sessionStorage.userId,
                      lastmodifiedtime: new Date(),
                      lastmodifiedrole: $window.sessionStorage.roleId
                    };
                  }
                }
                if ($scope.todotypes.length == 0) {
                  if ($scope.selectedRCcodeToPush[0] == 10) {
                    $scope.member = {
                      id: $routeParams.id,
                      answeredQuestion: $scope.healthsurveyquestion.serialno,
                      //rcCode: $scope.healthsurveyquestion.rcCode,
                      rcCode: $scope.selectedRCcodeToPush[0],
                      nextquestionId: $scope.selectedSerialNoToPush[0],
                      prevquestionId: $scope.previousSerialNoToPush[0],
                      assignedto: $scope.langrcdetail.user,
                      starttime: $scope.langrcdetail.startdate,
                      endtime: $scope.langrcdetail.enddate,
                      assignFlag: true,
                      statusId: 3,
                      //fieldassignFlag: true,
                      //etatime: $scope.langrcdetail.etadate,
                      lastmodifiedby: $window.sessionStorage.userId,
                      lastmodifiedtime: new Date(),
                      lastmodifiedrole: $window.sessionStorage.roleId
                      //rcCode: $scope.healthsurveyquestion.rcCode
                    };
                  } else {
                    $scope.member = {
                      id: $routeParams.id,
                      answeredQuestion: +skpqustn.serialno - 1,
                      // statusId: 7//here statusId: 2 not working
                      statusId: 2,
                      fieldassignFlag: true,
                      rcCode: skpqustn.rcCode,
                      etatime: $scope.langrcdetail.etadate,
                      lastmodifiedby: $window.sessionStorage.userId,
                      lastmodifiedtime: new Date(),
                      lastmodifiedrole: $window.sessionStorage.roleId
                    };
                  }
                }
                // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                //console.log("HERE at 478")
                if ($scope.healthsurveyquestion.storeInCustomField === true) {
                  $scope.member[$scope.healthsurveyquestion.customFieldName] = resp.answer;
                }
                Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {

                  ////////////////////////////////TODO///////////////////////////////////////
                  if ($scope.healthsurveyquestion.applytomessage == 'yes') {
                    for (var i = 0; i < $scope.todotypes.length; i++) {
                      var dueDate = parseInt($scope.todotypes[i].due);
                      var fifteendays = new Date();
                      fifteendays.setDate(fifteendays.getDate() + dueDate);
                      $scope.todoToCreate = {
                        "memberid": $routeParams.id,
                        "todotypeid": $scope.todotypes[i].id,
                        "todostatusid": 1,
                        "followupdate": new Date(),
                        "assignedto": 0,
                        "deleteflag": false,
                        "lastmodifiedby": $window.sessionStorage.userId,
                        "lastmodifiedrole": $window.sessionStorage.roleId,
                        "lastmodifiedtime": new Date(),
                        "createdby": $window.sessionStorage.userId,
                        "createdtime": new Date(),
                        "createdrole": $window.sessionStorage.roleId,
                        "orgstructure": $scope.memberOrgStructure,
                        "remarks": $scope.todotypes[i].remarks
                      }
                      // Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                      // $route.reload();
                      // });
                      $scope.todoToCreateArray.push($scope.todoToCreate);

                    }
                    $scope.saveTodos();
                  } else {
                    if ($scope.healthsurveyquestion.questiontype == 9) {
                      $scope.uploader.uploadAll();
                    } else {
                      $route.reload();
                    }
                  }


                  //////////////////////////////////TODO///////////////////////

                });
              });
            } else {
              //console.log("ELSE-524")
              for (var i = 0; i < $scope.todotypes.length; i++) {
                if ($scope.todotypes[i].toInitial === true) {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: 0,
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                  };
                  break;
                } else {
                  //if($scope.healthsurveyquestion.questiontype == 11){- $scope.selectedRCcodeToPush[0]
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: $scope.healthsurveyquestion.serialno,
                    // rcCode: $scope.healthsurveyquestion.rcCode,
                    rcCode: $scope.selectedRCcodeToPush[0],
                    nextquestionId: $scope.selectedSerialNoToPush[0],
                    prevquestionId: $scope.previousSerialNoToPush[0],
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                    //rcCode: $scope.healthsurveyquestion.rcCode//$scope.selectedSerialNoToPush[0]
                  };
                }
              }
              if ($scope.todotypes.length == 0) {
                if ($scope.selectedRCcodeToPush[0] == 10) {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: $scope.healthsurveyquestion.serialno,
                    //rcCode: $scope.healthsurveyquestion.rcCode,
                    rcCode: $scope.selectedRCcodeToPush[0],
                    nextquestionId: $scope.selectedSerialNoToPush[0],
                    prevquestionId: $scope.previousSerialNoToPush[0],
                    assignedto: $scope.langrcdetail.user,
                    starttime: $scope.langrcdetail.startdate,
                    endtime: $scope.langrcdetail.enddate,
                    assignFlag: true,
                    statusId: 3,
                    //fieldassignFlag: true,
                    //etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                    //rcCode: $scope.healthsurveyquestion.rcCode
                  };
                } else {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: $scope.healthsurveyquestion.serialno,
                    //rcCode: $scope.healthsurveyquestion.rcCode,
                    rcCode: $scope.selectedRCcodeToPush[0],
                    nextquestionId: $scope.selectedSerialNoToPush[0],
                    prevquestionId: $scope.previousSerialNoToPush[0],
                    //statusId: 5,
                    statusId: 2,
                    fieldassignFlag: true,
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                    //rcCode: $scope.healthsurveyquestion.rcCode
                  };
                }
              }
              // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {langrcdetail
              //console.log("HERE at 555")
              if ($scope.selectedSerialNoToPush[0] == 0) {
                $scope.member = {
                  id: $routeParams.id,
                  // statusId: 5,
                  rcCode: $scope.selectedRCcodeToPush[0],
                  statusId: 15,
                  nextquestionId: 0,
                  etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                };
              }
              //console.log($scope.member.statusId, "STATUS-ID-563")
              if ($scope.healthsurveyquestion.storeInCustomField === true) {
                $scope.member[$scope.healthsurveyquestion.customFieldName] = resp.answer;
              }
              Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                ////////////////////////////////TODO///////////////////////////////////////
                $scope.myansoptionArray = [];
                $scope.mynewansoptionArray = [];
                $scope.selectedRCcodeToPush = [];
                $scope.selectedSerialNoToPush = [];
                //console.log($scope.selectedSerialNoToPush[0], "VALUE-NOW-567")

                if ($scope.healthsurveyquestion.applytomessage == 'yes') {

                  for (var i = 0; i < $scope.todotypes.length; i++) {
                    var dueDate = parseInt($scope.todotypes[i].due);
                    var fifteendays = new Date();
                    fifteendays.setDate(fifteendays.getDate() + dueDate);
                    $scope.todoToCreate = {
                      "memberid": $routeParams.id,
                      "todotypeid": $scope.todotypes[i].id,
                      "todostatusid": 1,
                      "followupdate": new Date(),
                      "assignedto": 0,
                      "deleteflag": false,
                      "lastmodifiedby": $window.sessionStorage.userId,
                      "lastmodifiedrole": $window.sessionStorage.roleId,
                      "lastmodifiedtime": new Date(),
                      "createdby": $window.sessionStorage.userId,
                      "createdtime": new Date(),
                      "createdrole": $window.sessionStorage.roleId,
                      "orgstructure": $scope.memberOrgStructure,
                      "remarks": $scope.todotypes[i].remarks
                    }
                    // Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                    // $route.reload();
                    // });
                    $scope.todoToCreateArray.push($scope.todoToCreate);
                  }
                  $scope.saveTodos();
                } else {
                  if ($scope.healthsurveyquestion.questiontype == 9) {
                    $scope.uploader.uploadAll();
                  } else {
                    $route.reload();
                  }
                }
                //////////////////////////////////TODO///////////////////////
              });
            }
          });
        }
      }
      //console.log($routeParams.id, "285")
      $scope.okSummaryAns();
      //window.location = '/onetoone/' + $routeParams.id;
      //$location.path("/onetoone/" + $routeParams.id);
    };

    $scope.saveTodoCount = 0;
    $scope.saveTodos = function () {
      Restangular.all('todos').post($scope.todoToCreateArray[$scope.saveTodoCount]).then(function (todoresp) {
        $scope.saveTodoCount++;
        if ($scope.saveTodoCount < $scope.todoToCreateArray.length) {
          $scope.saveTodos();
        } else {
          if ($scope.healthsurveyquestion.questiontype == 9) {
            $scope.uploader.uploadAll();
          } else {
            $route.reload();
          }
        }
      });
    }

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    /////////////////////////////////TODO DROPDOWN////////////////////////////
    $scope.todoToPush = [];
    $scope.todotypes = [];
    $scope.selectedRCcodeToPush = [];
    $scope.selectedSerialNoToPush = [];
    $scope.previousSerialNoToPush = [];
    /////////////////////////////////TODO DROPDOWN////////////////////////////
    $scope.$watch('onetoonesum.answer', function (newValue, oldValue) {
      console.log("newValue", newValue);
      console.log("oldValue", oldValue);
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        Restangular.one('rcdetails', $routeParams.id).get().then(function (rcdetailresp) {
          $scope.langrcdetail.serno = rcdetailresp.serno;

          //$scope.original = langrcdetail;
          // $scope.langrcdetail = Restangular.copy($scope.original);
          //console.log(rcdetailresp, "MSG-585")
        });
        //console.log(newValue.toString(), newValue, "NewVALUE-696");
        console.log($scope.healthsurveyquestion, "$scope.healthsurveyquestion.questiontype-697")
        var SelectRCcode = newValue.toString().split("-");
        if (SelectRCcode[1] != 10) {
          $scope.hideAssigntkt = true;
          $scope.cities = null;
          $scope.users = null;
          $scope.langrcdetail = {
            starttime: null,
            endtime: null
          };
        }
        if (SelectRCcode[1] == 11 || SelectRCcode[1] == 16) {
          $scope.hideSumETA = false;
          // var date = new Date();
          // $scope.ETDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
          // var defaultETdate = $scope.ETDate + "T00:00:00";
          var date = new Date();
          $scope.ETDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
          // var defaultETdate = $scope.ETDate + "T00:00:00";
          //var defaultETdate = $scope.ETDate + "T07:20:35";
          var hr = ('0' + date.getHours()).slice(-2);
          var mn = ('0' + date.getMinutes()).slice(-2);
          //var sd = ('0' + date.getSeconds()).slice(-2);
          // var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn + ":" + sd;
          var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn;
          //console.log(defaultETdate, "MSG-510")
          //console.log(date.getHours(), date.getMinutes(), date.getSeconds(), "MSG-1460", hr, mn, sd)
          $scope.langrcdetail = {
            etadate: defaultETdate
          };
          //$scope.langrcdetail.etadate = defaultETdate;
          console.log($scope.langrcdetail.etadate, "MSG-516")
        } else if (SelectRCcode[1] == 10) {
          $scope.hideAssigntkt = false;
          var orgstr = $window.sessionStorage.orgStructure;
          var strorgval = orgstr.split(";");
          var cityval = strorgval[2].split("-");
          // console.log(strorgval, cityval, "strorgval-612")
          Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + cityval[1] + ']}}}').getList().then(function (cityresp) {
            $scope.cities = cityresp;
          });

          Restangular.one('rcdetailtodoview?filter[where][id]=' + $routeParams.id).get().then(function (orglevelresp) {
            //console.log(newValue, "newValue")
            $scope.orgStructure = orglevelresp[0];
            //console.log($scope.orgStructure, "$scope.orgStructure")
            $scope.orgStructure = orglevelresp[0].orgStructure;
            //console.log($scope.orgStructure, "ORG-STRUC")
            var OrgStructTmp = $scope.orgStructure;
            var reducedOrgStruct = OrgStructTmp.substring(0, 27);
            // var actualorgStructure = $scope.orgStructure.split(';')[1].split('-');//Previously
            var actualorgStructure = $scope.orgStructure.split(';')[1].split('-');
            actualorgStructure = actualorgStructure[1];
            //console.log(actualorgStructure, "ACTUAL")
            Restangular.all('users?filter[where][managerId]=0&filter[where][isPartner]=true&filter[where][orgStructure][like]=%' + reducedOrgStruct + '%').getList().then(function (userresp) {
              // Restangular.all('users?filter[where][orgStructure][like]=%' + $scope.orgStructure + '%').getList().then(function (userresp) {
              // Restangular.all('users?filter[where][orgStructure][like]=%' + actualorgStructure + '%').getList().then(function (userresp) {//old
              //console.log(userresp, "userresp")
              $scope.asps = userresp;
            });
          });
          // $scope.langrcdetail = {//Last removed
          //   etadate: null
          // };

          // var date = new Date();
          // $scope.ETDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
          // // var defaultETdate = $scope.ETDate + "T00:00:00";
          // //var defaultETdate = $scope.ETDate + "T07:20:35";
          // var hr = ('0' + date.getHours()).slice(-2);
          // var mn = ('0' + date.getMinutes()).slice(-2);
          // //var sd = ('0' + date.getSeconds()).slice(-2);
          // // var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn + ":" + sd;
          // var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn;
          // console.log(defaultETdate, "MSG-510")
          // //console.log(date.getHours(), date.getMinutes(), date.getSeconds(), "MSG-1460", hr, mn, sd)
          // $scope.langrcdetail = {
          //   starttime: defaultETdate
          // };
          //$scope.langrcdetail.etadate = defaultETdate;
          //console.log($scope.langrcdetail.etadate, "MSG-516")

        } else if (SelectRCcode[1] != 11 || SelectRCcode[1] != 16) {
          $scope.hideSumETA = true;
          // $scope.langrcdetail = {//Last removed
          //   etadate: null
          // };
        }
        //console.log($scope.langrcdetail.etadate, "MSG-1586")
        // else {
        //   $scope.hideSumETA = true;
        //   $scope.hideAssigntkt = true;
        //   $scope.langrcdetail = {
        //     etadate: null
        //   };
        //   console.log($scope.langrcdetail.etadate, "MSG-497")
        // }
        if ($scope.healthsurveyquestion.questiontype == 6) {
          $scope.selectedTodoTypes = "";
          $scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
          $scope.answerArray = newValue.toString().split(";");
          for (var i = 0; i < $scope.todoValueArray.length; i++) {
            for (var j = 0; j < $scope.answerArray.length; j++) {
              if ($scope.todoValueArray[i].split("-")[0] == $scope.answerArray[j]) {
                if ($scope.selectedTodoTypes == "") {
                  $scope.selectedTodoTypes = $scope.todoValueArray[i].split("-")[1];
                } else {
                  $scope.selectedTodoTypes = $scope.selectedTodoTypes + "," + $scope.todoValueArray[i].split("-")[1];
                }
              }
            }
          }
          Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
            $scope.todotypes = todos
          });

          $scope.selectedSerialNo = $scope.healthsurveyquestion.serialno;
          //console.log($scope.selectedSerialNo, "$scope.selectedSerialNo-445")
          $scope.selectedSerialNoToPush.push($scope.selectedSerialNo + 1);
          $scope.previousSerialNoToPush.push($scope.selectedSerialNo);
          Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":[' + $scope.selectedSerialNoToPush[0] + ']}}]}}').getList().then(function (squestnext) {
            $scope.nextquestion = squestnext;
            $scope.selectedSerialNoToPush = [];
            if ($scope.nextquestion.length == 0) {
              $scope.selectedSerialNoToPush.push($scope.nextquestion.length);
            }
          });
        } else if ($scope.healthsurveyquestion.questiontype == 11 || $scope.healthsurveyquestion.questiontype == 12) {
          //$scope.selectedTodoTypes = "";
          $scope.selectedRCcodeToPush = [];
          $scope.selectedSerialNoToPush = [];
          $scope.previousSerialNoToPush = [];
          $scope.selectedRcCode = "";
          //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
          $scope.selectedRcCode = newValue.toString().split("-")[1];
          //$scope.selectedRcCode = $scope.healthsurveyquestion.rcCode;

          // $scope.previousQuestion = newValue.toString().split("-")[0];
          // console.log($scope.previousQuestion, "PREVIOUS Q - 413")

          $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
          //$scope.selectedRCcodeToPush.push(newValue);
          //console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE-722")
          //console.log($scope.healthsurveyquestion.serialno, "serialno-723")

          //for getting previous question serial no. to update in rcdetail table
          //console.log($scope.healthsurveyquestion.question, $scope.healthsurveyquestion.serialno, "QTYPE-421")
          $scope.previousSerialNoToPush.push($scope.healthsurveyquestion.serialno);

          //console.log($scope.customerId, $scope.categoryId, $scope.subcategoryId, "customerId - categoryId - subcategoryId - 1349")
          ////---------------- New code -----------
          if ($scope.selectedRcCode == 93) {
            $scope.selectedSerialNoToPush.push(0);
            //console.log("Here-723", $scope.selectedSerialNoToPush.push(0), $scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0])
          } else {
            Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
              $scope.selectedSerialNo = surveyquest.serialno;
              $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
              console.log($scope.selectedSerialNo, "Selected-serialno-728")
            });
          }
          // ////------------------- This part --------------------------------------
          // $scope.selectedSerialNo = "";
          // Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]}}').getList().then(function (sqaslast) {
          //   //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}').getList().then(function (sqas) {
          //   //console.log("Tot-Survey-Question-Left-Now-Is: ", sqaslast.length, sqaslast)
          //   for (var k = 0; k < sqaslast.length; k++) {
          //     if (k == sqaslast.length - 1) {
          //       //console.log($scope.previousSerialNoToPush[0], "VALUES-1357", sqaslast[sqaslast.length - 1].serialno)
          //       if ($scope.previousSerialNoToPush[0] == sqaslast[sqaslast.length - 1].serialno) {
          //         $scope.selectedSerialNoToPush.push(0);
          //         //console.log("Here-1356", $scope.selectedSerialNoToPush.push(0), $scope.selectedSerialNoToPush[0])
          //       } else {
          //         Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
          //           $scope.selectedSerialNo = surveyquest.serialno;
          //           $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
          //           //console.log($scope.selectedSerialNo, "Selected-serialno-731")
          //         });
          //       }
          //     }
          //   }
          // });
          // ////------------------- This part --------------------------------------

          // $scope.selectedSerialNo = "";
          // // Restangular.one('surveyquestions/findOne?filter[where][question]=' + newValue).get().then(function (surveyquest) {
          // //Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + newValue).get().then(function (surveyquest) {
          // Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
          //   //Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + newValue).get().then(function (surveyquest) {
          //   $scope.selectedSerialNo = surveyquest.serialno;
          //   $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
          //   console.log($scope.selectedSerialNo, "Selected-serialno-731")
          // });
          //console.log($scope.selectedSerialNoToPush, "Outside-Pushed-Selected-serialno-733")
          // Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
          // $scope.todotypes = todos
          // });
        }
        // else if ($scope.healthsurveyquestion.questiontype == 1){
        else if ($scope.healthsurveyquestion.questiontype == 9) {
          //provide from here $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
          //here itself increase with 1 the whatever serialno is
          //no need to define there as per question type
          //text value as $scope.selectedRcCode
          //---- there is need to implement $scope.selectedRCcodeToPush just like $scope.selectedSerialNoToPush
          $scope.selectedRCcodeToPush = [];
          $scope.selectedSerialNoToPush = [];
          $scope.previousSerialNoToPush = [];
          $scope.selectedRcCode = "";
          $scope.selectedSerialNo = "";
          //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
          $scope.selectedRcCode = newValue.toString();
          $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
          //console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "$scope.selectedRcCode-766") ///--- to send
          $scope.selectedSerialNo = $scope.healthsurveyquestion.serialno;
          //console.log($scope.selectedSerialNo, "$scope.selectedSerialNo-445")
          $scope.selectedSerialNoToPush.push($scope.selectedSerialNo + 1);
          $scope.previousSerialNoToPush.push($scope.selectedSerialNo);
          //console.log($scope.previousSerialNoToPush, "$scope.selectedSerialNo-448")
          //try to find whether for this increased serialno & related customer, category and subcategory is there any
          //record in the table - if no then push 0 in this array $scope.selectedSerialNoToPush.push(0)
          //console.log($scope.selectedSerialNoToPush[0], "Next-serialno-461")
          //console.log("Here at - 772", $scope.customerId, $scope.categoryId, $scope.subcategoryId)
          // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno]=' + $scope.selectedSerialNoToPush[0]).getList().then(function (squestnext){
          Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":[' + $scope.selectedSerialNoToPush[0] + ']}}]}}').getList().then(function (squestnext) {
            //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":['+ $scope.selectedSerialNoToPush[0] +']}}]}}').getList().then(function (squestnext){
            $scope.nextquestion = squestnext;
            $scope.selectedSerialNoToPush = [];
            //console.log($scope.nextquestion, $scope.nextquestion.length, "468")
            if ($scope.nextquestion.length == 0) {
              $scope.selectedSerialNoToPush.push($scope.nextquestion.length);
              //console.log($scope.selectedSerialNoToPush[0], "Next SR No.-471")
            }
          });

          //console.log($scope.selectedSerialNoToPush[0], "Next-serialno-475") ///--- to send
          //Also the below need to handle properly above post because text entry
          // if($scope.selectedSerialNoToPush[0] == undefined){
          // $scope.selectedSerialNoToPush.push(0);
          // console.log($scope.selectedSerialNoToPush[0],"VALUE-NOW-440")
          // }
          //need to re-define the ticket closing.
        }
      }
    });

    $scope.$watch('onetoone.todoid', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        Restangular.one('todotypes', newValue).get().then(function (todo) {
          $scope.todoDueDays = todo.due;
        });
      }
    });


    $scope.modalSummaryAns = $modal.open({
      animation: true,
      templateUrl: 'template/Summaryanswers.html',
      scope: $scope,
      backdrop: 'static'


    });
  };

  $scope.okSummaryAns = function () {
    $scope.modalSummaryAns.close();
  };
  //$routeParams.id
  // $scope.okOnetooOne = function (fullname) {
  //     $scope.modalSummaryAns.close();
  //     console.log("here-435")
  //     var fullname = fullname;
  //     $rootScope.fullname = $window.sessionStorage.fullName = fullname;
  //     console.log('$rootScope.okOnetooOne', $rootScope.fullname);
  //     $location.path("/onetoone/" + fullname);
  //     $route.reload();
  //   };
  // $location.path("/onetoone/" + $routeParams.id);

  ////////////////////////////////////PREV ANSWERS////////////////////////////

  $scope.getQuestion = function (questionId) {
    return Restangular.one('surveyquestions', questionId).get().$object;
  };

  $scope.getUser = function (createdby) {
    return Restangular.one('users', createdby).get().$object;
  };

  $scope.getUpdateType = function (updatetpname) {
    return Restangular.one('updatetypes', updatetpname).get().$object;
  };

  $scope.getServiceEvent = function (serviceevnt) {
    return Restangular.one('serviceevents', serviceevnt).get().$object;
  };

  $scope.getRollbackRole = function (rollbackRoleby) {
    return Restangular.one('roles', rollbackRoleby).get().$object;
  };

  $scope.getRollbackUser = function (rollbackUser) {
    return Restangular.one('users', rollbackUser).get().$object;
  };

  $scope.getRollbackStatus = function (rollbackStatus) {
    return Restangular.one('rollbackstatuses', rollbackStatus).get().$object;
  };

  //////////////////////////////////RATING//////////////////////////////////////
  $scope.rating = 0;
  $scope.ratings = [{
    current: 3,
    max: 5
  }];

  $scope.getSelectedRating = function (rating) {
    //console.log(rating);
    $scope.onetoone.answer = rating;
  }

  $scope.setMinrate = function () {
    $scope.ratings = [{
      current: 1,
      max: 5
    }];
  }

  $scope.setMaxrate = function () {
    $scope.ratings = [{
      current: 5,
      max: 5
    }];
  }
  //////////////////////////////////RATING//////////////////////////////////////
  //console.log("In onetoone at 84")
  // Restangular.one('members', $routeParams.id).get().then(function (mem) {
  Restangular.one('rcdetails', $routeParams.id).get().then(function (mem) {

    Restangular.one('userviews?filter[where][managerId][gt]=0&filter[where][orgStructure][like]=%' + mem.pincodeId + '%').get().then(function (fes) {
      $scope.users = fes;
    });
    // $scope.memberName = mem.fullname;
    // $scope.memberName = mem.serno;
    $scope.memberName = mem.dispatchno;
    //$scope.memberName = mem.customername;
    //$scope.memberId = mem.memberId; No data/Not using
    $scope.customerId = mem.customerId;
    $scope.categoryId = mem.categoryId;
    $scope.subcategoryId = mem.subcategoryId;
    $scope.memberOrgStructure = mem.orgstructure;
    //console.log($scope.memberName, $scope.customerId, $scope.categoryId, $scope.subcategoryId, $scope.memberOrgStructure, "DIFFERENT PARAMS")
    if (mem.rollbackStatus == 'R') {
      $scope.submitDisable = true;
      $scope.disablerollback = true;
    } else if (mem.rcCode == 9) {
      $scope.submitDisable = false;
      $scope.disablerollback = true;
    } else {
      $scope.submitDisable = false;
      $scope.disablerollback = false;
    }

  });


  $scope.onetoonelanguage = Restangular.one('onetoonelanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;

  Restangular.one('rcdetails', $routeParams.id).get().then(function (newMem) {

    // $scope.newStatusId = newMem.statusId;

    console.log("Status for each RC: ", newMem.statusId);
    $scope.onetoone = {
      memberid: $routeParams.id,
      date: new Date(),
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      answer: null,
      statusid: newMem.statusId
    };
  });
  $scope.onetoonesum = {
    memberid: $routeParams.id,
    date: new Date(),
    deleteflag: false,
    lastmodifiedby: $window.sessionStorage.userId,
    lastmodifiedrole: $window.sessionStorage.roleId,
    lastmodifiedtime: new Date(),
    createdby: $window.sessionStorage.userId,
    createdtime: new Date(),
    createdrole: $window.sessionStorage.roleId,
    answer: null
  };

  //    Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().then(function (sqtn) {
  //        var quelength = sqtn.length;
  //
  //        Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][memberid]=' + $routeParams.id).getList().then(function (sans) {
  //            var anslength = sans.length;
  //            var percent = 100 * anslength;
  //            $scope.progressPercentage = percent / quelength;
  //            console.log($scope.progressPercentage);
  //
  //            $scope.myStyle = {
  //                "width": $scope.progressPercentage + "%"
  //            };
  //        });
  //    });
  // Restangular.one('members', $routeParams.id).get().then(function (member) {
  //console.log($routeParams.id, "$routeParams.id-651")
  $scope.groupserialno = 1;
  Restangular.one('rcdetails', $routeParams.id).get().then(function (member) {
    if (member.answeredQuestion == null) {
      member.answeredQuestion = 0;
    }
    //console.log("In onetoone at 128")
    Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][memberid]=' + $routeParams.id + '&filter[where][todostatusid]=1').getList().then(function (opntodos) {
      if (opntodos.length > 0) {
        // console.log($routeParams.id, opntodos, "Before going to todos")
        window.location = '/todos';
      } else {
        // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId).getList().then(function (ticketquestotal) {
        Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]}}').getList().then(function (ticketquestotal) {
          //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}').getList().then(function (ticketquestotal) {
          $scope.totquest = ticketquestotal;
          //console.log($scope.totquest, $scope.totquest.length, "For-Total-Question-944")
          // Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][memberid]=' + $routeParams.id).getList().then(function (sqas) {
          Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id).getList().then(function (sqas) {
            $scope.surveyanswers = sqas;
            if (ticketquestotal.length === sqas.length) {
              window.location = '/';
            }
            //console.log($scope.surveyanswers, "$scope.surveyanswers-145")
            //console.log(member.answeredQuestion, "member.answeredQuestion-146")
            var count = 0;
            $scope.myArray = [];
            $scope.myansoptionArray = [];
            $scope.mynewansoptionArray = [];
            //console.log($scope.surveyanswers.length, "$scope.surveyanswers.length")
            // if ($scope.surveyanswers.length == 0) {
              //console.log("here-955")
              $scope.disablerollback = true;
              // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
              // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
              // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
              Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"gt":[' + member.answeredQuestion + ']}},{"groupserialno":{"inq":[' + $scope.groupserialno + ']}}]}}').getList().then(function (sqs) {
                //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"gt":['+ member.answeredQuestion +']}}]}}').getList().then(function (sqs) {
                //console.log(sqs, "SQS-144")
                if (sqs.length == 0) {
                  //console.log("here-111")
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: 0,
                    //completedFlag: true
                  };
                  // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                  Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                    // window.location = '/todos';
                  });
                }

                ///////////////////Progress Calculation//////////////////////////////
                var maxSl = sqs[sqs.length - 1].serialno;
                var currentSl = member.answeredQuestion;
                $scope.progressPercentage = ((currentSl / maxSl) * 100).toFixed(0);
                ///////////////////Progress Calculation//////////////////////////////

                $scope.surveyquestions = sqs;
                for (var s = 0; s < $scope.surveyquestions.length; s++) {
                  if (!!$scope.surveyquestions[s].answeroptions) {
                    $scope.surveyquestions[s].answeroptionsarray = $scope.surveyquestions[s].answeroptions.split(',');
                  } else {
                    $scope.surveyquestions[s].answeroptionsarray = [];
                  }
                  $scope.surveyquestions[s].myOptionsArray = [];

                  angular.forEach($scope.surveyquestions[s].answeroptionsarray, function (data) {
                    if (data != '' || data != undefined) {
                      data = data.replace(/\s/g, "");
                      $scope.surveyquestions[s].myOptionsArray.push({
                        value: data,
                        visible: true,
                        answer: null
                      });
                      // console.log($scope.myOptionsArray);
                    }
                  });
                }
                console.log("$scope.surveyquestions", $scope.surveyquestions);
                $scope.healthsurveyquestion = $scope.surveyquestions[0];
                if (!!$scope.surveyquestions[0].answeroptions) {
                  $scope.myansoptionArray = $scope.surveyquestions[0].answeroptions.split(',');
                } else {
                  $scope.myansoptionArray = [];
                }
                //console.log($scope.myansoptionArray, "myansoptionArray-DECLARED-ARRAY-205")

                //Finally to bring in array commented below -
                //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
                // console.log($scope.healthsurveyquestion.answeroptionsarray, "ARRAY-176")
                //$scope.healthsurveyquestion.answeroptionsarray = $scope.myansoptionArray;//working

                for (var k = 0; k < $scope.myansoptionArray.length; k++) {
                  Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.myansoptionArray[k]).get().then(function (relatedquest) {
                    $scope.drpvalue = relatedquest.question + "-" + relatedquest.rcCode;
                    //console.log($scope.drpvalue, "$scope.drpvalue-237")
                    $scope.mynewansoptionArray.push($scope.drpvalue);
                  });

                }
                //console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-221")
                $scope.healthsurveyquestion.answeroptionsarray = $scope.myansoptionArray;
                // $scope.mynewansoptionArray = [];
                // angular.forEach($scope.myansoptionArray, function (data) {
                //     //if (data != '' || data != undefined) {
                //        // data = data.replace(/\s/g, "");

                //        $scope.mynewansoptionArray.push({data
                //            // value: data
                //             //visible: true,
                //             //answer: null
                //         });
                //         // console.log($scope.myOptionsArray);
                //     //}
                // });
                // console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-257")
                //}
                //console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-243")


                $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
                $scope.myOptions = [];
                //console.log($scope.healthsurveyquestion.question, "QTYPE-178")
                if ($scope.healthsurveyquestion.question === '6') {
                  $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
                } else if ($scope.healthsurveyquestion.question === '11' || $scope.healthsurveyquestion.question === '12' || $scope.healthsurveyquestion.question === '2') {
                  $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
                }
                $scope.myOptionsArray = [];

                angular.forEach($scope.myOptions, function (data) {
                  if (data != '' || data != undefined) {
                    data = data.replace(/\s/g, "");
                    $scope.myOptionsArray.push({
                      value: data,
                      visible: true,
                      answer: null
                    });
                    // console.log($scope.myOptionsArray);
                  }
                });
              });
            // } 
            
            // else {
            //   //console.log("Inside else - 1043-Also tot-Q-Lenth", $scope.totquest.length)
            //   //  $scope.disablerollback = false;
            //   angular.forEach($scope.surveyanswers, function (val) {
            //     $scope.myArray.push(val.questionid);
            //     count++;
            //     //console.log($scope.myArray, "$scope.myArray-207")
            //     //console.log(count, $scope.surveyanswers.length, "Inside else - 208", $scope.customerId, $scope.categoryId)
            //     if (count == $scope.surveyanswers.length) {
            //       //console.log(count, $scope.surveyanswers.length, $scope.customerId, $scope.categoryId, "Inside else - 194")
            //       // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {
            //       // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {

            //       //Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {
            //       //By Saty - removed '&filter[where][serialno][gt]=' + member.answeredQuestion for all questions for now ---
            //       // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId).getList().then(function (sqas) {
            //       Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}},{"groupserialno":{"inq":[' + $scope.groupserialno + ']}}]}}').getList().then(function (sqas) {
            //         //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}').getList().then(function (sqas) {
            //         //console.log($scope.customerId, $scope.categoryId, member.answeredQuestion, "customerId - categoryId - member.answeredQuestion")
            //         // console.log("Tot-Survey-Question-Left-Now-Is: ", sqas.length)
            //         if (sqas.length == 0) {
            //           //console.log("Here at 200 - tot-Q : tot-A", $scope.totquest.length, $scope.surveyanswers.length)
            //           if ($scope.totquest.length == $scope.surveyanswers.length) {
            //             $scope.member = {
            //               id: $routeParams.id,
            //               answeredQuestion: 0,
            //               completedFlag: true,
            //               // statusId: 5
            //               statusId: 15
            //             };
            //             // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
            //             Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
            //               window.location = '/';
            //               //window.location = '/todos';
            //             });
            //           } else {
            //             $scope.member = {
            //               id: $routeParams.id,
            //               fieldassignFlag: true
            //               //statusId: 3
            //             };
            //             //console.log("ELSE of sqas.length - 225")
            //             // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
            //             Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {

            //               //console.log("No more web questions for now....")
            //               window.location = '/';
            //             });
            //           }
            //         }

            //         ///////////////////Progress Calculation//////////////////////////////
            //         var maxSl = sqas[sqas.length - 1].serialno;
            //         var currentSl = member.answeredQuestion;
            //         $scope.progressPercentage = ((currentSl / maxSl) * 100).toFixed(0);
            //         ///////////////////Progress Calculation//////////////////////////////

            //         $scope.surveyquestions = sqas;
            //         for (var s = 0; s < $scope.surveyquestions.length; s++) {
            //           if (!!$scope.surveyquestions[s].answeroptions) {
            //             $scope.surveyquestions[s].answeroptionsarray = $scope.surveyquestions[s].answeroptions.split(',');
            //           } else {
            //             $scope.surveyquestions[s].answeroptionsarray = [];
            //           }
            //         }
            //         // console.log($scope.surveyquestions, "SURVEY QUESTION-252")
            //         //console.log(member.nextquestionId, "member.nextquestionId-754")
            //         // console.log($scope.surveyquestions[0],"SURVEY QUESTION-254")//----wrong flow
            //         // console.log(member.rcCode, "LAST QUESTION RELATED RC CODE")

            //         //$scope.myansoptionArray = $scope.surveyquestions[0].answeroptions.split(',');//----wrong flow
            //         //console.log($scope.myansoptionArray, "myansoptionArray-DECLARED-ARRAY-322")

            //         //Finally to bring in array commented below -
            //         //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
            //         // console.log($scope.healthsurveyquestion.answeroptionsarray, "ARRAY-176")
            //         //$scope.healthsurveyquestion.answeroptionsarray = $scope.myansoptionArray;//working


            //         //$scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;

            //         // console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-339")//----wrong flow

            //         if (member.nextquestionId > 0) {

            //           // console.log($scope.surveyquestions, "$scope.surveyquestions-359")
            //           // console.log(member.nextquestionId, "member.nextquestionId-360")

            //           for (var j = 0; j < $scope.surveyquestions.length; j++) {
            //             if ($scope.surveyquestions[j].serialno == member.nextquestionId) {
            //               //console.log("Matched-363")
            //               $scope.healthsurveyquestion = $scope.surveyquestions[j];
            //               //commented below one line to bring the dropdown rcCode alongwith questions
            //               //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[j].answeroptions.split(',');
            //               //$scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;
            //               //For clousre of ticket
            //               console.log(j, $scope.surveyquestions.length, "j-value-1156")
            //               if (j == $scope.surveyquestions.length - 1) {
            //                 // console.log("here-823")
            //                 $scope.mynewansoptionArray.push('Close');
            //                 //console.log($scope.mynewansoptionArray, "824")
            //                 $scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[j].answeroptions.split(',');
            //               } else {
            //                 //--- new binding
            //                 $scope.myansoptionArray = $scope.surveyquestions[j].answeroptions.split(',');
            //                 // console.log($scope.myansoptionArray, "371")
            //                 for (var k = 0; k < $scope.myansoptionArray.length; k++) {
            //                   Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.myansoptionArray[k]).get().then(function (relatedquest) {
            //                     $scope.drpvalue = relatedquest.question + "-" + relatedquest.rcCode;
            //                     // console.log($scope.drpvalue, "$scope.drpvalue-375")
            //                     $scope.mynewansoptionArray.push($scope.drpvalue);
            //                   });
            //                 }
            //                 $scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[j].answeroptions.split(',');
            //               } //new
            //             } else if (j == $scope.surveyquestions.length) {
            //               window.location = '/';
            //             }
            //           }

            //           // $scope.healthsurveyquestion = $scope.surveyquestions[0];
            //           // $scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
            //           // console.log($scope.healthsurveyquestion, "$scope.healthsurveyquestion----276")
            //           // console.log($scope.healthsurveyquestion.id,"ID-276")
            //           console.log($scope.healthsurveyquestion, "$scope.healthsurveyquestion.answeroptions-1184")
            //           $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
            //           $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
            //           console.log($scope.myOptions, "$scope.myOptions-1187")
            //           $scope.myOptionsArray = [];

            //           angular.forEach($scope.myOptions, function (data) {
            //             if (data != '' || data != undefined) {
            //               data = data.replace(/\s/g, "");
            //               $scope.myOptionsArray.push({
            //                 value: data,
            //                 visible: true,
            //                 answer: null
            //               });
            //               // console.log($scope.myOptionsArray);
            //             }
            //           });
            //           console.log($scope.myOptionsArray, "$scope.myOptionsArray-1201");
            //         } else {
            //           window.location = '/';
            //         }
            //       });
            //     }
            //   });
            // }
          });
        });
      } //--
    });
  });

  $scope.validatestring = "";
  $scope.todoToCreateArray = [];
  $scope.Submit = function () {

    for (var sq = 0; sq < $scope.surveyquestions.length; sq++) {
      $scope.surveyquestions[sq].questionid = $scope.surveyquestions[sq].id;
      $scope.surveyquestions[sq].deleteflag = false;
      $scope.surveyquestions[sq].memberid = $routeParams.id;
      // $scope.surveyquestions[sq].answer = $scope.surveyquestions[sq].answer;
      // $scope.surveyquestions[sq].uploadedfiles = $scope.surveyquestions[sq].uploadedfiles;
      $scope.answer = "";
      if ($scope.surveyquestions[sq].questiontype == 2) {
        angular.forEach($scope.surveyquestions[sq].myOptionsArray, function (data) {
          if (data.answer == true) {
            if ($scope.answer == "") {
              $scope.answer = data.value;
            } else {
              $scope.answer = $scope.answer + "," + data.value;
            }
          }
        });
        $scope.surveyquestions[sq].answer = $scope.answer;
      }

      if ($scope.surveyquestions[sq].questiontype == 9) {
        if ($scope.surveyquestions[sq].answer == null) {
          $scope.answer = $scope.surveyquestions[sq].uploadedfiles;
        } else {
          $scope.answer = $scope.surveyquestions[sq].uploadedfiles;
        }
        $scope.surveyquestions[sq].answer = $scope.answer;
      }
      delete $scope.surveyquestions[sq].id;
      if($scope.surveyquestions[sq].answer!=null){

        Restangular.all('surveyanswers').post(JSON.parse(JSON.stringify($scope.surveyquestions[sq]))).then(function (resp) {
  
        });
      }
    }

    $scope.uploader.uploadAll();

    console.log("$scope.surveyquestions", $scope.surveyquestions);



    $scope.groupserialno = $scope.groupserialno + 1;
    Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"groupserialno":{"inq":[' + $scope.groupserialno + ']}}]}}').getList().then(function (ticketquestotal) {
      if (ticketquestotal.length > 0) {
        $scope.surveyquestions = ticketquestotal;
        for (var s = 0; s < $scope.surveyquestions.length; s++) {
          if (!!$scope.surveyquestions[s].answeroptions) {
            $scope.surveyquestions[s].answeroptionsarray = $scope.surveyquestions[s].answeroptions.split(',');
          } else {
            $scope.surveyquestions[s].answeroptionsarray = [];
          }
          $scope.surveyquestions[s].myOptionsArray = [];

          angular.forEach($scope.surveyquestions[s].answeroptionsarray, function (data) {
            if (data != '' || data != undefined) {
              data = data.replace(/\s/g, "");
              $scope.surveyquestions[s].myOptionsArray.push({
                value: data,
                visible: true,
                answer: null
              });
              // console.log($scope.myOptionsArray);
            }
          });
        }
        console.log("$scope.surveyquestions", $scope.surveyquestions);
      } else {
        window.location = '/';
      }
    });
  }
  $scope.SubmitOld = function () {

    $scope.message = 'Answer has been saved!';
    $scope.modalTitle = 'Thank you!!!';
    console.log($scope.selectedRCcodeToPush[0], "$scope.selectedRCcodeToPush[0]-1235")

    $scope.answer = "";
    if ($scope.healthsurveyquestion.questiontype == 2) {
      angular.forEach($scope.myOptionsArray, function (data) {
        if (data.answer == true) {
          if ($scope.answer == "") {
            $scope.answer = data.value;
          } else {
            $scope.answer = $scope.answer + data.value;
          }
        }
      });
      $scope.onetoone.answer = $scope.answer;
    }

    if ($scope.healthsurveyquestion.questiontype == 9) {
      if ($scope.onetoone.uploadedfiles == null) {
        $scope.answer = $scope.onetoone.answer;
      } else {
        $scope.answer = $scope.onetoone.answer + "," + $scope.onetoone.uploadedfiles;
      }
      $scope.onetoone.answer = $scope.answer; datepickerStartdate
    }
    // document.getElementById('aspname').style.border = "";
    document.getElementById('datepickerETAdate').style.border = "";
    document.getElementById('datepickerStartdate').style.border = "";
    document.getElementById('datepickerEnddate').style.border = "";
    document.getElementById('couriername').style.border = "";
    document.getElementById('docket').style.border = "";
    document.getElementById('dispatchdate').style.border = "";
    if ($scope.onetoone.answer == '' || $scope.onetoone.answer == null) {
      $scope.validatestring = $scope.validatestring + 'Please Select Answer';
    } else if ($scope.onetoone.updatetypeid == '' || $scope.onetoone.updatetypeid == null) {
      $scope.validatestring = $scope.validatestring + 'Please Select Update Type';

    } else if ($scope.onetoone.serviceeventid == '' || $scope.onetoone.serviceeventid == null) {
      $scope.validatestring = $scope.validatestring + 'Please Select Service Event';

    } else if ($scope.selectedRCcodeToPush[0] == 11 || $scope.selectedRCcodeToPush[0] == 16) {
      //console.log("Here-1020", $scope.etadate)
      if ($scope.langrcdetail.etadate != undefined) {
        console.log($scope.langrcdetail.etadate, "Here-1021")
      }
      // console.log($scope.langrcdetail.etadate, "Here-1020", $scope.etadate)
      //if ($scope.langrcdetail.etadate == '' || $scope.etadate == undefined) {
      //if ($scope.langrcdetail.etadate == undefined) {
      // console.log($scope.dateTimelocal.validity.valid, "OUTPUT-1024")
      // var valuedate = document.getElementById('datepickerSumETAdate');
      // console.log(valuedate.value();
      // if ($scope.etadate == '' || $scope.etadate == null) {
      if ($scope.langrcdetail.etadate == '' || $scope.langrcdetail.etadate == null || $scope.langrcdetail.etadate == undefined) {
        //$scope.enddate = null;
        $scope.langrcdetail.etadate = null;
        $scope.validatestring = $scope.validatestring + 'Please Select ETA Time';
        document.getElementById('datepickerETAdate').style.border = "1px solid #ff0000";
      }
    } else if ($scope.selectedRCcodeToPush[0] == 10) {
      //console.log("Here-1020", $scope.etadate)
      // if ($scope.langrcdetail.city == '' || $scope.langrcdetail.city == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Select City';
      // } else 
      // if ($scope.onetoone.aspname == '' || $scope.onetoone.aspname == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Enter ASP Name';
      //   document.getElementById('aspname').style.border = "1px solid #ff0000";

      // } else 
      if ($scope.langrcdetail.user == '' || $scope.langrcdetail.user == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Engineer';
      } else if ($scope.langrcdetail.starttime == '' || $scope.langrcdetail.starttime == null || $scope.langrcdetail.starttime == undefined) {
        //$scope.enddate = null;
        $scope.langrcdetail.starttime = null;
        $scope.validatestring = $scope.validatestring + 'Please Select Start Date | Time';
        document.getElementById('datepickerStartdate').style.border = "1px solid #ff0000";
      } else if ($scope.langrcdetail.endtime == '' || $scope.langrcdetail.endtime == null || $scope.langrcdetail.endtime == undefined) {
        //$scope.enddate = null;
        $scope.langrcdetail.endtime = null;
        $scope.validatestring = $scope.validatestring + 'Please Select End Date | Time';
        document.getElementById('datepickerEnddate').style.border = "1px solid #ff0000";
      }
      // if ($scope.langrcdetail.starttime == undefined) {
      //   console.log($scope.langrcdetail.starttime, "Here-1248")

      //} 
      // else if ($scope.langrcdetail.endtime == undefined) {

      // }
    } else if ($scope.selectedRCcodeToPush[0] == 93) {

      if ($scope.onetoone.couriername == '' || $scope.onetoone.couriername == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter COURIER NAME';
        document.getElementById('couriername').style.border = "1px solid #ff0000";

      } else if ($scope.onetoone.docket == '' || $scope.onetoone.docket == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter DOCKET';
        document.getElementById('docket').style.border = "1px solid #ff0000";

      } else if ($scope.onetoone.dispatchdate == '' || $scope.onetoone.dispatchdate == null || $scope.onetoone.dispatchdate == undefined) {

        $scope.onetoone.dispatchdate = null;
        $scope.validatestring = $scope.validatestring + 'Please Select DISPATCH Date | Time';
        document.getElementById('dispatchdate').style.border = "1px solid #ff0000";
      }

    }

    if ($scope.validatestring != '') {
      $scope.toggleValidation();
      $scope.validatestring1 = $scope.validatestring;
      $scope.validatestring = '';
      //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    } else {

      // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      $scope.submitDisable = true;
      $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
      //added by me
      $scope.onetoone.deleteflag = false;
      //  console.log("Inside Submit button satusID:", $scope.onetoone.statusid);
      // Restangular.one('rcdetails', $routeParams.id).get().then(function (tickets) {
      //   console.log("Status for Each RC: ", tickets.statusId);
      //   $scope.onetoone.statusid = tickets.statusId;
      // });
      // console.log($scope.onetoone.answer, "THE ANSWER-434")
      // console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE, selectedRCcodeToPush[0]-435")
      // console.log($scope.healthsurveyquestion.serialno, "serialno-436")
      // console.log($scope.healthsurveyquestion.questiontype, "questiontype-437")
      // console.log($scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0], "Outside-Pushed-Selected-serialno-341")
      //on selection of close need to check with the displayed question RCcode - Is it the last question then pash 
      //in this $scope.selectedSerialNoToPush[0] array zero
      // if ($scope.selectedSerialNoToPush[0] == undefined) {
      //   $scope.selectedSerialNoToPush.push(0);
      //   console.log($scope.selectedSerialNoToPush[0], "VALUE-NOW-440")
      // }
      console.log($scope.selectedRCcodeToPush[0], "$scope.selectedRCcodeToPush[0]- $scope.selectedSerialNoToPush[0] - 1363", $scope.selectedSerialNoToPush[0])
      if ($scope.selectedSerialNoToPush[0] != undefined) {
        //console.log("Here-1289")
        //console.log($scope.langrcdetail.etadate, "Here-1290")
        Restangular.all('surveyanswers').post($scope.onetoone).then(function (resp) {
          if ($scope.healthsurveyquestion.skipquestion === true && $scope.onetoone.answer.toString().split(",").indexOf($scope.healthsurveyquestion.skipquestionvalue) > -1) {
            // Restangular.one('surveyquestions', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
            //console.log("IF-448")
            // Restangular.one('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId, $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
            Restangular.one('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]}}', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
              //Restangular.one('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
              for (var i = 0; i < $scope.todotypes.length; i++) {
                if ($scope.todotypes[i].toInitial === true) {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: 0,
                    assignedto: 0,
                    producttypeid: $scope.onetoone.updatetypeid,
                    serviceeventid: $scope.onetoone.serviceeventid,
                    serviceidentifier: $scope.onetoone.serviceidentifier,
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                  };
                  break;
                } else {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: +skpqustn.serialno - 1,
                    assignedto: 0,
                    producttypeid: $scope.onetoone.updatetypeid,
                    serviceeventid: $scope.onetoone.serviceeventid,
                    serviceidentifier: $scope.onetoone.serviceidentifier,
                    rcCode: skpqustn.rcCode,
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                  };
                }
              }
              if ($scope.todotypes.length == 0) {
                if ($scope.selectedRCcodeToPush[0] == 10) {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: $scope.healthsurveyquestion.serialno,
                    //rcCode: $scope.healthsurveyquestion.rcCode,
                    rcCode: $scope.selectedRCcodeToPush[0],
                    nextquestionId: $scope.selectedSerialNoToPush[0],
                    prevquestionId: $scope.previousSerialNoToPush[0],
                    assignedto: $scope.langrcdetail.user,
                    producttypeid: $scope.onetoone.updatetypeid,
                    serviceeventid: $scope.onetoone.serviceeventid,
                    serviceidentifier: $scope.onetoone.serviceidentifier,
                    starttime: $scope.langrcdetail.starttime,
                    endtime: $scope.langrcdetail.endtime,
                    aspname: $scope.onetoone.aspname,
                    assignFlag: true,
                    statusId: 3,
                    //fieldassignFlag: true,
                    //etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                    //rcCode: $scope.healthsurveyquestion.rcCode
                  };
                } else if ($scope.selectedRCcodeToPush[0] == 93) {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: $scope.healthsurveyquestion.serialno,
                    //rcCode: $scope.healthsurveyquestion.rcCode,
                    rcCode: $scope.selectedRCcodeToPush[0],
                    nextquestionId: $scope.selectedSerialNoToPush[0],
                    prevquestionId: $scope.previousSerialNoToPush[0],

                    producttypeid: $scope.onetoone.updatetypeid,
                    serviceeventid: $scope.onetoone.serviceeventid,
                    serviceidentifier: $scope.onetoone.serviceidentifier,
                    couriername: $scope.onetoone.couriername,
                    docket: $scope.onetoone.docket,
                    dispatchdate: $scope.onetoone.dispatchdate,

                    //fieldassignFlag: true,
                    //etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                    //rcCode: $scope.healthsurveyquestion.rcCode
                  };
                } else {
                  $scope.member = {
                    id: $routeParams.id,
                    answeredQuestion: +skpqustn.serialno - 1,
                    // statusId: 7//here statusId: 2 not working
                    statusId: 2,
                    fieldassignFlag: true,
                    rcCode: skpqustn.rcCode,
                    producttypeid: $scope.onetoone.updatetypeid,
                    serviceeventid: $scope.onetoone.serviceeventid,
                    serviceidentifier: $scope.onetoone.serviceidentifier,
                    etatime: $scope.langrcdetail.etadate,
                    lastmodifiedby: $window.sessionStorage.userId,
                    lastmodifiedtime: new Date(),
                    lastmodifiedrole: $window.sessionStorage.roleId
                  };
                }
              }
              if ($scope.healthsurveyquestion.storeInCustomField === true) {
                $scope.member[$scope.healthsurveyquestion.customFieldName] = resp.answer;
              }
              // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
              //console.log("HERE at 478")
              Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {

                ////////////////////////////////TODO///////////////////////////////////////
                if ($scope.healthsurveyquestion.applytomessage == 'yes') {
                  for (var i = 0; i < $scope.todotypes.length; i++) {
                    var dueDate = parseInt($scope.todotypes[i].due);
                    var fifteendays = new Date();
                    fifteendays.setDate(fifteendays.getDate() + dueDate);
                    $scope.todoToCreate = {
                      "memberid": $routeParams.id,
                      "todotypeid": $scope.todotypes[i].id,
                      "todostatusid": 1,
                      "followupdate": new Date(),
                      "assignedto": 0,
                      "deleteflag": false,
                      "lastmodifiedby": $window.sessionStorage.userId,
                      "lastmodifiedrole": $window.sessionStorage.roleId,
                      "lastmodifiedtime": new Date(),
                      "createdby": $window.sessionStorage.userId,
                      "createdtime": new Date(),
                      "createdrole": $window.sessionStorage.roleId,
                      "orgstructure": $scope.memberOrgStructure,
                      "remarks": $scope.todotypes[i].remarks
                    }
                    //                            Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                    //                                $route.reload();
                    //                            });
                    $scope.todoToCreateArray.push($scope.todoToCreate);

                  }
                  $scope.saveTodos();
                } else {
                  if ($scope.selectedFile != null) {
                    // if($scope.healthsurveyquestion.questiontype == 9){
                    $scope.uploader.uploadAll();
                  } else {
                    $route.reload();
                  }
                }


                //////////////////////////////////TODO///////////////////////

              });
            });
          } else {
            //console.log("ELSE-1519-Only working")
            for (var i = 0; i < $scope.todotypes.length; i++) {
              if ($scope.todotypes[i].toInitial === true) {
                $scope.member = {
                  id: $routeParams.id,
                  answeredQuestion: 0,
                  producttypeid: $scope.onetoone.updatetypeid,
                  serviceeventid: $scope.onetoone.serviceeventid,
                  serviceidentifier: $scope.onetoone.serviceidentifier,
                  etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                };
                break;
              } else {
                //if($scope.healthsurveyquestion.questiontype == 11){- $scope.selectedRCcodeToPush[0]
                //console.log("Here-1536")
                //console.log($scope.selectedRCcodeToPush[0], "$scope.selectedRCcodeToPush[0]-1537")
                $scope.member = {
                  id: $routeParams.id,
                  answeredQuestion: $scope.healthsurveyquestion.serialno,
                  // rcCode: $scope.healthsurveyquestion.rcCode,
                  rcCode: $scope.selectedRCcodeToPush[0],
                  nextquestionId: $scope.selectedSerialNoToPush[0],
                  prevquestionId: $scope.previousSerialNoToPush[0],
                  producttypeid: $scope.onetoone.updatetypeid,
                  serviceeventid: $scope.onetoone.serviceeventid,
                  serviceidentifier: $scope.onetoone.serviceidentifier,
                  etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                  //rcCode: $scope.healthsurveyquestion.rcCode//$scope.selectedSerialNoToPush[0]
                };
              }
            }
            //console.log("Then from here working")
            if ($scope.todotypes.length == 0) {
              if ($scope.selectedRCcodeToPush[0] == 10) {
                $scope.member = {
                  id: $routeParams.id,
                  answeredQuestion: $scope.healthsurveyquestion.serialno,
                  //rcCode: $scope.healthsurveyquestion.rcCode,
                  rcCode: $scope.selectedRCcodeToPush[0],
                  nextquestionId: $scope.selectedSerialNoToPush[0],
                  prevquestionId: $scope.previousSerialNoToPush[0],
                  assignedto: $scope.langrcdetail.user,
                  producttypeid: $scope.onetoone.updatetypeid,
                  serviceeventid: $scope.onetoone.serviceeventid,
                  serviceidentifier: $scope.onetoone.serviceidentifier,
                  starttime: $scope.langrcdetail.starttime,
                  endtime: $scope.langrcdetail.endtime,
                  aspname: $scope.onetoone.aspname,
                  assignFlag: true,
                  statusId: 3,
                  //fieldassignFlag: true,
                  //etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                  //rcCode: $scope.healthsurveyquestion.rcCode
                };
              } else if ($scope.selectedRCcodeToPush[0] == 93) {
                //console.log($scope.selectedRCcodeToPush[0], "$scope.selectedRCcodeToPush[0]-1582")
                $scope.member = {
                  id: $routeParams.id,
                  answeredQuestion: $scope.healthsurveyquestion.serialno,
                  //rcCode: $scope.healthsurveyquestion.rcCode,
                  rcCode: $scope.selectedRCcodeToPush[0],
                  nextquestionId: $scope.selectedSerialNoToPush[0],
                  prevquestionId: $scope.previousSerialNoToPush[0],

                  producttypeid: $scope.onetoone.updatetypeid,
                  serviceeventid: $scope.onetoone.serviceeventid,
                  serviceidentifier: $scope.onetoone.serviceidentifier,
                  couriername: $scope.onetoone.couriername,
                  docket: $scope.onetoone.docket,
                  dispatchdate: $scope.onetoone.dispatchdate,

                  //fieldassignFlag: true,
                  //etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                  //rcCode: $scope.healthsurveyquestion.rcCode
                };
              } else {
                $scope.member = {
                  id: $routeParams.id,
                  answeredQuestion: $scope.healthsurveyquestion.serialno,
                  //rcCode: $scope.healthsurveyquestion.rcCode,
                  rcCode: $scope.selectedRCcodeToPush[0],
                  nextquestionId: $scope.selectedSerialNoToPush[0],
                  prevquestionId: $scope.previousSerialNoToPush[0],
                  //statusId: 5,
                  statusId: 2,
                  fieldassignFlag: true,
                  producttypeid: $scope.onetoone.updatetypeid,
                  serviceeventid: $scope.onetoone.serviceeventid,
                  serviceidentifier: $scope.onetoone.serviceidentifier,
                  etatime: $scope.langrcdetail.etadate,
                  lastmodifiedby: $window.sessionStorage.userId,
                  lastmodifiedtime: new Date(),
                  lastmodifiedrole: $window.sessionStorage.roleId
                  //rcCode: $scope.healthsurveyquestion.rcCode
                };
              }
            }
            // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
            //console.log("HERE at 1628")
            //rcCode: $scope.selectedRCcodeToPush[0], added this line inside 24-jul-20  $scope.member = {....
            //console.log($scope.selectedRCcodeToPush[0], "$scope.selectedRCcodeToPush[0]-1629")
            if ($scope.selectedSerialNoToPush[0] == 0) {
              $scope.member = {
                id: $routeParams.id,
                // statusId: 5,
                rcCode: $scope.selectedRCcodeToPush[0],
                statusId: 15,
                nextquestionId: 0,
                etatime: $scope.langrcdetail.etadate,
                producttypeid: $scope.onetoone.updatetypeid,
                serviceeventid: $scope.onetoone.serviceeventid,
                serviceidentifier: $scope.onetoone.serviceidentifier,
                couriername: $scope.onetoone.couriername,
                docket: $scope.onetoone.docket,
                dispatchdate: $scope.onetoone.dispatchdate,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedtime: new Date(),
                lastmodifiedrole: $window.sessionStorage.roleId
              };
            }
            //console.log($scope.member.statusId, "STATUS-ID-563")
            if ($scope.healthsurveyquestion.storeInCustomField === true) {
              $scope.member[$scope.healthsurveyquestion.customFieldName] = resp.answer;
            }
            Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
              ////////////////////////////////TODO///////////////////////////////////////
              $scope.myansoptionArray = [];
              $scope.mynewansoptionArray = [];
              $scope.selectedRCcodeToPush = [];
              $scope.selectedSerialNoToPush = [];
              //console.log($scope.selectedSerialNoToPush[0], "VALUE-NOW-567")
              if ($scope.healthsurveyquestion.applytomessage == 'yes') {

                for (var i = 0; i < $scope.todotypes.length; i++) {
                  var dueDate = parseInt($scope.todotypes[i].due);
                  var fifteendays = new Date();
                  fifteendays.setDate(fifteendays.getDate() + dueDate);
                  $scope.todoToCreate = {
                    "memberid": $routeParams.id,
                    "todotypeid": $scope.todotypes[i].id,
                    "todostatusid": 1,
                    "followupdate": new Date(),
                    "assignedto": 0,
                    "deleteflag": false,
                    "lastmodifiedby": $window.sessionStorage.userId,
                    "lastmodifiedrole": $window.sessionStorage.roleId,
                    "lastmodifiedtime": new Date(),
                    "createdby": $window.sessionStorage.userId,
                    "createdtime": new Date(),
                    "createdrole": $window.sessionStorage.roleId,
                    "orgstructure": $scope.memberOrgStructure,
                    "remarks": $scope.todotypes[i].remarks
                  }
                  //                            Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                  //                                $route.reload();
                  //                            });
                  $scope.todoToCreateArray.push($scope.todoToCreate);

                }
                $scope.saveTodos();
              } else {
                if ($scope.selectedFile != null) {
                  // if($scope.healthsurveyquestion.questiontype == 9){
                  $scope.uploader.uploadAll();
                  // $route.reload();
                } else {
                  $route.reload();
                }
              }


              //////////////////////////////////TODO///////////////////////
            });
          }
        }); //-- upto here
      }
    }
  };
  $scope.saveTodoCount = 0;
  $scope.saveTodos = function () {
    Restangular.all('todos').post($scope.todoToCreateArray[$scope.saveTodoCount]).then(function (todoresp) {
      $scope.saveTodoCount++;
      if ($scope.saveTodoCount < $scope.todoToCreateArray.length) {
        $scope.saveTodos();
      } else {
        if ($scope.selectedFile != null) {
          // if($scope.healthsurveyquestion.questiontype == 9){
          $scope.uploader.uploadAll();
        } else {
          $route.reload();
        }
      }
    });
  }

  $scope.showValidation = false;
  $scope.toggleValidation = function () {
    $scope.showValidation = !$scope.showValidation;
  };

  //Datepicker settings start

  $scope.today = function () {
    $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
  };
  $scope.today();
  $scope.presenttoday = new Date();
  $scope.showWeeks = true;
  $scope.toggleWeeks = function () {
    $scope.showWeeks = !$scope.showWeeks;
  };
  $scope.clear = function () {
    $scope.dt = null;
  };
  $scope.clear = function () {
    $scope.mytime = null;
  };
  $scope.dtmax = new Date();
  $scope.toggleMin = function () {
    $scope.minDate = ($scope.minDate) ? null : new Date();
  };
  $scope.toggleMin();
  $scope.picker = {};
  $scope.mod = {};
  $scope.start = {};
  $scope.incident = {};
  $scope.hlth = {};
  $scope.datestartedart = {};
  $scope.lasttest = {};

  $scope.open = function ($event, item, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker' + index).focus();
    });
    item.opened = true;
  };
  $scope.open1 = function ($event, item, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepicker1' + index).focus();
    });
    $scope.picker.opened = true;
  };

  $scope.opendob = function ($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepickerdob' + index).focus();
    });
    $scope.picker.dobopened = true;
  };

  $scope.openstartdate = function ($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepickerstartdate' + index).focus();
    });
    $scope.picker.startdateopened = true;
  };

  //Timepicker Start
  $scope.FromTime = new Date();
  $scope.ToTime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 1;

  $scope.ismeridian = true;

  $scope.startchanged = function () {
    console.log('Start Time changed to: ' + $filter('date')($scope.starttime, 'shortTime'));
  };
  $scope.endchanged = function () {
    console.log('End Time changed to: ' + $filter('date')($scope.endtime, 'shortTime'));
  };

  // $scope.clear = function () {
  // $scope.mytime = null;
  // };
  //Timepicker End

  $scope.todoToPush = [];
  $scope.todotypes = [];
  $scope.selectedRCcodeToPush = [];
  $scope.selectedSerialNoToPush = [];
  $scope.previousSerialNoToPush = [];
  /////////////////////////////////TODO DROPDOWN////////////////////////////
  $scope.$watch('onetoone.answer', function (newValue, oldValue) {
    if (newValue == '' || newValue == undefined || newValue == null) {
      return;
    } else {
      //console.log(newValue.toString(), newValue, "NewVALUE-696");yyyy-MM-ddTHH:mm:ss
      // var SelectRCcode = '';
      var SelectRCcode = newValue.toString().split("-");
      //console.log(SelectRCcode[1], "SELECT-RC-Code-1557")
      if (SelectRCcode[1] != 10) {
        $scope.hideEngineer = true;
        $scope.cities = null;
        $scope.users = null;
        // $scope.langrcdetail = {
        //   starttime: null,
        //   endtime: null
        // };
      }
      if (SelectRCcode[1] == 11 || SelectRCcode[1] == 16) {
        console.log("Inside rccode == 11 & 16")
        $scope.hideETA = false;
        document.getElementById('datepickerETAdate').style.border = "";
        var date = new Date();
        $scope.ETDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        //var defaultETdate = $scope.ETDate + "T00:00:00";
        //var defaultETdate = $scope.ETDate + "T07:20:35";

        var hr = ('0' + date.getHours()).slice(-2);
        var mn = ('0' + date.getMinutes()).slice(-2);
        //var sd = ('0' + date.getSeconds()).slice(-2);
        //var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn + ":" + sd;
        var defaultETdate = $scope.ETDate + "T" + hr + ":" + mn;

        //console.log(defaultETdate, "MSG-1572")
        //console.log(date.getHours(), date.getMinutes(), date.getSeconds(), "MSG-1460", hr, mn, sd)

        $scope.langrcdetail = {
          etadate: defaultETdate
        };
        //$scope.langrcdetail.etadate = defaultETdate;
        console.log($scope.langrcdetail.etadate, "MSG-1579")

      } else if (SelectRCcode[1] == 10) {
        console.log("Inside rccode == 10")
        $scope.hideEngineer = false;
        document.getElementById('datepickerStartdate').style.border = "";
        document.getElementById('datepickerEnddate').style.border = "";
        var date = new Date();
        $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        //console.log($scope.FromDate, "CURRENT DATE - 36")
        var startdate = $scope.FromDate + "T00:00:00";
        $scope.mindate = startdate;

        //console.log($routeParams.id, "member-id-1717")
        // $scope.disablerollback = false;
        var orgstr = $window.sessionStorage.orgStructure;
        //console.log(orgstr, "STR-ELSE")
        var strorgval = orgstr.split(";");
        //console.log(strorgval, strorgval.length, "strorgval-1722")
        // if (strorgval.length == 2) { //Previously
        if (strorgval.length > 0) {
          //For senior manager login (for state level)
          if (strorgval.length === 1) {
            var stateval = strorgval[0].split("-");
          } else {
            var stateval = strorgval[1].split("-");
          }
          // var stateval = strorgval[1].split("-");
          //console.log(stateval, stateval[1], "STATE-VAL-ELSE")

          Restangular.one('rcdetails/findOne?filter[where][id]=' + $routeParams.id).get().then(function (rcdetailsresp) {
            $scope.ticketCityId = rcdetailsresp.cityId;
            $scope.ticketStateId = rcdetailsresp.stateId;
            //console.log($scope.ticketStateId, $scope.ticketCityId, "ticket-StateId-CityId-1729")
            Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + $scope.ticketStateId + ']}}}').getList().then(function (stateresp) {
              //console.log(stateresp, "STATE-RESP")
              //$scope.states = stateresp;
              $scope.state = stateresp[0].name;
            });
            Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + $scope.ticketCityId + ']}}}').getList().then(function (cityresp) {
              //console.log(cityresp, "CITY-RESP")
              //$scope.cities = cityresp;
              $scope.city = cityresp[0].name;
            });
          });

        }
        // else if (strorgval.length == 3) {
        //   var stateval = strorgval[1].split("-");
        //   console.log(stateval, stateval[1], "STATE-VAL-ELSE")
        //   var cityval = strorgval[2].split("-");
        //   Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + cityval[1] + ']}}}').getList().then(function (cityresp) {
        //     //console.log(cityresp, "CITY-RESP")
        //     //$scope.cities = cityresp;
        //     $scope.city = cityresp[0].name;
        //   });
        // }
        // //console.log(cityval, cityval[1], cityval.length, "CITY-VAL-ELSE")
        // Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + stateval[1] + ']}}}').getList().then(function (stateresp) {
        //   console.log(stateresp, "STATE-RESP")
        //   //$scope.states = stateresp;
        //   $scope.state = stateresp[0].name;
        // });

        //Initially it was here before putting into above two conditions ---
        // Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + cityval[1] + ']}}}').getList().then(function (cityresp) {
        //   //console.log(cityresp, "CITY-RESP")
        //   $scope.cities = cityresp;
        // });
        console.log($routeParams.id, "$routeParams.id-1770")
        Restangular.one('rcdetailtodoview?filter[where][id]=' + $routeParams.id).get().then(function (orglevelresp) {
          //console.log(newValue, "newValue")
          $scope.orgStructure = orglevelresp[0];
          //console.log($scope.orgStructure, "$scope.orgStructure-1774")
          $scope.orgStructure = orglevelresp[0].orgStructure;
          //console.log($scope.orgStructure, "ORG-STRUC-1776")
          // var OrgStructTmp = $scope.orgStructure;
          // var reducedOrgStruct = OrgStructTmp.substring(0, 32);
          // console.log(reducedOrgStruct, "reducedOrgStruct-1779")
          // var actualorgStructure = $scope.orgStructure.split(';')[1].split('-');//Previously
          var actualorgStructure = $scope.orgStructure.split(';')[3].split('-');
          actualorgStructure = actualorgStructure[1];
          //console.log(actualorgStructure, "ACTUAL-1971")
          // Restangular.all('users?filter[where][orgStructure][like]=%' + reducedOrgStruct + '%').getList().then(function (userresp) {
          Restangular.all('users?filter[where][managerId]=0&filter[where][isPartner]=true&filter[where][orgStructure][like]=%' + actualorgStructure + '%').getList().then(function (userresp) {
            // Restangular.all('users?filter[where][orgStructure][like]=%' + $scope.orgStructure + '%').getList().then(function (userresp) {
            // Restangular.all('users?filter[where][orgStructure][like]=%' + actualorgStructure + '%').getList().then(function (userresp) {//old
            console.log(userresp, "userresp")
            $scope.asps = userresp;
          });
        });
        // $scope.langrcdetail = {
        //   etadate: null
        // };
        //console.log($scope.langrcdetail.etadate, "MSG-1633")
      } else if (SelectRCcode[1] == 93) {
        console.log("Inside rccode == 93")
        $scope.hideCallclosed = false;

      } else if (SelectRCcode[1] != 11 || SelectRCcode[1] != 16 || SelectRCcode[1] != 93) {
        console.log("Inside rccode != 11")
        $scope.hideETA = true;
        $scope.hideCallclosed = true;
        // $scope.langrcdetail = {
        //   etadate: null
        // };
        //console.log($scope.langrcdetail.etadate, "MSG-1586")
      }
      if ($scope.healthsurveyquestion.questiontype == 6) {
        $scope.selectedTodoTypes = "";
        $scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue ? $scope.healthsurveyquestion.applytodovalue.split(";") : [];
        $scope.answerArray = newValue.toString().split(";");
        for (var i = 0; i < $scope.todoValueArray.length; i++) {
          for (var j = 0; j < $scope.answerArray.length; j++) {
            if ($scope.todoValueArray[i].split("-")[0] == $scope.answerArray[j]) {
              if ($scope.selectedTodoTypes == "") {
                $scope.selectedTodoTypes = $scope.todoValueArray[i].split("-")[1];
              } else {
                $scope.selectedTodoTypes = $scope.selectedTodoTypes + "," + $scope.todoValueArray[i].split("-")[1];
              }
            }
          }
        }
        Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
          $scope.todotypes = todos
        });
        $scope.selectedSerialNo = $scope.healthsurveyquestion.serialno;
        //console.log($scope.selectedSerialNo, "$scope.selectedSerialNo-445")
        $scope.selectedSerialNoToPush.push($scope.selectedSerialNo + 1);
        $scope.previousSerialNoToPush.push($scope.selectedSerialNo);
        Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":[' + $scope.selectedSerialNoToPush[0] + ']}}]}}').getList().then(function (squestnext) {
          $scope.nextquestion = squestnext;
          if ($scope.nextquestion.length == 0) {
            $scope.selectedSerialNoToPush = [];
            $scope.selectedSerialNoToPush.push($scope.nextquestion.length);
          }
        });
      } else if ($scope.healthsurveyquestion.questiontype == 11 || $scope.healthsurveyquestion.questiontype == 12) {
        //$scope.selectedTodoTypes = "";
        $scope.selectedRCcodeToPush = [];
        $scope.selectedSerialNoToPush = [];
        $scope.previousSerialNoToPush = [];
        $scope.selectedRcCode = "";
        //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
        $scope.selectedRcCode = newValue.toString().split("-")[1];
        //$scope.selectedRcCode = $scope.healthsurveyquestion.rcCode;
        $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
        console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE-1976")
        //console.log($scope.healthsurveyquestion.serialno, "serialno-723")
        //console.log($scope.healthsurveyquestion.question, $scope.healthsurveyquestion.serialno, "QTYPE-1273")
        $scope.previousSerialNoToPush.push($scope.healthsurveyquestion.serialno);
        //for getting previous question serial no. to update in rcdetail table
        //$scope.previousQuestion = newValue.toString().split("-")[0];
        //console.log($scope.previousSerialNoToPush, $scope.previousSerialNoToPush[0], "PREVIOUS Q - 1271")

        //here need to find out for $scope.previousSerialNoToPush[0] content i.e. displayed question serial no.
        //now 14 Is it last question for that ticket related customer, category and sub category if yes then pass 0 
        //$scope.selectedSerialNoToPush.push(0);
        //if last question then will not go below Restangular.one('surveyquestions/findOne?filter[where][rcCode]
        //console.log($scope.customerId, $scope.categoryId, $scope.subcategoryId, "customerId - categoryId - subcategoryId - 1349")
        ////---------------- New code -----------
        if ($scope.selectedRcCode == 93) {
          $scope.selectedSerialNoToPush.push(0);
          $scope.onetoone.statusid = 15;
          // console.log("Inside asnwer satusID:", $scope.onetoone.statusid);
          // console.log("Here-1992", $scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0])
          // console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE-1993")
        } else {
          Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
            $scope.selectedSerialNo = surveyquest.serialno;
            $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
            //console.log($scope.selectedSerialNo, "Selected-serialno-1984")
          });
        }
        // ////------------------- This part --------------------------------------
        // $scope.selectedSerialNo = "";
        // Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]}}').getList().then(function (sqaslast) {
        //   //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":['+ $scope.subcategoryId +']}}]}}').getList().then(function (sqas) {
        //   console.log("Tot-Survey-Question-Left-Now-Is: ", sqaslast.length, sqaslast)
        //   for (var k = 0; k < sqaslast.length; k++) {
        //     if (k == sqaslast.length - 1) {
        //       console.log($scope.previousSerialNoToPush[0], "VALUES-1357", sqaslast[sqaslast.length - 1].serialno)
        //       if ($scope.previousSerialNoToPush[0] == sqaslast[sqaslast.length - 1].serialno) {
        //         $scope.selectedSerialNoToPush.push(0);
        //         console.log("Here-1356", $scope.selectedSerialNoToPush.push(0), $scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0])
        //       } else {
        //         Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
        //           $scope.selectedSerialNo = surveyquest.serialno;
        //           $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
        //           //console.log($scope.selectedSerialNo, "Selected-serialno-731")
        //         });
        //       }
        //     }
        //   }
        // });
        // ////------------------- This part --------------------------------------

        // $scope.selectedSerialNo = "";
        // // Restangular.one('surveyquestions/findOne?filter[where][question]=' + newValue).get().then(function (surveyquest) {
        // //Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + newValue).get().then(function (surveyquest) {
        // Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
        //   $scope.selectedSerialNo = surveyquest.serialno;
        //   $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
        //   console.log($scope.selectedSerialNo, "Selected-serialno-731")
        // });
        //console.log($scope.selectedSerialNoToPush, "Outside-Pushed-Selected-serialno-1378")
        // Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
        //     $scope.todotypes = todos
        // });
      }
      // else if ($scope.healthsurveyquestion.questiontype == 1){
      else if ($scope.healthsurveyquestion.questiontype == 9) {
        //provide from here $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
        //here itself increase with 1 the whatever serialno is
        //no need to define there as per question type
        //text value as $scope.selectedRcCode
        //---- there is need to implement $scope.selectedRCcodeToPush just like $scope.selectedSerialNoToPush
        $scope.selectedRCcodeToPush = [];
        $scope.selectedSerialNoToPush = [];
        $scope.previousSerialNoToPush = [];
        $scope.selectedRcCode = "";
        $scope.selectedSerialNo = "";
        //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
        $scope.selectedRcCode = newValue.toString();
        $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
        //console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "$scope.selectedRcCode-766") ///--- to send
        $scope.selectedSerialNo = $scope.healthsurveyquestion.serialno;
        //console.log($scope.selectedSerialNo, "$scope.selectedSerialNo-768")
        $scope.selectedSerialNoToPush.push($scope.selectedSerialNo + 1);
        $scope.previousSerialNoToPush.push($scope.selectedSerialNo);
        //console.log($scope.previousSerialNoToPush, "$scope.selectedSerialNo-448")
        //try to find whether for this increased serialno & related customer, category and subcategory is there any
        //record in the table - if no then push 0 in this array $scope.selectedSerialNoToPush.push(0)
        //console.log($scope.selectedSerialNoToPush[0], "Next-serialno-772")
        //console.log("Here at - 772", $scope.customerId, $scope.categoryId, $scope.subcategoryId)
        // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno]=' + $scope.selectedSerialNoToPush[0]).getList().then(function (squestnext){
        Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":[' + $scope.selectedSerialNoToPush[0] + ']}}]}}').getList().then(function (squestnext) {
          //Restangular.all('surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"fieldFlag":{"inq":[false]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' + $scope.customerId + ']}}]},{"categoryId":{"inq":[' + $scope.categoryId + ']}}]},{"subcategoryId":{"inq":[' + $scope.subcategoryId + ']}}]},{"serialno":{"inq":['+ $scope.selectedSerialNoToPush[0] +']}}]}}').getList().then(function (squestnext){
          $scope.nextquestion = squestnext;
          //console.log($scope.nextquestion, $scope.nextquestion.length, "775")
          if ($scope.nextquestion.length == 0) {
            $scope.selectedSerialNoToPush = [];
            $scope.selectedSerialNoToPush.push($scope.nextquestion.length);
            //console.log($scope.selectedSerialNoToPush[0], "Next SR No.-778")
          }
        });

        //console.log($scope.selectedSerialNoToPush[0], "Next-serialno-782") ///--- to send
        //Also the below need to handle properly above post because text entry
        // if($scope.selectedSerialNoToPush[0] == undefined){
        //     $scope.selectedSerialNoToPush.push(0);
        //    console.log($scope.selectedSerialNoToPush[0],"VALUE-NOW-440")
        // }
        //need to re-define the ticket closing.
      }
    }
  });

  $scope.$watch('onetoone.todoid', function (newValue, oldValue) {
    if (newValue == '' || newValue == undefined || newValue == null) {
      return;
    } else {
      Restangular.one('todotypes', newValue).get().then(function (todo) {
        $scope.todoDueDays = todo.due;
      });
    }
  });

  $scope.$watch('onetoone.aspname', function (newValue, oldValue) {
    if (newValue == '' || newValue == undefined || newValue == null) {
      return;
    } else {
      Restangular.one('users?filter[where][managerId]=' + newValue).get().then(function (fes) {
        $scope.users = fes;
      });
    }
  });
  /////////////////////////////////TODO DROPDOWN////////////////////////////



  /******************************google map****************************/
  $scope.LocateMe = function () {
    $scope.mapdataModal = true;

    var map = new google.maps.Map(document.getElementById('mapCanvas'), {
      zoom: 4,

      center: new google.maps.LatLng(12.9538477, 77.3507369),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    });
    var marker, i;

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(12.9538477, 77.3507369),
      map: map,
      html: ''
    });

    $scope.toggleMapModal();
  };

  $scope.toggleMapModal = function () {
    $scope.mapcount = 0;

    ///////////////////////////////////////////////////////MAP//////////////////////////

    var geocoder = new google.maps.Geocoder();

    function geocodePosition(pos) {
      geocoder.geocode({
        latLng: pos
      }, function (responses) {
        if (responses && responses.length > 0) {
          updateMarkerAddress(responses[0].formatted_address);
        } else {
          updateMarkerAddress('Cannot determine address at this location.');
        }
      });
    }

    function updateMarkerStatus(str) {
      document.getElementById('markerStatus').innerHTML = str;
    }

    function updateMarkerPosition(latLng) {
      //  console.log(latLng);
      $scope.onetoone.answer = latLng.lat() + ',' + latLng.lng();
      //$scope.member.longitude = latLng.lng();

      //  console.log('$scope.updatepromotion', $scope.updatepromotion);

      document.getElementById('info').innerHTML = [
        latLng.lat(),
        latLng.lng()
      ].join(', ');
    }

    function updateMarkerAddress(str) {
      document.getElementById('mapaddress').innerHTML = str;
    }
    var map;

    function initialize() {

      $scope.latitude = 12.9538477;
      $scope.longitude = 77.3507369;
      navigator.geolocation.getCurrentPosition(function (location) {
        console.log(location.coords.latitude);
        console.log(location.coords.longitude);
        console.log(location.coords.accuracy);
        $scope.latitude = location.coords.latitude;
        $scope.longitude = location.coords.longitude;
        //                });

        // console.log('$scope.address', $scope.address);

        var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
        map = new google.maps.Map(document.getElementById('mapCanvas'), {
          zoom: 4,
          center: new google.maps.LatLng($scope.latitude, $scope.longitude),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        });
        var marker = new google.maps.Marker({
          position: latLng,
          title: 'Point A',
          map: map,
          draggable: true
        });

        // Update current position info.
        updateMarkerPosition(latLng);
        geocodePosition(latLng);

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'dragstart', function () {
          updateMarkerAddress('Dragging...');
        });

        google.maps.event.addListener(marker, 'drag', function () {
          updateMarkerStatus('Dragging...');
          updateMarkerPosition(marker.getPosition());
        });

        google.maps.event.addListener(marker, 'dragend', function () {
          updateMarkerStatus('Drag ended');
          geocodePosition(marker.getPosition());
        });
      });


    }

    // Onload handler to fire off the app.
    //google.maps.event.addDomListener(window, 'load', initialize);
    initialize();

    window.setTimeout(function () {
      google.maps.event.trigger(map, 'resize');
      map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
      map.setZoom(10);
    }, 1000);


    $scope.SaveMap = function () {
      $scope.showMapModal = !$scope.showMapModal;
      //  console.log($scope.reportincident);
    };

    //console.log('fdfd');
    $scope.showMapModal = !$scope.showMapModal;
  };

  //////////////////////////////////////////IMAGE UPLOAD///////////////////////////////////

  $scope.Created = false;
  $scope.Updated = true;
  $scope.imageedit = true;
  $scope.hideuploder = true;

  $scope.imageUploadChange = function (event, surveyquestion) {
    console.log("event", event.id);
    // console.log("surveyquestion",surveyquestion);
    $scope.surveyquestions[+(event.id)].uploadedfiles = event.value.split(/[\/\\]/).pop();
    $scope.onetoone.uploadedfiles = event.value.split(/[\/\\]/).pop();
  }

  // $scope.getFileDetails = function(elm){
  //     $scope.onetoone.uploadedfiles = elm.value.split(/[\/\\]/).pop();
  //     $scope.files = [];
  //     $scope.$apply(function () {
  //         for (var i = 0; i < elm.files.length; i++) {
  //             $scope.files.push(elm.files[i])
  //         }
  //     });
  // }


  // $scope.reduceattachment = function () {
  //   uploader.queue.length = uploader.queue.length - 1;
  // };
  $scope.reduceattachment = function () {
    uploader.queue.length = 0;
  };

  $scope.clear = function () {
    angular.element("input[type='file']").val(null);
  };

  var uploader = $scope.uploader = $fileUploader.create({
    scope: $scope, // to automatically update the html. Default: $rootScope
    url: '//idcamp-api.herokuapp.com/api/v1/containers/converbiz/upload',
    formData: [{
      key: 'value'
    }],
    filters: [
      function (item) { // first user filter
        console.info('filter1');
        return true;
      }
    ]
  });

  $scope.imagecreate = false;
  $scope.Submitted = false;
  // ADDING FILTERS

  $scope.uploader.cancelAll = function () {
    console.log("cancel uploaded queue");
    $scope.hideuploder = true;
  };

  uploader.filters.push(function (item) { // second user filter
    console.info('filter2');
    return true;
  });
  //$scope.Submitted = false;
  $scope.isCreateView = false;
  // REGISTER HANDLERS

  uploader.bind('afteraddingfile', function (event, item) {
    console.info('After adding a file', item);
    $scope.onetoone.answer = $scope.healthsurveyquestion.rcCode;
    $scope.selectedFile = item;
  });

  uploader.bind('whenaddingfilefailed', function (event, item) {
    console.info('When adding a file failed', item);
  });

  uploader.bind('afteraddingall', function (event, items) {
    console.info('After adding all files', items);
    // if (items.length >= 2) {
    //     items[0].remove();
    // }
    console.log(items[0].file.name.toString(), "Browsed file name-1704")
    var str = items[0].file.name.toString().split('.');
    //console.log(str, "str-1706")
    str[0] = str[0].replace(/[^A-Z0-9]+/ig, "_");
    //str[0] = str[0].replace(/[^A-Z0-9]+/ig, "");
    console.log(str[0], "str[0] value-1708")
    var finalName = str.join('.');
    console.log('finalName', finalName);
    items[0].finalName = finalName;
    var myArray = [];
    var count = 0;
    angular.forEach(items, function (value) {
      myArray.push(value.file.name);
      $scope.onetoone.uploadedfiles = myArray.join();
      count++;
      $scope.Sno = count;
    });

    /********new add for file size chcking*******/
    console.info('After adding all file', items);
    var imageUrl = '//idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/' + items[0].file.name;
    $.ajax({
      type: 'HEAD',
      url: imageUrl,
      success: function () {
        alert('Duplicate File Name Change the file name to upload');
        items[0].remove();
        document.getElementById('Prosize').value = '';
      },
      error: function () {
        // alert('Page not found.');
      }
    });

    $scope.filesize = items[0].file.size / 1024 / 1024;
    $scope.finalsize = Math.round($scope.filesize);
    console.log('  $scope.filesize', $scope.filesize);
    console.log('  $scope.finalsize', $scope.finalsize);
    // console.log(' $window.sessionStorage.Productsize', $window.sessionStorage.Productsize);
    if ($scope.finalsize <= 5) {
      console.log('i am in if');
    } else {
      console.log('i am in else');
      items[0].remove();
      alert('select small file');
      document.getElementById('Prosize').value = null;
    }

  });

  uploader.bind('beforeupload', function (event, item) {
    console.info('Before upload', item);
  });

  uploader.bind('progress', function (event, item, progress) {
    console.info('Progress: ' + progress, item);
  });

  uploader.bind('success', function (event, xhr, item, response) {
    // $scope.saveoffer();
    console.info('Success', xhr, item, response);
    $scope.$broadcast('uploadCompleted', item);
  });

  uploader.bind('cancel', function (event, xhr, item) {
    console.info('Cancel', xhr, item);
  });

  uploader.bind('error', function (event, xhr, item, response) {
    console.info('Error', xhr, item, response);
  });

  uploader.bind('complete', function (event, xhr, item, response) {

  });

  uploader.bind('progressall', function (event, progress) {
    console.info('Total progress: ' + progress);
  });

  uploader.bind('completeall', function (event, items) {
    //  toaster.pop('success', 'file added successfully');
    //$scope.spinner.stop();
    //window.location = "/itemdefinitions";
    console.info('Complete all', items);
    //$route.reload();
  });

  $scope.load = function () {
    $http.get('//idcamp-api.herokuapp.com/api/v1/containers/converbiz/files').success(function (data) {
      console.log(data);
    });
  };

  $scope.delete = function (index, id) {
    $http.delete('//idcamp-api.herokuapp.com/api/v1/containers/converbiz/files/' + encodeURIComponent(id)).success(function (data, status, headers) {
      $scope.files.splice(index, 1);
    });
  };

  $scope.$on('uploadCompleted', function (event) {
    console.log('uploadCompleted event received');

    $scope.load();
  });

  //////////////////////////////////////////IMAGE UPLOAD END///////////////////////////////////
});
