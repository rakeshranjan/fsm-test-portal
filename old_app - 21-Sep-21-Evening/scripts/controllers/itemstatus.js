'use strict';
angular.module('secondarySalesApp')
    .controller('ItemstatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
          }
      
        //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        if ($window.sessionStorage.prviousLocation != "partials/itemstatus" || $window.sessionStorage.prviousLocation != "partials/customers") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
          }
      
          $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
          $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
          };
      
          $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
          $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
          };
    
           // ******************************** EOF Pagination *************************************************

        Restangular.all('itemstatuses?filter[where][deleteflag]=false').getList().then(function (itemstat) {
            $scope.itemstatuses = itemstat;
            angular.forEach($scope.itemstatuses, function (member, index) {
              member.index = index + 1;


            });
        });

});