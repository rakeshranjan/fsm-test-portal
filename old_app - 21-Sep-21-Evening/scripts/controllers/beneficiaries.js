'use strict';

angular.module('secondarySalesApp')

  .controller('BnCtrl', function ($scope, Restangular, $route, $window, $filter, $modal) {

    //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
    //            window.location = "/";
    //        }
    //----------- For Sub Menu Navigation ------------------
    window.sessionStorage.carryCount = 0;

    /*********************************** Pagination *******************************************/

    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/beneficiaries-form" || $window.sessionStorage.prviousLocation != "partials/beneficiaries") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };


    $scope.saveCount = 0;
    $scope.currentPos = 0;
    $scope.perPage = 100;
    $scope.rcdetails = [];

    $scope.restCall = function (roleId) {
      if ($scope.avgCountNew > $scope.saveCount) {
        if (roleId == 2) {
          Restangular.all('ticketdetailsviews?filter[limit]=' + $scope.perPage + '&filter[skip]=' + $scope.currentPos + '&filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId).getList().then(function (rcdetailsresp) {
            $scope.currentPos += $scope.perPage;
            $scope.rcdetails = $scope.rcdetails.concat(rcdetailsresp);

            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;
            });
            $scope.saveCount++;
            if (rcdetailsresp.length > 0) {
              $scope.restCall(roleId);
            }
          });
        } else {
          Restangular.all('ticketdetailsviews?filter[limit]=' + $scope.perPage + '&filter[skip]=' + $scope.currentPos + '&filter[where][deleteflag]=false').getList().then(function (rcdetailsresp) {
            $scope.currentPos += $scope.perPage;
            $scope.rcdetails = $scope.rcdetails.concat(rcdetailsresp);

            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;
            });
            $scope.saveCount++;
            if (rcdetailsresp.length > 0) {
              $scope.restCall(roleId);
            }
          });
        }
      }
    };

    if (window.sessionStorage.roleId == 2) {
      // Restangular.one('ticketdetailsviews/count?where[deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId).get().then(function (pcount) {
      //   $scope.pcount = pcount.count;
      //   $scope.avgCount = $scope.pcount / 100;
      //   $scope.avgCountNew = Math.round($scope.avgCount) + 1;
      //  // $scope.restCall(window.sessionStorage.roleId);
      // });

      Restangular.all('languagedefinitions?filter[where][deleteFlag]=false').getList().then(function (reslang) {
        $scope.languageDisplays = reslang;
      });

      Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + window.sessionStorage.customerId).getList().then(function (customer) {
        $scope.customers = customer;
        $scope.customerId = customer[0].id;
        $scope.restZnCall();
      });
    } else {
      // Restangular.one('ticketdetailsviews/count?where[deleteflag]=false').get().then(function (pcount) {
      //   $scope.pcount = pcount.count;
      //   $scope.avgCount = $scope.pcount / 100;
      //   $scope.avgCountNew = Math.round($scope.avgCount) + 1;
      // //  $scope.restCall(window.sessionStorage.roleId);
      // });

      Restangular.all('languagedefinitions?filter[where][deleteFlag]=false').getList().then(function (reslang) {
        $scope.languageDisplays = reslang;
      });

      Restangular.all('customers?filter[where][deleteFlag]=false').getList().then(function (customer) {
        $scope.customers = customer;
        $scope.customerId = customer[0].id;
        $scope.restZnCall();
      });
    }

    $scope.restZnCall = function () {
      Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (zoneResp) {
        $scope.zones = zoneResp;
        $scope.zoneId = zoneResp[0].id;
      });
    }

    $scope.getcategory = function (categoryId) {
      return Restangular.one('categories', categoryId).get().$object;
    };

    $scope.subgetcategory = function (subcategoryId) {
      return Restangular.one('subcategories', subcategoryId).get().$object;
    };

    $scope.getcity = function (cityId) {
      if (cityId > 0) {
        return Restangular.one('organisationlocations', cityId).get().$object;
      }
    };

    $scope.getpincode = function (pincodeId) {
      if (pincodeId > 0) {
        return Restangular.one('organisationlocations', pincodeId).get().$object;
      }
    };

    $scope.getstatus = function (statusId) {
      return Restangular.one('todostatuses', statusId).get().$object;
    };

    // $scope.restCallCust = function (customerId) {
    //   if ($scope.avgCountNew > $scope.saveCount) {
    //     Restangular.all('ticketdetailsviews?filter[limit]=' + $scope.perPage + '&filter[skip]=' + $scope.currentPos + '&filter[where][deleteflag]=false&filter[where][customerId]=' + customerId).getList().then(function (rcdetailsresp) {
    //       $scope.currentPos += $scope.perPage;
    //       $scope.rcdetails = $scope.rcdetails.concat(rcdetailsresp);

    //       angular.forEach($scope.rcdetails, function (member, index) {
    //         member.index = index + 1;
    //       });
    //       $scope.saveCount++;
    //       if (rcdetailsresp.length > 0) {
    //         $scope.restCallCust(customerId);
    //       }
    //     });
    //   }
    // };

    $scope.restCallZone = function (zoneId) {
      if ($scope.avgCountNew > $scope.saveCount) {
        // Restangular.all('ticketdetailsviews?filter[limit]=' + $scope.perPage + '&filter[skip]=' + $scope.currentPos + '&filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][statusId]=' + zoneId).getList().then(function (rcdetailsresp) {
        Restangular.all('ticketdetailsviews?filter[limit]=' + $scope.perPage + '&filter[skip]=' + $scope.currentPos + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][statusId]=' + zoneId).getList().then(function (rcdetailsresp) {
          $scope.currentPos += $scope.perPage;
          $scope.rcdetails = $scope.rcdetails.concat(rcdetailsresp);

          angular.forEach($scope.rcdetails, function (member, index) {
            member.index = index + 1;
          });
          $scope.saveCount++;
          if (rcdetailsresp.length > 0) {
            $scope.restCallZone(zoneId);
          }
        });
      }
    };

    // $scope.$watch('customerId', function (newValue, oldValue) {
    //   if (newValue === oldValue || newValue === '' || newValue === null) {
    //     return;
    //   } else {
    //     $scope.zoneId = $scope.zones[0].id;
    //     // $scope.saveCount = 0;
    //     // $scope.currentPos = 0;
    //     // $scope.perPage = 100;
    //     // $scope.rcdetails = [];

    //     // Restangular.one('ticketdetailsviews/count?where[deleteflag]=false&where[customerId]=' + newValue).get().then(function (pcount) {
    //     //   $scope.pcount = pcount.count;
    //     //   $scope.avgCount = $scope.pcount / 100;
    //     //   $scope.avgCountNew = Math.round($scope.avgCount) + 1;
    //     //   $scope.restCallCust(newValue);
    //     // });
    //   }
    // });

    $scope.$watch('zoneId', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        // console.log('zoneid', newValue);
        $scope.saveCount = 0;
        $scope.currentPos = 0;
        $scope.perPage = 100;
        $scope.rcdetails = [];

        // Restangular.one('ticketdetailsviews/count?where[deleteflag]=false&where[statusId]=' + newValue + '&where[customerId]=' + $scope.customerId).get().then(function (pcount) {
        Restangular.one('ticketdetailsviews/count?where[statusId]=' + newValue + '&where[customerId]=' + $scope.customerId).get().then(function (pcount) {
          $scope.pcount = pcount.count;
          if ($scope.pcount > $scope.perPage) {
            $scope.avgCount = $scope.pcount / 100;
            $scope.avgCountNew = Math.round($scope.avgCount) + 1;
            $scope.restCallZone(newValue);
          } else {
            $scope.restZoneCall(newValue);
          }
        });
      }
    });

    $scope.restZoneCall = function (zoneId) {
      //  $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][statusId]=' + zoneId + '&filter[where][customerId]=' + $scope.customerId;
      $scope.memberUrl = 'ticketdetailsviews?filter[where][statusId]=' + zoneId + '&filter[where][customerId]=' + $scope.customerId;
      Restangular.all($scope.memberUrl).getList().then(function (member) {
        $scope.rcdetails = member;
        angular.forEach($scope.rcdetails, function (member, index) {
          member.index = index + 1;
        });
      });
    };

    /************************************* INDEX ***************************************************/
    //     $scope.searchbulkupdate = '';

    //     $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    //     //console.log($scope.orgStructureSplit, "$scope.orgStructureSplit-428")
    //     $scope.CountrySplit = $scope.orgStructureSplit[0];
    //     $scope.ZoneSplit = $scope.orgStructureSplit[1];
    //     //console.log($scope.CountrySplit, "$scope.CountrySplit-430")
    //     $scope.intermediateOrgStructure = "";
    //     for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
    //       if ($scope.intermediateOrgStructure === "") {
    //         $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
    //       } else {
    //         $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
    //       }
    //     }
    //     //console.log($scope.intermediateOrgStructure, "$scope.intermediateOrgStructure-436")
    //     //--- No need now (Let it be because LHS menu somewhere linked) - It was for single state & multi city ---------
    //     $scope.orgStructureFilterArray = [];
    //     if ($scope.orgStructureSplit.length > 0) {
    //       $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
    //       console.log($scope.LocationsSplit, "$scope.LocationsSplit-444")
    //       for (var i = 0; i < $scope.LocationsSplit.length; i++) {
    //         $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
    //       }

    //     }
    //     console.log($scope.orgStructureFilterArray, "$scope.orgStructureFilterArray-451")
    //     $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    //     console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));
    //     //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
    //     $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
    //     console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
    //     //--- End of No need now - It was for single state & multi city ---------

    // //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
    //   $scope.orgStructureZoneFilterArray = [];
    //   if ($scope.orgStructureSplit.length > 1) {
    //     $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    //     console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
    //     for (var i = 0; i < $scope.ZoneSplit.length; i++) {
    //       $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[0] + "-" + $scope.ZoneSplit[i]);
    //     }
    //   }

    //   //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
    //   $scope.orgStructureStateFilterArray = [];
    //   if ($scope.orgStructureSplit.length > 1) {
    //     $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //   //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    //     console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //    // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
    //    for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
    //     for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //       // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //       $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //     }
    //    }
    //   }

    //     //-- For Multi state & multi City array filter work Satyen ---------------------------------
    //     // $scope.orgStructureStateFilterArray = [];
    //     // // if ($scope.orgStructureSplit.length > 0) {
    //     //   if ($scope.orgStructureSplit.length > 1) {
    //     //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //     //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //     //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //     //    // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //     //    $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //     //   }
    //     // }
    //     //console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-468")
    //     $scope.orgStructureFinalFilterArray = [];
    //     if ($scope.orgStructureStateFilterArray.length > 0) {
    //       for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
    //         for (var i = 0; i < $scope.LocationsSplit.length; i++) {
    //           $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
    //         }
    //       }
    //     }
    //     //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
    //     $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", "");
    //     // Restangular.all('members?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (member) {
    //     // Restangular.all('members?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgLevel + '%').getList().then(function (member) {

    //     // Restangular.all('members?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgLevel + '%' + '&filter[where][createdby]=' + $window.sessionStorage.userId).getList().then(function (member) {
    //     $scope.exportArray = [];


    //   /*  Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (member) {
    //       $scope.members = member;

    //       angular.forEach($scope.members, function (data, index) {
    //         data.index = index + 1;

    //         Restangular.all('categories?filter[where][deleteFlag]=false').getList().then(function (categories) {
    //           $scope.categoryname = categories;
    //           for (var j = 0; j < $scope.categoryname.length; j++) {
    //             if (data.categoryId == $scope.categoryname[j].id) {
    //               data.categoryname = $scope.categoryname[j].name;
    //               break;
    //             }
    //           }
    //         });

    //         Restangular.all('subcategories?filter[where][deleteFlag]=false').getList().then(function (subcategories) {
    //           $scope.subcategoryname = subcategories;
    //           for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //             if (data.subcategoryId == $scope.subcategoryname[j].id) {
    //               data.subcategoryname = $scope.subcategoryname[j].name;
    //               break;
    //             }
    //           }
    //         });

    //         Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
    //           $scope.cityname = cities;
    //           for (var j = 0; j < $scope.cityname.length; j++) {
    //             if (data.cityId == $scope.cityname[j].id) {
    //               data.cityname = $scope.cityname[j].name;
    //               break;
    //             }
    //           }
    //         });

    //         Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
    //           $scope.pincodename = pincodes;
    //           for (var j = 0; j < $scope.pincodename.length; j++) {
    //             if (data.pincodeId == $scope.pincodename[j].id) {
    //               data.pincodename = $scope.pincodename[j].name;
    //               break;
    //             }
    //           }
    //         });

    //         Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
    //           $scope.statusname = statuses;
    //           for (var j = 0; j < $scope.statusname.length; j++) {
    //             if (data.statusId == $scope.statusname[j].id) {
    //               data.statusname = $scope.statusname[j].name;
    //               break;
    //             }
    //           }
    //         });

    //         $scope.exportArray.push(data);
    //       });
    //     }); */

    //     if (window.sessionStorage.roleId == 11) {
    //       Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + window.sessionStorage.userId).getList().then(function (member) {
    //         $scope.members = member;

    //         angular.forEach($scope.members, function (data, index) {
    //           data.index = index + 1;

    //           Restangular.all('categories?filter[where][deleteFlag]=false').getList().then(function (categories) {
    //             $scope.categoryname = categories;
    //             for (var j = 0; j < $scope.categoryname.length; j++) {
    //               if (data.categoryId == $scope.categoryname[j].id) {
    //                 data.categoryname = $scope.categoryname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('subcategories?filter[where][deleteFlag]=false').getList().then(function (subcategories) {
    //             $scope.subcategoryname = subcategories;
    //             for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //               if (data.subcategoryId == $scope.subcategoryname[j].id) {
    //                 data.subcategoryname = $scope.subcategoryname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
    //             $scope.cityname = cities;
    //             for (var j = 0; j < $scope.cityname.length; j++) {
    //               if (data.cityId == $scope.cityname[j].id) {
    //                 data.cityname = $scope.cityname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
    //             $scope.pincodename = pincodes;
    //             for (var j = 0; j < $scope.pincodename.length; j++) {
    //               if (data.pincodeId == $scope.pincodename[j].id) {
    //                 data.pincodename = $scope.pincodename[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
    //             $scope.statusname = statuses;
    //             for (var j = 0; j < $scope.statusname.length; j++) {
    //               if (data.statusId == $scope.statusname[j].id) {
    //                 data.statusname = $scope.statusname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           $scope.exportArray.push(data);
    //         });
    //       }); 

    //     } else if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4 || window.sessionStorage.roleId == 8) {
    //       Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%').getList().then(function (member) {
    //         $scope.members = member;

    //         angular.forEach($scope.members, function (data, index) {
    //           data.index = index + 1;

    //           Restangular.all('categories?filter[where][deleteFlag]=false').getList().then(function (categories) {
    //             $scope.categoryname = categories;
    //             for (var j = 0; j < $scope.categoryname.length; j++) {
    //               if (data.categoryId == $scope.categoryname[j].id) {
    //                 data.categoryname = $scope.categoryname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('subcategories?filter[where][deleteFlag]=false').getList().then(function (subcategories) {
    //             $scope.subcategoryname = subcategories;
    //             for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //               if (data.subcategoryId == $scope.subcategoryname[j].id) {
    //                 data.subcategoryname = $scope.subcategoryname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
    //             $scope.cityname = cities;
    //             for (var j = 0; j < $scope.cityname.length; j++) {
    //               if (data.cityId == $scope.cityname[j].id) {
    //                 data.cityname = $scope.cityname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
    //             $scope.pincodename = pincodes;
    //             for (var j = 0; j < $scope.pincodename.length; j++) {
    //               if (data.pincodeId == $scope.pincodename[j].id) {
    //                 data.pincodename = $scope.pincodename[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
    //             $scope.statusname = statuses;
    //             for (var j = 0; j < $scope.statusname.length; j++) {
    //               if (data.statusId == $scope.statusname[j].id) {
    //                 data.statusname = $scope.statusname[j].name;
    //                 break;
    //               }
    //             }
    //           });

    //           $scope.exportArray.push(data);
    //         });
    //       }); 

    //     } else if (window.sessionStorage.roleId == 5) {
    //       console.log($scope.orgStructureFilter, "ORGSTRUCT-FILTER-570")
    //         //-- Previous code of Single state & multi city was same as was in it is commented for role == 7 and 15 -----
    //         console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-574")
    //         //var str = $scope.orgStructureFilter; //previously
    //         var str = $scope.orgStructureUptoStateFilter;
    //         var strOrgstruct = str.split('"')
    //         //console.log(strOrgstruct[1], "strorgstruct-578", strOrgstruct)

    //         $scope.strOrgstructFinal = [];
    //         for (var x = 0; x < strOrgstruct.length; x++) {
    //           if (x % 2 != 0) {
    //             $scope.strOrgstructFinal.push(strOrgstruct[x]);
    //           }
    //         }
    //         //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-586")

    //         // from here ---
    //         // $scope.count = 0;
    //         $scope.membersFinal = [];
    //         for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
    //           Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

    //             $scope.membersnew = membr;
    //             //console.log($scope.membersnew, "TICKET-DISPLAY-595")
    //             if ($scope.membersnew.length > 0) {
    //               angular.forEach($scope.membersnew, function (value, index) {
    //                 $scope.membersFinal.push(value);
    //               });
    //             }
    //             $scope.members = $scope.membersFinal;
    //             // console.log($scope.membersFinal, "$scope.membersFinal-602")
    //             // console.log($scope.membersFinal.length, "$scope.membersFinal-603")
    //             // console.log($scope.members, "$scope.members-604")

    //             angular.forEach($scope.members, function (data, index) {
    //               data.index = index + 1;

    //               Restangular.all('categories?filter[where][deleteFlag]=false').getList().then(function (categories) {
    //                 $scope.categoryname = categories;
    //                 for (var j = 0; j < $scope.categoryname.length; j++) {
    //                   if (data.categoryId == $scope.categoryname[j].id) {
    //                     data.categoryname = $scope.categoryname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('subcategories?filter[where][deleteFlag]=false').getList().then(function (subcategories) {
    //                 $scope.subcategoryname = subcategories;
    //                 for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //                   if (data.subcategoryId == $scope.subcategoryname[j].id) {
    //                     data.subcategoryname = $scope.subcategoryname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
    //                 $scope.cityname = cities;
    //                 for (var j = 0; j < $scope.cityname.length; j++) {
    //                   if (data.cityId == $scope.cityname[j].id) {
    //                     data.cityname = $scope.cityname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
    //                 $scope.pincodename = pincodes;
    //                 for (var j = 0; j < $scope.pincodename.length; j++) {
    //                   if (data.pincodeId == $scope.pincodename[j].id) {
    //                     data.pincodename = $scope.pincodename[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
    //                 $scope.statusname = statuses;
    //                 for (var j = 0; j < $scope.statusname.length; j++) {
    //                   if (data.statusId == $scope.statusname[j].id) {
    //                     data.statusname = $scope.statusname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               $scope.exportArray.push(data);

    //             });
    //           });
    //         }
    //     } else if (window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
    //       var str = $scope.orgStructureFilter;
    //         var strOrgstruct = str.split('"');
    //         //console.log(strOrgstruct[1], "strorgstruct-638", strOrgstruct)
    //         $scope.strOrgstructFinal = [];
    //         for (var x = 0; x < strOrgstruct.length; x++) {
    //           if (x % 2 != 0) {
    //             $scope.strOrgstructFinal.push(strOrgstruct[x]);
    //           }
    //         }
    //         //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-651")

    //         // $scope.count = 0;
    //         $scope.membersFinal = [];
    //         for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
    //           Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][completedFlag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {

    //             $scope.membersnew = membr;
    //             //console.log($scope.membersnew, "TICKET-DISPLAY-662")
    //             if ($scope.membersnew.length > 0) {
    //               angular.forEach($scope.membersnew, function (value, index) {
    //                 $scope.membersFinal.push(value);
    //               });
    //             }
    //             $scope.members = $scope.membersFinal;
    //             // console.log($scope.membersFinal, "$scope.membersFinal-669")
    //             // console.log($scope.membersFinal.length, "$scope.membersFinal-670")
    //             // console.log($scope.members, "$scope.members-760")

    //             angular.forEach($scope.members, function (data, index) {
    //               data.index = index + 1;

    //               Restangular.all('categories?filter[where][deleteFlag]=false').getList().then(function (categories) {
    //                 $scope.categoryname = categories;
    //                 for (var j = 0; j < $scope.categoryname.length; j++) {
    //                   if (data.categoryId == $scope.categoryname[j].id) {
    //                     data.categoryname = $scope.categoryname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('subcategories?filter[where][deleteFlag]=false').getList().then(function (subcategories) {
    //                 $scope.subcategoryname = subcategories;
    //                 for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //                   if (data.subcategoryId == $scope.subcategoryname[j].id) {
    //                     data.subcategoryname = $scope.subcategoryname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
    //                 $scope.cityname = cities;
    //                 for (var j = 0; j < $scope.cityname.length; j++) {
    //                   if (data.cityId == $scope.cityname[j].id) {
    //                     data.cityname = $scope.cityname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
    //                 $scope.pincodename = pincodes;
    //                 for (var j = 0; j < $scope.pincodename.length; j++) {
    //                   if (data.pincodeId == $scope.pincodename[j].id) {
    //                     data.pincodename = $scope.pincodename[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
    //                 $scope.statusname = statuses;
    //                 for (var j = 0; j < $scope.statusname.length; j++) {
    //                   if (data.statusId == $scope.statusname[j].id) {
    //                     data.statusname = $scope.statusname[j].name;
    //                     break;
    //                   }
    //                 }
    //               });

    //               $scope.exportArray.push(data);
    //             });
    //           });
    //         }
    //     }

    //     /******************************Export Function */

    //     $scope.exportTicket = function () {
    //       console.log("inside export func");
    //       //$scope.exportArray = [];
    //       $scope.TotalData = [];
    //       $scope.valConCount = 0;

    //       if ($scope.exportArray.length == 0) {
    //         alert('No data found');
    //       } else {
    //         for (var arr = 0; arr < $scope.exportArray.length; arr++) {
    //           $scope.TotalData.push({
    //             'Sr No': $scope.exportArray[arr].index,
    //             'SER': $scope.exportArray[arr].serno,
    //             'CUSTOMER NAME': $scope.exportArray[arr].customername,
    //             'CONTACT NO': $scope.exportArray[arr].contactno,
    //             'CATEGORY': $scope.exportArray[arr].categoryname,
    //             'SUBCATEGORY': $scope.exportArray[arr].subcategoryname,
    //             'CITY': $scope.exportArray[arr].cityname,
    //             'PINCODE': $scope.exportArray[arr].pincodename,
    //             'ADDRESS': $scope.exportArray[arr].customeraddress,
    //             'STATUS': $scope.exportArray[arr].statusname,
    //             'ETA': $scope.exportArray[arr].etatime,
    //             'START DATE/TIME': $scope.exportArray[arr].starttime,
    //             'END DATE/TIME': $scope.exportArray[arr].endtime,
    //             'LAST ACTION': $scope.exportArray[arr].lastmodifiedtime,


    //           });

    //           $scope.valConCount++;
    //           if ($scope.exportArray.length == $scope.valConCount) {
    //             alasql('SELECT * INTO XLSX("ticketlist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
    //           }
    //         }
    //       }
    //     };

    //     /************************************* Delete ***************************************************/

    //     $scope.Delete = function (id) {
    //       $scope.item = [{
    //         deleteflag: true
    //       }]
    //       // Restangular.one('members/' + id).customPUT($scope.item[0]).then(function () {
    //       Restangular.one('rcdetails/' + id).customPUT($scope.item[0]).then(function () {
    //         $route.reload();
    //       });
    //     };

    //     $scope.getgender = function (genderId) {
    //       return Restangular.one('genders', genderId).get().$object;
    //     };

    //     $scope.getcustomer = function (customerId) {
    //       return Restangular.one('customers', customerId).get().$object;
    //     };

    //     $scope.getcategory = function (categoryId) {
    //       return Restangular.one('categories', categoryId).get().$object;
    //     };

    //     $scope.subgetcategory = function (subcategoryId) {
    //       return Restangular.one('subcategories', subcategoryId).get().$object;
    //     };

    //     $scope.getcity = function (cityId) {
    //       return Restangular.one('organisationlocations', cityId).get().$object;
    //     };

    //     $scope.getpincode = function (pincodeId) {
    //       return Restangular.one('organisationlocations', pincodeId).get().$object;
    //     };

    //     $scope.getstatus = function (statusId) {
    //       return Restangular.one('todostatuses', statusId).get().$object;
    //     };
    //     // $scope.getoccupation = function (occupationId) {
    //     //   return Restangular.one('occupations', occupationId).get().$object;
    //     // };

    //     // $scope.geteducation = function (educationId) {
    //     //   return Restangular.one('educations', educationId).get().$object;
    //     // };


    //     $scope.sort = {
    //       active: '',
    //       descending: undefined
    //     }

    //     $scope.changeSorting = function (column) {

    //       var sort = $scope.sort;

    //       if (sort.active == column) {
    //         sort.descending = !sort.descending;

    //       } else {
    //         sort.active = column;
    //         sort.descending = false;
    //       }
    //     };

    //     $scope.getIcon = function (column) {

    //       var sort = $scope.sort;

    //       if (sort.active == column) {
    //         return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
    //       }
    //     }

    $scope.navigateToEdit = function (id) {
      window.location = '/formlevel/edit/' + id;
    };

  });