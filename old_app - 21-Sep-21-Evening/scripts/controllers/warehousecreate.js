'use strict';
angular.module('secondarySalesApp')
  .controller('WarehouseCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    /*********/

    //console.log("$routeParams.id-5", $routeParams.id)
    $scope.showForm = function () {
      var visible = $location.path() === '/warehousecreate' || $location.path() === '/warehousecreate/edit/' + $routeParams.id;
      return visible;
    };

    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/warehousecreate';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/warehousecreate';
      return visible;
    };

    if ($location.path() === '/warehousecreate') {
      $scope.disableDropdown = false;
    } else {
      $scope.disableDropdown = true;
    }
    /************************************************************************************/
    $scope.warehouse = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteflag: false
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/warehousecreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    // ******************************** EOF Pagination *************************************************

    //------------------------------- Cust-Catg-Subcatg-------------------------
    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });

    $scope.$watch('warehouse.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('warehouse.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    //------------------------------- end of Cust-Catg-Subcatg-------------------------

    //   Restangular.all('warehousetypes').getList().then(function (cust) {
    //     $scope.customers = cust;
    //   });

    Restangular.all('warehousetypes').getList().then(function (wtypes) {
      $scope.warehousetypes = wtypes;
    });

    Restangular.all('states').getList().then(function (ste) {
      $scope.states = ste;
      if ($scope.warehouse.stateId) {
        $scope.warehouse.stateId = $scope.warehouse.stateId;
      }
    });

    $scope.$watch('warehouse.stateId', function (newValue, oldValue) {
      if (newValue == oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('cities?filter[where][stateId]=' + newValue).getList().then(function (cty) {
          $scope.cities = cty;
          if ($scope.warehouse.cityId) {
            $scope.warehouse.cityId = $scope.warehouse.cityId;
          }
        });
      }
    });

    //----------- for Role warehouse ie. for roleId - 34 we need stateid so that we can have populate all cities in location dropdown 
    //----------- that we are selecting while creating user for this roleId. 
    Restangular.all('users?filter[where][deleteflag]=false&filter[where][roleId]=' + 34).getList().then(function (usr) {
      $scope.users = usr;
      console.log("$scope.users-106", $scope.users)
      $scope.orgstruct = $scope.users[0].orgStructure;
      console.log("$scope.users-108", $scope.orgstruct)
      var orgstr = $scope.orgstruct;
      var strorgval = orgstr.split(";");
      var cityval = strorgval[1].split("-");
      console.log("cityval-112", cityval[1])
      Restangular.all('organisationlocations?filter[where][parent]=' + cityval[1]).getList().then(function (locs) {
        $scope.locations = locs;
        if ($scope.warehouse.site) {
          $scope.warehouse.site = $scope.warehouse.site;
        }
      });
    });


    /****************************************** CREATE *********************************/
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.Save = function () {
      document.getElementById('name').style.border = "";
      // document.getElementById('salescode').style.border = "";
      document.getElementById('email').style.border = "";
      document.getElementById('mobile').style.border = "";

      $scope.warehouse.salescode = $scope.warehouse.warehousecode;

      if ($scope.warehouse.customerId == '' || $scope.warehouse.customerId == null) {
        $scope.warehouse.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a Customer';
        //       document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.categoryId == '' || $scope.warehouse.categoryId == null) {
        $scope.warehouse.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a Category';
        //       document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.subcategoryId == '' || $scope.warehouse.subcategoryId == null) {
        $scope.warehouse.subcategoryId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a Subcategory';
        //       document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.warehousecode == '' || $scope.warehouse.warehousecode == null) {
        $scope.warehouse.warehousecode = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a Warehouse Code';
        //       document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.warehousename == '' || $scope.warehouse.warehousename == null) {
        $scope.warehouse.warehousename = null;
        $scope.validatestring = $scope.validatestring + 'Plese Enter WareHouse Name';
        document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.warehousetypeId == '' || $scope.warehouse.warehousetypeId == null) {
        $scope.warehouse.warehousetypeId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a WareHouse Type';
        //     document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.email == '' || $scope.warehouse.email == null) {
        $scope.validatestring = $scope.validatestring + 'Plese Enter Email Address';
        document.getElementById('email').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.stateId == '' || $scope.warehouse.stateId == null) {
        $scope.warehouse.stateId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Select a State';
        //     document.getElementById('name').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.cityId == '' || $scope.warehouse.cityId == null) {
        $scope.warehouse.cityId = null;
        $scope.validatestring = $scope.validatestring + 'Plese Enter City';
        //   document.getElementById('salescode').style.border = "1px solid #ff0000";
      } else if ($scope.warehouse.mobile == '' || $scope.warehouse.mobile == null) {
        $scope.validatestring = $scope.validatestring + 'Plese Enter Mobile Number';
        document.getElementById('mobile').style.border = "1px solid #ff0000";
      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {
        Restangular.all('warehouses').post($scope.warehouse).then(function () {
          //  console.log('warehouses Saved');
          window.location = '/warehouse';
        }, function (response) {
          alert(response.data.error.detail);
          console.error(response);
        });
      }
    };

    /***************************** UPDATE *******************************************/

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Update = function () {

    document.getElementById('name').style.border = "";
    //console.log($scope.itemcategory, "message-323")
    document.getElementById('name').style.border = "";
    // document.getElementById('salescode').style.border = "";
    document.getElementById('email').style.border = "";
    document.getElementById('mobile').style.border = "";

    $scope.warehouse.salescode = $scope.warehouse.warehousecode;

    if ($scope.warehouse.customerId == '' || $scope.warehouse.customerId == null) {
      $scope.warehouse.customerId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a Customer';
      //       document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.categoryId == '' || $scope.warehouse.categoryId == null) {
      $scope.warehouse.categoryId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a Category';
      //       document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.subcategoryId == '' || $scope.warehouse.subcategoryId == null) {
      $scope.warehouse.subcategoryId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a Subcategory';
      //       document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.warehousecode == '' || $scope.warehouse.warehousecode == null) {
      $scope.warehouse.warehousecode = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a Warehouse Code';
      //       document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.warehousename == '' || $scope.warehouse.warehousename == null) {
      $scope.warehouse.warehousename = null;
      $scope.validatestring = $scope.validatestring + 'Plese Enter WareHouse Name';
      document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.warehousetypeId == '' || $scope.warehouse.warehousetypeId == null) {
      $scope.warehouse.warehousetypeId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a WareHouse Type';
      //     document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.email == '' || $scope.warehouse.email == null) {
      $scope.validatestring = $scope.validatestring + 'Plese Enter Email Address';
      document.getElementById('email').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.stateId == '' || $scope.warehouse.stateId == null) {
      $scope.warehouse.stateId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Select a State';
      //     document.getElementById('name').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.cityId == '' || $scope.warehouse.cityId == null) {
      $scope.warehouse.cityId = null;
      $scope.validatestring = $scope.validatestring + 'Plese Enter City';
      //   document.getElementById('salescode').style.border = "1px solid #ff0000";
    } else if ($scope.warehouse.mobile == '' || $scope.warehouse.mobile == null) {
      $scope.validatestring = $scope.validatestring + 'Plese Enter Mobile Number';
      document.getElementById('mobile').style.border = "1px solid #ff0000";
    }
    if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        }
    else {
        $scope.message = 'Warehouse has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;
        Restangular.one('warehouses/' + $routeParams.id).customPUT($scope.warehouse).then(function () {
            window.location = '/warehouse';
            
        });
      }
    };

    if ($routeParams.id) {
      console.log("$routeParams.id", $routeParams.id)
      Restangular.one('warehouses', $routeParams.id).get().then(function (whrs) {
        $scope.original = whrs;
        $scope.warehouse = Restangular.copy($scope.original);
      });
    }

  });