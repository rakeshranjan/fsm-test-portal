'use strict';

angular.module('secondarySalesApp')
  .controller('FormLevelSummaryCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout) {

    console.log("Form Level Summary");
    
    $scope.disableLocation = false;
    $scope.submitDisable = true;

    if (window.sessionStorage.roleId == 1) {
      $scope.hideMenu = true;
    } else {
      $scope.hideMenu = false;
    }

    //console.log($location.path(), $location.absUrl(), $location.url(), "CURRENT LOCATION - 15")
    // window.sessionStorage.currentLocation = $location.url();
    //window.sessionStorage.currentLocation = $location.path();

    console.log(window.sessionStorage.currenURLParamId, "currenURLParamId-20")
    $routeParams.id = window.sessionStorage.currenURLParamId;
    window.sessionStorage.LastLocation = '/formlevelsummary/edit/' + window.sessionStorage.currenURLParamId;
    //window.sessionStorage.currenURLParamId =  $routeParams.id;
    // if(window.sessionStorage.LastLocation != null) {
    // if(window.sessionStorage.currenURLParamId != null) {
    //   console.log("here-19")
     console.log($routeParams.id, "$routeParams.id-20")
     //window.location = '/formlevelsummary/edit/' + window.sessionStorage.currenURLParamId;
     //window.sessionStorage.LastLocation = $location.absUrl();
    //}
    
    $scope.cancelClick = function () {
      //window.sessionStorage.currenURLParamId = null;
      if (window.sessionStorage.prviousLocation == "partials/serviceorder") {
        window.location = '/serviceorder';
      } else {
        //window.location = '/tickets';
          if (window.sessionStorage.roleId == 1) {
            window.location = '/formleveldetailslist';
          } else {
            // if(window.sessionStorage.previousLocationCarried == "partials/serviceorder"){
              if (window.sessionStorage.carryCount > 0) {
              window.location = '/serviceorder';
            } else {
              window.location = '/tickets';
            }
            // window.location = '/tickets';
          }
      }
    };

    Restangular.one('rcdetailslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
      $scope.rcdetailLang = langResponse[0];
      if (window.sessionStorage.roleId == 1) {
        $scope.hideMenu = true;
      } else {
        $scope.hideMenu = false;
      }
    });

    $scope.showForm = function () {
      var visible = $location.path() === '/formlevel/create' || $location.path() === '/formlevel/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/formlevel/create';
        return visible;
      }
    };

    
    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });

    $scope.$watch('langrcdetail.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('langrcdetail.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });
  

    $scope.modalTitle = 'Thank You';
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';

    
    console.log("In onetoone at 84")
    // Restangular.one('members', $routeParams.id).get().then(function (mem) {
        Restangular.one('rcdetails', $routeParams.id).get().then(function (mem) {
        // $scope.memberName = mem.fullname;
        $scope.memberName = mem.serno;
        //$scope.memberName = mem.customername;
        //$scope.memberId = mem.memberId; No data/Not using
        $scope.customerId = mem.customerId;
        $scope.categoryId = mem.categoryId;
        $scope.subcategoryId = mem.subcategoryId;
        $scope.memberOrgStructure = mem.orgstructure;
        console.log($scope.memberName,$scope.customerId,$scope.categoryId, $scope.subcategoryId, $scope.memberOrgStructure,"DIFFERENT PARAMS")
    });


    $scope.onetoonelanguage = Restangular.one('onetoonelanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;


    $scope.onetoone = {
      memberid: $routeParams.id,
      date: new Date(),
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      answer: null
  };

    
      $scope.HideCreateButton = false;
      $scope.langdisable = true;
      $scope.disableLocation = true;
     
      Restangular.one('rcdetails', $routeParams.id).get().then(function (member) {
        $scope.original = member;
        $scope.langrcdetail = Restangular.copy($scope.original);
             console.log('$scope.todo', $scope.langrcdetail);

            //  Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.langrcdetail.rcCode).get().then(function (questwithcode){
            //    $scope.questWithCode = questwithcode;
            //   $scope.allInfo = function (data) {
            //     return data.question + '-' + data.rcCode;
            // }
        if (member.answeredQuestion == null) {
            member.answeredQuestion = 0;
        }
        console.log("In onetoone at 128")
        Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][memberid]=' + $routeParams.id + '&filter[where][todostatusid]=1').getList().then(function (opntodos) {
            if (opntodos.length > 0) {
               // console.log($routeParams.id, opntodos, "Before going to todos")
                window.location = '/todos';
            } else {
                Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId).getList().then(function (ticketquestotal) {
                    $scope.totquest = ticketquestotal;
                    console.log($scope.totquest, $scope.totquest.length,"For-Total-Question-136")
                Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][memberid]=' + $routeParams.id).getList().then(function (sqas) {
                    $scope.surveyanswers = sqas;
                    console.log($scope.surveyanswers, "$scope.surveyanswers-145")
                    console.log(member.answeredQuestion, "member.answeredQuestion-146")
                    var count = 0;
                    $scope.myArray = [];
                    $scope.myansoptionArray = [];
                    $scope.mynewansoptionArray = [];
                    console.log($scope.surveyanswers.length, "$scope.surveyanswers.length")
                    if ($scope.surveyanswers.length == 0) {
                        console.log("here-140")
                        // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
                            // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
                            Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqs) {
                                console.log(sqs,"SQS-144")
                            if (sqs.length == 0) {
                                //console.log("here-111")
                                $scope.member = {
                                    id: $routeParams.id,
                                    answeredQuestion: 0,
                                    etatime: $scope.langrcdetail.etadate
                                    //completedFlag: true
                                };
                                // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                                    Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                                    // window.location = '/todos';
                                });
                            }

                            ///////////////////Progress Calculation//////////////////////////////
                            var maxSl = sqs[sqs.length - 1].serialno;
                            var currentSl = member.answeredQuestion;
                            $scope.progressPercentage = ((currentSl / maxSl) * 100).toFixed(0);
                            ///////////////////Progress Calculation//////////////////////////////

                            $scope.surveyquestions = sqs;
                            $scope.healthsurveyquestion = $scope.surveyquestions[0];
                            $scope.myansoptionArray = $scope.surveyquestions[0].answeroptions.split(',');
                            console.log($scope.myansoptionArray, "myansoptionArray-DECLARED-ARRAY-205")

                            //Finally to bring in array commented below -
                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
                            // console.log($scope.healthsurveyquestion.answeroptionsarray, "ARRAY-176")
                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.myansoptionArray;//working

                            for(var k = 0; k < $scope.myansoptionArray.length; k++){
                                Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.myansoptionArray[k]).get().then(function (relatedquest){
                                    $scope.drpvalue = relatedquest.question + "-" + relatedquest.rcCode;
                                    // $scope.drpvalue = relatedquest.rcCode;
                                    console.log($scope.drpvalue, "$scope.drpvalue-237")
                                    $scope.mynewansoptionArray.push($scope.drpvalue);
                                });
                                
                            }
                            console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-221")
                            $scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;
                            // $scope.mynewansoptionArray = [];
                            // angular.forEach($scope.myansoptionArray, function (data) {
                            //     //if (data != '' || data != undefined) {
                            //        // data = data.replace(/\s/g, "");

                            //        $scope.mynewansoptionArray.push({data
                            //            // value: data
                            //             //visible: true,
                            //             //answer: null
                            //         });
                            //         // console.log($scope.myOptionsArray);
                            //     //}
                            // });
                            // console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-257")
                            //}
                            //console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-243")


                            $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
                            $scope.myOptions = [];
                            console.log($scope.healthsurveyquestion.question, "QTYPE-178")
                            if ($scope.healthsurveyquestion.question === '6') {
                                $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
                            }
                            else if ($scope.healthsurveyquestion.question === '11') {
                                $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
                            }
                            $scope.myOptionsArray = [];

                            angular.forEach($scope.myOptions, function (data) {
                                if (data != '' || data != undefined) {
                                    data = data.replace(/\s/g, "");
                                    $scope.myOptionsArray.push({
                                        value: data,
                                        visible: true,
                                        answer: null
                                    });
                                    // console.log($scope.myOptionsArray);
                                }
                            });
                        });
                    } else {
                        console.log("Inside else - 189-Also tot-Q-Lenth", $scope.totquest.length)
                        angular.forEach($scope.surveyanswers, function (val) {
                            $scope.myArray.push(val.questionid);
                            count++;
                            console.log($scope.myArray, "$scope.myArray-207")
                            console.log(count, $scope.surveyanswers.length, "Inside else - 208", $scope.customerId, $scope.categoryId)
                            if (count == $scope.surveyanswers.length) {
                                //console.log(count, $scope.surveyanswers.length, $scope.customerId, $scope.categoryId, "Inside else - 194")
                                // Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {
                                    // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {

                                    //Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno][gt]=' + member.answeredQuestion).getList().then(function (sqas) {
                                        //By Saty - removed '&filter[where][serialno][gt]=' + member.answeredQuestion for all questions for now ---
                                    Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId).getList().then(function (sqas) {
                                    //console.log($scope.customerId, $scope.categoryId, member.answeredQuestion, "customerId - categoryId - member.answeredQuestion")
                                    console.log("Tot-Survey-Question-Left-Now-Is: ",sqas.length)    
                                    if (sqas.length == 0) {
                                        console.log("Here at 200 - tot-Q : tot-A", $scope.totquest.length, $scope.surveyanswers.length)
                                        if($scope.totquest.length == $scope.surveyanswers.length){
                                        $scope.member = {
                                            id: $routeParams.id,
                                            answeredQuestion: 0,
                                            completedFlag: true,
                                            statusId: 5,
                                            etatime: $scope.langrcdetail.etadate
                                        };
                                        // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                                            Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                                            window.location = '/';
                                             //window.location = '/todos';
                                        });
                                        } else {
                                            $scope.member = {
                                                id: $routeParams.id,
                                                fieldassignFlag: true,
                                                etatime: $scope.langrcdetail.etadate
                                                //statusId: 3
                                            };
                                            console.log("ELSE of sqas.length - 225")
                                            // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                                            Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                                            
                                            console.log("No more web questions for now....")
                                            window.location = '/';
                                            });
                                        }
                                    }

                                    ///////////////////Progress Calculation//////////////////////////////
                                    var maxSl = sqas[sqas.length - 1].serialno;
                                    var currentSl = member.answeredQuestion;
                                    $scope.progressPercentage = ((currentSl / maxSl) * 100).toFixed(0);
                                    ///////////////////Progress Calculation//////////////////////////////
                                    
                                    $scope.surveyquestions = sqas;
                                    // console.log($scope.surveyquestions,"SURVEY QUESTION-252")
                                    // console.log($scope.surveyquestions[0],"SURVEY QUESTION-254")//----wrong flow
                                    // console.log(member.rcCode, "LAST QUESTION RELATED RC CODE")

                                   //$scope.myansoptionArray = $scope.surveyquestions[0].answeroptions.split(',');//----wrong flow
                                    //console.log($scope.myansoptionArray, "myansoptionArray-DECLARED-ARRAY-322")

                            //Finally to bring in array commented below -
                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
                            // console.log($scope.healthsurveyquestion.answeroptionsarray, "ARRAY-176")
                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.myansoptionArray;//working
                          

                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;

                           // console.log($scope.mynewansoptionArray, "$scope.mynewansoptionArray-339")//----wrong flow
                                   
                                    if(member.nextquestionId > 0) {
                                    
                                    // console.log($scope.surveyquestions, "$scope.surveyquestions-359")
                                    // console.log(member.nextquestionId, "member.nextquestionId-360")

                                    for(var j = 0; j < $scope.surveyquestions.length; j++) {
                                        if($scope.surveyquestions[j].serialno == member.nextquestionId){
                                            console.log("Matched-363")
                                            $scope.healthsurveyquestion = $scope.surveyquestions[j];
                                            //commented below one line to bring the dropdown rcCode alongwith questions
                                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[j].answeroptions.split(',');
                                            //$scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;

                                            //--- new binding
                                            $scope.myansoptionArray = $scope.surveyquestions[j].answeroptions.split(',');
                                            console.log($scope.myansoptionArray,"371")
                                            for(var k = 0; k < $scope.myansoptionArray.length; k++){
                                                Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.myansoptionArray[k]).get().then(function (relatedquest){
                                                $scope.drpvalue = relatedquest.question + "-" + relatedquest.rcCode;
                                                // $scope.drpvalue = relatedquest.rcCode;
                                                console.log($scope.drpvalue, "$scope.drpvalue-375")
                                                $scope.mynewansoptionArray.push($scope.drpvalue);
                                                });
                                            }
                                            $scope.healthsurveyquestion.answeroptionsarray = $scope.mynewansoptionArray;
                                        }
                                        else if(j == $scope.surveyquestions.length){
                                            window.location = '/';
                                        }
                                    }

                                    // $scope.healthsurveyquestion = $scope.surveyquestions[0];
                                    // $scope.healthsurveyquestion.answeroptionsarray = $scope.surveyquestions[0].answeroptions.split(',');
                                    // console.log($scope.healthsurveyquestion, "$scope.healthsurveyquestion----276")
                                    // console.log($scope.healthsurveyquestion.id,"ID-276")

                                    $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
                                    $scope.myOptions = $scope.healthsurveyquestion.answeroptions.split(',');
                                    $scope.myOptionsArray = [];

                                    angular.forEach($scope.myOptions, function (data) {
                                        if (data != '' || data != undefined) {
                                            data = data.replace(/\s/g, "");
                                            $scope.myOptionsArray.push({
                                                value: data,
                                                visible: true,
                                                answer: null
                                            });
                                            // console.log($scope.myOptionsArray);
                                        }
                                    });
                                  } else {
                                    window.location = '/';
                                  }
                                });
                            }
                        });
                    }
                });
            });
            }//--
        });

      // });
    });

    $scope.validatestring = "";
    $scope.todoToCreateArray = [];

    $scope.Submit = function () {

        $scope.message = 'Answer has been saved!';
        $scope.modalTitle = 'Thank you!!!';

        $scope.answer = "";
        if ($scope.healthsurveyquestion.questiontype == 2) {
            angular.forEach($scope.myOptionsArray, function (data) {
                if (data.answer == true) {
                    if ($scope.answer == "") {
                        $scope.answer = data.value;
                    } else {
                        $scope.answer = $scope.answer + data.value;
                    }
                }
            });
            $scope.onetoone.answer = $scope.answer;
        }

        if ($scope.onetoone.answer == '' || $scope.onetoone.answer == null) {
            $scope.validatestring = $scope.validatestring + 'Please Select Answer';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        } else {

            // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.onetoone.questionid = $scope.healthsurveyquestion.id;
            //added by me
            $scope.onetoone.deleteflag = false;
            // console.log($scope.onetoone.answer, "THE ANSWER-434")
            // console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "SELECTED RC CODE, selectedRCcodeToPush[0]-435")
            // console.log($scope.healthsurveyquestion.serialno, "serialno-436")
            // console.log($scope.healthsurveyquestion.questiontype, "questiontype-437")
            // console.log($scope.selectedSerialNoToPush, $scope.selectedSerialNoToPush[0], "Outside-Pushed-Selected-serialno-341")
            if($scope.selectedSerialNoToPush[0] == undefined){
                $scope.selectedSerialNoToPush.push(0);
               console.log($scope.selectedSerialNoToPush[0],"VALUE-NOW-440")
            }
        
            Restangular.all('surveyanswers').post($scope.onetoone).then(function (resp) {


                if ($scope.healthsurveyquestion.skipquestion === true && $scope.onetoone.answer.toString().split(",").indexOf($scope.healthsurveyquestion.skipquestionvalue) > -1) {
                    // Restangular.one('surveyquestions', $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
                        console.log("IF-448")
                    Restangular.one('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId, $scope.healthsurveyquestion.skipquestionid).get().then(function (skpqustn) {
                        for (var i = 0; i < $scope.todotypes.length; i++) {
                            if ($scope.todotypes[i].toInitial === true) {
                                $scope.member = {
                                    id: $routeParams.id,
                                    answeredQuestion: 0,
                                    assignedto: 0,
                                    etatime: $scope.langrcdetail.etadate
                                };
                                break;
                            } else {
                                $scope.member = {
                                    id: $routeParams.id,
                                    answeredQuestion: +skpqustn.serialno - 1,
                                    assignedto: 0,
                                    rcCode: skpqustn.rcCode,
                                    etatime: $scope.langrcdetail.etadate
                                };
                            }
                        }
                        if ($scope.todotypes.length == 0) {
                            $scope.member = {
                                id: $routeParams.id,
                                answeredQuestion: +skpqustn.serialno - 1,
                               // statusId: 7//here statusId: 2 not working
                               statusId: 2,
                               fieldassignFlag: true,
                               rcCode: skpqustn.rcCode,
                               etatime: $scope.langrcdetail.etadate
                            };
                        }
                        // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                            console.log("HERE at 478")
                            Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {

                            ////////////////////////////////TODO///////////////////////////////////////
                            if ($scope.healthsurveyquestion.applytomessage == 'yes') {
                                for (var i = 0; i < $scope.todotypes.length; i++) {
                                    var dueDate = parseInt($scope.todotypes[i].due);
                                    var fifteendays = new Date();
                                    fifteendays.setDate(fifteendays.getDate() + dueDate);
                                    $scope.todoToCreate = {
                                            "memberid": $routeParams.id,
                                            "todotypeid": $scope.todotypes[i].id,
                                            "todostatusid": 1,
                                            "followupdate": new Date(),
                                            "assignedto": 0,
                                            "deleteflag": false,
                                            "lastmodifiedby": $window.sessionStorage.userId,
                                            "lastmodifiedrole": $window.sessionStorage.roleId,
                                            "lastmodifiedtime": new Date(),
                                            "createdby": $window.sessionStorage.userId,
                                            "createdtime": new Date(),
                                            "createdrole": $window.sessionStorage.roleId,
                                            "orgstructure": $scope.memberOrgStructure,
                                            "remarks": $scope.todotypes[i].remarks
                                        }
                                        //                            Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                                        //                                $route.reload();
                                        //                            });
                                    $scope.todoToCreateArray.push($scope.todoToCreate);

                                }
                                $scope.saveTodos();
                            } else {
                                if($scope.healthsurveyquestion.questiontype == 9){
                                    $scope.uploader.uploadAll();
                                }else{
                                    $route.reload();
                                }
                            }


                            //////////////////////////////////TODO///////////////////////

                        });
                    });
                } else {
                    console.log("ELSE-524")
                    for (var i = 0; i < $scope.todotypes.length; i++) {
                        if ($scope.todotypes[i].toInitial === true) {
                            $scope.member = {
                                id: $routeParams.id,
                                answeredQuestion: 0,
                                etatime: $scope.langrcdetail.etadate
                            };
                            break;
                        } else {
                            //if($scope.healthsurveyquestion.questiontype == 11){- $scope.selectedRCcodeToPush[0]
                            $scope.member = {
                                id: $routeParams.id,
                                answeredQuestion: $scope.healthsurveyquestion.serialno,
                                // rcCode: $scope.healthsurveyquestion.rcCode,
                                rcCode: $scope.selectedRCcodeToPush[0],
                                nextquestionId: $scope.selectedSerialNoToPush[0],
                                etatime: $scope.langrcdetail.etadate
                                //rcCode: $scope.healthsurveyquestion.rcCode//$scope.selectedSerialNoToPush[0]
                            };
                        }
                    }
                    if ($scope.todotypes.length == 0) {
                        $scope.member = {
                            id: $routeParams.id,
                            answeredQuestion: $scope.healthsurveyquestion.serialno,
                            //rcCode: $scope.healthsurveyquestion.rcCode,
                            rcCode: $scope.selectedRCcodeToPush[0],
                            nextquestionId: $scope.selectedSerialNoToPush[0],
                            //statusId: 5,
                           statusId: 2,
                           fieldassignFlag: true,
                           etatime: $scope.langrcdetail.etadate
                           //rcCode: $scope.healthsurveyquestion.rcCode
                        };
                    }
                    // Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function () {
                        console.log("HERE at 555")
                        if($scope.selectedSerialNoToPush[0] == 0){
                            $scope.member = {
                                id: $routeParams.id,
                                statusId: 5,
                                nextquestionId: 0,
                                etatime: $scope.langrcdetail.etadate
                            };
                        }
                        console.log($scope.member.statusId, "STATUS-ID-563")
                        Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function () {
                        ////////////////////////////////TODO///////////////////////////////////////
                        $scope.myansoptionArray = [];
                        $scope.mynewansoptionArray = [];
                        $scope.selectedRCcodeToPush = [];
                        $scope.selectedSerialNoToPush = [];
                        console.log($scope.selectedSerialNoToPush[0],"VALUE-NOW-567")
                        if ($scope.healthsurveyquestion.applytomessage == 'yes') {

                            for (var i = 0; i < $scope.todotypes.length; i++) {
                                var dueDate = parseInt($scope.todotypes[i].due);
                                var fifteendays = new Date();
                                fifteendays.setDate(fifteendays.getDate() + dueDate);
                                $scope.todoToCreate = {
                                        "memberid": $routeParams.id,
                                        "todotypeid": $scope.todotypes[i].id,
                                        "todostatusid": 1,
                                        "followupdate": new Date(),
                                        "assignedto": 0,
                                        "deleteflag": false,
                                        "lastmodifiedby": $window.sessionStorage.userId,
                                        "lastmodifiedrole": $window.sessionStorage.roleId,
                                        "lastmodifiedtime": new Date(),
                                        "createdby": $window.sessionStorage.userId,
                                        "createdtime": new Date(),
                                        "createdrole": $window.sessionStorage.roleId,
                                        "orgstructure": $scope.memberOrgStructure,
                                        "remarks": $scope.todotypes[i].remarks
                                    }
                                    //                            Restangular.all('todos').post($scope.todoToCreate).then(function (todoresp) {
                                    //                                $route.reload();
                                    //                            });
                                $scope.todoToCreateArray.push($scope.todoToCreate);

                            }
                            $scope.saveTodos();
                        } else {
                           if($scope.healthsurveyquestion.questiontype == 9){
                                    $scope.uploader.uploadAll();
                                }else{
                                    $route.reload();
                                }
                        }


                        //////////////////////////////////TODO///////////////////////
                    });
                }
            });

        }
    };
    $scope.saveTodoCount = 0;
    $scope.saveTodos = function () {
        Restangular.all('todos').post($scope.todoToCreateArray[$scope.saveTodoCount]).then(function (todoresp) {
            $scope.saveTodoCount++;
            if ($scope.saveTodoCount < $scope.todoToCreateArray.length) {
                $scope.saveTodos();
            } else {
               if($scope.healthsurveyquestion.questiontype == 9){
                                    $scope.uploader.uploadAll();
                                }else{
                                    $route.reload();
                                }
            }
        });
    }
  /////////////////////////////////TODO DROPDOWN////////////////////////////
  $scope.todoToPush = [];
    $scope.todotypes = [];
    $scope.selectedRCcodeToPush = [];
    $scope.selectedSerialNoToPush = [];
    /////////////////////////////////TODO DROPDOWN////////////////////////////
    $scope.$watch('onetoone.answer', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
          return;
      } else {
          console.log(newValue.toString(), newValue, "NewVALUE-696");
          console.log($scope.healthsurveyquestion.questiontype, "$scope.healthsurveyquestion.questiontype-697")
          if ($scope.healthsurveyquestion.questiontype == 6) {
              $scope.selectedTodoTypes = "";
              $scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
              $scope.answerArray = newValue.toString().split(";");
              for (var i = 0; i < $scope.todoValueArray.length; i++) {
                  for (var j = 0; j < $scope.answerArray.length; j++) {
                      if ($scope.todoValueArray[i].split("-")[0] == $scope.answerArray[j]) {
                          if ($scope.selectedTodoTypes == "") {
                              $scope.selectedTodoTypes = $scope.todoValueArray[i].split("-")[1];
                          } else {
                              $scope.selectedTodoTypes = $scope.selectedTodoTypes + "," + $scope.todoValueArray[i].split("-")[1];
                          }
                      }
                  }
              }
              Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
                  $scope.todotypes = todos
              });
          }
          else if ($scope.healthsurveyquestion.questiontype == 11) {
              //$scope.selectedTodoTypes = "";
              $scope.selectedRCcodeToPush = [];
              $scope.selectedSerialNoToPush = [];
              $scope.selectedRcCode = "";
              //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
              $scope.selectedRcCode = newValue.toString().split("-")[1];
              //$scope.selectedRcCode = $scope.healthsurveyquestion.rcCode;
              $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
              console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0],"SELECTED RC CODE-722")
              //console.log($scope.healthsurveyquestion.serialno, "serialno-723")

              $scope.selectedSerialNo = "";
              // Restangular.one('surveyquestions/findOne?filter[where][question]=' + newValue).get().then(function (surveyquest) {
              //Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + newValue).get().then(function (surveyquest) {
              Restangular.one('surveyquestions/findOne?filter[where][rcCode]=' + $scope.selectedRcCode).get().then(function (surveyquest) {
              $scope.selectedSerialNo = surveyquest.serialno;
              $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
              console.log($scope.selectedSerialNo, "Selected-serialno-731")
              });
              console.log($scope.selectedSerialNoToPush, "Outside-Pushed-Selected-serialno-733")
              // Restangular.all('todotypes?filter={"where":{"and":[{"id":{"inq":[' + $scope.selectedTodoTypes + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (todos) {
              //     $scope.todotypes = todos
              // });
          }
          else if ($scope.healthsurveyquestion.questiontype == 1){
              //provide from here $scope.selectedSerialNoToPush.push($scope.selectedSerialNo);
              //here itself increase with 1 the whatever serialno is
              //no need to define there as per question type
              //text value as $scope.selectedRcCode
              //---- there is need to implement $scope.selectedRCcodeToPush just like $scope.selectedSerialNoToPush
              $scope.selectedRCcodeToPush = [];
              $scope.selectedSerialNoToPush = [];
              $scope.selectedRcCode = "";
              $scope.selectedSerialNo = "";
              //$scope.todoValueArray = $scope.healthsurveyquestion.applytodovalue.split(";");
              $scope.selectedRcCode = newValue.toString();
              $scope.selectedRCcodeToPush.push($scope.selectedRcCode);
              console.log($scope.selectedRcCode, $scope.selectedRCcodeToPush[0], "$scope.selectedRcCode-766")///--- to send
              $scope.selectedSerialNo = $scope.healthsurveyquestion.serialno;
              console.log($scope.selectedSerialNo, "$scope.selectedSerialNo-768")
              $scope.selectedSerialNoToPush.push($scope.selectedSerialNo+1);
              //try to find whether for this increased serialno & related customer, category and subcategory is there any
              //record in the table - if no then push 0 in this array $scope.selectedSerialNoToPush.push(0)
              console.log($scope.selectedSerialNoToPush[0], "Next-serialno-772")
              console.log("Here at - 772", $scope.customerId, $scope.categoryId, $scope.subcategoryId)
              Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][fieldFlag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + $scope.subcategoryId + '&filter[where][serialno]=' + $scope.selectedSerialNoToPush[0]).getList().then(function (squestnext){
                  $scope.nextquestion = squestnext;
                  $scope.selectedSerialNoToPush = [];
                  console.log($scope.nextquestion, $scope.nextquestion.length, "775")
                  if($scope.nextquestion.length == 0){
                      $scope.selectedSerialNoToPush.push($scope.nextquestion.length);
                      console.log($scope.selectedSerialNoToPush[0], "Next SR No.-778")
                  }
              });

              console.log($scope.selectedSerialNoToPush[0], "Next-serialno-782")///--- to send
              //Also the below need to handle properly above post because text entry
              // if($scope.selectedSerialNoToPush[0] == undefined){
              //     $scope.selectedSerialNoToPush.push(0);
              //    console.log($scope.selectedSerialNoToPush[0],"VALUE-NOW-440")
              // }
              //need to re-define the ticket closing.
          }
      }
  });

    $scope.$watch('onetoone.todoid', function (newValue, oldValue) {
        if (newValue == '' || newValue == undefined || newValue == null) {
            return;
        } else {
            Restangular.one('todotypes', newValue).get().then(function (todo) {
                $scope.todoDueDays = todo.due;
            });
        }
    });
    /////////////////////////////////TODO DROPDOWN////////////////////////////
// Date & Time Setting
$scope.$parent.etadate = new Date();
$scope.dtmax = new Date();
$scope.toggleMin = function () {
  $scope.minDate = ($scope.minDate) ? null : new Date();
};
$scope.toggleMin();
$scope.picker = {};
$scope.mod = {};
$scope.start = {};
$scope.incident = {};
$scope.hlth = {};
$scope.datestartedart = {};
$scope.lasttest = {};

$scope.disabled = function (date, mode) {
  return (mode === 'day' && (date.getDay() === 0));
};

$scope.openstartdate = function ($event, index) {
  $event.preventDefault();
  $event.stopPropagation();
  $timeout(function () {
    $('#datepickerstartdate' + index).focus();
  });
  $scope.picker.startdateopened = true;
};

$scope.openenddate = function ($event, index) {
  $event.preventDefault();
  $event.stopPropagation();
  $timeout(function () {
    $('#datepickerenddate' + index).focus();
  });
  $scope.picker.enddateopened = true;
};

//Timepicker Start
$scope.FromTime = new Date();
$scope.ToTime = new Date();

$scope.hstep = 1;
$scope.mstep = 1;

$scope.ismeridian = true;

$scope.startchanged = function () {
  console.log('Start Time changed to: ' + $filter('date')($scope.starttime, 'shortTime'));
};
$scope.endchanged = function () {
  console.log('End Time changed to: ' + $filter('date')($scope.endtime, 'shortTime'));
};

$scope.clear = function () {
  $scope.mytime = null;
};
//Timepicker End
// Date & Time setting End
    $scope.LocateMe = function () {
      $scope.mapdataModal = true;

      var map = new google.maps.Map(document.getElementById('mapCanvas'), {
        zoom: 4,

        center: new google.maps.LatLng(12.9538477, 77.3507369),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      });
      var marker, i;

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(12.9538477, 77.3507369),
        map: map,
        html: ''
      });

      $scope.toggleMapModal();
    };

    $scope.toggleMapModal = function () {
      $scope.mapcount = 0;

      ///////////////////////////////////////////////////////MAP//////////////////////////

      var geocoder = new google.maps.Geocoder();

      function geocodePosition(pos) {
        geocoder.geocode({
          latLng: pos
        }, function (responses) {
          if (responses && responses.length > 0) {
            updateMarkerAddress(responses[0].formatted_address);
          } else {
            updateMarkerAddress('Cannot determine address at this location.');
          }
        });
      }

      function updateMarkerStatus(str) {
        document.getElementById('markerStatus').innerHTML = str;
      }

      function updateMarkerPosition(latLng) {
        //  console.log(latLng);
        $scope.langrcdetail.latitude = latLng.lat();
        $scope.langrcdetail.longitude = latLng.lng();

        //  console.log('$scope.updatepromotion', $scope.updatepromotion);

        document.getElementById('info').innerHTML = [
          latLng.lat(),
          latLng.lng()
        ].join(', ');
      }

      function updateMarkerAddress(str) {
        document.getElementById('mapaddress').innerHTML = str;
      }
      var map;

      function initialize() {

        $scope.latitude = 12.9538477;
        $scope.longitude = 77.3507369;
        navigator.geolocation.getCurrentPosition(function (location) {
          //                    console.log(location.coords.latitude);
          //                    console.log(location.coords.longitude);
          //                    console.log(location.coords.accuracy);
          $scope.latitude = location.coords.latitude;
          $scope.longitude = location.coords.longitude;
          //                });

          // console.log('$scope.address', $scope.address);

          var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
          map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 4,
            center: new google.maps.LatLng($scope.latitude, $scope.longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
          });
          var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: true
          });

          // Update current position info.
          updateMarkerPosition(latLng);
          geocodePosition(latLng);

          // Add dragging event listeners.
          google.maps.event.addListener(marker, 'dragstart', function () {
            updateMarkerAddress('Dragging...');
          });

          google.maps.event.addListener(marker, 'drag', function () {
            updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
          });

          google.maps.event.addListener(marker, 'dragend', function () {
            updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());
          });
        });


      }

      // Onload handler to fire off the app.
      //google.maps.event.addDomListener(window, 'load', initialize);
      initialize();

      window.setTimeout(function () {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
        map.setZoom(10);
      }, 1000);


      $scope.SaveMap = function () {
        $scope.showMapModal = !$scope.showMapModal;
        //  console.log($scope.reportincident);
      };

      //console.log('fdfd');
      $scope.showMapModal = !$scope.showMapModal;
    };


  });
