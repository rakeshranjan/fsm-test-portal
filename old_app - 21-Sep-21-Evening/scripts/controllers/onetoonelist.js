'use strict';

angular.module('secondarySalesApp')
  .controller('OneToOneListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    $scope.modalTitle = 'Thank You';

    $scope.showForm = function () {
      var visible = $location.path() === '/servicequestion/create' || $location.path() === '/servicequestion/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/servicequestion/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/servicequestion/create' || $location.path() === '/servicequestion/edit/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/servicequestion/create' || $location.path() === '/servicequestion/' + $routeParams.id;
      return visible;
    };


    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/onetoonelist") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });

    // Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catg) {
    //   $scope.categories = catg;
    // });

    Restangular.all('surveyquestions?filter={"where":{"and":[{"questiontype":{"inq":[2,6]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (surveyquestio) {
      $scope.surveyquestions = surveyquestio;
    });

    Restangular.all('onetoonetypes?filter[where][deleteflag]=false').getList().then(function (one) {
      $scope.onetoonetypes = one;
    });

    Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (tdo) {
      $scope.todotypes = tdo;
    });

    Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
      $scope.languages = langs;
    });

    //$scope.onetoone.fieldFlag = false;

    $scope.checkedChange = function (value) {
      //console.log('index', index);
      //console.log('value', value);
      $scope.onetoone.fieldFlag = value;
      //console.log($scope.onetoone.fieldFlag, "PPPPP")
    };

    $scope.checkedwebChange = function (value) {

      $scope.onetoone.webfieldFlag = value;

    };

    $scope.onetoone = {
      question: '',
      applyskip: '',
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      deleteflag: false,
      fieldFlag: false,
      webfieldFlag: false,
      multi: 'no',
      length: '256',
      default: null
    };

    $scope.showApplySkip = false;
    $scope.skipHide = false;
    $scope.skipDefault = false;

    $scope.$watch('onetoone.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('onetoone.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    $scope.$watch('onetoone.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('customfieldlabels?filter[where][subCategoryId]=' + newValue).getList().then(function (cstmfieldlabel) {
          $scope.customfieldlabels = [];
          if(cstmfieldlabel.length>0){
            for(var i=0;i<20;i++){
              if(cstmfieldlabel[0]["field"+$scope.inWords(i+1)]){
                $scope.customfieldlabels.push({name:"field"+$scope.inWords(i+1),displayName:cstmfieldlabel[0]["field"+$scope.inWords(i+1)]});
              }
            }
          }
          // console.log("$scope.customfieldlabels",$scope.customfieldlabels);
        });
      }
    });

    var a = ['','one','two','three','four', 'five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

 $scope.inWords = function(num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + '' + a[n[5][1]]) + '' : '';
    return str;
}

    $scope.$watch('onetoone.questiontype', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        if (newValue == 1) {
          $scope.skipHide = false;
        } else {
          $scope.skipHide = true;
        }

        if (newValue == 7) {
          $scope.onetoone.default = $filter('date')(new Date(), 'dd-MMM-yy');
          $scope.skipDefault = true;
        } else if (newValue == 6) {

          $scope.skipDefault = true;
        } else if (newValue == 1 || newValue == 5) {
          $scope.onetoone.default = null;
          $scope.skipDefault = false;
        }

      }
    });

    $scope.answerOptionsArray = [];

    $scope.$watch('onetoone.answeroptions', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        if ($scope.onetoone.questiontype == 6 || $scope.onetoone.questiontype == 2) {
          $scope.onetoone.default = newValue.split(",")[0];
          var array = newValue.split(",");
          $scope.answerOptionsArray = [];
          for (var i = 0; i < array.length; i++) {
            $scope.answerOptionsArray.push({
              "value": array[i],
              "todotype": null
            });
          }
        }
      }
    });

    $scope.$watch('onetoone.applyskip', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        if (newValue == 'yes') {
          $scope.showApplySkip = true;
        } else {
          $scope.showApplySkip = false;
        }
      }
    });

    $scope.$watch('onetoone.skipquestionid', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        Restangular.one('surveyquestions', newValue).get().then(function (sq) {
          $scope.skipnewArray = sq.answeroptions.split(',');
        });
      }
    });

    $scope.changeSkipValue = function (value) {

      var myValue = value.trim();

      var data = $scope.skipnewArray.filter(function (arr) {
        return arr.trim() == myValue
      })[0];

      if (data != undefined) {
        $scope.skipnewArray.splice($scope.skipnewArray.indexOf(data), 1);
        $scope.todoArray = []

        angular.forEach($scope.skipnewArray, function (val) {
          val = val.trim();
          $scope.todoArray.push({
            value: val
          })
        });
      }
    };

    /*-------------------------------------------------------------------------------------*/

    /************* SAVE *******************************************/

    // $scope.orderChange = function (code) {
    //   Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().then(function (chekrcCode) {

    //     console.log(code);

    //     var data = chekrcCode.filter(function (arr) {
    //       return arr.rcCode == code
    //     })[0];
    //     if (data != undefined) {
    //       $scope.onetoone.rcCode = '';
    //       $scope.toggleValidation();
    //       $scope.validatestring1 = 'Please Select different RCcode';
    //     }

    //   });
    // };

    $scope.validatestring = '';
    $scope.submitDisable = true;
    $scope.langModel = false;

    $scope.LangClick = function () {

      angular.forEach($scope.languages, function (data) {
        data.question = $scope.onetoone.question;
        data.answeroptions = $scope.onetoone.answeroptions;
      });

      $scope.langModel = true;
    };

    $scope.SaveLang = function () {

      angular.forEach($scope.languages, function (data, index) {
        data.inx = index + 1;
        $scope.onetoone["questionlang" + data.inx] = data.question;
        $scope.onetoone["valuelang" + data.inx] = data.answeroptions;
      });

      console.log($scope.onetoone);
      $scope.langModel = false;
      $scope.submitDisable = false;
    };

    var sArray = ['one', 'two', 'three', 'four', 'five'];

    $scope.Save = function () {

      //            console.log($scope.todoArray);
      //
      //            angular.forEach($scope.todoArray, function (data, index) {
      //                $scope.onetoone["applytodovalue" + '' + sArray[index]] = data.value;
      //                $scope.onetoone["applytodovalue" + '' + sArray[index] + '' + "todoid"] = data.todomzg;
      //            });
      if ($scope.onetoone.applytomessage == 'yes' && $scope.onetoone.answeroptions.length > 0 && ($scope.onetoone.questiontype == 6 || $scope.onetoone.questiontype == 2)) {
        $scope.applytodovalue = "";
        angular.forEach($scope.answerOptionsArray, function (data, index) {
          if ($scope.applytodovalue == "") {
            $scope.applytodovalue = data.value + "-" + data.todotype;
          } else {
            $scope.applytodovalue = $scope.applytodovalue + ";" + data.value + "-" + data.todotype;
          }
        });

        $scope.onetoone.applytodovalue = $scope.applytodovalue;

      }
      console.log($scope.onetoone);

      document.getElementById('question').style.borderColor = "";
      // document.getElementById('rccode').style.borderColor = "";
      //document.getElementById('worktype').style.borderColor = "";
      document.getElementById('serialno').style.borderColor = "";

      if ($scope.onetoone.customerId == '' || $scope.onetoone.customerId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Customer';

      } else if ($scope.onetoone.categoryId == '' || $scope.onetoone.categoryId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Category';

      } else if ($scope.onetoone.subcategoryId == '' || $scope.onetoone.subcategoryId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Subcategory';

      } else if ($scope.onetoone.question == '' || $scope.onetoone.question == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Question';
        document.getElementById('question').style.borderColor = "#FF0000";

      } 
      // else if ($scope.onetoone.rcCode == '' || $scope.onetoone.rcCode == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Enter RC Code';
      //   document.getElementById('rccode').style.borderColor = "#FF0000";

      // }
      // else if ($scope.onetoone.worktype == '' || $scope.onetoone.worktype == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Select Work Type';
      //   document.getElementById('worktype').style.borderColor = "#FF0000";

      // }  
      else if ($scope.onetoone.questiontype == '' || $scope.onetoone.questiontype == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Question Type';

      } else if ($scope.onetoone.serialno == '' || $scope.onetoone.serialno == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Order of Question';
        document.getElementById('serialno').style.borderColor = "#FF0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {

        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;

        //console.log($scope.onetoone.fieldFlag, "BEFORE SAVE-315")

        Restangular.all('surveyquestions').post($scope.onetoone).then(function (resp) {
          $scope.skipquestionupdate = {
            "skipquestionid": resp.id,
            "skipquestionvalue": resp.skipquestionvalue,
            "id": resp.skipquestionid,
            "skipquestion": true
          };
          Restangular.one('surveyquestions', resp.skipquestionid).customPUT($scope.skipquestionupdate).then(function (resp) {
            window.location = '/servicequestion-list';
          });
        });
      }
    };

    /***************************** UPDATE *******************************************/

    $scope.validatestring = '';

    $scope.Update = function (clicked) {

      document.getElementById('question').style.borderColor = "";
      // document.getElementById('rccode').style.borderColor = "";
      // document.getElementById('worktype').style.borderColor = "";
      document.getElementById('serialno').style.borderColor = "";

      if ($scope.onetoone.applytomessage == 'yes' && $scope.onetoone.answeroptions.length > 0 && ($scope.onetoone.questiontype == 6 || $scope.onetoone.questiontype == 2)) {
        $scope.applytodovalue = "";
        angular.forEach($scope.answerOptionsArray, function (data, index) {
          if ($scope.applytodovalue == "") {
            $scope.applytodovalue = data.value + "-" + data.todotype;
          } else {
            $scope.applytodovalue = $scope.applytodovalue + ";" + data.value + "-" + data.todotype;
          }
        });

        $scope.onetoone.applytodovalue = $scope.applytodovalue;

      }
      if ($scope.onetoone.customerId == '' || $scope.onetoone.customerId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Customer';

      } else if ($scope.onetoone.categoryId == '' || $scope.onetoone.categoryId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Category';

      } else if ($scope.onetoone.subcategoryId == '' || $scope.onetoone.subcategoryId == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Subcategory';

      } else if ($scope.onetoone.question == '' || $scope.onetoone.question == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Question';
        document.getElementById('question').style.borderColor = "#FF0000";

      } 
      // else if ($scope.onetoone.rcCode == '' || $scope.onetoone.rcCode == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Enter RC Code';
      //   document.getElementById('rccode').style.borderColor = "#FF0000";

      // } 
      // else if ($scope.onetoone.worktype == '' || $scope.onetoone.worktype == null) {
      //   $scope.validatestring = $scope.validatestring + 'Please Select Work Type';
      //   document.getElementById('worktype').style.borderColor = "#FF0000";

      // }  
      else if ($scope.onetoone.questiontype == '' || $scope.onetoone.questiontype == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Question Type';

      } else if ($scope.onetoone.serialno == '' || $scope.onetoone.serialno == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Order of Question';
        document.getElementById('serialno').style.borderColor = "#FF0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {

        $scope.onetoone.lastmodifiedby = $window.sessionStorage.userId;
        $scope.onetoone.lastmodifiedtime = new Date();
        $scope.onetoone.lastmodifiedrole = $window.sessionStorage.roleId;

        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;

        Restangular.all('surveyquestions', $routeParams.id).customPUT($scope.onetoone).then(function (resp) {
          $scope.skipquestionupdate = {
            "skipquestionid": resp.id,
            "skipquestionvalue": resp.skipquestionvalue,
            "id": resp.skipquestionid,
            "skipquestion": true
          };
          Restangular.one('surveyquestions', resp.skipquestionid).customPUT($scope.skipquestionupdate).then(function (resp) {
           window.location = '/servicequestion-list';
          });
        });
      }
    };

    $scope.gettodo = function (todotypeid) {
      return Restangular.one('todotypes', todotypeid).get().$object;
    };

    /*---------------------------Delete---------------------------------------------------*/

    $scope.Delete = function (id) {
      $scope.item = [{
        deleteflag: true
      }]
      Restangular.one('todostatuses/' + id).customPUT($scope.item[0]).then(function () {
        $route.reload();
      });
    }

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    if ($routeParams.id) {
      $scope.statecodeDisable = true;
      $scope.membercountDisable = true;
      $scope.message = 'Service Question has been Updated!';
      Restangular.one('surveyquestions', $routeParams.id).get().then(function (tdosts) {
        $scope.original = tdosts;
        $scope.onetoone = Restangular.copy($scope.original);
      });
    } else {
      $scope.message = 'Service Question has been Created!';
    }

  });
