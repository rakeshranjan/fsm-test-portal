'use strict';

angular.module('secondarySalesApp')
	.controller('followupbulkupdatesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/followupbulkupdates/create' || $location.path() === '/followupbulkupdates/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/followupbulkupdates/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/followupbulkupdates/create' || $location.path() === '/followupbulkupdates/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/followupbulkupdates/create' || $location.path() === '/followupbulkupdates/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.sales_zoneId == null || $window.sessionStorage.sales_zoneId == undefined) {
			$window.sessionStorage.sales_zoneId = null;
			$window.sessionStorage.sales_currentPage = 1;
		} else {
			$scope.zoneId = $window.sessionStorage.sales_zoneId;
			$scope.currentpage = $window.sessionStorage.sales_currentPage;
		}

		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.sales_currentPage = newPage;
		};


		if ($window.sessionStorage.prviousLocation != "partials/followupbulkupdate") {
			$window.sessionStorage.sales_zoneId = '';
			$window.sessionStorage.sales_currentPage = 1;
			$scope.currentpage = 1;
		}
		/*********/

		$scope.submitfollowupbulkupdates = Restangular.all('bulkupdatefollowups').getList().$object;

		if ($routeParams.id) {
			Restangular.one('bulkupdatefollowups', $routeParams.id).get().then(function (followupbulkupdate) {
				$scope.original = followupbulkupdate;
				$scope.followupbulkupdate = Restangular.copy($scope.original);
			});
		}
		$scope.searchfollowupbulkupdate = $scope.name;

		/********************************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('bulkupdatefollowups').getList().then(function (zn) {
			$scope.followupbulkupdates = zn;
			angular.forEach($scope.followupbulkupdates, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Savefollowupbulkupdate = function () {
			$scope.submitfollowupbulkupdates.post($scope.followupbulkupdate).then(function () {
				console.log('followupbulkupdate Saved');
				window.location = '/followupbulkupdates';
			});
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatefollowupbulkupdate = function () {
			document.getElementById('name').style.border = "";
			if ($scope.followupbulkupdate.name == '' || $scope.followupbulkupdate.name == null) {
				$scope.followupbulkupdate.name = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your followupbulkupdate name';
				document.getElementById('name').style.border = "1px solid #ff0000";

			}
			if ($scope.validatestring != '') {
				alert($scope.validatestring);
				$scope.validatestring = '';
			} else {
				$scope.submitfollowupbulkupdates.customPUT($scope.followupbulkupdate).then(function () {
					console.log('followupbulkupdate Saved');
					window.location = '/followupbulkupdates';
				});
			}


		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				Restangular.one('bulkupdatefollowups/' + id).remove($scope.followupbulkupdate).then(function () {
					$route.reload();
				});

			} else {

			}

		}

	});
