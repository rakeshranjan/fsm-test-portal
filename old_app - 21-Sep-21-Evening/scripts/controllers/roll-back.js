'use strict';

angular.module('secondarySalesApp')
  .controller('RollbackCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    console.log("Rollback");
    // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rollbackFlag]=true').getList().then(function (membr) {
      Restangular.all('rcdetails?filter={"where":{"and":[{"rollbackStatus":{"inq":["R","A"]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (membr) {
      $scope.members = membr;
      console.log("Rollback Tickets: ", $scope.members);
      angular.forEach($scope.members, function (member, index) {
        member.index = index + 1;

        $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + member.id).getList().then(function (HRes) {
              $scope.HealthAnswers = HRes;

              Restangular.one('surveyquestions?filter[where][id]=' + $scope.HealthAnswers[HRes.length-1].questionid).get().then(function (ticketsquest) {
                console.log("Ticket Answers: ", ticketsquest);
                

              });
            });
      });
      

    });

    $scope.getstatus = function (statusId) {
      return Restangular.one('todostatuses', statusId).get().$object;
    };

    $scope.getstate = function (stateId) {
      return Restangular.one('organisationlocations', stateId).get().$object;
    };

    $scope.getcity = function (cityId) {
    return Restangular.one('organisationlocations', cityId).get().$object;
    };

    $scope.getpincode = function (pincodeId) {
    return Restangular.one('organisationlocations', pincodeId).get().$object;
    };

    $scope.getengineer = function (assignedto) {
      return Restangular.one('users', assignedto).get().$object;
    };

});