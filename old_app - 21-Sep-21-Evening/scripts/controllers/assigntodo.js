'use strict';
angular.module('secondarySalesApp').controller('AssignTodoCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $document) {
    $scope.getTodotype = function (todotypeId) {
        return Restangular.one('todotypes', todotypeId).get().$object;
    };
    Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=10').getList().then(function (circles) {
        $scope.circles = circles;
    });
    Restangular.all('users?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (member) {
        $scope.users = member;
    });

    $scope.$watch('circle', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (ssa) {
                $scope.ssas = ssa;
            });
        }
    });

    $scope.$watch('ssa', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (circles) {
                $scope.sites = circles;
            });
        }
    });

    $scope.$watch('site', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (circles) {
                $scope.typeofsites = circles;
            });
        }
    });

    $scope.$watch('typeofsite', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            Restangular.all('members?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + "10-"+$scope.circle+";13-"+$scope.ssa+";16-"+$scope.site+";19-"+$scope.newValue+'%').getList().then(function (member) {
                $scope.members = member;
            });
        }
    });

    $scope.$watch('equipment', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            Restangular.all('todos?filter[where][deleteflag]=false&filter[where][todostatusid]=1&filter[where][memberid]=' + newValue).getList().then(function (member) {
                $scope.todos = member;
            });
        }
    });

    $scope.todoCount = 0;
    $scope.updateTodo = function () {
        $scope.todos[$scope.todoCount].assignedto = $scope.user;
        Restangular.one('todos', $scope.todos[$scope.todoCount].id).customPUT($scope.todos[$scope.todoCount]).then(function (resp) {
            $scope.todoCount++;
            if ($scope.todoCount < $scope.todos.length) {
                $scope.updateTodo();
            } else {
                window.location = '/todos';
            }
        });
    }
});