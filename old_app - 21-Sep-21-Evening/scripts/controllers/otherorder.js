'use strict';

angular.module('secondarySalesApp')
	.controller('OtherorderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    
        Restangular.all('poheaders?filter[where][flag]=true').getList().then(function (po) {
            $scope.poheaders = po;
            angular.forEach($scope.poheaders, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.$watch('customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });

                Restangular.all('poheaders?filter[where][customerId]=' + newValue).getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });

                Restangular.all('poheaders?filter[where][categoryId]=' + newValue).getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('poheaders?filter[where][subcategoryId]=' + newValue).getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        /*** view form ***/

        $scope.showViewForm = false;

        if ($routeParams.id) {
            $scope.showViewForm = true;

            Restangular.all('poheaders?filter[where][id]=' + $routeParams.id).getList().then(function (po) {
                $scope.poheadersList = po;
                angular.forEach($scope.poheadersList, function (member, index) {
                    member.index = index + 1;
                });
            });

            Restangular.all('potrailers?filter[where][poheaderId]=' + $routeParams.id).getList().then(function (potrailer) {
                $scope.potrailersList = potrailer;
                angular.forEach($scope.potrailersList, function (member, index) {
                    member.index = index + 1;
                });
            });
        }

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getItem = function (itemid) {
            return Restangular.one('itemdefinitions', itemid).get().$object;
        };
      
    });