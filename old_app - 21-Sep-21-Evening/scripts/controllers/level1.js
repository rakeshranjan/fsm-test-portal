'use strict';

angular.module('secondarySalesApp')
    .controller('Level1Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/level1/create' || $location.path() === '/level1/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level1/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level1/create' || $location.path() === '/level1/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level1/create' || $location.path() === '/level1/' + $routeParams.id;
            return visible;
        };

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            if (leveldefinition.length == 0) {
                window.location = '/';
            }

            if ($routeParams.id) {
                $scope.message = leveldefinition[0].name + ' has been Updated!';
            } else {
                $scope.message = leveldefinition[0].name + ' has been Created!';
            }

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/zones") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (lvl1) {
            $scope.levelones = lvl1;

            angular.forEach($scope.levelones, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /************************ end ********************************************************/

        $scope.DisplayLevelones = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png'
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayLevelones.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png'
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLevelones.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLevelones[index].disableLang = false;
                $scope.DisplayLevelones[index].disableAdd = false;
                $scope.DisplayLevelones[index].disableRemove = false;
                $scope.DisplayLevelones[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayLevelones[index].disableLang = true;
                $scope.DisplayLevelones[index].disableAdd = false;
                $scope.DisplayLevelones[index].disableRemove = false;
                $scope.DisplayLevelones[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayLevelones[$scope.lastIndex].disableAdd = true;
            $scope.DisplayLevelones[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayLevelones[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayLevelones);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('levelones/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel1();
        };

        var saveCount = 0;

        $scope.saveLevel1 = function () {

            if (saveCount < $scope.DisplayLevelones.length) {

                if ($scope.DisplayLevelones[saveCount].name == '' || $scope.DisplayLevelones[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel1();
                } else {
                    Restangular.all('levelones').post($scope.DisplayLevelones[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel1();
                    });
                }

            } else {
                window.location = '/level1-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.levelone.name == '' || $scope.levelone.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter' + $scope.leveldefinitions[0].name + 'Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.levelone.lastmodifiedby = $window.sessionStorage.userId;
                $scope.levelone.lastmodifiedtime = new Date();
                $scope.levelone.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('levelones', $routeParams.id).customPUT($scope.levelone).then(function (resp) {
                    window.location = '/level1-list';
                });
            }
        };

        $scope.getLevel = function (levelid) {
            return Restangular.one('levels', levelid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.levelone) {
                if ($scope.levelone.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                        // console.log(x);

                        if ($scope.levelone[key] != null) {
                            $scope.languages[mCount].lang = $scope.levelone[key];
                            //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.levelone[key] == null) {
                            // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.levelone.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.levelone["lang" + data.inx] = data.lang;
                // console.log($scope.leveldefinition);
            });

            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('levelones', $routeParams.id).get().then(function (levelone) {
                $scope.original = levelone;
                $scope.levelone = Restangular.copy($scope.original);
            });
        }

    });
