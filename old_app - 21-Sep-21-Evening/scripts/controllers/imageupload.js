'use strict';

angular.module('secondarySalesApp')
  .controller('ImageUploadCtrl', function ($scope, Restangular, $fileUploader) {
      
      var uploader = $scope.uploader = $fileUploader.create({
      scope: $scope,                          // to automatically update the html. Default: $rootScope
      url: 'http://malpani-ss-api.herokuapp.com/api/v1/containers/converbiz/upload',
      formData: [
        { key: 'value' }
      ],
      filters: [
        function (item) {                    // first user filter
          console.info('filter1');
          return true;
        }
      ]
    });

    // ADDING FILTERS

    uploader.filters.push(function (item) { // second user filter
      console.info('filter2');
      return true;
    });

    // REGISTER HANDLERS

    uploader.bind('afteraddingfile', function (event, item) {
      console.info('After adding a file', item);
    });

    uploader.bind('whenaddingfilefailed', function (event, item) {
      console.info('When adding a file failed', item);
    });

    uploader.bind('afteraddingall', function (event, items) {
      console.info('After adding all files', items);
    });

    uploader.bind('beforeupload', function (event, item) {
      console.info('Before upload', item);
    });

    uploader.bind('progress', function (event, item, progress) {
      console.info('Progress: ' + progress, item);
    });

    uploader.bind('success', function (event, xhr, item, response) {
      console.info('Success', xhr, item, response);
      $scope.$broadcast('uploadCompleted', item);
    });

    uploader.bind('cancel', function (event, xhr, item) {
      console.info('Cancel', xhr, item);
    });

    uploader.bind('error', function (event, xhr, item, response) {
      console.info('Error', xhr, item, response);
    });

    uploader.bind('complete', function (event, xhr, item, response) {
      console.info('Complete', xhr, item, response);
    });

    uploader.bind('progressall', function (event, progress) {
      console.info('Total progress: ' + progress);
    });

    uploader.bind('completeall', function (event, items) {
      console.info('Complete all', items);
    });
    }).controller('FilesController', function ($scope, $http, Restangular) {

    $scope.load = function () {
      $http.get('http://malpani-ss-api.herokuapp.com/api/v1/containers/converbiz/files').success(function (data) {
        console.log(data);
        //$scope.files = data;
      });
    };
     $scope.partners = Restangular.all('partners?filter[where][groupId]=10').getList().$object;
    $scope.retailer='';
     $scope.$watch('retailer', function(newValue, oldValue){
          $scope.files = Restangular.all('promotions?filter[where][retailername]='+newValue).getList().$object;
        });
    

    $scope.delete = function (index, id) {
              console.log('id',id);
      $http.delete('http://malpani-ss-api.herokuapp.com/api/v1/containers/converbiz/files/' + encodeURIComponent(index)).success(function (data, status, headers) {
        $scope.files.splice(index, 1);
          Restangular.one('promotions', id).get().then(function(promotion) {
                    promotion.remove();
                  });
      });
    };
    
    $scope.swipe = function (image) {
   var largeImage = document.getElementById('largeImage');
   largeImage.style.display = 'block';
   largeImage.style.width=0+"px";
   largeImage.style.height=0+"px";
   var url=largeImage.getAttribute('hre')+image;
window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
}

    $scope.$on('uploadCompleted', function(event) {
      console.log('uploadCompleted event received');
      $scope.load();
    });

  });