'use strict';

angular.module('secondarySalesApp')
    .controller('WorkFlowCategoryCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/workflow-category/create' || $location.path() === '/workflow-category/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflow-category/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflow-category/create' || $location.path() === '/workflow-category/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflow-category/create' || $location.path() === '/workflow-category/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflow-category") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('workflows?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wf) {
            $scope.workflows = wf;
        });

        Restangular.all('workflowcategories?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wfcs) {
            $scope.workflowcategories = wfcs;

            angular.forEach($scope.workflowcategories, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.workflowdisplayId = '';

        $scope.$watch('workflowdisplayId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('workflowcategories?filter[where][deleteflag]=false&filter[where][parentFlag]=true' + '&filter[where][workflowid]=' + newValue).getList().then(function (wfcs) {
                    $scope.workflowcategories = wfcs;

                    angular.forEach($scope.workflowcategories, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        /************************ end ********************************************************/

        $scope.DisplayWorkflowCategories = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayWorkflowCategories.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
                language: 1,
                parentFlag: true
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayWorkflowCategories.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayWorkflowCategories[index].disableLang = false;
                $scope.DisplayWorkflowCategories[index].disableAdd = false;
                $scope.DisplayWorkflowCategories[index].disableRemove = false;
                $scope.DisplayWorkflowCategories[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayWorkflowCategories[index].disableLang = true;
                $scope.DisplayWorkflowCategories[index].disableAdd = false;
                $scope.DisplayWorkflowCategories[index].disableRemove = false;
                $scope.DisplayWorkflowCategories[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.validatestring = "";

        $scope.SaveLang = function () {

            document.getElementById('order').style.borderColor = "";

            if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.DisplayWorkflowCategories[$scope.lastIndex].disableAdd = true;
                $scope.DisplayWorkflowCategories[$scope.lastIndex].disableRemove = true;

                $scope.DisplayWorkflowCategories[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
                $scope.DisplayWorkflowCategories[$scope.lastIndex].default = $scope.myobj.default;
                $scope.DisplayWorkflowCategories[$scope.lastIndex].languages = angular.copy($scope.languages);

                //                angular.forEach($scope.languages, function (data, index) {
                //                    data.inx = index + 1;
                //                    $scope.DisplayWorkflowCategories[$scope.lastIndex]["lang" + data.inx] = data.lang;
                //                    console.log($scope.DisplayWorkflowCategories);;
                //                });

                $scope.langModel = false;
            }

        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflowcategories/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveWflowCategory();
        };

        var saveCount = 0;

        $scope.saveWflowCategory = function () {

            if (saveCount < $scope.DisplayWorkflowCategories.length) {

                if ($scope.DisplayWorkflowCategories[saveCount].name == '' || $scope.DisplayWorkflowCategories[saveCount].name == null) {
                    saveCount++;
                    $scope.saveWflowCategory();
                } else {

                    $scope.DisplayWorkflowCategories[saveCount].workflowid = $scope.workflow;
                    $scope.DisplayWorkflowCategories[saveCount].workflowstatusid = $scope.workflowstatus;

                    Restangular.all('workflowcategories').post($scope.DisplayWorkflowCategories[saveCount]).then(function (resp) {
                        //                        saveCount++;
                        //                        $scope.saveWflowCategory();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayWorkflowCategories[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayWorkflowCategories[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayWorkflowCategories[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                workflowid: resp.workflowid
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/workflow-category-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('workflowcategories').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveWflowCategory();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.workflowcategory.name == '' || $scope.workflowcategory.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Category Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.workflowcategory.lastmodifiedby = $window.sessionStorage.userId;
                $scope.workflowcategory.lastmodifiedtime = new Date();
                $scope.workflowcategory.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflowcategories', $routeParams.id).customPUT($scope.workflowcategory).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('workflowcategories', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/workflow-category-list';
            }

        }

        $scope.getworkflow = function (workflowid) {
            return Restangular.one('workflows', workflowid).get().$object;
        };

        $scope.getworkflowstatus = function (workflowstatusid) {
            return Restangular.one('workflowstatuses', workflowstatusid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};
            $scope.myobj.orderNo = $scope.workflowcategory.orderNo;
            $scope.myobj.default = $scope.workflowcategory.default;

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            //            for (var key in $scope.workflowcategory) {
            //                if ($scope.workflowcategory.hasOwnProperty(key)) {
            //                    var x = myVal + '' + uCount;
            //                    if (key == x) {
            //                        // console.log(x);
            //
            //                        if ($scope.workflowcategory[key] != null) {
            //                            $scope.languages[mCount].lang = $scope.workflowcategory[key];
            //                            //  console.log($scope.workflowcategory[key]);
            //                            uCount++;
            //                            mCount++;
            //                        } else if ($scope.workflowcategory[key] == null) {
            //                            // console.log($scope.workflowcategory[key]);
            //                            $scope.languages[mCount].lang = $scope.workflowcategory.lang1;
            //                            uCount++;
            //                            mCount++;
            //                        }
            //                    }
            //                }
            //            }
            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.workflowcategory.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
            //            angular.forEach($scope.languages, function (data, index) {
            //                data.inx = index + 1;
            //                $scope.workflowcategory["lang" + data.inx] = data.lang;
            //                // console.log($scope.workflowcategory);
            //            });
             angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if(data.id === lngwrkflw.language){
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.workflowcategory.orderNo = $scope.myobj.orderNo;
            $scope.workflowcategory.default = $scope.myobj.default;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Category has been Updated!';
            Restangular.one('workflowcategories', $routeParams.id).get().then(function (workflowcategory) {
                Restangular.all('workflowcategories?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                    $scope.original = workflowcategory;
                    $scope.workflowcategory = Restangular.copy($scope.original);
                });
            });

        } else {
            $scope.message = 'Category has been Created!';
        }

    });