'use strict';

angular.module('secondarySalesApp')
	.controller('SalesAreasCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter) {

		/***********************************************SHOW AND HIDE BUTTON**************************************/

		$scope.showForm1 = false;
		$scope.showForm = function () {
			var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/district/create';
				return visible;
				//	$scope.statedisable = false;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
			return visible;

		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
			return visible;
		};
		$scope.searchSalesArea = $scope.name;
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.sales_zoneId == null
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
		else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.zoneId = $window.sessionStorage.myRoute;
		}

		if ($window.sessionStorage.prviousLocation != "partials/sales-areas") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*....................................................................................*/
		$scope.zoneId = '';
		$scope.zonalid = '';
		$scope.$watch('zoneId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$window.sessionStorage.myRoute = newValue;
				$window.sessionStorage.currentRoute = newValue;
				//$scope.zoneId = $window.sessionStorage.myRoute;
				//console.log('Inside Value:', $window.sessionStorage.myRoute);
				$scope.sal = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					$scope.DisplaysalesAreas = sal;
					console.log('DisplaysalesAreas',sal);
					angular.forEach($scope.DisplaysalesAreas, function (member, index) {
						member.index = index + 1;
					});
				});
			}
			$scope.zonalid = +newValue;
		});

		/******************************************************************************************/
		$scope.getZone = function (zoneId) {
			return Restangular.one('zones', zoneId).get().$object;
		};

		$scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.zoneId = $window.sessionStorage.myRoute;
		});



		$scope.DisplaysalesAreas = [];
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('sales-areas/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

		$scope.salesArea = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		}


		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('zoneId').style.border = "";
			if ($scope.salesArea.name == '' || $scope.salesArea.name == null) {
				document.getElementById('name').style.borderColor = "#FF0000";
				$scope.validatestring = $scope.validatestring + 'Plese Enter District Area';
			} else if ($scope.salesArea.zoneId == '' || $scope.salesArea.zoneId == null) {
				$scope.salesArea.zoneId = '';
				document.getElementById('zoneId').style.border = "1px solid #ff0000";
				$scope.validatestring = $scope.validatestring + 'Plese Select State Area';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('sales-areas').post($scope.salesArea).then(function () {
					//$location.path('/sales-areas');
					window.location = '/sales-areas';
				});
			}
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};


		///////////////////////////////////////////////////////////UPDATE DELETE///////////////////////////////    

		//$scope.zones = Restangular.all('zones').getList().$object;
		$scope.modalTitle = 'Thank You';
		if ($routeParams.id) {
			$scope.showForm1 = true;
			$scope.message = 'District has been Updated!';
			Restangular.one('sales-areas', $routeParams.id).get().then(function (salesArea) {
				$scope.original = salesArea;
				$scope.salesArea = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'District has been Created!';
		}

		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.salesArea.name == '' || $scope.salesArea.name == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter District Area';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('sales-areas', $routeParams.id).customPUT($scope.salesArea).then(function () {
					console.log('Sales-Area Saved', $scope.salesArea);
					window.location = '/sales-areas';
					//$location.path('/sales-areas');
				});
			}
		};


		/////////////////////////////////////////CALL CITY ACCORDING TO ZONE//////////////////


	});
