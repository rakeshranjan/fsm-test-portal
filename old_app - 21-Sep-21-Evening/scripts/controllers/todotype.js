'use strict';

angular.module('secondarySalesApp')
    .controller('ToDoTypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/todo/create' || $location.path() === '/todo/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todo/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todo/create' || $location.path() === '/todo/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todo/create' || $location.path() === '/todo/' + $routeParams.id;
            return visible;
        };

        $scope.DisplayTodos = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayTodos.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
                language: 1,
                parentFlag: true,
                toInitial:false
            });
        };

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayTodos.splice(indexVal, 1);
        };

        $scope.myobj = {};

        $scope.typeChange = function (index) {
            $scope.DisplayTodos[index].name = '';
            $scope.DisplayTodos[index].disableLang = false;
            $scope.DisplayTodos[index].langdark = 'images/Lgrey.png';
        };

        $scope.levelChange = function (index) {
            $scope.DisplayTodos[index].disableLang = true;
            $scope.DisplayTodos[index].langdark = 'images/Ldark.png';
            $scope.DisplayTodos[index].disableAdd = false;
            $scope.DisplayTodos[index].disableRemove = false;
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name, type) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            if (type == 'todo') {
                $scope.myobj.hideShow = true;
                $scope.langModel = true;
            } else {
                $scope.myobj.hideShow = false;
                $scope.langModel = true;
            }

        };

        $scope.SaveLang = function () {

            if ($scope.myobj.hideShow == true) {

                document.getElementById('due').style.borderColor = "";

                if ($scope.myobj.due == '' || $scope.myobj.due == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Due Days';
                    document.getElementById('due').style.borderColor = "#FF0000";
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
                $scope.DisplayTodos[$scope.lastIndex].disableAdd = true;
                $scope.DisplayTodos[$scope.lastIndex].disableRemove = true;
                $scope.DisplayTodos[$scope.lastIndex].due = $scope.myobj.due;
                $scope.DisplayTodos[$scope.lastIndex].languages = angular.copy($scope.languages);

                //                angular.forEach($scope.languages, function (data, index) {
                //                    data.inx = index + 1;
                //                    $scope.DisplayTodos[$scope.lastIndex]["lang" + data.inx] = data.lang;
                //                    console.log($scope.DisplayTodos);;
                //                });

                $scope.langModel = false;
            }
        };


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/todotype") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (todo) {
            $scope.todotypes = todo;
            // console.log($scope.workflows);
            angular.forEach($scope.todotypes, function (member, index) {
                member.index = index + 1;

                if (member.type == 'todo') {
                    member.typename = 'ToDo';
                } else {
                    member.typename = 'Message';
                }

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        /*-------------------------------------------------------------------------------------*/

        /************* SAVE *******************************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveTodoType();
        };

        var saveCount = 0;

        $scope.saveTodoType = function () {

            if (saveCount < $scope.DisplayTodos.length) {

                if ($scope.DisplayTodos[saveCount].name == '' || $scope.DisplayTodos[saveCount].name == null) {
                    saveCount++;
                    $scope.saveTodoType();
                } else {
                    Restangular.all('todotypes').post($scope.DisplayTodos[saveCount]).then(function (resp) {
                        //                        saveCount++;
                        //                        $scope.saveTodoType();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayTodos[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayTodos[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayTodos[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                type: resp.type,
                                toInitial: resp.toInitial
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/todo-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('todotypes').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveTodoType();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.todotype.name == '' || $scope.todotype.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Todo Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.todotype.lastmodifiedby = $window.sessionStorage.userId;
                $scope.todotype.lastmodifiedtime = new Date();
                $scope.todotype.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('todotypes', $routeParams.id).customPUT($scope.todotype).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('todotypes', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/todo-list';
            }

        }


        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todotypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            if ($scope.todotype.type == 'todo') {
                $scope.myobj.hideShow = true;
                $scope.myobj.due = $scope.todotype.due;
            } else {
                $scope.myobj.hideShow = false;
            }

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.todotype.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if (data.id === lngwrkflw.language) {
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.todotype.due = $scope.myobj.due;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Todo has been Updated!';
            Restangular.one('todotypes', $routeParams.id).get().then(function (todotype) {
                Restangular.all('todotypes?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                    $scope.original = todotype;
                    $scope.todotype = Restangular.copy($scope.original);
                });
            });
        } else {
            $scope.message = 'Todo has been Created!';
        }

    });