'use strict';

angular.module('secondarySalesApp')
    .controller('ConfigCustomReportsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/
        //        if ($window.sessionStorage.roleId != 1) {
        //            window.location = "/";
        //        }
        $scope.showForm = function () {
            var visible = $location.path() === '/configcustomreport/create' || $location.path() === '/configcustomreport/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/configcustomreport/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/configcustomreport/create' || $location.path() === '/configcustomreport/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/configcustomreport/create' || $location.path() === '/configcustomreport/' + $routeParams.id;
            return visible;
        };
        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.myRoute;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        if ($window.sessionStorage.prviousLocation != "partials/configcustomreport") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.rol = Restangular.all('customreports').getList().then(function (customreports) {
            $scope.customreports = customreports;
            angular.forEach($scope.customreports, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.customreport = {
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            deleteflag: false,
            fieldstodisplay:[]
        };

        $scope.$watch('customreport.entity', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.fieldsToDisplay = [];
                if (newValue === 'member') {
                    $scope.fieldsToDisplay = $scope.memberFields;
                } else if (newValue === 'onetoone') {
                    $scope.fieldsToDisplay = $scope.onetooneFields;
                } else if (newValue === 'todo') {
                    $scope.fieldsToDisplay = $scope.todoFields;
                } else if (newValue === 'workflow') {
                    $scope.fieldsToDisplay = $scope.workflowFields;
                }
                
                if($routeParams.id){
                    for(var i=0;i<$scope.fieldsToDisplay.length;i++){
                        if($scope.customreport.fieldstodisplay.indexOf($scope.fieldsToDisplay[i].value)>-1){
                            $scope.fieldsToDisplay[i].isSelected = true;
                        }
                    }
                }
            }
        });

        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            if ($scope.customreport.name == '' || $scope.customreport.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                for(var i=0;i<$scope.fieldsToDisplay.length;i++){
                    if($scope.fieldsToDisplay[i].isSelected===true){
                        $scope.customreport.fieldstodisplay.push($scope.fieldsToDisplay[i].value);
                    }
                }
                Restangular.all('customreports').post($scope.customreport).then(function () {
                    //$scope.roles.post($scope.role).then(function () {
                    console.log('$scope.customreport', $scope.customreport);
                    window.location = '/configcustomreport';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        /******************************** UPDATE ********************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            if ($scope.customreport.name == '' || $scope.customreport.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //$scope.roles.customPUT($scope.role).then(function () {
                $scope.customreport.fieldstodisplay = [];
                for(var i=0;i<$scope.fieldsToDisplay.length;i++){
                    if($scope.fieldsToDisplay[i].isSelected===true){
                        $scope.customreport.fieldstodisplay.push($scope.fieldsToDisplay[i].value);
                    }
                }
                Restangular.one('customreports', $routeParams.id).customPUT($scope.customreport).then(function () {
                    console.log('$scope.customreport', $scope.customreport);
                    window.location = '/configcustomreport';
                });
            }
        };


        if ($routeParams.id) {
            $scope.message = 'Role has been Updated!';
            Restangular.one('customreports', $routeParams.id).get().then(function (customreport) {
                $scope.original = customreport;
                $scope.customreport = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Role has been Created!';
        }

        /*************************************************** DELETE ********************************/

        $scope.updateRoleFlag = function (id) {
                if (confirm("Are you sure want to delete..!") == true) {
                    Restangular.one('customreports/' + id).remove($scope.customreport).then(function () {
                        $route.reload();
                    });

                } else {

                }

            }
            /*************************************************** DELETE ********************************/

        /*************************************************** FIELDS ********************************/
        $scope.memberFields = [
            {
                "name": "Full Name",
                "value": "fullname",
                "isSelected": false
   },
            {
                "name": "Member Id",
                "value": "memberId",
                "isSelected": false
   },
            {
                "name": "DOB",
                "value": "dob",
                "isSelected": false
   },
            {
                "name": "GENDER",
                "value": "genderId",
                "isSelected": false
   },
            {
                "name": "OCCUPATION",
                "value": "occupationId",
                "isSelected": false
   },
            {
                "name": "EDUCATION",
                "value": "educationId",
                "isSelected": false
   },
            {
                "name": "ADDRESS",
                "value": "address",
                "isSelected": false
   },
            {
                "name": "AGE",
                "value": "age",
                "isSelected": false
   },
            {
                "name": "LATITUDE",
                "value": "latitude",
                "isSelected": false
   },
            {
                "name": "LONGITUDE",
                "value": "longitude",
                "isSelected": false
   },
            {
                "name": "STRESS",
                "value": "stress",
                "isSelected": false
   },
            {
                "name": "ORG STRUCTURE",
                "value": "orgstructure",
                "isSelected": false
   },
            {
                "name": "LAST MODIFIED BY",
                "value": "lastmodifiedby",
                "isSelected": false
   },
            {
                "name": "LAST MODIFIED TIME",
                "value": "lastmodifiedtime",
                "isSelected": false
   },
            {
                "name": "LAST MODIFIED ROLE",
                "value": "lastmodifiedrole",
                "isSelected": false
   },
            {
                "name": "CREATED BY",
                "value": "createdby",
                "isSelected": false
   },
            {
                "name": "CREATED TIME",
                "value": "createdtime",
                "isSelected": false
   },
            {
                "name": "CREATED ROLE",
                "value": "createdrole",
                "isSelected": false
   },
            {
                "name": "DELETE FLAG",
                "value": "deleteflag",
                "isSelected": false
   },
            {
                "name": "ID",
                "value": "id",
                "isSelected": false
   }
];

        Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[order]=serialno ASC').getList().then(function (surqustns) {
            $scope.surveyquestions = surqustns;

            $scope.onetooneFields = [];
            angular.forEach($scope.surveyquestions, function (surveyquestion, index) {
                $scope.onetooneFields.push({
                    "name": surveyquestion.question,
                    "value": surveyquestion.id,
                    "isSelected": false
                });
            });
        });
        $scope.todoFields = [
            {
                "name": "Member Id",
                "value": "memberid",
                "isSelected": false
   },
            {
                "name": "Todo Type Id",
                "value": "todotypeid",
                "isSelected": false
   },
            {
                "name": "Todo Status Id",
                "value": "todostatusid",
                "isSelected": false
   },
            {
                "name": "FollowUp Date",
                "value": "followupdate",
                "isSelected": false
   },
            {
                "name": "Assigned To",
                "value": "assignedto",
                "isSelected": false
   },
            {
                "name": "Last Modified By",
                "value": "lastmodifiedby",
                "isSelected": false
   },
            {
                "name": "Last Modified Time",
                "value": "lastmodifiedtime",
                "isSelected": false
   },
            {
                "name": "Last Modified Role",
                "value": "lastmodifiedrole",
                "isSelected": false
   },
            {
                "name": "Created By",
                "value": "createdby",
                "isSelected": false
   },
            {
                "name": "Created Time",
                "value": "createdtime",
                "isSelected": false
   },
            {
                "name": "Created Role",
                "value": "createdrole",
                "isSelected": false
   },
            {
                "name": "Delete Flag",
                "value": "deleteflag",
                "isSelected": false
   },
            {
                "name": "Org Structure",
                "value": "orgstructure",
                "isSelected": false
   },
            {
                "name": "ID",
                "value": "id",
                "isSelected": false
   }
];
        $scope.workflowFields = [
            {
                "name": "Member Id",
                "value": "memberid",
                "isSelected": false
   },
            {
                "name": "Assigned To",
                "value": "assignedto",
                "isSelected": false
   },
            {
                "name": "Workflow Id",
                "value": "workflowid",
                "isSelected": false
   },
            {
                "name": "Workflow Status Id",
                "value": "workflowstatusid",
                "isSelected": false
   },
            {
                "name": "Workflow Category Id",
                "value": "workflowcategoryid",
                "isSelected": false
   },
            {
                "name": "Workflow Priority Id",
                "value": "workflowpriorityid",
                "isSelected": false
   },
            {
                "name": "Followup Date",
                "value": "followupdate",
                "isSelected": false
   },
            {
                "name": "Last Modified By",
                "value": "lastmodifiedby",
                "isSelected": false
   },
            {
                "name": "Last Modified Time",
                "value": "lastmodifiedtime",
                "isSelected": false
   },
            {
                "name": "Last Modified Role",
                "value": "lastmodifiedrole",
                "isSelected": false
   },
            {
                "name": "Created By",
                "value": "createdby",
                "isSelected": false
   },
            {
                "name": "Created Time",
                "value": "createdtime",
                "isSelected": false
   },
            {
                "name": "Created Role",
                "value": "createdrole",
                "isSelected": false
   },
            {
                "name": "Delete Flag",
                "value": "deleteflag",
                "isSelected": false
   },
            {
                "name": "Org Structure",
                "value": "orgstructure",
                "isSelected": false
   },
            {
                "name": "ID",
                "value": "id",
                "isSelected": false
   }
];
        /*************************************************** FIELDS ********************************/


    });