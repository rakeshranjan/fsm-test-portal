'use strict';

angular.module('secondarySalesApp')
	.controller('EduCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
//		if ($window.sessionStorage.roleId != 1) {
//			window.location = "/";
//		}
		$scope.showForm = function () {
			var visible = $location.path() === '/education/create' || $location.path() === '/education/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/education/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/education/create' || $location.path() === '/education/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/education/create' || $location.path() === '/education/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		//console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/education") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		/*********/

		//  $scope.educations = Restangular.all('educations').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Education has been Updated!';
			Restangular.one('educations', $routeParams.id).get().then(function (education) {
                Restangular.all('educations?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
				$scope.original = education;
				$scope.education = Restangular.copy($scope.original);
                });
			});
		} else {
			$scope.message = 'Education has been Created!';
		}
		$scope.Search = $scope.name;

		/*************************** INDEX *******************************************/
    
		 Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        $scope.DisplayEducation = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayEducation.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayEducation.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayEducation[index].disableLang = false;
                $scope.DisplayEducation[index].disableAdd = false;
                $scope.DisplayEducation[index].disableRemove = false;
                $scope.DisplayEducation[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayEducation[index].disableLang = true;
                $scope.DisplayEducation[index].disableAdd = false;
                $scope.DisplayEducation[index].disableRemove = false;
                $scope.DisplayEducation[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.validatestring = "";

        $scope.SaveLang = function () {

            $scope.DisplayEducation[$scope.lastIndex].disableAdd = true;
            $scope.DisplayEducation[$scope.lastIndex].disableRemove = true;
                $scope.DisplayEducation[$scope.lastIndex].languages = angular.copy($scope.languages);

//            angular.forEach($scope.languages, function (data, index) {
//                data.inx = index + 1;
//                $scope.DisplayEducation[$scope.lastIndex]["lang" + data.inx] = data.lang;
//                console.log($scope.DisplayEducation);;
//            });

            $scope.langModel = false;
        };


//        if ($routeParams.id) {
//            $scope.message = 'Education has been Updated!';
//            Restangular.one('educations', $routeParams.id).get().then(function (education) {
//                $scope.original = education;
//                $scope.education = Restangular.copy($scope.original);
//            });
//        } else {
//            $scope.message = 'Education has been Created!';
//        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/

        Restangular.all('educations?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (zn) {
            $scope.educations = zn;
            angular.forEach($scope.educations, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveEdu();
        };

        var saveCount = 0;

        $scope.saveEdu = function () {

            if (saveCount < $scope.DisplayEducation.length) {

                if ($scope.DisplayEducation[saveCount].name == '' || $scope.DisplayEducation[saveCount].name == null) {
                    saveCount++;
                    $scope.saveGender();
                } else {

                    Restangular.all('educations').post($scope.DisplayEducation[saveCount]).then(function (resp) {
//                        saveCount++;
//                        $scope.saveEdu();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayEducation[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayEducation[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayEducation[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                type: resp.type
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/education';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('educations').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveEdu();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.education.name == '' || $scope.education.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Education Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.education.lastmodifiedby = $window.sessionStorage.userId;
                $scope.education.lastmodifiedtime = new Date();
                $scope.education.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('educations', $routeParams.id).customPUT($scope.education).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('educations', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/education';
            }

        }


        $scope.gender = {
            "name": '',
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.education.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if (data.id === lngwrkflw.language) {
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.langModel = false;
        };


        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('educations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

	});
