'use strict';

angular.module('secondarySalesApp')
  .controller('AddCustomerCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    $scope.modalTitle = 'Thank You';

    $scope.showForm = function () {
      var visible = $location.path() === '/add-customer/create' || $location.path() === '/add-customer/edit/' + $routeParams.id;
      return visible;
    };

    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/add-customer/create';
        return visible;
      }
    };

    $scope.customer = {
      customercode: null,
      customername: null,
      customerno: null,
      customeraddress: null,
      createdate: new Date(),
      customercategoryid: null,
      deleteflag: false,
      locationid: null,
      clientid: null
    };

    /*********************************** Pagination *******************************************/

    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/add-customer" || $window.sessionStorage.prviousLocation != "partials/add-customer") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    Restangular.all('customercategories?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customercategories = cust;
    });

    Restangular.one('customers', $window.sessionStorage.customerId).get().then(function (customer) {
      $scope.currentCustomer = customer;
    });

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customerdetails?filter[where][deleteflag]=false' + '&filter[where][clientid]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customerdetails?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;
      angular.forEach($scope.customers, function (member, index) {
        member.index = index + 1;
      });
    });

    $scope.saveCustomer = function () {
      $scope.customer.clientid = $window.sessionStorage.customerId;
      $scope.customer.orgroot = $scope.currentCustomer.orgroot;
      Restangular.all('customerdetails').post($scope.customer).then(function (custResponse) {
        $location.path('/add-customer');
      });
    };

    /*** Edit customer ***/

    if ($routeParams.id) {
      Restangular.one('customerdetails', $routeParams.id).get().then(function (customer) {
        $scope.original = customer;
        $scope.customer = Restangular.copy($scope.original);
      });
    }

    $scope.updateCustomer = function () {
      Restangular.one('customerdetails/' + $routeParams.id).customPUT($scope.customer).then(function () {
        $location.path('/add-customer');
      });
    };
  });
