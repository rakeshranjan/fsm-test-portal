'use strict';
angular.module('secondarySalesApp').controller('ApplyForDocumentCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout, $modal) {
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.HideCreateButton = false;
    /*****************************************************************************/
    $scope.documentmaster = {
        documentflag: 'yes',
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        lastmodifiedtime: new Date(),
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        deleteflag: false,
        memberId: []
    };
    $scope.documentmaster.memberId.push($window.sessionStorage.fullName);
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 45,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    /***********************************************************************/
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
    $scope.schemestages = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().$object;
    $scope.printschemes = Restangular.all('documenttypes').getList().$object;
    $scope.submitsurveyanswers = Restangular.all('surveyanswers');
    /**************************************** Member *******************************************/
    if ($window.sessionStorage.roleId == 5) {
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.documentmaster.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
        });
        // $scope.beneficiaries = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][id]=' + $window.sessionStorage.fullName).getList().$object;
        $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][id]=' + $window.sessionStorage.fullName).getList().then(function (part1) {
            $scope.beneficiaries = part1;
        });
    } else if ($window.sessionStorage.roleId == 15) {
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.documentmaster.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
        });
        $scope.beneficiaries = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
    } else {
        Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                $scope.documentmaster.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
            });
            $scope.beneficiaries = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().$object;
        });
    }
    /***********************************************************************/
    $scope.message = $scope.documentadded; //'Document has been applied!';
    $scope.modalTitle = $scope.thankyou;
    $scope.applyfordocumentdataModal = false;
    $scope.validatestring = '';
    $scope.documentdataModal = false;
    $scope.CreateClicked = false;
    $scope.documentdisabled = true;
    /*if ($scope.documentmaster.memberId == null || $scope.documentmaster.memberId == '' || $scope.documentmaster.memberId == undefined) {
			$scope.documentdisabled = true;
		} else {
			$scope.documentdisabled = false;
		}*/
    /*	$scope.docFunction = function (id) {
			console.log('docFunction', id);
			$scope.documentdisabled = false;
			$scope.currentMemberid = id;
			$scope.surveyanswer.beneficiaryid = id;
		};
		$scope.$watch('documentmaster.schemeId', function (newValue, oldValue) {
			$scope.currentdocid = newValue;
			if (newValue == oldValue || oldValue == undefined) {
				return;
			} else {
				Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.currentMemberid + '&filter[where][pillarid]=1' + '&filter[where][documentid]=' + newValue + '&filter[where][answer]=no').getList().then(function (sanswer) {
					console.log('sanswer', sanswer);
					if (sanswer[0].documentid === undefined) {
						return;
					} else if (sanswer[0].documentid == newValue) {
						alert('This document has been already applied. Please select some other document..');
						$scope.documentmaster.schemeId = '';
					} else {
						alert('Not Answer');
					}
				});
				Restangular.all('surveyquestions?filter[where][questiontype]=r' + '&filter[where][yesdocumentflag]=true' + '&filter[where][yesdocumentid]=' + newValue).getList().then(function (response) {
					$scope.totalpresnt = response;
					console.log('$scope.totalpresnt', $scope.totalpresnt);
					for (var i = 0; i < $scope.totalpresnt.length; i++) {
						if ($scope.totalpresnt[i].yesdocumentid == newValue) {
							$scope.surveyanswer.documentid = id;
							scope.surveyanswer.schemeid = id;
							scope.surveyanswer.pillarid = 1;
							//scope.surveyanswer.pillarid = 1;
							alert('Question Present');
						} else {
							alert('Question Not Present2');
							return;
						}
					}
				});
			}
		});
	*/
    /*$scope.surveyanswer = {
			"beneficiaryid": $scope.currentMemberid,
			"pillarid": 1,
			"questionid": 40,
			"answer": "yes",
			"lastupdatedtime": new Date(),
			"lastupdatedby": $window.sessionStorage.UserEmployeeId,
			"deleteflag": false,
			"documentid": $scope.currentdocid,
			"schemeid": $scope.currentdocid,
			"availeddate": new Date(),
			"referenceno": "101",
			"schemeid": new Date()
		};*/
    $scope.surveyanswer = {};
    $scope.Save = function () {
        console.log('$scope.currentdocid', $scope.currentdocid);
        $scope.todo = {
            "todotype": 29,
            "pillarid": 1,
            "beneficiaryid": $scope.documentmaster.memberId,
            "documentid": $scope.documentmaster.schemeId,
            "datetime": $scope.documentmaster.datetime,
            "stage": $scope.documentmaster.stage,
            "documentid": $scope.documentmaster.schemeId,
            "documentflag": 'yes',
            "pillarid": 1,
            "facility": $window.sessionStorage.coorgId,
            "district": $window.sessionStorage.salesAreaId,
            "state": $window.sessionStorage.zoneId,
            "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
            "lastmodifiedtime": new Date(),
            "site": $scope.beneficiarysite,
            "facilityId": $scope.getfacilityId,
            "roleId": $window.sessionStorage.roleId
        };
        if ($scope.documentmaster.memberId == '' || $scope.documentmaster.memberId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selmember;
        } else if ($scope.documentmaster.schemeId == '' || $scope.documentmaster.schemeId == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldocument;
        } else if ($scope.documentmaster.stage == '' || $scope.documentmaster.stage == null) {
            $scope.validatestring = $scope.validatestring + $scope.selstage;
        }
        if ($scope.documentmaster.stage == 4) {
            if ($scope.documentmaster.responserecieve == '' || $scope.documentmaster.responserecieve == null) {
                $scope.validatestring = $scope.validatestring + $scope.selresponsererec;
            }
        }
        if ($scope.documentmaster.stage == 5) {
            if ($scope.documentmaster.delay == '' || $scope.documentmaster.delay == null) {
                $scope.validatestring = $scope.validatestring + $scope.selreasondelay;
            }
        }
        if ($scope.documentmaster.responserecieve === 'Rejected') {
            if ($scope.documentmaster.rejection == '' || $scope.documentmaster.rejection == null) {
                $scope.validatestring = $scope.validatestring + $scope.selreasonrej;
            }
        }
        if ($scope.documentmaster.stage == 4 && $scope.documentmaster.responserecieve === 'Rejected') {
            $scope.todo.status = 3;
        } else if ($scope.documentmaster.stage == 9 || $scope.documentmaster.stage == 7) {
            $scope.todo.status = 3;
        } else {
            $scope.todo.status = 1;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.toggleLoading();
            ///////$scope.SaveDocument();
            Restangular.all('schememasters?filter[where][schemeId]=' + $scope.documentmaster.schemeId + '&filter[where][memberId]=' + $scope.documentmaster.memberId + '&filter[where][documentflag]=true').getList().then(function (schemaster) {
                if (schemaster.length > 0) {
                    $scope.schemeMasters = schemaster[0];
                }
                Restangular.all('surveyquestions?filter[where][questiontype]=r&filter[where][yespopup]=dateforid&filter[where][yesdocumentid]=' + $scope.documentmaster.schemeId).getList().then(function (srvyqstns) {
                    $scope.allSurveyQuestions = srvyqstns;
                    if (srvyqstns.length > 0) {
                        $scope.surveyQuestions = srvyqstns[0];
                        Restangular.all('surveyanswers?filter[where][questionid]=' + $scope.surveyQuestions.id + '&filter[where][beneficiaryid]=' + $scope.documentmaster.memberId).getList().then(function (srvyanswrs) {
                            if (srvyanswrs.length > 0) {
                                $scope.surveyAnswers = srvyanswrs[0];
                            }
                            $scope.ValidatingDocument();
                        });
                    } else {
                        //$scope.ValidatingDocument();
                        Restangular.all('surveyquestions?filter[where][skipdocument]=true&filter[where][skipondocument]=' + $scope.documentmaster.schemeId).getList().then(function (srvyqstns) {
                            $scope.allSurveyQuestions = srvyqstns;
                            if (srvyqstns.length > 0) {
                                $scope.surveyQuestions = srvyqstns[1];
                                Restangular.all('surveyanswers?filter[where][questionid]=' + $scope.surveyQuestions.id + '&filter[where][beneficiaryid]=' + $scope.documentmaster.memberId).getList().then(function (srvyanswrs) {
                                    if (srvyanswrs.length > 0) {
                                        $scope.surveyAnswers = srvyanswrs[0];
                                    }
                                    $scope.ValidatingDocument();
                                });
                            } else {
                                $scope.ValidatingDocument();
                            }
                        });
                    }
                });
            });
        }
        $scope.OK = function () {
            $scope.documentdataModal = !$scope.documentdataModal;
            window.location = '/';
        };
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.ValidatingDocument = function () {
        //				console.log('$scope.schemeMasters', $scope.schemeMasters);
        //				console.log('$scope.surveyQuestions', $scope.surveyQuestions);
        //				console.log('$scope.surveyAnswers', $scope.surveyAnswers);
        if ($scope.schemeMasters == undefined || $scope.schemeMasters.length == 0) {
            if ($scope.surveyAnswers == undefined && $scope.surveyQuestions == undefined) {
                $scope.SaveDocument();
            } else if ($scope.surveyQuestions != undefined && $scope.surveyAnswers == undefined) {
                $scope.SaveAnswers($scope.documentmaster.memberId, $scope.surveyQuestions.pillarid, $scope.surveyQuestions.id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.surveyQuestions.serialno, $scope.surveyQuestions.yesincrement, $scope.surveyQuestions.noincrement, $scope.surveyQuestions.yesdocumentflag, $scope.surveyQuestions.nodocumentflag, $scope.surveyQuestions.yesdocumentid, $scope.surveyQuestions.nodocumentid, new Date(), "", $window.sessionStorage.roleId);
                if ($scope.allSurveyQuestions.length > 1) {
                    if ($scope.documentmaster.stage == 9 || $scope.documentmaster.stage == 7 || ($scope.documentmaster.stage == 4 && $scope.documentmaster.responserecieve === 'Rejected')) {
                        $scope.SaveAllAnswers($scope.documentmaster.memberId, $scope.allSurveyQuestions[0].pillarid, $scope.allSurveyQuestions[0].id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.allSurveyQuestions[0].serialno, $scope.allSurveyQuestions[0].yesincrement, $scope.allSurveyQuestions[0].noincrement, $scope.allSurveyQuestions[0].yesdocumentflag, $scope.allSurveyQuestions[0].nodocumentflag, $scope.allSurveyQuestions[0].yesdocumentid, $scope.allSurveyQuestions[0].nodocumentid, new Date(), "", $window.sessionStorage.roleId);
                    }
                }
            } else if ($scope.surveyQuestions != undefined && $scope.surveyAnswers != undefined && $scope.surveyAnswers.answer.toLowerCase() != 'yes') {
                $scope.SaveAnswers($scope.documentmaster.memberId, $scope.surveyQuestions.pillarid, $scope.surveyQuestions.id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.surveyQuestions.serialno, $scope.surveyQuestions.yesincrement, $scope.surveyQuestions.noincrement, $scope.surveyQuestions.yesdocumentflag, $scope.surveyQuestions.nodocumentflag, $scope.surveyQuestions.yesdocumentid, $scope.surveyQuestions.nodocumentid, new Date(), "", $window.sessionStorage.roleId);
                if ($scope.allSurveyQuestions.length > 1) {
                    if ($scope.documentmaster.stage == 9 || $scope.documentmaster.stage == 7 || ($scope.documentmaster.stage == 4 && $scope.documentmaster.responserecieve === 'Rejected')) {
                        $scope.SaveAllAnswers($scope.documentmaster.memberId, $scope.allSurveyQuestions[0].pillarid, $scope.allSurveyQuestions[0].id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.allSurveyQuestions[0].serialno, $scope.allSurveyQuestions[0].yesincrement, $scope.allSurveyQuestions[0].noincrement, $scope.allSurveyQuestions[0].yesdocumentflag, $scope.allSurveyQuestions[0].nodocumentflag, $scope.allSurveyQuestions[0].yesdocumentid, $scope.allSurveyQuestions[0].nodocumentid, new Date(), "", $window.sessionStorage.roleId);
                    }
                }
            } else if ($scope.surveyQuestions != undefined && $scope.surveyAnswers != undefined && $scope.surveyAnswers.answer.toLowerCase() == 'yes') {
                $scope.modalInstanceLoad.close();
                //$scope.toggleValidation();
                //$scope.validatestring1 = "This Document is already been added";
                //alert("This Document is already been added");
                $scope.AtertFunction();
                /* $scope.modalOneAlert = $modal.open({
                     templateUrl: 'template/AlertModal.html'
                     , controller: 'ApplyForDocumentCtrl'
                         //, scope: $scope
                         
                     , backdrop: 'static'
                     , keyboard: false
                     , size: 'sm'
                     , windowClass: 'modal-danger'
                 });*/
                $route.reload();
            }
        } else {
            $scope.modalInstanceLoad.close();
            //$scope.toggleValidation();
            //$scope.validatestring1 = "This Document is already been added";
            $scope.AtertFunction();
            //alert("This Document is already been added");
            /* $scope.modalOneAlert = $modal.open({
                 templateUrl: 'template/AlertModal.html'
                 , controller: 'ApplyForDocumentCtrl'
                     //, scope: $scope
                     
                 , backdrop: 'static'
                 , keyboard: false
                 , size: 'sm'
                 , windowClass: 'modal-danger'
             });*/
            $route.reload();
        }
    }

    $scope.AtertFunction = function () {
        $scope.modalOneAlert = $modal.open({
            templateUrl: 'template/AlertModal.html',
            controller: 'ApplyForDocumentCtrl',
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
    }
    $scope.SaveAnswers = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, availeddate, referenceno, modifiedbyroleid) {
        $scope.surveyanswer = {};
        if (answer == 'yes') {
            $scope.surveyanswer.documentflag = yesdocumentflag;
            $scope.surveyanswer.documentid = yesdocumentid;
            console.log('$scope.surveyanswer.documentid1', $scope.surveyanswer.documentid);
            $scope.surveyanswer.schemeid = yesdocumentid;
        } else {
            $scope.surveyanswer.documentflag = nodocumentflag;
            $scope.surveyanswer.documentid = nodocumentid;
            $scope.surveyanswer.schemeid = nodocumentid;
            console.log('$scope.surveyanswer.documentid2', $scope.surveyanswer.documentid);
        }
        $scope.surveyanswer.beneficiaryid = beneficiaryid;
        $scope.surveyanswer.pillarid = pillarid;
        $scope.surveyanswer.questionid = questionid;
        $scope.surveyanswer.answer = answer;
        $scope.surveyanswer.lastupdatedtime = modifieddate;
        $scope.surveyanswer.lastupdatedby = modifiedby;
        $scope.surveyanswer.availeddate = availeddate;
        $scope.surveyanswer.referenceno = referenceno;
        $scope.surveyanswer.modifiedbyroleid = modifiedbyroleid;
        $scope.surveyanswer.documentflag = true;
        $scope.surveyanswer.stress_data = $scope.selectedBeneficiary.stress_data;
        $scope.submitsurveyanswers.post($scope.surveyanswer).then(function (resp) {
            console.log('answer Saved', resp);
            $scope.cdids = {};
            $scope.SaveDocument();
        }, function (error) {
            if (error.data.error.constraint == "pillar_question_beneficiary") {
                // console.log('error', error);
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid + '&filter[where][pillarid]=' + pillarid).getList().then(function (answered) {
                    if (answered.length > 0) {
                        $scope.surveyanswer.id = answered[0].id;
                        $scope.submitsurveyanswers.customPUT($scope.surveyanswer, answered[0].id).then(function (resp) {
                            $scope.SaveDocument();
                        });
                    }
                });
            }
        });
    };
    $scope.SaveAllAnswers = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, availeddate, referenceno, modifiedbyroleid) {
        $scope.surveyanswer = {};
        if (answer == 'yes') {
            $scope.surveyanswer.documentflag = yesdocumentflag;
            $scope.surveyanswer.documentid = yesdocumentid;
            console.log('$scope.surveyanswer.documentid1', $scope.surveyanswer.documentid);
            $scope.surveyanswer.schemeid = yesdocumentid;
        } else {
            $scope.surveyanswer.documentflag = nodocumentflag;
            $scope.surveyanswer.documentid = nodocumentid;
            $scope.surveyanswer.schemeid = nodocumentid;
            console.log('$scope.surveyanswer.documentid2', $scope.surveyanswer.documentid);
        }
        $scope.surveyanswer.beneficiaryid = beneficiaryid;
        $scope.surveyanswer.pillarid = pillarid;
        $scope.surveyanswer.questionid = questionid;
        $scope.surveyanswer.answer = answer;
        $scope.surveyanswer.lastupdatedtime = modifieddate;
        $scope.surveyanswer.lastupdatedby = modifiedby;
        $scope.surveyanswer.availeddate = availeddate;
        $scope.surveyanswer.referenceno = referenceno;
        $scope.surveyanswer.modifiedbyroleid = modifiedbyroleid;
        $scope.surveyanswer.stress_data = $scope.selectedBeneficiary.stress_data;
        $scope.submitsurveyanswers.post($scope.surveyanswer).then(function (resp) {
            console.log('answer Saved', resp);
            $scope.cdids = {};
        }, function (error) {
            if (error.data.error.constraint == "pillar_question_beneficiary") {
                // console.log('error', error);
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid + '&filter[where][pillarid]=' + pillarid).getList().then(function (answered) {
                    if (answered.length > 0) {
                        $scope.surveyanswer.id = answered[0].id;
                        $scope.submitsurveyanswers.customPUT($scope.surveyanswer, answered[0].id).then(function (resp) {});
                    }
                });
            }
        });
    };
    $scope.SaveDocument = function () {
            $scope.CreateClicked = true;
        $scope.documentmaster.stress_data = $scope.selectedBeneficiary.stress_data;
            Restangular.all('schememasters').post($scope.documentmaster).then(function (resp) {
                console.log('resp', resp);
                $scope.todo.reportincidentid = resp.id;
                $scope.auditlog.entityid = resp.id;
                if (resp.responserecieve == null || resp.responserecieve == undefined) {
                    var getresponserecieve = null;
                } else {
                    getresponserecieve = resp.responserecieve;
                }
                if (resp.rejection == null || resp.rejection == undefined) {
                    var getresponserejection = null;
                } else {
                    getresponserejection = resp.rejection;
                }
                if (resp.delay == null || resp.delay == undefined) {
                    var getresponsedelay = null;
                } else {
                    getresponsedelay = resp.delay;
                }
                var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Apply For Document Created With Following Details: ' + 'Member - ' + resp.memberId + ' , ' + 'Document  - ' + resp.schemeId + ' , ' + 'Stage  - ' + resp.stage + ' , ' + 'Response Recieve - ' + getresponserecieve + ' , ' + 'Respons For Rejection - ' + getresponserejection + ' , ' + 'Respons For Delay - ' + getresponsedelay + ' , ' + 'Date - ' + respdate;
                $scope.memberupdate = {
                    lastmeetingdate: new Date()
                }
                Restangular.all('beneficiaries/' + $window.sessionStorage.fullName).customPUT($scope.memberupdate).then(function (responsemember) {
                    console.log('responsemember', responsemember);
                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                        Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                            $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                            Restangular.all('todos').post($scope.todo).then(function (resptodos) {
                                //console.log('resptodos',resptodos);
                                $scope.modalInstanceLoad.close();
                                $scope.printmember = Restangular.one('beneficiaries', resp.memberId).get().$object;
                                $scope.printdocumenttype = Restangular.one('documenttypes', resp.schemeId).get().$object;
                                $scope.printstage = Restangular.one('schemestages', resp.stage).get().$object;
                                $scope.printdate = resp.datetime;
                                $scope.documentdataModal = !$scope.documentdataModal;
                            });
                        });
                    });
                });
            });
        }
        //Datepicker settings start
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    $scope.documentmaster.datetime = sevendays;
    $scope.today = function () {};
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmin = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.datetimehide = false;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;
    $scope.$watch('documentmaster.stage', function (newValue, oldValue) {
        var fifteendays = new Date();
        fifteendays.setDate(fifteendays.getDate() + 15);
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue) {
            return;
        } else if (newValue === '4') {
            $scope.reponserec = false;
            $scope.delaydis = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
        } else if (newValue === '5') {
            $scope.delaydis = false;
            $scope.reponserec = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.datetime = fifteendays;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '2') {
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = false;
            $scope.rejectdis = true;
            $scope.documentmaster.datetime = fifteendays;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '3') {
            $scope.documentmaster.datetime = fifteendays;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
        } else if (newValue === '7') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.documentmaster.datetime = new Date();
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
        } else if (newValue === '1') {
            $scope.documentmaster.datetime = fifteendays;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '8') {
            $scope.documentmaster.datetime = sevendays;
            $scope.datetimehide = false;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '6') {
            console.log('newValuedf', newValue);
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '9') {
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.datetime = new Date();
        } else {
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.documentmaster.delay = null;
            //$scope.documentmaster.datetime = '';
            $scope.datetimehide = false;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        }
    });

    $scope.$watch('documentmaster.memberId', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            Restangular.one('beneficiaries?filter[where][id]=' + newValue).get().then(function (benResp) {
                if (benResp.length > 0) {
                    $scope.selectedBeneficiary = benResp[0];
                } else {
                    $scope.selectedBeneficiary = {
                        stress_data: false
                    };
                }
            });
        }
    });

    $scope.rejectdis = true;
    $scope.$watch('documentmaster.responserecieve', function (newValue, oldValue) {
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 180);
        if (newValue === oldValue) {
            return;
        } else if (newValue === null) {
            return;
        } else if (newValue === 'Approved') {
            $scope.documentmaster.datetime = sevendays;
            $scope.rejectdis = true;
        } else if (newValue === 'Rejected') {
            $scope.rejectdis = false;
            $scope.documentmaster.datetime = new Date();
            $scope.datetimehide = true;
        } else {
            // $scope.documentmaster.datetime = '';
            $scope.rejectdis = true;
            $scope.documentmaster.rejection = null;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.documentmaster.delay = null;
            //$scope.documentmaster.datetime = '';
            $scope.datetimehide = false;
        }
    });
    /****************************** Language **********************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.applyforscheme = langResponse.applyforscheme;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.member = langResponse.member;
        $scope.document = langResponse.document;
        $scope.scheme = langResponse.scheme;
        $scope.stage = langResponse.stage;
        $scope.responsereceive = langResponse.responsereceive;
        $scope.responserejection = langResponse.responserejection;
        $scope.responsedelay = langResponse.responsedelay;
        $scope.followupdate = langResponse.followupdate;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.okbutton = langResponse.ok;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.palert = langResponse.alert;
        $scope.modalTitle = langResponse.createdetails;
        $scope.AlertMessage = langResponse.documentadded;
        $scope.thankyou = langResponse.thankyou;
        $scope.selmember = langResponse.selmember;
        $scope.seldocument = langResponse.seldocument;
        $scope.selstage = langResponse.selstage;
        $scope.selresponsererec = langResponse.selresponsererec;
        $scope.selreasondelay = langResponse.selreasondelay;
        //$scope.selpurposeofloan = langResponse.selpurposeofloan;
        $scope.selreasonrej = langResponse.selreasonrej;
        //$scope.enteramount = langResponse.enteramount;
        //$scope.selmonthrepay = langResponse.selmonthrepay;
        $scope.documentadded = langResponse.documentadded;
    });

});
/************************** NOt In Use ********************************
		
$scope.states = Restangular.all('zones').getList().$object;
$scope.genders = Restangular.all('genders').getList().$object;
$scope.educations = Restangular.all('educations').getList().$object;
$scope.submitdocumentmasters = Restangular.all('schememasters').getList().$object;
$scope.submittodos = Restangular.all('todos').getList().$object;
$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;

	if ($window.sessionStorage.roleId == 5) {
		Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
			$scope.comemberid = comember.id;
			$scope.partners1 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facilityId]=' + $scope.comemberid + '&filter={"where":{"lastmodifiedbyrole":{"inq":[5,6]}}}').getList().then(function (resPartner1) {
				$scope.partners = resPartner1;
			});
		});

	} else {
		$scope.partners2 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][lastmodifiedbyrole]=6').getList().then(function (resPartner2) {
			$scope.partners = resPartner2;
		});
	}*/
/******************************************************************************
				if ($window.sessionStorage.roleId == 5) {
					Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
						$scope.comemberid = comember.id;
						$scope.documentmaster.facilityId = comember.id;
						$scope.auditlog.facilityId = comember.id;
						$scope.getfacilityId = comember.id;
						
						$scope.part = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false').getList().then(function (part) {
							$scope.beneficiaries = part;
							angular.forEach($scope.beneficiaries, function (member, index) {
								member.index = index + 1;
							});
						});

					});
				} else {
					Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
						Restangular.one('comembers', fw.facility).get().then(function (comember) {
							$scope.comemberid = comember.id;
							$scope.documentmaster.facilityId = comember.id;
							$scope.auditlog.facilityId = comember.id;
							$scope.getfacilityId = comember.id;
						});
					});
					Restangular.all('beneficiaries?filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (part) {
						$scope.beneficiaries = part;
						angular.forEach($scope.beneficiaries, function (member, index) {
							member.index = index + 1;
						});
					});
				}
				
				
		$scope.sm = Restangular.all('documenttypes').getList().then(function (scheme) {
			$scope.printschemes = scheme;
				angular.forEach($scope.printschemes, function (member, index) {
				member.index = index;
				member.enabled = false;
			});
		});



/************************************************ Search On Modal *************************

 $scope.showForm = function () {
	     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
	     return visible;
	 };

	 $scope.isCreateView = function () {
	     if ($scope.showForm()) {
	         var visible = $location.path() === '/applyforschemes/create';
	         return visible;
	     }
	 };
	 $scope.hideCreateButton = function () {
	     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
	     return visible;
	 };

	 $scope.hideSearchFilter = function () {
	     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
	     return visible;
	 };

	$scope.Update = function () {
		$scope.submitdocumentmasters.customPUT($scope.documentmaster).then(function () {
			console.log('$scope.documentmasters', $scope.documentmasters);
			window.location = '/applyforschemes';
		});
	};


	$scope.$watch('documentmaster.memberId', function (newValue, oldValue) {
				//	$scope.getId = newValue;
				$scope.bbbb = Restangular.one('beneficiaries', newValue).get().then(function (resbes) {
					$scope.beneficiarysite = resbes.site;
				});
			});

			$scope.getGender = function (genderId) {
				return Restangular.one('genders', genderId).get().$object;
			};

			if ($routeParams.id) {
				Restangular.one('schemes', $routeParams.id).get().then(function (scheme) {
					$scope.original = scheme;
					$scope.documentmasters = Restangular.copy($scope.original);
					$scope.documentmasters.categ = scheme.category.split(",");
					$scope.documentmasters.agegrp = scheme.agegroup.split(",");
				});
			}

			$scope.DeleteFlag = function (id) {
				if (confirm("Are you sure want to delete..!") == true) {
					Restangular.one('schemes/' + id).remove($scope.documentmasters).then(function () {
						$route.reload();
					});

				} else {

				}

			}
	$scope.schemename = $scope.name;
	$scope.$watch('documentmaster.agegrp', function (newValue, oldValue) {
		if (newValue === oldValue) {
			return;
		} else {
			$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
				$scope.schemes = scheme;

				angular.forEach($scope.schemes, function (member, index) {
					member.index = index;
					member.enabled = false;
				});
			});
		}
	});

	$scope.$watch('documentmaster.gender', function (newValue, oldValue) {
		if (newValue === oldValue) {
			return;
		} else {
			$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
				$scope.schemes = scheme;

				angular.forEach($scope.schemes, function (member, index) {
					member.index = index;
					member.enabled = false;
				});
			});
		}
	});

	$scope.$watch('documentmaster.stateid', function (newValue, oldValue) {
		if (newValue === oldValue) {
			return;
		} else {
			$scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
				$scope.schemes = scheme;

				angular.forEach($scope.schemes, function (member, index) {
					member.index = index;
					member.enabled = false;
				});
			});
		}
	});

	$scope.showschemesModal = false;
	$scope.toggleschemesModal = function () {
		$scope.SaveScheme = function () {
			$scope.newArray = [];
			$scope.documentmaster.attendees = [];
			for (var i = 0; i < $scope.printschemes.length; i++) {
				if ($scope.printschemes[i].enabled == true) {
					$scope.newArray.push($scope.printschemes[i].index);
				}
			}
			$scope.documentmaster.attendees = $scope.newArray
			$scope.showschemesModal = !$scope.showschemesModal;
		};


		$scope.showschemesModal = !$scope.showschemesModal;
	};

	$scope.CancelScheme = function () {
		$scope.showschemesModal = !$scope.showschemesModal;
	};


	$scope.$watch('documentmaster.attendees', function (newValue, oldValue) {
	//console.log('watch.attendees', newValue);

	if (newValue === oldValue) {
		return;
	} else {
		if (newValue.length > oldValue.length) {
			// something was added
			var Array1 = newValue;
			var Array2 = oldValue;

			for (var i = 0; i < Array2.length; i++) {
				var arrlen = Array1.length;
				for (var j = 0; j < arrlen; j++) {
					if (Array2[i] == Array1[j]) {
						Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));

					}
				}
			}
			$scope.printschemes[Array1[0]].enabled = true;

			// do stuff
		} else if (newValue.length < oldValue.length) {
			// something was removed
			var Array1 = oldValue;
			var Array2 = newValue;

			for (var i = 0; i < Array2.length; i++) {
				var arrlen = Array1.length;
				for (var j = 0; j < arrlen; j++) {
					if (Array2[i] == Array1[j]) {
						Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
					}
				}
			}
			$scope.printschemes[Array1[0]].enabled = false;

		}
	}
	});
	*/