'use strict';

angular.module('secondarySalesApp')
    .controller('ReciptCreateCtrl', function ($scope, $rootScope, $filter, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        $scope.soheaderr = {
            fromdepotdist: '',
            sodate: $filter('date')(new Date(), 'd/M/yyyy'),
            flag: true
        };

        Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
            $scope.customers = cust;
        });

        Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catgresp) {
            $scope.categories = catgresp;
        });

        Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
        });

        // var splitArray = $window.sessionStorage.orgStructure.split(';');
        // var splitArray1 = splitArray[splitArray.length - 1];
        // var splitArray2 = splitArray1.split('-');
        // var splitArray2 = splitArray2[splitArray2.length - 1];
        // Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
            Restangular.all('poheaders?filter[where][flag]=true' + '&filter[where][transferflag]=true' + '&filter[where][receivedflag]=false' + '&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (po) {
                $scope.poheaders = po;
            });
        // });

        Restangular.all('itemdefinitions').getList().then(function (itms) {
            $scope.itemdefsdsplay = itms;
        });

        $scope.$watch('soheaderr.pono', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null) {

            } else {
                Restangular.all('potrailers?filter[where][poheaderId]=' + newValue).getList().then(function (potrailer) {
                    $scope.soheaderArray = potrailer;

                    $scope.totalamount = '0';

                    angular.forEach($scope.soheaderArray, function (value, index) {
                        delete value.id;
                        $scope.totalamount = parseInt($scope.totalamount) + parseInt(value.totalamount);

                        var data = $scope.itemdefsdsplay.filter(function (arr) {
                            return arr.id == value.itemcode
                        })[0];

                        if (data != undefined) {
                            value.itemname = data.itemcode;
                            value.name = data.id;
                        }
                    });
                    $scope.soheaderr.totalamount = $scope.totalamount;
                });

                Restangular.one('poheaders', newValue).get().then(function (poheader) {
                    $scope.soheaderr.fromdepotdist = poheader.fromdistret;
                    $scope.soheaderr.todistret = poheader.todepotdist;
                    $scope.soheaderr.toaddress = poheader.toaddress;
                    $scope.soheaderr.ponumber = poheader.ponumber;
                    $scope.soheaderr.state = poheader.stae;
                    $scope.soheaderr.city = poheader.city;
                    $scope.soheaderr.pin = poheader.pin;
                    $scope.soheaderr.discount = poheader.discount;
                    $scope.soheaderr.tax = poheader.tax;
                    $scope.soheaderr.netamount = poheader.netamount;
                    $scope.soheaderr.login = poheader.login;
                    $scope.mypoheaderid = poheader.id;

                    $scope.soheaderr.customerId = poheader.customerId;
                    $scope.soheaderr.categoryId = poheader.categoryId;
                    $scope.soheaderr.subcategoryId = poheader.subcategoryId;

                    var b = poheader.ponumber.replace(/[A-Z]/g, '');
                    $scope.soheaderr.sonumber = 'SO' + b;

                    Restangular.one('warehouses', poheader.fromdistret).get().then(function (whs) {
                        $scope.lwhCode = whs.warehousecode;

                        Restangular.one('warehouses', poheader.todepotdist).get().then(function (mwhs) {
                            $scope.vdrCode = mwhs.warehousecode;
                        });
                    });
                });
            }
        });

        $scope.removeRouteLinkItem = function (index) {
            var item = $scope.soheaderArray[index];
            $scope.soheaderArray.splice(index, 1);
            //console.log('rem obj', JSON.stringify(item));
            if (item && item.id !== undefined) {
                //  console.log('existing');
                Restangular.one('sotrailers', item.id).get().then(function (salesorder) {
                    salesorder.remove();
                });
            } else {
                // console.log('does not exist');
            }
        };

        $scope.lowChanged = function (scope, index) {
            // auto filling
            if (parseInt($scope.soheaderArray[index].quantityaccepted) > parseInt($scope.soheaderArray[index].quantity)) {
                $scope.soheaderArray[index].quantityaccepted = '';
            }
        };

        /** save so ***/

        $scope.poheaderupdate = {
            receivedflag: true
        };

        $scope.inheader = {};

        $scope.Save = function () {
            $scope.soheaderr.flag = true;
            $scope.soheaderr.approvedflag = true;

            Restangular.all('soheaders').post($scope.soheaderr).then(function (res) {
                Restangular.one('poheaders', $scope.mypoheaderid).customPUT($scope.poheaderupdate).then(function () {
                    $scope.saveCount = 0;
                    $scope.saveItems(res.id);
                });
            });
        };

        $scope.saveCount = 0;

        $scope.saveItems = function (headerId) {
            if ($scope.saveCount < $scope.soheaderArray.length) {
                $scope.soheaderArray[$scope.saveCount].sonumber = $scope.soheaderr.sonumber;
                $scope.soheaderArray[$scope.saveCount].soheaderid = headerId;
                $scope.soheaderArray[$scope.saveCount].customerId = $scope.soheaderr.customerId;
                $scope.soheaderArray[$scope.saveCount].categoryId = $scope.soheaderr.categoryId;
                $scope.soheaderArray[$scope.saveCount].subcategoryId = $scope.soheaderr.subcategoryId;

                Restangular.all('sotrailers').post($scope.soheaderArray[$scope.saveCount]).then(function (resp) {
                    $scope.inheader.itemstatusId = 3;
                    $scope.inheader.quantityinput = resp.quantityaccepted;
                    $scope.inheader.quantitytransfer = 0;
                    $scope.inheader.quantityreturn = 0;
                    $scope.inheader.customerId = $scope.soheaderr.customerId;
                    $scope.inheader.categoryId = $scope.soheaderr.categoryId;
                    $scope.inheader.subcategoryId = $scope.soheaderr.subcategoryId;
                    $scope.inheader.soheaderid = headerId;
                    $scope.inheader.sotrailerid = resp.id;
                    $scope.inheader.itemdefinitionId = resp.name;
                    $scope.inheader.warehouseId = $scope.soheaderr.fromdepotdist;

                    Restangular.all('inventories').post($scope.inheader).then(function (resp) {
                        $scope.saveCount++;
                        $scope.saveItems(headerId);
                    });
                });
            } else {
                window.location = '/recipt';
            }
        };
    });