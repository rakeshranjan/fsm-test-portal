'use strict';

angular.module('secondarySalesApp')
    .controller('OrganisationLocationCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/organisationlocation/create' || $location.path() === '/organisationlocation/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/organisationlocation/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/organisationlocation/create' || $location.path() === '/organisationlocation/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/organisationlocation/create' || $location.path() === '/organisationlocation/edit/' + $routeParams.id;
            return visible;
        };

        $scope.$watch('todo.rootid', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == oldValue) {
                return;
            } else {
                $scope.organisationlevels1 = [];

                Restangular.one('organisationlocations/findOne?filter[where][id]=' + newValue).get().then(function (orglocale) {
                    Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true' + '&filter[where][id]=' + orglocale.level).getList().then(function (tfs) {
                        $scope.organisationlevels1.push(tfs[0]);

                        Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC' + '&filter[where][rootid]=' + orglocale.level).getList().then(function (tdos) {
                            angular.forEach(tdos, function (member, index) {
                                $scope.organisationlevels1.push(member);
                            });
                            setTimeout(function () {
                                if ($scope.todoLevel) {
                                    $scope.todo.level = $scope.todoLevel;
                                    // console.log($scope.todo);
                                }
                            }, 1000);
                        });
                    });
                });
            }
        });

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/organisationlocation" || $window.sessionStorage.prviousLocation != "partials/organisationlocation") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /****************************************************************************/

        $scope.todo = {
            member: '',
            followUp: '',
            parent: 0,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };
        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (language) {
            $scope.languages = language;
        });

        $scope.levels = Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().$object;

        $scope.organisationlevels1 = Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[where][parent]=0&filter[order]=slno ASC').getList().$object;

        /************************************* INDEX ***************************************************/

        Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (tdo) {
            $scope.organisationlocations = tdo;
            angular.forEach($scope.organisationlocations, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 99).getList().then(function (country) {
        //     $scope.countries = country;
        // });
        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + 0).getList().then(function (country) {
            $scope.countries = country;
        });

        if (window.sessionStorage.roleId == 1) {
            Restangular.all('customers?filter[where][deleteFlag]=false').getList().then(function (cust) {
                $scope.customers = cust;
            });
        } else if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4) {
            Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + window.sessionStorage.customerId).getList().then(function (cust) {
                $scope.customers = cust;
            });
        }

        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=0&filter[where][languageparent]=true').getList().then(function (roots) {
            $scope.organisationroots = roots;
        });

        $scope.countryId = '';
        $scope.zoneId = '';
        $scope.stateId = '';
        $scope.cityId = '';
        $scope.customerId = '';

        $scope.$watch('rootId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.organisationlevels = [];

                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][rootid]=' + newValue).getList().then(function (ogrlocsresp) {
                    $scope.organisationlocations = ogrlocsresp;
                    angular.forEach($scope.organisationlocations, function (organisationlevel, index) {
                        organisationlevel.index = index + 1;
                    });
                });

                Restangular.one('organisationlocations/findOne?filter[where][id]=' + newValue).get().then(function (orglocale) {
                    Restangular.one('organisationlevels/findOne?filter[where][id]=' + orglocale.level).get().then(function (orglvl) {
                        Restangular.all('organisationlevels?filter[where][rootid]=' + orglvl.id + '&filter[where][languageparent]=true&filter[where][deleteflag]=false' + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
                            angular.forEach(ogrlevls, function (data, index) {
                                $scope.organisationlevels.push(data);
                            });

                            angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + organisationlevel.id).getList().then(function (ogrlocs) {
                                    organisationlevel.index = index;
                                    organisationlevel.organisationlocations = ogrlocs;
                                });
                            });
                        });
                    });
                });
            }
        });

        $scope.orgLevelChange = function (index, level) {
            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
                if (level) {
                    $scope.organisationlocations = ogrlocsresp;
                    angular.forEach($scope.organisationlocations, function (organisationlevel, index) {
                        organisationlevel.index = index + 1;
                    });
                }

                if (index < $scope.organisationlevels.length && level.length > 0) {
                    $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
                }
            });
        };

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.zoneId = '';
                $scope.stateId = '';
                $scope.cityId = '';
                // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parentlevel]=' + 36 + '&filter[where][parent]=' + newValue).getList().then(function (state) {
                //     $scope.states = state;

                // });
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (zoneResp) {
                    $scope.zones = zoneResp;

                });
                //  Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (tdo) {
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][rootid]=' + newValue).getList().then(function (tdo) {
                    //  Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 84).getList().then(function (tdo) {
                    $scope.organisationlocations = tdo;
                    angular.forEach($scope.organisationlocations, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
                $scope.countiesid = +newValue;
            }
        });
        $scope.orgZones = [];
        $scope.$watch('zoneId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.stateId = '';
                $scope.cityId = '';
                // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parentlevel]=' + 36 + '&filter[where][parent]=' + newValue).getList().then(function (state) {
                //     $scope.states = state;

                // });
                // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 105 + '&filter[where][parent]=' + newValue).getList().then(function (stateResp) {
                //     $scope.states = stateResp;

                // });
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (stateResp) {
                    $scope.states = stateResp;

                });
                //  Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (tdo) {
                //   //  Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 84).getList().then(function (tdo) {
                //     $scope.organisationlocations = tdo;
                //     angular.forEach($scope.organisationlocations, function (member, index) {
                //         member.index = index + 1;

                //         $scope.TotalTodos = [];
                //         $scope.TotalTodos.push(member);
                //     });
                // });
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (tdo) {
                    // Restangular.all('organisationlocations?filter={"where":{"and":[{"parent":{"inq":['+ newValue +']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (tdo) {
                    $scope.organisationlocations = tdo;
                    $scope.orgZones.push($scope.organisationlocations);
                    console.log("States: ", $scope.organisationlocations);
                    for (var i = 0; i < $scope.organisationlocations.length; i++) {
                        // $scope.orgLocations.push($scope.organisationlocations[i].id);
                        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + $scope.organisationlocations[i].id).getList().then(function (tdolocations) {
                            $scope.Locations = tdolocations;
                            $scope.orgZones.push($scope.Locations);
                        });
                        console.log("Organisation Locations:", $scope.orgZones);
                        angular.forEach($scope.orgZones, function (member, index) {
                            member.index = index + 1;

                            $scope.TotalTodos = [];
                            $scope.TotalTodos.push(member);
                        });
                    }

                });
                $scope.zonesid = +newValue;
            }
        });

        $scope.orgLocations = [];
        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.cityId = '';
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parentlevel]=' + 105 + '&filter[where][parent]=' + newValue).getList().then(function (city) {
                    $scope.cities = city;

                });
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (tdo) {
                    // Restangular.all('organisationlocations?filter={"where":{"and":[{"parent":{"inq":['+ newValue +']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (tdo) {
                    $scope.organisationlocations = tdo;
                    $scope.orgLocations.push($scope.organisationlocations);
                    console.log("States: ", $scope.organisationlocations);
                    for (var i = 0; i < $scope.organisationlocations.length; i++) {
                        // $scope.orgLocations.push($scope.organisationlocations[i].id);
                        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + $scope.organisationlocations[i].id).getList().then(function (tdolocations) {
                            $scope.Locations = tdolocations;
                            $scope.orgLocations.push($scope.Locations);
                        });
                        console.log("Organisation Locations:", $scope.orgLocations);
                        angular.forEach($scope.orgLocations, function (member, index) {
                            member.index = index + 1;

                            $scope.TotalTodos = [];
                            $scope.TotalTodos.push(member);
                        });
                    }
                    // console.log("Organisation Locations:", $scope.orgLocations);
                    // angular.forEach($scope.organisationlocations, function (member, index) {
                    //     member.index = index + 1;

                    //     $scope.TotalTodos = [];
                    //     $scope.TotalTodos.push(member);
                    // });
                });
                $scope.statesid = +newValue;
            }
        });

        $scope.$watch('cityId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue).getList().then(function (tdo) {
                    $scope.organisationlocations = tdo;
                    console.log("Cities: ", $scope.organisationlocations);
                    angular.forEach($scope.organisationlocations, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
                $scope.citiesid = +newValue;
            }
        });




        $scope.validatestring = "";



        $scope.Save = function (clicked) {

            if ($scope.todo.name == '' || $scope.todo.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            } else if ($scope.todo.level == '' || $scope.todo.level == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Level';
            } else if ($scope.hideparentlevel == false && ($scope.todo.parentlevel == '' || $scope.todo.parentlevel == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Parent Level';
            } else if ($scope.hideparentlevel == false && ($scope.todo.parent == '' || $scope.todo.parent == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Parent Location Name';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.todo.language = 1;
                $scope.todo.languageparent = true;

                $scope.LangLocations = [];
                Restangular.all('organisationlocations').post($scope.todo).then(function (resp) {
                    window.location = '/organisationlocation';
                });
            }
        };

        $scope.Update = function (clicked) {

            if ($scope.todo.name == '' || $scope.todo.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('organisationlocations', $routeParams.id).customPUT($scope.todo).then(function (resp) {
                    window.location = '/organisationlocation';
                });
            }
        };


        if ($routeParams.id) {

            $scope.message = 'Location has been updated!';

            Restangular.one('organisationlocations', $routeParams.id).get().then(function (td) {
                $scope.original = td;
                Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (resp) {
                    $scope.parentorganisationlocations = resp;
                    $scope.todo.parent = $scope.todo.parent;
                });
                $scope.todo = Restangular.copy($scope.original);
                $scope.todoLevel = td.level;
            });
        } else {
            $scope.message = 'Location has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('organisationlocations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getParent = function (locationid) {
            if (locationid != undefined && locationid != '' && locationid != null) {
                return Restangular.one('organisationlocations', locationid).get().$object;
            } else {
                return {
                    name: "ROOT"
                }
            }
        };

        $scope.getLevel = function (levelid) {
            return Restangular.one('organisationlevels', levelid).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.$watch('todo.parentlevel', function (newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            } else {
                Restangular.all('organisationlocations?filter[where][level]=' + newValue).getList().then(function (resp) {
                    $scope.parentorganisationlocations = resp;
                    $scope.todo.parent = $scope.todo.parent;
                });
            }
        });

        $scope.hideparentlevel = true;
        $scope.$watch('todo.level', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.all('organisationlevels?filter[where][id]=' + newValue).getList().then(function (resp) {
                    if (resp[0].parent == null || resp[0].parent == 0) {
                        $scope.hideparentlevel = true;
                    } else {
                        $scope.hideparentlevel = false;
                        $scope.todo.parentlevel = resp[0].parent;
                    }
                });
            }
        });


        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });
