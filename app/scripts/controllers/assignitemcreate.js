'use strict';

angular.module('secondarySalesApp')
    .controller('AssignitemCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.assigneditem = {};

        $scope.$watch('assigneditem.customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });
            }
        });

        $scope.$watch('assigneditem.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });
            }
        });

        $scope.$watch('assigneditem.subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                var splitArray = $window.sessionStorage.orgStructure.split(';');
                var splitArray1 = splitArray[splitArray.length - 1];
                var splitArray2 = splitArray1.split('-');
                var splitArray2 = splitArray2[splitArray2.length - 1];
                $scope.userDisplay = [];
                Restangular.all('users?filter[where][isPartner]=true' + '&filter[where][customerId]=' + $scope.assigneditem.customerId + '&filter[where][subcategoryId]=' + newValue + '&filter[where][roleId]=' + 33).getList().then(function (userresp) {
                    angular.forEach(userresp, function (resp, index) {
                        var arr = resp.orgStructure.split(';');
                        var arr1 = arr[arr.length - 1].split('-');
                        var arr2 = arr1[arr1.length - 1].split(',');
                        var filterObj = arr2.filter(function (e) {
                            return e == splitArray2;
                        });
                        if (filterObj.length > 0) {
                            $scope.userDisplay.push(resp);
                        }
                    });
                });

                $scope.itemdefinitions = Restangular.all('itemdefinitions?filter[where][subcategoryId]=' + newValue).getList().$object;

                var splitArray = $window.sessionStorage.orgStructure.split(';');
                var splitArray1 = splitArray[splitArray.length - 1];
                var splitArray2 = splitArray1.split('-');
                var splitArray2 = splitArray2[splitArray2.length - 1];
                Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
                    Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][site][neq]=0' + '&filter[where][id]=' + whs[0].id).getList().then(function (lwh) {
                        $scope.warehouselwh = lwh;
                        $scope.warehousename = lwh[0].warehousecode;
                        $scope.assigneditem.date = new Date();
                        $scope.assigneditem.warehouseid = lwh[0].id;
                    });
                });
            }
        });

        //Datepicker settings start

        $scope.minDate = new Date();

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end

        /*** array rows ***/

        $scope.myArray = [{
            itemcode: '',
            disabled: true
        }];

        $scope.addNew = function () {
            $scope.myArray.push({
                itemcode: '',
                disabled: true
            });
        };

        $scope.removeItem = function (index) {
            $scope.myArray.splice(index, 1);
        };

        $scope.getitem = function (index, id, oldval) {

            $scope.inventoriesdisplay = '';

            Restangular.one('itemdefinitions', id).get().then(function (itmdf) {

                Restangular.all('inventories?filter[where][warehouseId]=' + $scope.assigneditem.warehouseid + '&filter[where][itemstatusId]=' + 3 + '&filter[where][itemdefinitionId]=' + id).getList().then(function (inv) {
                    $scope.inventoriesdisplay = inv;
                    $scope.myArray[index].disabled = false;
                    $scope.myArray[index].inventoryList = $scope.inventoriesdisplay;

                    angular.forEach($scope.inventoriesdisplay, function (member, index) {
                        member.itemname = itmdf.itemcode;
                        member.myquantity = member.quantityinput - (member.quantitytransfer);
                        member.myquantity1 = member.quantityinput - (member.quantitytransfer);
                        member.newquantity = 0;
                        member.inputdisable = member.myquantity == 0 ? true : false;
                        // console.log(member);
                    });
                });
            });
        };

        $scope.checkAll = function () {
            if ($scope.selectedAll) {
                $scope.selectedAll = false;
            } else {
                $scope.selectedAll = true;
            }
            angular.forEach($scope.assignitem, function (soheader) {
                soheader.enabled = $scope.selectedAll;
            });
        };

        $scope.modelVisible = false;

        $scope.invarray = [];

        $scope.toggleModal1 = function (index, item, inv, invList) {

            $scope.inventories = invList;

            $scope.modelVisible = true;

            $scope.SaveScheme = function () {

                $scope.poheaderid = [];
                $scope.idvalue = [];
                $scope.newpoheaderid = 0;
                $scope.newsumvalue = 0;

                for (var i = 0; i < $scope.inventories.length; i++) {
                    $scope.newsumvalue = $scope.inventories[i].newquantity - (-$scope.newsumvalue);
                    $scope.myArray[index].quantityinput = $scope.newsumvalue;
                    if ($scope.inventories[i].newquantity > 0) {
                        $scope.myArray[index].linkId = $scope.inventories[i].id;
                        // console.log('myArray', $scope.myArray);
                    }
                    $scope.modelVisible = false;

                    $scope.inventories[i].myquantitytransfer = $scope.inventories[i].quantitytransfer;
                    $scope.invarray.push($scope.inventories[i]);
                }
            };
        };

        $scope.Cancel = function () {
            $scope.modelVisible = false;
        };

        $scope.changequantity = function (id, index, value) {
            var myQty = 0;
            $timeout(function () {
                if (value > $scope.inventories[index].myquantity1) {
                    alert('your entered quantity is more than current quantity');
                    $scope.inventories[index].newquantity = myQty;
                    $scope.inventories[index].myquantity = $scope.inventories[index].myquantity1;
                } else {
                    myQty = $scope.inventories[index].myquantity1 - (value);
                    $scope.inventories[index].myquantity = myQty;
                }
            }, 350);
        };

        $scope.inventorycreate = {};

        $scope.Save = function () {
            $scope.mysaveFunc1();
        };

        var cnt1 = 0;

        $scope.mysaveFunc1 = function () {

            if (cnt1 < $scope.myArray.length) {

                // $scope.assigneditem.managerid = $scope.salesManager.id;
                $scope.assigneditem.quantitytransfer = 0;
                $scope.assigneditem.quantityused = 0;
                $scope.assigneditem.quantityinput = $scope.myArray[cnt1].quantityinput;
                $scope.assigneditem.itemdefinitionid = $scope.myArray[cnt1].itemcode;
                $scope.assigneditem.linkId = $scope.myArray[cnt1].linkId;
                $scope.assigneditem.itemstatusId = 6;

                Restangular.all('assigneditems').post($scope.assigneditem).then(function (response) {
                    $scope.inventorycreate.quantityinput = response.quantityinput;
                    $scope.inventorycreate.quantitytransfer = 0;
                    $scope.inventorycreate.quantityreturn = 0;
                    $scope.inventorycreate.warehouseflag = false;
                    $scope.inventorycreate.warehouseId = $scope.assigneditem.partnerId;
                    $scope.inventorycreate.itemdefinitionId = response.itemdefinitionid;
                    $scope.inventorycreate.itemstatusId = 3;
                    $scope.inventorycreate.linkId = $scope.myArray[cnt1].linkId;
                    $scope.inventorycreate.approvedflag = true;
                    $scope.inventorycreate.customerId = $scope.assigneditem.customerId;
                    $scope.inventorycreate.categoryId = $scope.assigneditem.categoryId;
                    $scope.inventorycreate.subcategoryId = $scope.assigneditem.subcategoryId;

                    Restangular.all('inventories').post($scope.inventorycreate).then(function (invresp) {
                        cnt1++;
                        $scope.mysaveFunc1();
                    });
                });
            } else {
                $scope.inventoryUpdate();
            }
        };

        var mycount = 0;

        $scope.inventoryUpdate = function () {

            if (mycount < $scope.invarray.length) {

                $scope.invarray[mycount].quantitytransfer = parseInt($scope.invarray[mycount].myquantitytransfer) + parseInt($scope.invarray[mycount].newquantity);

                Restangular.one('inventories', $scope.invarray[mycount].id).customPUT($scope.invarray[mycount]).then(function () {
                    mycount++;
                    $scope.inventoryUpdate();
                });
            } else {
                window.location = '/assignitem';
            }
        };
    });