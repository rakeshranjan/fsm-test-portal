'use strict';

angular.module('secondarySalesApp')
  .controller('RouteassignmentUpdateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $http, $timeout,$filter,$route) {
      
        $scope.item = [{
            id:'',
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          }]
        
       $scope.tourtypes = Restangular.all('tourtypes').getList().$object;
      $scope.routelinks = Restangular.all('routelinks').getList().$object;
      $scope.routes = Restangular.all('distribution-routes').getList().$object;
      
       Restangular.one('routeassignments', $routeParams.id).get().then(function(routeassignment){
         $scope.item[0].id = routeassignment.id;
                  $scope.item[0].partnerId = routeassignment.partnerId;
                  $scope.item[0].date = routeassignment.date;
                  $scope.item[0].distributionRouteId = routeassignment.distributionRouteId;
                  $scope.item[0].tourtype = routeassignment.tourtype;
           //$scope.routes = Restangular.all('routelinkviews?filter[where][partnerid]='+routeassignment.partnerId).getList().$object;
        });
        
     
      // $scope.routeassignments = Restangular.one('routeassignments').get().$object;
      
      
     
    
      //Datepicker settings start
        $scope.today = function() {
            $scope.dt = new Date();
          };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function() {
            $scope.showWeeks = !$scope.showWeeks;
          };

        $scope.clear = function() {
            $scope.dt = null;
          };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
          };

        $scope.toggleMin = function() {
            $scope.minDate = ($scope.minDate) ? null : new Date();
          };
        $scope.toggleMin();

        $scope.open = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened = true;
          };
      
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
          };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
       
      /*********/
       
        $scope.removeRouteLinkItem = function(index) {
            var item = $scope.poheader.sample[index];
            $scope.poheader.sample.splice(index, 1);
            console.log('rem obj', JSON.stringify(item));
            if (item && item.id !== undefined) {
              console.log('existing');
              Restangular.one('routeassignments', item.id).get().then(function(purchaseorder) {
                    purchaseorder.remove();
                  });
            } else {
              console.log('does not exist');
            }
          };
      
       $scope.deleteRouteLinks = function(value) {
            
            console.log('$scope.item[0].id',$scope.item[0].id);
            console.log('$scope.item',$scope.item);
            //Restangular.one('routeassignments/'+$scope.item[0].id).delete($scope.item[0]);
              Restangular.one('routeassignments', $scope.item[0].id).get().then(function(purchaseorder) {
                    purchaseorder.remove();
                  });
            $route.reload();
          };

        $scope.addRouteLinks = function(value) {
            
            console.log('$scope.item[0].id',$scope.item[0].id);
            console.log('$scope.item',$scope.item);
            Restangular.one('routeassignments/'+$scope.item[0].id).customPUT($scope.item[0]);
            
               /* if($scope.item[0].date != ''&& $scope.item[0].partnerId!='' && $scope.item[0].tourtype!=''){
                    if ($scope.item[0].id!='') {
                  Restangular.one('routeassignments/'+$scope.item[0].id).customPUT($scope.item);
                        console.log('routeassignments updated', $scope.item[0]);
                } else {
                   $scope.routeassignments.post($scope.item[0]).then(function(){
                console.log('routeassignments saved', $scope.item[0]);
                    });
                }
                   
            }
           
           /* angular.forEach($scope.poheader.sample, function(value, key) {
                console.log(key + ': ' + JSON.stringify(value));
                console.log('$scope.poheader.sample[' + key + ']: ', $scope.poheader.sample[key]);
                if (value.id) {
                  Restangular.all('routeassignments').one(value.id).customPUT($scope.poheader.sample[key]);
                } else {
                  $scope.routeassignments.post($scope.poheader.sample[key]);
                }
                $route.reload();
              });*/
            $route.reload();
          };
      

        $scope.routeassignments = Restangular.all('routeassignments').getList().$object;
      
        $scope.getrouteassignment = function(partnerId){
          return Restangular.one('routeassignments', partnerId).get().$object;
        };
    
       

      });

