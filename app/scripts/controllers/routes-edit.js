'use strict';

angular.module('secondarySalesApp')
	.controller('RoutesEditCtrl', function ($scope, Restangular, $routeParams, $window) {
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
		$scope.isCreateView = false;
		$scope.Sitedistricdisable = true;
		$scope.Sitedistricdisable = true;
		$scope.Sitefacilitydisable = true;
		$scope.heading = 'Site Edit';
		$scope.routes = Restangular.all('distribution-routes').getList().$object;
		$scope.salesAreas = Restangular.all('sales-areas').getList().$object;
		$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
		$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;


		if ($routeParams.id) {
			Restangular.one('distribution-routes', $routeParams.id).get().then(function (route) {
				$scope.original = route;

				$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.zoneId + '&filter[where][deleteflag]=false').getList().then(function (salearea) {
					$scope.salesAreas = salearea;
				
					$scope.partners = Restangular.all('employees?filter[where][stateId]=' + $scope.original.zoneId + '&filter[where][district]=' + $scope.original.salesAreaId + '&filter[where][deleteflag]=false').getList().then(function (partner) {
						$scope.facilities = partner;
						$scope.route = Restangular.copy($scope.original);
					});
				});
			});
		};

		
		$scope.$watch('route.zoneId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
				return;
			} else {
				$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
			}
		});

		$scope.$watch('route.salesAreaId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
				return;
			} else {
				$scope.facilities = Restangular.all('employees?filter[where][stateId]=' + $scope.zonalid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
			}
		});

		
		/****************************************** Update *******************************************/
		$scope.modalTitle = 'Thank You';
		$scope.message = 'Site has been updated';
		$scope.showValidation = false;   
		$scope.validatestring = '';
		/*$scope.Update = function () {
			$scope.routes.customPUT($scope.route).then(function () {
				console.log('$scope.route', $scope.route);
				window.location = '/routes';
			});
		};*/

		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			
			if ($scope.route.name == '' || $scope.route.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Site Name';
				document.getElementById('name').style.borderColor = "#FF0000";
				
			} else if ($scope.route.zoneId == '' || $scope.route.zoneId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select State';
				//document.getElementById('state').style.borderColor = "#FF0000";
				
			} else if ($scope.route.salesAreaId === '' || $scope.route.salesAreaId === null) {
				$scope.validatestring = $scope.validatestring + 'Please Select District';
				//document.getElementById('state').style.borderColor = "#FF0000";
				
			} else if ($scope.route.partnerId == '' || $scope.route.partnerId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Facility';
				//document.getElementById('state').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.one('distribution-routes', $routeParams.id).customPUT($scope.route).then(function (znResponse) {	
					console.log('$scope.route', $scope.znResponse);
					window.location = '/routes';
					//$location.path('/routes');
				});
			}
		};
	
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

	});
