'use strict';

angular.module('secondarySalesApp')
    .controller('ArchiveCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/archivemember/create' || $location.path() === '/archivemember/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/archivemember/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/archivemember/create' || $location.path() === '/archivemember/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/archivemember/create' || $location.path() === '/archivemember/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/archivemember") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.noofterms = Restangular.all('noofterms').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'archive member has been Updated!';
            Restangular.one('archivemembers', $routeParams.id).get().then(function (noofterm) {

                $scope.original = noofterm;
                // console.log('$scope.original', $scope.original);
                $scope.archivemember = Restangular.copy($scope.original);
                // console.log('$scope.archivemember', $scope.archivemember);
            });
        } else {
            $scope.message = 'archive member has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('archivemembers?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.archivemembers = zn;
            //console.log('$scope.noofterms',$scope.noofterms);
            angular.forEach($scope.archivemembers, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.archivemember = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.archivemember.name == '' || $scope.archivemember.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.hnname == '' || $scope.archivemember.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.knname == '' || $scope.archivemember.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.taname == '' || $scope.archivemember.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.tename == '' || $scope.archivemember.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.mrname == '' || $scope.archivemember.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.archivemembers.post($scope.archivemember).then(function () {
                    window.location = '/archivemember';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.archivemember.name == '' || $scope.archivemember.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.hnname == '' || $scope.archivemember.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.knname == '' || $scope.archivemember.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.taname == '' || $scope.archivemember.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.tename == '' || $scope.archivemember.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.archivemember.mrname == '' || $scope.archivemember.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter reason for archive Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('archivemembers', $routeParams.id).customPUT($scope.archivemember).then(function () {
                    console.log('archive member Saved');
                    window.location = '/archivemember';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('archivemembers/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
