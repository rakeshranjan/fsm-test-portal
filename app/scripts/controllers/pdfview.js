'use strict';

angular.module('secondarySalesApp')
  .controller('PdfviewCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

    Restangular.one('rcdetails', $routeParams.id).get().then(function (rcDetail) {
      $scope.rcDetail = rcDetail;

      $scope.info1Header = rcDetail.info1.split(':')[0];
      $scope.info1Trailer = rcDetail.info1.split(':')[1];

      $scope.info2Header = rcDetail.info2.split(':')[0];
      $scope.info2Trailer = rcDetail.info2.split(':')[1];

      $scope.info3Header = rcDetail.info3.split(':')[0];
      $scope.info3Trailer = rcDetail.info3.split(':')[1];

      $scope.info4Header = rcDetail.info4.split(':')[0];
      $scope.info4Trailer = rcDetail.info4.split(':')[1];

      $scope.info5Header = rcDetail.info5.split(':')[0];
      $scope.info5Trailer = rcDetail.info5.split(':')[1];

      $scope.info6Header = rcDetail.info6.split(':')[0];
      $scope.info6Trailer = rcDetail.info6.split(':')[1];

      $scope.info7Header = rcDetail.info7.split(':')[0];
      $scope.info7Trailer = rcDetail.info7.split(':')[1];

      $scope.info8Header = rcDetail.info8.split(':')[0];
      $scope.info8Trailer = rcDetail.info8.split(':')[1];

      $scope.info9Header = rcDetail.info9.split(':')[0];
      $scope.info9Trailer = rcDetail.info9.split(':')[1];

      $scope.info10Header = rcDetail.info10.split(':')[0];
      $scope.info10Trailer = rcDetail.info10.split(':')[1];
    });

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customers?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;

      Restangular.all('groupviews?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + cust[0].id).getList().then(function (gfo) {
        $scope.groupinfo = gfo;

        angular.forEach($scope.groupinfo, function (member, index) {
          member.index = index + 1;

          Restangular.all('surveyquestionviews?filter[where][deleteflag]=false' + '&filter[' + '&filter[where][customerId]=' + cust[0].id + '&filter[where][groupserialno]=' + member.groupid).getList().then(function (srvy) {
            member.surveyquestions = srvy;

            angular.forEach(member.surveyquestions, function (member, index) {
              member.index = index + 1;
              if (member.questiontype == 5) {
                member.answer = member.question;
              }

              Restangular.all('surveyanswers?filter[where][questionid]=' + member.id + '&filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
                angular.forEach(HRes, function (data, index) {
                  data.index = index + 1;
                  member.answer = data.answer;
                  if (data.answer == null || data.answer == undefined || data.answer == '') {
                    member.uploadedfilesList = [];
                  } else {
                    member.uploadedfilesList = member.answer.split(',');
                  }
                });
              });
            });
          });
        });
      });
    });

    $scope.myFunction = function () {
      window.print();
    };
    $scope.taskBack = function () {
      $location.path("/task");
    };
  });