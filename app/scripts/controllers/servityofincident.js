'use strict';

angular.module('secondarySalesApp')
	.controller('servityofincidentCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/
		//$scope.modalTitle = 'Thank You';
		//$scope.message = 'servityofincident has been created!';
		$scope.showForm = function () {
			var visible = $location.path() === '/servityofincident/create' || $location.path() === '/servityofincident/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/servityofincident/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/servityofincident/create' || $location.path() === '/servityofincident/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/servityofincident/create' || $location.path() === '/servityofincident/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		}


		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/servityofincident") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$window.sessionStorage.myRoute_currentPage = 1;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('servityofincidents?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.servityofincidents = part;
			$scope.servityofincidentId = $window.sessionStorage.sales_servityofincidentId;
			angular.forEach($scope.servityofincidents, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.servityofincident = {
			deleteflag: false
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
			$scope.message = 'Servity of Incident has been Updated!';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('servityofincidents', $routeParams.id).get().then(function (servityofincident) {
				$scope.original = servityofincident;
				$scope.servityofincident = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Servity of Incident has been Created!';
		}
		/************* SAVE *******************************************/

		$scope.servityofincident = {
			"name": '',
			"deleteflag": false
		};
		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Saveservityofincident = function (clicked) {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.servityofincident.name == '' || $scope.servityofincident.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.hnname == '' || $scope.servityofincident.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.knname == '' || $scope.servityofincident.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.taname == '' || $scope.servityofincident.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.tename == '' || $scope.servityofincident.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.mrname == '' || $scope.servityofincident.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('servityofincidents').post($scope.servityofincident).then(function (Response) {
					window.location = '/servityofincident';

				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/***************************** UPDATE *******************************************/
		$scope.Updateservityofincident = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.servityofincident.name == '' || $scope.servityofincident.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.hnname == '' || $scope.servityofincident.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.knname == '' || $scope.servityofincident.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.taname == '' || $scope.servityofincident.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.tename == '' || $scope.servityofincident.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.servityofincident.mrname == '' || $scope.servityofincident.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter servity of incident in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('servityofincidents', $routeParams.id).customPUT($scope.servityofincident).then(function () {
					window.location = '/servityofincident';
				});
			}
		};
		/*---------------------------Delete---------------------------------------------------*/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('servityofincidents/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


	});
