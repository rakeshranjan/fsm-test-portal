'use strict';

angular.module('secondarySalesApp')
    .controller('ReciptCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        // if ($window.sessionStorage.orgStructure) {
        //     var splitArray = $window.sessionStorage.orgStructure.split(';');
        //     var splitArray1 = splitArray[splitArray.length - 1];
        //     var splitArray2 = splitArray1.split('-');
        //     var splitArray2 = splitArray2[splitArray2.length - 1];
        //     Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
        //         Restangular.all('soheaders?filter[where][flag]=true' + '&filter[where][fromdepotdist]=' + whs[0].id).getList().then(function (so) {
        //             $scope.soheaders = so;
        //             angular.forEach($scope.soheaders, function (member, index) {
        //                 member.index = index + 1;
        //             });
        //         });
        //     });
        // }

        Restangular.all('soheaders?filter[where][flag]=true' + '&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (so) {
            $scope.soheaders = so;
            angular.forEach($scope.soheaders, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getPartner = function (partnerid) {
            return Restangular.one('partners', partnerid).get().$object;
        };

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.$watch('customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });

                Restangular.all('soheaders?filter[where][customerId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });

                Restangular.all('soheaders?filter[where][categoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('soheaders?filter[where][subcategoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (so) {
                    $scope.soheaders = so;
                    angular.forEach($scope.soheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.showViewForm = false;

        if ($routeParams.id) {
            $scope.showViewForm = true;

            Restangular.all('soheaders?filter[where][id]=' + $routeParams.id).getList().then(function (so) {
                $scope.soheadersList = so;
                angular.forEach($scope.soheadersList, function (member, index) {
                    member.index = index + 1;
                });
            });

            Restangular.all('sotrailers?filter[where][soheaderid]=' + $routeParams.id).getList().then(function (sotrailer) {
                $scope.sotrailersList = sotrailer;
                angular.forEach($scope.sotrailersList, function (member, index) {
                    member.index = index + 1;
                });
            });
        }

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getItem = function (itemid) {
            return Restangular.one('itemdefinitions', itemid).get().$object;
        };
    });