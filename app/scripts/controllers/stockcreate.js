'use strict';

angular.module('secondarySalesApp')
    .controller('StockcreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        $scope.isCreateView = false;

        $scope.modelVisible = false;

        $scope.hidewarehouse = false;

        // $scope.zones = Restangular.all('zones').getList().$object;

        $scope.enableField = true;

        $scope.employees = Restangular.all('employees').getList().$object;
        $scope.deliverynotes = Restangular.all('deliverynotes').getList().$object;
        $scope.soheaders = Restangular.all('soheaders').getList().$object;
        $scope.warehouses = Restangular.all('warehouses').getList().$object;

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catgresp) {
            $scope.categories = catgresp;
        });

        Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
        });

        Restangular.all('poheaders?filter[where][flag]=true' + '&filter[where][transferflag]=false').getList().then(function (pos) {
            $scope.poheaders = pos;
        });

        $scope.$watch('deliverynote.subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][site][neq]=0').getList().then(function (whouse) {
                    $scope.warehousemwh = whouse;
                });

                Restangular.all('partners?filter[where][subcategoryId]=' + newValue).getList().then(function (ptrs) {
                    $scope.partners = ptrs;
                });

                $scope.itemdefinitions = Restangular.all('itemdefinitions?filter[where][subcategoryId]=' + newValue).getList().$object;
            }
        });

        $scope.$watch('deliverynote.pono', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('poheaders', newValue).get().then(function (poheader) {
                    $scope.deliverynote.fromdepotdistname = poheader.fromdistret;
                    $scope.deliverynote.todistretname = poheader.todepotdist;
                    $scope.deliverynote.login = poheader.login;
                    $scope.deliverynote.ponumber = poheader.ponumber;
                    $scope.mypoheaderid = poheader.id;
                    $scope.mypoheaderno = poheader.ponumber;

                    $scope.deliverynote.customerId = poheader.customerId;
                    $scope.deliverynote.categoryId = poheader.categoryId;
                    $scope.deliverynote.subcategoryId = poheader.subcategoryId;

                    Restangular.one('warehouses', poheader.todepotdist).get().then(function (wrhse) {
                        $scope.mainwarehouseid = wrhse.id;
                        $scope.mwhcode = wrhse.warehousecode;
                        Restangular.one('warehouses', poheader.fromdistret).get().then(function (lwrhse) {
                            $scope.warehouseId = lwrhse.id;
                            $scope.lwhcode = lwrhse.warehousecode;
                            $scope.deliverynote.toaddress = lwrhse.address;
                            $scope.deliverynote.state = lwrhse.stateId;
                            $scope.deliverynote.city = lwrhse.cityId;
                            $scope.deliverynote.pin = lwrhse.zip;
                        });
                    });

                    Restangular.all('potrailers?filter[where][poheaderId]=' + newValue).getList().then(function (poheadersample) {
                        $scope.poArray = poheadersample;

                        angular.forEach($scope.poArray, function (povalue) {
                            Restangular.one('itemdefinitions', povalue.itemcode).get().then(function (itm) {
                                povalue.enableField = true;
                                povalue.itemid = itm.id;
                                povalue.name = itm.id;
                                povalue.itemname = itm.itemcode;

                                Restangular.all('inventories?filter[where][itemdefinitionId]=' + itm.id + '&filter[where][itemstatusId]=' + 3 + '&filter[where][warehouseId]=' + $scope.mainwarehouseid + '&filter[where][approvedflag]=true').getList().then(function (invry) {
                                    povalue.inventories = invry;
                                    povalue.enableField = false;
                                });
                            });
                        });
                    });
                });
            }
        });

        $scope.myheader = {};

        $scope.$watch('popupquantity', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                $scope.myvalue = newValue;
            }
        });

        $scope.Cancel = function () {
            $scope.modelVisible = false;
        };

        $scope.addRouteLinkItem = function () {
            $scope.poArray.push({
                //'poheaderId': $routeParams.id
            });
        };

        $scope.checkAll = function () {
            if ($scope.selectedAll) {
                $scope.selectedAll = false;
            } else {
                $scope.selectedAll = true;
            }
            angular.forEach($scope.inventories, function (soheader) {
                soheader.enabled = $scope.selectedAll;
            });
        };

        $scope.inventoryArray = [];

        $scope.toggleModal1 = function (index, item, inventory, num, itemfullname) {
            $scope.inventories = inventory;
            $scope.selectedAll = true;
            $scope.checkAll();

            if (num == undefined) {
                for (var c = 0; c < $scope.inventories.length; c++) {
                    $scope.inventories[c].itemname = item;
                    $scope.inventories[c].itemFullName = itemfullname;
                    $scope.inventories[c].newquantitytransfer = $scope.inventories[c].quantityinput - ($scope.inventories[c].quantitytransfer);

                    $scope.inventories[c].newquantitytransfer1 = $scope.inventories[c].quantityinput - ($scope.inventories[c].quantitytransfer);
                    $scope.inventories[c].currentquantitytransfer = $scope.inventories[c].quantitytransfer;
                }
            }

            $scope.modelVisible = true;

            $scope.SaveScheme = function () {

                $scope.poheaderid = [];
                $scope.idvalue = [];
                $scope.newpoheaderid = 0;
                $scope.newsumvalue = 0;

                for (var i = 0; i < $scope.inventories.length; i++) {

                    if ($scope.inventories[i].enabled == true) {

                        $scope.poheaderid.push($scope.inventories[i].newquantitytransfer);

                        $scope.idvalue.push($scope.inventories[i].id);

                        $scope.newsumvalue = $scope.newsumvalue + (+$scope.inventories[i].newquantitytransfer);

                        if ($scope.newpoheaderid < $scope.poArray[index].quantity) {

                            $scope.newpoheaderid = $scope.newpoheaderid + (+$scope.inventories[i].newquantitytransfer1);

                            $scope.quantitynew = $scope.newpoheaderid;

                            if ($scope.quantitynew > $scope.poArray[index].quantity) {
                                if ($scope.inventories.length == 1) {
                                    $scope.inventories[i].quantitytransfer = $scope.poArray[index].quantity;
                                    $scope.inventories[i].newquantitytransfer = $scope.inventories[i].newquantitytransfer - ($scope.inventories[i].quantitytransfer);
                                    $scope.inventories[i].newquantitytransfer1 = $scope.inventories[i].newquantitytransfer - ($scope.inventories[i].quantitytransfer);
                                    $scope.poArray[index].quantitydeliver = $scope.poArray[index].quantity;
                                } else {
                                    $scope.inventories[i].quantitytransfer = $scope.quantitynew + (-$scope.poArray[index].quantity);
                                    $scope.poArray[index].quantitydeliver = $scope.quantitynew + (-$scope.inventories[i].quantitytransfer);
                                }
                            } else {
                                $scope.inventories[i].quantitytransfer = $scope.quantitynew - ($scope.inventories[i].newquantitytransfer1);
                                $scope.poArray[index].quantitydeliver = $scope.newsumvalue;
                            }
                            $scope.inventories[i].currentQuantityDeliver = $scope.poArray[index].quantitydeliver;
                            $scope.inventoryArray.push($scope.inventories[i]);
                            $scope.idvalue.toString();
                            $scope.inventory.linkId = $scope.idvalue;
                            $scope.poArray[index].enableField = true;
                            $scope.poArray[index].linkId = $scope.idvalue;
                            $scope.modelVisible = false;
                        } else {
                            $scope.inventories[i].enabled = false;
                            $scope.modelVisible = false;
                        }
                    }
                    $scope.inventory.quantitytransfer = $scope.poArray[index].quantitydeliver;
                }
            };

        };

        $scope.removeRouteLinkItem = function (index) {
            $scope.poArray.splice(index, 1);
        };

        $scope.inventory = {};

        $scope.mypoheader = {
            transferflag: true
        };

        $scope.validatestring = '';

        $scope.addRouteLinks = function () {

            if ($scope.deliverynote.customerId == '' || $scope.deliverynote.customerId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Project';

            } else if ($scope.deliverynote.pono == '' || $scope.deliverynote.pono == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a P.O Number';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                Restangular.all('deliverynotes').post($scope.deliverynote).then(function () {
                    Restangular.one('poheaders', $scope.mypoheaderid).customPUT($scope.mypoheader).then(function (response) {
                        $scope.saveInvFunc();
                    });
                });
            }
        };

        var potrailercount = 0;

        $scope.potrailerupdate = {};

        $scope.updatePoTrailerFunc = function () {
            if (mycount < $scope.poArray.length) {
                $scope.potrailerupdate.qtydeliver = $scope.poArray[potrailercount].quantitydeliver;
                Restangular.one('potrailers', $scope.poArray[potrailercount].id).customPUT($scope.potrailerupdate).then(function (response) {
                    potrailercount++;
                    $scope.updatePoTrailerFunc();
                });
            } else {
                $scope.saveInvFunc();
            }
        };

        var mycount = 0;

        $scope.saveInvFunc = function () {

            if (mycount < $scope.poArray.length) {

                $scope.inventory.itemstatusId = 2;
                $scope.inventory.quantityinput = $scope.poArray[mycount].quantity;
                $scope.inventory.quantitytransfer = 0;
                $scope.inventory.quantityreturn = 0;
                $scope.inventory.warehouseId = $scope.warehouseId;
                $scope.inventory.approvedflag = false;
                $scope.inventory.warehouseflag = true;
                $scope.inventory.itemdefinitionId = $scope.poArray[mycount].itemid;
                $scope.inventory.soid = $scope.deliverynote.country;
                $scope.inventory.linkId = $scope.poArray[mycount].linkId;
                $scope.inventory.soheaderid = $scope.poArray[mycount].poheaderid;
                $scope.inventory.sotrailerid = $scope.poArray[mycount].id;
                $scope.inventory.customerId = $scope.deliverynote.customerId;
                $scope.inventory.categoryId = $scope.deliverynote.categoryId;
                $scope.inventory.subcategoryId = $scope.deliverynote.subcategoryId;

                if ($scope.poArray[mycount].quantitydeliver > 0) {
                    Restangular.all('inventories').post($scope.inventory).then(function (response) {
                        mycount++;
                        $scope.saveInvFunc();
                    });
                } else {
                    mycount++;
                    $scope.saveInvFunc();
                }
            } else {
                $scope.updateInvFunc();
            }
        };

        var newcount = 0;

        $scope.updateInvFunc = function () {
            if (newcount < $scope.inventoryArray.length) {
                //   $scope.inventoryArray[newcount].quantitytransfer = parseInt($scope.inventoryArray[newcount].quantityinput) - parseInt($scope.inventoryArray[newcount].quantitytransfer);
                $scope.inventoryArray[newcount].quantitytransfer = parseInt($scope.inventoryArray[newcount].currentQuantityDeliver) + parseInt($scope.inventoryArray[newcount].currentquantitytransfer);
                // console.log('$scope.inventoryArray[newcount]', $scope.inventoryArray[newcount]);
                Restangular.one('inventories', $scope.inventoryArray[newcount].id).customPUT($scope.inventoryArray[newcount]).then(function (invResp) {
                    newcount++;
                    $scope.updateInvFunc();
                });
            } else {
                window.location = "/stock";
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

    });