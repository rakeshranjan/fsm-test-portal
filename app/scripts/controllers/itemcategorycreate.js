'use strict';
angular.module('secondarySalesApp')
  .controller('ItemcategoryCreate', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $timeout) {
    /*********/
    //console.log("$routeParams.id-5", $routeParams.id)
    $scope.showForm = function () {
      var visible = $location.path() === '/itemcategorycreate' || $location.path() === '/itemcategory/edit/' + $routeParams.id;
      return visible;
    };

    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/itemcategorycreate';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/itemcategorycreate';
      return visible;
    };

    if ($location.path() === '/itemcategorycreate') {
      $scope.disableDropdown = false;
    } else {
      $scope.disableDropdown = true;
    }
    /************************************************************************************/
    $scope.itemcategory = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteflag: false
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    // ******************************** EOF Pagination *************************************************

    if ($window.sessionStorage.roleId != 2) {
      Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itemcatg) {
        $scope.itemcategories = itemcatg;
        angular.forEach($scope.itemcategories, function (member, index) {
          member.index = index + 1;

          Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
            $scope.customername = custmor;
            for (var j = 0; j < $scope.customername.length; j++) {
              if (member.customerId == $scope.customername[j].id) {
                member.customername = $scope.customername[j].name;
                break;
              }
            }
          });
          Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
            $scope.categoryname = categ;
            for (var j = 0; j < $scope.categoryname.length; j++) {
              if (member.categoryId == $scope.categoryname[j].id) {
                member.categoryname = $scope.categoryname[j].name;
                break;
              }
            }
          });
          Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
            $scope.subcategoryname = subcateg;
            for (var j = 0; j < $scope.subcategoryname.length; j++) {
              if (member.subcategoryId == $scope.subcategoryname[j].id) {
                member.subcategoryname = $scope.subcategoryname[j].name;
                break;
              }
            }
          });
          // $scope.TotalTodos = [];
          // $scope.TotalTodos.push(member);

        });

      });
    }

    $scope.customerId = '';
    $scope.categoryId = '';
    $scope.subcategoryId = '';

    $scope.$watch('customerId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (cate) {
          $scope.categories = cate;
        });

        Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (itemcatg) {
          $scope.itemcategories = itemcatg;

          angular.forEach($scope.itemcategories, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
          });
        });
      }
    });

    $scope.$watch('categoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('subcategories?filter[where][categoryId]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId).getList().then(function (scate) {
          $scope.subcategories = scate;
        });

        Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId).getList().then(function (itemcatg) {
          $scope.itemcategories = itemcatg;

          angular.forEach($scope.itemcategories, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
          });
        });
      }
    });

    $scope.$watch('subCategoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + newValue).getList().then(function (itemcatg) {
          $scope.itemcategories = itemcatg;

          angular.forEach($scope.itemcategories, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            //   $scope.TotalTodos = [];
            //   $scope.TotalTodos.push(member);
          });
        });
      }
    });

    // $scope.testf = function () {
    //     console.log('testf');
    // };

    //------------------------------- Cust-Catg-Subcatg-------------------------

    if ($window.sessionStorage.roleId == 2) {
      Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
        $scope.customers = cust;
      });
    } else {
      Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });
    }

    $scope.$watch('itemcategory.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
      }
    });

    $scope.$watch('itemcategory.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    //------------------------------- end of Cust-Catg-Subcatg-------------------------

    /****************************************** CREATE *********************************/
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.Save = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.itemcategory, "message-282")
      if ($scope.itemcategory.name == '' || $scope.itemcategory.name == null) {
        $scope.itemcategory.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Itemcategory';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.itemcategory.customerId == '' || $scope.itemcategory.customerId == null) {
        $scope.itemcategory.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      } else if ($scope.itemcategory.categoryId == '' || $scope.itemcategory.categoryId == null) {
        $scope.itemcategory.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Category';

      } else if ($scope.itemcategory.subcategoryId == '' || $scope.itemcategory.subcategoryId == null) {
        $scope.itemcategory.subcategoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Itemcategory has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        Restangular.all('itemcategories').post($scope.itemcategory).then(function () {
          window.location = '/itemcategory';
        });
      }
    };

    /***************************** UPDATE *******************************************/

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Update = function () {

      document.getElementById('name').style.border = "";
      //console.log($scope.itemcategory, "message-323")
      if ($scope.itemcategory.name == '' || $scope.itemcategory.name == null) {
        $scope.itemcategory.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Itemcategory';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.itemcategory.customerId == '' || $scope.itemcategory.customerId == null) {
        $scope.itemcategory.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      } else if ($scope.itemcategory.categoryId == '' || $scope.itemcategory.categoryId == null) {
        $scope.itemcategory.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Category';

      } else if ($scope.itemcategory.subcategoryId == '' || $scope.itemcategory.subcategoryId == null) {
        $scope.itemcategory.subcategoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Itemcategory has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;
        Restangular.one('itemcategories/' + $routeParams.id).customPUT($scope.itemcategory).then(function () {
          window.location = '/itemcategory';

        });
      }
    };

    if ($routeParams.id) {
      console.log("$routeParams.id", $routeParams.id)
      Restangular.one('itemcategories', $routeParams.id).get().then(function (itmcatg) {
        $scope.original = itmcatg;
        $scope.itemcategory = Restangular.copy($scope.original);
      });
    }

  });