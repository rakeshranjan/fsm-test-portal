'use strict';

angular.module('secondarySalesApp')
  .controller('ExportDetailsCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

    //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
    //            window.location = "/";
    //        }

    $scope.modalTitle = 'Thank You';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.taskModel = false;

    $scope.addHtml = function (rcdetailId, member) {
      $scope.selectedSubCategoryId = member.subcategoryId;
      $scope.pdfLoad(rcdetailId);
    };
    $scope.showForm = function () {
      var visible = $location.path() === '/export-details/create' || $location.path() === '/export-details/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/export-details/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/export-details/create' || $location.path() === '/export-details/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/export-details/create' || $location.path() === '/export-details/' + $routeParams.id;
      return visible;
    };

    /*********************************** Pagination *******************************************/

    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/organisationlevel" || $window.sessionStorage.prviousLocation != "partials/organisationlevel") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      console.log('newPage', newPage, oldPage);
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /****************************************************************************/
    //---------- New codes -------------------
    // $scope.hourly = {
    //   customerId: $window.sessionStorage.customerId,
    // };

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      // $scope.customers = cust;
      $scope.organizations = cust;
      $scope.customerId = window.sessionStorage.customerId;
    });

    Restangular.one('customers?filter[where][id]=' + $window.sessionStorage.customerId).get().then(function (cus) {
      $scope.customerName = cus[0].name;

      Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (catgresp) {
        $scope.categories = catgresp;
        $scope.categoryId = catgresp[0].id;
        // $scope.categoryId = 23;
      });
    });

    $scope.obj = {
      subcategoryId: '',
      groupId: ''
    };

    $scope.categoryId = '';

    $scope.$watch('categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue + '&filter[where][worktype]=T').getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
        $scope.months = [{ name: 'Jan', value: '01' }, { name: 'Feb', value: '02' }, { name: 'Mar', value: '03' }, { name: 'Apr', value: '04' }, { name: 'May', value: '05' }, { name: 'June', value: '06' }, { name: 'July', value: '07' }, { name: 'Aug', value: '08' }, { name: 'Sep', value: '09' }, { name: 'Oct', value: '10' }, { name: 'Nov', value: '11' }, { name: 'Dec', value: '12' }];
      }
    });

    $scope.$watch('obj.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        $("#table1").hide();
        return;
      } else {
        Restangular.one('subcategories?filter[where][id]=' + newValue).get().then(function (scat) {
          $scope.subCategoryName = scat[0].name;
          $("#yourTextBox").hide();
          $("#yourTextBox1").hide();
          $("#yourTextBox2").hide();
          $("#table2").hide();
          $("#table1").show();
        });

        $scope.taskdate = new Date();

        $scope.organisationlevels = [];

        Restangular.one('customers/findOne?filter[where][id]=' + $scope.customerId).get().then(function (cust) {
          Restangular.one('organisationlocations/findOne?filter[where][id]=' + cust.orgroot).get().then(function (orglevel) {
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + orglevel.level).get().then(function (orglvl) {
              $scope.organisationlevels.push(orglvl);
              Restangular.all('organisationlevels?filter[where][rootid]=' + orglvl.id + '&filter[where][languageparent]=true&filter[where][deleteflag]=false' + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
                angular.forEach(ogrlevls, function (data, index) {
                  $scope.organisationlevels.push(data);
                });

                angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                  Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + organisationlevel.id).getList().then(function (ogrlocs) {
                    organisationlevel.index = index;
                    organisationlevel.organisationlocations = ogrlocs;
                  });
                });
              });
            });
          });
        });
      }
    });

    $scope.orgLevelChange = function (index, level) {
      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
        }
      });
      $scope.selectedData = index;
      $scope.selectedLevel = level;
      $scope.taskdate = new Date();

      //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
      console.log("$scope.selectedData-141", $scope.selectedData)
      console.log("$scope.selectedLevel-142", $scope.selectedLevel)

      if ($scope.selectedData == 1) {
        //var hourly_url = 'hourlyanswers?filter[where][data1]=' + $scope.selectedLevel;
        // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.selectedLevel;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][customerId]=' + window.sessionStorage.customerId;
      } else if ($scope.selectedData == 2) {
        //var hourly_url = 'hourlyanswers?filter[where][data2]=' + $scope.selectedLevel;
        //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[where][categoryId]=' + $scope.selectedLevel;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d2id]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 3) {
        //var hourly_url = 'hourlyanswers?filter[where][data3]=' + $scope.selectedLevel;
        //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d3id]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 4) {
        //var hourly_url = 'hourlyanswers?filter[where][data4]=' + $scope.selectedLevel;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d4id]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 5) {
        //var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d5id]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 5) {
        //var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d5id]=' + $scope.selectedLevel;
      }

      //$scope.formattedDate = $filter('date')(newValue, "MM/dd/yyyy");

      // if (window.sessionStorage.roleId == 2) {
      //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
      //   $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;

      // } else {
      //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false' + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
      //   $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false' + '&filter[order]=id%20ASC';
      // }

      Restangular.all($scope.memberUrl).getList().then(function (member) {
        // $scope.members = member;
        $scope.members = $filter('orderBy')(member, 'id', true);
        console.log($scope.members, "Member-Here at 177")
        angular.forEach($scope.members, function (member, index) {
          member.index = index + 1;
        });
      });
    };

    $scope.$watch('taskdate', function (newValue, oldValue) {
      if (newValue == '' || newValue == null) {
        return;
      } else {


        //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
        console.log("$scope.selectedData-148", $scope.selectedData)
        console.log("$scope.selectedLevel-149", $scope.selectedLevel)

        if ($scope.selectedData == 1) {
          //var hourly_url = 'hourlyanswers?filter[where][data1]=' + $scope.selectedLevel;
          // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][customerId]=' + window.sessionStorage.customerId;
        } else if ($scope.selectedData == 2) {
          //var hourly_url = 'hourlyanswers?filter[where][data2]=' + $scope.selectedLevel;
          //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[where][categoryId]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d2id]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 3) {
          //var hourly_url = 'hourlyanswers?filter[where][data3]=' + $scope.selectedLevel;
          //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d3id]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 4) {
          //var hourly_url = 'hourlyanswers?filter[where][data4]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d4id]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 5) {
          //var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d5id]=' + $scope.selectedLevel;
        }

        Restangular.all($scope.memberUrl).getList().then(function (member) {
          $scope.members = $filter('orderBy')(member, 'id', true);
          console.log($scope.members, "Member-Here at 184")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    $scope.$watch('obj.groupId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null) {
        return;
      } else {
        if ($scope.selectedData == 1) {
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[where][groupserialno]=' + newValue;
        } else if ($scope.selectedData == 2) {
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d2id]=' + $scope.selectedLevel + '&filter[where][groupserialno]=' + newValue;
        } else if ($scope.selectedData == 3) {
          //var hourly_url = 'hourlyanswers?filter[where][data3]=' + $scope.selectedLevel;
          //$scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d3id]=' + $scope.selectedLevel + '&filter[where][groupserialno]=' + newValue;
        } else if ($scope.selectedData == 4) {
          //var hourly_url = 'hourlyanswers?filter[where][data4]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d4id]=' + $scope.selectedLevel + '&filter[where][groupserialno]=' + newValue;
        } else if ($scope.selectedData == 5) {
          //var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
          $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=' + 'T' + '&filter[where][d5id]=' + $scope.selectedLevel + '&filter[where][groupserialno]=' + newValue;
        }

        Restangular.all($scope.memberUrl).getList().then(function (member) {
          $scope.members = $filter('orderBy')(member, 'id', true);
          console.log($scope.members, "Member-Here at 184")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    //----------- end of new codes -----------

    $scope.todo = {
      member: '',
      followUp: '',
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId
    };
    Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
      $scope.roles = role;
    });

    Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (language) {
      $scope.languages = language;
    });

    if (window.sessionStorage.roleId == 2) {
      Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + window.sessionStorage.customerId).getList().then(function (customer) {
        //Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + 15).getList().then(function (customer) {
        $scope.customers = customer;
        $scope.customerId = window.sessionStorage.customerId;
        $scope.orgroot = customer[0].orgroot;
        console.log("$scope.orgroot-85", $scope.orgroot)
        //$scope.customerId = 15;
        // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parentlevel]=' + window.sessionStorage.orgLevel).getList().then(function (zoneResp) {
        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + $scope.orgroot).getList().then(function (zoneResp) {
          //console.log("zoneResp-88", zoneResp)
          $scope.zones = zoneResp;
        });
      });
    } else {
      Restangular.all('customers?filter[where][deleteFlag]=false').getList().then(function (customer) {
        $scope.customers = customer;
        if ($scope.customers.length > 0) {
          $scope.customerId = $scope.customers[$scope.customers.length - 1].id;
        }
      });
    }

    // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (customer) {
    //   $scope.customers = customer;
    // });

    /************************************* INDEX ***************************************************/

    $scope.getQuestion = function (questionId) {
      return Restangular.one('surveyquestions', questionId).get().$object;
    };

    $scope.getStatus = function (questionId) {
      return Restangular.one('todostatuses', questionId).get().$object;
    };

    $scope.getUser = function (createdby) {
      return Restangular.one('users', createdby).get().$object;
    };

    $scope.getCustomer = function (createdby) {
      return Restangular.one('customers', createdby).get().$object;
    };

    $scope.getUpdateType = function (updatetpname) {
      return Restangular.one('updatetypes', updatetpname).get().$object;
    };

    $scope.getServiceEvent = function (serviceevnt) {
      return Restangular.one('serviceevents', serviceevnt).get().$object;
    };

    $scope.getRollbackRole = function (rollbackRoleby) {
      return Restangular.one('roles', rollbackRoleby).get().$object;
    };

    $scope.getRollbackUser = function (rollbackUser) {
      return Restangular.one('users', rollbackUser).get().$object;
    };

    $scope.getRollbackStatus = function (rollbackStatus) {
      return Restangular.one('rollbackstatuses', rollbackStatus).get().$object;
    };

    Restangular.all('exportheaders?filter[where][userId]=' + $window.sessionStorage.userId).getList().then(function (tdo) {
      $scope.organisationlevels = tdo;
      angular.forEach($scope.organisationlevels, function (member, index) {
        member.index = index + 1;

        $scope.TotalTodos = [];
        $scope.TotalTodos.push(member);
      });
    });

    $scope.hideDropDown = true;

    if ($routeParams.id) {
      $scope.hideDropDown = false;
      $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
        // $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + $routeParams.id).getList().then(function (HRes) {
        // $scope.HealthAnswers = HRes;
        $scope.HealthAnswers = $filter('orderBy')(HRes, 'questionid')
        console.log($scope.HealthAnswers, "ANS-35");
        angular.forEach($scope.HealthAnswers, function (member, index) {
          member.index = index + 1;
          // if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
          if (member.answer == null || member.answer == undefined || member.answer == '') {
            member.uploadedfilesList = [];
          } else {
            // member.uploadedfilesList = member.uploadedfiles.split(',');
            member.uploadedfilesList = member.answer.split(',');
            // console.log(member.uploadedfilesList, "member.uploadedfilesList-133")
          }
        });
      });

      Restangular.one('rcdetails', $routeParams.id).get().then(function (rcDetail) {
        $scope.rcDetail = rcDetail;
        $scope.CustomerName = rcDetail.customername;
        $scope.currDate = rcDetail.endtime;
        $scope.dispatchNumber = rcDetail.dispatchno;

        $scope.info1Header = rcDetail.info1.split(':')[0];
        $scope.info1Trailer = rcDetail.info1.split(':')[1];

        $scope.info2Header = rcDetail.info2.split(':')[0];
        $scope.info2Trailer = rcDetail.info2.split(':')[1];

        $scope.info3Header = rcDetail.info3.split(':')[0];
        $scope.info3Trailer = rcDetail.info3.split(':')[1];

        $scope.info4Header = rcDetail.info4.split(':')[0];
        $scope.info4Trailer = rcDetail.info4.split(':')[1];

        $scope.info5Header = rcDetail.info5.split(':')[0];
        $scope.info5Trailer = rcDetail.info5.split(':')[1];

        $scope.info6Header = rcDetail.info6.split(':')[0];
        $scope.info6Trailer = rcDetail.info6.split(':')[1];

        $scope.info7Header = rcDetail.info7.split(':')[0];
        $scope.info7Trailer = rcDetail.info7.split(':')[1];

        $scope.info8Header = rcDetail.info8.split(':')[0];
        $scope.info8Trailer = rcDetail.info8.split(':')[1];

        $scope.info9Header = rcDetail.info9.split(':')[0];
        $scope.info9Trailer = rcDetail.info9.split(':')[1];

        $scope.info10Header = rcDetail.info10.split(':')[0];
        $scope.info10Trailer = rcDetail.info10.split(':')[1];

        Restangular.one('customers', rcDetail.customerId).get().then(function (cust) {
          $scope.CompanyName = cust.name;
        });

        Restangular.one('groupinfo?filter[where][customerId]=' + rcDetail.customerId + '&filter[where][categoryId]=' + rcDetail.categoryId + '&filter[where][subcategoryId]=' + rcDetail.subcategoryId).get().then(function (group) {
          $scope.groupList = group;
        });
      });
    }

    // $scope.imageSource = '//idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/Jawaharlal_Nehru_Crrr1223454sPage.jpg';

    $scope.validatestring = "";
    $scope.langModel = false;

    $scope.LangClick = function () {

      angular.forEach($scope.languages, function (data) {
        if (!data.location) {
          data.location = $scope.todo.name;
        }
      });

      $scope.langModel = true;
    };

    $scope.SaveLang = function () {
      $scope.langModel = false;
      $scope.submitDisable = false;
    };

    $scope.Save = function (clicked) {

      if ($scope.todo.name == '' || $scope.todo.name == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Name';

      } else if ($scope.todo.slno == '' || $scope.todo.slno == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Order';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {

        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        $scope.todo.language = 1;
        $scope.todo.languageparent = true;
        Restangular.all('organisationlevels').post($scope.todo).then(function (resp) {
          $scope.LangLocations = [];
          for (var i = 0; i < $scope.languages.length; i++) {
            $scope.langObject = {
              deleteflag: false,
              language: $scope.languages[i].id,
              languageparent: false,
              languageparentid: resp.id,
              name: $scope.languages[i].location,
              parent: resp.parent,
              slno: resp.slno
            };
            $scope.LangLocations.push($scope.langObject);
          }
          $scope.SaveLangLocations();
        });
      }
    };
    $scope.LangCount = 0;
    $scope.SaveLangLocations = function () {
      Restangular.all('organisationlevels').post($scope.LangLocations[$scope.LangCount]).then(function (resp) {
        $scope.LangCount++;
        if ($scope.LangLocations.length > $scope.LangCount) {
          $scope.SaveLangLocations();
        } else {
          window.location = '/organisationlevel';
        }
      });
    }

    $scope.Update = function (clicked) {

      if ($scope.todo.name == '' || $scope.todo.name == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Name';

      } else if ($scope.todo.slno == '' || $scope.todo.slno == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Order';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {

        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;

        Restangular.one('organisationlevels', $routeParams.id).customPUT($scope.todo).then(function (resp) {
          $scope.UpLangLocations = [];
          if ($scope.langorganisationlevels.length > 0) {
            angular.forEach($scope.languages, function (lang) {
              angular.forEach($scope.langorganisationlevels, function (data) {
                if (lang.languageid === data.language) {
                  data.name = lang.location;
                  data.parent = resp.parent;
                  $scope.UpLangLocations.push(data);
                }
              });
            });

            $scope.UpdateLangLocations();
          } else {
            for (var i = 0; i < $scope.languages.length; i++) {
              $scope.langObject = {
                deleteflag: false,
                language: $scope.languages[i].id,
                languageparent: false,
                languageparentid: resp.id,
                name: $scope.languages[i].location,
                parent: resp.parent,
                slno: resp.slno
              };
              $scope.UpLangLocations.push($scope.langObject);
            }
            $scope.UpdateSaveLangLocations();
          }
        });
      }
    };

    $scope.UpdateLangCount = 0;
    $scope.UpdateLangLocations = function () {
      Restangular.one('organisationlevels').customPUT($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
        $scope.UpdateLangCount++;
        if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
          $scope.UpdateLangLocations();
        } else {
          window.location = '/organisationlevel';
        }
      });
    }
    $scope.UpdateSaveLangLocations = function () {
      Restangular.all('organisationlevels').post($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
        $scope.UpdateLangCount++;
        if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
          $scope.UpdateSaveLangLocations();
        } else {
          window.location = '/organisationlevel';
        }
      });
    }


    if ($routeParams.id) {

      $scope.message = 'Level has been updated!';

      Restangular.one('organisationlevels', $routeParams.id).get().then(function (td) {
        $scope.original = td;
        $scope.todo = Restangular.copy($scope.original);


        Restangular.all('organisationlevels?filter[where][languageparentid]=' + $routeParams.id).getList().then(function (resp) {
          $scope.langorganisationlevels = resp;
          angular.forEach(resp, function (data) {
            angular.forEach($scope.languages, function (lang) {
              if (lang.languageid === data.language) {
                lang.location = data.name;
              }
            });
          });
        });




      });
    } else {
      $scope.message = 'Level has been created!';
    }


    /************************************* Delete ***************************************************/

    $scope.Delete = function (id) {
      $scope.item = [{
        deleteflag: true
      }]
      Restangular.one('organisationlevels/' + id).customPUT($scope.item[0]).then(function () {
        $route.reload();
      });
    };

    $scope.getUser = function (id) {
      return Restangular.one('users', id).get().$object;
    };

    $scope.sort = {
      active: '',
      descending: undefined
    }

    $scope.changeSorting = function (column) {

      var sort = $scope.sort;

      if (sort.active == column) {
        sort.descending = !sort.descending;

      } else {
        sort.active = column;
        sort.descending = false;
      }
    };

    $scope.getIcon = function (column) {

      var sort = $scope.sort;

      if (sort.active == column) {
        return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
      }
    }

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    /** users call ***/

    // Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (zoneResp) {
    //   $scope.zones = zoneResp;
    // });

    if (window.sessionStorage.roleId == 2) {
      // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parentlevel]=' + window.sessionStorage.orgLevel).getList().then(function (zoneResp) {
      //   $scope.zones = zoneResp;
      // });
    } else {
      Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (zoneResp) {
        $scope.zones = zoneResp;
      });
    }

    // $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false';
    $scope.filteredStartDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
    $scope.filteredEndDate = new Date($scope.endDate);
    $scope.filteredEndDate.setDate($scope.filteredEndDate.getDate() + 1);
    $scope.filteredEndDate = $filter('date')($scope.filteredEndDate, 'yyyy-MM-dd');
    // if (window.sessionStorage.roleId == 2) {
    //   $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
    // } else {
    //   $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false' + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
    // }
    if (window.sessionStorage.roleId == 2) {
      // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
      // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
      $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][customerId]=' + window.sessionStorage.customerId;

    } else {
      // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false' + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
      // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false' + '&filter[order]=id%20ASC';
      $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T' + '&filter[order]=id%20ASC';
    }

    Restangular.all($scope.memberUrl).getList().then(function (member) {
      // $scope.members = member;
      $scope.members = $filter('orderBy')(member, 'id', true);
      console.log($scope.members, "Member-Here at 625")
      angular.forEach($scope.members, function (member, index) {
        member.index = index + 1;
      });
    });

    $scope.$watch('customerId', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        // $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
        // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
        // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][customerId]=' + newValue;

        Restangular.all($scope.memberUrl).getList().then(function (member) {
          // $scope.members = member;
          $scope.members = $filter('orderBy')(member, 'id', true);
          console.log($scope.members, "Member-Here at 644")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
          });
        });

        //----------------------------
        // if ($scope.zoneId.length > 0) {
        //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
        //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
        //   $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
        // } else {
        //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId;
        //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId;
        //   $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][customerId]=' + $scope.customerId;
        // }
        // Restangular.all($scope.memberUrl).getList().then(function (member) {
        //   // $scope.members = member;
        //   $scope.members = $filter('orderBy')(member, 'id', true);
        //   console.log($scope.members, "Member-Here at 663")
        //   angular.forEach($scope.members, function (member, index) {
        //     member.index = index + 1;
        //   });
        // });

        //----------------------------

      }
    });

    //----------------------------
    // if ($scope.zoneId.length > 0) {
    //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
    //   $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
    // } else {
    //   // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId;
    //   $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId;
    // }
    // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + window.sessionStorage.customerId;
    $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][customerId]=' + window.sessionStorage.customerId;
    Restangular.all($scope.memberUrl).getList().then(function (member) {
      //$scope.members = member;
      //$scope.ordersummaries = $filter('orderBy')(msgmstrs, 'id', true);
      $scope.members = $filter('orderBy')(member, 'id', true);
      console.log($scope.members, "Member-Here at 688")
      angular.forEach($scope.members, function (member, index) {
        member.index = index + 1;
      });
    });

    //----------------------------

    $scope.$watch('zoneId', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        // $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][stateId]=' + newValue + '&filter[where][customerId]=' + $scope.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
        // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][stateId]=' + newValue + '&filter[where][customerId]=' + $scope.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
        // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][stateId]=' + newValue + '&filter[where][customerId]=' + $scope.customerId;
        $scope.memberUrl = 'rcdtlviews?filter[where][worktype]=T&filter[where][stateId]=' + newValue + '&filter[where][customerId]=' + $scope.customerId;
        Restangular.all($scope.memberUrl).getList().then(function (member) {
          //$scope.members = member;
          $scope.members = $filter('orderBy')(member, 'id', true);
          console.log($scope.members, "Member-Here at 708")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    $scope.$watch('endDate', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        $scope.filteredStartDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
        $scope.filteredEndDate = new Date($scope.endDate);
        $scope.filteredEndDate.setDate($scope.filteredEndDate.getDate() + 1);
        $scope.filteredEndDate = $filter('date')($scope.filteredEndDate, 'yyyy-MM-dd');
        $scope.memberUrl = "";
        // if ($scope.zoneId.length > 0) {
        //   $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
        // } else {
        //   $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId;
        // }
        if ($scope.zoneId.length > 0) {
          $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][stateId]=' + $scope.zoneId;
        } else {
          // $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][customerId]=' + $scope.customerId;
          $scope.memberUrl = 'ticketdetailsviews?filter[where][deleteflag]=false&filter[where][customerId]=' + $scope.customerId;
        }
        Restangular.all($scope.memberUrl).getList().then(function (member) {
          $scope.members = member;
          console.log($scope.members, "Member-Here at 305")
          angular.forEach($scope.members, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
      $scope.organisationlocations = organisationlocations;
      console.log($scope.organisationlocations, "$scope.organisationlocations-member-37");
      var userUrl = '';
      if ($window.sessionStorage.isPartner === 'true') {
        userUrl = 'users?filter[where][managerId]=' + $window.sessionStorage.userId;
      } else {
        userUrl = 'users?filter[where][managerId]=0'
      }
      $scope.led = Restangular.all(userUrl).getList().then(function (response) {

        //$scope.tds = Restangular.all('users?filter={"where":{"and":[{"roleId":{"inq":[16]}},{"id":{"inq":[1249]}}]}}').getList().then(function (response) {

        console.log('response', response);
        $scope.users = response;
        angular.forEach($scope.users, function (member, index) {
          member.index = index + 1;
          if (member.deleteflag === 'true') {
            member.backgroundColor = "#ff0000"
          } else {
            member.backgroundColor = "#000000"
          }

          if (member.orgStructure != null) {
            var array = member.orgStructure.split(';');
            console.log(array, array.length, "MSG-75")

            member.pinList = [];
            member.cityList = [];
            member.stateList = [];
            member.zoneList = [];

            if (array.length == 5) {
              var pinarray = array[array.length - 1].split('-')[1].split(',');
              var cityarray = array[array.length - 2].split('-')[1].split(',');
              var statearray = array[array.length - 3].split('-')[1].split(',');
              var zonearray = array[array.length - 4].split('-')[1].split(',');

              //member.pinList = pinarray.split(',');
              if (pinarray.length > 1) {
                //member.pinList = pinarray.split(',');
                for (var p = 0; p <= pinarray.length - 1; p++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + pinarray[p] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.pinnumber = orglocResponse[0].name;
                    member.pinList.push(member.pinnumber);
                  });
                  //member.pinList.push(pinarray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + pinarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.pinnumber = orglocResponse[0].name;
                  member.pinList.push(member.pinnumber);
                });
                //member.pinList = pinarray[0];
              }
              //member.cityList = cityarray.split(',');
              if (cityarray.length > 1) {
                //member.cityList = cityarray.split(',');
                for (var c = 0; c <= cityarray.length - 1; c++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.cityname = orglocResponse[0].name;
                    member.cityList.push(member.cityname);
                  });
                  //member.cityList.push(cityarray[c]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.cityname = orglocResponse[0].name;
                  member.cityList.push(member.cityname);
                });
                //member.cityList = cityarray[0];
              }
              //member.stateList = statearray.split(',');
              if (statearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= statearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.statename = orglocResponse[0].name;
                  member.stateList.push(member.statename);
                });
                //member.stateList = statearray[0];
              }

              if (zonearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= zonearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.zonename = orglocResponse[0].name;
                  member.zoneList.push(member.zonename);
                });
                //member.stateList = statearray[0];
              }
              console.log(member.pinList, "member.pinList-82")
              console.log(member.cityList, "member.cityList-83")
              console.log(member.stateList, "member.stateList-84")
              console.log(member.zoneList, "member.zoneList-84")
            }

            else if (array.length == 4) {
              // var pinarray = array[array.length - 1].split('-')[1].split(',');
              // var cityarray = array[array.length - 2].split('-')[1].split(',');
              // var statearray = array[array.length - 3].split('-')[1].split(',');

              var cityarray = array[array.length - 1].split('-')[1].split(',');
              var statearray = array[array.length - 2].split('-')[1].split(',');
              var zonearray = array[array.length - 3].split('-')[1].split(',');

              //member.pinList = pinarray.split(',');
              // if (pinarray.length > 1) {
              //   //member.pinList = pinarray.split(',');
              //   for (var p = 0; p <= pinarray.length - 1; p++) {
              //     Restangular.one('organisationlocations?filter[where][id]=' + pinarray[p] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //       //console.log(orglocResponse, "orglocResponse-124")
              //       member.pinnumber = orglocResponse[0].name;
              //       member.pinList.push(member.pinnumber);
              //     });
              //     //member.pinList.push(pinarray[k]);
              //   }
              // } else {
              //   Restangular.one('organisationlocations?filter[where][id]=' + pinarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //     //console.log(orglocResponse, "orglocResponse-124")
              //     member.pinnumber = orglocResponse[0].name;
              //     member.pinList.push(member.pinnumber);
              //   });
              //   //member.pinList = pinarray[0];
              // }
              //member.cityList = cityarray.split(',');
              if (cityarray.length > 1) {
                //member.cityList = cityarray.split(',');
                for (var c = 0; c <= cityarray.length - 1; c++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.cityname = orglocResponse[0].name;
                    member.cityList.push(member.cityname);
                  });
                  //member.cityList.push(cityarray[c]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.cityname = orglocResponse[0].name;
                  member.cityList.push(member.cityname);
                });
                //member.cityList = cityarray[0];
              }
              //member.stateList = statearray.split(',');
              if (statearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= statearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.statename = orglocResponse[0].name;
                  member.stateList.push(member.statename);
                });
                //member.stateList = statearray[0];
              }

              if (zonearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= zonearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.zonename = orglocResponse[0].name;
                  member.zoneList.push(member.zonename);
                });
                //member.stateList = statearray[0];
              }
              // console.log(member.pinList, "member.pinList-82")
              console.log(member.cityList, "member.cityList-83")
              console.log(member.stateList, "member.stateList-84")
              console.log(member.zoneList, "member.zoneList-84")

            } else if (array.length == 3) {
              // var cityarray = array[array.length - 1].split('-')[1].split(',');
              // var statearray = array[array.length - 2].split('-')[1].split(',');

              var statearray = array[array.length - 1].split('-')[1].split(',');
              var zonearray = array[array.length - 2].split('-')[1].split(',');
              //member.cityList = cityarray.split(',');
              // if (cityarray.length > 1) {
              //   // member.cityList = cityarray.split(',');
              //   for (var c = 0; c <= cityarray.length - 1; c++) {
              //     Restangular.one('organisationlocations?filter[where][id]=' + cityarray[c] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //       //console.log(orglocResponse, "orglocResponse-124")
              //       member.cityname = orglocResponse[0].name;
              //       member.cityList.push(member.cityname);
              //     });
              //     //member.cityList.push(cityarray[c]);
              //   }
              // } else {
              //   Restangular.one('organisationlocations?filter[where][id]=' + cityarray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //     //console.log(orglocResponse, "orglocResponse-124")
              //     member.cityname = orglocResponse[0].name;
              //     member.cityList.push(member.cityname);
              //   });
              //   //member.cityList = cityarray[0];
              // }
              //member.stateList = statearray.split(',');
              if (statearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= statearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.statename = orglocResponse[0].name;
                    member.stateList.push(member.statename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.statename = orglocResponse[0].name;
                  member.stateList.push(member.statename);
                });
                //member.stateList = statearray[0];
              }

              if (zonearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= zonearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  //member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.zonename = orglocResponse[0].name;
                  member.zoneList.push(member.zonename);
                });
                //member.stateList = statearray[0];
              }
              console.log(member.cityList, "member.cityList-100")
              console.log(member.stateList, "member.stateList-101")
            } else if (array.length == 2) {
              // var statearray = array[array.length - 1].split('-')[1].split(',');
              var zonearray = array[array.length - 1].split('-')[1].split(',');
              console.log(zonearray, "statearray-104")
              // if (statearray.length > 1) {
              //   //member.stateList = statearray.split(',');
              //   for (var k = 0; k <= statearray.length - 1; k++) {
              //     Restangular.one('organisationlocations?filter[where][id]=' + statearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //       //console.log(orglocResponse, "orglocResponse-124")
              //       member.statename = orglocResponse[0].name;
              //       member.stateList.push(member.statename);
              //     });
              //     // member.stateList.push(statearray[k]);
              //   }
              // } else {
              //   Restangular.one('organisationlocations?filter[where][id]=' + statearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
              //     //console.log(orglocResponse, "orglocResponse-124")
              //     member.statename = orglocResponse[0].name;
              //     member.stateList.push(member.statename);
              //   });
              //   //member.stateList = statearray[0];
              // }
              if (zonearray.length > 1) {
                //member.stateList = statearray.split(',');
                for (var k = 0; k <= zonearray.length - 1; k++) {
                  Restangular.one('organisationlocations?filter[where][id]=' + zonearray[k] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                    //console.log(orglocResponse, "orglocResponse-124")
                    member.zonename = orglocResponse[0].name;
                    member.zoneList.push(member.zonename);
                  });
                  // member.stateList.push(statearray[k]);
                }
              } else {
                Restangular.one('organisationlocations?filter[where][id]=' + zonearray[0] + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
                  //console.log(orglocResponse, "orglocResponse-124")
                  member.zonename = orglocResponse[0].name;
                  member.zoneList.push(member.zonename);
                });
                //member.stateList = statearray[0];
              }
              console.log(member.zoneList, "member.zoneList-110")
            }
            // var subarray = array[array.length - 1].split('-')[1].split(',');
            // console.log(subarray, "SUB-ARRAY")

            // if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
            //   member.uploadedfilesList = [];
            // } else 
            //{
            // member.uploadedfilesList = member.uploadedfiles.split(',');
            // member.stateList = member.statearray.split(',');
            //}

          }

          //---- Initially working code -----------------------------------------
          // if (member.orgStructure != null) {
          //   member.locationname = "";
          //   var array = member.orgStructure.split(';');
          //   var subarray = array[array.length - 1].split('-')[1].split(',');
          //   for (var i = 0; i < $scope.organisationlocations.length; i++) {
          //     for (var j = 0; j < subarray.length; j++) {
          //       if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
          //         if (member.locationname === "") {
          //           member.locationname = $scope.organisationlocations[i].name;
          //         } else {
          //           member.locationname = member.locationname + "," + $scope.organisationlocations[i].name;
          //         }
          //       }
          //     }
          //   }
          //   member.location = array[array.length - 1].split('-')[1];
          // }
          //---- End of Initially working code -----------------------------------------

          // //---- Later Modified by me saty for multiple pincode display ----------------------
          // if (member.orgStructure != null) {
          //   //member.locationnamestate = "";
          //   member.locationname = "";
          //   member.locationnamepincode = "";
          //   console.log(member.orgStructure, "orgStructure-72")
          //   var array = member.orgStructure.split(';');
          //   console.log(array, array.length, "MSG-75")
          //   var subarray = array[array.length - 1].split('-')[1].split(',');
          //   console.log(subarray, "SUB-ARRAY")
          //   for (var i = 0; i < $scope.organisationlocations.length; i++) {
          //     for (var j = 0; j < subarray.length; j++) {
          //       if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
          //         if ($scope.organisationlocations[i].parent == null) {
          //           if (member.locationname === "") {
          //             member.locationname = $scope.organisationlocations[i].name;
          //           } else {
          //             member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
          //           }
          //         } else if ($scope.organisationlocations[i].parent != null) {
          //           Restangular.one('organisationlocations?filter[where][id]=' + $scope.organisationlocations[i].parent + '&filter[where][deleteFlag]=false').get().then(function (orglocResponse) {
          //             //console.log(orglocResponse, "orglocResponse-89")
          //             member.locationname = orglocResponse[0].name;
          //           });
          //           if (member.locationnamepincode === "") {
          //             member.locationnamepincode = $scope.organisationlocations[i].name;
          //           } else {
          //             member.locationnamepincode = member.locationnamepincode + ", " + $scope.organisationlocations[i].name;
          //           }
          //         }
          //       }
          //     }
          //   }
          //   member.location = array[array.length - 1].split('-')[1];
          // }
          // //---- End of Later Modified by me saty for multiple pincode display ----------------------




        });
      });

    });

    $scope.exportData = function () {
      $scope.filteredStartDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
      $scope.filteredEndDate = new Date($scope.endDate);
      $scope.filteredEndDate.setDate($scope.filteredEndDate.getDate() + 1);
      $scope.filteredEndDate = $filter('date')($scope.filteredEndDate, 'yyyy-MM-dd');
      $scope.exportData = [];
      if ($scope.zoneId.length > 0) {
        $scope.memberUrl = 'rcdetails?filter[where][customerid]=' + $scope.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate + '&filter[where][stateId]=' + $scope.zoneId;
      } else {
        $scope.memberUrl = 'rcdetails?filter[where][customerid]=' + $scope.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate;
      }
      Restangular.one($scope.memberUrl).getList().then(function (rcdetails) {
        Restangular.one('surveyquestions?filter[where][customerId]=' + $scope.customerId + '&filter[order]=id%20ASC').getList().then(function (surveyquestions) {
          Restangular.one('rcdetailviews?filter[where][customerid]=' + $scope.customerId + '&filter[order]=id%20ASC&filter[where][endtime][between][0]=' + $scope.filteredStartDate + '&filter[where][endtime][between][1]=' + $scope.filteredEndDate).getList().then(function (rcdetailviews) {
            for (var i = 0; i < rcdetails.length; i++) {
              $scope.tempObject = {};
              $scope.tempObject['Client Name'] = rcdetails[i].customername;

              $scope.filteredEndTime = new Date(rcdetails[i].endtime);
              $scope.filteredEndTime = $filter('date')($scope.filteredEndTime, 'yyyy-MM-dd');

              $scope.tempObject.Date = $scope.filteredEndTime;
              $scope.tempObject['Work Order Number'] = rcdetails[i].dispatchno;
              $scope.dataExist = false;
              for (var j = 0; j < rcdetailviews.length; j++) {
                if (rcdetails[i].id === rcdetailviews[j].id) {
                  $scope.tempObject['State'] = rcdetailviews[j].stateName;
                  $scope.tempObject['Login Id'] = rcdetailviews[j].installerName;
                }
                for (var k = 0; k < surveyquestions.length; k++) {
                  if (!$scope.tempObject[surveyquestions[k].groupserialno + '.' + surveyquestions[k].serialno + ' ' + surveyquestions[k].question]) {
                    $scope.tempObject[surveyquestions[k].groupserialno + '.' + surveyquestions[k].serialno + ' ' + surveyquestions[k].question] = "";
                  }

                  if (rcdetails[i].id === rcdetailviews[j].id && rcdetailviews[j].questionid === surveyquestions[k].id) {
                    $scope.tempObject[surveyquestions[k].groupserialno + '.' + surveyquestions[k].serialno + ' ' + surveyquestions[k].question] = rcdetailviews[j].answer;
                    $scope.dataExist = true;
                  }
                }
              }
              if ($scope.dataExist) {
                $scope.exportData.push($scope.tempObject);
              }
            }
            console.log("$scope.exportData", $scope.exportData);
            if ($scope.exportData.length > 0) {
              alasql('SELECT * INTO XLSX("WorkOrderExport.xlsx",{headers:true}) FROM ?', [$scope.exportData]);
            }
          });
        });
      });
    }

    /*** end ***/

    //Datepicker settings start

    $scope.today = function () {
      $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
    };
    $scope.today();
    $scope.presenttoday = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
      $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
      $scope.dt = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
      $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.mod = {};
    $scope.start = {};
    $scope.incident = {};
    $scope.hlth = {};
    $scope.datestartedart = {};
    $scope.lasttest = {};

    $scope.open = function ($event, item, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepicker' + index).focus();
      });
      item.opened = true;
    };
    $scope.open1 = function ($event, item, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepicker1' + index).focus();
      });
      item.opened = true;
    };

    $scope.openstart = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepickerstart' + index).focus();
      });
      $scope.picker.startopened = true;
    };

    $scope.openend = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepickerend' + index).focus();
      });
      $scope.picker.endopened = true;
    };

    $scope.showDocImage = false;

    $scope.exportHTML = function () {
      // $scope.showDocImage = true;
      // var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
      //   "xmlns:w='urn:schemas-microsoft-com:office:word' " +
      //   "xmlns='http://www.w3.org/TR/REC-html40'>" +
      //   "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
      // var footer = "</body></html>";
      // var sourceHTML = header + document.getElementById("source-html").innerHTML + footer;

      // var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
      // var fileDownload = document.createElement("a");
      // document.body.appendChild(fileDownload);
      // fileDownload.href = source;
      // fileDownload.download = 'document.doc';
      // fileDownload.click();
      // document.body.removeChild(fileDownload);
      // $scope.showDocImage = false;

      if (!window.Blob) {
        alert('Your legacy browser does not support this action.');
        return;
      }

      var html, link, blob, url, css;

      // EU A4 use: size: 841.95pt 595.35pt;
      // US Letter use: size:11.0in 8.5in;

      // css = (
      //   '<style>' +
      //   '@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
      //   'div.WordSection1 {page: WordSection1;}' +
      //   'table{border-collapse:collapse;border: 2px solid rgb(200, 200, 200);letter-spacing: 1px;font-family: sans-serif;font-size: .8rem;}th{border:1px gray solid;width:5em;padding:2px;text-align: center;}td{border:1px gray solid;width:5em;padding:2px;text-align: center;}' +
      //   '</style>'
      // );

      css = (
        '<style>' +
        '@page WordSection1{size: 941.95pt 695.35pt;mso-page-orientation: landscape;}' +
        'div.WordSection1 {page: WordSection1;}' +
        'table{border-spacing: 10px;border-collapse: separate;}th{text-align: center;}td{text-align: center;}td:first-child{width:350px;padding-left:1.2em;}' +
        '</style>'
      );

      html = window.docx.innerHTML;
      blob = new Blob(['\ufeff', css + html], {
        type: 'application/msword'
      });
      url = URL.createObjectURL(blob);
      link = document.createElement('A');
      link.href = url;
      // Set default file name. 
      // Word will append file extension - do not add an extension here.
      link.download = $scope.rcDetail.dispatchno;
      document.body.appendChild(link);
      if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, $scope.rcDetail.dispatchno + '.doc'); // IE10-11
      else link.click();  // other browsers
      document.body.removeChild(link);
    };

    $scope.ansEditModel = false;

    $scope.isEditDispatchNumber = false;

    $scope.openEditPanel = function (object) {
      if (object == true) {
        $scope.isEditDispatchNumber = true;
        $scope.ansEditModel = true;
        Restangular.one('rcdetails', $routeParams.id).get().then(function (rcDetail) {
          $scope.editedDispatchNumber = rcDetail.dispatchno;
        });
      } else {
        $scope.editObject = object;
        $scope.isEditDispatchNumber = false;
        $scope.splittedArray = object.answer.split(',');
        Restangular.one('surveyquestions?filter[where][id]=' + object.questionid).get().then(function (ticketquestotal) {
          if (ticketquestotal.length > 0) {
            if (ticketquestotal[0].questiontype == 9) {
              $scope.ansEditModel = false;
            } else if (ticketquestotal[0].questiontype == 13) {
              $scope.ansEditModel = false;
            } else {
              $scope.ansEditModel = true;
            }

            $scope.surveyquestions = ticketquestotal;
            for (var s = 0; s < $scope.surveyquestions.length; s++) {
              if (!!$scope.surveyquestions[s].answeroptions) {
                $scope.surveyquestions[s].answeroptionsarray = $scope.surveyquestions[s].answeroptions.split(',');
              } else {
                $scope.surveyquestions[s].answeroptionsarray = [];
              }
              $scope.surveyquestions[s].myOptionsArray = [];
              angular.forEach($scope.surveyquestions[s].answeroptionsarray, function (data, index) {
                if (data != '' || data != undefined) {
                  data = data.replace(/\s/g, "");
                  var answerObj = $scope.splittedArray.indexOf(data) !== -1;
                  $scope.surveyquestions[s].myOptionsArray.push({
                    value: data,
                    visible: true,
                    answer: answerObj
                  });
                }
              });
              $scope.surveyquestions[s].answer = object.answer;
            }
          }
        });
      }
    };

    $scope.UpdateAnswer = function () {
      var r = confirm("Are you sure you want to update?");
      if (r == true) {
        if ($scope.isEditDispatchNumber) {
          $scope.SubmitNew();
        } else {
          $scope.Submit();
        }
      } else {
        return;
      }
    };

    $scope.changeDispatch = function (dispatchno) {
      $scope.updatedDispatchNumber = dispatchno;
    };

    $scope.updateObj = {};

    $scope.SubmitNew = function () {
      $scope.updateObj.dispatchno = $scope.updatedDispatchNumber;
      Restangular.one('rcdetails', $routeParams.id).customPUT($scope.updateObj).then(function (resp) {
        $scope.ansEditModel = false;
        setTimeout(function () { $route.reload(); }, 1500);
      });
    };

    $scope.Submit = function () {

      for (var sq = 0; sq < $scope.surveyquestions.length; sq++) {
        $scope.surveyquestions[sq].questionid = $scope.surveyquestions[sq].id;
        $scope.surveyquestions[sq].deleteflag = false;
        $scope.surveyquestions[sq].memberid = $routeParams.id;
        // $scope.surveyquestions[sq].answer = $scope.surveyquestions[sq].answer;
        // $scope.surveyquestions[sq].uploadedfiles = $scope.surveyquestions[sq].uploadedfiles;
        $scope.answer = "";
        if ($scope.surveyquestions[sq].questiontype == 2) {
          angular.forEach($scope.surveyquestions[sq].myOptionsArray, function (data) {
            if (data.answer == true) {
              if ($scope.answer == "") {
                $scope.answer = data.value;
              } else {
                $scope.answer = $scope.answer + "," + data.value;
              }
            }
          });
          $scope.surveyquestions[sq].answer = $scope.answer;
        }

        if ($scope.surveyquestions[sq].questiontype == 9) {
          if ($scope.surveyquestions[sq].answer == null) {
            $scope.answer = $scope.surveyquestions[sq].uploadedfiles;
          } else {
            $scope.answer = $scope.surveyquestions[sq].uploadedfiles;
          }
          $scope.surveyquestions[sq].answer = $scope.answer;
        }
        delete $scope.surveyquestions[sq].id;
        if ($scope.surveyquestions[sq].answer != null) {

          Restangular.one('surveyanswers', $scope.editObject.id).customPUT(JSON.parse(JSON.stringify($scope.surveyquestions[sq]))).then(function (resp) {
            $scope.ansEditModel = false;
            setTimeout(function () { $route.reload(); }, 1500);
          });
        }
      }
    };


    //Datepicker settings start

    $scope.today = function () {
      $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
    };
    $scope.today();
    $scope.presenttoday = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
      $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
      $scope.dt = null;
    };
    $scope.clear = function () {
      $scope.mytime = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
      $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.mod = {};
    $scope.start = {};
    $scope.incident = {};
    $scope.hlth = {};
    $scope.datestartedart = {};
    $scope.lasttest = {};

    $scope.open = function ($event, item, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepicker' + index).focus();
      });
      item.opened = true;
    };
    $scope.open1 = function ($event, item, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepicker1' + index).focus();
      });
      $scope.picker.opened = true;
    };

    $scope.opendob = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepickerdob' + index).focus();
      });
      $scope.picker.dobopened = true;
    };

    $scope.openstartdate = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();
      $timeout(function () {
        $('#datepickerstartdate' + index).focus();
      });
      $scope.picker.startdateopened = true;
    };
    $scope.open = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();

      $timeout(function () {
        $('#datepicker' + index).focus();
      });
      $scope.opened = true;
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    /*** pdf view and print ***/

    $scope.pdfLoad = function (rcdetailId) {
      Restangular.one('rcdetails', rcdetailId).get().then(function (rcDetail) {
        $scope.rcDetail = rcDetail;

        $scope.info1Header = rcDetail.info1 ? rcDetail.info1.split(':')[0] : null;
        $scope.info1Trailer = rcDetail.info1 ? rcDetail.info1.split(':')[1] : null;

        $scope.info2Header = rcDetail.info2 ? rcDetail.info2.split(':')[0] : null;
        $scope.info2Trailer = rcDetail.info2 ? rcDetail.info2.split(':')[1] : null;

        $scope.info3Header = rcDetail.info3 ? rcDetail.info3.split(':')[0] : null;
        $scope.info3Trailer = rcDetail.info3 ? rcDetail.info3.split(':')[1] : null;

        $scope.info4Header = rcDetail.info4 ? rcDetail.info4.split(':')[0] : null;
        $scope.info4Trailer = rcDetail.info4 ? rcDetail.info4.split(':')[1] : null;

        $scope.info5Header = rcDetail.info5 ? rcDetail.info5.split(':')[0] : null;
        $scope.info5Trailer = rcDetail.info5 ? rcDetail.info5.split(':')[1] : null;

        $scope.info6Header = rcDetail.info6 ? rcDetail.info6.split(':')[0] : null;
        $scope.info6Trailer = rcDetail.info6 ? rcDetail.info6.split(':')[1] : null;

        $scope.info7Header = rcDetail.info7 ? rcDetail.info7.split(':')[0] : null;
        $scope.info7Trailer = rcDetail.info7 ? rcDetail.info7.split(':')[1] : null;

        $scope.info8Header = rcDetail.info8 ? rcDetail.info8.split(':')[0] : null;
        $scope.info8Trailer = rcDetail.info8 ? rcDetail.info8.split(':')[1] : null;

        $scope.info9Header = rcDetail.info9 ? rcDetail.info9.split(':')[0] : null;
        $scope.info9Trailer = rcDetail.info9 ? rcDetail.info9.split(':')[1] : null;

        $scope.info10Header = rcDetail.info10 ? rcDetail.info10.split(':')[0] : null;
        $scope.info10Trailer = rcDetail.info10 ? rcDetail.info10.split(':')[1] : null;
      });

      Restangular.one('rcdtlviews', rcdetailId).get().then(function (rcdtlview) {
        $scope.subCategoryName = rcdtlview.subcategoryname;
        $scope.dispatchno = rcdtlview.dispatchno;
        $scope.starttime = $filter('date')(rcdtlview.starttime, "dd-MM-yyyy");
      });

      Restangular.all('rcdtlviews?filter[where][worktype]=I' + '&filter[where][linkid]=' + rcdetailId).getList().then(function (issues) {
        $scope.issuesList = issues;
      });

      $scope.taskModel = true;

      var customerUrl = '';

      if ($window.sessionStorage.roleId == 2) {
        customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
      } else {
        customerUrl = 'customers?filter[where][deleteflag]=false';
      }

      Restangular.all(customerUrl).getList().then(function (cust) {
        $scope.customers = cust;
        $scope.customerLogo = cust[0].logo;
        $scope.customerSignature = cust[0].signature;

        Restangular.all('groupinfo?filter[where][customerId]=' + cust[0].id + '&filter[where][subcategoryId]=' + $scope.selectedSubCategoryId).getList().then(function (gfo) {
          $scope.groupinfo = $filter('orderBy')(gfo, 'groupid', false);

          angular.forEach($scope.groupinfo, function (member, index) {
            member.index = index + 1;

            Restangular.all('surveyquestionviews?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + cust[0].id + '&filter[where][groupserialno]=' + member.groupid).getList().then(function (srvy) {
              member.surveyquestions = srvy;

              angular.forEach(member.surveyquestions, function (member, index) {
                member.index = index + 1;
                if (member.questiontype == 5) {
                  member.answer = member.question;
                }

                Restangular.all('surveyanswers?filter[where][questionid]=' + member.id + '&filter[where][memberid]=' + rcdetailId).getList().then(function (HRes) {
                  angular.forEach(HRes, function (data, index) {
                    data.index = index + 1;
                    member.answer = data.answer;
                    if (data.answer == null || data.answer == undefined || data.answer == '') {
                      member.uploadedfilesList = [];
                    } else {
                      member.uploadedfilesList = member.answer.split(',');
                    }
                  });
                });
              });
            });
          });
        });
      });
    };

    $scope.export = function () {
      // var printContents = document.getElementById('printarea').innerHTML;
      // var originalContents = document.body.innerHTML;

      // document.body.innerHTML = printContents;

      // window.print();


      // document.body.innerHTML = originalContents;
      console.log('converting...');

      // var printableArea = document.getElementById('printable');

      // html2canvas(printableArea, {
      //   useCORS: false,
      //   onrendered: function (canvas) {

      //     var pdf = new jsPDF('p', 'pt', 'letter');

      //     var pageHeight = 980;
      //     var pageWidth = 900;
      //     for (var i = 0; i <= printableArea.clientHeight / pageHeight; i++) {
      //       var srcImg = canvas;
      //       var sX = 0;
      //       var sY = pageHeight * i; // start 1 pageHeight down for every new page
      //       var sWidth = pageWidth;
      //       var sHeight = pageHeight;
      //       var dX = 0;
      //       var dY = 0;
      //       var dWidth = pageWidth;
      //       var dHeight = pageHeight;

      //       window.onePageCanvas = document.createElement("canvas");
      //       onePageCanvas.setAttribute('width', pageWidth);
      //       onePageCanvas.setAttribute('height', pageHeight);
      //       var ctx = onePageCanvas.getContext('2d');
      //       ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

      //       var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);
      //       var width = onePageCanvas.width;
      //       var height = onePageCanvas.clientHeight;

      //       if (i > 0) // if we're on anything other than the first page, add another page
      //         pdf.addPage(612, 791); // 8.5" x 11" in pts (inches*72)

      //       pdf.setPage(i + 1); // now we declare that we're working on that page
      //       pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .62), (height * .62)); // add content to the page

      //     }
      //     setTimeout(function () {
      //       pdf.save('test.pdf');
      //     }, 5000);
      //   }
      // });
      // $scope.runMe();

      var printContent = document.getElementById('printable');
      var htmlToPrint = '' +
      '<style type="text/css">' +
      'table th, table td {' +
      'border:1px solid #000;' +
      'padding;0.5em;' +
      '}' +
      '</style>';
      var windowUrl = $scope.subCategoryName;
      var uniqueName = new Date();
      var windowName = 'Print' + uniqueName.getTime();
      var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
      htmlToPrint += printContent.outerHTML;
      printWindow.document.write(htmlToPrint);
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
    };
  });