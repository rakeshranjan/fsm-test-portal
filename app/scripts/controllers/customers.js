'use strict';

angular.module('secondarySalesApp')
  .controller('CustomersCtrl', function ($scope, $rootScope, $fileUploader, Restangular, $location, $routeParams, $window, $route, $filter) {
    /*********/

    $scope.showForm = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/customers/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/customers/create' || $location.path() === '/customers/edit/' + $routeParams.id;
      return visible;
    };

    Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (con) {
      $scope.orgroots = [];
      angular.forEach(con, function (member, index) {
        if (member.parent == null || member.parent == 0) {
          $scope.orgroots.push(member);
        }
      });
    });

    /************************************************************************************/
    $scope.customer = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteFlag: false
    };

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };

    /****************************************** CREATE *********************************/

    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.savecustomer = function () {
      document.getElementById('name').style.border = "";
      console.log($scope.customer, "message")
      if ($scope.customer.name == '' || $scope.customer.name == null) {
        $scope.customer.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Customer has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

        $scope.customerName = $scope.customer.name;
        $scope.submitDisable = true;

        Restangular.all('customers').post($scope.customer).then(function (customer) {
          $scope.saveSequence(customer.id);
        });
      }
    };

    $scope.saveSequence = function (customerId) {
      $scope.seq = {
        desc: 'ticket',
        initial: 'tckt00',
        customerId: customerId,
        sequence: 0
      };

      Restangular.all('sequences').post($scope.seq).then(function () {
        window.location = '/customers-list';
      });
    };

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Updatecustomer = function () {
      document.getElementById('name').style.border = "";

      if ($scope.customer.name == '' || $scope.customer.name == null) {
        $scope.customer.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Customer Name';
        document.getElementById('name').style.border = "1px solid #ff0000";

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      } else {
        $scope.message = 'Customer has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;

        Restangular.one('customers/' + $routeParams.id).customPUT($scope.customer).then(function () {
          window.location = '/customers-list';
        });
      }
    };

    if ($routeParams.id) {
      Restangular.one('customers', $routeParams.id).get().then(function (custom) {
        $scope.original = custom;
        $scope.customer = Restangular.copy($scope.original);
      });
    }

    AWS.config.update({
      accessKeyId: 'AKIA4IBG6MOYWQYSWCGN',
      secretAccessKey: 'PotAVI9ZAClOpIOrJERwriw6q6gRM7Om1HjPx72Y',
      region: 'ap-south-1',
      signatureVersion: 'v4'
    });

    var bucketName = 'citly'; // Enter your bucket name
    var bucket = new AWS.S3({
      params: {
        Bucket: bucketName
      }
    });

    $scope.UpdatecustomerImage = function () {
      var results = document.getElementById('results');
      var fileChooser = document.getElementById('file-chooser');
      var file = fileChooser.files[0];

      if (file) {
        results.innerHTML = '';
        var objKey = file.name;
        var params = {
          Bucket: 'citly',
          Key: objKey,
          ContentType: file.type,
          Body: file,
          ACL: 'public-read'
        };
        // console.log('results', params, results);

        $scope.customer.logo = objKey;

        bucket.putObject(params, function (err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else {
            $scope.UpdatecustomerSignature();
            // console.log(data); // successful response
          }
        });
      } else {
        if ($routeParams.id && $scope.customer.logo) {
          $scope.UpdatecustomerSignature();
        } else {
          $scope.validatestring = $scope.validatestring + 'Please Select Logo';
          $scope.toggleValidation();
          $scope.validatestring1 = $scope.validatestring;
          $scope.validatestring = '';
        }
      }
    };

    /*** upload seal ***/

    $scope.UpdatecustomerSignature = function () {
      var results = document.getElementById('results-signature');
      var fileChooser = document.getElementById('file-chooser-signature');
      var file = fileChooser.files[0];

      if (file) {
        results.innerHTML = '';
        var objKey = file.name;
        var params = {
          Bucket: 'citly',
          Key: objKey,
          ContentType: file.type,
          Body: file,
          ACL: 'public-read'
        };
        // console.log('results', params, results);

        $scope.customer.signature = objKey;

        bucket.putObject(params, function (err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else {
            if ($routeParams.id) {
              $scope.Updatecustomer();
            } else {
              $scope.savecustomer();
            }
            // console.log(data); // successful response
          }
        });
      } else {
        if ($routeParams.id && $scope.customer.logo) {
          $scope.Updatecustomer();
        } else {
          $scope.validatestring = $scope.validatestring + 'Please Select Signature';
          $scope.toggleValidation();
          $scope.validatestring1 = $scope.validatestring;
          $scope.validatestring = '';
        }
      }
    };

    /*******************************Uploder end**********************************/
  });
