'use strict';

angular.module('secondarySalesApp')
    .controller('InventoryuserCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        $scope.inv = {};

        $scope.pageSize = 25;

        $scope.customers = Restangular.all('customers?filter[where][deleteflag]=false').getList().$object;

        $scope.customersList = Restangular.all('customers?filter[where][deleteflag]=false').getList().$object;

        $scope.warehousedsply1 = Restangular.all('warehouses').getList().$object;

        $scope.getitemdefinition = function (itemdefinitionId) {
            return Restangular.one('itemdefinitions', itemdefinitionId).get().$object;
        };

        $scope.getitemstatus = function (itemstatusId) {
            return Restangular.one('itemstatuses', itemstatusId).get().$object;
        };

        $scope.getitemdefinition = function (itemdefinitionId) {
            return Restangular.one('itemdefinitions', itemdefinitionId).get().$object;
        };

        $scope.getcategory = function (categoryId) {
            return Restangular.one('categories', categoryId).get().$object;
        };

        $scope.getsubgategory = function (subcategoryId) {
            return Restangular.one('subcategories', subcategoryId).get().$object;
        };

        Restangular.all('warehouses?filter[where][deleteflag]=false&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (wh) {
            $scope.warehouses = wh;
        });

        $scope.inventorydisplay = [];

        if ($window.sessionStorage.orgStructure) {
            var splitArray = $window.sessionStorage.orgStructure.split(';');
            var splitArray1 = splitArray[splitArray.length - 1];
            var splitArray2 = splitArray1.split('-');
            var splitArray2 = splitArray2[splitArray2.length - 1];
            Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
                Restangular.all('inventories?filter[where][itemstatusId]=' + 3 + '&filter[where][warehouseId]=' + whs[0].id).getList().then(function (creq) {
                    $scope.inventories = creq;
        
                    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custList) {
        
                        angular.forEach($scope.inventories, function (member, index) {
                            //Just add the index to your item
                            member.index = index + 1;
        
                            //project
                            var data = custList.filter(function (arr) {
                                return arr.id == member.customerId
                            })[0];
        
                            if (data != undefined) {
                                member.customername = data.name;
                            }
        
                            var data1 = _.findWhere($scope.warehousedsply1, {
                                id: member.warehouseId
                            });
        
                            if (data1 != undefined) {
                                member.warehousename = data1.warehousecode;
                            }
        
        
                            var data2 = _.findWhere($scope.itemdefdisplay1, {
                                id: member.itemdefinitionId
                            });
        
                            if (data2 != undefined) {
                                member.itemdef = data2.name;
                            }
        
                            if (member.approvedflag == true) {
                                member.approved = 'Yes';
                            } else {
                                member.approved = 'No';
                            }
        
                            $scope.inventorydisplay.push(member);
                            // console.log('inventorydisplay', $scope.inventorydisplay)
                        });
                    });
                });
            });
        }

        $scope.$watch('warehouseId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                $scope.inventorydisplay = [];

                $scope.inv = Restangular.all('inventories?filter[where][warehouseId]=' + newValue + '&filter[where][itemstatusId]=' + 3).getList().then(function (inv) {
                    $scope.inventories = inv;
                    angular.forEach($scope.inventories, function (member, index) {
                        member.index = index + 1;

                        //project
                        var data = $scope.customersList.filter(function (arr) {
                            return arr.id == member.customerId
                        })[0];

                        if (data != undefined) {
                            member.customername = data.name;
                        }

                        var data1 = _.findWhere($scope.warehousedsply1, {
                            id: member.warehouseId
                        });

                        if (data1 != undefined) {
                            member.warehousename = data1.warehousecode;
                        }

                        var data2 = _.findWhere($scope.itemdefdisplay1, {
                            id: member.itemdefinitionId
                        });

                        if (data2 != undefined) {
                            member.itemdef = data2.name;
                        }

                        if (member.approvedflag == true) {
                            member.approved = 'Yes';
                        } else {
                            member.approved = 'No';
                        }

                        $scope.inventorydisplay.push(member);
                    });
                });
            }
        });
    });