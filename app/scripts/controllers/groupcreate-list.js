'use strict';

angular.module('secondarySalesApp')
  .controller('GroupCreateListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    $scope.modalTitle = 'Thank You';

    $scope.DisplayTodoStatus = [{
      name: '',
      disableLang: false,
      disableAdd: false,
      disableRemove: false,
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      langdark: 'images/Lgrey.png'
    }];

    $scope.getcustomer = function (customerId) {
      return Restangular.one('customers', customerId).get().$object;
    };

    $scope.getcategory = function (categoryId) {
      return Restangular.one('categories', categoryId).get().$object;
    };

    $scope.getsubcategory = function (subcategoryId) {
      return Restangular.one('subcategories', subcategoryId).get().$object;
    };

    $scope.Add = function (index) {

      $scope.myobj = {};

      var idVal = index + 1;

      $scope.DisplayTodoStatus.push({
        name: '',
        disableLang: false,
        disableAdd: false,
        disableRemove: false,
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        langdark: 'images/Lgrey.png'
      });
    };

    $scope.Remove = function (index) {
      var indexVal = index - 1;
      $scope.DisplayTodoStatus.splice(indexVal, 1);
    };

    $scope.myobj = {};

    $scope.typeChange = function (index) {
      $scope.DisplayTodoStatus[index].name = '';
      $scope.DisplayTodoStatus[index].disableLang = false;
      $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
    };

    $scope.levelChange = function (index) {
      $scope.DisplayTodoStatus[index].disableLang = true;
      $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
      $scope.DisplayTodoStatus[index].disableAdd = false;
      $scope.DisplayTodoStatus[index].disableRemove = false;
    };

    $scope.langModel = false;

    $scope.Lang = function (index, name, type) {

      $scope.lastIndex = index;

      angular.forEach($scope.languages, function (data) {
        data.lang = name;
        // console.log(data);
      });

      $scope.langModel = true;
    };

    //   $scope.SaveLang = function () {

    //     if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
    //       $scope.validatestring = $scope.validatestring + 'Please Enter Due Days';
    //       document.getElementById('order').style.borderColor = "#FF0000";

    //     }
    //     if ($scope.myobj.actionable == '' || $scope.myobj.actionable == null) {
    //       $scope.validatestring = $scope.validatestring + 'Please Select Actionable';
    //       document.getElementById('action').style.borderColor = "#FF0000";
    //     }
    //     if ($scope.validatestring != '') {
    //       $scope.toggleValidation();
    //       $scope.validatestring1 = $scope.validatestring;
    //       $scope.validatestring = '';
    //       //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    //     } else {

    //       //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
    //       $scope.DisplayTodoStatus[$scope.lastIndex].disableAdd = true;
    //       $scope.DisplayTodoStatus[$scope.lastIndex].disableRemove = true;
    //       $scope.DisplayTodoStatus[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
    //       $scope.DisplayTodoStatus[$scope.lastIndex].actionable = $scope.myobj.actionable;

    //       angular.forEach($scope.languages, function (data, index) {
    //         data.inx = index + 1;
    //         $scope.DisplayTodoStatus[$scope.lastIndex]["lang" + data.inx] = data.lang;
    //         console.log($scope.DisplayTodoStatus);;
    //       });

    //       $scope.langModel = false;
    //     }
    //   };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    // if ($window.sessionStorage.prviousLocation != "partials/onetoonelist-view" || $window.sessionStorage.prviousLocation != "partials/onetoonelist") {
    //     $window.sessionStorage.myRoute_currentPage = 1;
    //     $window.sessionStorage.myRoute_currentPagesize = 25;
    // }
    if ($window.sessionStorage.prviousLocation != "partials/groupcreate-list" || $window.sessionStorage.prviousLocation != "partials/groupcreate") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    // Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().then(function (srvy) {
    //     $scope.surveyquestions = srvy;

    // Restangular.all('groupinfo').getList().then(function (ginfo) {

    if ($window.sessionStorage.roleId == 2) {
      $scope.groupinfo = [];
    } else {
      Restangular.all('groupviews').getList().then(function (ginfo) {
        $scope.groupinfo = ginfo;
        angular.forEach($scope.groupinfo, function (member, index) {
          member.index = index + 1;
        });
      });
    }

    $scope.customerId = '';
    $scope.categoryId = '';
    $scope.subcategoryId = '';

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customers?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;
    });

    $scope.$watch('customerId', function (newValue, oldValue) {
      if (newValue == '' || oldValue == undefined) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (catg) {
          $scope.categories = catg;
        });
        // Restangular.all('groupinfo?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (gfo) {
        Restangular.all('groupviews?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (gfo) {
          $scope.groupinfo = gfo;

          angular.forEach($scope.groupinfo, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    $scope.$watch('categoryId', function (newValue, oldValue) {
      if (newValue == '' || oldValue == undefined) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (scatg) {
          $scope.subcategories = scatg;
        });
        // Restangular.all('groupinfo?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (gfo) {
        Restangular.all('groupviews?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (gfo) {
          $scope.groupinfo = gfo;
          angular.forEach($scope.groupinfo, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    $scope.$watch('subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || oldValue == undefined) {
        return;
      } else {
        // Restangular.all('groupinfo?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (gfo) {
        Restangular.all('groupviews?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (gfo) {
          $scope.groupinfo = gfo;
          angular.forEach($scope.groupinfo, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });
  });
