'use strict';

angular.module('secondarySalesApp')
  .controller('SchCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/schemes/create' || $location.path() === '/schemes/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/schemes/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/schemes/create'|| $location.path() === '/schemes/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/schemes/create'|| $location.path() === '/schemes/' + $routeParams.id;
        return visible;
      };

    
    /*********/

  //  $scope.documenttypes = Restangular.all('documenttypes').getList().$object;
    
    if($routeParams.id){
      Restangular.one('schemes', $routeParams.id).get().then(function(scheme){
				$scope.original = scheme;
				$scope.scheme = Restangular.copy($scope.original);
      });
    }
    $scope.Search = $scope.name;
    
/************************************************************************** INDEX *******************************************/
       $scope.zn = Restangular.all('schemes').getList().then(function (zn) {
        $scope.schemes = zn;
        angular.forEach($scope.schemes, function (member, index) {
            member.index = index + 1;
        });
    });
    
/*************************************************************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.Save = function () {
          
                    $scope.schemes.post($scope.scheme).then(function () {
                        console.log('schemes  Saved');
                        window.location = '/schemes';
                    });
        };
/*************************************************************************** UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
          if ($scope.scheme.name == '' || $scope.scheme.name == null) {
                $scope.scheme.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your document Type';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.schemes.customPUT($scope.scheme).then(function () {
                        console.log('schemes Saved');
                        window.location = '/schemes';
                    });
                }


        };
/*************************************************************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('schemes/' + id).remove($scope.scheme).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


