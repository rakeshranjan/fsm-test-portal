'use strict';

angular.module('secondarySalesApp')
    .controller('marathiCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        $scope.isCreateSave = true;
        $scope.heading = 'Multi-Language';

        $scope.multilanguage = {
            languageId: 2
        };

        $scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;
        $scope.submitmobilemultilanguages = Restangular.all('mobilemultilanguages').getList().$object;
        $scope.submitfacilitymultilanguages = Restangular.all('facilitymultilanguages').getList().$object;

        $scope.SaveLanguage = function () {
            $scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
                $scope.submitmobilemultilanguages.customPUT($scope.mobilemultilanguage).then(function (resp) {
                    $scope.submitfacilitymultilanguages.customPUT($scope.facilitymultilanguage).then(function (resp) {
                        console.log('Response', response);
                        window.location = '/Languages/marathi';
                    });
                });
            });
        };

        Restangular.one('multilanguages', 6).get().then(function (zone) {
            $scope.original = zone;
            $scope.multilanguage = Restangular.copy($scope.original);
            Restangular.one('mobilemultilanguages', 6).get().then(function (mobile) {
                $scope.mobileoriginal = mobile;
                $scope.mobilemultilanguage = Restangular.copy($scope.mobileoriginal);
                Restangular.one('facilitymultilanguages', 6).get().then(function (facility) {
                    $scope.facilityoriginal = facility;
                    $scope.facilitymultilanguage = Restangular.copy($scope.facilityoriginal);
                    $scope.modalInstanceLoad.close();
                });
            });
        });

        $scope.LanguageName = 'मराठी';
        $scope.Language = 'भाषा';
    });