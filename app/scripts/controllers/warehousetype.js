'use strict';
angular.module('secondarySalesApp')
    .controller('WarehousetypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
          }
      
        //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        if ($window.sessionStorage.prviousLocation != "partials/warehousetype" || $window.sessionStorage.prviousLocation != "partials/customers") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
          }
      
          $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
          $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
          };
      
          $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
          $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
          };
    
           // ******************************** EOF Pagination *************************************************

        Restangular.all('warehousetypes').getList().then(function (warehousetype) {
            $scope.warehousetypes = warehousetype;
            angular.forEach($scope.warehousetypes, function (member, index) {
              member.index = index + 1;


            });
        });
});