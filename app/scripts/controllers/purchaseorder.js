'use strict';

angular.module('secondarySalesApp')
    .controller('PurchaseorderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        if ($window.sessionStorage.roleId != 2) {
            Restangular.all('poheaders?filter[where][flag]=false').getList().then(function (po) {
                $scope.poheaders = po;
                angular.forEach($scope.poheaders, function (member, index) {
                    member.index = index + 1;
                });
            });
        }

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getPartner = function (partnerId) {
            return Restangular.one('partners', partnerId).get().$object;
        };

        if ($window.sessionStorage.roleId == 2) {
            Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
                $scope.customers = cust;
            });
        } else {
            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
                $scope.customers = cust;
            });
        }

        $scope.getPartner = function (partnerid) {
            return Restangular.one('partners', partnerid).get().$object;
        };

        $scope.$watch('customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });

                Restangular.all('poheaders?filter[where][customerId]=' + newValue + '&filter[where][flag]=false').getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });

                Restangular.all('poheaders?filter[where][categoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('poheaders?filter[where][subcategoryId]=' + newValue + '&filter[where][flag]=false').getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });

                Restangular.all('partners?filter[where][subcategoryId]=' + newValue).getList().then(function (ptrs) {
                    $scope.partners = ptrs;
                });
            }
        });

        $scope.$watch('VendorId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('poheaders?filter[where][todepotdist]=' + newValue + '&filter[where][flag]=false').getList().then(function (po) {
                    $scope.poheaders = po;
                    angular.forEach($scope.poheaders, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });
    });