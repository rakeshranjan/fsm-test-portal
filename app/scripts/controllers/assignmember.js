'use strict';
angular.module('secondarySalesApp').controller('AssignMemberCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $document, $timeout) {
  /*********/
  $scope.getTodotype = function (todotypeId) {
    return Restangular.one('todotypes', todotypeId).get().$object;
  };
  //Restangular.all('membertodoview?filter[where][orgtructure][like]=%' + $window.sessionStorage.orgStructure + '%&filter[where][todoCount][lte]=0').getList().then(function (member) {
  //$scope.members = member;
  //});
  // Restangular.all('users?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (member) {
  //   $scope.users = member;
  // });

  // var vorgStructure = window.sessionStorage.orgStructure;
  // var strLevelcities = vorgStructure.split("-");
  // var strcities = strLevelcities[1];
  // console.log(strcities, "Cities-Id")
  //console.log("Here at assign ticket")

  // $scope.today = function () {
  //   $scope.startdate = new Date();
  // };
  // $scope.mindate = new Date();
  // //$scope.dateformat = "MM/dd/yyyy";
  // $scope.today();
  // $scope.showcalendar = function ($event) {
  //   $scope.showdp = true;
  // };
  // $scope.showdp = false;
  //$scope.mindate = "2020-03-20T00:00:00";
  //$scope.todaydate = "2020-03-20T00:00:00";
  // $scope.startdate = new Date();
  //$scope.mindate = new Date();
  //console.log($scope.mindate, "DATES-32", $scope.startdate)
  var date = new Date();
  $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  //console.log($scope.FromDate, "CURRENT DATE - 36")
  var startdate = $scope.FromDate + "T00:00:00";
  $scope.mindate = startdate;
  //console.log(startdate, "Fdate-38", $scope.todaydate)
  // var vorgStructure = window.sessionStorage.orgStructure;
  // //console.log(str, "STR")
  // var strval = vorgStructure.split(";");
  // console.log(strval, "STR-VAL")

  // var countryval = strval[0].split("-");
  // console.log(countryval, "COUNTRY-VAL")
  // var countryvalId = countryval[1];

  // var stateval = strval[1].split("-");
  // console.log(stateval, "STATE-VAL")
  // var statevalId = stateval[1];

  // var cityval = strval[2].split("-");
  // console.log(cityval, "CITY-VAL")
  // var cityvalId = cityval[1];

  var orgstr = $window.sessionStorage.orgStructure;
  //console.log(orgstr, "STR-ELSE")
  var strorgval = orgstr.split(";");
  //console.log(strorgval, "strorgval-132-ELSE")
  var stateval = strorgval[1].split("-");
  //console.log(stateval, stateval[1], "STATE-VAL-ELSE")
  var cityval = strorgval[2].split("-");
  //console.log(cityval, cityval[1], cityval.length, "CITY-VAL-ELSE")

  // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=null').getList().then(function (cityresp) {
  // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=null&filter={"where": {"id": {"inq": [1,2,3,4]}}}').getList().then(function (cityresp) {
  // Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter={"where": {"id": {"inq": [' + strcities + ']}}}').getList().then(function (cityresp) {
  // Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + strcities + ']}}}&filter={"where": {"deleteflag": "false"}}').getList().then(function (cityresp) {

  // Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + strcities + ']}}}').getList().then(function (cityresp) {
  Restangular.all('organisationlocations?filter={"where": {"id": {"inq": [' + cityval[1] + ']}}}').getList().then(function (cityresp) {
    //console.log(cityresp, "CITY-RESP")
    $scope.cities = cityresp;
  });

  $scope.$watch('city', function (newValue, oldValue) {
    if (newValue == '' || newValue == null || newValue == oldValue) {
      return;
    } else {
      //console.log(newValue, "newValue")
      // Restangular.all('membertodoview?filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (member) {
      // Restangular.all('membertodoview?&filter[where][assignFlag]=false&filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (member) {
      // Restangular.all('rcdetailtodoview?&filter[where][assignFlag]=false&filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (member) {
      Restangular.all('rcdetailtodoview?&filter[where][assignFlag]=false&filter[where][fieldassignFlag]=true&filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (member) {
        // Restangular.all('membertodoview?&filter[where][assignFlag]=false&filter[where][fieldassignFlag]=true&filter[where][orgStructure][like]=%' + newValue + '%').getList().then(function (member) {
        //console.log(member, "Member");
        $scope.members = member;
      });

    }
  });

  $scope.$parent.startdate = new Date();

  $scope.$watch('equipment', function (newValue, oldValue) {
    if (newValue == '' || newValue == null || newValue == oldValue) {
      return;
    } else {
      //console.log(newValue, "newValue-136")
      // Restangular.all('users?filter[where][level][like]=%' + $window.sessionStorage.orgLevel + '%').getList().then(function (member) {
      //   Restangular.all('users?filter[where][orgStructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (member) {
      //   console.log(member, "MEMBER")
      //   $scope.users = member;
      // });
      // Restangular.one('membertodoview?filter[where][id]=' + newValue).get().then(function (orglevelresp) {
      Restangular.one('rcdetailtodoview?filter[where][id]=' + newValue).get().then(function (orglevelresp) {
        //console.log(newValue, "newValue")
        $scope.orgStructure = orglevelresp[0];
        //console.log($scope.orgStructure, "$scope.orgStructure")
        $scope.orgStructure = orglevelresp[0].orgStructure;
        //console.log($scope.orgStructure, "ORG-STRUC")
        // var actualorgStructure = $scope.orgStructure.split(';')[1].split('-');//Previously
        var actualorgStructure = $scope.orgStructure.split(';')[1].split('-');
        actualorgStructure = actualorgStructure[1];
        //console.log(actualorgStructure, "ACTUAL")
        Restangular.all('users?filter[where][orgStructure][like]=%' + $scope.orgStructure + '%').getList().then(function (userresp) {
          // Restangular.all('users?filter[where][orgStructure][like]=%' + actualorgStructure + '%').getList().then(function (userresp) {
          //console.log(userresp, "userresp")
          $scope.users = userresp;
        });
      });
    }
  });

  $scope.todoCount = 0;
  $scope.updateTodo = function () {
    document.getElementById('datepickerstartdate').style.border = "";
    document.getElementById('datepickerenddate').style.border = "";

    if ($scope.startdate == '' || $scope.startdate == null) {
      //toaster.pop('error', 'please select Start Date');
      $scope.startdate = null;
      $scope.validatestring = $scope.validatestring + 'Please Select Start Date';
      document.getElementById('datepickerstartdate').style.border = "1px solid #ff0000";
    } else if ($scope.enddate == '' || $scope.enddate == null) {
      //toaster.pop('error', 'please select End Date');
      $scope.enddate = null;
      $scope.validatestring = $scope.validatestring + 'Please Select End Date';
      document.getElementById('datepickerenddate').style.border = "1px solid #ff0000";
      //console.log($scope.$parent.startdate, "START-TIME")
      //   console.log($scope.startdate, "--", starttime, $scope.$parent.startdate, "START-TIME")
    } else {
      $scope.memberUpdate = {
        id: $scope.equipment,
        assignedto: $scope.user,
        starttime: $scope.$parent.startdate,
        endtime: $scope.$parent.enddate,
        assignFlag: true,
        statusId: 3
      };
      // Restangular.one('members', $scope.equipment).customPUT($scope.memberUpdate).then(function (resp) {
      Restangular.one('rcdetails', $scope.equipment).customPUT($scope.memberUpdate).then(function (resp) {
        window.location = '/assigntickets-list';
        // window.location = '/todos';
      });
    }
  }
  //   $scope.updateTodo = function () {
  //     $scope.memberUpdate = {
  //       id: $scope.equipment,
  //       assignedto: $scope.user
  //     };
  //     Restangular.one('members', $scope.equipment).customPUT($scope.memberUpdate).then(function (resp) {
  //       window.location = '/todos';
  //     });
  //   }

  $scope.dtmax = new Date();
  $scope.toggleMin = function () {
    $scope.minDate = ($scope.minDate) ? null : new Date();
  };
  $scope.toggleMin();
  $scope.picker = {};
  $scope.mod = {};
  $scope.start = {};
  $scope.incident = {};
  $scope.hlth = {};
  $scope.datestartedart = {};
  $scope.lasttest = {};

  $scope.disabled = function (date, mode) {
    return (mode === 'day' && (date.getDay() === 0));
  };

  $scope.openstartdate = function ($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepickerstartdate' + index).focus();
    });
    $scope.picker.startdateopened = true;
  };

  $scope.openenddate = function ($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    $timeout(function () {
      $('#datepickerenddate' + index).focus();
    });
    $scope.picker.enddateopened = true;
  };

  //Timepicker Start
  $scope.FromTime = new Date();
  $scope.ToTime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 1;

  $scope.ismeridian = true;

  $scope.startchanged = function () {
    console.log('Start Time changed to: ' + $filter('date')($scope.starttime, 'shortTime'));
  };
  $scope.endchanged = function () {
    console.log('End Time changed to: ' + $filter('date')($scope.endtime, 'shortTime'));
  };

  $scope.clear = function () {
    $scope.mytime = null;
  };
  //Timepicker End

});
