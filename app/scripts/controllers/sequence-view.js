'use strict';

angular.module('secondarySalesApp')
  .controller('SequenceCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    $scope.showForm = function () {
      var visible = $location.path() === '/sequence/create' || $location.path() === '/sequence/edit/' + $routeParams.id;
      return visible;
    };

    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/sequence/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/sequence/create' || $location.path() === '/sequence/edit/' + $routeParams.id;
      return visible;
    };


    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/sequence/create' || $location.path() === '/sequence/edit/' + $routeParams.id;
      return visible;
    };

    $scope.sequence = {
      customerId: $window.sessionStorage.customerId
    };

    $scope.modalTitle = 'Thank You';

    $scope.DisplayTodoStatus = [{
      name: '',
      disableLang: false,
      disableAdd: false,
      disableRemove: false,
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      langdark: 'images/Lgrey.png'
    }];

    $scope.Add = function (index) {

      $scope.myobj = {};

      var idVal = index + 1;

      $scope.DisplayTodoStatus.push({
        name: '',
        disableLang: false,
        disableAdd: false,
        disableRemove: false,
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        langdark: 'images/Lgrey.png'
      });
    };

    $scope.Remove = function (index) {
      var indexVal = index - 1;
      $scope.DisplayTodoStatus.splice(indexVal, 1);
    };

    $scope.myobj = {};

    $scope.typeChange = function (index) {
      $scope.DisplayTodoStatus[index].name = '';
      $scope.DisplayTodoStatus[index].disableLang = false;
      $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
    };

    $scope.levelChange = function (index) {
      $scope.DisplayTodoStatus[index].disableLang = true;
      $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
      $scope.DisplayTodoStatus[index].disableAdd = false;
      $scope.DisplayTodoStatus[index].disableRemove = false;
    };

    $scope.langModel = false;

    $scope.Lang = function (index, name, type) {

      $scope.lastIndex = index;

      angular.forEach($scope.languages, function (data) {
        data.lang = name;
        // console.log(data);
      });

      $scope.langModel = true;
    };

    if ($routeParams.id) {
      Restangular.one('sequences', $routeParams.id).get().then(function (seq) {
        $scope.sequence = seq;
      });
    }

    $scope.Save = function () {
      Restangular.all('sequences').post($scope.sequence).then(function () {
        window.location = '/sequence-list';
      });
    };

    $scope.Update = function () {
      Restangular.one('sequences', $routeParams.id).customPUT($scope.sequence).then(function () {
        window.location = '/sequence-list';
      });
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    // if ($window.sessionStorage.prviousLocation != "partials/onetoonelist-view" || $window.sessionStorage.prviousLocation != "partials/onetoonelist") {
    //     $window.sessionStorage.myRoute_currentPage = 1;
    //     $window.sessionStorage.myRoute_currentPagesize = 25;
    // }
    if ($window.sessionStorage.prviousLocation != "partials/customerslist-view" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    var seqUrl = '';
    $scope.showAddButton = true;

    if ($window.sessionStorage.roleId == 2) {
      $scope.showAddButton = false;
      seqUrl = 'sequences?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $window.sessionStorage.customerId;
    } else {
      $scope.showAddButton = true;
      seqUrl = 'sequences?filter[where][deleteflag]=false';
    }

    Restangular.all(seqUrl).getList().then(function (seq) {
      $scope.sequences = seq;
      angular.forEach($scope.sequences, function (member, index) {
        member.index = index + 1;
      });
    });
  });
