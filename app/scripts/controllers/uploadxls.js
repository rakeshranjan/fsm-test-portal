"use strict";

angular
  .module("secondarySalesApp")
  .controller("FormLevelRCDetailsUploadxlsCtrl", function (
    $scope,
    $rootScope,
    Restangular,
    $location,
    $routeParams,
    $window,
    $route,
    toaster,
    $filter
  ) {
    console.log("In upload xls");

    //$scope.hideSubmit = true;
    //$(".loader").css("display", "block");
    //,
    //toaster

    $scope.DisableSave = false;

    // Restangular.all("customers?filter[where][deleteflag]=false")
    //   .getList()
    //   .then(function (cust) {
    //     $scope.customers = cust;
    //   });

    if (window.sessionStorage.roleId == 1) {
      Restangular.all('customers?filter[where][deleteFlag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });
    } 
    else if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4) {
      //console.log("here-177")
      Restangular.all('customers?filter[where][deleteFlag]=false&filter[where][id]=' + window.sessionStorage.customerId).getList().then(function (cust) {
        $scope.customers = cust;
      });
    }

    // Restangular.all("organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=null")
    //   .getList()
    //   .then(function (countryresp) {
    //     $scope.countries = countryresp;
    //   });

    //   Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catg) {
    //     $scope.categories = catg;
    //   });

    $scope.$watch("member.customerId", function (newValue, oldValue) {
      if (newValue == "" || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all(
            "categories?filter[where][deleteflag]=false&filter[where][customerId]=" +
            newValue
          )
          .getList()
          .then(function (catgresp) {
            $scope.categories = catgresp;
          });
      }
    });

    $scope.$watch("member.categoryId", function (newValue, oldValue) {
      if (newValue == "" || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all(
            "subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=" +
            newValue
          )
          .getList()
          .then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
          });
      }
    });

    // Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
    //   console.log(ogrlevls, "ORGLEVLS-47")
    //   $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + ogrlevls[0].id).getList().then(function (ogrlocs) {
    //     $scope.organisationlevels = ogrlevls;
    //     console.log($scope.organisationlevels, "$scope.organisationlevels-321-User related")
    //     angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
    //       if (organisationlevel.id === ogrlevls[0].id) {
    //         organisationlevel.organisationlocations = ogrlocs;
    //       }
    //     });
    //   });
    // });
    //here commented -----

    // $scope.orgLevelChange = function (index, level) {
    //   console.log(level, "LEVEL")
    //   $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
    //     if (index < $scope.organisationlevels.length && level.length > 0) {
    //       $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
    //     }
    //   });
    // };

    // Restangular.all(
    //   "organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=null"
    // )
    //   .getList()
    //   .then(function(cityresp) {
    //     $scope.cities = cityresp;
    //   });

    // $scope.$watch("member.cityId", function(newValue, oldValue) {
    //   if (newValue == "" || newValue == null || newValue == oldValue) {
    //     return;
    //   } else {
    //     //console.log(newValue, "newValue-136")
    //     Restangular.one("organisationlocations?filter[where][id]=" + newValue)
    //       .get()
    //       .then(function(orglevelresp) {
    //         $scope.orglevel = orglevelresp[0];
    //         //console.log($scope.orglevel, "$scope.orglevel")
    //         $scope.level = orglevelresp[0].level;
    //         //console.log($scope.level, "LEVEL")
    //         Restangular.all(
    //           "organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=" +
    //             newValue +
    //             "&filter[where][parentlevel]=" +
    //             $scope.level
    //         )
    //           .getList()
    //           .then(function(pincoderesp) {
    //             $scope.pincodes = pincoderesp;
    //           });
    //       });
    //   }
    // });

    console.log('$window.sessionStorage.organizationId', $window.sessionStorage.organizationId);
    $scope.hideSubmit = true;
    $scope.DisableValidate = true;

    var X = XLSX;
    var XW = {
      /* worker message */
      msg: 'xlsx',
      /* worker scripts */
      rABS: 'scripts/services/xlsxworker2.js',
      norABS: 'scripts/services/xlsxworker1.js',
      noxfer: 'scripts/services/xlsxworker.js'
    };


    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
    if (!rABS) {
      document.getElementsByName("userabs")[0].disabled = true;
      document.getElementsByName("userabs")[0].checked = false;
    }

    var use_worker = typeof Worker !== 'undefined';
    if (!use_worker) {
      document.getElementsByName("useworker")[0].disabled = true;
      document.getElementsByName("useworker")[0].checked = false;
    }

    function xw_noxfer(data, cb) {
      var worker = new Worker(XW.noxfer);
      worker.onmessage = function (e) {
        switch (e.data.t) {
          case 'ready':
            break;
          case 'e':
            console.error(e.data.d);
            break;
          case XW.msg:
            cb(JSON.parse(e.data.d));
            break;
        }
      };
      var arr = rABS ? data : btoa(fixdata(data));
      worker.postMessage({
        d: arr,
        b: rABS
      });
    }

    function xw_xfer(data, cb) {
      var worker = new Worker(rABS ? XW.rABS : XW.norABS);
      worker.onmessage = function (e) {
        switch (e.data.t) {
          case 'ready':
            break;
          case 'e':
            console.error(e.data.d);
            break;
          default:
            var xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r");
            console.log("done");
            $scope.DisableValidate = false;
            cb(JSON.parse(xx));
            break;
        }
      };
      if (rABS) {
        var val = s2ab(data);
        worker.postMessage(val[1], [val[1]]);
      } else {
        worker.postMessage(data, [data]);
      }
    }

    function xw(data, cb) {
      //transferable = document.getElementsByName("xferable")[0].checked;
      transferable = true;
      if (transferable) xw_xfer(data, cb);
      else xw_noxfer(data, cb);
    }

    var transferable = use_worker;
    if (!transferable) {
      document.getElementsByName("xferable")[0].disabled = true;
      document.getElementsByName("xferable")[0].checked = false;
    }

    function s2ab(s) {
      var b = new ArrayBuffer(s.length * 2),
        v = new Uint16Array(b);
      for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
      return [v, b];
    }

    function ab2str(data) {
      var o = "",
        l = 0,
        w = 10240;
      for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
      o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
      return o;
    }

    function to_json(workbook) {
      var result = {};
      workbook.SheetNames.forEach(function (sheetName) {
        var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
          result[sheetName] = roa;
        }
      });
      return result;
    }

    function to_csv(workbook) {
      var result = [];
      workbook.SheetNames.forEach(function (sheetName) {
        var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
        if (csv.length > 0) {
          result.push("SHEET: " + sheetName);
          result.push("");
          result.push(csv);
        }
      });
      return result.join("\n");
    }

    function to_formulae(workbook) {
      var result = [];
      workbook.SheetNames.forEach(function (sheetName) {
        var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
        if (formulae.length > 0) {
          result.push("SHEET: " + sheetName);
          result.push("");
          result.push(formulae.join("\n"));
        }
      });
      return result.join("\n");
    }

    function process_wb(wb) {
      var output = "";
      switch (get_radio_value("format")) {
        case "csv":
          output = to_csv(wb);
          break;
        case "form":
          output = to_formulae(wb);
          break;
        default:
          output = JSON.stringify(to_json(wb), 2, 2);
          $scope.entries = to_json(wb);
          $scope.newEntries = [];
          //  $scope.xlsentries = $scope.entries.Sheet1;
          $scope.xlsentries = $scope.entries.Sheet0;
          $scope.entries.Sheet1 = $scope.entries.Sheet0;
          var currentEntryIndex;
          for (var i = 0; i < $scope.entries.Sheet1.length; i++) {
            // if ($scope.entries.Sheet1[i].no) {
            // if ($scope.entries.Sheet1[i].serno) {
            if ($scope.entries.Sheet1[i]['Dispatch Number']) {
              //currentEntryIndex = i;

            } else {
              console.log("Here-255")
              // $scope.entries.Sheet1[currentEntryIndex].noofdropdown++;

            }
          }

          for (var i = $scope.entries.Sheet1.length - 1; i >= 0; i--) {
            // if ($scope.entries.Sheet1[i].no == undefined) {
            // if ($scope.entries.Sheet1[i].serno == undefined) {
            //  $scope.entries.Sheet1[i]['dispatchno'] = $scope.entries.Sheet1[i]['Dispatch Number'];

            // if ($scope.xlsentries[0]['Dispatch Number'] != null) {
            $scope.entries.Sheet1[i]['dispatchno'] = $scope.entries.Sheet1[i]['Dispatch Number'];

            $scope.entries.Sheet1[i]['servicecalldate'] = $scope.entries.Sheet1[i]['Service Call Date'];

            $scope.entries.Sheet1[i]['serviceprovider'] = $scope.entries.Sheet1[i]['Service Provider'];

            $scope.entries.Sheet1[i]['logisticprovider'] = $scope.entries.Sheet1[i]['Logistics Provider'];

            $scope.entries.Sheet1[i]['transport'] = $scope.entries.Sheet1[i]['Transport'];

            $scope.entries.Sheet1[i]['dispatchstatus'] = $scope.entries.Sheet1[i]['Dispatch Status'];

            $scope.entries.Sheet1[i]['lob'] = $scope.entries.Sheet1[i]['LOB'];

            $scope.entries.Sheet1[i]['sysclassification'] = $scope.entries.Sheet1[i]['System Classification'];

            $scope.entries.Sheet1[i]['endservicewindow'] = $scope.entries.Sheet1[i]['End Service Window'];

            $scope.entries.Sheet1[i]['servicetag'] = $scope.entries.Sheet1[i]['Service Tag'];

            $scope.entries.Sheet1[i]['dsptype'] = $scope.entries.Sheet1[i]['DPS Type'];

            $scope.entries.Sheet1[i]['calltype'] = $scope.entries.Sheet1[i]['Call Type'];

            $scope.entries.Sheet1[i]['servicelevel'] = $scope.entries.Sheet1[i]['Service Level'];

            $scope.entries.Sheet1[i]['customerno'] = $scope.entries.Sheet1[i]['Customer Number'];

            $scope.entries.Sheet1[i]['customername'] = $scope.entries.Sheet1[i]['Customer Name'];

            $scope.entries.Sheet1[i]['customercontactname'] = $scope.entries.Sheet1[i]['Customer Contact Name'];

            $scope.entries.Sheet1[i]['customercontactphno'] = $scope.entries.Sheet1[i]['Customer Contact Phone Number'];

            $scope.entries.Sheet1[i]['customercontactemail'] = $scope.entries.Sheet1[i]['Customer Contact Email Address'];

            $scope.entries.Sheet1[i]['customersecondarycontactname'] = $scope.entries.Sheet1[i]['Customer Secondary Contact Name'];

            $scope.entries.Sheet1[i]['customersecondarycontactphno'] = $scope.entries.Sheet1[i]['Customer Secondary Contact Phone Number'];

            $scope.entries.Sheet1[i]['customersecondarycontactemail'] = $scope.entries.Sheet1[i]['Customer Secondary Contact Email Address'];

            $scope.entries.Sheet1[i]['country'] = $scope.entries.Sheet1[i]['Country'];

            $scope.entries.Sheet1[i]['state'] = $scope.entries.Sheet1[i]['State'];

            $scope.entries.Sheet1[i]['city'] = $scope.entries.Sheet1[i]['City'];

            $scope.entries.Sheet1[i]['servicepostalcode'] = $scope.entries.Sheet1[i]['Service Postal Code'];

            $scope.entries.Sheet1[i]['serviceaddress1'] = $scope.entries.Sheet1[i]['Service Address Line 1'];

            $scope.entries.Sheet1[i]['serviceaddress2'] = $scope.entries.Sheet1[i]['Service Address Line 2'];

            $scope.entries.Sheet1[i]['serviceaddress3'] = $scope.entries.Sheet1[i]['Service Address Line 3'];

            $scope.entries.Sheet1[i]['serviceaddress4'] = $scope.entries.Sheet1[i]['Service Address Line 4'];

            $scope.entries.Sheet1[i]['partnumber'] = $scope.entries.Sheet1[i]['Part Number'];

            $scope.entries.Sheet1[i]['partquantity'] = $scope.entries.Sheet1[i]['Part Quantity'];

            $scope.entries.Sheet1[i]['partstatus'] = $scope.entries.Sheet1[i]['Part Status'];

            $scope.entries.Sheet1[i]['partstatusdate'] = $scope.entries.Sheet1[i]['Part Status Date'];

            $scope.entries.Sheet1[i]['carriername'] = $scope.entries.Sheet1[i]['Carrier Name'];

            $scope.entries.Sheet1[i]['waybillno'] = $scope.entries.Sheet1[i]['Waybill Number'];

            $scope.entries.Sheet1[i]['agentdescription'] = $scope.entries.Sheet1[i]['Agent Description'];

            $scope.entries.Sheet1[i]['closuredate'] = $scope.entries.Sheet1[i]['Closure Date'];

            $scope.entries.Sheet1[i]['commentstovendor'] = $scope.entries.Sheet1[i]['Comments to Vendor'];

            $scope.entries.Sheet1[i]['warrantyinvoicedate'] = $scope.entries.Sheet1[i]['Warranty Invoice Date'];

            $scope.entries.Sheet1[i]['warrantyinvoiceno'] = $scope.entries.Sheet1[i]['Warranty Invoice Number'];

            $scope.entries.Sheet1[i]['warrantybillfromstate'] = $scope.entries.Sheet1[i]['Warranty Bill From State'];

            $scope.entries.Sheet1[i]['originalorderbuid'] = $scope.entries.Sheet1[i]['Original Order BUID'];

            $scope.entries.Sheet1[i]['replycode'] = $scope.entries.Sheet1[i]['Reply Code'];

            $scope.entries.Sheet1[i]['engineerid'] = $scope.entries.Sheet1[i]['Engineer Id'];

            $scope.entries.Sheet1[i]['engineername'] = $scope.entries.Sheet1[i]['Engineer Name'];

            if ($scope.entries.Sheet1[i]['Dispatch Number'] == undefined) {
              $scope.entries.Sheet1.splice(i, 1);
            }
          }

          console.log($scope.entries, "307")

          angular.forEach($scope.xlsentries, function (member, index) {
            //Just add the index to your item;

            member.index = index;
          });

          // console.log('output', $scope.entries.Sheet1);
      }
      /*if (out.innerText === undefined) out.textContent = output;
      else out.innerText = output;*/
      if (typeof console !== 'undefined') console.log("output", new Date());
    }

    function get_radio_value(radioName) {
      var radios = document.getElementsByName(radioName);
      for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked || radios.length === 1) {
          return radios[i].value;
        }
      }
    }

    var xlf = document.getElementById('xlf');
    console.log('Event Start');

    function handleFile(e) {
      $scope.hideSubmit = true;
      $scope.hideValidate = false;
      console.log('Event');
      //rABS = document.getElementsByName("userabs")[0].checked;
      // use_worker = document.getElementsByName("useworker")[0].checked;
      rABS = true;
      use_worker = true;
      var files = e.target.files;
      $scope.myFileObject = files[0];
      var f = files[0]; {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
          if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
          var data = e.target.result;
          if (use_worker) {
            xw(data, process_wb);
          } else {
            var wb;
            if (rABS) {
              wb = X.read(data, {
                type: 'binary'
              });
            } else {
              var arr = fixdata(data);
              wb = X.read(btoa(arr), {
                type: 'base64'
              });
            }
            process_wb(wb);
          }
        };
        if (rABS) reader.readAsBinaryString(f);
        else reader.readAsArrayBuffer(f);
      }
    }

    function validateFileType(e) {
      $scope.DisableSubmit = true;
      $scope.xlsentries = [];
      // $scope.offermanagement.imagename = this.value.split(/[\/\\]/).pop();
      // console.log('fileInput',this.value);

      var _validVideoFileExtensions = [".xlsx", ".xls"];
      var sFileName = this.value;
      if (sFileName.length > 0) {
        var blnValid = false;
        for (var j = 0; j < _validVideoFileExtensions.length; j++) {
          var sCurExtension = _validVideoFileExtensions[j];
          if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
            blnValid = true;
            $scope.CorrectFormat = true;
            handleFile(e);
            break;
          }
        }

        if (!blnValid) {
          alert("Sorry, " + sFileName.split(/[\/\\]/).pop() + " is invalid, allowed extensions are: " + _validVideoFileExtensions.join(", "));
          xlf.value = null;
          //$scope.uploader.queue[0].remove();
          // console.log('$scope.uploader',$scope.uploader.queue[0]);
          $scope.CorrectFormat = false;
        }
      }

      /* if (item.file.type != 'video/mp4') {
           alert('Accepts Only .mp4 files');
           item.remove();
       }*/
    };

    xlf.addEventListener('change', validateFileType, false);



    //------------------- Save ----------------------------
    Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {

      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + ogrlevls[0].id).getList().then(function (ogrlocs) {
        $scope.organisationlevels = ogrlevls;

        $scope.orgstructure = "";
        angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
          if (organisationlevel.id === ogrlevls[0].id) {
            organisationlevel.organisationlocations = ogrlocs;
          }
          if ($scope.orgstructure === "") {
            $scope.orgstructure = organisationlevel.id + "-" + organisationlevel.locationid;
          } else {
            $scope.orgstructure = $scope.orgstructure + ";" + organisationlevel.id + "-" + organisationlevel.locationid;
          }
        });

      });
    });

    $scope.partners = Restangular.all("rcdetails").getList().$object;
    //console.log($scope.partners, "rcdetails-object")
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';

    $scope.SaveCount = 0;

    $scope.fileobject = {};

    //console.log($scope.entries.Sheet1.length, "$scope.entries.Sheet1.length-439")

    $scope.Save = function () {
      document.getElementById('customer').style.border = "";
      if ($scope.member.customerId == '' || $scope.member.customerId == null) {
        $scope.member.customerId = null;
        console.log("Please Select Customer-441")
        document.getElementById('customer').style.border = "1px solid #ff0000";
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';
        toaster.pop('error', 'Please Select Customer');
        // document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.member.categoryId == '' || $scope.member.categoryId == null) {
        $scope.member.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Category';
        toaster.pop('error', 'Please Select Category');

      } else if ($scope.member.subcategoryId == '' || $scope.member.subcategoryId == null) {
        $scope.member.subcategoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select SubCategory';
        toaster.pop('error', 'Please Select SubCategory');
      }

      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';

      } else {
        $scope.DisableSave = true;

        $scope.fileobject.filename = $scope.myFileObject.name;
        $scope.fileobject.uploaddate = new Date();
        $scope.fileobject.deleteFlag = false;

        Restangular.all('fileuploads').post($scope.fileobject).then(function (resp) {
          $scope.SaveRecords(resp.id);
        });
      }
    };

    $scope.SaveRecords = function (fileid) {

      $scope.item = $scope.entries.Sheet1;

      /**********************Spiner*************/
      var opts = {
        lines: 10, // The number of lines to draw
        length: 1, // The length of each line
        width: 20, // The line thickness
        radius: 10, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#47A9F7', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 42, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
      };
      var target = document.getElementById('spinner');
      //var spinner = new Spinner(opts).spin(target);
      $scope.spinner = new Spinner(opts);

      /***************************************/
      console.log($scope.entries.Sheet1.length, "$scope.entries.Sheet1.length-471");

      if ($scope.SaveCount < $scope.entries.Sheet1.length) {
        $scope.item[$scope.SaveCount].customerId = $scope.member.customerId;
        $scope.item[$scope.SaveCount].categoryId = $scope.member.categoryId;
        $scope.item[$scope.SaveCount].subcategoryId = $scope.member.subcategoryId;

        $scope.item[$scope.SaveCount].statusId = 1;
        $scope.item[$scope.SaveCount].nextquestionId = 1;
        $scope.item[$scope.SaveCount].deleteflag = false;
        $scope.item[$scope.SaveCount].language = 1;
        $scope.item[$scope.SaveCount].assignedto = 0;
        $scope.item[$scope.SaveCount].completedFlag = false;
        $scope.item[$scope.SaveCount].assignFlag = false;
        $scope.item[$scope.SaveCount].approvalFlag = false;
        $scope.item[$scope.SaveCount].fieldassignFlag = false;
        $scope.item[$scope.SaveCount].rollbackStatus = 'N';
        $scope.item[$scope.SaveCount].lastmodifiedby = $window.sessionStorage.userId;
        $scope.item[$scope.SaveCount].lastmodifiedrole = $window.sessionStorage.roleId;
        //$scope.item[$scope.SaveCount].lastmodifiedtime = new Date();
        $scope.item[$scope.SaveCount].createdby = $window.sessionStorage.userId;
        $scope.item[$scope.SaveCount].createdtime = new Date();
        $scope.item[$scope.SaveCount].createdrole = $window.sessionStorage.roleId;
        $scope.item[$scope.SaveCount].fileid = fileid;

        // Restangular.one('organisationlocations/findOne?filter[where][name]=' + $scope.item[$scope.SaveCount].pincode).get().then(function (orglocpincoderesp) {
        // Restangular.one('organisationlocations/findOne?filter[where][deleteflag]=false&filter[where][name]=' + $scope.item[$scope.SaveCount].pincode).get().then(function (orglocpincoderesp) {
        Restangular.one('organisationlocations/findOne?filter[where][deleteflag]=false&filter[where][name]=' + $scope.item[$scope.SaveCount].servicepostalcode).get().then(function (orglocpincoderesp) {
          $scope.pincodeid = orglocpincoderesp.id;
          Restangular.one('organisationlocations', orglocpincoderesp.parent).get().then(function (orgloccityresp) {
            $scope.cityid = orgloccityresp.id;
            Restangular.one('organisationlocations', orgloccityresp.parent).get().then(function (orglocstateresp) {
              $scope.stateid = orglocstateresp.id;
              Restangular.one('organisationlocations', orglocstateresp.parent).get().then(function (orgloczoneresp) {
                $scope.zoneid = orgloczoneresp.id;
                $scope.countryid = orgloczoneresp.id;
                console.log("$scope.orgstructure",$scope.orgstructure);
                var strorgstructval = $scope.orgstructure.split(";");
                var strcountry = strorgstructval[0];
                // var strzone = strorgstructval[1];
                var strstate = strorgstructval[1];
                var strcity = strorgstructval[2];
                 var strpincode = strorgstructval[3];
                var actualorgstructure = strcountry.split("-")[0] + "-" + $scope.countryid + ";" + strstate.split("-")[0] + "-" + $scope.stateid + ";" + strcity.split("-")[0] + "-" + $scope.cityid 
                + ";"  + strpincode.split("-")[0] + "-" + $scope.pincodeid;
                // var actualorgstructure = strcountry.split("-")[0] + "-" + $scope.countryid + ";" + strzone.split("-")[0] + "-" + $scope.zoneid + ";" + strstate.split("-")[0] + "-" + $scope.stateid + ";" + strcity.split("-")[0] + "-" + $scope.cityid 
                // + ";" + strpincode.split("-")[0] + "-" + $scope.pincodeid;

                $scope.item[$scope.SaveCount].countryId = $scope.countryid;
                $scope.item[$scope.SaveCount].zoneId = $scope.zoneid;
                $scope.item[$scope.SaveCount].stateId = $scope.stateid;
                $scope.item[$scope.SaveCount].cityId = $scope.cityid;
                $scope.item[$scope.SaveCount].pincodeId = $scope.pincodeid;
                $scope.item[$scope.SaveCount].orgstructure = actualorgstructure;

                // var address = $scope.item[$scope.SaveCount].customeraddress;
                var address = $scope.item[$scope.SaveCount].serviceaddress2;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                  'address': address
                }, function (results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    $scope.item[$scope.SaveCount].latitude = location.lat();
                    $scope.item[$scope.SaveCount].longitude = location.lng();

                    $scope.spinner.spin(document.getElementById('spinner'));

                    var findDuplicate = $scope.partners.filter(function (arr) {
                      return arr.dispatchno == $scope.item[$scope.SaveCount].dispatchno
                    });

                    if (findDuplicate.length == 0) {
                      console.log('no duplicate');
                      Restangular.all('rcdetails').post($scope.item[$scope.SaveCount]).then(function (Resp) {
                        $scope.SaveCount++;
                        $scope.SaveRecords(fileid);
                      });
                    } else {
                      console.log('duplicate record');
                      $scope.SaveCount++;
                      $scope.SaveRecords(fileid);
                    }
                  } else {
                    $scope.item[$scope.SaveCount].latitude = null;
                    $scope.item[$scope.SaveCount].longitude = null;

                    $scope.spinner.spin(document.getElementById('spinner'));

                    var findDuplicate = $scope.partners.filter(function (arr) {
                      return arr.dispatchno == $scope.item[$scope.SaveCount].dispatchno
                    });

                    if (findDuplicate.length == 0) {
                      console.log('no duplicate');
                      Restangular.all('rcdetails').post($scope.item[$scope.SaveCount]).then(function (Resp) {
                        $scope.SaveCount++;
                        $scope.SaveRecords(fileid);
                      });
                    } else {
                      console.log('duplicate record');
                      $scope.SaveCount++;
                      $scope.SaveRecords(fileid);
                    }
                  }
                });
              });
            });
          });
        });
      } else {
        $scope.spinner.stop();
        window.location = "/formleveldetailslist";
      }
    };

    // here saty
    //--------------------------- Validate -----------------------------

    $scope.questionTypes = ["Radio Button", "Text Field", "Text Display", "Check Box"];
    $scope.questionTypesShotNames = ["r", "t", "td", "c"];
    $scope.Validate = function () {
      $scope.errortext = '';
      var re = /\S+@\S+\.\S+/;

      // if ($scope.xlsentries[0]['serno'] && $scope.xlsentries[0]['rcstatus'] && $scope.xlsentries[0]['orderno'] && $scope.xlsentries[0]['modeldescription'] && $scope.xlsentries[0]['customername'] && $scope.xlsentries[0]['customeraddress'] && $scope.xlsentries[0]['pincode'] && $scope.xlsentries[0]['state'] && $scope.xlsentries[0]['city'] && $scope.xlsentries[0]['contactno'] && $scope.xlsentries[0]['contactpersonname']) {
      if ($scope.xlsentries[0]['dispatchno'] && $scope.xlsentries[0]['serviceprovider'] && $scope.xlsentries[0]['dispatchstatus'] && $scope.xlsentries[0]['lob'] && $scope.xlsentries[0]['sysclassification'] && $scope.xlsentries[0]['servicetag'] && $scope.xlsentries[0]['dsptype'] && $scope.xlsentries[0]['servicelevel'] && $scope.xlsentries[0]['customerno'] && $scope.xlsentries[0]['customername'] && $scope.xlsentries[0]['customercontactname'] && $scope.xlsentries[0]['customercontactphno'] && $scope.xlsentries[0]['customercontactemail'] && $scope.xlsentries[0]['country'] && $scope.xlsentries[0]['state'] && $scope.xlsentries[0]['servicepostalcode'] && $scope.xlsentries[0]['serviceaddress1'] && $scope.xlsentries[0]['serviceaddress2'] && $scope.xlsentries[0]['serviceaddress3'] && $scope.xlsentries[0]['serviceaddress4']) {
        // dataList["state"] exists. do stuff.
        console.log('presence');
        $scope.hideValidate = true;

        for (var i = 0; i < $scope.xlsentries.length; i++) {

          if ($scope.xlsentries[i]['dispatchno'] == "" || $scope.xlsentries[i]['dispatchno'].length != 11) {
            alert('Your dispatchno is not in specified format. It should be of exactly 11 digits.');
            //$scope.hideSubmit = true;
            $scope.hideValidate = false;
            // $scope.errortext = $scope.errortext + 'Invalid dispatchno at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 1) + ' Valid dispatchno is of 11 digits \r\n';
            // $scope.errortext = $scope.errortext + 'Invalid dispatchno at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 1 + ' Row Number ' + parseInt(i + 1)) + ' Valid dispatchno is of 11 digits \r\n';
            $scope.errortext = $scope.errortext + 'Invalid dispatchno at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + ' Valid dispatchno is of 11 digits \r\n';
          }
          //}
          // for (var j = 0; j < $scope.xlsentries.length; j++) {
          //   if ($scope.xlsentries[j]['serviceprovider'] == null) {
          //     alert('Your serviceprovider data is having blank value. Please check the serviceprovider column');
          //     $scope.hideValidate = false;
          //     $scope.errortext = $scope.errortext + 'Blank serviceprovider found at Column Number::' + JSON.stringify($scope.xlsentries[j].index + 2) + '\r\n';
          //   }
          // }
          if ($scope.xlsentries[i]['serviceprovider'] == null) {
            alert('Your serviceprovider data is having blank value. Please check the serviceprovider column');
            $scope.hideValidate = false;
            // $scope.errortext = $scope.errortext + 'Blank serviceprovider found at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 2 + ' Row Number ' + parseInt(i + 1)) + '\r\n';
            $scope.errortext = $scope.errortext + 'Blank serviceprovider found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['dispatchstatus'] == null) {
            alert('Your dispatchstatus data is having blank value. Please check the dispatchstatus column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank dispatchstatus found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['lob'] == null) {
            alert('Your lob data is having blank value. Please check the lob column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank lob found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['sysclassification'] == null) {
            alert('Your sysclassification data is having blank value. Please check the sysclassification column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank sysclassification found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['servicetag'] == null) {
            alert('Your servicetag data is having blank value. Please check the servicetag column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank servicetag found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['dsptype'] == null) {
            alert('Your dsptype data is having blank value. Please check the dsptype column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank dsptype found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['servicelevel'] == null) {
            alert('Your servicelevel data is having blank value. Please check the servicelevel column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank servicelevel found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['customerno'] == null) {
            alert('Your customerno data is having blank value. Please check the customerno column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank customerno found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['customername'] == null) {
            alert('Your customername data is having blank value. Please check the customername column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank customername found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['customercontactname'] == null) {
            alert('Your customercontactname data is having blank value. Please check the customercontactname column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank customercontactname found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['customercontactphno'] == null) {
            alert('Your customercontactphno data is having blank value. Please check the customercontactphno column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank customercontactphno found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['customercontactemail'] == null) {
            alert('Your customercontactemail data is having blank value. Please check the customercontactemail column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank customercontactemail found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['country'] == null) {
            alert('Your country data is having blank value. Please check the country column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank country found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['state'] == null) {
            alert('Your state data is having blank value. Please check the state column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank state found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          // if ($scope.xlsentries[i]['city'] == null) {
          //   alert('Your city data is having blank value. Please check the city column');
          //   $scope.hideValidate = false;
          //   $scope.errortext = $scope.errortext + 'Blank city found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          // }
          if ($scope.xlsentries[i]['servicepostalcode'] == null) {
            alert('Your servicepostalcode data is having blank value. Please check the servicepostalcode column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank servicepostalcode found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['serviceaddress1'] == null) {
            alert('Your serviceaddress1 data is having blank value. Please check the serviceaddress1 column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank serviceaddress1 found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['serviceaddress2'] == null) {
            alert('Your serviceaddress2 data is having blank value. Please check the serviceaddress2 column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank serviceaddress2 found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['serviceaddress3'] == null) {
            alert('Your serviceaddress3 data is having blank value. Please check the serviceaddress3 column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank serviceaddress3 found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }
          if ($scope.xlsentries[i]['serviceaddress4'] == null) {
            alert('Your serviceaddress4 data is having blank value. Please check the serviceaddress4 column');
            $scope.hideValidate = false;
            $scope.errortext = $scope.errortext + 'Blank serviceaddress4 found at ::' + JSON.stringify('Row Number ' + parseInt(i + 1)) + '\r\n';
          }

          //------------ Previous validation type ------------------------------------------------------------
          //console.log($scope.xlsentries.length,"XLS-ENTRIES-LENGTH")
          // if ($scope.questionTypesShotNames.indexOf($scope.xlsentries[i].questiontype) === -1) {
          //   if ($scope.questionTypesShotNames.indexOf($scope.xlsentries[i].serno) === -1) {
          //   //console.log(i + ':::' + $scope.xlsentries[i].questiontype.length);
          //   console.log($scope.xlsentries[i].serno, "XLSENTRIES-SERNO")
          //   //console.log($scope.questionTypesShotNames.indexOf($scope.xlsentries[i].questiontype),"INDEX OF")
          //     //console.log(i + ':::' + $scope.xlsentries[i].mobile.length);
          //     // console.log(i + ':::' + $scope.xlsentries[i].zip.length);
          //     $scope.errortext = $scope.errortext + 'Invalid Question Type at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 2) + ' Valid Question Types Are "Radio Button", "Text Field", "Text Display", "Check Box" \r\n';
          // }
        } //end of for loop

        var textFile = null,
          makeTextFile = function (text) {
            var data = new Blob([text], {
              type: 'text/plain'
            });
            // If we are replacing a previously generated file we need to
            // manually revoke the object URL to avoid memory leaks.
            if (textFile !== null) {
              window.URL.revokeObjectURL(textFile);
            }
            textFile = window.URL.createObjectURL(data);
            return textFile;
          };

        if ($scope.errortext != '') {
          alert('Some Of your data is not in proper format. Click on "Download Error Text" to download the error log.');
          var link = document.getElementById('downloadlink');
          link.href = makeTextFile($scope.errortext);
          link.style.display = 'block';
        } else {
          $scope.hideSubmit = false;
        }
      } else {
        // dataList["state"] does not exist
        // console.log('absence');
        alert('Your xls is not in specified format.');
      }

    }

  });
