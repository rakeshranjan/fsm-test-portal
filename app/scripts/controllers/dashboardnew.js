'use strict';

angular.module('secondarySalesApp')
  .controller('DashboardNewCtrl', function ($scope, $http, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

    if ($window.sessionStorage.roleId === '33' || $window.sessionStorage.roleId === '15') {
      $scope.showAssign = false;
    } else if ($window.sessionStorage.roleId === '5') {
      $scope.showAssign = false;
    }
    else {
      $scope.showAssign = true;
    }
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/dashboardnew") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });
    $scope.$watch('dashboardnew.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == oldValue) {
        return;
      }
      else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (piegresp) {
          console.log("sdf", piegresp);
          $scope.pie = piegresp;
        });
      }
    });

    $scope.$watch('dashboardnew.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (piegresp) {
          console.log("asdf", piegresp);
          $scope.pie = piegresp;
        });
      }
    });

    $scope.$watch('dashboardnew.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        console.log("newvalue", newValue);
        if ($window.sessionStorage.roleId == 1) {
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][subcategoryId]=' + newValue)
        .getList().then(function (piegresp) {
          console.log("asdf", piegresp);
          $scope.pie = piegresp;
          console.log("ppp", $scope.pie);
          console.log("token", $window.sessionStorage.accessToken);
          $scope.buildpiechart(piegresp);
        });
      }
      else if($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15)
      {
        var vorgStructure = window.sessionStorage.orgStructure;
      var strLevelcities = vorgStructure.split(";");
      var strcities = strLevelcities[2];
      var strcitiesid = strcities.split("-");
      console.log(strcitiesid, "Cities-Id");
      var strvalue = strcitiesid[1];
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][subcategoryId]=' + newValue+'&filter[where][cityId]=' + strvalue)
        .getList().then(function (piegresp) {
          $scope.pie = piegresp;
          $scope.buildpiechart(piegresp);
        });

      }
      else if($window.sessionStorage.roleId == 5)
      {
        var vorgStructure1 = window.sessionStorage.orgStructure;
      var strLevelcities1 = vorgStructure1.split(";");
      var strcities1 = strLevelcities1[2];
      var strcitiesid1 = strcities1.split("-");
      console.log(strcitiesid1, "Cities-Id");
      var strvalue1 = strcitiesid1[1];
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][subcategoryId]=' + newValue+'&filter[where][cityId]=' + strvalue1)
        .getList().then(function (piegresp) {
          $scope.pie = piegresp;
          $scope.buildpiechart(piegresp);
        });

      }
      else if($window.sessionStorage.roleId == 11)
      {
        var strvalue1 = window.sessionStorage.userId;
        Restangular.all('ticket_status_role_view?filter[where][deleteflag]=false&filter[where][subcategoryId]=' + newValue+'&filter[where][assignedto]=' + strvalue1)
        .getList().then(function (data2) {
          // $scope.pie = piegresp;

          function listProducts(prods) {
            var product_names = [];
          for(var i=0;i<data2.length;i++){
            if(data2[i].name=="ASSIGN"||data2[i].name=="IN PROGRESS"||data2[i].name=="COMPLETED"||data2[i].name=="APPROVAL PENDING")
            {
              product_names.push({
                todostatusId: prods[i].todostatusId,
                name: prods[i].name,
                total_open_count: prods[i].total_open_count,
                stotal_backoffice_count: prods[i].total_backoffice_count,
                total_assign_count: prods[i].total_assign_count,
                total_inprogress_count: prods[i].total_inprogress_count,
                total_completed_count: prods[i].total_completed_count,
                total_approval_pending_count: prods[i].total_approval_pending_count,
                customerId:prods[i].customerId,
                categoryId:prods[i].categoryId,
                subcategoryId:prods[i].subcategoryId,
                });
              
            }

          }
          return product_names;
          }
          $scope.buildpiechart(listProducts(data2));
        });
      }
      }
      
    });
          $scope.buildpiechart = function (piegresp) {
          var Gtotal_open_count = 0;
          var Gtotal_backoffice_count =0;
          var Gtotal_assign_count = 0;
          var Gtotal_inprogress_count = 0;
          var Gtotal_completed_count = 0;
          var Gtotal_approval_pending_count = 0;
          console.log("piegresp", piegresp);
          console.log("rrr", piegresp.length);

          if (piegresp.length == 0) {
            $scope.charts =
              [
                {
                  key: 'OPEN',
                  value: 0
                },
                {
                  key: 'BACK OFFICE',
                  value: 0
                },
                {
                  key: 'ASSIGNED',
                  value: 0
                },
                {
                  key: 'IN PROGRESS',
                  value: 0
                },
                {
                  key: 'COMPLETED',
                  value: 0
                },
                {
                  key: 'APPROVAL PENDING',
                  value: 0
                }
              ];
          }
          else {
            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId == 1) {
                Gtotal_open_count = Gtotal_open_count + $scope.pie[i].total_open_count;

              }
              var name1 = 'OPEN';

              var temp1 = Gtotal_open_count;

            }
            
            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId == 2) {
                 Gtotal_backoffice_count = Gtotal_backoffice_count + $scope.pie[i].total_backoffice_count;

              }
              var name2 = 'BACK OFFICE';

              var temp2 = Gtotal_backoffice_count;

            }

            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId == 3) {
                Gtotal_assign_count = Gtotal_assign_count + $scope.pie[i].total_assign_count;

              }
              var name3 = 'ASSIGNED';

              var temp3 = Gtotal_assign_count;

            }

            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId == 4) {
                Gtotal_inprogress_count = Gtotal_inprogress_count + $scope.pie[i].total_inprogress_count;

              }
              var name4 = 'IN PROGRESS';

              var temp4 = Gtotal_inprogress_count;


            }
            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId == 5) {
                Gtotal_completed_count = Gtotal_completed_count + $scope.pie[i].total_completed_count;

              }
              var name5 = 'COMPLETED';

              var temp5 = Gtotal_completed_count;
              console.log("ttt", piegresp[i].name);

            }
            for (var i = 0; i < piegresp.length; i++) {
              if (piegresp[i].todostatusId ==6) {
                Gtotal_approval_pending_count = Gtotal_approval_pending_count + $scope.pie[i].total_approval_pending_count;

              }
              var name6 = 'APPROVAL PENDING';

              var temp6 = Gtotal_approval_pending_count;
              console.log("name", temp5);

            }
            var s = piegresp[0].customerId;
            var d = piegresp[0].categoryId;
            var f = piegresp[0].subcategoryId;

            console.log("www", piegresp.length);
            $scope.charts = [
              {
                key: name1,
                value: temp1,
                ar: [s, d, f]
              },
              {
                key: name2,
                value: temp2,
                ar: [s, d, f]
              },
              {
                key: name3,
                value: temp3,
                ar: [s, d, f]
              },
              {
                key: name4,
                value: temp4,
                ar: [s, d, f]
              },
              {
                key: name5,
                value: temp5,
                ar: [s, d, f]
              },
              {
                key: name6,
                value: temp6,
                ar: [s, d, f]
              }
            ];
          }

          console.log("df", $scope.charts);
          var width = 430, height = 320;
          var colors = d3.scaleOrdinal().range(['#4363d8','#800000', '#911eb4', '#f58231', '#f032e6', '#008080']);
          var color_hash = {  0 : ['#4363d8'],
          1 : ['#800000'],
          2 : ['#911eb4'],
          3 : ['#f58231'],
          4 : ['#f032e6'],
          5 : ['#008080']
        }
          var svg = d3.select("#pie-chart").append("svg").attr("width", width).attr("height", height).style("background", "white");

          var data1 = d3.pie().sort(null).value(function (d) { return d.value; })($scope.charts);

          console.log("ght", data1);
          var segments = d3.arc().innerRadius(120 - 75)
            .outerRadius(120)
            .padAngle(0.05)
            .padRadius(50)

          var arcOver = d3.arc().outerRadius(120 + 15).innerRadius(120 - 60).padAngle(0.1);

          var sections = svg.append("g").attr("transform", "translate(220,140)")
            .selectAll("path")
            .data(data1).attr("class", "arc");

          var path = sections.enter().append("path").attr("d", segments).classed("parts", true).attr("fill", function (d, i) {
            return colors(i);
          });

          path.on("mouseenter", function (d) {
            d3.select(this)
              .attr("stroke", "black")
              .transition()
              .duration(200)
              .attr("d", arcOver)
              .attr("stroke-width", 1);
          })

            .on("mouseleave", function (d) {
              d3.select(this).transition().duration(500).attr("d", segments).attr("stroke", "none");
            })

          .on("click", function (d) {
            console.log("cli", d);
            if (d.data.key == 'IN PROGRESS') {
              console.log("hhh", d);
              
               if (d.data.key == 'IN PROGRESS') {
                var st = 4;
              }
            }
            else if(d.data.key == 'BACK OFFICE'){
              console.log("bck", d);
              if (d.data.key == 'BACK OFFICE') {
                  var st = 2;
                }
              }
              else if(d.data.key == 'ASSIGNED'){
                console.log("assi", d);
                if (d.data.key == 'ASSIGNED') {
                    var st = 3;
                  }
                }
                console.log("rrrrrttttttttttt",d);
                if ($window.sessionStorage.roleId == '1') {
              Restangular.all('ticket_progresspercent_view?filter[where][deleteflag]=false&filter[where][customerId]=' + d.data.ar[0] + '&filter[where][categoryId]=' + d.data.ar[1] + '&filter[where][subcategoryId]=' + d.data.ar[2] + '&filter[where][statusId]=' + st + '&access_token=' + $window.sessionStorage.accessToken)
                .getList().then(function (data3) {
                  console.log("data2", data3);
                  console.log("ggg", data3.length);

                  function listProducts(prods) {
                    var product_names = [];
                    for (var i = 0; i < prods.length; i += 1) {
                      product_names.push({
                        key2: prods[i].fullname,
                        value2: prods[i].progresspercent,
                        statusId2: prods[i].statusId,
                        starttime2: prods[i].starttime,
                        endtime2: prods[i].endtime,
                        customer2: prods[i].customer,
                        category2: prods[i].category,
                        subcategory2: prods[i].subcategory,
                        status2: prods[i].status,
                        username2:prods[i].username,
                        etatime2:prods[i].etatime});
                    }
                    return product_names;
                  }
                  console.log("fff", listProducts(data3));
                  console.log(listProducts(data3).length);
                  var sor = listProducts(data3);
                  // var ascarray= _.sortBy(sor, function(home) {return parseInt(home.value2);});
                  sor.sort(function (a, b) {
                    return a.value2 - b.value2;
                  });
                  console.log("ascarray",sor);
                  $scope.buildbarchart(sor);

                });
              }
              else if ($window.sessionStorage.roleId == '7' || $window.sessionStorage.roleId == '15') {
                Restangular.all('ticket_progresspercent_view?filter[where][deleteflag]=false&filter[where][customerId]=' + d.data.ar[0] + '&filter[where][categoryId]=' + d.data.ar[1] + '&filter[where][subcategoryId]=' + d.data.ar[2] + '&filter[where][statusId]=' + st + '&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + $window.sessionStorage.accessToken)
                .getList().then(function (data3) {
                  console.log("data2", data3);
                  console.log("ggg", data3.length);

                  function listProducts(prods) {
                    var product_names = [];
                    for (var i = 0; i < prods.length; i += 1) {
                      product_names.push({
                        key2: prods[i].fullname,
                        value2: prods[i].progresspercent,
                        statusId2: prods[i].statusId,
                        starttime2: prods[i].starttime,
                        endtime2: prods[i].endtime,
                        customer2: prods[i].customer,
                        category2: prods[i].category,
                        subcategory2: prods[i].subcategory,
                        status2: prods[i].status,
                        username2:prods[i].username,
                        etatime2:prods[i].etatime});
                    }
                    return product_names;
                  }
                  console.log("fff", listProducts(data3));
                  console.log(listProducts(data3).length);
                  var sor = listProducts(data3);
                  sor.sort(function (a, b) {
                    return a.value2 - b.value2;
                  });
                  console.log("ascarray",sor);
                  $scope.buildbarchart(sor);

                });
              }
              else if ($window.sessionStorage.roleId == '5') {
                Restangular.all('ticket_progresspercent_view?filter[where][deleteflag]=false&filter[where][customerId]=' + d.data.ar[0] + '&filter[where][categoryId]=' + d.data.ar[1] + '&filter[where][subcategoryId]=' + d.data.ar[2] + '&filter[where][statusId]=' + st + '&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%&access_token=' + $window.sessionStorage.accessToken)
                .getList().then(function (data3) {
                  console.log("data2", data3);
                  console.log("ggg", data3.length);

                  function listProducts(prods) {
                    var product_names = [];
                    for (var i = 0; i < prods.length; i += 1) {
                      product_names.push({
                        key2: prods[i].fullname,
                        value2: prods[i].progresspercent,
                        statusId2: prods[i].statusId,
                        starttime2: prods[i].starttime,
                        endtime2: prods[i].endtime,
                        customer2: prods[i].customer,
                        category2: prods[i].category,
                        subcategory2: prods[i].subcategory,
                        status2: prods[i].status,
                        username2:prods[i].username,
                        etatime2:prods[i].etatime});
                    }
                    return product_names;
                  }
                  console.log("fff", listProducts(data3));
                  console.log(listProducts(data3).length);
                  var sor = listProducts(data3);
                  sor.sort(function (a, b) {
                    return a.value2 - b.value2;
                  });
                  console.log("ascarray",sor);
                  $scope.buildbarchart(sor);

                });
              }
              else if ($window.sessionStorage.roleId == '11') {
               var strvalue1 = window.sessionStorage.userId;
                Restangular.all('ticket_progresspercent_view?filter[where][deleteflag]=false&filter[where][customerId]=' + d.data.ar[0] + '&filter[where][categoryId]=' + d.data.ar[1] + '&filter[where][subcategoryId]=' + d.data.ar[2] + '&filter[where][statusId]=' + st + '&filter[where][assignedto]=' + strvalue1 + '&access_token=' + $window.sessionStorage.accessToken)
                .getList().then(function (data3) {
                  console.log("data2", data3);
                  console.log("ggg", data3.length);

                  function listProducts(prods) {
                    var product_names = [];
                    for (var i = 0; i < prods.length; i += 1) {
                      product_names.push({
                        key2: prods[i].fullname,
                        value2: prods[i].progresspercent,
                        statusId2: prods[i].statusId,
                        starttime2: prods[i].starttime,
                        endtime2: prods[i].endtime,
                        customer2: prods[i].customer,
                        category2: prods[i].category,
                        subcategory2: prods[i].subcategory,
                        status2: prods[i].status,
                        etatime2:prods[i].etatime,
                        username2:prods[i].username,
                        etatime2:prods[i].etatime
                        });
                    }
                    return product_names;
                  }
                  console.log("fff", listProducts(data3));
                  console.log("eta",listProducts(data3).etatime2);
                  var sor = listProducts(data3);
                  sor.sort(function (a, b) {
                    return a.value2 - b.value2;
                  });
                  console.log("ascarray",sor);
                  $scope.buildbarchart(sor);
                });
              }
          });

                  // var inprogressvalues =  listProducts(data).filter(function(hero) {
                  //   return hero.statusId == 4;
                  // });
                  // console.log("tyu",inprogressvalues);

                  // function groupBy(objectArray, property) {
                  //   return objectArray.reduce(function (acc, obj) {
                  //     var key = obj[property];
                  //     if (!acc[key]) {
                  //       acc[key] = [];
                  //     }
                  //     acc[key].push(obj);
                  //     return acc;
                  //   }, {});
                  // }

                  // var groupedPeople = groupBy(listProducts(data), 'key');

                  // console.log("kkk", groupedPeople);
                  // console.log("lll", Object.keys(groupedPeople));
                  // var keyvalues = Object.keys(groupedPeople);
                  // for (var j = 0; j < keyvalues.length; j++) 
                  // {
                  //   console.log("xxx", keyvalues[0]);
                  //   var values = Object.values(groupedPeople);
                  //   console.log("ccc", values[0]);
                  //   console.log("vvv", values[0].length);
                  //   var lowest = Number.POSITIVE_INFINITY;
                  //   var highest = Number.NEGATIVE_INFINITY;
                  //   var tmp;
                  //   for (var i = values[j].length - 1; i >= 0; i--) {
                  //     tmp = values[j][i].value;
                  //     if (tmp < lowest) lowest = tmp;
                  //     if (tmp > highest) highest = tmp;
                  //   }
                  //   var newArray= new Array({key:keyvalues[j],value:highest,statusId:listProducts(data)[j].statusId},);
                  // }
                  // console.log("ggg", highest);
                  // console.log("lll", lowest);
                  // console.log("high", newArray);

            
          var content = d3.select("g").selectAll("text").data(data1);
          content.enter().append("text").classed("inside", true).each
            (function (d) {
              var center = segments.centroid(d);
              if (d.value != 0) {
                d3.select(this).attr("x", center[0]).attr("y", center[1])
                  .text(d.value);
              }
            });

          // var legends = svg.append("g")
          //   .attr("transform", "translate(1, 0)")
          //   .selectAll(".legends").data(data1);

          // var legend = legends.enter().append("g").classed("legends", true)
          //   .attr("transform", function (d, i) { return "translate(0," + (i + 1) * 22 + ")"; });

          // legend.append("rect").attr("width", 13).attr("height", 13).attr("fill", function (d, i) {
          //   return colors(i);
          // });
          // legend.append("text").classed("label", true).text(function (d) { return d.data.key; })
          //   // .attr("fill", function (d,i) { return colors(i); })
          //   .attr("x", 20)
          //   .attr("y", 10);

    var legend = svg.append("g")
           .attr("class", "legend")
           .attr("x", width)
         // .attr("y", 10)
       .attr("height", 50)
       .attr("width", 300)
       .attr("transform", "translate(" + (35) + ", 210)");

    legend.selectAll('g').data(data1)
      .enter()
      .append('g')
      .attr( "transform", function(d,i) {
        if(i<3){
       var xOff = (i % 3) * 80
        var yOff = Math.floor(i  / 3) * 30
        return "translate(" + xOff + "," + yOff + ")"
      }
      else{
        var xOff = (i % 3) * 140
        var yOff = Math.floor(i  / 3) * 30
        return "translate(" + xOff + "," + yOff + ")"
      }
       })
      .each(function(d, i) {
        console.log("lll",d)
        if(i<3){
        var g = d3.select(this);
      g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 60)
      .attr("y", 65).style("fill", color_hash[String(i)][0]);
      g.append("text").classed("label", true).text(function (d) {console.log("sdfj",d);
      return d.data.key; })
        //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
        .attr("x", i * 60 + 20)
        .attr("y", 75).style("fill", "#000000");
        }
        else{
          console.log("ooo",i);
          var g = d3.select(this);
      g.append("rect").attr("width", 14).attr("height", 14).attr("x", i * 0)
      .attr("y", 65).style("fill", color_hash[String(i)][0]);
      g.append("text").classed("label", true).text(function (d) {console.log("sdfj",d);
      return d.data.key; })
        //  .attr("fill", function (d,k) { console.log("ooo",k);return colors(k); })
        .attr("x", i * 0 + 20)
        .attr("y", 75).style("fill", "#000000");
        }
        });
      }
       

    $scope.buildbarchart = function (pData) {
      
      console.log("jkl", pData);
      console.log("eta",pData.etatime2);
      var padding = { top: 15, right: 20, bottom: 63, left: 30 };
      var width = 500 - padding.left - padding.right;
      var height = 300 - padding.top - padding.bottom;

      var svg = d3.select("#bar-chart").append("svg")
        .attr("width", width + padding.left + padding.right)
        .attr("height", height + padding.top + padding.bottom)
        .append("g")
        .attr("transform", "translate(" + padding.left + "," + padding.top + ")");

        var tooltip = d3.select("body").append("div").attr("class", "toolTip");

      var yScale = d3.scaleLinear()
        .domain([0, 100/*d3.max($scope.charts,function(d){return d.value2 })*/])
        .range([height, 0]);

      var xScale = d3.scaleBand()
        .domain(pData.map(function (d) { return d.key2 }))
        .range([0, width])
        .padding(0.6);
      //xAxis
      var xAxis = svg.append("g")
        .classed("xAxis", true)
        .attr('transform', 'translate(' + padding.left + ',' + (height + padding.top) + ')')
        .call(d3.axisBottom(xScale))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-15)")
        .style("text-anchor", "end") //for getting the text in end
        .attr("fill", "black");

      //yAxis

      var yAxis = svg.append("g")
        .classed("yAxis", true)
        .attr('transform', 'translate(' + padding.left + ',' + padding.top + ')')
        .call(d3.axisLeft(yScale));

      //GRID
      // svg.append('g')
      //   .attr('class', 'grid')
      //   .attr('transform', 'translate(' + padding.left + ',' + (height + padding.top) + ')')
      //   .call(d3.axisBottom()
      //     .scale(xScale)
      //     .tickSize(-height, 0, 0)
      //     .tickFormat(''))

      // svg.append('g')
      //   .attr('class', 'grid')
      //   .attr('transform', 'translate(' + padding.left + ',' + padding.top + ')')
      //   .call(d3.axisLeft()
      //     .scale(yScale)
      //     .tickSize(-width, 0, 0)
      //     .tickFormat(''))

      //LEGEND
      svg.append('text')
        .attr('x', -(height / 2) - 20)
        .attr('y', 60 / 19)
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .attr('class', 'text')
        .style('fill', 'blueViolet')
        .text('Progress (%)')

      svg.append('text')
        .attr('x', width / 2 + 60)
        .attr('y', 282)
        .attr('text-anchor', 'middle')
        .attr('class', 'text')
        .style('fill', 'blueViolet')
        .text('Tickets');

      var rectGrp = svg.append("g").attr('transform', 'translate(' + padding.left + ',' + padding.top + ')');

      rectGrp.selectAll("rect").data(pData).enter()
        .append("rect")
        .attr("stroke", "black")
        .on("mousemove", onMouseMove)
        .on("mouseout", onMouseOut)
        .on("mouseover", function () { tooltip.style("display", null); })
        .attr("width", xScale.bandwidth())
        .transition()
        .ease(d3.easeLinear)
        .duration(300)
        // .delay(function (d, i) {
        //   return i * 5;// for the movement of bar
        // })
        .attr("height", function (d, i) {
          return height - yScale(d.value2);
        })
        .attr("x", function (d, i) {
          return xScale(d.key2);
        })
        .attr("y", function (d, i) {
          return yScale(d.value2);
        })
        .attr("fill", function (d, i) {
          if (d.statusId2 == 1) {
            return '#4363d8';
          }
          else if (d.statusId2 == 2) {
            return '#800000';
          }
          else if (d.statusId2 == 3) {
            return '#911eb4';
          }
          else if (d.statusId2 == 4) {
            return '#f58231';
          }
          else if (d.statusId2 == 5) {
            return '#f032e6';
          }
          else if (d.statusId2 == 6) {
            return '#008080';
          }
        });
      function onMouseMove(d, i) {

        d3.select(this).attr('class', 'highlight');
        d3.select(this)
          .transition()
          .duration(400)
          .attr('width', xScale.bandwidth() + 1)//for increase the width
          .attr("y", function (d) { return yScale(d.value2) - 10; })
          .attr("height", function (d) { return height - yScale(d.value2) + 10 });

        rectGrp.append("text")
          .attr('class', 'val') // add class to text label
          .attr('x', function () {
            return xScale(d.key2);
          })
          .attr('y', function () {
            return yScale(d.value2) - 15;
          })
          .text(function () {
            return [d.value2 + ' %'];  // Value of the text
          });

          tooltip
          .style("left", d3.event.pageX - 100 + "px")
          .style("top", d3.event.pageY -300 + "px")
          // .style("left", (d3.mouse(this)[0])-50 + "px")
          // .style("top", (d3.mouse(this)[1]) + "px")
          .style("display", "inline-block")
          .style("background-color", "pink")
          .html(function(){
            if(d.status2 == 'IN PROGRESS'||d.status2 == 'BACK OFFICE'){
            return "<b>Ticket Name : </b>"+(d.key2)+"<br>"+"<b>Status : </b>"+(d.status2)+"<br>"+"<b>Customer : </b>"+(d.customer2)+"<br>"+"<b>Category : </b>"+(d.category2) +"<br>"+"<b>SubCategory : </b>"+(d.subcategory2) +"<br>" + "<b>Start Date :</b> " + (d.starttime2)+"<br>" + "<b>End Date : </b>" + (d.endtime2)+"<br>"+ "<b>ETA : </b>" + (d.etatime2);
            }
            else if (d.status2 == 'ASSIGN')
            {
              return "<b>User Name : </b>"+ (d.username2)+"<br>"+"<b>Ticket Name : </b>"+(d.key2)+"<br>"+"<b>Status : </b>"+(d.status2)+"<br>"+"<b>Customer : </b>"+(d.customer2)+"<br>"+"<b>Category : </b>"+(d.category2) +"<br>"+"<b>SubCategory : </b>"+(d.subcategory2) +"<br>" + "<b>Start Date :</b> " + (d.starttime2)+"<br>" + "<b>End Date : </b>" + (d.endtime2)+"<br>"+"<b>ETA : </b>" + (d.etatime2);
            }
            });
    
      }
      function onMouseOut(d, i) {
        d3.select(this).attr('class', 'bar');
        d3.select(this)
          .transition()
          .duration(400)
          .attr('width', xScale.bandwidth())
          .attr("y", function (d) { return yScale(d.value2); })
          .attr("height", function (d) { return height - yScale(d.value2); });

        d3.selectAll('.val')
          .remove();
          tooltip.style("display", "none");
      }
    }
    ////////////////////////////////////////////////////////code for refresh the chart///////////////////////////
    $(document).ready(function () {
      $("select").click(function () {
        $("#pie-chart").empty();
        $("#bar-chart").empty();

      });
    });

    $(document).ready(function () {
      $("#pie-chart").click(function () {
        $("#bar-chart").empty();

      });
    });

  });
