'use strict';

angular.module('secondarySalesApp')
    .controller('UpdateTypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
//        if ($window.sessionStorage.roleId != 1) {
//            window.location = "/";
//        }

        $scope.showForm = function () {
            var visible = $location.path() === '/updatetype/create' || $location.path() === '/updatetype/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/updatetype/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/updatetype/create' || $location.path() === '/updatetype/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/updatetype/create' || $location.path() === '/updatetype/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/updatetype") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.updatetypes = zn;
            angular.forEach($scope.updatetypes, function (member, index) {
                member.index = index + 1;
            });
        });

        if ($routeParams.id) {
            Restangular.one('updatetypes', $routeParams.id).get().then(function (updtetp) {
              $scope.original = updtetp;

              $scope.updatetype = Restangular.copy($scope.original);
      
                 
            });
          };
        
        

        $scope.updatetype = {
            deleteflag: false,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            console.log('i m inside save');
            document.getElementById('updatetypename').style.border = "";
            document.getElementById('updatetypeorder').style.border = "";
      
            if ($scope.updatetype.name == '' || $scope.updatetype.name == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Update Type';
              document.getElementById('updatetypename').style.border = "1px solid #ff0000";
      
            } else if ($scope.updatetype.order == '' || $scope.updatetype.order == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Order';
              document.getElementById('updatetypeorder').style.border = "1px solid #ff0000";
      
            } 
      
            if ($scope.validatestring != '') {
              $scope.toggleValidation();
              $scope.validatestring1 = $scope.validatestring;
              $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
              Restangular.all('updatetypes').post($scope.updatetype).then(function (conResponse) {
                console.log('conResponse', conResponse);
                $scope.message = 'Update Type has been created!';
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                
                setTimeout(function () {
                  window.location = '/updatetype';
                  // window.location = '/workflowlanguage';
                }, 350);
              });
            };
      
          };

          $scope.Update = function () {
            console.log('i m inside update');
            document.getElementById('updatetypename').style.border = "";
            document.getElementById('updatetypeorder').style.border = "";
      
            if ($scope.updatetype.name == '' || $scope.updatetype.name == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Update Type';
              document.getElementById('updatetypename').style.border = "1px solid #ff0000";
      
            } else if ($scope.updatetype.order == '' || $scope.updatetype.order == null) {
              $scope.validatestring = $scope.validatestring + 'Please  Enter Order';
              document.getElementById('updatetypeorder').style.border = "1px solid #ff0000";
      
            } 
      
            if ($scope.validatestring != '') {
              $scope.toggleValidation();
              $scope.validatestring1 = $scope.validatestring;
              $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
              Restangular.all('updatetypes/' + $routeParams.id).customPUT($scope.updatetype).then(function (conResponse) {
                console.log('conResponse', conResponse);
                $scope.message = 'Update Type has been updated!';
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                
                setTimeout(function () {
                  window.location = '/updatetype';
                  // window.location = '/workflowlanguage';
                }, 350);
              });
            };
      
          };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        


        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('updatetypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
