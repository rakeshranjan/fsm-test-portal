'use strict';

angular.module('secondarySalesApp')
    .controller('FieldrequestCreateCtrl', function ($scope, $filter, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
            $scope.customers = cust;
        });

        Restangular.all('states').getList().then(function (ste) {
            $scope.states = ste;
        });

        Restangular.all('payterms').getList().then(function (pterm) {
            $scope.payterms = pterm;
        });

        $scope.purchaseorder = {
            todepotdist: '',
            flag: true,
            receivedflag: false,
            transferflag: false,
            discount: 0,
            tax: 0,
            netamount: 0,
            login: $window.sessionStorage.userName
        };

        $scope.$watch('purchaseorder.customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });
                Restangular.all('warehouses?filter[where][categoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 2).getList().then(function (lwh) {
                    $scope.warehouselwh = lwh;
                });

                Restangular.all('warehouses?filter[where][categoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 1).getList().then(function (mwh) {
                    $scope.warehousemwh = mwh;
                });
            }
        });

        $scope.$watch('purchaseorder.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });
                Restangular.all('warehouses?filter[where][categoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 2).getList().then(function (lwh) {
                    if (lwh.length > 0) {
                        $scope.warehouselwh = lwh;
                    }
                });

                Restangular.all('warehouses?filter[where][categoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 1).getList().then(function (mwh) {
                    if (mwh.length > 0) {
                        $scope.warehousemwh = mwh;
                    }
                });
            }
        });

        $scope.$watch('purchaseorder.subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                // var splitArray = $window.sessionStorage.orgStructure.split(';');
                // var splitArray1 = splitArray[splitArray.length - 1];
                // var splitArray2 = splitArray1.split('-');
                // var splitArray2 = splitArray2[splitArray2.length - 1];
                // Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
                //     Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][site][neq]=0' + '&filter[where][id]=' + whs[0].id).getList().then(function (lwh) {
                //         $scope.warehouselwh = lwh;
                //     });
                // });

                Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 2).getList().then(function (lwh) {
                    if (lwh.length > 0) {
                        $scope.warehouselwh = lwh;
                    }
                });

                Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][warehousetypeId]=' + 1).getList().then(function (mwh) {
                    if (mwh.length > 0) {
                        $scope.warehousemwh = mwh;
                    }
                });

                $scope.itemdefinitions = Restangular.all('itemdefinitions?filter[where][subcategoryId]=' + newValue).getList().$object;

                Restangular.all('poheaders').getList().then(function (poheader) {
                    var ponumber = poheader.length + 1;
                    $scope.purchaseorder.ponumber = 'PO00' + ponumber;
                });
            }
        });

        $scope.$watch('purchaseorder.state', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('cities?filter[where][stateId]=' + newValue).getList().then(function (cty) {
                    $scope.cities = cty;
                });

            }
        });

        $scope.$watch('purchaseorder.todepotdist', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.one('warehouses', newValue).get().then(function (warehouse) {
                    $scope.purchaseorder.toaddress = warehouse.address;
                    $scope.purchaseorder.city = warehouse.cityId;
                    $scope.purchaseorder.pin = warehouse.zip;

                    Restangular.all('states?filter[where][id]=' + warehouse.stateId).getList().then(function (res) {
                        $scope.states = res;
                        $scope.purchaseorder.state = res[0].id;
                    });
                });
            }
        });

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = new Date();
            $scope.purchaseorder.deliverydate = $scope.dt;
            $scope.purchaseorder.podate = $filter('date')($scope.dt, 'd/M/yyyy');
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end

        /*** logics ***/

        $scope.poTrailersArray = [{
            itemcode: ''
        }]

        $scope.lowChanged = function (scope, index) {
            // auto filling
            $scope.poTrailersArray[index].totalamount = (parseInt($scope.poTrailersArray[index].quantity) * parseInt($scope.poTrailersArray[index].sellprice));
            $scope.totalPrice();
            // $scope.purchaseorder.totalamount  = parseInt($scope.poTrailersArray[index].totalamount)+parseInt($scope.purchaseorder.totalamount);
        };

        $scope.totalPrice = function (value) {
            var total = 0;
            for (var count = 0; count < $scope.poTrailersArray.length; count++) {
                total += $scope.poTrailersArray[count].sellprice * $scope.poTrailersArray[count].quantity;
            }
            $scope.purchaseorder.totalamount = total;
            return total;
        }

        $scope.addRouteLinkItem = function () {
            $scope.poTrailersArray.push({
                'poheaderId': ''
            });
        };

        $scope.removeRouteLinkItem = function (index) {
            var item = $scope.poTrailersArray[index];
            $scope.purchaseorder.totalamount = parseInt($scope.purchaseorder.totalamount) - parseInt($scope.poTrailersArray[index].totalamount);
            $scope.poTrailersArray.splice(index, 1);
        };


        $scope.removeItem = function (index) {
            $scope.poTrailersArray.splice(index, 1);
            $scope.totalPrice();

        };

        $scope.getprice = function (id, index) {
            Restangular.one('itemdefinitions', id).get().then(function (ress) {
                $scope.poTrailersArray[index].sellprice = ress.unitprice;
            });
        };

        $scope.total = function () {
            //  console.log($scope.total);
            $scope.purchaseorder.totalamount = $scope.purchaseorder.sellprice * $scope.purchaseorder.quantity;
            $scope.purchaseorder.netamount = $scope.purchaseorder.totalamount;
            //return local time string
        };

        /*** save function ***/

        $scope.Save = function () {
            Restangular.all('poheaders').post($scope.purchaseorder).then(function (res) {
                $scope.saveCount = 0;
                $scope.saveItems(res.id);
            });
        };

        $scope.saveCount = 0;

        $scope.saveItems = function (headerId) {
            if ($scope.saveCount < $scope.poTrailersArray.length) {
                $scope.poTrailersArray[$scope.saveCount].ponumber = $scope.purchaseorder.ponumber;
                $scope.poTrailersArray[$scope.saveCount].poheaderId = headerId;
                $scope.poTrailersArray[$scope.saveCount].customerId = $scope.purchaseorder.customerId;
                $scope.poTrailersArray[$scope.saveCount].categoryId = $scope.purchaseorder.categoryId;
                $scope.poTrailersArray[$scope.saveCount].subcategoryId = $scope.purchaseorder.subcategoryId;

                Restangular.all('potrailers').post($scope.poTrailersArray[$scope.saveCount]).then(function (resp) {
                    $scope.saveCount++;
                    $scope.saveItems(headerId);
                });

            } else {
                window.location = '/fieldrequest';
            }
        };

        $scope.showViewForm = false;

        if ($routeParams.id) {
            $scope.showViewForm = true;

            Restangular.all('poheaders?filter[where][id]=' + $routeParams.id).getList().then(function (po) {
                $scope.poheaders = po;
                angular.forEach($scope.poheaders, function (member, index) {
                    member.index = index + 1;
                });
            });

            Restangular.all('potrailers?filter[where][poheaderId]=' + $routeParams.id).getList().then(function (potrailer) {
                $scope.potrailers = potrailer;
                angular.forEach($scope.potrailers, function (member, index) {
                    member.index = index + 1;
                });
            });
        }

        $scope.getWareHouse = function (warehousetypeId) {
            return Restangular.one('warehouses', warehousetypeId).get().$object;
        };

        $scope.getItem = function (itemid) {
            return Restangular.one('itemdefinitions', itemid).get().$object;
        };
    });