'use strict';

angular.module('secondarySalesApp')
  .controller('SubCategoryCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
    /*********/

    $scope.showForm = function () {
      var visible = $location.path() === '/subcategory/create' || $location.path() === '/subcategory/edit/' + $routeParams.id;
      return visible;
    };
    $scope.isCreateView = function () {
      if ($scope.showForm()) {
        var visible = $location.path() === '/subcategory/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function () {
      var visible = $location.path() === '/subcategory/create' || $location.path() === '/subcategory/edit/' + $routeParams.id;
      return visible;
    };
    $scope.hideSearchFilter = function () {
      var visible = $location.path() === '/subcategory/create' || $location.path() === '/subcategory/edit/' + $routeParams.id;
      return visible;
    };

    if ($location.path() === '/subcategory/create') {
      $scope.disableDropdown = false;
    } else {
      $scope.disableDropdown = true;
    }
    /************************************************************************************/
    $scope.subcategory = {
      //organizationId: $window.sessionStorage.organizationId,
      deleteFlag: false
    };
    // if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
    //     $window.sessionStorage.facility_zoneId = null;
    //     $window.sessionStorage.facility_stateId = null;
    //     $window.sessionStorage.facility_currentPage = 1;
    //     $window.sessionStorage.facility_currentPageSize = 25;
    // } else {
    //     $scope.countryId = $window.sessionStorage.facility_zoneId;
    //     $scope.stateId = $window.sessionStorage.facility_stateId;
    //     $scope.currentpage = $window.sessionStorage.facility_currentPage;
    //     $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    // }


    // if ($window.sessionStorage.prviousLocation != "partials/onetoonetype") {
    //     $window.sessionStorage.facility_zoneId = '';
    //     $window.sessionStorage.facility_stateId = '';
    //     $window.sessionStorage.facility_currentPage = 1;
    //     $scope.currentpage = 1;
    //     $window.sessionStorage.facility_currentPageSize = 25;
    //     $scope.pageSize = 25;
    // }

    // $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    // $scope.pageFunction = function (mypage) {
    //     console.log('mypage', mypage);
    //     $scope.pageSize = mypage;
    //     $window.sessionStorage.facility_currentPageSize = mypage;
    // };

    // $scope.currentpage = $window.sessionStorage.facility_currentPage;
    // $scope.PageChanged = function (newPage, oldPage) {
    //     $scope.currentpage = newPage;
    //     $window.sessionStorage.facility_currentPage = newPage;
    // };

    /************************************************/

    // $scope.DisplayOneToOneTypes = [{
    //     name: '',
    //     disableAdd: false,
    //     disableRemove: false,
    //     deleteflag: false,
    //     lastmodifiedby: $window.sessionStorage.userId,
    //     lastmodifiedrole: $window.sessionStorage.roleId,
    //     lastmodifiedtime: new Date(),
    //     createdby: $window.sessionStorage.userId,
    //     createdtime: new Date(),
    //     createdrole: $window.sessionStorage.roleId
    // }];

    // $scope.Add = function (index) {

    //     var idVal = index + 1;

    //         $scope.DisplayOneToOneTypes.push({
    //             name: '',
    //             disableAdd: false,
    //             disableRemove: false,
    //             deleteflag: false,
    //             lastmodifiedby: $window.sessionStorage.userId,
    //             lastmodifiedrole: $window.sessionStorage.roleId,
    //             lastmodifiedtime: new Date(),
    //             createdby: $window.sessionStorage.userId,
    //             createdtime: new Date(),
    //             createdrole: $window.sessionStorage.roleId
    //         });
    // };

    // $scope.Remove = function (index) {
    //     var indexVal = index - 1;
    //     $scope.DisplayOneToOneTypes.splice(indexVal, 1);
    // };

    // $scope.levelChange = function (index, name) {
    //     if (name == '' || name == null) {
    //         $scope.DisplayOneToOneTypes[index].disableAdd = false;
    //         $scope.DisplayOneToOneTypes[index].disableRemove = false;
    //     } else {
    //         $scope.DisplayOneToOneTypes[index].disableAdd = true;
    //         $scope.DisplayOneToOneTypes[index].disableRemove = true;
    //     }
    // };

    /*************************************** DELETE *******************************/

    // $scope.Delete = function (id) {
    //     $scope.item = [{
    //         deleteflag: true
    //     }]
    //     Restangular.one('onetoonetypes/' + id).customPUT($scope.item[0]).then(function () {
    //         $route.reload();
    //     });
    // }
    /********************************************** WATCH ***************************************/

    // Restangular.all('onetoonetypes?filter[where][deleteflag]=false').getList().then(function (ty) {
    //     $scope.onetoonetypes = ty;
    //     angular.forEach($scope.onetoonetypes, function (member, index) {
    //         member.index = index + 1;

    //         $scope.TotalTodos = [];
    //         $scope.TotalTodos.push(member);
    //     });
    // });

    var customerUrl = '';

    if ($window.sessionStorage.roleId == 2) {
      customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
    } else {
      customerUrl = 'customers?filter[where][deleteflag]=false';
    }

    Restangular.all(customerUrl).getList().then(function (cust) {
      $scope.customers = cust;
    });

    $scope.$watch('subcategory.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });

        Restangular.all('users?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[where][roleId]=2').getList().then(function (user) {
          $scope.usersList = user;
        });
      }
    });

    Restangular.all('ticketcategories?filter[where][deleteflag]=false').getList().then(function (tktcatg) {
      $scope.ticketcategories = tktcatg;
    });

    $scope.$watch('subcategory.worktypename', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        $scope.myVar = '';
        Restangular.one('ticketcategories/findOne?filter[where][deleteflag]=false&filter[where][id]=' + newValue).get().then(function (ticketcatg) {
          $scope.tktcode = ticketcatg.ticketcode;
          //console.log("$scope.tktcode-164", $scope.tktcode)
          if ($scope.tktcode == 'T') {
            //console.log("$scope.tktcode-166", $scope.tktcode)
            $scope.disableControl = true;
          } else {
            $scope.disableControl = false;
          }
        });
      }
    });

    $scope.getselectedvalue = function (defaultvalue) {
      //console.log("here-176", $scope.myVar)
      console.log("here-178", defaultvalue)
      $scope.selectedvalue = defaultvalue;
      // var inputVal = document.getElementById('task').value;
      // var inputVal1 = document.getElementById('meeting').value;
      // var inputVal2 = document.getElementById('reminder').value;
      // console.log("inputVal-178", inputVal, inputVal1, inputVal2)

    };

    /****************************************** CREATE *********************************/
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
      $scope.showValidation = !$scope.showValidation;
    };
    $scope.validatestring = '';
    $scope.submitDisable = false;

    $scope.savesubcategory = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.category, "message")
      //console.log("$scope.selectedvalue-198", $scope.selectedvalue)
      if ($scope.subcategory.name == '' || $scope.subcategory.name == null) {
        $scope.subcategory.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Subcategory';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.subcategory.customerId == '' || $scope.subcategory.customerId == null) {
        $scope.subcategory.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      } else if ($scope.subcategory.categoryId == '' || $scope.subcategory.categoryId == null) {
        $scope.subcategory.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Category';

      }
      //  else if ($scope.subcategory.worktype == '' || $scope.subcategory.worktype == null) {
      //   $scope.subcategory.worktype = null;
      //   $scope.validatestring = $scope.validatestring + 'Please  Select Work Type';
      //  }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.subcategory.worktype = $scope.tktcode;
        $scope.subcategory.defaultFlag = $scope.selectedvalue;
        //console.log("$scope.subcategory.worktype", $scope.subcategory.worktype)
        $scope.message = 'Subcategory has been created!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        Restangular.all('subcategories').post($scope.subcategory).then(function () {
          window.location = '/subcategory-list';
        });
      }
    };

    /***************************** UPDATE *******************************************/

    // $scope.Update = function (clicked) {

    //     if ($scope.onetoonetype.name == '' || $scope.onetoonetype.name == null) {
    //         $scope.validatestring = $scope.validatestring + 'Please Enter Question Type';
    //         document.getElementById('name').style.borderColor = "#FF0000";
    //     }
    //     if ($scope.validatestring != '') {
    //         $scope.toggleValidation();
    //         $scope.validatestring1 = $scope.validatestring;
    //         $scope.validatestring = '';
    //         //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    //     } else {

    //         $scope.onetoonetype.lastmodifiedby = $window.sessionStorage.userId;
    //         $scope.onetoonetype.lastmodifiedtime = new Date();
    //         $scope.onetoonetype.lastmodifiedrole = $window.sessionStorage.roleId;

    //         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
    //         $scope.submitDisable = true;

    //         Restangular.one('onetoonetypes', $routeParams.id).customPUT($scope.onetoonetype).then(function (resp) {
    //             window.location = '/onetoone-type-list';
    //         });
    //     }
    // };

    // Restangular.one('customers', $routeParams.id).customPUT($scope.customer).then(function (resp) {
    //   window.location = '/customers-list';
    // });
    //}
    // };

    // $scope.showValidation = false;
    // $scope.toggleValidation = function () {
    //     $scope.showValidation = !$scope.showValidation;
    // };

    // if ($routeParams.id) {
    //     $scope.message = 'Type has been Updated!';
    //     $scope.statecodeDisable = true;
    //     $scope.membercountDisable = true;
    //     Restangular.one('onetoonetypes', $routeParams.id).get().then(function (onetoonetype) {
    //         $scope.original = onetoonetype;
    //         $scope.onetoonetype = Restangular.copy($scope.original);
    //     });
    // } else {
    //     $scope.message = 'Type has been created!';
    // }

    // $scope.showValidation = false;
    // $scope.toggleValidation = function () {
    //   $scope.showValidation = !$scope.showValidation;
    // };

    // if ($routeParams.id) {
    //   $scope.message = 'Customer has been Updated!';
    //   $scope.statecodeDisable = true;
    //   $scope.membercountDisable = true;
    //   Restangular.one('customers', $routeParams.id).get().then(function (custmers) {
    //     $scope.original = custmers;
    //     $scope.onetoonetype = Restangular.copy($scope.original);
    //   });
    // } else {
    //   $scope.message = 'Customer has been created!';
    // }

    $scope.validatestring = '';
    $scope.updatecount = 0;
    $scope.Updatesubcategory = function () {
      document.getElementById('name').style.border = "";

      document.getElementById('name').style.border = "";
      //console.log($scope.category, "message")
      if ($scope.subcategory.name == '' || $scope.subcategory.name == null) {
        $scope.subcategory.name = null;
        $scope.validatestring = $scope.validatestring + 'Please  Enter Subcategory';
        document.getElementById('name').style.border = "1px solid #ff0000";

      } else if ($scope.subcategory.customerId == '' || $scope.subcategory.customerId == null) {
        $scope.subcategory.customerId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

      } else if ($scope.subcategory.categoryId == '' || $scope.subcategory.categoryId == null) {
        $scope.subcategory.categoryId = null;
        $scope.validatestring = $scope.validatestring + 'Please  Select Category';

      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
      }
      else {
        $scope.message = 'Subcategory has been updated!';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.neWsubmitDisable = true;
        Restangular.one('subcategories/' + $routeParams.id).customPUT($scope.subcategory).then(function () {
          window.location = '/subcategory-list';

        });
      }
    };

    if ($routeParams.id) {
      Restangular.one('subcategories', $routeParams.id).get().then(function (subcatg) {
        $scope.original = subcatg;
        $scope.subcategory = Restangular.copy($scope.original);
      });
    }

  });
