'use strict';
angular.module('secondarySalesApp').controller('FWCreateCtrl', function ($scope, Restangular, $window, $filter, $modal, AnalyticsRestangular) {
    if ($window.sessionStorage.roleId != 5) {
        window.location = "/";
    }
    $scope.isCreateView = true;
    $scope.heading = 'Field Worker Create';
    $scope.HideUsername = false;
    $scope.UpdateClicked = false;
    $scope.hideSitesAssign = true;
    $scope.toggleLoading = function () {
        $scope.modalInstanceLoad = $modal.open({
            animation: true
            , templateUrl: 'template/LodingModal.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'sm'
        });
    };
    $scope.fieldworker = {
        address: ''
        , lastmodifiedtime: new Date()
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , facility: $window.sessionStorage.coorgId
        , deleteflag: false
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId
        , modifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , entityroleid: 51
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , facility: $window.sessionStorage.coorgId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    $scope.user = {
        flag: true
        , lastmodifiedtime: new Date()
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , deleteflag: false
        , roleId: 6
        , status: 'active'
        , salesAreaId: $window.sessionStorage.salesAreaId
        , coorgId: $window.sessionStorage.coorgId
        , zoneId: $window.sessionStorage.zoneId
    };
    /******************************* Not In Use *************************

    		$scope.employees = Restangular.all('employees').getList().$object;
    		$scope.partners = Restangular.all('partners').getList().$object;
    		$scope.submitpartners = Restangular.all('fieldworkers').getList().$object;
    		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
    		$scope.retailers = Restangular.all('retailers').getList().$object;
    		$scope.zones = Restangular.all('zones').getList().$object;
    		$scope.submitusers = Restangular.all('users').getList().$object;

    		$scope.groups = Restangular.all('groups').getList().$object;
    		$scope.states = Restangular.all('states').getList().$object;
    		$scope.retailercategories = Restangular.all('retailercategories').getList().$object;


    		// $scope.partner = {};
    		$scope.groups = Restangular.one('groups', 9).get().then(function (group) {
    			$scope.groupname = group.name;
    			$scope.fieldworker.groupId = group.id;
    		});

    ********************************************************************/
    Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
        $scope.zoneName = zone.name;
        $scope.fieldworker.state = zone.id;
    });
    Restangular.one('sales-areas', $window.sessionStorage.salesAreaId).get().then(function (salesarea) {
        $scope.salesareaName = salesarea.name;
        $scope.fieldworker.district = salesarea.id;
    });
    //$scope.getSalescode = emp.salesCode;
    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
        $scope.coorgName = comember.name;
        $scope.fieldworker.facilityId = comember.id;
        $scope.auditlog.facilityId = comember.id;
        Restangular.one('fieldworkers?filter[where][facility]=' + $window.sessionStorage.coorgId).get().then(function (fldwrkrs) {
            Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (facility) {
                $scope.fieldworker.firstname = 'fw' + facility.salesCode.toLowerCase() + (fldwrkrs.length + 1);
                $scope.fieldworker.fwcode = (fldwrkrs.length + 1);
                $scope.modalInstanceLoad.close();
            });
        });
    });
    /************************************ SAVE *******************************************/
    $scope.validatestring = '';
    $scope.UpdateComember = {
        fwcount: 0
    };
    $scope.createDisabled = false;
    $scope.dataModal = false;
    $scope.Save = function () {
        if ($scope.fieldworker.firstname == '' || $scope.fieldworker.firstname == null) {
            //$scope.fieldworker.firstname = null;
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
            document.getElementById('firstname').style.border = "1px solid #ff0000";
        }
        else if ($scope.fieldworker.lastname == '' || $scope.fieldworker.lastname == null) {
            //$scope.fieldworker.lastname = null;
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
            //document.getElementById('lastname').style.border = "1px solid #ff0000";
        }
        else if ($scope.fieldworker.mobile == '' || $scope.fieldworker.mobile == null) {
            //$scope.fieldworker.mobile = null;
            $scope.validatestring = $scope.validatestring + $scope.entermobile;
            //document.getElementById('mobile').style.border = "1px solid #ff0000";
        }
        else if ($scope.fieldworker.email == '' || $scope.fieldworker.email == null) {
            //$scope.fieldworker.mobile = null;
            $scope.validatestring = $scope.validatestring + $scope.enteremail;
            //document.getElementById('mobile').style.border = "1px solid #ff0000";
        }
        else if ($scope.user.password == '' || $scope.user.password == null) {
            //$scope.fieldworker.mobile = null;
            $scope.validatestring = $scope.validatestring + $scope.enterpassword;
            //document.getElementById('mobile').style.border = "1px solid #ff0000";
        }
        else if ($scope.fieldworker.language == '' || $scope.fieldworker.language == null) {
            //$scope.fieldworker.mobile = null;
            $scope.validatestring = $scope.validatestring +$scope.sellanguage;
            //document.getElementById('mobile').style.border = "1px solid #ff0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //alert($scope.validatestring);
            //$scope.validatestring = '';
            //$scope.dataModal = !$scope.dataModal;
        }
        else {
            $scope.UpdateClicked = true;
            $scope.createDisabled = true;
            //$scope.submitpartners.post($scope.fieldworker).then(function (submitfw) {
            Restangular.all('fieldworkers').post($scope.fieldworker).then(function (submitfw) {
                $scope.auditlog.entityid = submitfw.id;
                $scope.user.username = submitfw.firstname;
                $scope.user.mobile = submitfw.mobile;
                $scope.user.employeeid = submitfw.id;
                $scope.user.email = submitfw.email;
                $scope.user.language = submitfw.language;
                var respdate = $filter('date')(submitfw.lastmodifiedtime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Field Worker Created With Following Details: ' + 'User Name - ' + submitfw.firstname + ' , ' + 'Full Name - ' + submitfw.lastname + ' , ' + 'FW Code - ' + submitfw.fwcode + ' , ' + 'Email - ' + submitfw.email + ' , ' + 'Mobile - ' + submitfw.mobile + 'Address - ' + submitfw.address + ' , ' + 'latitude - ' + submitfw.latitude + ' , ' + 'Language - ' + submitfw.language + ' , ' + 'Last Modied - ' + respdate;
                //$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    //$scope.submitusers.post($scope.user).then(function (responseuser) {
                    Restangular.all('users').post($scope.user).then(function (responseuser) {
                        $scope.user.id = responseuser.id;
                        $scope.user.roleid = responseuser.roleId;
                        $scope.user.coorg_id = responseuser.coorgId;
                        $scope.user.employee_id = responseuser.employeeid;
                        $scope.user.zone_id = responseuser.zoneId;
                        $scope.user.salesarea_id = responseuser.salesAreaId;
                        AnalyticsRestangular.all('users').post($scope.user).then(function (analyticsuser) {
                            $scope.UpdateComember.fwcount = +($scope.fieldworker.fwcode) + 1;
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.UpdateComember).then(function (comember) {
                                //window.location = '/fieldworkers';
                                $scope.dataModal = !$scope.dataModal;
                            });
                        });
                        Restangular.one('languages', submitfw.language).get().then(function (lang) {
                            $scope.languageName = lang.name;
                        });
                    }, function (error) {
                        console.log('user error', error);
                    });
                });
            }, function (error) {
                $scope.UpdateClicked = false;
                console.log('fw error', error);
                if (error.data.error.constraint == 'unique_firstname') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.usernameexits;//"Username already exits please refresh the page and try again";
                }
                else if (error.data.error.constraint == 'unique_email') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.emailexit;//"email id already exits";
                }
            });
        }
        $scope.OK = function () {
            $scope.dataModal = !$scope.dataModal;
            console.log('Save');
            window.location = '/fieldworker';
        };
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /*
    		$scope.$watch('partner.stateId', function (newValue, oldValue) {
    			if (newValue === oldValue || newValue == '') {
    				return;
    			} else {
    				$scope.cities = Restangular.all('cities?filter[where][stateId]=' + newValue).getList().$object;
    			}
    		});


    		$scope.$watch('partner.salesManagerId', function (newValue, oldValue) {
    			$scope.distributionRoutes = Restangular.all('routelinkviews?filter[where][partnerid]=' + newValue + '&filter[where][flag]=null').getList().$object;
    		});

    		$scope.partner = {
    			address: ''
    		}
    		*/
    /********************************************* Map ******************************/
    $scope.showMapModal = false;
    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                }
                else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            $scope.fieldworker.latitude = latLng.lat() + ',' + latLng.lng();
            $scope.fieldworker.longitude = latLng.lng();
            document.getElementById('info').innerHTML = [
    latLng.lat()
    , latLng.lng()
  ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {
            $scope.address = $scope.fieldworker.address;
            // console.log('$scope.address', $scope.address);
            $scope.latitude = 21.0000;
            $scope.longitude = 78.0000;
            if ($scope.address.length > 0) {
                var addressgeocoder = new google.maps.Geocoder();
                addressgeocoder.geocode({
                    'address': $scope.address
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $scope.latitude = parseInt(results[0].geometry.location.lat());
                        $scope.longitude = parseInt(results[0].geometry.location.lng());
                        //console.log($scope.latitude, $scope.longitude);
                        var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                        map = new google.maps.Map(document.getElementById('mapCanvas'), {
                            zoom: 4
                            , center: new google.maps.LatLng($scope.latitude, $scope.longitude)
                            , mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var marker = new google.maps.Marker({
                            position: latLng
                            , title: 'Point A'
                            , map: map
                            , draggable: true
                        });
                        // Update current position info.
                        updateMarkerPosition(latLng);
                        geocodePosition(latLng);
                        // Add dragging event listeners.
                        google.maps.event.addListener(marker, 'dragstart', function () {
                            updateMarkerAddress('Dragging...');
                        });
                        google.maps.event.addListener(marker, 'drag', function () {
                            updateMarkerStatus('Dragging...');
                            updateMarkerPosition(marker.getPosition());
                        });
                        google.maps.event.addListener(marker, 'dragend', function () {
                            updateMarkerStatus('Drag ended');
                            geocodePosition(marker.getPosition());
                        });
                    }
                });
            }
            else {
                $scope.latitude = 21.0000;
                $scope.longitude = 78.0000;
                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4
                    , center: new google.maps.LatLng($scope.latitude, $scope.longitude)
                    , mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var marker = new google.maps.Marker({
                    position: latLng
                    , title: 'Point A'
                    , map: map
                    , draggable: true
                });
                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);
                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });
                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });
                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            }
        }
        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();
        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(4);
        }, 1000);
        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
            console.log($scope.reportincident);
        };
        //console.log('fdfd');
        $scope.showMapModal = !$scope.showMapModal;
    };
    $scope.CancelMap = function () {
        if ($scope.mapcount == 0) {
            $scope.showMapModal = !$scope.showMapModal;
            $scope.mapcount++;
        }
    };
    /****************************** Language *********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.fieldworkercreate = langResponse.fieldworkercreate;
        $scope.divbasicinfo = langResponse.divbasicinfo;
        $scope.divuserinfo = langResponse.divuserinfo;
        $scope.printfirstname = langResponse.firstname;
        $scope.printlastname = langResponse.lastname;
        $scope.printfwcode = langResponse.fwcode;
        $scope.printemail = langResponse.email;
        $scope.state = langResponse.state;
        $scope.district = langResponse.district;
        $scope.comanagerhdf = langResponse.comanagerhdf;
        $scope.fullname = langResponse.fullname;
        $scope.printmobile = langResponse.mobile;
        $scope.printaddress = langResponse.address;
        $scope.latlong = langResponse.latlong;
        $scope.username = langResponse.username;
        $scope.password = langResponse.password;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.ok = langResponse.ok;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        //$scope.modalTitle = langResponse.createdetails;
        
		$scope.enterfullname = langResponse.enterfullname;
		$scope.entermobile = langResponse.entermobile;
		$scope.enteremail = langResponse.enteremail;
		$scope.enterpassword = langResponse.enterpassword;
		$scope.sellanguage = langResponse.sellanguage;
		$scope.usernameexits = langResponse.usernameexits;
		$scope.emailexit = langResponse.emailexit;
		
    });
    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle12 = 'Created With the Following Details';
        $scope.LanguagePrint = 'Language';
    }
    else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle12 = 'निम्न विवरण के साथ बनाया';
        $scope.LanguagePrint = 'भाषा';
    }
    else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle12 = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        $scope.LanguagePrint = 'ಭಾಷೆ';
    }
    else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle12 = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        $scope.LanguagePrint = 'மொழி';
    }
    else if ($window.sessionStorage.language == 5) {
        $scope.modalTitle12 = 'క్రింది వివరాలను తో రూపొందించబడింది';
        $scope.LanguagePrint = 'భాష';
    }
    else if ($window.sessionStorage.language == 6) {
        $scope.modalTitle12 = 'ह्या माहिती प्रमाणे करनायत आलेळे आहे';
        $scope.LanguagePrint = 'भाषा';
    }
});