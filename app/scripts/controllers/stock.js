'use strict';

angular.module('secondarySalesApp')
	.controller('StockCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
	
      
        $scope.hidewarehouse = false;

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (usr) {


            if (usr.usertype == 'warehouse') {
                $scope.hidewarehouse = true;

            } else {
                $scope.hidewarehouse = false;

            }
        });


        $scope.getZone = function (zoneId) {
            return Restangular.one('zones', zoneId).get().$object;
        };

        $scope.getSalesArea = function (salesAreaId) {
            return Restangular.one('sales-areas', salesAreaId).get().$object;
        };

        $scope.getDistributionArea = function (distributionAreaId) {
            return Restangular.one('distribution-areas', distributionAreaId).get().$object;
        };

        $scope.getDistributionSubarea = function (distributionSubareaId) {
            return Restangular.one('distribution-subareas', distributionSubareaId).get().$object;
        };

        $scope.getwarehousetype = function (warehousetypeId) {
            return Restangular.one('warehousetypes', warehousetypeId).get().$object;
        };

        $scope.getpartner = function (partnerId) {
            return Restangular.one('partners', partnerId).get().$object;
        };

        $scope.getstate = function (state) {
            return Restangular.one('states', state).get().$object;
        };

        $scope.getcity = function (city) {
            return Restangular.one('cities', city).get().$object;
        };
        $scope.getpayterm = function (paytermId) {
            return Restangular.one('payterms', paytermId).get().$object;
        };

        $scope.getWareHouse = function (warehouseId) {
            return Restangular.one('warehouses', warehouseId).get().$object;
        };

        $scope.dn = Restangular.all('deliverynotes?filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (dn) {
            $scope.deliverynotes = dn;
            angular.forEach($scope.deliverynotes, function (member, index) {
                member.index = index + 1;
               // console.log(member);
            });
        });
    });