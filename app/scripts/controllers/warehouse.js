'use strict';
angular.module('secondarySalesApp')
  .controller('WarehouseCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    console.log("$routeParams.id-5", $routeParams.id)
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/warehouse" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    // ******************************** EOF Pagination *************************************************

    //------------------------------- Cust-Catg-Subcatg-------------------------
    if ($window.sessionStorage.roleId == 2) {
      Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
        $scope.customers = cust;
      });
    } else {
      Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });
    }

    $scope.$watch('warehouse.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;
        });

        Restangular.all('warehouses?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (whrs) {
          $scope.warehouses = whrs;
          angular.forEach($scope.warehouses, function (member, index) {
            member.index = index + 1;
  
            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('warehousetypes?filter[where][deleteflag]=false').getList().then(function (whtype) {
              $scope.warehousetypename = whtype;
              for (var j = 0; j < $scope.warehousetypename.length; j++) {
                if (member.warehousetypeId == $scope.warehousetypename[j].id) {
                  member.warehousetypename = $scope.warehousetypename[j].warehousetype;
                  break;
                }
              }
            });
            Restangular.all('states?filter[where][deleteflag]=false').getList().then(function (state) {
              $scope.statename = state;
              for (var j = 0; j < $scope.statename.length; j++) {
                if (member.stateId == $scope.statename[j].id) {
                  member.statename = $scope.statename[j].name;
                  break;
                }
              }
            });
            Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (city) {
              $scope.cityname = city;
              for (var j = 0; j < $scope.cityname.length; j++) {
                if (member.cityId == $scope.cityname[j].id) {
                  member.cityname = $scope.cityname[j].name;
                  break;
                }
              }
            });
            // $scope.TotalTodos = [];
            // $scope.TotalTodos.push(member);
  
          });
        });
      }
    });

    $scope.$watch('warehouse.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });

        Restangular.all('warehouses?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (whrs) {
          $scope.warehouses = whrs;
          angular.forEach($scope.warehouses, function (member, index) {
            member.index = index + 1;
  
            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('warehousetypes?filter[where][deleteflag]=false').getList().then(function (whtype) {
              $scope.warehousetypename = whtype;
              for (var j = 0; j < $scope.warehousetypename.length; j++) {
                if (member.warehousetypeId == $scope.warehousetypename[j].id) {
                  member.warehousetypename = $scope.warehousetypename[j].warehousetype;
                  break;
                }
              }
            });
            Restangular.all('states?filter[where][deleteflag]=false').getList().then(function (state) {
              $scope.statename = state;
              for (var j = 0; j < $scope.statename.length; j++) {
                if (member.stateId == $scope.statename[j].id) {
                  member.statename = $scope.statename[j].name;
                  break;
                }
              }
            });
            Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (city) {
              $scope.cityname = city;
              for (var j = 0; j < $scope.cityname.length; j++) {
                if (member.cityId == $scope.cityname[j].id) {
                  member.cityname = $scope.cityname[j].name;
                  break;
                }
              }
            });
            // $scope.TotalTodos = [];
            // $scope.TotalTodos.push(member);
  
          });
        });
      }
    });

    $scope.$watch('warehouse.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('warehouses?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (whrs) {
          $scope.warehouses = whrs;
          angular.forEach($scope.warehouses, function (member, index) {
            member.index = index + 1;
  
            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('warehousetypes?filter[where][deleteflag]=false').getList().then(function (whtype) {
              $scope.warehousetypename = whtype;
              for (var j = 0; j < $scope.warehousetypename.length; j++) {
                if (member.warehousetypeId == $scope.warehousetypename[j].id) {
                  member.warehousetypename = $scope.warehousetypename[j].warehousetype;
                  break;
                }
              }
            });
            Restangular.all('states?filter[where][deleteflag]=false').getList().then(function (state) {
              $scope.statename = state;
              for (var j = 0; j < $scope.statename.length; j++) {
                if (member.stateId == $scope.statename[j].id) {
                  member.statename = $scope.statename[j].name;
                  break;
                }
              }
            });
            Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (city) {
              $scope.cityname = city;
              for (var j = 0; j < $scope.cityname.length; j++) {
                if (member.cityId == $scope.cityname[j].id) {
                  member.cityname = $scope.cityname[j].name;
                  break;
                }
              }
            });
            // $scope.TotalTodos = [];
            // $scope.TotalTodos.push(member);
  
          });
        });
      }
    });

    //------------------------------- end of Cust-Catg-Subcatg-------------------------

    if ($window.sessionStorage.roleId != 2) {
      Restangular.all('warehouses?filter[where][deleteflag]=false').getList().then(function (whrs) {
        $scope.warehouses = whrs;
        angular.forEach($scope.warehouses, function (member, index) {
          member.index = index + 1;

          Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
            $scope.customername = custmor;
            for (var j = 0; j < $scope.customername.length; j++) {
              if (member.customerId == $scope.customername[j].id) {
                member.customername = $scope.customername[j].name;
                break;
              }
            }
          });
          Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
            $scope.categoryname = categ;
            for (var j = 0; j < $scope.categoryname.length; j++) {
              if (member.categoryId == $scope.categoryname[j].id) {
                member.categoryname = $scope.categoryname[j].name;
                break;
              }
            }
          });
          Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
            $scope.subcategoryname = subcateg;
            for (var j = 0; j < $scope.subcategoryname.length; j++) {
              if (member.subcategoryId == $scope.subcategoryname[j].id) {
                member.subcategoryname = $scope.subcategoryname[j].name;
                break;
              }
            }
          });
          Restangular.all('warehousetypes?filter[where][deleteflag]=false').getList().then(function (whtype) {
            $scope.warehousetypename = whtype;
            for (var j = 0; j < $scope.warehousetypename.length; j++) {
              if (member.warehousetypeId == $scope.warehousetypename[j].id) {
                member.warehousetypename = $scope.warehousetypename[j].warehousetype;
                break;
              }
            }
          });
          Restangular.all('states?filter[where][deleteflag]=false').getList().then(function (state) {
            $scope.statename = state;
            for (var j = 0; j < $scope.statename.length; j++) {
              if (member.stateId == $scope.statename[j].id) {
                member.statename = $scope.statename[j].name;
                break;
              }
            }
          });
          Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (city) {
            $scope.cityname = city;
            for (var j = 0; j < $scope.cityname.length; j++) {
              if (member.cityId == $scope.cityname[j].id) {
                member.cityname = $scope.cityname[j].name;
                break;
              }
            }
          });
          // $scope.TotalTodos = [];
          // $scope.TotalTodos.push(member);

        });
      });
    }
  });