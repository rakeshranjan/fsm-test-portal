'use strict';
angular.module('secondarySalesApp')
  .controller('ItemDefinitionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    /*********/
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/itemdefinition" || $window.sessionStorage.prviousLocation != "partials/customers") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    if ($window.sessionStorage.roleId == 2) {
      Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
        $scope.customers = cust;
      });
    } else {
      Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });
    }

    // ******************************** EOF Pagination *************************************************


    $scope.$watch('customerId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (cate) {
          $scope.categories = cate;
        });

        Restangular.all('itemdefinitions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (itemdef) {
          $scope.itemdefinitions = itemdef;
          angular.forEach($scope.itemdefinitions, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
              $scope.itemcategoryname = itmcateg;
              for (var j = 0; j < $scope.itemcategoryname.length; j++) {
                if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                  member.itemcategoryname = $scope.itemcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtyp) {
              $scope.itemtypename = itmtyp;
              for (var j = 0; j < $scope.itemtypename.length; j++) {
                if (member.itemtypeId == $scope.itemtypename[j].id) {
                  member.itemtypename = $scope.itemtypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('unitmeasurements?filter[where][deleteflag]=false').getList().then(function (unitmeasure) {
              $scope.unitmeasurementname = unitmeasure;
              for (var j = 0; j < $scope.unitmeasurementname.length; j++) {
                if (member.unitmeasurementId == $scope.unitmeasurementname[j].id) {
                  member.unitmeasurementname = $scope.unitmeasurementname[j].unit;
                  break;
                }
              }
            });
            // $scope.TotalTodos = [];
            // $scope.TotalTodos.push(member);

          });

        });
      }
    });

    $scope.$watch('categoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (cate) {
          $scope.subcategories = cate;
        });

        Restangular.all('itemdefinitions?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (itemdef) {
          $scope.itemdefinitions = itemdef;
          angular.forEach($scope.itemdefinitions, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
              $scope.itemcategoryname = itmcateg;
              for (var j = 0; j < $scope.itemcategoryname.length; j++) {
                if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                  member.itemcategoryname = $scope.itemcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtyp) {
              $scope.itemtypename = itmtyp;
              for (var j = 0; j < $scope.itemtypename.length; j++) {
                if (member.itemtypeId == $scope.itemtypename[j].id) {
                  member.itemtypename = $scope.itemtypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('unitmeasurements?filter[where][deleteflag]=false').getList().then(function (unitmeasure) {
              $scope.unitmeasurementname = unitmeasure;
              for (var j = 0; j < $scope.unitmeasurementname.length; j++) {
                if (member.unitmeasurementId == $scope.unitmeasurementname[j].id) {
                  member.unitmeasurementname = $scope.unitmeasurementname[j].unit;
                  break;
                }
              }
            });
          });
        });
      }
    });

    $scope.$watch('subcategoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('itemcategories?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (cate) {
          $scope.itemcategories = cate;
        });

        Restangular.all('itemdefinitions?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (itemdef) {
          $scope.itemdefinitions = itemdef;
          angular.forEach($scope.itemdefinitions, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
              $scope.itemcategoryname = itmcateg;
              for (var j = 0; j < $scope.itemcategoryname.length; j++) {
                if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                  member.itemcategoryname = $scope.itemcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtyp) {
              $scope.itemtypename = itmtyp;
              for (var j = 0; j < $scope.itemtypename.length; j++) {
                if (member.itemtypeId == $scope.itemtypename[j].id) {
                  member.itemtypename = $scope.itemtypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('unitmeasurements?filter[where][deleteflag]=false').getList().then(function (unitmeasure) {
              $scope.unitmeasurementname = unitmeasure;
              for (var j = 0; j < $scope.unitmeasurementname.length; j++) {
                if (member.unitmeasurementId == $scope.unitmeasurementname[j].id) {
                  member.unitmeasurementname = $scope.unitmeasurementname[j].unit;
                  break;
                }
              }
            });
          });
        });
      }
    });

    $scope.$watch('itemcategoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('itemtypes?filter[where][deleteflag]=false' + '&filter[where][itemcategoryId]=' + newValue).getList().then(function (cate) {
          $scope.itemtypes = cate;
        });

        Restangular.all('itemdefinitions?filter[where][deleteflag]=false' + '&filter[where][itemcategoryId]=' + newValue).getList().then(function (itemdef) {
          $scope.itemdefinitions = itemdef;
          angular.forEach($scope.itemdefinitions, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
              $scope.itemcategoryname = itmcateg;
              for (var j = 0; j < $scope.itemcategoryname.length; j++) {
                if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                  member.itemcategoryname = $scope.itemcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtyp) {
              $scope.itemtypename = itmtyp;
              for (var j = 0; j < $scope.itemtypename.length; j++) {
                if (member.itemtypeId == $scope.itemtypename[j].id) {
                  member.itemtypename = $scope.itemtypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('unitmeasurements?filter[where][deleteflag]=false').getList().then(function (unitmeasure) {
              $scope.unitmeasurementname = unitmeasure;
              for (var j = 0; j < $scope.unitmeasurementname.length; j++) {
                if (member.unitmeasurementId == $scope.unitmeasurementname[j].id) {
                  member.unitmeasurementname = $scope.unitmeasurementname[j].unit;
                  break;
                }
              }
            });
          });
        });
      }
    });

    $scope.$watch('itemtypeId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('itemdefinitions?filter[where][deleteflag]=false' + '&filter[where][itemtypeId]=' + newValue).getList().then(function (itemdef) {
          $scope.itemdefinitions = itemdef;
          angular.forEach($scope.itemdefinitions, function (member, index) {
            member.index = index + 1;

            Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
              $scope.customername = custmor;
              for (var j = 0; j < $scope.customername.length; j++) {
                if (member.customerId == $scope.customername[j].id) {
                  member.customername = $scope.customername[j].name;
                  break;
                }
              }
            });
            Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
              $scope.categoryname = categ;
              for (var j = 0; j < $scope.categoryname.length; j++) {
                if (member.categoryId == $scope.categoryname[j].id) {
                  member.categoryname = $scope.categoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
              $scope.subcategoryname = subcateg;
              for (var j = 0; j < $scope.subcategoryname.length; j++) {
                if (member.subcategoryId == $scope.subcategoryname[j].id) {
                  member.subcategoryname = $scope.subcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itmcateg) {
              $scope.itemcategoryname = itmcateg;
              for (var j = 0; j < $scope.itemcategoryname.length; j++) {
                if (member.itemcategoryId == $scope.itemcategoryname[j].id) {
                  member.itemcategoryname = $scope.itemcategoryname[j].name;
                  break;
                }
              }
            });
            Restangular.all('itemtypes?filter[where][deleteflag]=false').getList().then(function (itmtyp) {
              $scope.itemtypename = itmtyp;
              for (var j = 0; j < $scope.itemtypename.length; j++) {
                if (member.itemtypeId == $scope.itemtypename[j].id) {
                  member.itemtypename = $scope.itemtypename[j].name;
                  break;
                }
              }
            });
            Restangular.all('unitmeasurements?filter[where][deleteflag]=false').getList().then(function (unitmeasure) {
              $scope.unitmeasurementname = unitmeasure;
              for (var j = 0; j < $scope.unitmeasurementname.length; j++) {
                if (member.unitmeasurementId == $scope.unitmeasurementname[j].id) {
                  member.unitmeasurementname = $scope.unitmeasurementname[j].unit;
                  break;
                }
              }
            });
          });
        });
      }
    });
  });