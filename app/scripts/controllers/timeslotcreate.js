'use strict';

angular.module('secondarySalesApp')
    .controller('TimeslotCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        $scope.category = {};

        $scope.disableSave = false;

        var customerUrl = '';

        if ($window.sessionStorage.roleId == 2) {
            customerUrl = 'customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId;
        } else {
            customerUrl = 'customers?filter[where][deleteflag]=false';
        }

        Restangular.all(customerUrl).getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.$watch('category.customerid', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (cat) {
                    $scope.categories = cat;
                });
            }
        });

        $scope.$watch('category.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (subcat) {
                    $scope.subcategories = subcat;
                });
            }
        });

        $scope.$watch('category.subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == undefined) {
                return;
            } else {
                Restangular.one('subcategories', newValue).get().then(function (scat) {
                    if (scat.worktype == 'H' || scat.worktype == 'M') {
                        if (!$routeParams.id) {
                            Restangular.all('timeslots?filter[where][deleteflag]=false').getList().then(function (tslots) {
                                $scope.timeslots = tslots;

                                angular.forEach($scope.timeslots, function (data) {
                                    data.desc = data.minute;
                                    data.isChecked = false;
                                });
                            });
                        }

                        Restangular.all('customertimeslots?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + $scope.category.categoryId + '&filter[where][subcategoryId]=' + newValue + '&filter[where][customerId]=' + $scope.category.customerid).getList().then(function (custList) {
                            $scope.customertimeslotList = custList;
                            if ($scope.customertimeslotList.length > 0) {
                                $scope.disableSave = true;
                            } else {
                                $scope.disableSave = false;
                            }
                        });
                    } else {
                        $scope.timeslots = [];
                        $scope.disableSave = false;
                    }
                });
            }
        });

        $scope.saveTimeslot = function () {
            var timeslotid = [];
            var count = 0;
            angular.forEach($scope.timeslots, function (data) {
                if (data.isChecked) {
                    var slot = data.id + '-' + data.desc;
                    timeslotid.push(slot);
                    count++;
                } else {
                    count++;
                }
                if (count == $scope.timeslots.length) {
                    $scope.category.timeslotids = timeslotid.toString();
                    Restangular.all('customertimeslots').post($scope.category).then(function () {
                        window.location = '/timeslot';
                    });
                }
            });
            if ($scope.timeslots.length == 0) {
                $scope.category.timeslotids = null;
                Restangular.all('customertimeslots').post($scope.category).then(function () {
                    window.location = '/timeslot';
                });
            }
        };

        $scope.updateTimeslot = function () {
            var timeslotid = [];
            var count = 0;
            angular.forEach($scope.timeslots, function (data) {
                if (data.isChecked) {
                    var slot = data.id + '-' + data.desc;
                    timeslotid.push(slot);
                    count++;
                } else {
                    count++;
                }
                if (count == $scope.timeslots.length) {
                    $scope.category.timeslotids = timeslotid.toString();
                    Restangular.one('customertimeslots', $routeParams.id).customPUT($scope.category).then(function () {
                        window.location = '/timeslot';
                    });
                }
            });
            if ($scope.timeslots.length == 0) {
                $scope.category.timeslotids = null;
                Restangular.all('customertimeslots').post($scope.category).then(function () {
                    window.location = '/timeslot';
                });
            }
        };

        $scope.showSave = true;

        if ($routeParams.id) {
            $scope.showSave = false;
            Restangular.one('customertimeslots', $routeParams.id).get().then(function (custData) {
                $scope.category = custData;
                var splitArray = custData.timeslotids.split(',');
                console.log('splitArray', splitArray);

                Restangular.all('timeslots?filter[where][deleteflag]=false').getList().then(function (tslots) {
                    $scope.timeslots = tslots;
                    angular.forEach($scope.timeslots, function (data) {
                        data.desc = data.minute;
                        var filter_array = splitArray.filter(function (arr) {
                            var ijk = arr.split('-');
                            return ijk[0] == data.id
                        });

                        if (filter_array.length > 0) {
                            var f_value = filter_array[filter_array.length - 1].split('-');
                            data.desc = f_value[f_value.length - 1];
                            data.isChecked = true;
                        }
                    });
                });
            });
        }
    });