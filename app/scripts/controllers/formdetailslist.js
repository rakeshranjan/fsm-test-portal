'use strict';

angular.module('secondarySalesApp')
  .controller('FormLevelDetailsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

    console.log("here");
    // $scope.rcdetailslanguages = Restangular.all('rcdetailslanguages?filter[where][deleteflag]=false').getList().$object;
    Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailsresp) {
      $scope.rcdetails = rcdetailsresp;
      console.log($scope.rcdetails, "RC-RESP-10")

      Restangular.all('languagedefinitions?filter[where][deleteFlag]=false').getList().then(function (reslang) {
        $scope.languageDisplays = reslang;
        console.log($scope.languageDisplays, "LANG-DISP-14")

        angular.forEach($scope.rcdetails, function (member, index) {
          member.index = index + 1;

          // for (var a = 0; a < $scope.languageDisplays.length; a++) {
          //   if (member.languageId == $scope.languageDisplays[a].id) {
          //     member.languageName = $scope.languageDisplays[a].name;
          //     break;
          //   }
          // }


          //$scope.TotalData = member;
          // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
        });
      });

    });

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (customer) {
      $scope.customers = customer;
    });

    Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (zoneResp) {
      $scope.zones = zoneResp;
    });

    $scope.getcategory = function (categoryId) {
      return Restangular.one('categories', categoryId).get().$object;
    };

    $scope.subgetcategory = function (subcategoryId) {
      return Restangular.one('subcategories', subcategoryId).get().$object;
    };

    $scope.getcity = function (cityId) {
      return Restangular.one('organisationlocations', cityId).get().$object;
    };

    $scope.getpincode = function (pincodeId) {
      return Restangular.one('organisationlocations', pincodeId).get().$object;
    };

    $scope.getstatus = function (statusId) {
      return Restangular.one('todostatuses', statusId).get().$object;
    };

    $scope.$watch('customerId', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue;

        Restangular.all($scope.memberUrl).getList().then(function (member) {
          $scope.rcdetails = member;
          console.log($scope.rcdetails, "Member-Here at 305")
          angular.forEach($scope.rcdetails, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });

    $scope.$watch('zoneId', function (newValue, oldValue) {
      //console.log('Report Too');
      if (newValue === oldValue || newValue === '' || newValue === null) {
        return;
      } else {
        $scope.memberUrl = 'rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + newValue;

        Restangular.all($scope.memberUrl).getList().then(function (member) {
          $scope.rcdetails = member;
          console.log($scope.rcdetails, "Member-Here at 305")
          angular.forEach($scope.rcdetails, function (member, index) {
            member.index = index + 1;
          });
        });
      }
    });


  });
