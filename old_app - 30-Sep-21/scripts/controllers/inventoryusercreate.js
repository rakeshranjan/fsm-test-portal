'use strict';

angular.module('secondarySalesApp')
    .controller('InventoryuserCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        Restangular.all('customers?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.customerId).getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.inventory = {
            quantitytransfer: 0,
            quantityreturn: 0
        };

        $scope.$watch('inventory.customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });
            }
        });

        $scope.$watch('inventory.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });
            }
        });

        $scope.$watch('inventory.subcategoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                var splitArray = $window.sessionStorage.orgStructure.split(';');
                var splitArray1 = splitArray[splitArray.length - 1];
                var splitArray2 = splitArray1.split('-');
                var splitArray2 = splitArray2[splitArray2.length - 1];
                Restangular.all('warehouses?filter[where][site]=' + splitArray2).getList().then(function (whs) {
                    Restangular.all('warehouses?filter[where][subcategoryId]=' + newValue + '&filter[where][id]=' + whs[0].id).getList().then(function (whouse) {
                        $scope.warehouses = whouse;
                    });
                });

                Restangular.all('itemdefinitions?filter[where][subcategoryId]=' + newValue).getList().then(function (items) {
                    $scope.itemdefinitions = items;
                });

                Restangular.all('itemstatuses?filter[where][deleteflag]=false').getList().then(function (status) {
                    $scope.itemstatuses = status;
                });
            }
        });

        $scope.validatestring = '';

        $scope.Save = function () {
            // document.getElementById('name').style.border = "";

            if ($scope.inventory.customerId == '' || $scope.inventory.customerId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Customer';
                // document.getElementById('name').style.border = "1px solid #ff0000";

            } else if ($scope.inventory.warehouseId == '' || $scope.inventory.warehouseId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Warehouse';

            } else if ($scope.inventory.itemdefinitionId == '' || $scope.inventory.itemdefinitionId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Item';

            } else if ($scope.inventory.itemstatusId == '' || $scope.inventory.itemstatusId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Item Status';

            } else if ($scope.inventory.quantityinput == '' || $scope.inventory.quantityinput == null) {
                $scope.validatestring = $scope.validatestring + 'Plese enter quantity input';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.inventory.deleteflag = false;
                $scope.inventory.approvedflag = false;

                Restangular.all('inventories').post($scope.inventory).then(function () {
                    //    console.log('Zone Saved');
                    window.location = '/inventoryuser';
                });
            }
        };

        $scope.Update = function () {
            // document.getElementById('name').style.border = "";

            if ($scope.inventory.customerId == '' || $scope.inventory.customerId == null) {
                // $scope.inventory.soid = null;
                $scope.validatestring = $scope.validatestring + 'Please select a Customer';
                // document.getElementById('name').style.border = "1px solid #ff0000";
            } else if ($scope.inventory.itemdefinitionId == '' || $scope.inventory.itemdefinitionId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Item';

            } else if ($scope.inventory.itemstatusId == '' || $scope.inventory.itemstatusId == null) {
                $scope.validatestring = $scope.validatestring + 'Plese select a Item Status';

            } else if ($scope.inventory.quantityinput == '' || $scope.inventory.quantityinput == null) {
                $scope.validatestring = $scope.validatestring + 'Plese enter quantity input';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                Restangular.all('inventories').customPUT($scope.inventory).then(function () {
                    $location.path('/inventoryuser');
                });
            }
        };

        $scope.showCreate = true;

        if ($routeParams.id) {
            $scope.showCreate = false;
            Restangular.one('inventories', $routeParams.id).get().then(function (inventory) {
                $scope.original = inventory;
                $scope.inventory = Restangular.copy($scope.original);
            });
        }

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.minimumDate = new Date();

        $scope.minDate = new Date();

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
    });