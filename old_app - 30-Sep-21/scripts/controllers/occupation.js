'use strict';

angular.module('secondarySalesApp')
    .controller('OccupationCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
//        if ($window.sessionStorage.roleId != 1) {
//            window.location = "/";
//        }

        $scope.showForm = function () {
            var visible = $location.path() === '/occupation/create' || $location.path() === '/occupation/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/occupation/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/occupation/create' || $location.path() === '/occupation/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/occupation/create' || $location.path() === '/occupation/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/occupation") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        $scope.DisplayOccupation = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayOccupation.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayOccupation.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayOccupation[index].disableLang = false;
                $scope.DisplayOccupation[index].disableAdd = false;
                $scope.DisplayOccupation[index].disableRemove = false;
                $scope.DisplayOccupation[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayOccupation[index].disableLang = true;
                $scope.DisplayOccupation[index].disableAdd = false;
                $scope.DisplayOccupation[index].disableRemove = false;
                $scope.DisplayOccupation[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.validatestring = "";

        $scope.SaveLang = function () {

            $scope.DisplayOccupation[$scope.lastIndex].disableAdd = true;
            $scope.DisplayOccupation[$scope.lastIndex].disableRemove = true;
                $scope.DisplayOccupation[$scope.lastIndex].languages = angular.copy($scope.languages);

//            angular.forEach($scope.languages, function (data, index) {
//                data.inx = index + 1;
//                $scope.DisplayOccupation[$scope.lastIndex]["lang" + data.inx] = data.lang;
//                console.log($scope.DisplayOccupation);;
//            });

            $scope.langModel = false;
        };


        if ($routeParams.id) {
            $scope.message = 'Occupation has been Updated!';
            Restangular.one('occupations', $routeParams.id).get().then(function (occupation) {
                Restangular.all('occupations?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                $scope.original = occupation;
                $scope.occupation = Restangular.copy($scope.original);
                });
            });
        } else {
            $scope.message = 'Occupation has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/

        Restangular.all('occupations?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (occ) {
            $scope.occupations = occ;
            angular.forEach($scope.occupations, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveOcc();
        };

        var saveCount = 0;

        $scope.saveOcc = function () {

            if (saveCount < $scope.DisplayOccupation.length) {

                if ($scope.DisplayOccupation[saveCount].name == '' || $scope.DisplayOccupation[saveCount].name == null) {
                    saveCount++;
                    $scope.saveOcc();
                } else {

                    Restangular.all('occupations').post($scope.DisplayOccupation[saveCount]).then(function (resp) {
//                        saveCount++;
//                        $scope.saveOcc();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayOccupation[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayOccupation[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayOccupation[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                type: resp.type
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/occupation';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('occupations').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveOcc();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.occupation.name == '' || $scope.occupation.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Occupation Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.occupation.lastmodifiedby = $window.sessionStorage.userId;
                $scope.occupation.lastmodifiedtime = new Date();
                $scope.occupation.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('occupations', $routeParams.id).customPUT($scope.occupation).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('occupations', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/occupation';
            }

        }


        $scope.gender = {
            "name": '',
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.occupation.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
             angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if (data.id === lngwrkflw.language) {
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.langModel = false;
        };


        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('occupations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
