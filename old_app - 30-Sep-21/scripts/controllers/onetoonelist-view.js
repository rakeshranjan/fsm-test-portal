'use strict';

angular.module('secondarySalesApp')
  .controller('OneToOneListViewCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/
    $scope.modalTitle = 'Thank You';

    $scope.DisplayTodoStatus = [{
      name: '',
      disableLang: false,
      disableAdd: false,
      disableRemove: false,
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId,
      langdark: 'images/Lgrey.png'
    }];

    $scope.Add = function (index) {

      $scope.myobj = {};

      var idVal = index + 1;

      $scope.DisplayTodoStatus.push({
        name: '',
        disableLang: false,
        disableAdd: false,
        disableRemove: false,
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.userId,
        lastmodifiedrole: $window.sessionStorage.roleId,
        lastmodifiedtime: new Date(),
        createdby: $window.sessionStorage.userId,
        createdtime: new Date(),
        createdrole: $window.sessionStorage.roleId,
        langdark: 'images/Lgrey.png'
      });
    };

    $scope.Remove = function (index) {
      var indexVal = index - 1;
      $scope.DisplayTodoStatus.splice(indexVal, 1);
    };

    $scope.myobj = {};

    $scope.typeChange = function (index) {
      $scope.DisplayTodoStatus[index].name = '';
      $scope.DisplayTodoStatus[index].disableLang = false;
      $scope.DisplayTodoStatus[index].langdark = 'images/Lgrey.png';
    };

    $scope.levelChange = function (index) {
      $scope.DisplayTodoStatus[index].disableLang = true;
      $scope.DisplayTodoStatus[index].langdark = 'images/Ldark.png';
      $scope.DisplayTodoStatus[index].disableAdd = false;
      $scope.DisplayTodoStatus[index].disableRemove = false;
    };

    $scope.langModel = false;

    $scope.Lang = function (index, name, type) {

      $scope.lastIndex = index;

      angular.forEach($scope.languages, function (data) {
        data.lang = name;
        // console.log(data);
      });

      $scope.langModel = true;
    };

    $scope.SaveLang = function () {

      if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
        $scope.validatestring = $scope.validatestring + 'Please Enter Due Days';
        document.getElementById('order').style.borderColor = "#FF0000";

      }
      if ($scope.myobj.actionable == '' || $scope.myobj.actionable == null) {
        $scope.validatestring = $scope.validatestring + 'Please Select Actionable';
        document.getElementById('action').style.borderColor = "#FF0000";
      }
      if ($scope.validatestring != '') {
        $scope.toggleValidation();
        $scope.validatestring1 = $scope.validatestring;
        $scope.validatestring = '';
        //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
      } else {

        //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
        $scope.DisplayTodoStatus[$scope.lastIndex].disableAdd = true;
        $scope.DisplayTodoStatus[$scope.lastIndex].disableRemove = true;
        $scope.DisplayTodoStatus[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
        $scope.DisplayTodoStatus[$scope.lastIndex].actionable = $scope.myobj.actionable;

        angular.forEach($scope.languages, function (data, index) {
          data.inx = index + 1;
          $scope.DisplayTodoStatus[$scope.lastIndex]["lang" + data.inx] = data.lang;
          console.log($scope.DisplayTodoStatus);;
        });

        $scope.langModel = false;
      }
    };

    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/onetoonelist-view" || $window.sessionStorage.prviousLocation != "partials/onetoonelist") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      console.log('mypage', mypage);
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };

    /*********************************** INDEX *******************************************/

    Restangular.all('surveyquestionviews?filter[where][deleteflag]=false').getList().then(function (srvy){
      $scope.surveyquestions = srvy;
      angular.forEach($scope.surveyquestions, function (member, index){
        member.index = index + 1;
      });
    });

    // Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().then(function (srvy) {
    //   $scope.surveyquestions = srvy;
    //   console.log("srvy", srvy)
    //   angular.forEach($scope.surveyquestions, function (member, index) {
    //     member.index = index + 1;

    //     //                if (member.applyskip == 'yes') {
    //     //                    member.applyskipname = 'Yes';
    //     //                } else {
    //     //                    member.applyskipname = 'No';
    //     //                }
    //     Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
    //       $scope.customername = custmor;
    //       for (var j = 0; j < $scope.customername.length; j++) {
    //         if (member.customerId == $scope.customername[j].id) {
    //           member.customername = $scope.customername[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
    //       $scope.categoryname = categ;
    //       for (var j = 0; j < $scope.categoryname.length; j++) {
    //         if (member.categoryId == $scope.categoryname[j].id) {
    //           member.categoryname = $scope.categoryname[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
    //       $scope.subcategoryname = subcateg;
    //       for (var j = 0; j < $scope.subcategoryname.length; j++) {
    //         if (member.subcategoryId == $scope.subcategoryname[j].id) {
    //           member.subcategoryname = $scope.subcategoryname[j].name;
    //           break;
    //         }
    //       }
    //     });
    //     $scope.TotalTodos = [];
    //     $scope.TotalTodos.push(member);
    //   });
    // });

    $scope.gettodo = function (applytomessage) {
      return Restangular.one('todotypes', applytomessage).get().$object;
    };

    $scope.gettype = function (questiontype) {
      return Restangular.one('onetoonetypes', questiontype).get().$object;
    };

    $scope.customerId = '';
    $scope.categoryId = '';
    $scope.subcategoryId = '';

    Restangular.all('customers').getList().then(function (cust) {
      $scope.customers = cust;
    });

    $scope.$watch('customerId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (catg) {
          $scope.categories = catg;
        });

        Restangular.all('surveyquestionviews?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (srvy) {
          $scope.surveyquestions = srvy;
          angular.forEach($scope.surveyquestions, function (member, index){
            member.index = index + 1;
          });
        });
      //   Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + newValue).getList().then(function (srvy) {
      //     $scope.surveyquestions = srvy;

      //     angular.forEach($scope.surveyquestions, function (member, index) {
      //       member.index = index + 1;

      //       Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
      //         $scope.customername = custmor;
      //         for (var j = 0; j < $scope.customername.length; j++) {
      //           if (member.customerId == $scope.customername[j].id) {
      //             member.customername = $scope.customername[j].name;
      //             break;
      //           }
      //         }
      //       });
      //       Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
      //         $scope.categoryname = categ;
      //         for (var j = 0; j < $scope.categoryname.length; j++) {
      //           if (member.categoryId == $scope.categoryname[j].id) {
      //             member.categoryname = $scope.categoryname[j].name;
      //             break;
      //           }
      //         }
      //       });
      //       Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
      //         $scope.subcategoryname = subcateg;
      //         for (var j = 0; j < $scope.subcategoryname.length; j++) {
      //           if (member.subcategoryId == $scope.subcategoryname[j].id) {
      //             member.subcategoryname = $scope.subcategoryname[j].name;
      //             break;
      //           }
      //         }
      //       });
      //       $scope.TotalTodos = [];
      //       $scope.TotalTodos.push(member);
      //     });
      //   });
      }
    });

    $scope.$watch('categoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('subcategories?filter[where][categoryId]=' + newValue).getList().then(function (scate) {
          $scope.subcategories = scate;
        });

        Restangular.all('surveyquestionviews?filter[where][deleteflag]=false' + '&filter[where][categoryId]=' + newValue).getList().then(function (srvy) {
          $scope.surveyquestions = srvy;
          angular.forEach($scope.surveyquestions, function (member, index){
            member.index = index + 1;
          });
        });
        // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId).getList().then(function (srvy) {
        //   $scope.surveyquestions = srvy;

        //   angular.forEach($scope.surveyquestions, function (member, index) {
        //     member.index = index + 1;

        //     Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
        //       $scope.customername = custmor;
        //       for (var j = 0; j < $scope.customername.length; j++) {
        //         if (member.customerId == $scope.customername[j].id) {
        //           member.customername = $scope.customername[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
        //       $scope.categoryname = categ;
        //       for (var j = 0; j < $scope.categoryname.length; j++) {
        //         if (member.categoryId == $scope.categoryname[j].id) {
        //           member.categoryname = $scope.categoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
        //       $scope.subcategoryname = subcateg;
        //       for (var j = 0; j < $scope.subcategoryname.length; j++) {
        //         if (member.subcategoryId == $scope.subcategoryname[j].id) {
        //           member.subcategoryname = $scope.subcategoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     $scope.TotalTodos = [];
        //     $scope.TotalTodos.push(member);
        //   });
        // });
      }
    });

    $scope.$watch('subCategoryId', function (newValue, oldValue) {
      if (newValue === oldValue || newValue == '') {
        return;
      } else {
        Restangular.all('surveyquestionviews?filter[where][deleteflag]=false' + '&filter[where][subcategoryId]=' + newValue).getList().then(function (srvy) {
          $scope.surveyquestions = srvy;
          angular.forEach($scope.surveyquestions, function (member, index){
            member.index = index + 1;
          });
        });
        // Restangular.all('surveyquestions?filter[where][deleteflag]=false' + '&filter[where][customerId]=' + $scope.customerId + '&filter[where][categoryId]=' + $scope.categoryId + '&filter[where][subcategoryId]=' + newValue).getList().then(function (srvy) {
        //   $scope.surveyquestions = srvy;

        //   angular.forEach($scope.surveyquestions, function (member, index) {
        //     member.index = index + 1;

        //     Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (custmor) {
        //       $scope.customername = custmor;
        //       for (var j = 0; j < $scope.customername.length; j++) {
        //         if (member.customerId == $scope.customername[j].id) {
        //           member.customername = $scope.customername[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (categ) {
        //       $scope.categoryname = categ;
        //       for (var j = 0; j < $scope.categoryname.length; j++) {
        //         if (member.categoryId == $scope.categoryname[j].id) {
        //           member.categoryname = $scope.categoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     Restangular.all('subcategories?filter[where][deleteflag]=false').getList().then(function (subcateg) {
        //       $scope.subcategoryname = subcateg;
        //       for (var j = 0; j < $scope.subcategoryname.length; j++) {
        //         if (member.subcategoryId == $scope.subcategoryname[j].id) {
        //           member.subcategoryname = $scope.subcategoryname[j].name;
        //           break;
        //         }
        //       }
        //     });
        //     $scope.TotalTodos = [];
        //     $scope.TotalTodos.push(member);
        //   });
        // });
      }
    });
  });
