'use strict';

angular.module('secondarySalesApp')
  .controller('Rollback-viewCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

    console.log("Roll Back View");

    $scope.memberlanguage = Restangular.one('memberlanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;

    $scope.$watch('member.customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
          $scope.categories = catgresp;

        });
      }
    });

    $scope.$watch('member.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
      }
    });

    $scope.disableApproveButton = false;
    $scope.disableRejectButton = false;
    // -------------Rollback Approve Function --------------------------------------------

    $scope.rollbackApprove = function (clicked) {
      $scope.disableApproveButton = true;
      console.log("rollbackApprove Function");
      console.log("Ticket Rollback remark", $scope.tktansw);
      Restangular.one('rcdetails', $routeParams.id).get().then(function (tickets) {
        // Restangular.all('surveyanswers?filter[where][memberid]=' + $scope.member.id).getList().then(function (sansw) {
        Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + $routeParams.id).getList().then(function (sansw) {
          $scope.SurveyAnswers = sansw;
          console.log("Ticket Details: ", tickets);
          console.log("Last Answered QuestionID", $scope.SurveyAnswers[sansw.length - 1].questionid);

          Restangular.one('surveyquestions?filter[where][id]=' + $scope.SurveyAnswers[sansw.length - 1].questionid).get().then(function (ticketsquest) {
            console.log("LatQuestion Serial No: ", ticketsquest[0].serialno);
            // $scope.tktansw = {
            //   id: $scope.SurveyAnswers[sansw.length - 1].id,
            //   deleteflag: true,
            //   rollbackstatusId: 2
            // };
            $scope.tktanswUpdate = {
              id: $scope.SurveyAnswers[sansw.length - 1].id,
              deleteflag: true
              //rollbackstatusId: 2
            };
              Restangular.one('surveyanswers', $scope.SurveyAnswers[sansw.length - 1].id).customPUT($scope.tktanswUpdate).then(function (resAns) {


            });
            if(sansw.length > 1) {
            $scope.tktansw = {
              memberid: $routeParams.id,
              questionid: $scope.SurveyAnswers[sansw.length - 1].questionid,
              answer: $scope.SurveyAnswers[sansw.length - 2].answer,
              deleteflag: true,
              documentflag: $scope.SurveyAnswers[sansw.length - 1].documentflag,
              stress_data: $scope.SurveyAnswers[sansw.length - 1].stress_data,
              lastmodifiedby: $window.sessionStorage.userId,
              lastmodifiedrole: $window.sessionStorage.roleId,
              lastmodifiedtime: new Date(),
              createdby: $window.sessionStorage.userId,
              createdtime: new Date(),
              createdrole: $window.sessionStorage.roleId,
              remark: $scope.SurveyAnswers[sansw.length - 1].remark,
              uploadedfiles: $scope.SurveyAnswers[sansw.length - 1].uploadedfiles,
              updatetypeid: $scope.SurveyAnswers[sansw.length - 1].updatetypeid,
              serviceeventid: $scope.SurveyAnswers[sansw.length - 1].serviceeventid,
              serviceidentifier: $scope.SurveyAnswers[sansw.length - 1].serviceidentifier,
              aspname: $scope.SurveyAnswers[sansw.length - 1].aspname,
              rollbackRequestTime: new Date(),
              rollbackstatusId: 2,
              rollbackRoleId: $window.sessionStorage.roleId,
              loginUserId: $window.sessionStorage.userId,
             // rollbackRemark: $scope.SurveyAnswers[sansw.length - 1].rollbackRemark
             rollbackRemark: 'Approved',
             statusid: $scope.SurveyAnswers[sansw.length - 1].statusid
            };
          } else {
            $scope.tktansw = {
              memberid: $routeParams.id,
              questionid: $scope.SurveyAnswers[sansw.length - 1].questionid,
              answer: $scope.SurveyAnswers[sansw.length - 1].answer,
              deleteflag: true,
              documentflag: $scope.SurveyAnswers[sansw.length - 1].documentflag,
              stress_data: $scope.SurveyAnswers[sansw.length - 1].stress_data,
              lastmodifiedby: $window.sessionStorage.userId,
              lastmodifiedrole: $window.sessionStorage.roleId,
              lastmodifiedtime: new Date(),
              createdby: $window.sessionStorage.userId,
              createdtime: new Date(),
              createdrole: $window.sessionStorage.roleId,
              remark: $scope.SurveyAnswers[sansw.length - 1].remark,
              uploadedfiles: $scope.SurveyAnswers[sansw.length - 1].uploadedfiles,
              updatetypeid: $scope.SurveyAnswers[sansw.length - 1].updatetypeid,
              serviceeventid: $scope.SurveyAnswers[sansw.length - 1].serviceeventid,
              serviceidentifier: $scope.SurveyAnswers[sansw.length - 1].serviceidentifier,
              aspname: $scope.SurveyAnswers[sansw.length - 1].aspname,
              rollbackRequestTime: new Date(),
              rollbackstatusId: 2,
              rollbackRoleId: $window.sessionStorage.roleId,
              loginUserId: $window.sessionStorage.userId,
             // rollbackRemark: $scope.SurveyAnswers[sansw.length - 1].rollbackRemark
             rollbackRemark: 'Approved',
             statusid: $scope.SurveyAnswers[sansw.length - 1].statusid
            };
          }
            Restangular.all('surveyanswers').post($scope.tktansw).then(function (conResponse) {
              console.log('conResponse', conResponse);
              // Restangular.one('surveyanswers', $scope.SurveyAnswers[sansw.length - 1].id).customPUT($scope.tktansw).then(function (resAns) {


            });
            console.log("Current RC CODE: ", tickets.rcCode);
            if ($scope.SurveyAnswers.length < 2) {
              if (tickets.rcCode == 10) {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  assignedto: 0,
                  producttypeid: null,
                  serviceeventid: null,
                  serviceidentifier: '',
                  starttime: '',
                  endtime: '',
                  aspname: '',
                  assignFlag: false,
                  statusId: 1,
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              } else if (tickets.rcCode == 11 || tickets.rcCode == 16) {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  statusId: 1,
                  producttypeid: null,
                  serviceeventid: null,
                  serviceidentifier: '',
                  etatime: '',
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              } else {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  statusId: 1,
                  producttypeid: null,
                  serviceeventid: null,
                  serviceidentifier: '',
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              }
            } else {
              if (tickets.rcCode == 10) {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  assignedto: 0,
                  producttypeid: $scope.SurveyAnswers[sansw.length - 2].updatetypeid,
                  serviceeventid: $scope.SurveyAnswers[sansw.length - 2].serviceeventid,
                  serviceidentifier: $scope.SurveyAnswers[sansw.length - 2].serviceidentifier,
                  starttime: '',
                  endtime: '',
                  aspname: '',
                  assignFlag: false,
                  statusId: 2,
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              } else if (tickets.rcCode == 11 || tickets.rcCode == 16) {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  producttypeid: $scope.SurveyAnswers[sansw.length - 2].updatetypeid,
                  serviceeventid: $scope.SurveyAnswers[sansw.length - 2].serviceeventid,
                  serviceidentifier: $scope.SurveyAnswers[sansw.length - 2].serviceidentifier,
                  etatime: '',
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              } else {
                $scope.member = {
                  id: $routeParams.id,
                  nextquestionId: ticketsquest[0].serialno,
                  rcCode: ticketsquest[0].rcCode,
                  producttypeid: $scope.SurveyAnswers[sansw.length - 2].updatetypeid,
                  serviceeventid: $scope.SurveyAnswers[sansw.length - 2].serviceeventid,
                  serviceidentifier: $scope.SurveyAnswers[sansw.length - 2].serviceidentifier,
                  rollbackRemark: $scope.rollbackRemarked,
                  rollbackStatus: 'A'

                };
              }
            }
            Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function (resp) {

              window.location = '/roll-back';
            });
          });
        });
      });

    };

    // -------------Rollback Reject Function --------------------------------------------

    $scope.rollbackReject = function (clicked) {
      $scope.disableRejectButton = true;
      console.log("rollbackReject Function");
      console.log("Ticket Rollback remark", $scope.tktansw);
      Restangular.one('rcdetails', $routeParams.id).get().then(function (tickets) {
        Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + $routeParams.id).getList().then(function (sansw) {
          $scope.SurveyAnswers = sansw;

          // $scope.tktansw = {
          //   id: $scope.SurveyAnswers[sansw.length - 1].id,
          //   rollbackstatusId: 3
          // };
          // Restangular.one('surveyanswers', $scope.SurveyAnswers[sansw.length - 1].id).customPUT($scope.tktansw).then(function (resAns) {
          $scope.tktansw = {
            memberid: $routeParams.id,
            questionid: $scope.SurveyAnswers[sansw.length - 1].questionid,
            answer: $scope.SurveyAnswers[sansw.length - 1].answer,
            deleteflag: false,
            documentflag: $scope.SurveyAnswers[sansw.length - 1].documentflag,
            stress_data: $scope.SurveyAnswers[sansw.length - 1].stress_data,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            remark: $scope.SurveyAnswers[sansw.length - 1].remark,
            uploadedfiles: $scope.SurveyAnswers[sansw.length - 1].uploadedfiles,
            updatetypeid: $scope.SurveyAnswers[sansw.length - 1].updatetypeid,
            serviceeventid: $scope.SurveyAnswers[sansw.length - 1].serviceeventid,
            serviceidentifier: $scope.SurveyAnswers[sansw.length - 1].serviceidentifier,
            aspname: $scope.SurveyAnswers[sansw.length - 1].aspname,
            rollbackRequestTime: new Date(),
            rollbackstatusId: 3,
            rollbackRoleId: $window.sessionStorage.roleId,
            loginUserId: $window.sessionStorage.userId,
            rollbackRemark: $scope.SurveyAnswers[sansw.length - 1].rollbackRemark,
            statusid: $scope.SurveyAnswers[sansw.length - 1].statusid
          };
          Restangular.all('surveyanswers').post($scope.tktansw).then(function (conResponse) {
            console.log('conResponse', conResponse);

          });
          $scope.member = {
            id: $routeParams.id,
            rollbackRemark: $scope.rollbackRemarked,
            rollbackStatus: 'N'

          };
          Restangular.one('rcdetails', $routeParams.id).customPUT($scope.member).then(function (resp) {

            window.location = '/roll-back';
          });
        });
      });
    };


    // if ($routeParams.id) {
    $scope.disableLocation = true;
    // Restangular.one('members', $routeParams.id).get().then(function (member) {
    Restangular.one('rcdetails', $routeParams.id).get().then(function (member) {
      console.log(member, "MEMBER-321")

      Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().then(function (organisationlevels) {
        $scope.organisationlevels = organisationlevels;
        //console.log($scope.organisationlevels,"UPDATE-LN-324")
        $scope.orgstructure = member.orgstructure.split(";");

        for (var i = 0; i < $scope.organisationlevels.length; i++) {
          for (var j = 0; j < $scope.orgstructure.length; j++) {
            $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
            if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
              $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1];
              //console.log($scope.organisationlevels[i].locationid, $scope.organisationlevels[i].name,"333")
            }
          }
        }
        //console.log("$scope.organisationlevels", $scope.organisationlevels);

        Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
          $scope.organisationlocations = organisationlocations;

          angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
            organisationlevel.index = index;
            organisationlevel.organisationlocations = organisationlocations;
          });

          $scope.original = member;
          $scope.member = Restangular.copy($scope.original);
          $scope.rollbackRemarked = $scope.member.rollbackRemark;

          if ($scope.member.assignedto > 0) {
            Restangular.one('users/findOne?filter[where][id]=' + $scope.member.assignedto).get().then(function (userresp) {
              // $scope.user = userresp.username;
              $scope.username = userresp.username;
            });
          }

          $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id + '&filter[where][deleteflag]=false').getList().then(function (HRes) {
            $scope.HealthAnswers = HRes;

            //   Restangular.one('surveyquestions?filter[where][id]=' + $scope.HealthAnswers[HRes.length-1].questionid).get().then(function (ticketsquest) {
            //     console.log("Ticket Answers: ", ticketsquest);
            //     Restangular.one('surveyanswers', $scope.HealthAnswers[HRes.length-1].id).get().then(function (tktrollbackRemark) {
            //       $scope.tktansw = tktrollbackRemark;
            //       console.log("Ticket Rollback Remark: ", $scope.tktansw);
            //     });

            //   });
            console.log($scope.HealthAnswers, "ANS-35");
            angular.forEach($scope.HealthAnswers, function (member1, index) {
              member1.index = index + 1;
              if (member1.uploadedfiles == null || member1.uploadedfiles == undefined || member1.uploadedfiles == '') {
                member1.uploadedfilesList = [];
              } else {
                member1.uploadedfilesList = member1.uploadedfiles.split(',');
              }

            });


          });

        });
      });
    });

    $scope.getQuestion = function (questionId) {
      return Restangular.one('surveyquestions', questionId).get().$object;
    };

    $scope.getUser = function (createdby) {
      return Restangular.one('users', createdby).get().$object;
    };

  });
