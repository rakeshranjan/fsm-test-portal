'use strict';
angular.module('secondarySalesApp')
    .controller('ItemDefinitionCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $timeout) {
        /*********/
        //console.log("$routeParams.id-5", $routeParams.id)
        $scope.showForm = function () {
            var visible = $location.path() === '/itemdefinitioncreate' || $location.path() === '/itemdefinition/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/itemdefinitioncreate';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/itemdefinitioncreate';
            return visible;
        };

        if ($location.path() === '/itemdefinitioncreate') {
            $scope.disableDropdown = false;
          } else {
            $scope.disableDropdown = true;
          }
          /************************************************************************************/
          $scope.itemdefinition = {
            //organizationId: $window.sessionStorage.organizationId,
            deleteflag: false
          };

          /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
    //   if ($window.sessionStorage.prviousLocation != "partials/itemcategorycreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
    if ($window.sessionStorage.prviousLocation != "partials/itemdefinitioncreate" || $window.sessionStorage.prviousLocation != "partials/customers") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
      }
  
      $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
      $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
      };
  
      $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
      $scope.pageFunction = function (mypage) {
        console.log('mypage', mypage);
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
      };

      // ******************************** EOF Pagination *************************************************

      //------------------------------- Cust-Catg-Subcatg-------------------------
      Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
        $scope.customers = cust;
      });

      $scope.$watch('itemdefinition.customerId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
            $scope.categories = catgresp;
          });
        }
      });
  
      $scope.$watch('itemdefinition.categoryId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
            $scope.subcategories = subcatgresp;
          });
        }
      });

      //------------------------------- end of Cust-Catg-Subcatg-------------------------

      Restangular.all('itemcategories?filter[where][deleteflag]=false').getList().then(function (itemcatg) {
        $scope.itemcategories = itemcatg;
      });

      $scope.$watch('itemdefinition.itemcategoryId', function (newValue, oldValue) {
        if (newValue == '' || newValue == null || newValue == oldValue) {
          return;
        } else {
          Restangular.all('itemtypes?filter[where][deleteflag]=false&filter[where][itemcategoryId]=' + newValue).getList().then(function (itmtyp) {
            $scope.itemtypes = itmtyp;
          });
        }
      });

      //$scope.unitmeasurements = Restangular.all('unitmeasurements').getList().$object;
      Restangular.all('unitmeasurements').getList().then(function (unit) {
        $scope.unitmeasurements = unit;
        if ($scope.itemdefinition.unitmeasurementId) {
          $scope.itemdefinition.unitmeasurementId = $scope.itemdefinition.unitmeasurementId;
        }
      });

      //$scope.glcodes = Restangular.all('glcodes').getList().$object;
      Restangular.all('glcodes?filter[where][deleteflag]=false').getList().then(function (glcd) {
        $scope.glcodes = glcd;
      });

        $scope.$watch('itemdefinition.glcodeid', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                //$scope.subglcodes = Restangular.all('subglcodes?filter[where][glcodeid]=' + newValue).getList().$object;
                Restangular.all('subglcodes?filter[where][deleteflag]=false&filter[where][glcodeid]=' + newValue).getList().then(function (subglcd) {
                  $scope.subglcodes = subglcd;
                  if ($scope.itemdefinition.subglcode) {
                    $scope.itemdefinition.subglcode = $scope.itemdefinition.subglcode;
                  }
                });
            }
        });

        /****************************************** CREATE *********************************/
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
        document.getElementById('name').style.border = "";
        //console.log($scope.itemdefinition, "message-282")
            if ($scope.itemdefinition.name == '' || $scope.itemdefinition.name == null) {
            $scope.itemdefinition.name = null;
            $scope.validatestring = $scope.validatestring + 'Please  Enter Item Name';
            document.getElementById('name').style.border = "1px solid #ff0000";

            } else if ($scope.itemdefinition.customerId == '' || $scope.itemdefinition.customerId == null) {
                $scope.itemdefinition.customerId = null;
                $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

            } else if ($scope.itemdefinition.categoryId == '' || $scope.itemdefinition.categoryId == null) {
            $scope.itemdefinition.categoryId = null;
            $scope.validatestring = $scope.validatestring + 'Please  Select Category';

            } else if ($scope.itemdefinition.subcategoryId == '' || $scope.itemdefinition.subcategoryId == null) {
                $scope.itemdefinition.subcategoryId = null;
                $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';
    
            }
            if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            }
            else {
            $scope.message = 'Itemdefinition has been created!';
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            Restangular.all('itemdefinitions').post($scope.itemdefinition).then(function () {
                window.location = '/itemdefinition';
            });
        }
        };

        /***************************** UPDATE *******************************************/

     $scope.validatestring = '';
     $scope.updatecount = 0;

     $scope.Update = function () {
      document.getElementById('name').style.border = "";
      //console.log($scope.itemdefinition, "message-282")
          if ($scope.itemdefinition.name == '' || $scope.itemdefinition.name == null) {
          $scope.itemdefinition.name = null;
          $scope.validatestring = $scope.validatestring + 'Please  Enter Itemdefinition Name';
          document.getElementById('name').style.border = "1px solid #ff0000";

          } else if ($scope.itemdefinition.customerId == '' || $scope.itemdefinition.customerId == null) {
              $scope.itemdefinition.customerId = null;
              $scope.validatestring = $scope.validatestring + 'Please  Select Customer';

          } else if ($scope.itemdefinition.categoryId == '' || $scope.itemdefinition.categoryId == null) {
          $scope.itemdefinition.categoryId = null;
          $scope.validatestring = $scope.validatestring + 'Please  Select Category';

          } else if ($scope.itemdefinition.subcategoryId == '' || $scope.itemdefinition.subcategoryId == null) {
              $scope.itemdefinition.subcategoryId = null;
              $scope.validatestring = $scope.validatestring + 'Please  Select Subcategory';
  
          }
         if ($scope.validatestring != '') {
         $scope.toggleValidation();
         $scope.validatestring1 = $scope.validatestring;
         $scope.validatestring = '';
         }
         else {
         $scope.message = 'Itemdefinition has been updated!';
         $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
         $scope.neWsubmitDisable = true;
         Restangular.one('itemdefinitions/' + $routeParams.id).customPUT($scope.itemdefinition).then(function () {
             window.location = '/itemdefinition';
             
         });
       }
     };

       if ($routeParams.id) {
         console.log("$routeParams.id", $routeParams.id)
         Restangular.one('itemdefinitions', $routeParams.id).get().then(function (itmdef) {
           $scope.original = itmdef;
           $scope.itemdefinition = Restangular.copy($scope.original);
         });
       }

});