'use strict';

angular.module('secondarySalesApp')
    .controller('CustomFieldsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/customfields/create' || $location.path() === '/customfields/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/customfields/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/customfields/create' || $location.path() === '/customfields/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/customfields/create' || $location.path() === '/customfields/' + $routeParams.id;
            return visible;
        };


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/customfields") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /**********************************/


        /****************************************************************************************INDEX****************************/
        $scope.getCustomer = function (customerId) {
			return Restangular.one('customers', customerId).get().$object;
        };

        $scope.getCategory = function (categoryId) {
			return Restangular.one('categories', categoryId).get().$object;
        };

        $scope.getSubCategory = function (subCategoryId) {
			return Restangular.one('subcategories', subCategoryId).get().$object;
        };
        
        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
        });

        $scope.$watch('customfield.customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                    $scope.categories = catgresp;
                });
            }
        });

        $scope.$watch('customfield.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                    $scope.subcategories = subcatgresp;
                });
            }
        });

        $scope.searchCon = $scope.name;

        $scope.con = Restangular.all('customfieldlabels').getList().then(function (con) {
            $scope.customfields = con;
            console.log($scope.customfields);
            angular.forEach($scope.customfields, function (member, index) {
                member.index = index + 1;
            });
        });
        $scope.customfield = {
            "deleteflag": false
        };

        /****************************************************************************CREATE************************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            // if ($scope.customfield.name == '' || $scope.customfield.name == null) {
            //     $scope.customfield.name = null;
            //     $scope.validatestring = $scope.validatestring + 'Plese enter your customfield name';
            //     document.getElementById('name').style.border = "1px solid #ff0000";

            // }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.customfields.post($scope.customfield).then(function () {
                    console.log('$scope.customfield', $scope.customfield);
                    window.location = '/customfields';
                });
            }
        };

        /***********************************************************************************UPDATE****************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            // if ($scope.customfield.name == '' || $scope.customfield.name == null) {
            //     $scope.customfield.name = null;
            //     $scope.validatestring = $scope.validatestring + 'Plese enter your customfield name';
            //     document.getElementById('name').style.border = "1px solid #ff0000";

            // }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.customfields.customPUT($scope.customfield).then(function () {
                    console.log('$scope.customfield', $scope.customfield);
                    window.location = '/customfields';
                });
            }
        };
        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



        if ($routeParams.id) {
            $scope.message = 'Custom Field has been Updated!';
            Restangular.one('customfieldlabels', $routeParams.id).get().then(function (customfield) {
                $scope.original = customfield;
                $scope.customfield = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Custom Field has been Created!';
        }


        /**********************************************************************************************DELETE*************************/

        //        $scope.Delete = function (id) {
        //            if (confirm("Are you sure want to delete..!") == true) {
        //                Restangular.one('customfields/' + id).remove($scope.customfield).then(function () {
        //                    $route.reload();
        //                });
        //
        //            } else {
        //
        //            }
        //
        //        }
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('customfields/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
