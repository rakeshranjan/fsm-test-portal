'use strict';

angular.module('secondarySalesApp')
  .controller('HourlyCtrl', function ($scope, $http, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    $scope.hourly = {
      customerId: $window.sessionStorage.customerId,
    };

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });

    Restangular.one('customers?filter[where][id]=' + $window.sessionStorage.customerId).get().then(function (cus) {
      $scope.customerName = cus[0].name;

      Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (catgresp) {
        $scope.categories = catgresp;
        $scope.hourly.categoryId = 22;
      });
    });

    $scope.$watch('hourly.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
        $scope.months = [{ name: 'Jan', value: '01' }, { name: 'Feb', value: '02' }, { name: 'Mar', value: '03' }, { name: 'Apr', value: '04' }, { name: 'May', value: '05' }, { name: 'June', value: '06' }, { name: 'July', value: '07' }, { name: 'Aug', value: '08' }, { name: 'Sep', value: '09' }, { name: 'Oct', value: '10' }, { name: 'Nov', value: '11' }, { name: 'Dec', value: '12' }];
      }
    });

    $scope.$watch('hourly.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        $("#table1").hide();
        return;
      } else {
        Restangular.one('subcategories?filter[where][id]=' + newValue).get().then(function (scat) {
          $scope.subCategoryName = scat[0].name;
          $("#yourTextBox").hide();
          $("#yourTextBox1").hide();
          $("#yourTextBox2").hide();
          $("#table2").hide();
          $("#table1").show();
        });

        $scope.organisationlevels = [];

        Restangular.one('customers/findOne?filter[where][id]=' + $scope.hourly.customerId).get().then(function (cust) {
          Restangular.one('organisationlocations/findOne?filter[where][id]=' + cust.orgroot).get().then(function (orglevel) {
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + orglevel.level).get().then(function (orglvl) {
              $scope.organisationlevels.push(orglvl);
              Restangular.all('organisationlevels?filter[where][rootid]=' + orglvl.id + '&filter[where][languageparent]=true&filter[where][deleteflag]=false' + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
                angular.forEach(ogrlevls, function (data, index) {
                  $scope.organisationlevels.push(data);
                });

                angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                  Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + organisationlevel.id).getList().then(function (ogrlocs) {
                    organisationlevel.index = index;
                    organisationlevel.organisationlocations = ogrlocs;
                  });
                });
              });
            });
          });
        });

        $scope.timeOptions = [];

        var currentdate = new Date();

        var rcdetails_url_cust =
          'timeslots';

        var cust_timeslots_url =
          'customertimeslots' +
          '?filter[where][customerid]=' +
          $scope.hourly.customerId;

        Restangular.all(cust_timeslots_url).getList().then(function (custData) {
          var splitArray = custData[0].timeslotids.split(',');
          Restangular.all(rcdetails_url_cust).getList().then(function (custtimes) {
            angular.forEach(custtimes, function (obj, index) {
              var filter_array = splitArray.filter(function (arr) {
                return arr == obj.id
              });
              if (filter_array.length > 0) {
                $scope.timeOptions.push(obj);
              }
            });
          });
        });
      }
    });

    $scope.$watch('hourly.date', function (newValue, oldValue) {
      if (newValue == '' || newValue == null) {
        return;
      } else {
        var url = 'surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' +
          $scope.hourly.customerId +
          ']}}]},{"categoryId":{"inq":[' +
          $scope.hourly.categoryId +
          ']}}]},{"subcategoryId":{"inq":[' +
          $scope.hourly.subcategoryId +
          ']}}]}]}}';

        if ($scope.selectedData == 1) {
          var hourly_url = 'hourlyanswers?filter[where][data1]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 2) {
          var hourly_url = 'hourlyanswers?filter[where][data2]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 3) {
          var hourly_url = 'hourlyanswers?filter[where][data3]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 4) {
          var hourly_url = 'hourlyanswers?filter[where][data4]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 5) {
          var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 5) {
          var hourly_url = 'hourlyanswers?filter[where][data5]=' + $scope.selectedLevel;
        }

        $scope.formattedDate = $filter('date')(newValue, "MM/dd/yyyy");

        Restangular.all(url).getList().then(function (questions) {
          $scope.surveyquestions = questions;

          Restangular.all(hourly_url).getList().then(function (hanswers) {
            $scope.hourlyanswers = hanswers;

            angular.forEach($scope.surveyquestions, function (question, index) {
              var data01 = $scope.hourlyanswers.filter(function (arr) {
                return arr.questionid == question.id && arr.workdate == $scope.formattedDate
              });

              angular.forEach($scope.timeOptions, function (time, index) {
                question[time.hour + 'show'] = true;
                angular.forEach(data01, function (dat, index) {
                  question[time.hour] = dat[time.hour];
                });
              });
            });
          });
        });
      }
    });

    $scope.orgLevelChange = function (index, level) {
      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
        }
      });
      $scope.selectedData = index;
      $scope.selectedLevel = level;
      $scope.hourly.date = new Date();
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
      return (mode === 'day' && (date.getDay() === 0));
    };

    $scope.toggleMin = function () {
      $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event, index) {
      $event.preventDefault();
      $event.stopPropagation();

      $timeout(function () {
        $('#datepicker' + index).focus();
      });
      $scope.opened = true;
    };

    $scope.dateOptions = {
      'year-format': 'yy',
      'starting-day': 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end

    $scope.stakeholderdataModal1 = false;
    $scope.stakeholderdataModal2 = false;
    $scope.stakeholderdataModal3 = false;

    $scope.clickAnswer = function (surveyquestion, data) {
      if (surveyquestion.questiontype == 7) {
        $scope.stakeholderdataModal1 = true;
        $scope.modalTitle = 'Date';
        $scope.dateDisplay = $filter('date')(data, "dd-MM-yyyy hh:mm a");
      } else if (surveyquestion.questiontype == 8) {
        $scope.stakeholderdataModal2 = true;
        $scope.modalTitle = 'Location';
        var lngLng = data.split(',');
        var myLat = parseInt(lngLng[0]);
        var myLng = parseInt(lngLng[1]);

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: new google.maps.LatLng(myLat, myLng),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(myLat, myLng),
          map: map,
        });

      } else if (surveyquestion.questiontype == 9) {
        $scope.stakeholderdataModal3 = true;
        $scope.modalTitle = 'Image';
        $scope.imageSource = '//citly.s3.ap-south-1.amazonaws.com/' + data;
      }
    };
  });