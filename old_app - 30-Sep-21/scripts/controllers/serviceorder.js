'use strict';

angular.module('secondarySalesApp')
  .controller('ServiceOrderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/

    console.log("Service Order");
    $scope.showButton = false;
    $scope.hidesearchDetail = true;

    //------- for xls export ------------------------------------------
    $scope.rcdetailsDisplay = Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().$object;

    $scope.cityDisplay = Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().$object;
    $scope.statusDisplay = Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().$object;
    $scope.userDisplay = Restangular.all('users?filter[where][deleteflag]=false').getList().$object;
    $scope.updatetypeDisplay = Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().$object;
    $scope.serviceeventDisplay = Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().$object;
    //Restangular.all('serviceevents?filter[where][deleteflag]=false').getList().then(function (serviceeventresp) {
    //Restangular.all('updatetypes?filter[where][deleteflag]=false').getList().then(function (updatetyperesp) {
    //Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (engineer) {
    //Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (todostatusresp) {

    //------- end of for xls export ------------------------------------------

    $scope.rccodes = Restangular.all('surveyquestions?filter[where][deleteflag]=false').getList().$object;
    //console.log($scope.rccodes, "$scope.rccodes-15")

    $scope.customers = Restangular.all('customers?filter[where][deleteFlag]=false').getList().$object;

    $scope.statuses = Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().$object;

    $scope.getRCcode = function (questionId) {
      return Restangular.one('surveyquestions/findOne?filter[where][serialno]=' + questionId).get().$object;
    };

    $scope.hideSearchdetail = function () {
      $scope.hidesearchDetail = true;
      $scope.showButton = false;

    };

    $scope.Searchdetail = function () {
      $scope.hidesearchDetail = false;
      $scope.showButton = true;
    };

    $scope.hideTable = false;
    $scope.showtopIcon = true;


    $scope.hideclkTable = function () {
      console.log("hideclkTable");
      $scope.showtopIcon = false;
      $scope.hideTable = true;

    };

    $scope.showclkTable = function () {
      console.log("showclkTable");
      $scope.showtopIcon = true;
      $scope.hideTable = false;

    };
    // ------------New code---------------
    $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    $scope.CountrySplit = $scope.orgStructureSplit[0];
    $scope.ZoneSplit = $scope.orgStructureSplit[1];
    $scope.intermediateOrgStructure = "";
    for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
      if ($scope.intermediateOrgStructure === "") {
        $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
      } else {
        $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
      }
    }

    $scope.orgStructureFilterArray = [];
    if ($scope.orgStructureSplit.length > 0) {
      $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
      for (var i = 0; i < $scope.LocationsSplit.length; i++) {
        $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
      }

    }
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));

    //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
    $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
    console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
    //--- End of No need now - It was for single state & multi city ---------

//-- For Multi Zone, state & multi City array filter work Ravi ---------------------------------
  $scope.orgStructureZoneFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
    for (var i = 0; i < $scope.ZoneSplit.length; i++) {
      $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[0] + "-" + $scope.ZoneSplit[i]);
    }
  }

  //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
  $scope.orgStructureStateFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
  //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    console.log($scope.StatesSplit, "$scope.StatesSplit-461")
   // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
   for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
    for (var i = 0; i < $scope.StatesSplit.length; i++) {
      // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
      $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    }
   }
  }

    //-- For Multi state & multi City array filter work Satyen ---------------------------------
    // $scope.orgStructureStateFilterArray = [];
    // // if ($scope.orgStructureSplit.length > 0) {
    //   if ($scope.orgStructureSplit.length > 1) {
    //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //    // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //    $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //   }
    // }
    //console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-468")
    $scope.orgStructureFinalFilterArray = [];
    if ($scope.orgStructureStateFilterArray.length > 0) {
      for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
        for (var i = 0; i < $scope.LocationsSplit.length; i++) {
          $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
        }
      }
    }
    //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", "");
    //console.log("$scope.orgStructureFinalFilterArray", JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", ""));

    //-- End of For Multi state & multi City array filter work Satyen ---------------------------------

    $scope.$watch('customerId', function (newValue, oldValue) {
      if (newValue == '' || newValue == undefined || newValue == null) {
        return;
      } else {
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (rcdetailsresp) {
            $scope.rcdetails = rcdetailsresp;
            console.log($scope.rcdetails, "RC-RESP-10")

            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;

            });

          });
        } else if ($window.sessionStorage.roleId == 8) {
          Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (rcdetailsresp) {
            $scope.rcdetails = rcdetailsresp;
            console.log($scope.rcdetails, "RC-RESP-10")

            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;

            });

          });
        } else if ($window.sessionStorage.roleId == 7 || $window.sessionStorage.roleId == 15) {
          var str1 = $scope.orgStructureFilter;
          var strOrgstruct1 = str1.split('"');
          $scope.strOrgstructFinals = [];
          for (var y = 0; y < strOrgstruct1.length; y++) {
            if (y % 2 != 0) {
              $scope.strOrgstructFinals.push(strOrgstruct1[y]);
            }
          }
          $scope.membersFinals = [];
          for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
            Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
              // $scope.rcdetails = rcdetailsresp;
              $scope.membersnew = rcdetailsresp;
              if ($scope.membersnew.length > 0) {
                angular.forEach($scope.membersnew, function (value, index) {
                  $scope.membersFinals.push(value);
                });
              }
              $scope.rcdetails = $scope.membersFinals;
              angular.forEach($scope.rcdetails, function (member, index) {
                member.index = index + 1;

              });

            });
          }
        } else if ($window.sessionStorage.roleId == 5) {
          var str1 = $scope.orgStructureUptoStateFilter;
          var strOrgstruct1 = str1.split('"');
          $scope.strOrgstructFinals = [];
          for (var y = 0; y < strOrgstruct1.length; y++) {
            if (y % 2 != 0) {
              $scope.strOrgstructFinals.push(strOrgstruct1[y]);
            }
          }
          $scope.membersFinals = [];
          for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
            Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
              // $scope.rcdetails = rcdetailsresp;
              $scope.membersnew = rcdetailsresp;
              if ($scope.membersnew.length > 0) {
                angular.forEach($scope.membersnew, function (value, index) {
                  $scope.membersFinals.push(value);
                });
              }
              $scope.rcdetails = $scope.membersFinals;
              console.log("Ticket details for Customer category => ", $scope.rcdetails);

              angular.forEach($scope.rcdetails, function (member, index) {
                member.index = index + 1;

              });

            });
          }
        }
      }
    });

    $scope.checkedcloseChange = function (value) {

      $scope.serchdetails.closeFlag = value;
      //console.log($scope.serchdetails.closeFlag, "$scope.serchdetails.closeFlag-193")

    };

    $scope.serchdetailsArray = [];
    $scope.goSearch = function () {
      console.log("gosearch Function");
      $scope.membersFilterFinals = [];
      // $scope.checkedcloseChange = function (value) {

      //   $scope.serchdetails.closeFlag = value;

      // };
      //forfilter wise export purpose -----
      $scope.exportFilterFinals = [];
      //-----------------------------------
      console.log("serachdetails", $scope.serchdetails);
      $scope.DisableExport = false;
      $scope.DellExport = false;

      // if ($window.sessionStorage.roleId == 1) {
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {

        $scope.gorestURL = '';
        $scope.gofilterAdmindata();

        $scope.DisableExport = true;
        $scope.DisablefilterExport = false;
        console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-214")
        //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-224")
        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        Restangular.all($scope.gorestURL).getList().then(function (rcdetailsresp) {
          $scope.rcdetails = rcdetailsresp;
          console.log($scope.rcdetails, "RC-RESP-10");

          angular.forEach($scope.rcdetails, function (member, index) {
            member.index = index + 1;

          });
          $scope.exportFilterFinals.push($scope.rcdetails);
        });
        //});

        // //---- Single field search ---------------------------
        // if ($scope.serchdetails.rcstatus != undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined) {
        //   console.log("IF Part-213");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-214")
        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-224")
        //   // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });
        //   //});
        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.servicetag == undefined) {
        //   console.log("ELSE IF Part for dispatchno");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     //$scope.exportFilterDataNew($scope.rcdetails);
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.servicetag != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.contactno == undefined) {
        //   console.log("ELSE IF Part for servicetag");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.customercontactname != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined) {
        //   console.log("ELSE IF Part for customercontactname");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customercontactname][like]=%' + $scope.serchdetails.customercontactname + '%').getList().then(function (rcdetailsresp) {
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customercontactname]=' + $scope.serchdetails.customercontactname).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.customercontactname == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customerno != undefined) {
        //   console.log("ELSE IF Part for customerno");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerno]=' + $scope.serchdetails.customerno).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.customercontactname == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customercontactphno != undefined) {
        //   console.log("ELSE IF Part for customercontactno");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customercontactphno]=' + $scope.serchdetails.customercontactphno).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.contactno == undefined) {
        //   console.log("ELSE IF Part for CUSTOMER NAME");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno != undefined) {
        //   console.log("ELSE IF Part for CONTACT NO");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][contactno]=' + $scope.serchdetails.contactno).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.status != undefined) {
        //   console.log("ELSE IF Part for STATUS");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for ENGINEER");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     console.log(rcenggresp, "USERS");
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id).getList().then(function (rcdetailsresp) {
        //       $scope.rcdetails = rcdetailsresp;

        //       console.log($scope.rcdetails, "RC-RESP-10");

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   });

        // } else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.closeFlag == true) {
        //   console.log("ELSE IF Part for Closed");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + 15).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "RC-RESP-10");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.customerName != undefined) {
        //   console.log("ELSE IF Part for Customer Name");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername][like]=%' + $scope.serchdetails.customerName + '%').getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log("Filter customer name", $scope.rcdetails);

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });

        // } else if ($scope.serchdetails.servicePostalCode != undefined) {
        //   console.log("ELSE IF Part for Service Postal Code");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.one('organisationlocations?filter[where][deleteflag]=false&filter[where][name]=' + $scope.serchdetails.servicePostalCode).get().then(function (rcpinresp) {
        //     console.log(rcpinresp, "PIN");
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][pincodeId]=' + rcpinresp[0].id).getList().then(function (rcdetailsresp) {
        //       $scope.rcdetails = rcdetailsresp;
        //       console.log("Filter Service Postal Code", $scope.rcdetails);

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   });
        // }

        // //---- end of Single field search ---------------------------

        // //  $scope.membersFilterFinals = []; defined above
        // //---- dispatchno in combination with one and more fields
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno & rcstatus search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-465")
        //   // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "dispatchno with RcCode-421");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });
        //   //});

        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno & service tag search");
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "dispatchno with service tag-429");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });
        //   // });

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-503")
        //   // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus).getList().then(function (rcdetailsresp) {
        //     $scope.rcdetails = rcdetailsresp;
        //     console.log($scope.rcdetails, "dispatchno with RcCode-421");

        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });
        //   //});

        // }

        // //--------- end of dispatchno in combination with one and more fields
        // else {
        //   $scope.DisableExport = false;
        //   // $scope.DisablefilterExport = true;
        //   $scope.DisablefilterExport = false;
        //   console.log("Else part for all Data");
        //   Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (Part1) {
        //     $scope.rcdetails = Part1;
        //     angular.forEach($scope.rcdetails, function (member, index) {
        //       member.index = index + 1;

        //     });
        //     $scope.exportFilterFinals.push($scope.rcdetails);
        //   });
        // }

      } 
      else if ($window.sessionStorage.roleId == 8) {
        $scope.gorestURL = '';
       // $scope.gofilterAdmindata();
       $scope.gofilterdata();
        $scope.DisableExport = true;
        $scope.DisablefilterExport = false;
        console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-214")
        //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-224")
        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        Restangular.all($scope.gorestURL + $window.sessionStorage.orgStructure + '%').getList().then(function (rcdetailsresp) {
          $scope.rcdetails = rcdetailsresp;
          console.log($scope.rcdetails, "RC-RESP-10");

          angular.forEach($scope.rcdetails, function (member, index) {
            member.index = index + 1;

          });
          $scope.exportFilterFinals.push($scope.rcdetails);
        });
      }
      else if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
        // from here new code ---
        // For Role 7 & 15 ------
        var str1 = $scope.orgStructureFilter;
        var strOrgstruct1 = str1.split('"');
        $scope.strOrgstructFinals = [];
        for (var y = 0; y < strOrgstruct1.length; y++) {
          if (y % 2 != 0) {
            $scope.strOrgstructFinals.push(strOrgstruct1[y]);
          }
        }
        $scope.membersFinals = [];
        $scope.gorestURL = '';

        $scope.gofilterdata();

        console.log($scope.gorestURL, "$scope.gorestURL-523")
        //---- Single field search ---------------------------
        // if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("IF Part");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-550")

        for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
          // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {

          // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
          Restangular.all($scope.gorestURL + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
            // $scope.rcdetails = rcdetailsresp;
            $scope.membersnew = rcdetailsresp;
            //console.log($scope.membersnew, "RC-RESP-10");
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinals.push(value);
              });
            }
            $scope.rcdetails = $scope.membersFinals;
            //console.log("Ticket details for RCstatus => ", $scope.rcdetails);
            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;

            });
            $scope.exportFilterFinals.push($scope.rcdetails);
          });
        }
        //} 


        // for Senior Manager ----------------------------------------
      } else if ($window.sessionStorage.roleId == 5) {
        // from here new code ---
        var str1 = $scope.orgStructureUptoStateFilter;
        var strOrgstruct1 = str1.split('"');
        $scope.strOrgstructFinals = [];
        for (var y = 0; y < strOrgstruct1.length; y++) {
          if (y % 2 != 0) {
            $scope.strOrgstructFinals.push(strOrgstruct1[y]);
          }
        }
        $scope.membersFinals = [];
        $scope.gorestURL = '';

        $scope.gofilterdata();

        console.log($scope.gorestURL, "$scope.gorestURL-574")

        for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
          // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {

          // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
          Restangular.all($scope.gorestURL + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
            // $scope.rcdetails = rcdetailsresp;
            $scope.membersnew = rcdetailsresp;
            //console.log($scope.membersnew, "RC-RESP-10");
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinals.push(value);
              });
            }
            $scope.rcdetails = $scope.membersFinals;
            //console.log("Ticket details for RCstatus => ", $scope.rcdetails);
            angular.forEach($scope.rcdetails, function (member, index) {
              member.index = index + 1;

            });
            $scope.exportFilterFinals.push($scope.rcdetails);
          });
        }

        // //----------------------------------------------------------
        // if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //if ($scope.serchdetails.rcstatus != undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined) {
        //   //console.log("IF Part for senior manager");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   //  console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-991")

        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       // console.log($scope.membersnew, "RC-RESP-10");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;
        //       //  console.log("Ticket details for RCstatus => ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        //   //});
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined) {
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;

        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;
        //       // console.log("Ticket details for dispatchno => ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.servicetag != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.contactno == undefined) {
        //   //console.log("Role 5 Part for servicetag");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;

        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;
        //       // console.log("Ticket details for CUSTOMER NAME => ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // }
        // // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        // // //else if ($scope.serchdetails.customercontactname != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined) {
        // //   //console.log("Role 5 Part for CUSTOMER NAME");
        // //   $scope.DisableExport = true;
        // //   $scope.DisablefilterExport = false;

        // //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        // //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customercontactname]=' + $scope.serchdetails.customercontactname + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        // //       // $scope.rcdetails = rcdetailsresp;
        // //       $scope.membersnew = rcdetailsresp;

        // //       if ($scope.membersnew.length > 0) {
        // //         angular.forEach($scope.membersnew, function (value, index) {
        // //           $scope.membersFinals.push(value);
        // //         });
        // //       }
        // //       $scope.rcdetails = $scope.membersFinals;
        // //       console.log("Ticket details for CUSTOMER NAME => ", $scope.rcdetails);
        // //       angular.forEach($scope.rcdetails, function (member, index) {
        // //         member.index = index + 1;

        // //       });
        // //       $scope.exportFilterFinals.push($scope.rcdetails);
        // //     });
        // //   }
        // // }
        // //  else if ($scope.serchdetails.customercontactname == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customerno != undefined) {
        // //   //console.log("for role 7 & 15 Part for customerno-537");
        // //   $scope.DisableExport = true;
        // //   $scope.DisablefilterExport = false;

        // //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        // //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customerno]=' + $scope.serchdetails.customerno + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        // //       // $scope.rcdetails = rcdetailsresp;
        // //       $scope.membersnew = rcdetailsresp;

        // //       if ($scope.membersnew.length > 0) {
        // //         angular.forEach($scope.membersnew, function (value, index) {
        // //           $scope.membersFinals.push(value);
        // //         });
        // //       }
        // //       $scope.rcdetails = $scope.membersFinals;
        // //       //console.log("Ticket details for CUSTOMER NAME => ", $scope.rcdetails);
        // //       angular.forEach($scope.rcdetails, function (member, index) {
        // //         member.index = index + 1;

        // //       });
        // //       $scope.exportFilterFinals.push($scope.rcdetails);
        // //     });
        // //   }

        // // } 
        // // else if ($scope.serchdetails.customercontactname == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customercontactphno != undefined) {
        // //   //console.log("ELSE IF Part for customercontactno");
        // //   $scope.DisableExport = true;
        // //   $scope.DisablefilterExport = false;

        // //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        // //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customercontactphno]=' + $scope.serchdetails.customercontactphno + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        // //       // $scope.rcdetails = rcdetailsresp;
        // //       $scope.membersnew = rcdetailsresp;

        // //       if ($scope.membersnew.length > 0) {
        // //         angular.forEach($scope.membersnew, function (value, index) {
        // //           $scope.membersFinals.push(value);
        // //         });
        // //       }
        // //       $scope.rcdetails = $scope.membersFinals;
        // //       //console.log("Ticket details for CUSTOMER NAME => ", $scope.rcdetails);
        // //       angular.forEach($scope.rcdetails, function (member, index) {
        // //         member.index = index + 1;

        // //       });
        // //       $scope.exportFilterFinals.push($scope.rcdetails);
        // //     });
        // //   }

        // // } 
        // // else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno != undefined) {
        // //   $scope.DisableExport = true;
        // //   $scope.DisablefilterExport = false;

        // //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        // //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][contactno]=' + $scope.serchdetails.contactno + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        // //       // $scope.rcdetails = rcdetailsresp;
        // //       $scope.membersnew = rcdetailsresp;
        // //       if ($scope.membersnew.length > 0) {
        // //         angular.forEach($scope.membersnew, function (value, index) {
        // //           $scope.membersFinals.push(value);
        // //         });
        // //       }
        // //       $scope.rcdetails = $scope.membersFinals;
        // //       console.log("Ticket details for CONTACT NO => ", $scope.rcdetails);
        // //       angular.forEach($scope.rcdetails, function (member, index) {
        // //         member.index = index + 1;

        // //       });
        // //       $scope.exportFilterFinals.push($scope.rcdetails);
        // //     });
        // //   }
        // // } 
        // else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.status != undefined) {
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;

        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;
        //       //console.log("Tickets for status => ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   //else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.engineer != undefined) {

        //   //console.log("ELSE IF Part for ENGINEER");
        //   // $scope.DisableExport = true;
        //   // $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");

        //     if (rcenggresp.length > 0) {
        //       $scope.DisableExport = true;
        //       $scope.DisablefilterExport = false;
        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFinals.push(value);
        //             });
        //           }
        //           $scope.rcdetails = $scope.membersFinals;
        //           //  //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       $scope.rcdetails = $scope.membersFinals;
        //     }
        //   });

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.customerName != undefined) {
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername][like]=%' + $scope.serchdetails.customerName + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[n] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;

        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;
        //       //  console.log("Tickets for status Customer Name=> Senior Manager ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.servicePostalCode != undefined) {

        //   //console.log("ELSE IF Part for ENGINEER");
        //   // $scope.DisableExport = true;
        //   // $scope.DisablefilterExport = false;

        //   Restangular.one('organisationlocations?filter[where][deleteflag]=false&filter[where][name]=' + $scope.serchdetails.servicePostalCode).get().then(function (rcpinresp) {
        //     console.log(rcpinresp, "USERS");

        //     if (rcpinresp.length > 0) {
        //       $scope.DisableExport = true;
        //       $scope.DisablefilterExport = false;
        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][pincodeId]=' + rcpinresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFinals.push(value);
        //             });
        //           }
        //           $scope.rcdetails = $scope.membersFinals;
        //           //  console.log("Tickets for SERVICE POSTAL CODE => Senior Manager ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       $scope.rcdetails = $scope.membersFinals;
        //     }
        //   });

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined && $scope.serchdetails.closeFlag == true) {
        //   //else if ($scope.serchdetails.rcstatus == undefined && $scope.serchdetails.dispatchno == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.contactno == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.engineer == undefined && $scope.serchdetails.closeFlag == true) {
        //   console.log("ELSE IF Part for Closed");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   for (var n = 0; n < $scope.strOrgstructFinals.length; n++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + 15 + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       // $scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;

        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinals;

        //       //console.log("Tickets for Closed => ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // }
        // //---- end of Single field search and Closed ---------------------------
        // //  $scope.membersFilterFinals = []; defined above
        // //---- Two field search ----------------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno & rcstatus search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-852")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        //   //});

        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno & service tag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       // console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno & customerName search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //  console.log("ELSE IF Part for dispatchno & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   // console.log("ELSE IF Part for dispatchno & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   //  console.log("ELSE IF Part for dispatchno & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //  console.log("ELSE IF Part for customerName & rcstatus search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //  console.log("ELSE IF Part for customerName & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   // console.log("ELSE IF Part for customerName & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //  console.log("ELSE IF Part for customerName & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   // console.log("ELSE IF Part for customerName & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   // console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   //  console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   //  console.log("ELSE IF Part for rcstatus & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for rcstatus & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   //else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.customercontactname == undefined && $scope.serchdetails.customerno == undefined && $scope.serchdetails.customercontactphno == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-909")
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][rcCode]=' + rcststusresp.rcCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }

        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //---- End of Two field Search -------------------------------------
        // //---- Three field Search ------------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName & rcstatus search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for rcstatus, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for rcstatus, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for status, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //---- End of Three field Search -----------------------------------

        // //---- Four field Search -------------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus & status search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, servicePostalCode and servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, status, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status & servicePostalCode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for rcstatus, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for rcstatus, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //---- End of Four field Search ----------------------------------

        // //---- Five field Search -----------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus, status & servicepostalcode search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus, status & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus, status & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for rcstatus, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //---- End of Five field Search -----------------------------------

        // //--- Six field Search --------------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer == undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus, status, servicePostalCode & servicetag search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;
        //   // Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //   //   console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //   for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //       //$scope.rcdetails = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with service tag-737");
        //       $scope.membersnew = rcdetailsresp;
        //       //console.log($scope.rcdetails, "dispatchno with RcCode-712");
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFilterFinals.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFilterFinals;

        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, rcstatus, status, servicePostalCode & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, rcstatus, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for dispatchno, customerName, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // } else if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //----- End of Six field Search -----------------------------------

        // //----- Seven field Search ----------------------------------------
        // else if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName != undefined && $scope.serchdetails.rcstatus != undefined && $scope.serchdetails.status != undefined && $scope.serchdetails.servicePostalCode != undefined && $scope.serchdetails.servicetag != undefined && $scope.serchdetails.engineer != undefined) {
        //   console.log("ELSE IF Part for customerName, rcstatus, status, servicePostalCode, servicetag & engineer search");
        //   $scope.DisableExport = true;
        //   $scope.DisablefilterExport = false;

        //   Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
        //     //console.log(rcenggresp, "USERS");
        //     if (rcenggresp.length > 0) {

        //       for (var m = 0; m < $scope.strOrgstructFinals.length; m++) {
        //         //Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //         Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][customername]=' + $scope.serchdetails.customerName + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus + '&filter[where][statusId]=' + $scope.serchdetails.status + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag + '&filter[where][assignedto]=' + rcenggresp[0].id + '&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[m] + '%').getList().then(function (rcdetailsresp) {
        //           // $scope.rcdetails = rcdetailsresp;
        //           $scope.membersnew = rcdetailsresp;
        //           if ($scope.membersnew.length > 0) {
        //             angular.forEach($scope.membersnew, function (value, index) {
        //               $scope.membersFilterFinals.push(value);
        //               //$scope.membersFinals.push(value);
        //             });
        //           }
        //           // $scope.rcdetails = $scope.membersFinals;
        //           $scope.rcdetails = $scope.membersFilterFinals;
        //           //console.log("Tickets for ENGINEER => ", $scope.rcdetails);
        //           angular.forEach($scope.rcdetails, function (member, index) {
        //             member.index = index + 1;

        //           });
        //           $scope.exportFilterFinals.push($scope.rcdetails);
        //         });
        //       }
        //     } else {
        //       // $scope.rcdetails = $scope.membersFinals;
        //       $scope.rcdetails = $scope.membersFilterFinals;
        //     }
        //   });
        // }
        // //----- End of Seven field Search ---------------------------------
        // else {
        //   $scope.DisableExport = false;
        //   // $scope.DisablefilterExport = true;
        //   $scope.DisablefilterExport = false;

        //   var str = $scope.orgStructureUptoStateFilter;
        //   var strOrgstruct = str.split('"');

        //   $scope.strOrgstructFinal = [];
        //   for (var x = 0; x < strOrgstruct.length; x++) {
        //     if (x % 2 != 0) {
        //       $scope.strOrgstructFinal.push(strOrgstruct[x]);
        //     }
        //   }
        //   $scope.membersFinal = [];
        //   for (var l = 0; l < $scope.strOrgstructFinal.length; l++) {
        //     // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (part2) {
        //     Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[l] + '%').getList().then(function (part2) {
        //       console.log('role test');
        //       // $scope.rcdetails = part2;
        //       $scope.membersnew = part2;
        //       console.log($scope.membersnew, "$scope.membersnew-901")
        //       if ($scope.membersnew.length > 0) {
        //         angular.forEach($scope.membersnew, function (value, index) {
        //           $scope.membersFinal.push(value);
        //         });
        //       }
        //       $scope.rcdetails = $scope.membersFinal;
        //       console.log("All Ticket details  for Senior Manager=> ", $scope.rcdetails);
        //       angular.forEach($scope.rcdetails, function (member, index) {
        //         member.index = index + 1;

        //       });
        //       $scope.exportFilterFinals.push($scope.rcdetails);
        //     });
        //   }
        // }
        //end for Senior Manager
      } else {
        console.log("ELSE Part");
        $scope.DisableExport = false;
        //  $scope.exportArray = [];
        Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailsresp) {
          $scope.rcdetails = rcdetailsresp;
          console.log($scope.rcdetails, "RC-RESP-10")

          angular.forEach($scope.rcdetails, function (data, index) {
            data.index = index + 1;
            // $scope.exportArray.push(member);
            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 39).getList().then(function (states) {
              $scope.statename = states;
              for (var j = 0; j < $scope.statename.length; j++) {
                if (data.stateId == $scope.statename[j].id) {
                  data.statename = $scope.statename[j].name;
                  break;
                }
              }
            });

            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 42).getList().then(function (cities) {
              $scope.cityname = cities;
              for (var j = 0; j < $scope.cityname.length; j++) {
                if (data.cityId == $scope.cityname[j].id) {
                  data.cityname = $scope.cityname[j].name;
                  break;
                }
              }
            });

            Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + 45).getList().then(function (pincodes) {
              $scope.pincodename = pincodes;
              for (var j = 0; j < $scope.pincodename.length; j++) {
                if (data.pincodeId == $scope.pincodename[j].id) {
                  data.pincodename = $scope.pincodename[j].name;
                  break;
                }
              }
            });

            Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (statuses) {
              $scope.statusname = statuses;
              for (var j = 0; j < $scope.statusname.length; j++) {
                if (data.statusId == $scope.statusname[j].id) {
                  data.statusname = $scope.statusname[j].name;
                  break;
                }
              }
            });

            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (engineer) {
              $scope.engineername = engineer;
              for (var j = 0; j < $scope.engineername.length; j++) {
                if (data.assignedto == $scope.engineername[j].id) {
                  data.engineername = $scope.engineername[j].username;
                  break;
                }
              }
            });

            // Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + data.id).getList().then(function (tktanswers) {
            //   $scope.ticketanswers = tktanswers;
            //   console.log("Ticket Answers: ", data.dispatchno, $scope.ticketanswers);

            //   data.lastaction = $scope.ticketanswers[$scope.ticketanswers.length-1].createdtime;
            // });


            $scope.exportArray.push(data);

          });

        });

        $scope.DisableExport = false;
        $scope.valConCount = 0;


      }

    };

    $scope.serchdetails = {};
    $scope.clearSerch = function () {
      $scope.serchdetails = {};
      $scope.customerId = null;

    };

    $scope.gofilterAdmindata = function () {
      console.log("Here in Admin filter data")

      $scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false'
      //&filter[where][orgstructure][like]=%';

      if ($scope.serchdetails.dispatchno != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno;
      }
      if ($scope.serchdetails.customerName != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][customername]=' + $scope.serchdetails.customerName;
      }
      if ($scope.serchdetails.rcstatus != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus;
      }
      if ($scope.serchdetails.status != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][statusId]=' + $scope.serchdetails.status;
      }
      if ($scope.serchdetails.servicePostalCode != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode;
      }
      if ($scope.serchdetails.servicetag != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag;
      }
      if ($scope.serchdetails.engineer != undefined) {
        Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
          if (rcenggresp.length > 0) {
            $scope.engineerId = rcenggresp[0].id;
          }
        });
        $scope.gorestURL = $scope.gorestURL + '&filter[where][assignedto]=' + $scope.engineerId;
      }

      // if ($scope.serchdetails.dispatchno != undefined || $scope.serchdetails.customerName != undefined || $scope.serchdetails.rcstatus != undefined || $scope.serchdetails.status != undefined || $scope.serchdetails.servicePostalCode != undefined || $scope.serchdetails.servicetag != undefined || $scope.serchdetails.engineer != undefined) {
      //   $scope.gorestURL = $scope.gorestURL + '&filter[where][orgstructure][like]=%';
      // }

      if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        $scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false';
      }

      // Restangular.all($scope.restURL).getList().then(function (part2) {

      // });

    };

    $scope.gofilterAdminDumpdata = function () {
      console.log("Here in Admin Dump filter data")

      //$scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false'
      //&filter[where][orgstructure][like]=%';
      $scope.gorestURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false'

      if ($scope.serchdetails.dispatchno != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno;
      }
      if ($scope.serchdetails.customerName != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][customername]=' + $scope.serchdetails.customerName;
      }
      if ($scope.serchdetails.rcstatus != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][reply_code]=' + $scope.serchdetails.rcstatus;
      }
      if ($scope.serchdetails.status != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][statusId]=' + $scope.serchdetails.status;
      }
      if ($scope.serchdetails.servicePostalCode != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode;
      }
      if ($scope.serchdetails.servicetag != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag;
      }
      if ($scope.serchdetails.engineer != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][engineer_name]=' + $scope.serchdetails.engineer;
      }

      // if ($scope.serchdetails.dispatchno != undefined || $scope.serchdetails.customerName != undefined || $scope.serchdetails.rcstatus != undefined || $scope.serchdetails.status != undefined || $scope.serchdetails.servicePostalCode != undefined || $scope.serchdetails.servicetag != undefined || $scope.serchdetails.engineer != undefined) {
      //   $scope.gorestURL = $scope.gorestURL + '&filter[where][orgstructure][like]=%';
      // }

      if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //$scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false';
        $scope.gorestURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false'
      }

      // Restangular.all($scope.restURL).getList().then(function (part2) {

      // });

    };

    $scope.gofilterZoneDumpdata = function () {
      console.log("Here in Zone Dump filter data")

      //$scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false'
      //&filter[where][orgstructure][like]=%';
      $scope.gorestURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%'

      if ($scope.serchdetails.dispatchno != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno;
      }
      if ($scope.serchdetails.customerName != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][customername]=' + $scope.serchdetails.customerName;
      }
      if ($scope.serchdetails.rcstatus != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][reply_code]=' + $scope.serchdetails.rcstatus;
      }
      if ($scope.serchdetails.status != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][statusId]=' + $scope.serchdetails.status;
      }
      if ($scope.serchdetails.servicePostalCode != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode;
      }
      if ($scope.serchdetails.servicetag != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag;
      }
      if ($scope.serchdetails.engineer != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][engineer_name]=' + $scope.serchdetails.engineer;
      }

      // if ($scope.serchdetails.dispatchno != undefined || $scope.serchdetails.customerName != undefined || $scope.serchdetails.rcstatus != undefined || $scope.serchdetails.status != undefined || $scope.serchdetails.servicePostalCode != undefined || $scope.serchdetails.servicetag != undefined || $scope.serchdetails.engineer != undefined) {
      //   $scope.gorestURL = $scope.gorestURL + '&filter[where][orgstructure][like]=%';
      // }

      if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        //$scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false';
        $scope.gorestURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + window.sessionStorage.orgStructure + '%'
      }

      // Restangular.all($scope.restURL).getList().then(function (part2) {

      // });

    };

    $scope.gofilterdata = function () {
      // if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
      //   $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][orgstructure][like]=%';

      // }
      // $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[$scope.SaveCount] + '%';
      // $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%';

      $scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false'
      //&filter[where][orgstructure][like]=%';

      if ($scope.serchdetails.dispatchno != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno;
      }
      if ($scope.serchdetails.customerName != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][customername]=' + $scope.serchdetails.customerName;
      }
      if ($scope.serchdetails.rcstatus != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][rcCode]=' + $scope.serchdetails.rcstatus;
      }
      if ($scope.serchdetails.status != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][statusId]=' + $scope.serchdetails.status;
      }
      if ($scope.serchdetails.servicePostalCode != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode;
      }
      if ($scope.serchdetails.servicetag != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag;
      }
      if ($scope.serchdetails.engineer != undefined) {
        Restangular.one('users?filter[where][deleteflag]=false&filter[where][username]=' + $scope.serchdetails.engineer).get().then(function (rcenggresp) {
          if (rcenggresp.length > 0) {
            $scope.engineerId = rcenggresp[0].id;
          }
        });
        $scope.gorestURL = $scope.gorestURL + '&filter[where][assignedto]=' + $scope.engineerId;
      }

      if ($scope.serchdetails.dispatchno != undefined || $scope.serchdetails.customerName != undefined || $scope.serchdetails.rcstatus != undefined || $scope.serchdetails.status != undefined || $scope.serchdetails.servicePostalCode != undefined || $scope.serchdetails.servicetag != undefined || $scope.serchdetails.engineer != undefined) {
        $scope.gorestURL = $scope.gorestURL + '&filter[where][orgstructure][like]=%';
      }
      if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        $scope.gorestURL = 'rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%';
      }

      // Restangular.all($scope.restURL).getList().then(function (part2) {

      // });

    };

    $scope.getstate = function (stateId) {
      return Restangular.one('organisationlocations', stateId).get().$object;
    };

    $scope.getcity = function (cityId) {
      return Restangular.one('organisationlocations', cityId).get().$object;
    };

    $scope.getpincode = function (pincodeId) {
      return Restangular.one('organisationlocations', pincodeId).get().$object;
    };

    $scope.getstatus = function (statusId) {
      return Restangular.one('todostatuses', statusId).get().$object;
    };

    $scope.getengineer = function (assignedto) {
      return Restangular.one('users', assignedto).get().$object;
    };

    $scope.getupdatetype = function (updatetypeId) {
      return Restangular.one('updatetypes', updatetypeId).get().$object;
    };

    $scope.getserviceevent = function (serviceeventId) {
      return Restangular.one('serviceevents', serviceeventId).get().$object;
    };

    // Export Function

    $scope.exportDataNew = function () {
      console.log("inside all export func");
      // $scope.exportcountSrMgr = 0;
      //$scope.DisableExport = true;
      //$scope.exportArray = [];
      $scope.exportAllArray = [];
      $scope.exportNewAllArray = [];
      $scope.exportFinalAllArray = [];
      // $scope.exportFinalNewAllArray = [];

      ////define role based export here -------
      // Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {
      $scope.rolebasedURL = '';
      $scope.rcdetailsall = '';
      $scope.filteredRCDetail = {};

      // if ($window.sessionStorage.roleId == 1) {
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {
        //$scope.rolebasedURL = 'rcdetails?filter[where][deleteflag]=false';
        Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {
          $scope.rcdetailsall = rcdetailresp;

          console.log($scope.rcdetailsall, "$scope.rcdetailsall-1065")

          $scope.exportAllArray.push($scope.rcdetailsall);
          var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
          $scope.exportNewAllArray = JSON.parse(old);

          //console.log($scope.exportNewAllArray, $scope.exportNewAllArray[0].length, "$scope.exportNewAllArray-1015")

          angular.forEach($scope.exportNewAllArray[0], function (data, index) {
            data.index = index + 1;

            var cityData = $scope.cityDisplay.filter(function (arr) {
              return arr.id == data.cityId
            })[0];

            if (cityData != undefined) {
              data.cityName = cityData.name;
            }

            //statusDisplay - $scope.statusDisplay
            var statusData = $scope.statusDisplay.filter(function (arr) {
              return arr.id == data.statusId
            })[0];

            if (statusData != undefined) {
              data.statusName = statusData.name;
            }

            //$scope.userDisplay
            var userData = $scope.userDisplay.filter(function (arr) {
              return arr.id == data.assignedto
            })[0];

            if (userData != undefined) {
              data.engineerName = userData.username;
            }

            //$scope.updatetypeDisplay
            var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
              return arr.id == data.producttypeid
            })[0];

            if (updatetypeData != undefined) {
              data.updatetypeName = updatetypeData.name;
            }

            //$scope.serviceeventDisplay
            var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
              return arr.id == data.serviceeventid
            })[0];

            if (serviceeventData != undefined) {
              data.serviceeventName = serviceeventData.name;
            }
            data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
            data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
            data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
            data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
            data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
            data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');

            //console.log(data, "data-1070")
            $scope.exportFinalAllArray.push(data);

          });
          //console.log($scope.exportFinalAllArray, $scope.exportFinalAllArray.length, "$scope.exportFinalAllArray-1092")

          // var rcdetailalldata = JSON.stringify($scope.exportFinalAllArray);
          // console.log(rcdetailalldata, "rcdetailalldata-1088")

          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                /* 'Sr No': $scope.exportFinalAllArray[arr].index,
                 'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                 'Customer Name': $scope.exportFinalAllArray[arr].customername,
                 'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                 'Dispatch Status': $scope.exportFinalAllArray[arr].statusName,
                 'Reply Code': $scope.exportFinalAllArray[arr].rcCode,
                 'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                 'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                 'Engineer': $scope.exportFinalAllArray[arr].engineerName,
                 'Customer No': $scope.exportFinalAllArray[arr].customerno,
                 'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                 'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                 'City': $scope.exportFinalAllArray[arr].cityName,
                 'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                 'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                 'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                 'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                 'ETA': $scope.exportFinalAllArray[arr].etatime,
                 'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                 'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                 'Update Type': $scope.exportFinalAllArray[arr].updatetypeName,
                 'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                 'Service Event': $scope.exportFinalAllArray[arr].serviceeventName,
                 'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                 'Service Event Date Time': $scope.exportFinalAllArray[arr].lastmodifiedtime */
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].statusName,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].cityName,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].rcCode,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineerName,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].updatetypeName,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].serviceeventName,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].lastmodifiedtime
              });

              $scope.valConCount++;

              //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1097")
              if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                alasql('SELECT * INTO XLSX("Alllist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
              }
            }
          }

        });

      } else if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
        var str = $scope.orgStructureFilter;
        var strOrgstruct = str.split('"');

        $scope.strOrgstructFinal = [];
        $scope.mycount = 0;
        $scope.listcount = 0;
        $scope.exportcount = 0;
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
          $scope.mycount++;
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinals.length-1215")
        $scope.myArray = [];
        $scope.membersFinal = [];
        //Below line ($scope.SaveCount = 0; $scope.pushwithDataCount = 0;) is for repeated export -- saty
        $scope.SaveCount = 0;
        $scope.pushwithDataCount = 0;
        console.log("here-1211")
        $scope.customLoop();

        //console.log($scope.exportFinalAllArray, $scope.exportFinalAllArray.length, "$scope.exportFinalAllArray-1092")

        // var rcdetailalldata = JSON.stringify($scope.exportFinalAllArray);
        // console.log(rcdetailalldata, "rcdetailalldata-1088")

      } else if ($window.sessionStorage.roleId == 5) {
        // $scope.exportcountSrMgr = 0;
        console.log("here-1382")
        //$scope.exportcountSrMgr = $scope.exportcountSrMgr + 1;

        var str1 = $scope.orgStructureUptoStateFilter;
        var strOrgstruct1 = str1.split('"');
        $scope.strOrgstructFinals = [];
        $scope.mycount = 0;
        $scope.listcount = 0;
        $scope.exportcount = 0;
        for (var y = 0; y < strOrgstruct1.length; y++) {
          if (y % 2 != 0) {
            $scope.strOrgstructFinals.push(strOrgstruct1[y]);
          }
          $scope.mycount++;
        }
        //console.log($scope.strOrgstructFinals, "$scope.strOrgstructFinals.length-1350")
        $scope.myArray = [];
        $scope.membersFinal = [];
        //Below line ($scope.SaveCountMgr = 0; $scope.pushwithDataCount1 = 0;) is for repeated export -- saty
        $scope.SaveCountMgr = 0;
        $scope.pushwithDataCount1 = 0;
        console.log("here-1352")
        $scope.customLoopSrMgr();


      };
    }
    // $scope.exportFilterDataNew = function (rcdetailsallfiltereddata) {
    $scope.exportFilterDataNew = function () {
      console.log("Filter export to xls-4299")
      //$scope.exportArray = [];
      $scope.exportAllArray = [];
      $scope.exportNewAllArray = [];
      $scope.exportFinalAllArray = [];
      //console.log(rcdetailsallfiltereddata, "rcdetailsallfiltereddata-1659")

      //Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {
      // $scope.rcdetailsall = rcdetailresp;

      //$scope.rcdetailsall = rcdetailsallfiltereddata;
      $scope.rcdetailsall = $scope.exportFilterFinals[0];

      //console.log($scope.rcdetailsall, "$scope.rcdetailsall-1067")

      $scope.exportAllArray.push($scope.rcdetailsall);
      var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
      $scope.exportNewAllArray = JSON.parse(old);

      //console.log($scope.exportNewAllArray, $scope.exportNewAllArray[0].length, "$scope.exportNewAllArray-1015")

      angular.forEach($scope.exportNewAllArray[0], function (data, index) {
        data.index = index + 1;

        var cityData = $scope.cityDisplay.filter(function (arr) {
          return arr.id == data.cityId
        })[0];

        if (cityData != undefined) {
          data.cityName = cityData.name;
        }

        //statusDisplay - $scope.statusDisplay
        var statusData = $scope.statusDisplay.filter(function (arr) {
          return arr.id == data.statusId
        })[0];

        if (statusData != undefined) {
          data.statusName = statusData.name;
        }

        //$scope.userDisplay
        var userData = $scope.userDisplay.filter(function (arr) {
          return arr.id == data.assignedto
        })[0];

        if (userData != undefined) {
          data.engineerName = userData.username;
        }

        //$scope.updatetypeDisplay
        var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
          return arr.id == data.producttypeid
        })[0];

        if (updatetypeData != undefined) {
          data.updatetypeName = updatetypeData.name;
        }

        //$scope.serviceeventDisplay
        var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
          return arr.id == data.serviceeventid
        })[0];

        if (serviceeventData != undefined) {
          data.serviceeventName = serviceeventData.name;
        }

        data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
        data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
        data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
        data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
        data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mm:ssa');

        //console.log(data, "data-1070")
        $scope.exportFinalAllArray.push(data);

      });
      //console.log($scope.exportFinalAllArray, $scope.exportFinalAllArray.length, "$scope.exportFinalAllArray-1092")

      // var rcdetailalldata = JSON.stringify($scope.exportFinalAllArray);
      // console.log(rcdetailalldata, "rcdetailalldata-1088")

      $scope.TotalData = [];
      $scope.valConCount = 0;

      if ($scope.exportFinalAllArray.length == 0) {
        alert('No data found');
      } else {
        for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
          $scope.TotalData.push({
            'Sr No': $scope.exportFinalAllArray[arr].index,
            'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
            'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
            'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
            'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
            'Transport': $scope.exportFinalAllArray[arr].transport,
            'Dispatch Status': $scope.exportFinalAllArray[arr].statusName,
            'LOB': $scope.exportFinalAllArray[arr].lob,
            'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
            'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
            'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
            'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
            'Call Type': $scope.exportFinalAllArray[arr].calltype,
            'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
            'Customer Name': $scope.exportFinalAllArray[arr].customername,
            'Customer Number': $scope.exportFinalAllArray[arr].customerno,
            'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
            'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
            'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
            'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
            'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
            'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
            'City': $scope.exportFinalAllArray[arr].cityName,
            'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
            'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
            'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
            'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
            'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
            'Part Number': $scope.exportFinalAllArray[arr].partnumber,
            'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
            'Part Status': $scope.exportFinalAllArray[arr].partstatus,
            'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
            'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
            'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
            'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
            'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
            'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
            'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
            'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
            'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
            'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
            'Reply Code': $scope.exportFinalAllArray[arr].rcCode,
            'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
            'Engineer Name': $scope.exportFinalAllArray[arr].engineerName,
            'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
            'ETA': $scope.exportFinalAllArray[arr].etatime,
            'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
            'End Date Time': $scope.exportFinalAllArray[arr].endtime,
            'Update Type': $scope.exportFinalAllArray[arr].updatetypeName,
            'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
            'Service Event': $scope.exportFinalAllArray[arr].serviceeventName,
            'ASP Name': $scope.exportFinalAllArray[arr].aspname,
            'Service Event Date Time': $scope.exportFinalAllArray[arr].lastmodifiedtime

          });

          $scope.valConCount++;

          //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1097")
          if ($scope.exportFinalAllArray.length == $scope.valConCount) {
            alasql('SELECT * INTO XLSX("Exportlist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
          }
        }
      }

      //});
    }
    //$scope.exportDellArray = [];
    $scope.exportDellData = function () {
      console.log("inside Dell export func");
      //$scope.exportArray = [];
      //console.log($scope.exportArray, "$scope.exportArray-1046")
      $scope.exportDellArray = [];
      $scope.exportNewDellArray = [];
      $scope.exportFinalDellArray = [];
      //----- for date filter
      $scope.exportdatefilterDellArray = [];
      $scope.exportdatefilterNewDellArray = [];
      //  $scope.selectedAnswers = ["CALL ASSIGNED TO ENGINEER-10","CALL TAKEN BY ENGINEER-17","WAITING FOR PART ETA/SDT-18","CONFIRM DILIVERY STATUS PART/PC-20"];

      if ($window.sessionStorage.roleId == 1) {

        console.log($scope.serchdetails.fromdate, "serchdetails.fromdate-1849")
        console.log($scope.serchdetails.todate, "serchdetails.todate-1850")

        // Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailsresp) {
        //  Restangular.all('surveyanswers?filter[where][deleteflag]=false').getList().then(function (rcdetailsresp) {
        // if ($scope.serchdetails.fromdate != null || $scope.serchdetails.fromdate != undefined || $scope.serchdetails.fromdate != '' || $scope.serchdetails.todate != null || $scope.serchdetails.todate != undefined || $scope.serchdetails.todate != '') {
        if ($scope.serchdetails.fromdate != undefined || $scope.serchdetails.todate != undefined) {

          if ($scope.serchdetails.fromdate == null || $scope.serchdetails.fromdate == undefined || $scope.serchdetails.fromdate == '') {
            //$scope.serchdetails.fromdate = null;
            //$scope.validatestring = $scope.validatestring + 'Please Select From Date';
            alert('Please select From date..');
          } else if ($scope.serchdetails.todate == null || $scope.serchdetails.todate == undefined || $scope.serchdetails.todate == '') {
            //$scope.serchdetails.fromdate = null;
            //$scope.validatestring = $scope.validatestring + 'Please Select From Date';
            alert('Please select To date..');
          } else if ($scope.serchdetails.todate < $scope.serchdetails.fromdate) {
            alert('"To Date Time" cannot be less than "From Date Time.."');
          } else {
            //------------ my -----------------
            console.log("inside date filter export func");
            // var date = new Date();
            // $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

            //console.log($scope.FromDate, "CURRENT DATE - 56")
            //var todaydate = $scope.FromDate + "T00:00:00";

            //$scope.FromDate = '2020-04-15'; //e.g.
            // console.log($scope.serchdetails.fromdate, "serchdetails.fromdate-1859")
            // console.log($scope.serchdetails.todate, "serchdetails.todate-1860")

            // var fromdate = $scope.FromDate + "T00:00:00";
            // var todate = $scope.FromDate + "T23:59:00";
            // console.log(fromdate, todate, "Between Dates-62")

            //console.log($window.sessionStorage.orgStructure, "ORG-STRUCTURE-59")

            Restangular.all('surveyanswers?filter={"where":{"and":[{"answer":{"inq":["CALLED CUSTOMER UNABLE TO SET ETA-15","DSP TO FOLLOW UP-39","CATASTROPE-38","ETA FIXED WITH CUSTOMER WITHIN SLA-16","CUSTOMER REQUEST OUTSIDE SLA-11","SYSTEM FIXED-53","CALL CLOSED-93","CALL CANCELLED BEFORE ENGINEER REACHING SITE-81","CALL CANCELLED WHILE ENGINEER ONSITE-84"]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (rcdetailsnewresp) {
              $scope.rcdetailsnewdell = rcdetailsnewresp;
              console.log($scope.rcdetailsnewdell, $scope.rcdetailsnewdell.length, "RC-RESP-1894")

              // Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][lastmodifiedtime][between]=' + fromdate + '&filter[where][lastmodifiedtime][between]=' + todate).getList().then(function (surveyresp) {
              Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][lastmodifiedtime][between]=' + $scope.serchdetails.fromdate + '&filter[where][lastmodifiedtime][between]=' + $scope.serchdetails.todate).getList().then(function (surveyresp) {
                console.log(surveyresp, "surveyresp-1874");
                $scope.tktdatefilterArray = surveyresp;

                angular.forEach($scope.rcdetailsnewdell, function (member, index) {
                  //console.log(index, "index-1898")
                  //member.index = index + 1;
                  for (var i = 0; i < $scope.tktdatefilterArray.length; i++) {
                    if (member.id == $scope.tktdatefilterArray[i].id) {
                      $scope.exportdatefilterDellArray.push($scope.tktdatefilterArray[i]);
                      break;
                    }
                  }
                });

                console.log($scope.exportdatefilterDellArray, "$scope.exportdatefilterDellArray-1907")

                var old = JSON.stringify($scope.exportdatefilterDellArray).replace(/null/g, '""');
                // $scope.exportNewDellArray = JSON.parse(old);
                $scope.exportdatefilterNewDellArray = JSON.parse(old);
                console.log($scope.exportdatefilterNewDellArray, $scope.exportdatefilterNewDellArray.length, "$scope.exportdatefilterNewDellArray-1914")
                //console.log($scope.exportdatefilterDellArray.length, "$scope.exportdatefilterDellArray-1915")


                //angular.forEach($scope.exportdatefilterDellArray, function (member, index) {
                angular.forEach($scope.exportdatefilterNewDellArray, function (member, index) {
                  member.index = index + 1;
                  //console.log(member.index, " member.index-1886")

                  //$scope.DispatchnoDisplay
                  var dispatchnoNewData = $scope.rcdetailsDisplay.filter(function (arr) {
                    return arr.id == member.memberid
                  })[0];

                  var olddispatchData = JSON.stringify(dispatchnoNewData).replace(/null/g, '""');
                  var dispatchnoData = JSON.parse(olddispatchData);

                  if (dispatchnoData != undefined) {
                    member.dispatchNumber = dispatchnoData.dispatchno;
                    // member.replyCode = dispatchnoData.rcCode;
                    member.waybillNo = dispatchnoData.waybillno;
                    member.partNumber = dispatchnoData.partnumber;
                    member.partQuantity = dispatchnoData.partquantity;
                    member.modelDescription = dispatchnoData.modeldescription;
                    member.closureDate = dispatchnoData.closuredate;
                  }

                  //$scope.userDisplay
                  // var userData = $scope.userDisplay.filter(function (arr) {
                  //   return arr.id == member.assignedto
                  // })[0];

                  // if (userData != undefined) {
                  //   member.engineerName = userData.username;
                  // }

                  var userData = $scope.userDisplay.filter(function (arr) {
                    return arr.id == dispatchnoData.assignedto
                  })[0];

                  if (userData != undefined) {
                    member.engineerName = userData.username;
                  }

                  //$scope.updatetypeDisplay
                  // var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
                  //   return arr.id == member.producttypeid
                  // })[0];
                  var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
                    return arr.id == member.updatetypeid
                  })[0];

                  if (updatetypeData != undefined) {
                    member.updatetypeName = updatetypeData.name;
                  }

                  //$scope.serviceeventDisplay
                  var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
                    return arr.id == member.serviceeventid
                  })[0];

                  if (serviceeventData != undefined) {
                    member.serviceeventName = serviceeventData.name;
                  }
                  var replyAnsCode = member.answer.split("-");
                  member.replyCode = replyAnsCode[1];
                  //console.log("ReplyCode ", member.replyCode);
                  member.lastmodifiedtime = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy h:mm:ssa');
                  member.warrantyinvoicedate = $filter('date')(member.warrantyinvoicedate, 'dd/MM/yyyy h:mma');
                  member.closuredate = $filter('date')(member.closuredate, 'dd/MM/yyyy h:mma');
                  // // $scope.exportArray.push(data);
                  // $scope.exportDellArray.push(data);

                  $scope.exportFinalDellArray.push(member);
                  // var old = JSON.stringify($scope.exportDellArray).replace(/null/g, '""'); 
                  // $scope.exportNewDellArray = JSON.parse(old);

                });
                //console.log($scope.exportFinalDellArray, $scope.exportFinalDellArray.length, "$scope.exportFinalDellArray-1987")

                $scope.TotalData = [];
                $scope.valConCount = 0;

                // console.log($scope.exportDellArray, "$scope.exportDellArray-1096")
                // console.log($scope.exportNewDellArray, $scope.exportNewDellArray.length, "$scope.exportNewDellArray-1097")

                if ($scope.exportFinalDellArray.length == 0) {
                  alert('No data found');
                } else {
                  for (var arr = 0; arr < $scope.exportFinalDellArray.length; arr++) {
                    $scope.TotalData.push({
                      //'Sr No': $scope.exportFinalDellArray[arr].index,
                      'Dispatch': $scope.exportFinalDellArray[arr].dispatchNumber,
                      'Update Type': $scope.exportFinalDellArray[arr].updatetypeName,
                      'Service Identifier': $scope.exportFinalDellArray[arr].serviceidentifier,
                      'Service Event': $scope.exportFinalDellArray[arr].serviceeventName,
                      'Service Event Date Time': $scope.exportFinalDellArray[arr].lastmodifiedtime,
                      'Reply Code': $scope.exportFinalDellArray[arr].replyCode,
                      'Comments': $scope.exportFinalDellArray[arr].remark,
                      'Waybill Number': $scope.exportFinalDellArray[arr].waybillNo,
                      'Waybill Carrier': '',
                      'Waybill Direction': '',
                      'Waybill Date Time': '',
                      'Engineer': $scope.exportFinalDellArray[arr].engineerName,
                      'Part Number': $scope.exportFinalDellArray[arr].partNumber,
                      'Part Quantity': $scope.exportFinalDellArray[arr].partQuantity,
                      'Parts Used Type': $scope.exportFinalDellArray[arr].modelDescription,
                      'Reject Reason': $scope.exportFinalDellArray[arr].closureDate
                    });

                    $scope.valConCount++;
                    //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1100")
                    if ($scope.exportFinalDellArray.length == $scope.valConCount) {
                      alasql('SELECT * INTO XLSX("DellDateFilterList.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                    }
                  }
                }
                //-------------end of my ----------
              });
            });
          }

        } else {
          console.log("inside all dell export func");
          Restangular.all('surveyanswers?filter={"where":{"and":[{"answer":{"inq":["CALLED CUSTOMER UNABLE TO SET ETA-15","DSP TO FOLLOW UP-39","CATASTROPE-38","ETA FIXED WITH CUSTOMER WITHIN SLA-16","CUSTOMER REQUEST OUTSIDE SLA-11","SYSTEM FIXED-53","CALL CLOSED-93","CALL CANCELLED BEFORE ENGINEER REACHING SITE-81","CALL CANCELLED WHILE ENGINEER ONSITE-84"]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (rcdetailsresp) {
            $scope.rcdetailsdell = rcdetailsresp;
            console.log($scope.rcdetailsdell, "RC-RESP-2039")

            $scope.exportDellArray.push($scope.rcdetailsdell);
            var old = JSON.stringify($scope.exportDellArray).replace(/null/g, '""');
            $scope.exportNewDellArray = JSON.parse(old);

            console.log($scope.exportNewDellArray, $scope.exportNewDellArray[0].length, "$scope.exportNewDellArray-2045")

            angular.forEach($scope.exportNewDellArray[0], function (member, index) {
              member.index = index + 1;
              //console.log(member.index, " member.index-1886")

              //$scope.DispatchnoDisplay
              var dispatchnoNewData = $scope.rcdetailsDisplay.filter(function (arr) {
                return arr.id == member.memberid
              })[0];

              var olddispatchData = JSON.stringify(dispatchnoNewData).replace(/null/g, '""');
              var dispatchnoData = JSON.parse(olddispatchData);

              if (dispatchnoData != undefined) {
                member.dispatchNumber = dispatchnoData.dispatchno;
                // member.replyCode = dispatchnoData.rcCode;
                member.waybillNo = dispatchnoData.waybillno;
                member.partNumber = dispatchnoData.partnumber;
                member.partQuantity = dispatchnoData.partquantity;
                member.modelDescription = dispatchnoData.modeldescription;
                member.closureDate = dispatchnoData.closuredate;
              }

              //$scope.userDisplay
              // var userData = $scope.userDisplay.filter(function (arr) {
              //   return arr.id == member.assignedto
              // })[0];

              // if (userData != undefined) {
              //   member.engineerName = userData.username;
              // }

              var userData = $scope.userDisplay.filter(function (arr) {
                return arr.id == dispatchnoData.assignedto
              })[0];

              if (userData != undefined) {
                member.engineerName = userData.username;
              }

              //$scope.updatetypeDisplay
              // var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
              //   return arr.id == member.producttypeid
              // })[0];
              var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
                return arr.id == member.updatetypeid
              })[0];

              if (updatetypeData != undefined) {
                member.updatetypeName = updatetypeData.name;
              }

              //$scope.serviceeventDisplay
              var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
                return arr.id == member.serviceeventid
              })[0];

              if (serviceeventData != undefined) {
                member.serviceeventName = serviceeventData.name;
              }
              var replyAnsCode = member.answer.split("-");
              member.replyCode = replyAnsCode[1];
              //console.log("ReplyCode ", member.replyCode);
              member.lastmodifiedtime = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy h:mm:ssa');
              member.warrantyinvoicedate = $filter('date')(member.warrantyinvoicedate, 'dd/MM/yyyy h:mma');
              member.closuredate = $filter('date')(member.closuredate, 'dd/MM/yyyy h:mma');
              // // $scope.exportArray.push(data);
              // $scope.exportDellArray.push(data);

              $scope.exportFinalDellArray.push(member);
              // var old = JSON.stringify($scope.exportDellArray).replace(/null/g, '""'); 
              // $scope.exportNewDellArray = JSON.parse(old);

            });
            console.log($scope.exportFinalDellArray, $scope.exportFinalDellArray.length, "$scope.exportFinalDellArray-1092")

            $scope.TotalData = [];
            $scope.valConCount = 0;

            // console.log($scope.exportDellArray, "$scope.exportDellArray-1096")
            // console.log($scope.exportNewDellArray, $scope.exportNewDellArray.length, "$scope.exportNewDellArray-1097")

            if ($scope.exportFinalDellArray.length == 0) {
              alert('No data found');
            } else {
              for (var arr = 0; arr < $scope.exportFinalDellArray.length; arr++) {
                $scope.TotalData.push({
                  //'Sr No': $scope.exportFinalDellArray[arr].index,
                  'Dispatch': $scope.exportFinalDellArray[arr].dispatchNumber,
                  'Update Type': $scope.exportFinalDellArray[arr].updatetypeName,
                  'Service Identifier': $scope.exportFinalDellArray[arr].serviceidentifier,
                  'Service Event': $scope.exportFinalDellArray[arr].serviceeventName,
                  'Service Event Date Time': $scope.exportFinalDellArray[arr].lastmodifiedtime,
                  'Reply Code': $scope.exportFinalDellArray[arr].replyCode,
                  'Comments': $scope.exportFinalDellArray[arr].remark,
                  'Waybill Number': $scope.exportFinalDellArray[arr].waybillNo,
                  'Waybill Carrier': '',
                  'Waybill Direction': '',
                  'Waybill Date Time': '',
                  'Engineer': $scope.exportFinalDellArray[arr].engineerName,
                  'Part Number': $scope.exportFinalDellArray[arr].partNumber,
                  'Part Quantity': $scope.exportFinalDellArray[arr].partQuantity,
                  'Parts Used Type': $scope.exportFinalDellArray[arr].modelDescription,
                  'Reject Reason': $scope.exportFinalDellArray[arr].closureDate
                });

                $scope.valConCount++;
                //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1100")
                if ($scope.exportFinalDellArray.length == $scope.valConCount) {
                  alasql('SELECT * INTO XLSX("DellList.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                }
              }
            }

          });

        }

        //----------- here the code was ---

      }

    };

    $scope.nextRunCount = 0;
    $scope.totalRunCount = 0;

    $scope.runNext = function () {
      $scope.exportcount = 0;
      $scope.exportFinalAllArray = [];

      $scope.nextRunCount = $scope.nextRunCount + 1;
      $scope.totalRunCount = $scope.nextRunCount;

      // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1580")
      console.log($scope.totalRunCount, "$scope.totalRunCount-1971")

      $scope.exportAllArray.push($scope.rcdetailsall);
      var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
      $scope.exportNewAllArray = JSON.parse(old);

      // angular.forEach($scope.rcdetailsall, function (data, index) {
      angular.forEach($scope.exportNewAllArray[0], function (data, index) {
        data.index = index + 1;
        console.log("here-1975")
        var cityData = $scope.cityDisplay.filter(function (arr) {
          return arr.id == data.cityId
        })[0];

        if (cityData != undefined) {
          data.cityName = cityData.name;
        }

        //statusDisplay - $scope.statusDisplay
        var statusData = $scope.statusDisplay.filter(function (arr) {
          return arr.id == data.statusId
        })[0];

        if (statusData != undefined) {
          data.statusName = statusData.name;
        }

        //$scope.userDisplay
        var userData = $scope.userDisplay.filter(function (arr) {
          return arr.id == data.assignedto
        })[0];

        if (userData != undefined) {
          data.engineerName = userData.username;
        }

        //$scope.updatetypeDisplay
        var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
          return arr.id == data.producttypeid
        })[0];

        if (updatetypeData != undefined) {
          data.updatetypeName = updatetypeData.name;
        }

        //$scope.serviceeventDisplay
        var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
          return arr.id == data.serviceeventid
        })[0];

        if (serviceeventData != undefined) {
          data.serviceeventName = serviceeventData.name;
        }
        data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
        data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
        data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
        data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
        data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
        data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
        $scope.exportFinalAllArray.push(data);
        $scope.exportcount++;
        if ($scope.exportcount == $scope.rcdetailsall.length) {
          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].statusName,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].cityName,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].rcCode,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineerName,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].updatetypeName,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].serviceeventName,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].lastmodifiedtime
              });

              $scope.valConCount++;
              console.log($scope.valConCount, $scope.exportFinalAllArray.length, "valcount-exportArray Length-1671")
              console.log($scope.totalRunCount, $scope.pushwithDataCount, "$scope.totalRunCount -- $scope.pushwithDataCount-1571")
              // if ($scope.totalRunCount == $scope.pushwithDataCount + 1) {
              if ($scope.rcdetailsall.length == $scope.pushwithDataCount) {
                if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                  //$scope.isExport = false;
                  alasql('SELECT * INTO XLSX("Alllist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                }
              }
            }
          }
        }
      });
    };

    $scope.SaveCount = 0;
    $scope.pushwithDataCount = 0;
    $scope.customLoop = function () {
      console.log($scope.strOrgstructFinal.length, "$scope.strOrgstructFinal.length-2082", $scope.SaveCount)
      $scope.totalLoopCount = $scope.strOrgstructFinal.length; //saty
      if ($scope.SaveCount < $scope.strOrgstructFinal.length) {
        console.log($scope.strOrgstructFinal[$scope.SaveCount], "$scope.strOrgstructFinal[$scope.SaveCount]-2085")
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[$scope.SaveCount] + '%').getList().then(function (part2) {
          console.log('role test-2086');
          // $scope.rcdetails = part2;
          $scope.membersnew = part2;
          //console.log($scope.membersnew, "$scope.membersnew-1687", $scope.membersnew.length)
          if ($scope.membersnew.length > 0) {
            // $scope.pushwithDataCount = $scope.pushwithDataCount + 1;
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.pushwithDataCount = $scope.pushwithDataCount + 1;
              $scope.membersFinal.push(value);
              $scope.myArray.push(value);
              // $scope.SaveCount++;
              // $scope.customLoop();
              console.log("In Push-2098")
            });
            $scope.SaveCount++;
            $scope.customLoop();
          } else {
            $scope.SaveCount++;
            $scope.customLoop();
          }
          $scope.rcdetailsall = $scope.membersFinal;
          // console.log("All Ticket details - 1225", $scope.myArray);
          console.log("All Ticket details - 2108", $scope.rcdetailsall);
        });
      } else {
        // console.log($scope.rcdetailsall, "$scope.rcdetailsall-1703")
        // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1712")
        $scope.runNext();
      }
    };

    //------------- csutomLoopSrMgr ----------
    $scope.SaveCountMgr = 0;
    $scope.pushwithDataCount1 = 0;
    $scope.totalLoopCount1 = 0;
    $scope.runcount1 = 0;

    $scope.customLoopSrMgr = function () {
      // console.log($scope.exportcountSrMgr, "$scope.exportcountSrMgr-1674")
      //console.log($scope.strOrgstructFinal.length, "$scope.strOrgstructFinal.length-1681")
      $scope.totalLoopCount1 = $scope.strOrgstructFinals.length; //saty
      console.log($scope.strOrgstructFinals.length, "$scope.strOrgstructFinals.length-$scope.SaveCountMgr-1625", $scope.SaveCountMgr)
      if ($scope.SaveCountMgr < $scope.strOrgstructFinals.length) {
        Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[$scope.SaveCountMgr] + '%').getList().then(function (part3) {
          //console.log('role test SrMgr');
          // $scope.rcdetails = part3;
          $scope.membersnew = part3;
          console.log($scope.membersnew, "$scope.membersnew-1631", $scope.membersnew.length)
          if ($scope.membersnew.length > 0) {
            // $scope.pushwithDataCount1 = $scope.pushwithDataCount1 + 1;
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.pushwithDataCount1 = $scope.pushwithDataCount1 + 1;
              $scope.membersFinal.push(value);
              $scope.myArray.push(value);
              // $scope.SaveCountMgr++;
              // $scope.customLoopSrMgr();
            });
            $scope.SaveCountMgr++;
            $scope.customLoopSrMgr();
          } else {
            $scope.SaveCountMgr++;
            $scope.customLoopSrMgr();
          }
          $scope.rcdetailsall = $scope.membersFinal;
          // console.log("All Ticket details - 1641", $scope.myArray);
          console.log("All Ticket details - 1646", $scope.rcdetailsall);
        });
      } else {
        console.log($scope.rcdetailsall, "$scope.rcdetailsall-$scope.myArray.length-1649", $scope.myArray.length)
        console.log($scope.pushwithDataCount1, "$scope.pushwithDataCount1- $scope.totalLoopCount1-1650", $scope.totalLoopCount1)
        console.log($scope.SaveCountMgr, "$scope.SaveCountMgr-1651")
        $scope.runcount1 = $scope.runcount1 + 1;
        // $scope.diffCount = $scope.runcount1 - $scope.totalLoopCount1;
        // console.log($scope.diffCount, "$scope.diffCount-1656")
        // if ($scope.runcount1 <= $scope.totalLoopCount1) {
        $scope.runNextforSrMgr();
        // }
      }
    };

    //----------- runNextforsrmgr ----------------------------

    $scope.nextRunCnt = 0;
    $scope.totalRunCnt = 0;

    $scope.runNextforSrMgr = function () {
      $scope.exportcount = 0;
      $scope.exportFinalAllArray = [];

      $scope.nextRunCnt = $scope.nextRunCnt + 1;
      $scope.totalRunCnt = $scope.nextRunCnt;

      // console.log($scope.pushwithDataCount1, $scope.myArray.length, "$scope.pushwithDataCount1-$scope.myArray.length-1673")
      // // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1580")
      // console.log($scope.totalRunCnt, "$scope.totalRunCnt-1676")
      // console.log($scope.rcdetailsall, "$scope.rcdetailsall-1677")

      $scope.exportAllArray.push($scope.rcdetailsall);
      var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
      $scope.exportNewAllArray = JSON.parse(old);

      // angular.forEach($scope.rcdetailsall, function (data, index) {
      angular.forEach($scope.exportNewAllArray[0], function (data, index) {
        data.index = index + 1;
        console.log("here-1680")
        var cityData = $scope.cityDisplay.filter(function (arr) {
          return arr.id == data.cityId
        })[0];

        if (cityData != undefined) {
          data.cityName = cityData.name;
        }

        //statusDisplay - $scope.statusDisplay
        var statusData = $scope.statusDisplay.filter(function (arr) {
          return arr.id == data.statusId
        })[0];

        if (statusData != undefined) {
          data.statusName = statusData.name;
        }

        //$scope.userDisplay
        var userData = $scope.userDisplay.filter(function (arr) {
          return arr.id == data.assignedto
        })[0];

        if (userData != undefined) {
          data.engineerName = userData.username;
        }

        //$scope.updatetypeDisplay
        var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
          return arr.id == data.producttypeid
        })[0];

        if (updatetypeData != undefined) {
          data.updatetypeName = updatetypeData.name;
        }

        //$scope.serviceeventDisplay
        var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
          return arr.id == data.serviceeventid
        })[0];

        if (serviceeventData != undefined) {
          data.serviceeventName = serviceeventData.name;
        }
        data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
        data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
        data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
        data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
        data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
        data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
        $scope.exportFinalAllArray.push(data);
        $scope.exportcount++;
        console.log($scope.exportFinalAllArray.length, $scope.exportcount, "$scope.exportFinalAllArray.length -- $scope.exportcount-1728")
        if ($scope.exportcount == $scope.rcdetailsall.length) {
          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].statusName,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].cityName,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].rcCode,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineerName,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].updatetypeName,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].serviceeventName,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].lastmodifiedtime
              });

              $scope.valConCount++;
              // console.log($scope.valConCount, $scope.exportFinalAllArray.length, "valcount-exportArray Length-1671")
              // console.log($scope.rcdetailsall.length, $scope.totalRunCnt, $scope.pushwithDataCount1, "$scope.totalRunCnt--$scope.pushwithDataCount1-1722")
              //if ($scope.totalRunCnt == $scope.pushwithDataCount1 + $scope.diffCount) {
              if ($scope.rcdetailsall.length == $scope.pushwithDataCount1) {
                if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                  //$scope.isExport = false;
                  alasql('SELECT * INTO XLSX("Alllist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                }
              }
            }
          }
        }
      });
    };

    $scope.exportAllDumpData = function () {
      console.log("inside All Dump export func");

      $scope.exportAllArray = [];
      $scope.exportNewAllArray = [];
      $scope.exportFinalAllArray = [];
      // $scope.exportFinalNewAllArray = [];

      ////define role based export here -------
      // Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {
      $scope.rolebasedURL = '';
      $scope.rcdetailsall = '';
      $scope.filteredRCDetail = {};

      // if ($window.sessionStorage.roleId == 1) {
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 4) {
        //$scope.rolebasedURL = 'rcdetails?filter[where][deleteflag]=false';
        // Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {


        $scope.gorestURL = '';
        $scope.gofilterAdminDumpdata();

        $scope.DisableExport = true;
        $scope.DisablefilterExport = false;
        console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-214")
        //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-224")
        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        // Restangular.all($scope.gorestURL).getList().then(function (rcdetailsresp) {
        //   $scope.rcdetails = rcdetailsresp;
        //   console.log($scope.rcdetails, "RC-RESP-10-5286");

        //   angular.forEach($scope.rcdetails, function (member, index) {
        //     member.index = index + 1;

        //   });
        //   $scope.exportFilterFinals.push($scope.rcdetails);
        // });


        //----------------------------------------
        // Restangular.all('finalalldumptemplate_view').getList().then(function (rcdetailresp) {
        console.log($scope.gorestURL, "$scope.gorestURL-5342")
        Restangular.all($scope.gorestURL).getList().then(function (rcdetailresp) {
          $scope.rcdetailsall = rcdetailresp;

          console.log($scope.rcdetailsall, "$scope.rcdetailsall-5346")

          $scope.exportAllArray.push($scope.rcdetailsall);
          var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
          $scope.exportNewAllArray = JSON.parse(old);

          //console.log($scope.exportNewAllArray, $scope.exportNewAllArray[0].length, "$scope.exportNewAllArray-1015")

          angular.forEach($scope.exportNewAllArray[0], function (data, index) {
            data.index = index + 1;

            var cityData = $scope.cityDisplay.filter(function (arr) {
              return arr.id == data.cityId
            })[0];

            if (cityData != undefined) {
              data.cityName = cityData.name;
            }

            //statusDisplay - $scope.statusDisplay
            var statusData = $scope.statusDisplay.filter(function (arr) {
              return arr.id == data.statusId
            })[0];

            if (statusData != undefined) {
              data.statusName = statusData.name;
            }

            //$scope.userDisplay
            var userData = $scope.userDisplay.filter(function (arr) {
              return arr.id == data.assignedto
            })[0];

            if (userData != undefined) {
              data.engineerName = userData.username;
            }

            //$scope.updatetypeDisplay
            var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
              return arr.id == data.producttypeid
            })[0];

            if (updatetypeData != undefined) {
              data.updatetypeName = updatetypeData.name;
            }

            //$scope.serviceeventDisplay
            var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
              return arr.id == data.serviceeventid
            })[0];

            if (serviceeventData != undefined) {
              data.serviceeventName = serviceeventData.name;
            }
            data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
            data.rollbackRequestTime = $filter('date')(data.rollbackRequestTime, 'dd/MM/yyyy h:mma');
            data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
            data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
            data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
            data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
            // data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
            data.service_event_date_time = $filter('date')(data.service_event_date_time, 'dd/MM/yyyy h:mm:ssa');

            //console.log(data, "data-1070")
            $scope.exportFinalAllArray.push(data);

          });
          //console.log($scope.exportFinalAllArray, $scope.exportFinalAllArray.length, "$scope.exportFinalAllArray-1092")

          // var rcdetailalldata = JSON.stringify($scope.exportFinalAllArray);
          // console.log(rcdetailalldata, "rcdetailalldata-1088")

          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].dispatch_status,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Name': $scope.exportFinalAllArray[arr].customername,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].city,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].reply_code,
                'User': $scope.exportFinalAllArray[arr].user,
                'Comments': $scope.exportFinalAllArray[arr].comments,
                'Rollback Punched Date /Time ': $scope.exportFinalAllArray[arr].rollbackRequestTime,
                'Rollback Requested Role': $scope.exportFinalAllArray[arr].rollback_requested_role,
                'Rollback Requested User': $scope.exportFinalAllArray[arr].rollback_requested_user,
                'Rollback Status': $scope.exportFinalAllArray[arr].rollback_status,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineer_name,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].update_type,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].service_event,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Courier Name': $scope.exportFinalAllArray[arr].couriername,
                'Docket': $scope.exportFinalAllArray[arr].docket,
                'Dispatch Date': $scope.exportFinalAllArray[arr].dispatchdate,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].service_event_date_time
              });

              $scope.valConCount++;

              //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1097")
              if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                alasql('SELECT * INTO XLSX("AllDumplist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
              }
            }
          }

        });

      } 
      // Added new code for Zone level
      if ($window.sessionStorage.roleId == 8) {
        //$scope.rolebasedURL = 'rcdetails?filter[where][deleteflag]=false';
        // Restangular.all('rcdetails?filter[where][deleteflag]=false').getList().then(function (rcdetailresp) {


        $scope.gorestURL = '';
        $scope.gofilterZoneDumpdata();

        $scope.DisableExport = true;
        $scope.DisablefilterExport = false;
        console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-214")
        //Restangular.one('surveyquestions', $scope.serchdetails.rcstatus).get().then(function (rcststusresp) {
        //console.log("serachdetailsrcStatus", rcststusresp.rcCode);
        //console.log($scope.serchdetails.rcstatus, "$scope.serchdetails.rcstatus-224")
        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][rcCode]=' + rcststusresp.rcCode).getList().then(function (rcdetailsresp) {
        // Restangular.all($scope.gorestURL).getList().then(function (rcdetailsresp) {
        //   $scope.rcdetails = rcdetailsresp;
        //   console.log($scope.rcdetails, "RC-RESP-10-5286");

        //   angular.forEach($scope.rcdetails, function (member, index) {
        //     member.index = index + 1;

        //   });
        //   $scope.exportFilterFinals.push($scope.rcdetails);
        // });


        //----------------------------------------
        // Restangular.all('finalalldumptemplate_view').getList().then(function (rcdetailresp) {
        console.log($scope.gorestURL, "$scope.gorestURL-Zone Level5342")
        Restangular.all($scope.gorestURL).getList().then(function (rcdetailresp) {
          $scope.rcdetailsall = rcdetailresp;

          console.log($scope.rcdetailsall, "$scope.rcdetailsall-5346")

          $scope.exportAllArray.push($scope.rcdetailsall);
          var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
          $scope.exportNewAllArray = JSON.parse(old);

          //console.log($scope.exportNewAllArray, $scope.exportNewAllArray[0].length, "$scope.exportNewAllArray-1015")

          angular.forEach($scope.exportNewAllArray[0], function (data, index) {
            data.index = index + 1;

            var cityData = $scope.cityDisplay.filter(function (arr) {
              return arr.id == data.cityId
            })[0];

            if (cityData != undefined) {
              data.cityName = cityData.name;
            }

            //statusDisplay - $scope.statusDisplay
            var statusData = $scope.statusDisplay.filter(function (arr) {
              return arr.id == data.statusId
            })[0];

            if (statusData != undefined) {
              data.statusName = statusData.name;
            }

            //$scope.userDisplay
            var userData = $scope.userDisplay.filter(function (arr) {
              return arr.id == data.assignedto
            })[0];

            if (userData != undefined) {
              data.engineerName = userData.username;
            }

            //$scope.updatetypeDisplay
            var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
              return arr.id == data.producttypeid
            })[0];

            if (updatetypeData != undefined) {
              data.updatetypeName = updatetypeData.name;
            }

            //$scope.serviceeventDisplay
            var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
              return arr.id == data.serviceeventid
            })[0];

            if (serviceeventData != undefined) {
              data.serviceeventName = serviceeventData.name;
            }
            data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
            data.rollbackRequestTime = $filter('date')(data.rollbackRequestTime, 'dd/MM/yyyy h:mma');
            data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
            data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
            data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
            data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
            // data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
            data.service_event_date_time = $filter('date')(data.service_event_date_time, 'dd/MM/yyyy h:mm:ssa');

            //console.log(data, "data-1070")
            $scope.exportFinalAllArray.push(data);

          });
          //console.log($scope.exportFinalAllArray, $scope.exportFinalAllArray.length, "$scope.exportFinalAllArray-1092")

          // var rcdetailalldata = JSON.stringify($scope.exportFinalAllArray);
          // console.log(rcdetailalldata, "rcdetailalldata-1088")

          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].dispatch_status,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Name': $scope.exportFinalAllArray[arr].customername,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].city,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].reply_code,
                'User': $scope.exportFinalAllArray[arr].user,
                'Comments': $scope.exportFinalAllArray[arr].comments,
                'Rollback Punched Date /Time ': $scope.exportFinalAllArray[arr].rollbackRequestTime,
                'Rollback Requested Role': $scope.exportFinalAllArray[arr].rollback_requested_role,
                'Rollback Requested User': $scope.exportFinalAllArray[arr].rollback_requested_user,
                'Rollback Status': $scope.exportFinalAllArray[arr].rollback_status,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineer_name,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].update_type,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].service_event,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Courier Name': $scope.exportFinalAllArray[arr].couriername,
                'Docket': $scope.exportFinalAllArray[arr].docket,
                'Dispatch Date': $scope.exportFinalAllArray[arr].dispatchdate,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].service_event_date_time
              });

              $scope.valConCount++;

              //console.log($scope.valConCount, $scope.exportDellArray.length, "valcount-exportArray Length-1097")
              if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                alasql('SELECT * INTO XLSX("AllDumplist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
              }
            }
          }

        });

      }

      
      else if ($window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
        var str = $scope.orgStructureFilter;
        var strOrgstruct = str.split('"');

        $scope.strOrgstructFinal = [];
        $scope.mycount = 0;
        $scope.listcount = 0;
        $scope.exportcount = 0;
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
          $scope.mycount++;
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinals.length-1215")
        $scope.myArray = [];
        $scope.membersFinal = [];
        //Below line ($scope.SaveCount = 0; $scope.pushwithDataCount = 0;) is for repeated export -- saty
        $scope.SaveCount = 0;
        $scope.pushwithDataCount = 0;
        //console.log("here-2790")
        $scope.customDumpLoop();

        // Restangular.all('finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[$scope.SaveCount] + '%').getList().then(function (part2) {

        // });


      } else if ($window.sessionStorage.roleId == 5) {
        // $scope.exportcountSrMgr = 0;
        //console.log("here-2800")
        //$scope.exportcountSrMgr = $scope.exportcountSrMgr + 1;

        var str1 = $scope.orgStructureUptoStateFilter;
        var strOrgstruct1 = str1.split('"');
        $scope.strOrgstructFinals = [];
        $scope.mycount = 0;
        $scope.listcount = 0;
        $scope.exportcount = 0;
        for (var y = 0; y < strOrgstruct1.length; y++) {
          if (y % 2 != 0) {
            $scope.strOrgstructFinals.push(strOrgstruct1[y]);
          }
          $scope.mycount++;
        }
        //console.log($scope.strOrgstructFinals, "$scope.strOrgstructFinals.length-1350")
        $scope.myArray = [];
        $scope.membersFinal = [];
        //Below line ($scope.SaveCountMgr = 0; $scope.pushwithDataCount1 = 0;) is for repeated export -- saty
        $scope.SaveCountMgr = 0;
        $scope.pushwithDataCount1 = 0;
        //console.log("here-2821")
        $scope.customDumpLoopSrMgr();


      };

    };

    $scope.nextRunCount = 0;
    $scope.totalRunCount = 0;

    $scope.runDumpNext = function () {
      $scope.exportcount = 0;
      $scope.exportFinalAllArray = [];

      $scope.nextRunCount = $scope.nextRunCount + 1;
      $scope.totalRunCount = $scope.nextRunCount;

      // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1580")
      //console.log($scope.totalRunCount, "$scope.totalRunCount-1971")

      $scope.exportAllArray.push($scope.rcdetailsall);
      var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
      $scope.exportNewAllArray = JSON.parse(old);

      // angular.forEach($scope.rcdetailsall, function (data, index) {
      angular.forEach($scope.exportNewAllArray[0], function (data, index) {
        data.index = index + 1;
        //console.log("here-1975")
        var cityData = $scope.cityDisplay.filter(function (arr) {
          return arr.id == data.cityId
        })[0];

        if (cityData != undefined) {
          data.cityName = cityData.name;
        }

        //statusDisplay - $scope.statusDisplay
        var statusData = $scope.statusDisplay.filter(function (arr) {
          return arr.id == data.statusId
        })[0];

        if (statusData != undefined) {
          data.statusName = statusData.name;
        }

        //$scope.userDisplay
        var userData = $scope.userDisplay.filter(function (arr) {
          return arr.id == data.assignedto
        })[0];

        if (userData != undefined) {
          data.engineerName = userData.username;
        }

        //$scope.updatetypeDisplay
        var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
          return arr.id == data.producttypeid
        })[0];

        if (updatetypeData != undefined) {
          data.updatetypeName = updatetypeData.name;
        }

        //$scope.serviceeventDisplay
        var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
          return arr.id == data.serviceeventid
        })[0];

        if (serviceeventData != undefined) {
          data.serviceeventName = serviceeventData.name;
        }

        data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
        data.rollbackRequestTime = $filter('date')(data.rollbackRequestTime, 'dd/MM/yyyy h:mma');
        data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
        data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
        data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
        data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
        // data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
        data.service_event_date_time = $filter('date')(data.service_event_date_time, 'dd/MM/yyyy h:mm:ssa');

        $scope.exportFinalAllArray.push(data);
        $scope.exportcount++;
        if ($scope.exportcount == $scope.rcdetailsall.length) {
          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].dispatch_status,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Name': $scope.exportFinalAllArray[arr].customername,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].city,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].reply_code,
                'User': $scope.exportFinalAllArray[arr].user,
                'Comments': $scope.exportFinalAllArray[arr].comments,
                'Rollback Punched Date /Time ': $scope.exportFinalAllArray[arr].rollbackRequestTime,
                'Rollback Requested Role': $scope.exportFinalAllArray[arr].rollback_requested_role,
                'Rollback Requested User': $scope.exportFinalAllArray[arr].rollback_requested_user,
                'Rollback Status': $scope.exportFinalAllArray[arr].rollback_status,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineer_name,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].update_type,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].service_event,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Courier Name': $scope.exportFinalAllArray[arr].couriername,
                'Docket': $scope.exportFinalAllArray[arr].docket,
                'Dispatch Date': $scope.exportFinalAllArray[arr].dispatchdate,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].service_event_date_time
              });

              $scope.valConCount++;
              // console.log($scope.valConCount, $scope.exportFinalAllArray.length, "valcount-exportArray Length-2938")
              // console.log($scope.totalRunCount, $scope.pushwithDataCount, "$scope.totalRunCount -- $scope.pushwithDataCount-2939")
              // if ($scope.totalRunCount == $scope.pushwithDataCount + 1) {
              if ($scope.rcdetailsall.length == $scope.pushwithDataCount) {
                if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                  //$scope.isExport = false;
                  alasql('SELECT * INTO XLSX("AllDumplist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                }
              }
            }
          }
        }
      });
    };

    $scope.SaveCount = 0;
    $scope.pushwithDataCount = 0;
    //$scope.selectedRcCode = [];
    $scope.customDumpLoop = function () {
      //console.log($scope.strOrgstructFinal.length, "$scope.strOrgstructFinal.length-2956", $scope.SaveCount)
      $scope.totalLoopCount = $scope.strOrgstructFinal.length;
      $scope.restURL = '';
      //$scope.selectedRcCode = [];
      if ($scope.SaveCount < $scope.strOrgstructFinal.length) {
        //console.log($scope.strOrgstructFinal[$scope.SaveCount], "$scope.strOrgstructFinal[$scope.SaveCount]-3112")

        //--- call here - $scope.filterdata(); -----
        $scope.filterdata();
        //console.log($scope.restURL, "$scope.restURL-5931")

        //console.log($scope.restURL, "CUSTOM MADE URL")
        Restangular.all($scope.restURL + $scope.strOrgstructFinal[$scope.SaveCount] + '%').getList().then(function (part2) {
          // Restangular.all('finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[$scope.SaveCount] + '%').getList().then(function (part2) {
          //console.log('role test-2961');
          // $scope.rcdetails = part2;
          $scope.membersnew = part2;
          //console.log($scope.membersnew, "$scope.membersnew-1687", $scope.membersnew.length)
          if ($scope.membersnew.length > 0) {
            // $scope.pushwithDataCount = $scope.pushwithDataCount + 1;
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.pushwithDataCount = $scope.pushwithDataCount + 1;
              $scope.membersFinal.push(value);
              $scope.myArray.push(value);
              // $scope.SaveCount++;
              // $scope.customLoop();
              //console.log("In Push-2973")
            });
            $scope.SaveCount++;
            $scope.customDumpLoop();
          } else {
            $scope.SaveCount++;
            $scope.customDumpLoop();
          }
          $scope.rcdetailsall = $scope.membersFinal;
          // console.log("All Ticket details - 1225", $scope.myArray);
          //console.log("All Ticket details - 2983", $scope.rcdetailsall);
        });
      } else {
        // console.log($scope.rcdetailsall, "$scope.rcdetailsall-1703")
        // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1712")
        $scope.runDumpNext();
      }
    };

    $scope.filterdata = function () {
      // if ($scope.serchdetails.dispatchno != undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
      //   $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno + '&filter[where][orgstructure][like]=%';

      // }
      // $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinal[$scope.SaveCount] + '%';
      // $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%';

      $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false'
      //&filter[where][orgstructure][like]=%';

      if ($scope.serchdetails.dispatchno != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][dispatchno]=' + $scope.serchdetails.dispatchno;
      }
      if ($scope.serchdetails.customerName != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][customername]=' + $scope.serchdetails.customerName;
      }
      if ($scope.serchdetails.rcstatus != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][reply_code]=' + $scope.serchdetails.rcstatus;
      }
      if ($scope.serchdetails.status != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][statusId]=' + $scope.serchdetails.status;
      }
      if ($scope.serchdetails.servicePostalCode != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][servicepostalcode]=' + $scope.serchdetails.servicePostalCode;
      }
      if ($scope.serchdetails.servicetag != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][servicetag]=' + $scope.serchdetails.servicetag;
      }
      if ($scope.serchdetails.engineer != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][engineer_name]=' + $scope.serchdetails.engineer;
      }

      if ($scope.serchdetails.dispatchno != undefined || $scope.serchdetails.customerName != undefined || $scope.serchdetails.rcstatus != undefined || $scope.serchdetails.status != undefined || $scope.serchdetails.servicePostalCode != undefined || $scope.serchdetails.servicetag != undefined || $scope.serchdetails.engineer != undefined) {
        $scope.restURL = $scope.restURL + '&filter[where][orgstructure][like]=%';
      }
      if ($scope.serchdetails.dispatchno == undefined && $scope.serchdetails.customerName == undefined && $scope.serchdetails.rcstatus == undefined && $scope.serchdetails.status == undefined && $scope.serchdetails.servicePostalCode == undefined && $scope.serchdetails.servicetag == undefined && $scope.serchdetails.engineer == undefined) {
        $scope.restURL = 'finalalldumptemplate_view?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%';
      }

      // Restangular.all($scope.restURL).getList().then(function (part2) {

      // });

    };

    //------------- csutomLoopSrMgr ----------
    $scope.SaveCountMgr = 0;
    $scope.pushwithDataCount1 = 0;
    $scope.totalLoopCount1 = 0;
    $scope.runcount1 = 0;

    $scope.customDumpLoopSrMgr = function () {

      $scope.totalLoopCount1 = $scope.strOrgstructFinals.length;
      //console.log($scope.strOrgstructFinals.length, "$scope.strOrgstructFinals.length-$scope.SaveCountMgr-1625", $scope.SaveCountMgr)
      $scope.restURL = '';
      //$scope.selectedRcCode = [];
      if ($scope.SaveCountMgr < $scope.strOrgstructFinals.length) {
        //console.log($scope.strOrgstructFinal[$scope.SaveCount], "$scope.strOrgstructFinal[$scope.SaveCount]-3112")
        $scope.filterdata();

        //console.log($scope.restURL, "CUSTOM MADE URL")

        // Restangular.all('rcdetails?filter[where][deleteflag]=false&filter[where][orgstructure][like]=%' + $scope.strOrgstructFinals[$scope.SaveCountMgr] + '%').getList().then(function (part3) {
        //Restangular.all($scope.restURL + $scope.strOrgstructFinal[$scope.SaveCount] + '%').getList().then(function (part2) {
        Restangular.all($scope.restURL + $scope.strOrgstructFinals[$scope.SaveCountMgr] + '%').getList().then(function (part3) {
          //console.log('role test SrMgr');
          // $scope.rcdetails = part3;
          $scope.membersnew = part3;
          //console.log($scope.membersnew, "$scope.membersnew-3130", $scope.membersnew.length)
          if ($scope.membersnew.length > 0) {
            // $scope.pushwithDataCount1 = $scope.pushwithDataCount1 + 1;
            angular.forEach($scope.membersnew, function (value, index) {
              $scope.pushwithDataCount1 = $scope.pushwithDataCount1 + 1;
              $scope.membersFinal.push(value);
              $scope.myArray.push(value);
              // $scope.SaveCountMgr++;
              // $scope.customLoopSrMgr();
            });
            $scope.SaveCountMgr++;
            $scope.customDumpLoopSrMgr();
          } else {
            $scope.SaveCountMgr++;
            $scope.customDumpLoopSrMgr();
          }
          $scope.rcdetailsall = $scope.membersFinal;
          // console.log("All Ticket details - 1641", $scope.myArray);
          //console.log("All Ticket details - 3147", $scope.rcdetailsall);
        });
      } else {
        // console.log($scope.rcdetailsall, "$scope.rcdetailsall-$scope.myArray.length-3150", $scope.myArray.length)
        // console.log($scope.pushwithDataCount1, "$scope.pushwithDataCount1- $scope.totalLoopCount1-3151", $scope.totalLoopCount1)
        // console.log($scope.SaveCountMgr, "$scope.SaveCountMgr-3152")
        $scope.runcount1 = $scope.runcount1 + 1;
        // $scope.diffCount = $scope.runcount1 - $scope.totalLoopCount1;
        // console.log($scope.diffCount, "$scope.diffCount-1656")
        // if ($scope.runcount1 <= $scope.totalLoopCount1) {
        $scope.runDumpNextforSrMgr();
        // }
      }
    };

    //----------- runNextforsrmgr ----------------------------

    $scope.nextRunCnt = 0;
    $scope.totalRunCnt = 0;

    $scope.runDumpNextforSrMgr = function () {
      $scope.exportcount = 0;
      $scope.exportFinalAllArray = [];

      $scope.nextRunCnt = $scope.nextRunCnt + 1;
      $scope.totalRunCnt = $scope.nextRunCnt;

      // console.log($scope.pushwithDataCount1, $scope.myArray.length, "$scope.pushwithDataCount1-$scope.myArray.length-3174")
      // // console.log($scope.pushwithDataCount, "$scope.pushwithDataCount-1580")
      // console.log($scope.totalRunCnt, "$scope.totalRunCnt-3176")
      // console.log($scope.rcdetailsall, "$scope.rcdetailsall-3177")

      $scope.exportAllArray.push($scope.rcdetailsall);
      var old = JSON.stringify($scope.exportAllArray).replace(/null/g, '""');
      $scope.exportNewAllArray = JSON.parse(old);

      // angular.forEach($scope.rcdetailsall, function (data, index) {
      angular.forEach($scope.exportNewAllArray[0], function (data, index) {
        data.index = index + 1;
        //console.log("here-3186")
        var cityData = $scope.cityDisplay.filter(function (arr) {
          return arr.id == data.cityId
        })[0];

        if (cityData != undefined) {
          data.cityName = cityData.name;
        }

        //statusDisplay - $scope.statusDisplay
        var statusData = $scope.statusDisplay.filter(function (arr) {
          return arr.id == data.statusId
        })[0];

        if (statusData != undefined) {
          data.statusName = statusData.name;
        }

        //$scope.userDisplay
        var userData = $scope.userDisplay.filter(function (arr) {
          return arr.id == data.assignedto
        })[0];

        if (userData != undefined) {
          data.engineerName = userData.username;
        }

        //$scope.updatetypeDisplay
        var updatetypeData = $scope.updatetypeDisplay.filter(function (arr) {
          return arr.id == data.producttypeid
        })[0];

        if (updatetypeData != undefined) {
          data.updatetypeName = updatetypeData.name;
        }

        //$scope.serviceeventDisplay
        var serviceeventData = $scope.serviceeventDisplay.filter(function (arr) {
          return arr.id == data.serviceeventid
        })[0];

        if (serviceeventData != undefined) {
          data.serviceeventName = serviceeventData.name;
        }

        data.servicecalldate = $filter('date')(data.servicecalldate, 'dd/MM/yyyy h:mma');
        data.rollbackRequestTime = $filter('date')(data.rollbackRequestTime, 'dd/MM/yyyy h:mma');
        data.createdtime = $filter('date')(data.createdtime, 'dd/MM/yyyy h:mma');
        data.etatime = $filter('date')(data.etatime, 'dd/MM/yyyy h:mma');
        data.starttime = $filter('date')(data.starttime, 'dd/MM/yyyy h:mma');
        data.endtime = $filter('date')(data.endtime, 'dd/MM/yyyy h:mma');
        // data.lastmodifiedtime = $filter('date')(data.lastmodifiedtime, 'dd/MM/yyyy h:mma');
        data.service_event_date_time = $filter('date')(data.service_event_date_time, 'dd/MM/yyyy h:mm:ssa');

        $scope.exportFinalAllArray.push(data);
        $scope.exportcount++;
        //console.log($scope.exportFinalAllArray.length, $scope.exportcount, "$scope.exportFinalAllArray.length -- $scope.exportcount-3238")
        if ($scope.exportcount == $scope.rcdetailsall.length) {
          $scope.TotalData = [];
          $scope.valConCount = 0;

          if ($scope.exportFinalAllArray.length == 0) {
            alert('No data found');
          } else {
            for (var arr = 0; arr < $scope.exportFinalAllArray.length; arr++) {
              $scope.TotalData.push({
                'Sr No': $scope.exportFinalAllArray[arr].index,
                'Dispatch': $scope.exportFinalAllArray[arr].dispatchno,
                'Service Call Date': $scope.exportFinalAllArray[arr].servicecalldate,
                'Service Provider': $scope.exportFinalAllArray[arr].serviceprovider,
                'Logistics Provider': $scope.exportFinalAllArray[arr].logisticprovider,
                'Transport': $scope.exportFinalAllArray[arr].transport,
                'Dispatch Status': $scope.exportFinalAllArray[arr].dispatch_status,
                'LOB': $scope.exportFinalAllArray[arr].lob,
                'System Classification': $scope.exportFinalAllArray[arr].sysclassification,
                'End Service Window': $scope.exportFinalAllArray[arr].endservicewindow,
                'Service Tag': $scope.exportFinalAllArray[arr].servicetag,
                'DSP Type': $scope.exportFinalAllArray[arr].dsptype,
                'Call Type': $scope.exportFinalAllArray[arr].calltype,
                'Service Level': $scope.exportFinalAllArray[arr].servicelevel,
                'Customer Name': $scope.exportFinalAllArray[arr].customername,
                'Customer Number': $scope.exportFinalAllArray[arr].customerno,
                'Customer Contact Name': $scope.exportFinalAllArray[arr].customercontactname,
                'Customer Contact Phone Number': $scope.exportFinalAllArray[arr].customercontactphno,
                'Customer Contact Email. Addrs.': $scope.exportFinalAllArray[arr].customercontactemail,
                'Customer Secondary Contact Name': $scope.exportFinalAllArray[arr].customersecondarycontactname,
                'Customer Secondary Contact Phone Number': $scope.exportFinalAllArray[arr].customersecondarycontactphno,
                'Customer Secondary Contact Email Address': $scope.exportFinalAllArray[arr].customersecondarycontactemail,
                'City': $scope.exportFinalAllArray[arr].city,
                'Service Postal Code': $scope.exportFinalAllArray[arr].servicepostalcode,
                'Service Address 1': $scope.exportFinalAllArray[arr].serviceaddress1,
                'Service Address 2': $scope.exportFinalAllArray[arr].serviceaddress2,
                'Service Address 3': $scope.exportFinalAllArray[arr].serviceaddress3,
                'Service Address 4': $scope.exportFinalAllArray[arr].serviceaddress4,
                'Part Number': $scope.exportFinalAllArray[arr].partnumber,
                'Part Quantity': $scope.exportFinalAllArray[arr].partquantity,
                'Part Status': $scope.exportFinalAllArray[arr].partstatus,
                'Part Status Date': $scope.exportFinalAllArray[arr].partstatusdate,
                'Carrier Name': $scope.exportFinalAllArray[arr].carriername,
                'Waybill Number': $scope.exportFinalAllArray[arr].waybillno,
                'Agent Description': $scope.exportFinalAllArray[arr].agentdescription,
                'Closure Date': $scope.exportFinalAllArray[arr].closuredate,
                'Comments to Vendor': $scope.exportFinalAllArray[arr].commentstovendor,
                'Warranty Invoice Date': $scope.exportFinalAllArray[arr].warrantyinvoicedate,
                'Warranty Invoice Number': $scope.exportFinalAllArray[arr].warrantyinvoiceno,
                'Warranty Bill From State': $scope.exportFinalAllArray[arr].warrantybillfromstate,
                'Original Order BUID': $scope.exportFinalAllArray[arr].originalorderbuid,
                'Reply Code': $scope.exportFinalAllArray[arr].reply_code,
                'User': $scope.exportFinalAllArray[arr].user,
                'Comments': $scope.exportFinalAllArray[arr].comments,
                'Rollback Punched Date /Time ': $scope.exportFinalAllArray[arr].rollbackRequestTime,
                'Rollback Requested Role': $scope.exportFinalAllArray[arr].rollback_requested_role,
                'Rollback Requested User': $scope.exportFinalAllArray[arr].rollback_requested_user,
                'Rollback Status': $scope.exportFinalAllArray[arr].rollback_status,
                'Engineer Id': $scope.exportFinalAllArray[arr].engineerid,
                'Engineer Name': $scope.exportFinalAllArray[arr].engineer_name,
                'Created Date Time': $scope.exportFinalAllArray[arr].createdtime,
                'ETA': $scope.exportFinalAllArray[arr].etatime,
                'Start Date Time': $scope.exportFinalAllArray[arr].starttime,
                'End Date Time': $scope.exportFinalAllArray[arr].endtime,
                'Update Type': $scope.exportFinalAllArray[arr].update_type,
                'Service Identifier': $scope.exportFinalAllArray[arr].serviceidentifier,
                'Service Event': $scope.exportFinalAllArray[arr].service_event,
                'ASP Name': $scope.exportFinalAllArray[arr].aspname,
                'Courier Name': $scope.exportFinalAllArray[arr].couriername,
                'Docket': $scope.exportFinalAllArray[arr].docket,
                'Dispatch Date': $scope.exportFinalAllArray[arr].dispatchdate,
                'Service Event Date Time': $scope.exportFinalAllArray[arr].service_event_date_time
              });

              $scope.valConCount++;
              // console.log($scope.valConCount, $scope.exportFinalAllArray.length, "valcount-exportArray Length-3303")
              // console.log($scope.rcdetailsall.length, $scope.totalRunCnt, $scope.pushwithDataCount1, "$scope.totalRunCnt--$scope.pushwithDataCount1-3304")
              //if ($scope.totalRunCnt == $scope.pushwithDataCount1 + $scope.diffCount) {
              if ($scope.rcdetailsall.length == $scope.pushwithDataCount1) {
                if ($scope.exportFinalAllArray.length == $scope.valConCount) {
                  //$scope.isExport = false;
                  alasql('SELECT * INTO XLSX("AllDumplist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                }
              }
            }
          }
        }
      });
    };

  });
