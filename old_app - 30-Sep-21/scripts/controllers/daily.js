'use strict';

angular.module('secondarySalesApp')
  .controller('DailyCtrl', function ($scope, $http, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/


    $scope.daily = {
      customerId: $window.sessionStorage.customerId,
    };

    Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
      $scope.customers = cust;
    });

    Restangular.one('customers?filter[where][id]=' + $window.sessionStorage.customerId).get().then(function (cus) {
      $scope.customerName = cus[0].name;

      Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + $window.sessionStorage.customerId).getList().then(function (catgresp) {
        $scope.categories = catgresp;
        $scope.daily.categoryId = catgresp[0].id;
      });
    });

    $scope.$watch('daily.categoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        return;
      } else {
        Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
          $scope.subcategories = subcatgresp;
        });
        $scope.months = [{ name: 'Jan', value: '01' }, { name: 'Feb', value: '02' }, { name: 'Mar', value: '03' }, { name: 'Apr', value: '04' }, { name: 'May', value: '05' }, { name: 'June', value: '06' }, { name: 'July', value: '07' }, { name: 'Aug', value: '08' }, { name: 'Sep', value: '09' }, { name: 'Oct', value: '10' }, { name: 'Nov', value: '11' }, { name: 'Dec', value: '12' }];
      }
    });

    $scope.$watch('daily.subcategoryId', function (newValue, oldValue) {
      if (newValue == '' || newValue == null || newValue == oldValue) {
        $("#table1").hide();
        return;

      } else {
        Restangular.one('subcategories?filter[where][id]=' + newValue).get().then(function (scat) {
          $scope.subCategoryName = scat[0].name;
          $("#yourTextBox").hide();
          $("#yourTextBox1").hide();
          $("#yourTextBox2").hide();
          $("#table2").hide();
          $("#table1").show();
        });

        $scope.organisationlevels = [];

        Restangular.one('customers/findOne?filter[where][id]=' + $scope.daily.customerId).get().then(function (cust) {
          Restangular.one('organisationlocations/findOne?filter[where][id]=' + cust.orgroot).get().then(function (orglevel) {
            Restangular.one('organisationlevels/findOne?filter[where][id]=' + orglevel.level).get().then(function (orglvl) {
              $scope.organisationlevels.push(orglvl);
              Restangular.all('organisationlevels?filter[where][rootid]=' + orglvl.id + '&filter[where][languageparent]=true&filter[where][deleteflag]=false' + '&filter[order]=slno ASC').getList().then(function (ogrlevls) {
                angular.forEach(ogrlevls, function (data, index) {
                  $scope.organisationlevels.push(data);
                });

                angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                  Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + organisationlevel.id).getList().then(function (ogrlocs) {
                    organisationlevel.index = index;
                    organisationlevel.organisationlocations = ogrlocs;

                    // for (var j = 0; j < $scope.orgstructure.length; j++) {
                    //   $scope.LevelId = organisationlevel.languageparent == true ? organisationlevel.id : organisationlevel.languageparentid;
                    //   if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
                    //     organisationlevel.locationid = $scope.orgstructure[j].split("-")[1].split(",");
                    //   }
                    // }
                  });
                });
              });
            });
          });
        });
      }
    });

    $scope.$watch('daily.monthid', function (newValue, oldValue) {
      if (newValue == '' || newValue == null) {
        return;
      } else {
        var url = 'surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' +
          $scope.daily.customerId +
          ']}}]},{"categoryId":{"inq":[' +
          $scope.daily.categoryId +
          ']}}]},{"subcategoryId":{"inq":[' +
          $scope.daily.subcategoryId +
          ']}}]}]}}';

        if ($scope.selectedData == 1) {
          var dailyanswers_url = 'dailyanswers?filter[where][data1]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 2) {
          var dailyanswers_url = 'dailyanswers?filter[where][data2]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 3) {
          var dailyanswers_url = 'dailyanswers?filter[where][data3]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 4) {
          var dailyanswers_url = 'dailyanswers?filter[where][data4]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 5) {
          var dailyanswers_url = 'dailyanswers?filter[where][data5]=' + $scope.selectedLevel;
        } else if ($scope.selectedData == 5) {
          var dailyanswers_url = 'dailyanswers?filter[where][data5]=' + $scope.selectedLevel;
        }

        Restangular.all(url).getList().then(function (questions) {
          $scope.surveyquestions = questions;

          Restangular.all(dailyanswers_url).getList().then(function (answers) {
            $scope.dailyanswers = answers;

            angular.forEach($scope.surveyquestions, function (question, index) {
              var data01 = $scope.dailyanswers.filter(function (arr) {
                var arrDat = arr.workdate ? arr.workdate.split('/') : [];
                return arr.questionid == question.id && arrDat[0] == newValue
              });
              // console.log('data01', data01);
              angular.forEach(data01, function (answer, index) {
                var ansArray = answer.workdate.split('/');
                var ansObj = ansArray[1];
                question[ansObj] = answer.answer;
              });
            });
          });
        });
      }
    });

    $scope.changeMonth = function (currentMonth) {
      var url = 'surveyquestions?filter={"where":{"and":[{"and":[{"and":[{"and":[{"and":[{"platform":{"inq":["web","web and mobile"]}},{"deleteflag":{"inq":[false]}}]},{"customerId":{"inq":[' +
        $scope.daily.customerId +
        ']}}]},{"categoryId":{"inq":[' +
        $scope.daily.categoryId +
        ']}}]},{"subcategoryId":{"inq":[' +
        $scope.daily.subcategoryId +
        ']}}]}]}}';

      if ($scope.selectedData == 1) {
        var dailyanswers_url = 'dailyanswers?filter[where][data1]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 2) {
        var dailyanswers_url = 'dailyanswers?filter[where][data2]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 3) {
        var dailyanswers_url = 'dailyanswers?filter[where][data3]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 4) {
        var dailyanswers_url = 'dailyanswers?filter[where][data4]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 5) {
        var dailyanswers_url = 'dailyanswers?filter[where][data5]=' + $scope.selectedLevel;
      } else if ($scope.selectedData == 5) {
        var dailyanswers_url = 'dailyanswers?filter[where][data5]=' + $scope.selectedLevel;
      }

      Restangular.all(url).getList().then(function (questions) {
        $scope.surveyquestions = questions;

        Restangular.all(dailyanswers_url).getList().then(function (answers) {
          $scope.dailyanswers = answers;

          angular.forEach($scope.surveyquestions, function (question, index) {
            var data01 = $scope.dailyanswers.filter(function (arr) {
              var arrDat = arr.workdate ? arr.workdate.split('/') : [];
              return arr.questionid == question.id && arrDat[0] == currentMonth
            });
            // console.log('question', question);
            angular.forEach(data01, function (answer, index) {
              var ansArray = answer.workdate.split('/');
              var ansObj = ansArray[1];
              question[ansObj] = answer.answer;
            });
          });
        });
      });
    };

    $scope.orgLevelChange = function (index, level) {
      $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + level).getList().then(function (ogrlocsresp) {
        if (index < $scope.organisationlevels.length && level.length > 0) {
          $scope.organisationlevels[index].organisationlocations = ogrlocsresp;
        }
      });
      // console.log('level', level, index);
      $scope.selectedData = index;
      $scope.selectedLevel = level;

      var currentdate = new Date();
      var currentMonth = (currentdate.getMonth() < 9 ? '0' : '') + (currentdate.getMonth() + 1);
      $scope.daily.monthid = currentMonth;
      $scope.changeMonth(currentMonth);
    };

    $scope.stakeholderdataModal1 = false;
    $scope.stakeholderdataModal2 = false;
    $scope.stakeholderdataModal3 = false;

    $scope.clickAnswer = function (surveyquestion, data) {
      if (surveyquestion.questiontype == 7) {
        $scope.stakeholderdataModal1 = true;
        $scope.modalTitle = 'Date';
        $scope.dateDisplay = $filter('date')(data, "dd-MM-yyyy hh:mm a");
      } else if (surveyquestion.questiontype == 8) {
        $scope.stakeholderdataModal2 = true;
        $scope.modalTitle = 'Location';
        var lngLng = data.split(',');
        var myLat = parseInt(lngLng[0]);
        var myLng = parseInt(lngLng[1]);

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: new google.maps.LatLng(myLat, myLng),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(myLat, myLng),
          map: map,
        });

      } else if (surveyquestion.questiontype == 9) {
        $scope.stakeholderdataModal3 = true;
        $scope.modalTitle = 'Image';
        $scope.imageSource = '//citly.s3.ap-south-1.amazonaws.com/' + data;
      }
    };
  })
  .directive('modal4', function () {
    return {
      template: '<div class="modal fade" data-backdrop="static">' +
        '<div class="modal-dialog modal-md">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="btn" style="float: right" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title">{{ title1 }}</h4>' +
        '</div>' +
        '<div class="modal-body" ng-transclude></div>' +
        '</div>' +
        '</div>' +
        '</div>',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: true,
      link: function postLink(scope, element, attrs) {
        scope.title1 = attrs.title1;
        scope.$watch(attrs.visible, function (value) {
          // console.log('value', value);
          if (value == true) {
            //console.log('elementif', element[0]);
            $(element).modal('show');
            // document.getElementsByClassName("modal-dialog").modal='show';
          } else {
            // console.log('elementelse', element[0]);
            $(element).modal('hide');
            //document.getElementsByClassName("modal-dialog").modal='hide';
          }
        });

        $(element).on('shown.bs.modal', function () {
          scope.$apply(function () {
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function () {
          scope.$apply(function () {
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });