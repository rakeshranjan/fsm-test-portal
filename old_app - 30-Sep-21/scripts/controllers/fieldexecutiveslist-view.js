'use strict';

angular.module('secondarySalesApp')
  .controller('FieldexListCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {



    /*********************************** Pagination *******************************************/

    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
      $window.sessionStorage.myRoute = null;
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    if ($window.sessionStorage.prviousLocation != "partials/fieldexecutiveslist-view" || $window.sessionStorage.prviousLocation != "partials/fieldexecutiveslist-view") {
      $window.sessionStorage.myRoute_currentPage = 1;
      $window.sessionStorage.myRoute_currentPagesize = 25;
    }

    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
      $scope.currentpage = newPage;
      $window.sessionStorage.myRoute_currentPage = newPage;
    };

    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
      $scope.pageSize = mypage;
      $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    $scope.todolanguage = Restangular.one('todolanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;


    //        $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

    $scope.todo = {
      memberid: '',
      followupdate: '',
      deleteflag: false,
      lastmodifiedby: $window.sessionStorage.userId,
      lastmodifiedrole: $window.sessionStorage.roleId,
      lastmodifiedtime: new Date(),
      createdby: $window.sessionStorage.userId,
      createdtime: new Date(),
      createdrole: $window.sessionStorage.roleId
    };

    /************************************* SORTING ***************************************************/

    $scope.sort = {
      active: '',
      descending: undefined
    }

    $scope.changeSorting = function (column) {

      var sort = $scope.sort;

      if (sort.active == column) {
        sort.descending = !sort.descending;

      } else {
        sort.active = column;
        sort.descending = false;
      }
    };

    $scope.getIcon = function (column) {

      var sort = $scope.sort;

      if (sort.active == column) {
        return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
      }
    }

    /************************************* SORTING ***************************************************/
    /* $scope.getLocation = function (location) {
         return Restangular.one('organisationlocations', location).get().$object;
     };
     $scope.getLevel = function (level) {
         return Restangular.one('organisationlevels', level).get().$object;
     }; */

    /************************************* INDEXING ***************************************************/
   /* if ($window.sessionStorage.prviousLocation != "fieldexecutiveslist-view") {
      $window.sessionStorage.user_zoneId = '';
      $window.sessionStorage.user_facilityId = '';
      $window.sessionStorage.user_currentPage = 1;
      $window.sessionStorage.user_currentPagesize = 25;
      $scope.managercity = [];
      $scope.managerallcity = [];
      $scope.arrayManagersCity = []; //by satyen

      console.log($window.sessionStorage.orgStructure, "$window.sessionStorage.orgStructure-94")

      Restangular.one('users/findOne?filter[where][orgStructure][like]=%' + $window.sessionStorage.orgStructure + '%').get().then(function (orgstrc) {
        console.log("OrgStructure: ", orgstrc.orgStructure);
        var orgStructureSplit = orgstrc.orgStructure.split("-");
        var mngrcities = orgStructureSplit[1].split(",");

        //console.log(mngrcities.length, "MANAGER-LENGTH")
        for (var i = 0; i < mngrcities.length; i++) {
          $scope.managercity.push(orgStructureSplit[0] + "-" + mngrcities[i]);
        }
        console.log($scope.managercity, "MANAGER-CITY")
        console.log(orgstrc.orgStructure, "ORG-STRUCT-105")
        // $scope.led = Restangular.all('users?filter[where][roleId]=' + 11 + '&filter[where][deleteflag]=false' + '&filter[where][orgstructure][like]=%' + orgstrc.orgStructure).getList().then(function (response) {
        //Restangular.all('users?filter[where][roleId]=' + 11 + '&filter[where][deleteflag]=false' + '&filter[where][orgstructure][like]=%' + orgstrc.orgStructure).getList().then(function (response) {
        var indexcount = 0;
        // for (var j = 0; j < mngrcities.length; j++) {
        //   //console.log(mngrcities[j], "MGR-CITY-111")
        //   console.log($scope.managercity[j], j, "MANAGER-CITY-112")
          // Restangular.all('users?filter[where][roleId]=' + 11 + '&filter[where][deleteflag]=false' + '&filter[where][orgStructure][like]=%' + orgstrc.orgStructure + '%').getList().then(function (response) { //by me last %
          Restangular.all('users?filter[where][roleId]=' + 11 + '&filter[where][deleteflag]=false' + '&filter[where][orgStructure][like]=%' + $window.sessionStorage.orgStructure + '%').getList().then(function (response) { //by me last %
            // $scope.userUrl = 'users?filter={"where":{"and":[{"orgStructure":{"inq":[' + $scope.managercity + ']}}]}}';
              
            $scope.users = response;
            console.log("Field Executives", response);
            $scope.managerallcity = $scope.users;

            console.log($scope.managerallcity, "$scope.managerallcity-121")

            angular.forEach($scope.users, function (member, index) {
              member.index = index + 1;
              indexcount = indexcount + 1;
              // var membersplit = member.orgStructure.split(";");
              // var memberCity = membersplit[0].split("-");
              // var memberpin = membersplit[1].split("-");
              // var memberpincode = memberpin[1].split(",");

              // Restangular.all('organisationlocations?filter[where][deleteflag]=false' + '&filter[where][parentlevel]=null').getList().then(function (cities) {
              //   $scope.cityName = cities;
              //   for (var j = 0; j < $scope.cityName.length; j++) {

              //     if (memberCity[1] == $scope.cityName[j].id) {
              //       member.cityname = $scope.cityName[j].name;
              //       break;
              //     }
              //   }
              // });
              // //console.log($scope.managercityname, "CITY-NAME-143")
              // // Restangular.all('organisationlocations?filter[where][deleteflag]=false' + '&filter[where][neq][parentlevel]=null').getList().then(function (pincodes) {
              // //   $scope.pinCode = pincodes;
              // //   //console.log("PinCodes: ", $scope.pinCode);
              // //   for (var j = 0; j < $scope.pinCode.length; j++) {
              // //     for (var i = 0; i < memberpincode.length; i++) {
              // //       if (memberpincode[i] == $scope.pinCode[j].id) {
              // //         member.pincode = $scope.pinCode[j].name;
              // //         break;
              // //       }
              // //       break;
              // //     }
              // //   }
              // // });
              // Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
              //   $scope.organisationlocations = organisationlocations;
              //   if (member.orgStructure != null) {
              //     member.locationname = "";
              //     var array = member.orgStructure.split(';');
              //     var subarray = array[array.length - 1].split('-')[1].split(',');
              //     for (var i = 0; i < $scope.organisationlocations.length; i++) {
              //       for (var j = 0; j < subarray.length; j++) {
              //         if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
              //           if (member.locationname === "") {
              //             member.locationname = $scope.organisationlocations[i].name;
              //           } else {
              //             member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
              //           }
              //         }
              //       }
              //     }
              //     member.location = array[array.length - 1].split('-')[1];
              //   }
              // });

              //console.log(member.cityname, member.locationname, "CITY-LOCATION")
              $scope.arrayManagersCity.push({
                index: indexcount,
                username: member.username,
                mobile: member.mobile,
                orgStructure: member.orgStructure
              });
            });
            $scope.users = $scope.arrayManagersCity;
            //console.log($scope.users, "USERS-200")

            angular.forEach($scope.users, function (member, index) {
              member.index = index + 1;
              var membersplit = member.orgStructure.split(";");
              var memberCity = membersplit[2].split("-");
              var memberpin = membersplit[1].split("-");
              var memberpincode = memberpin[1].split(",");

              // Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (cities) {
              //   $scope.cityName = cities;
              //   for (var j = 0; j < $scope.cityName.length; j++) {

              //     if (memberCity[1] == $scope.cityName[j].id) {
              //       member.cityname = $scope.cityName[j].name;
              //       break;
              //     }
              //   }
              // });

              Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (cities) {
                $scope.cityName = cities;
                if (member.orgStructure != null) {
                  member.cityname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 2].split('-')[1].split(',');
                  for (var i = 0; i < $scope.cityName.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.cityName[i].id + "") {
                        if (member.cityname === "") {
                          member.cityname = $scope.cityName[i].name;
                        } else {
                          member.cityname = member.cityname + ", " + $scope.cityName[i].name;
                        }
                      }
                    }
                  }
                  member.cityn = array[array.length - 2].split('-')[1];
                }
              });

              Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                $scope.organisationlocations = organisationlocations;
                if (member.orgStructure != null) {
                  member.locationname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 1].split('-')[1].split(',');
                  for (var i = 0; i < $scope.organisationlocations.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
                        if (member.locationname === "") {
                          member.locationname = $scope.organisationlocations[i].name;
                        } else {
                          member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
                        }
                      }
                    }
                  }
                  member.location = array[array.length - 1].split('-')[1];
                }
              });

            });

          });
      //  }

        //console.log($scope.arrayManagersCity, "Final manager city-196")
        // console.log($scope.users, "USERS-198")
      });
    } */

// New codes for Multi states & city

    $scope.searchbulkupdate = '';

    $scope.orgStructureSplit = $window.sessionStorage.orgStructure && $window.sessionStorage.orgStructure != 'null' ? $window.sessionStorage.orgStructure.split(";") : "";
    //console.log($scope.orgStructureSplit, "$scope.orgStructureSplit-428")
    $scope.CountrySplit = $scope.orgStructureSplit[0];
    $scope.ZoneSplit = $scope.orgStructureSplit[1];
    //console.log($scope.CountrySplit, "$scope.CountrySplit-430")
    $scope.intermediateOrgStructure = "";
    for (var i = 0; i < $scope.orgStructureSplit.length - 1; i++) {
      if ($scope.intermediateOrgStructure === "") {
        $scope.intermediateOrgStructure = $scope.orgStructureSplit[i];
      } else {
        $scope.intermediateOrgStructure = $scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[i];
      }
    }
    //console.log($scope.intermediateOrgStructure, "$scope.intermediateOrgStructure-436")
    //--- No need now (Let it be because LHS menu somewhere linked) - It was for single state & multi city ---------
    $scope.orgStructureFilterArray = [];
    if ($scope.orgStructureSplit.length > 0) {
      $scope.LocationsSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
      console.log($scope.LocationsSplit, "$scope.LocationsSplit-444")
      for (var i = 0; i < $scope.LocationsSplit.length; i++) {
        $scope.orgStructureFilterArray.push($scope.intermediateOrgStructure + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
      }

    }
    console.log($scope.orgStructureFilterArray, "$scope.orgStructureFilterArray-451")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", "");
    console.log("$scope.orgStructureFilter", JSON.stringify($scope.orgStructureFilterArray).replace("[", "").replace("]", ""));
    //-- $scope.orgStructureUptoStateFilter below is being used for Role - 5 only --------------
    $scope.orgStructureUptoStateFilter = $scope.orgStructureFilter;
    console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-454")
    //--- End of No need now - It was for single state & multi city ---------

//-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
  $scope.orgStructureZoneFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
    for (var i = 0; i < $scope.ZoneSplit.length; i++) {
      $scope.orgStructureZoneFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.ZoneSplit[i]);
    }
  }

  //-- For Multi Zone, state & multi City array filter work by Ravi ---------------------------------
  $scope.orgStructureStateFilterArray = [];
  if ($scope.orgStructureSplit.length > 1) {
    $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[1].split(",");
  //  $scope.ZoneSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 3].split("-")[1].split(",");
    console.log($scope.StatesSplit, "$scope.StatesSplit-461")
   // console.log($scope.ZoneSplit, "$scope.ZoneSplit-461")
   for (var k = 0; k < $scope.orgStructureZoneFilterArray.length; k++) {
    for (var i = 0; i < $scope.StatesSplit.length; i++) {
      // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
      $scope.orgStructureStateFilterArray.push($scope.orgStructureZoneFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.StatesSplit[i]);
    }
   }
  }

    // //-- For Multi state & multi City array filter work Satyen ---------------------------------
    // $scope.orgStructureStateFilterArray = [];
    // // if ($scope.orgStructureSplit.length > 0) {
    //   if ($scope.orgStructureSplit.length > 1) {
    //   $scope.StatesSplit = $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[1].split(",");
    //   console.log($scope.StatesSplit, "$scope.StatesSplit-461")
    //   for (var i = 0; i < $scope.StatesSplit.length; i++) {
    //    // $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //    $scope.orgStructureStateFilterArray.push($scope.CountrySplit + ";" + $scope.ZoneSplit + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 2].split("-")[0] + "-" + $scope.StatesSplit[i]);
    //   }
    // }
    //console.log($scope.orgStructureStateFilterArray, "$scope.orgStructureStateFilterArray-468")
    $scope.orgStructureFinalFilterArray = [];
    if ($scope.orgStructureStateFilterArray.length > 0) {
      for (var k = 0; k < $scope.orgStructureStateFilterArray.length; k++) {
        for (var i = 0; i < $scope.LocationsSplit.length; i++) {
          $scope.orgStructureFinalFilterArray.push($scope.orgStructureStateFilterArray[k] + ";" + $scope.orgStructureSplit[$scope.orgStructureSplit.length - 1].split("-")[0] + "-" + $scope.LocationsSplit[i]);
        }
      }
    }
    //console.log($scope.orgStructureFinalFilterArray, "$scope.orgStructureFinalFilterArray-477")
    $scope.orgStructureFilter = JSON.stringify($scope.orgStructureFinalFilterArray).replace("[", "").replace("]", "");
// New code added for Country & Zone level
    if (window.sessionStorage.roleId == 2 || window.sessionStorage.roleId == 3 || window.sessionStorage.roleId == 4 || window.sessionStorage.roleId == 8) {
      
      var userUrl = '';
      if($window.sessionStorage.isPartner === 'true'){
        userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId;
      }else{
        userUrl = 'users?filter[where][deleteflag]=false&filter[where][orgStructure][like]=%' + window.sessionStorage.orgStructure + '%' + '&filter[where][roleId]=' + 11
      }
      Restangular.all(userUrl).getList().then(function (membr) {

            $scope.users = membr;
            // console.log($scope.membersFinal, "$scope.membersFinal-602")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-603")
             console.log($scope.users, "Users for Senior manager")

            angular.forEach($scope.users, function (member, index) {
              member.index = index + 1;

              var membersplit = member.orgStructure.split(";");
            //  var memberCity = membersplit[2].split("-");
              var memberpin = membersplit[1].split("-");
              var memberpincode = memberpin[1].split(",");

              Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (cities) {
                $scope.cityName = cities;
                if (member.orgStructure != null) {
                  member.cityname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 2].split('-')[1].split(',');
                  for (var i = 0; i < $scope.cityName.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.cityName[i].id + "") {
                        if (member.cityname === "") {
                          member.cityname = $scope.cityName[i].name;
                        } else {
                          member.cityname = member.cityname + ", " + $scope.cityName[i].name;
                        }
                      }
                    }
                  }
                  member.cityn = array[array.length - 2].split('-')[1];
                }
              });

              Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                $scope.organisationlocations = organisationlocations;
                if (member.orgStructure != null) {
                  member.locationname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 1].split('-')[1].split(',');
                  for (var i = 0; i < $scope.organisationlocations.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
                        if (member.locationname === "") {
                          member.locationname = $scope.organisationlocations[i].name;
                        } else {
                          member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
                        }
                      }
                    }
                  }
                  member.location = array[array.length - 1].split('-')[1];
                }
              });

              
            });
          });
        
    }

   else if (window.sessionStorage.roleId == 5 || window.sessionStorage.roleId == 6) {
      console.log($scope.orgStructureFilter, "ORGSTRUCT-FILTER-570")
        //-- Previous code of Single state & multi city was same as was in it is commented for role == 7 and 15 -----
        console.log($scope.orgStructureUptoStateFilter, "$scope.orgStructureUptoStateFilter-574")
        //var str = $scope.orgStructureFilter; //previously
        var str = $scope.orgStructureUptoStateFilter;
        var strOrgstruct = str.split('"')
        //console.log(strOrgstruct[1], "strorgstruct-578", strOrgstruct)

        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-586")

        // from here ---
        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          var userUrl = '';
      if($window.sessionStorage.isPartner === 'true'){
        $scope.membersFinal = [];
        userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId;
      }else{
        userUrl = 'users?filter[where][deleteflag]=false&filter[where][orgStructure][like]=%' + $scope.strOrgstructFinal[k] + '%' + '&filter[where][roleId]=' + 11
      }
          Restangular.all(userUrl).getList().then(function (membr) {

            $scope.membersnew = membr;
            //console.log($scope.membersnew, "TICKET-DISPLAY-595")
            if($window.sessionStorage.isPartner === 'true'){
              $scope.membersFinal = [];
            }
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.users = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-602")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-603")
             console.log($scope.users, "Users for Senior manager")

            angular.forEach($scope.users, function (member, index) {
              member.index = index + 1;

              var membersplit = member.orgStructure.split(";");
            //  var memberCity = membersplit[2].split("-");
              var memberpin = membersplit[1].split("-");
              var memberpincode = memberpin[1].split(",");

              Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (cities) {
                $scope.cityName = cities;
                if (member.orgStructure != null) {
                  member.cityname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 2].split('-')[1].split(',');
                  for (var i = 0; i < $scope.cityName.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.cityName[i].id + "") {
                        if (member.cityname === "") {
                          member.cityname = $scope.cityName[i].name;
                        } else {
                          member.cityname = member.cityname + ", " + $scope.cityName[i].name;
                        }
                      }
                    }
                  }
                  member.cityn = array[array.length - 2].split('-')[1];
                }
              });

              Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                $scope.organisationlocations = organisationlocations;
                if (member.orgStructure != null) {
                  member.locationname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 1].split('-')[1].split(',');
                  for (var i = 0; i < $scope.organisationlocations.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
                        if (member.locationname === "") {
                          member.locationname = $scope.organisationlocations[i].name;
                        } else {
                          member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
                        }
                      }
                    }
                  }
                  member.location = array[array.length - 1].split('-')[1];
                }
              });

              
            });
          });
        }
    }

    else if (window.sessionStorage.roleId == 7 || window.sessionStorage.roleId == 15) {
      var str = $scope.orgStructureFilter;
        var strOrgstruct = str.split('"');
        //console.log(strOrgstruct[1], "strorgstruct-638", strOrgstruct)
        $scope.strOrgstructFinal = [];
        for (var x = 0; x < strOrgstruct.length; x++) {
          if (x % 2 != 0) {
            $scope.strOrgstructFinal.push(strOrgstruct[x]);
          }
        }
        //console.log($scope.strOrgstructFinal, "$scope.strOrgstructFinal-651")

        // $scope.count = 0;
        $scope.membersFinal = [];
        for (var k = 0; k < $scope.strOrgstructFinal.length; k++) {
          // Restangular.all('users?filter[where][deleteflag]=false&filter[where][orgStructure][like]=%' + $scope.strOrgstructFinal[k] + '%').getList().then(function (membr) {
            var userUrl = '';
            if($window.sessionStorage.isPartner === 'true'){
              $scope.membersFinal = [];
              userUrl = 'users?filter[where][managerId]='+$window.sessionStorage.userId;
            }else{
              userUrl = 'users?filter[where][deleteflag]=false&filter[where][orgStructure][like]=%' + $scope.strOrgstructFinal[k] + '%' + '&filter[where][roleId]=' + 11
            }  
          Restangular.all(userUrl).getList().then(function (membr) {

            $scope.membersnew = membr;
            console.log($scope.membersnew, "TICKET-DISPLAY-662")
            if($window.sessionStorage.isPartner === 'true'){
              $scope.membersFinal = [];
            }
            if ($scope.membersnew.length > 0) {
              angular.forEach($scope.membersnew, function (value, index) {
                $scope.membersFinal.push(value);
              });
            }
            $scope.users = $scope.membersFinal;
            // console.log($scope.membersFinal, "$scope.membersFinal-669")
            // console.log($scope.membersFinal.length, "$scope.membersFinal-670")
             console.log($scope.users, "Users for manager")

            angular.forEach($scope.users, function (member, index) {
              member.index = index + 1;

              var membersplit = member.orgStructure.split(";");
              var memberCity = membersplit[2].split("-");
              var memberpin = membersplit[1].split("-");
              var memberpincode = memberpin[1].split(",");

              Restangular.all('organisationlocations?filter[where][deleteflag]=false').getList().then(function (cities) {
                $scope.cityName = cities;
                if (member.orgStructure != null) {
                  member.cityname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 2].split('-')[1].split(',');
                  for (var i = 0; i < $scope.cityName.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.cityName[i].id + "") {
                        if (member.cityname === "") {
                          member.cityname = $scope.cityName[i].name;
                        } else {
                          member.cityname = member.cityname + ", " + $scope.cityName[i].name;
                        }
                      }
                    }
                  }
                  member.cityn = array[array.length - 2].split('-')[1];
                }
              });

              Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                $scope.organisationlocations = organisationlocations;
                if (member.orgStructure != null) {
                  member.locationname = "";
                  var array = member.orgStructure.split(';');
                  var subarray = array[array.length - 1].split('-')[1].split(',');
                  for (var i = 0; i < $scope.organisationlocations.length; i++) {
                    for (var j = 0; j < subarray.length; j++) {
                      if (subarray[j] + "" === $scope.organisationlocations[i].id + "") {
                        if (member.locationname === "") {
                          member.locationname = $scope.organisationlocations[i].name;
                        } else {
                          member.locationname = member.locationname + ", " + $scope.organisationlocations[i].name;
                        }
                      }
                    }
                  }
                  member.location = array[array.length - 1].split('-')[1];
                }
              });

              
            });
          });
        }
    }


  });
