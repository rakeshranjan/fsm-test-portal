'use strict';

angular.module('secondarySalesApp')
    .controller('WorkFlowPriorityCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/workflow-priority/create' || $location.path() === '/workflow-priority/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflow-priority/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflow-priority/create' || $location.path() === '/workflow-priority/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflow-priority/create' || $location.path() === '/workflow-priority/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflow-priority") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('workflows?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wf) {
            $scope.workflows = wf;
        });

        Restangular.all('workflowpriorities?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wfcp) {
            $scope.workflowpriorities = wfcp;

            angular.forEach($scope.workflowpriorities, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.workflowdisplayId = '';

        $scope.$watch('workflowdisplayId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('workflowpriorities?filter[where][deleteflag]=false&filter[where][parentFlag]=true' + '&filter[where][workflowid]=' + newValue).getList().then(function (wfcp) {
                    $scope.workflowpriorities = wfcp;

                    angular.forEach($scope.workflowpriorities, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        /************************ end ********************************************************/

        $scope.DisplayWorkflowPriorities = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayWorkflowPriorities.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayWorkflowPriorities.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayWorkflowPriorities[index].disableLang = false;
                $scope.DisplayWorkflowPriorities[index].disableAdd = false;
                $scope.DisplayWorkflowPriorities[index].disableRemove = false;
                $scope.DisplayWorkflowPriorities[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayWorkflowPriorities[index].disableLang = true;
                $scope.DisplayWorkflowPriorities[index].disableAdd = false;
                $scope.DisplayWorkflowPriorities[index].disableRemove = false;
                $scope.DisplayWorkflowPriorities[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.validatestring = "";

        $scope.SaveLang = function () {

            document.getElementById('order').style.borderColor = "";

            if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.DisplayWorkflowPriorities[$scope.lastIndex].disableAdd = true;
                $scope.DisplayWorkflowPriorities[$scope.lastIndex].disableRemove = true;

                $scope.DisplayWorkflowPriorities[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
                $scope.DisplayWorkflowPriorities[$scope.lastIndex].default = $scope.myobj.default;
                $scope.DisplayWorkflowPriorities[$scope.lastIndex].languages = angular.copy($scope.languages);

//                angular.forEach($scope.languages, function (data, index) {
//                    data.inx = index + 1;
//                    $scope.DisplayWorkflowPriorities[$scope.lastIndex]["lang" + data.inx] = data.lang;
//                    console.log($scope.DisplayWorkflowPriorities);;
//                });

                $scope.langModel = false;
            }

        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflowpriorities/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveWflowPriority();
        };

        var saveCount = 0;

        $scope.saveWflowPriority = function () {

            if (saveCount < $scope.DisplayWorkflowPriorities.length) {

                if ($scope.DisplayWorkflowPriorities[saveCount].name == '' || $scope.DisplayWorkflowPriorities[saveCount].name == null) {
                    saveCount++;
                    $scope.saveWflowPriority();
                } else {

                    $scope.DisplayWorkflowPriorities[saveCount].workflowid = $scope.workflow;
                    $scope.DisplayWorkflowPriorities[saveCount].workflowstatusid = $scope.workflowstatus;
                    $scope.DisplayWorkflowPriorities[saveCount].workflowcategoryid = $scope.workflowcategory;

                    Restangular.all('workflowpriorities').post($scope.DisplayWorkflowPriorities[saveCount]).then(function (resp) {
//                        saveCount++;
//                        $scope.saveWflowPriority(); 
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayWorkflowPriorities[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayWorkflowPriorities[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayWorkflowPriorities[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                workflowid: resp.workflowid
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/workflow-priority-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('workflowpriorities').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveWflowPriority();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.workflowpriority.name == '' || $scope.workflowpriority.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Priority Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.workflowpriority.lastmodifiedby = $window.sessionStorage.userId;
                $scope.workflowpriority.lastmodifiedtime = new Date();
                $scope.workflowpriority.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflowpriorities', $routeParams.id).customPUT($scope.workflowpriority).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };

        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('workflowpriorities', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/workflow-priority-list';
            }

        }

        $scope.getworkflow = function (workflowid) {
            return Restangular.one('workflows', workflowid).get().$object;
        };

        $scope.getworkflowstatus = function (workflowstatusid) {
            return Restangular.one('workflowstatuses', workflowstatusid).get().$object;
        };

        $scope.getworkflowcategory = function (workflowcategoryid) {
            return Restangular.one('workflowcategories', workflowcategoryid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};
            $scope.myobj.orderNo = $scope.workflowpriority.orderNo;
            $scope.myobj.default = $scope.workflowpriority.default;

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.workflowpriority.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
             angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if(data.id === lngwrkflw.language){
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });
            $scope.workflowpriority.orderNo = $scope.myobj.orderNo;
            $scope.workflowpriority.default = $scope.myobj.default;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Priority has been Updated!';

            Restangular.one('workflowpriorities', $routeParams.id).get().then(function (workflowpriority) {
                Restangular.all('workflowpriorities?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                $scope.original = workflowpriority;
                $scope.workflowpriority = Restangular.copy($scope.original);
                });
            });

        } else {
            $scope.message = 'Priority has been Created!';
        }

    });
