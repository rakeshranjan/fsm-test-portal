'use strict';

angular.module('secondarySalesApp')
    .controller('COArchieveCtrl', function ($scope, Restangular, $route,$window) {
        $scope.employees = Restangular.all('employees').getList().$object;
        $scope.SalesManager = null;
        $scope.Distributor = null;
        $scope.Route = null;
        $scope.salesmanagerid = null;
        $scope.distributorid = null;

        $scope.zones = Restangular.all('zones').getList().$object;
        $scope.salesAreas = Restangular.all('sales-areas').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.distributionSubAreas = Restangular.all('distribution-subareas').getList().$object;


        $scope.searchPart = $scope.firstName;
        /*********************************************************** GET *********************************************/
        $scope.$watch('zoneId', function (newValue, oldValue) {
            $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
        });

        $scope.$watch('salesAreaId', function (newValue, oldValue) {
            $scope.distributionAreas = Restangular.all('distribution-areas?filter[where][salesAreaId]=' + newValue).getList().$object;
        });



        /******************************************************************* DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('partners/' + id).remove($scope.partner).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

        /********************************************** WATCH *************************************************/
        $scope.zoneId = '';
        $scope.zonalid = '';
        $scope.salesAreaId = '';
        $scope.salesid = '';
        $scope.distributionAreaId = '';
        $scope.distribtionareaid = '';
        $scope.partnerId = '';

        $scope.$watch('zoneId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;

                $scope.sal = Restangular.all('partners?filter[where][zoneId]=' + newValue + '&filter[where][groupId]=10').getList().then(function (sal) {
                    $scope.partners = sal;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                    });
                });
                $scope.zonalid = +newValue;
            }
        });

        /****************************************** salesAreaId ************************************/

        $scope.$watch('salesAreaId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.filterpartners = Restangular.all('partners?filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=8').getList().$object;
                $scope.sal = Restangular.all('partners?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=10').getList().then(function (sal) {
                    $scope.partners = sal;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                    });
                });

                $scope.salesid = +newValue;
            }
        });


        /****************************************** partnerId ************************************/

        $scope.$watch('partnerId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                console.log('newValue', newValue);
                $scope.sal = Restangular.all('partners?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][coorgId]=' + newValue + '&filter[where][groupId]=10').getList().then(function (sal) {
                    $scope.partners = sal;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                    });
                });

                $scope.partid = +newValue;
            }
        });

        /*
      $scope.$watch('distributionAreaId', function (newValue, oldValue) {
     // $scope.partners = Restangular.all('partners?filter[where][distributionAreaId]=' + newValue).getList().$object;  
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.sal = Restangular.all('partners?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' +  $scope.salesid + '&filter[where][distributionAreaId]=' +newValue +'&filter[where][groupId]=10').getList().then(function (sal) {
                    $scope.partners = sal;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                    });
                });

          
            $scope.distribtionareaid = +newValue;
            }
        });
   
    */



        /************************************************************ INDEX *************************************/
        $scope.part = Restangular.all('partners?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][coorgId]=' + $window.sessionStorage.coorgId + '&filter[where][groupId]=10').getList().then(function (part) {
            $scope.partners = part;
            angular.forEach($scope.partners, function (member, index) {
                member.index = index + 1;
            });
        });

    });
