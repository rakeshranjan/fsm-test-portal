'use strict';

angular.module('secondarySalesApp')
    .controller('TelaguCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        $scope.isCreateSave = true;
        $scope.heading = 'Multi-Language';

        $scope.multilanguage = {
            languageId: 5
        };

        $scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;
        $scope.submitmobilemultilanguages = Restangular.all('mobilemultilanguages').getList().$object;
        $scope.submitfacilitymultilanguages = Restangular.all('facilitymultilanguages').getList().$object;

        $scope.SaveLanguage = function () {
            $scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
                $scope.submitmobilemultilanguages.customPUT($scope.mobilemultilanguage).then(function (resp) {
                    $scope.submitfacilitymultilanguages.customPUT($scope.facilitymultilanguage).then(function (res) {
                        console.log('Response', response);
                        window.location = '/Languages/telugu';
                    });
                });
            });
        };

        Restangular.one('multilanguages', 5).get().then(function (zone) {
            $scope.original = zone;
            $scope.multilanguage = Restangular.copy($scope.original);
            Restangular.one('mobilemultilanguages', 5).get().then(function (mobile) {
                $scope.mobileoriginal = mobile;
                $scope.mobilemultilanguage = Restangular.copy($scope.mobileoriginal);
                Restangular.one('facilitymultilanguages', 5).get().then(function (facility) {
                    $scope.facilityoriginal = facility;
                    $scope.facilitymultilanguage = Restangular.copy($scope.facilityoriginal);
                    $scope.modalInstanceLoad.close();
                });
            });
        });

        $scope.LanguageName = 'తెలుగు';
        $scope.Language = 'భాషా';
    });