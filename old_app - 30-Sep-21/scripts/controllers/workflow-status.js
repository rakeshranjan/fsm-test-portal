'use strict';

angular.module('secondarySalesApp')
    .controller('WorkFlowStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/workflow-status/create' || $location.path() === '/workflow-status/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflow-status/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflow-status/create' || $location.path() === '/workflow-status/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflow-status/create' || $location.path() === '/workflow-status/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflow-status") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('workflows?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wf) {
            $scope.workflows = wf;
        });

        Restangular.all('workflowstatuses?filter[where][deleteflag]=false&filter[where][parentFlag]=true').getList().then(function (wfs) {
            $scope.workflowstatuses = wfs;

            angular.forEach($scope.workflowstatuses, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.workflowdisplayId = '';

        $scope.$watch('workflowdisplayId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('workflowstatuses?filter[where][deleteflag]=false&filter[where][parentFlag]=true' + '&filter[where][workflowid]=' + newValue).getList().then(function (wfs) {
                    $scope.workflowstatuses = wfs;

                    angular.forEach($scope.workflowstatuses, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        /************************ end ********************************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });


        $scope.DisplayWorkflowStatuses = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png',
            language: 1,
            parentFlag: true
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayWorkflowStatuses.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png',
                language: 1,
                parentFlag: true
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayWorkflowStatuses.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayWorkflowStatuses[index].disableLang = false;
                $scope.DisplayWorkflowStatuses[index].disableAdd = false;
                $scope.DisplayWorkflowStatuses[index].disableRemove = false;
                $scope.DisplayWorkflowStatuses[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayWorkflowStatuses[index].disableLang = true;
                $scope.DisplayWorkflowStatuses[index].disableAdd = false;
                $scope.DisplayWorkflowStatuses[index].disableRemove = false;
                $scope.DisplayWorkflowStatuses[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.validatestring = "";

        $scope.SaveLang = function () {

            document.getElementById('order').style.borderColor = "";

            if ($scope.myobj.orderNo == '' || $scope.myobj.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  $scope.DisplayDefineLevels[$scope.lastIndex].disableLang = false;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].disableAdd = true;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].disableRemove = true;

                $scope.DisplayWorkflowStatuses[$scope.lastIndex].orderNo = $scope.myobj.orderNo;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].due = $scope.myobj.due;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].default = $scope.myobj.default;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].actionble = $scope.myobj.actionble;
                $scope.DisplayWorkflowStatuses[$scope.lastIndex].languages = angular.copy($scope.languages);
                //                console.log($scope.DisplayWorkflowStatuses);
                //
                //                angular.forEach($scope.languages, function (data, index) {
                //                    data.inx = index + 1;
                //                    $scope.DisplayWorkflowStatuses[$scope.lastIndex]["lang" + data.inx] = data.lang;
                //                    console.log($scope.DisplayWorkflowStatuses);;
                //                });

                $scope.langModel = false;
            }

        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflowstatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveWflowStatus();
        };

        var saveCount = 0;

        $scope.saveWflowStatus = function () {

            if (saveCount < $scope.DisplayWorkflowStatuses.length) {

                if ($scope.DisplayWorkflowStatuses[saveCount].name == '' || $scope.DisplayWorkflowStatuses[saveCount].name == null) {
                    saveCount++;
                    $scope.saveWflowStatus();
                } else {

                    $scope.DisplayWorkflowStatuses[saveCount].workflowid = $scope.workflow;

                    Restangular.all('workflowstatuses').post($scope.DisplayWorkflowStatuses[saveCount]).then(function (resp) {
                        //                        saveCount++;
                        //                        $scope.saveWflowStatus();
                        $scope.saveLangCount = 0;
                        $scope.languageWorkflows = [];
                        for (var i = 0; i < $scope.DisplayWorkflowStatuses[saveCount].languages.length; i++) {
                            $scope.languageWorkflows.push({
                                name: $scope.DisplayWorkflowStatuses[saveCount].languages[i].lang,
                                deleteflag: false,
                                lastmodifiedby: $window.sessionStorage.userId,
                                lastmodifiedrole: $window.sessionStorage.roleId,
                                lastmodifiedtime: new Date(),
                                createdby: $window.sessionStorage.userId,
                                createdtime: new Date(),
                                createdrole: $window.sessionStorage.roleId,
                                language: $scope.DisplayWorkflowStatuses[saveCount].languages[i].id,
                                parentFlag: false,
                                parentId: resp.id,
                                orderNo: resp.orderNo,
                                due: resp.due,
                                default: resp.default,
                                actionble: resp.actionble,
                                workflowid: resp.workflowid
                            });
                        }
                        saveCount++;
                        $scope.saveWorkflowLanguage();
                    });
                }

            } else {
                window.location = '/workflow-status-list';
            }
        };

        $scope.saveWorkflowLanguage = function () {

            if ($scope.saveLangCount < $scope.languageWorkflows.length) {
                Restangular.all('workflowstatuses').post($scope.languageWorkflows[$scope.saveLangCount]).then(function (resp) {
                    $scope.saveLangCount++;
                    $scope.saveWorkflowLanguage();
                });
            } else {
                $scope.saveWflowStatus();
            }

        }

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.workflowstatus.name == '' || $scope.workflowstatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.workflowstatus.lastmodifiedby = $window.sessionStorage.userId;
                $scope.workflowstatus.lastmodifiedtime = new Date();
                $scope.workflowstatus.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflowstatuses', $routeParams.id).customPUT($scope.workflowstatus).then(function (resp) {
                    $scope.updateWorkflowLanguage();
                });
            }
        };
        $scope.updateLangCount = 0;

        $scope.updateWorkflowLanguage = function () {

            if ($scope.updateLangCount < $scope.LangWorkflows.length) {
                Restangular.all('workflowstatuses', $scope.LangWorkflows[$scope.updateLangCount].id).customPUT($scope.LangWorkflows[$scope.updateLangCount]).then(function (resp) {
                    $scope.updateLangCount++;
                    $scope.updateWorkflowLanguage();
                });
            } else {
                window.location = '/workflow-status-list';
            }

        }

        $scope.getworkflow = function (workflowid) {
            return Restangular.one('workflows', workflowid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            $scope.myobj.orderNo = $scope.workflowstatus.orderNo;
            $scope.myobj.due = $scope.workflowstatus.due;
            $scope.myobj.default = $scope.workflowstatus.default;
            $scope.myobj.actionble = $scope.workflowstatus.actionble;

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var i = 0; i < $scope.languages.length; i++) {
                if ($scope.LangWorkflows.length > 0) {
                    for (var j = 0; j < $scope.LangWorkflows.length; j++) {
                        if ($scope.LangWorkflows[j].language === $scope.languages[i].id) {
                            $scope.languages[i].lang = $scope.LangWorkflows[j].name;
                        }
                    }
                } else {
                    $scope.languages[mCount].lang = $scope.workflowstatus.lang1;
                }
            };
        };

        $scope.UpdateLang = function () {
           angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                //                $scope.workflow["lang" + data.inx] = data.lang;
                angular.forEach($scope.LangWorkflows, function (lngwrkflw, innerIndex) {
                    if(data.id === lngwrkflw.language){
                        lngwrkflw.name = data.lang;
                    }
                });
                // console.log($scope.workflow);
            });

            $scope.workflowstatus.orderNo = $scope.myobj.orderNo;
            $scope.workflowstatus.due = $scope.myobj.due;
            $scope.workflowstatus.default = $scope.myobj.default;
            $scope.workflowstatus.actionble = $scope.myobj.actionble;
            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            $scope.message = 'Status has been Updated!';
            Restangular.one('workflowstatuses', $routeParams.id).get().then(function (workflowstatus) {
                Restangular.all('workflowstatuses?filter[where][parentId]=' + $routeParams.id).getList().then(function (langwrkflws) {
                    $scope.LangWorkflows = langwrkflws;
                    $scope.original = workflowstatus;
                    $scope.workflowstatus = Restangular.copy($scope.original);
                });
            });
        } else {
            $scope.message = 'Status has been Created!';
        }

    });