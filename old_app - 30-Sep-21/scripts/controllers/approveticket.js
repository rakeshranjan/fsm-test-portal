'use strict';
angular.module('secondarySalesApp').controller('ApproveticketCtrl', function ($scope, Restangular, $routeParams, $filter, $timeout, $window, $route, $modal, $fileUploader) {

  console.log("Approve Ticket");

  $scope.memberlanguage = Restangular.one('memberlanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;


  // Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
  //   $scope.customers = cust;
  // });

  $scope.$watch('member.customerId', function (newValue, oldValue) {
    if (newValue == '' || newValue == null || newValue == oldValue) {
      return;
    } else {
      Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
        $scope.categories = catgresp;

      });
    }
  });

  $scope.$watch('member.categoryId', function (newValue, oldValue) {
    if (newValue == '' || newValue == null || newValue == oldValue) {
      return;
    } else {
      Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
        $scope.subcategories = subcatgresp;
      });
    }
  });

  // -------------Approve Function --------------------------------------------

  $scope.Approve = function (clicked) {
    // Restangular.one('members', $routeParams.id).get().then(function (member) {
    Restangular.one('rcdetails', $routeParams.id).get().then(function (member) {
      member.approvalFlag = true;
      $scope.approveDisable = true;
      Restangular.one('rcdetails', $routeParams.id).customPUT(member).then(function (resp) {
        window.location = '/approvetickets-list';
      });


    });

  };


 // if ($routeParams.id) {
    $scope.disableLocation = true;
    // Restangular.one('members', $routeParams.id).get().then(function (member) {
    Restangular.one('rcdetails', $routeParams.id).get().then(function (member) {
      console.log(member, "MEMBER-321")

      Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().then(function (organisationlevels) {
        $scope.organisationlevels = organisationlevels;
        //console.log($scope.organisationlevels,"UPDATE-LN-324")
        $scope.orgstructure = member.orgstructure.split(";");

        for (var i = 0; i < $scope.organisationlevels.length; i++) {
          for (var j = 0; j < $scope.orgstructure.length; j++) {
            $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
            if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
              $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1];
              //console.log($scope.organisationlevels[i].locationid, $scope.organisationlevels[i].name,"333")
            }
          }
        }
        //console.log("$scope.organisationlevels", $scope.organisationlevels);

        Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
          $scope.organisationlocations = organisationlocations;

          angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
            organisationlevel.index = index;
            organisationlevel.organisationlocations = organisationlocations;
          });



          $scope.original = member;
          $scope.member = Restangular.copy($scope.original);

          Restangular.one('users/findOne?filter[where][id]=' + $scope.member.assignedto).get().then(function (userresp) {
            $scope.user = userresp.username;

            $scope.HealthAnswers1 = Restangular.all('surveyanswers?filter[where][memberid]=' + $scope.member.id + '&filter[where][deleteflag]=false').getList().then(function (HRes) {
              $scope.HealthAnswers = HRes;
              console.log($scope.HealthAnswers, "ANS-35");
              angular.forEach($scope.HealthAnswers, function (member, index) {
                  member.index = index + 1;

                  if (member.uploadedfiles == null || member.uploadedfiles == undefined || member.uploadedfiles == '') {
                    member.uploadedfilesList = [];
                } else {
                    member.uploadedfilesList = member.uploadedfiles.split(',');
                }
                  // Restangular.all('surveyanswers?filter[where][deleteflag]=false&filter[where][memberid]=' + mbr.id).getList().then(function (mbranswer) {
                 // console.log(mbr.id, "MEMBER-ID-133")
                  // Restangular.all('surveyanswers?filter[where][memberid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.HealthAnswers[index].id).getList().then(function (mbranswer) {
                  //   $scope.memberanswer = mbranswer;
                  //   console.log("Survey ANswers: ", $scope.memberanswer);
                    
          
                    
                  // });
          
          
                });
          });

          });

          

        });
      });
    });
 // }

  

  

$scope.getQuestion = function (questionId) {
    return Restangular.one('surveyquestions', questionId).get().$object;
};

$scope.getUser = function (createdby) {
    return Restangular.one('users', createdby).get().$object;
};

});
