'use strict';

angular.module('secondarySalesApp')
  .controller('RoutelinksCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/routelinks/create' || $location.path() === '/routelinks/' + $routeParams.id;
      return visible;
    };  
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/routelinks/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/routelinks/create'|| $location.path() === '/routelinks/' + $routeParams.id;
        return visible;
      };

    /*********/

    //$scope.partners = Restangular.all('employees?filter={"where":{"groupId":{"inq":[1,4,5,6,7]}}}').getList().$object;
    
    $scope.partners = Restangular.all('employees').getList().$object;

    $scope.routes = Restangular.all('distribution-routes').getList().$object;

    $scope.getPartner = function(partnerId){
      return Restangular.one('employees', partnerId).get().$object;
    };

    $scope.getdistributionRoute = function(distributionRouteId){
      return Restangular.one('distribution-routes', distributionRouteId).get().$object;
    };
    /*********/

    //$scope.routelinks = Restangular.all('routelinks').getList().$object;
    
    if($routeParams.id){
      Restangular.one('routelinks', $routeParams.id).get().then(function(routelink){
				$scope.original = routelink;
				$scope.routelink = Restangular.copy($scope.original);
      });
    }
      
       $scope.salesagentId='';
       $scope.$watch('salesagentId', function(newValue, oldValue){
          $scope.routelinks = Restangular.all('routelinks?filter[where][partnerId]='+newValue).getList().$object;
        });
  });
