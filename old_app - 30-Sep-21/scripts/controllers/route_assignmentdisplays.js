'use strict';

angular.module('secondarySalesApp')
  .controller('RouteassignmentDisplayCtrl', function ($scope, Restangular,$filter) {
       $scope.tod = function() {
            $scope.date =  $filter('date')(new Date(), 'y-MM-dd');
           $scope.date1 =  (new Date());
            $scope.previousdate = new Date($scope.date1.getTime() - (1 * 24 * 60 * 60 * 1000));
           $scope.date2 =  $filter('date')($scope.previousdate, 'y-MM-dd');
           console.log('date format',$scope.date2);
           
          };
     $scope.routeassignment = {
            partnerId: ''
          };
      $scope.tod();
      
      $scope.$watch('routeassignment.partnerId', function(newValue, oldValue){
          console.log('newValue: ' + newValue);
          console.log('oldValue: ' + oldValue);
            /////////////////////////////////////////////////////////
            
            $scope.item = [{
            id:'',
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          }]
            /////////////////////////////////////////////////////////
          $scope.routeassignments = Restangular.all('routeassignments?filter[where][partnerId]='+newValue+'&filter[where][date][gte]='+$scope.date2+'&filter[where][flag]=null').getList().$object;
        });
     
   // $scope.partners = Restangular.all('employees?filter={"where":{"groupId":{"inq":[1,4,5,6,7]}}}').getList().$object;
    $scope.partners = Restangular.all('employees').getList().$object;
    
    $scope.getpartner = function(partnerId){
      return Restangular.one('employees', partnerId).get().$object;
    };
     
    $scope.getroute = function(route){
      return Restangular.one('distribution-routes', route).get().$object;
    };
    $scope.gettourtype = function(tourtype){
      return Restangular.one('tourtypes', tourtype).get().$object;
    };
    //$scope.routeassignments = Restangular.all('routeassignments').getList().$object;
  });
