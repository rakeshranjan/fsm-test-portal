'use strict';

angular.module('secondarySalesApp')
    .controller('OrganisationLevelCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/organisationlevel/create' || $location.path() === '/organisationlevel/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/organisationlevel/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/organisationlevel/create' || $location.path() === '/organisationlevel/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/organisationlevel/create' || $location.path() === '/organisationlevel/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/organisationlevel" || $window.sessionStorage.prviousLocation != "partials/organisationlevel") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /****************************************************************************/

        $scope.todo = {
            member: '',
            followUp: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId
        };
        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (language) {
            $scope.languages = language;
        });

        /************************************* INDEX ***************************************************/

        Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC ').getList().then(function (tdo) {
            $scope.organisationlevels = tdo;
            angular.forEach($scope.organisationlevels, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });


        $scope.validatestring = "";
        $scope.langModel = false;

        $scope.LangClick = function () {

            angular.forEach($scope.languages, function (data) {
                if (!data.location) {
                    data.location = $scope.todo.name;
                }
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {
            $scope.langModel = false;
            $scope.submitDisable = false;
        };

        $scope.Save = function (clicked) {

            if ($scope.todo.name == '' || $scope.todo.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            } else if ($scope.todo.slno == '' || $scope.todo.slno == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.todo.language = 1;
                $scope.todo.languageparent = true;
                Restangular.all('organisationlevels').post($scope.todo).then(function (resp) {
                    $scope.LangLocations = [];
                    for (var i = 0; i < $scope.languages.length; i++) {
                        $scope.langObject = {
                            deleteflag: false,
                            language: $scope.languages[i].id,
                            languageparent: false,
                            languageparentid: resp.id,
                            name: $scope.languages[i].location,
                            parent: resp.parent,
                            slno: resp.slno
                        };
                        $scope.LangLocations.push($scope.langObject);
                    }
                    $scope.SaveLangLocations();
                });
            }
        };
        $scope.LangCount = 0;
        $scope.SaveLangLocations = function () {
            Restangular.all('organisationlevels').post($scope.LangLocations[$scope.LangCount]).then(function (resp) {
                $scope.LangCount++;
                if ($scope.LangLocations.length > $scope.LangCount) {
                    $scope.SaveLangLocations();
                } else {
                    window.location = '/organisationlevel';
                }
            });
        }

        $scope.Update = function (clicked) {

            if ($scope.todo.name == '' || $scope.todo.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            } else if ($scope.todo.slno == '' || $scope.todo.slno == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('organisationlevels', $routeParams.id).customPUT($scope.todo).then(function (resp) {
                    $scope.UpLangLocations = [];
                    if ($scope.langorganisationlevels.length > 0) {
                        angular.forEach($scope.languages, function (lang) {
                            angular.forEach($scope.langorganisationlevels, function (data) {
                                if (lang.languageid === data.language) {
                                    data.name = lang.location;
                                    data.parent = resp.parent;
                                    $scope.UpLangLocations.push(data);
                                }
                            });
                        });

                        $scope.UpdateLangLocations();
                    } else {
                        for (var i = 0; i < $scope.languages.length; i++) {
                            $scope.langObject = {
                                deleteflag: false,
                                language: $scope.languages[i].id,
                                languageparent: false,
                                languageparentid: resp.id,
                                name: $scope.languages[i].location,
                                parent: resp.parent,
                                slno: resp.slno
                            };
                            $scope.UpLangLocations.push($scope.langObject);
                        }
                        $scope.UpdateSaveLangLocations();
                    }
                });
            }
        };

        $scope.UpdateLangCount = 0;
        $scope.UpdateLangLocations = function () {
            Restangular.one('organisationlevels').customPUT($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
                $scope.UpdateLangCount++;
                if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
                    $scope.UpdateLangLocations();
                } else {
                    window.location = '/organisationlevel';
                }
            });
        }
        $scope.UpdateSaveLangLocations = function () {
            Restangular.all('organisationlevels').post($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
                $scope.UpdateLangCount++;
                if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
                    $scope.UpdateSaveLangLocations();
                } else {
                    window.location = '/organisationlevel';
                }
            });
        }


        if ($routeParams.id) {

            $scope.message = 'Level has been updated!';

            Restangular.one('organisationlevels', $routeParams.id).get().then(function (td) {
                $scope.original = td;
                $scope.todo = Restangular.copy($scope.original);


                Restangular.all('organisationlevels?filter[where][languageparentid]=' + $routeParams.id).getList().then(function (resp) {
                    $scope.langorganisationlevels = resp;
                    angular.forEach(resp, function (data) {
                        angular.forEach($scope.languages, function (lang) {
                            if (lang.languageid === data.language) {
                                lang.location = data.name;
                            }
                        });
                    });
                });




            });
        } else {
            $scope.message = 'Level has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('organisationlevels/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getParent = function (levelid) {
            if (levelid != undefined && levelid != '' && levelid != null) {
                return Restangular.one('organisationlevels', levelid).get().$object;
            } else {
                return {
                    name: "ROOT"
                }
            }
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };


    });