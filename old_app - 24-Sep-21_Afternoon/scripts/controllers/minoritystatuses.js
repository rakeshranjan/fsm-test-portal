'use strict';

angular.module('secondarySalesApp')
	.controller('MinorityStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/

		$scope.showForm = function () {
			var visible = $location.path() === '/minoritystatus/create' || $location.path() === '/minoritystatus/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/minoritystatus/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/minoritystatus/create' || $location.path() === '/minoritystatus/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/minoritystatus/create' || $location.path() === '/minoritystatus/' + $routeParams.id;
			return visible;
		};


		/*********/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


			if ($window.sessionStorage.prviousLocation != "partials/minoritystatuses") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}
		/******************************/

		if ($routeParams.id) {
			$scope.message = 'Minority Status has been Updated!';
			Restangular.one('minoritystatuses', $routeParams.id).get().then(function (minoritystatus) {
				$scope.original = minoritystatus;
				$scope.minoritystatus = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Minority Status has been Created!';
		}
		$scope.Search = $scope.name;
		//$scope.Search = '';
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		$scope.searchduetodostatus = '';
		/********************************** INDEX *******************************************/
		$scope.zn = Restangular.all('minoritystatuses?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.minoritystatuses = zn;
			angular.forEach($scope.minoritystatuses, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.minoritystatus = {
			"name": '',
			"deleteflag": false
		};
		/****************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.minoritystatus.name == '' || $scope.minoritystatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Minority Status';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitdataModal = !$scope.submitdataModal;
				$scope.submitDisable = true;
				Restangular.all('minoritystatuses').post($scope.minoritystatus).then(function () {
					window.location = '/minoritystatus';
				});
			}
		};
		/************************************* UPDATE *******************************************/
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.minoritystatus.name == '' || $scope.minoritystatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Minority Status';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitdataModal = !$scope.submitdataModal;
				$scope.submitDisable = true;
				Restangular.one('minoritystatuses', $routeParams.id).customPUT($scope.minoritystatus).then(function () {
					window.location = '/minoritystatus';
				});
			}
		};
		/*************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('minoritystatuses/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
	});
