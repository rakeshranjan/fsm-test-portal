'use strict';

angular.module('secondarySalesApp')
    .controller('MemberCustomCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/membercustom/create' || $location.path() === '/membercustom/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/membercustom/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/membercustom/create' || $location.path() === '/membercustom/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/membercustom/create' || $location.path() === '/membercustom/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/membercustom" || $window.sessionStorage.prviousLocation != "partials/membercustom") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        $scope.membercustom = {
            membercustomid: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            enableforroles: []
        };

        /************************************* INDEX ***************************************************/

        Restangular.all('membercustomfields?filter[where][deleteflag]=false&filter[where][parentflag]=true').getList().then(function (wflow) {
            $scope.membercustomheaders = wflow;
            angular.forEach($scope.membercustomheaders, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });

        $scope.languageDisable = true;



        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        $scope.validatestring = "";
        $scope.langModel = false;
        $scope.submitDisable = true;

        $scope.LangClick = function () {

            angular.forEach($scope.languages, function (data) {
                if (!data.fieldname) {
                data.fieldname = $scope.membercustom.name;
                data.values = $scope.membercustom.values;
                }
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
            });

            console.log($scope.onetoone);
            $scope.langModel = false;
            $scope.submitDisable = false;
        };

        $scope.Save = function (clicked) {
            if($scope.membercustomheaders.length>=5){
                $scope.validatestring = $scope.validatestring + 'User can configure only 5 custom fields';
            }
            else if ($scope.membercustom.name == '' || $scope.membercustom.language == name) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            } else if ($scope.membercustom.type == '' || $scope.membercustom.type == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Type';

            } else if ($scope.membercustom.mandatory == '' || $scope.membercustom.mandatory == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Mandatory';

            } 
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.membercustom.language = 1;
                $scope.membercustom.parentflag = true;
                Restangular.all('membercustomfields').post($scope.membercustom).then(function (resp) {
                    $scope.LangLocations = [];
                    for (var i = 0; i < $scope.languages.length; i++) {
                        $scope.langObject = {
                            deleteflag: false,
                            language: $scope.languages[i].id,
                            parentflag: false,
                            parentid: resp.id,
                            name: $scope.languages[i].fieldname,
                            values: $scope.languages[i].values,
                            baselangvalues: resp.values,
                            default: resp.default,
                            mandatory: resp.mandatory,
                            maxchars: resp.maxchars,
                            maxsize: resp.maxsize,
                            multiselect: resp.multiselect,
                            type: resp.type
                        };
                        $scope.LangLocations.push($scope.langObject);
                    }
                    $scope.SaveLangLocations();
                });
            }
        };


        $scope.LangCount = 0;
        $scope.SaveLangLocations = function () {
            Restangular.all('membercustomfields').post($scope.LangLocations[$scope.LangCount]).then(function (resp) {
                $scope.LangCount++;
                if ($scope.LangLocations.length > $scope.LangCount) {
                    $scope.SaveLangLocations();
                } else {
                    window.location = '/membercustom';
                }
            });
        }

        $scope.Update = function (clicked) {
            if ($scope.membercustom.name == '' || $scope.membercustom.language == name) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';

            } else if ($scope.membercustom.type == '' || $scope.membercustom.type == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Type';

            } else if ($scope.membercustom.mandatory == '' || $scope.membercustom.mandatory == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Mandatory';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('membercustomfields', $routeParams.id).customPUT($scope.membercustom).then(function (resp) {
                    $scope.UpLangLocations = [];
                    if ($scope.langorganisationfields.length > 0) {
                        angular.forEach($scope.languages, function (lang) {
                            angular.forEach($scope.langorganisationfields, function (data) {
                                if (lang.languageid === data.language) {
                                    data.name = lang.fieldname;
                                    data.values = lang.values;
                                    $scope.UpLangLocations.push(data);
                                }
                            });
                        });

                        $scope.UpdateLangLocations();
                    } else {
                        for (var i = 0; i < $scope.languages.length; i++) {
                            $scope.langObject = {
                                deleteflag: false,
                                language: $scope.languages[i].id,
                                parentflag: false,
                                parentid: resp.id,
                                name: $scope.languages[i].fieldname,
                                baselangvalues: resp.values,
                                values: $scope.languages[i].values,
                                default: resp.default,
                                mandatory: resp.mandatory,
                                maxchars: resp.maxchars,
                                maxsize: resp.maxsize,
                                multiselect: resp.multiselect,
                                type: resp.type
                            };
                            $scope.UpLangLocations.push($scope.langObject);
                        }
                        $scope.UpdateSaveLangLocations();
                    }
                });
            }
        };

        $scope.UpdateLangCount = 0;
        $scope.UpdateLangLocations = function () {
            Restangular.one('membercustomfields').customPUT($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
                $scope.UpdateLangCount++;
                if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
                    $scope.UpdateLangLocations();
                } else {
                    window.location = '/membercustom';
                }
            });
        }
        $scope.UpdateSaveLangLocations = function () {
            Restangular.all('membercustomfields').post($scope.UpLangLocations[$scope.UpdateLangCount]).then(function (resp) {
                $scope.UpdateLangCount++;
                if ($scope.UpLangLocations.length > $scope.UpdateLangCount) {
                    $scope.UpdateSaveLangLocations();
                } else {
                    window.location = '/membercustom';
                }
            });
        }

        if ($routeParams.id) {

            $scope.message = 'membercustom has been updated!';

            Restangular.one('membercustomfields', $routeParams.id).get().then(function (wflow) {
                $scope.original = wflow;
                $scope.membercustom = Restangular.copy($scope.original);

                Restangular.all('membercustomfields?filter[where][languageparentid]=' + $routeParams.id).getList().then(function (resp) {
                    $scope.langorganisationfields = resp;
                    angular.forEach(resp, function (data) {
                        angular.forEach($scope.languages, function (lang) {
                            if (lang.languageid === data.language) {
                                lang.fieldname = data.name;
                                lang.values = data.values;
                            }
                        });
                    });
                });
            });
        } else {
            $scope.message = 'membercustom has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('membercustomfields/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getLanguage = function (language) {
            return Restangular.one('languagedefinitions/findOne?filter[where][languageid]=' + language).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


    });