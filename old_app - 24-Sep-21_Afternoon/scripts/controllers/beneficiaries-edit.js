'use strict';
angular.module('secondarySalesApp')
    .controller('BnEditCtrl', function ($scope, Restangular, $filter, $timeout, $window, $route, $modal, $routeParams,$fileUploader) {

        $scope.hideSave = false;
        $scope.hideUpdate = true;
        $scope.languageId = $window.sessionStorage.language;
        $scope.memberlanguage = Restangular.one('memberlanguages/findOne?filter[where][language]=' + $window.sessionStorage.language).get().$object;

        Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=null').getList().then(function (cityresp) {
            $scope.cities = cityresp;
          });

        Restangular.all('genders?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language).getList().then(function (gender) {
            $scope.genders = gender;
        });

        Restangular.all('customers?filter[where][deleteflag]=false').getList().then(function (cust) {
            $scope.customers = cust;
          });
      
        //   Restangular.all('categories?filter[where][deleteflag]=false').getList().then(function (catg) {
        //     $scope.categories = catg;
        //   });

        Restangular.all('occupations?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language).getList().then(function (occupation) {
            $scope.occupations = occupation;
        });

        Restangular.all('educations?filter[where][deleteflag]=false&filter[where][language]='+$window.sessionStorage.language).getList().then(function (education) {
            $scope.educations = education;
        });

        $scope.mapdataModal = false;

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });


        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (l1) {
            $scope.levelones = l1;
        });

        $scope.$watch('member.customerId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
              return;
            } else {
              Restangular.all('categories?filter[where][deleteflag]=false&filter[where][customerId]=' + newValue).getList().then(function (catgresp) {
                $scope.categories = catgresp;
                //console.log($scope.categories, "CATEGORY")
                //member.categoryId = $scope.categories.id;
              });
            }
          });

          $scope.$watch('member.categoryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
              return;
            } else {
              Restangular.all('subcategories?filter[where][deleteflag]=false&filter[where][categoryId]=' + newValue).getList().then(function (subcatgresp) {
                $scope.subcategories = subcatgresp;
              });
            }
          });

        //   Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().then(function (ogrlevls) {
        //     console.log(ogrlevls, "ogrlevls-14")
        //     $scope.orgLevel = (ogrlevls[0].languageparent == true ? ogrlevls[0].id : ogrlevls[0].languageparentid);
        //     console.log($scope.orgLevel, "$scope.orgLevel-16")
        //     //   $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + $scope.orgLevel).getList().then(function (ogrlocs) {
        //     $scope.organisationlocations = Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][level]=' + $scope.orgLevel).getList().then(function (ogrlocs) {
        //       $scope.organisationlevels = ogrlevls;
        //       console.log($scope.organisationlevels, "$scope.organisationlevels-15")
        //       angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
        //         organisationlevel.index = index;
        //         // if (organisationlevel.id === ogrlevls[0].languageparent ? ogrlevls[0].id : ogrlevls[0].languageparentid) {
        //         //   organisationlevel.organisationlocations = ogrlocs;
        //         // }
        //         organisationlevel.organisationlocations = ogrlocs;
        //       });
        //     });
        //   });

          $scope.$watch('member.cityId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null || newValue == oldValue) {
              return;
            } else {
              //console.log(newValue, "newValue-136")
              Restangular.one('organisationlocations?filter[where][id]=' + newValue).get().then(function (orglevelresp) {
                $scope.orglevel = orglevelresp[0];
                //console.log($scope.orglevel, "$scope.orglevel")
                $scope.level = orglevelresp[0].level;
                //console.log($scope.level, "LEVEL")
                Restangular.all('organisationlocations?filter[where][deleteflag]=false&filter[where][parent]=' + newValue + '&filter[where][parentlevel]=' + $scope.level).getList().then(function (pincoderesp) {
                  $scope.pincodes = pincoderesp;
                });
              });
            }
          });

        $scope.$watch('member.leveloneid', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null) {
                return;
            } else {
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwos = l2;
                });
            }
        });

        $scope.$watch('member.leveltwoid', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null) {
                return;
            } else {
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (l3) {
                    $scope.levelthrees = l3;
                });
            }
        });

        $scope.$watch('member.levelthreeid', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null) {
                return;
            } else {
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][levelthreeid]=' + newValue).getList().then(function (l4) {
                    $scope.levelfours = l4;
                });
            }
        });

        $scope.$watch('member.levelfoureid', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null) {
                return;
            } else {
                Restangular.all('levelfives?filter[where][deleteflag]=false' + '&filter[where][levelfourid]=' + newValue).getList().then(function (l5) {
                    $scope.levelfives = l5;
                });
            }
        });

     /*   $scope.$watch('member.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcount++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.member.age = diff;
            }
        });

        $scope.$watch('member.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('age').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.member.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.member.dob = null;
                    $scope.member.age = null;
                }
            }
        }); */


        /********************** Save Function *******************************/

        $scope.validatestring = "";

        $scope.Update = function (clicked) {

            document.getElementById('customer').style.borderColor = "";
            document.getElementById('name').style.borderColor = "";
            document.getElementById('address').style.borderColor = "";
            document.getElementById('phone').style.borderColor = "";
            document.getElementById('latitude').style.borderColor = "";
            document.getElementById('longitude').style.borderColor = "";

            if ($scope.member.customerId == '' || $scope.member.customerId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Customer';
                document.getElementById('customer').style.borderColor = "#FF0000";
        
              } else if ($scope.member.categoryId == '' || $scope.member.categoryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Category';
                document.getElementById('category').style.borderColor = "#FF0000";
        
              } else if ($scope.member.subcategoryId == '' || $scope.member.subcategoryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Subcategory';
                document.getElementById('subcategory').style.borderColor = "#FF0000";
        
              } else if ($scope.organisationlevels[0].locationid == null || $scope.organisationlevels[0].locationid == '') {
                $scope.validatestring = 'Please Select ' + $scope.organisationlevels[0].name;
                
              } else if ($scope.organisationlevels[1].locationid == null || $scope.organisationlevels[1].locationid == '') {
                $scope.validatestring = 'Please Select ' + $scope.organisationlevels[1].name;
                
              } else if ($scope.member.fullname == '' || $scope.member.fullname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Ticket Name';
                document.getElementById('name').style.borderColor = "#FF0000";
        
              } else if ($scope.member.address == '' || $scope.member.address == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Address';
                document.getElementById('address').style.borderColor = "#FF0000";
        
              } else if ($scope.member.mobile == '' || $scope.member.mobile == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Phone';
                document.getElementById('phone').style.borderColor = "#FF0000";
        
              } else if ($scope.member.mobile.length != 10) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Valid Phone No.';
                document.getElementById('phone').style.borderColor = "#FF0000";
        
              } else if ($scope.member.latitude == '' || $scope.member.latitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Latitude';
                document.getElementById('latitude').style.borderColor = "#FF0000";
      
              } else if ($scope.member.longitude == '' || $scope.member.longitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Longitude';
                document.getElementById('longitude').style.borderColor = "#FF0000";
            }
        
             /* else if ($scope.organisationlevels.locationid == null) {
                for (var i = 0; i < $scope.organisationlevels.length; i++) {
                if ($scope.organisationlevels[i].locationid == null || $scope.organisationlevels[i].locationid == '') {
                   $scope.validatestring = 'Please Select ' + $scope.organisationlevels[i].name;
                   break;
                 }
               } 
              } */
            
            
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.message = 'Ticket has been updated!';

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                $scope.orgstructure = "";
                for (var i = 0; i < $scope.organisationlevels.length; i++) {
                    $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
                    if ($scope.orgstructure === "") {
                        $scope.orgstructure = $scope.LevelId + "-" + $scope.organisationlevels[i].locationid;
                    } else {
                        $scope.orgstructure = $scope.orgstructure + ";" + $scope.LevelId + "-" + $scope.organisationlevels[i].locationid;
                    }
                }
                $scope.member.orgstructure = $scope.orgstructure;

                // for (var i = 0; i < $scope.membercustomfields.length; i++) {
                //     if (i == 0) {
                //         $scope.member.fieldOneType = $scope.membercustomfields[0].type;
                //         $scope.member.fieldOneValue = $scope.membercustomfields[0].answeredvalue;
                //     }
                //     if (i == 1) {
                //         $scope.member.fieldTwoType = $scope.membercustomfields[1].type;
                //         $scope.member.fieldTwoValue = $scope.membercustomfields[1].answeredvalue;
                //     }
                //     if (i == 2) {
                //         $scope.member.fieldThreeType = $scope.membercustomfields[2].type;
                //         $scope.member.fieldThreeValue = $scope.membercustomfields[2].answeredvalue;
                //     }
                //     if (i == 3) {
                //         $scope.member.fieldFourType = $scope.membercustomfields[3].type;
                //         $scope.member.fieldFourValue = $scope.membercustomfields[3].answeredvalue;
                //     }
                //     if (i == 4) {
                //         $scope.member.fieldFiveType = $scope.membercustomfields[4].type;
                //         $scope.member.fieldFiveValue = $scope.membercustomfields[4].answeredvalue;
                //     }

                // }

                $scope.member.lastmodifiedby = $window.sessionStorage.userId;
                $scope.member.lastmodifiedtime = new Date();
                $scope.member.lastmodifiedrole = $window.sessionStorage.roleId;

                Restangular.one('members', $routeParams.id).customPUT($scope.member).then(function (resp) {
                    window.location = '/tickets';
                });
            }
        };

        $scope.disableLocation = false;

        if ($routeParams.id) {
            $scope.disableLocation = true;
            Restangular.one('members', $routeParams.id).get().then(function (member) {
                //console.log(member,"MEMBER-321")
                
                    Restangular.all('organisationlevels?filter[where][deleteflag]=false&filter[where][languageparent]=true&filter[order]=slno ASC').getList().then(function (organisationlevels) {
                    $scope.organisationlevels = organisationlevels;
                    //console.log($scope.organisationlevels,"UPDATE-LN-324")
                    $scope.orgstructure = member.orgstructure.split(";");

                    for (var i = 0; i < $scope.organisationlevels.length; i++) {
                        for (var j = 0; j < $scope.orgstructure.length; j++) {
                            $scope.LevelId = $scope.organisationlevels[i].languageparent == true ? $scope.organisationlevels[i].id : $scope.organisationlevels[i].languageparentid;
                            if ($scope.LevelId + "" === $scope.orgstructure[j].split("-")[0]) {
                                $scope.organisationlevels[i].locationid = $scope.orgstructure[j].split("-")[1];
                                //console.log($scope.organisationlevels[i].locationid, $scope.organisationlevels[i].name,"333")
                            }
                        }
                    }
                    //console.log("$scope.organisationlevels", $scope.organisationlevels);

                    Restangular.all('organisationlocations').getList().then(function (organisationlocations) {
                        $scope.organisationlocations = organisationlocations;

                        angular.forEach($scope.organisationlevels, function (organisationlevel, index) {
                            organisationlevel.index = index;
                            organisationlevel.organisationlocations = organisationlocations;
                        });


                        Restangular.all('membercustomfields?filter[where][deleteflag]=false&filter[where][languageparent]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (custfields) {
                            $scope.membercustomfields = custfields;
                            angular.forEach($scope.membercustomfields, function (cstfield, index) {

                                if (index === 0) {
                                    cstfield.answeredvalue = member.fieldOneValue;
                                }
                                if (index == 1) {
                                    cstfield.answeredvalue = member.fieldTwoValue;
                                }
                                if (index == 2) {
                                    cstfield.answeredvalue = member.fieldThreeValue;
                                }
                                if (index == 3) {
                                    cstfield.answeredvalue = member.fieldFourValue;
                                }
                                if (index == 4) {
                                    cstfield.answeredvalue = member.fieldFiveValue;
                                }

                                if (cstfield.type == 'dropdown') {
                                    cstfield.dropdownvalues = [];
                                    for (var i = 0; i < cstfield.values.split(",").length; i++) {
                                        cstfield.dropdownvalues.push({
                                            name: cstfield.values.split(",")[i],
                                            selectedvalue: cstfield.baselangvalues.split(",")[i]
                                        });
                                    }
                                }

                            });


                            $scope.original = member;
                            $scope.member = Restangular.copy($scope.original);
                        });


                    });
                });
            });
        }

        /*********************** end **************************************/

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'dd-MMM-yy');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
        };

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.opencustdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#custumdate' + index).focus();
            });
            $scope.picker['custdateopened' + index] = true;
        };

        /******************************google map****************************/
        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.member.latitude = latLng.lat();
                $scope.member.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };
    
    //////////////////////////////////////////IMAGE UPLOAD///////////////////////////////////
    $scope.Created = false;
        $scope.Updated = true;
        $scope.imageedit = true;
    
    
    $scope.imageUploadChange = function(event,index){
        $scope.membercustomfields[+event.id].answeredvalue = event.value.split(/[\/\\]/).pop();
    }



        var uploader = $scope.uploader = $fileUploader.create({
            scope: $scope, // to automatically update the html. Default: $rootScope
            url: 'http://parle-ss-api-secure.herokuapp.com/api/v1/containers/cbizorg/upload',
            formData: [
                {
                    key: 'value'
                }
      ],
            filters: [
        function (item) { // first user filter
                    console.info('filter1');
                    return true;
        }
      ]
        });



        $scope.imagecreate = false;


        $scope.Submitted = false;
        // ADDING FILTERS

        uploader.filters.push(function (item) { // second user filter
            console.info('filter2');
            return true;
        });
        $scope.Submitted = false;
        $scope.isCreateView = false;
        // REGISTER HANDLERS

        uploader.bind('afteraddingfile', function (event, item) {
            console.info('After adding a file', item);
        });

        uploader.bind('whenaddingfilefailed', function (event, item) {
            console.info('When adding a file failed', item);
        });

        uploader.bind('afteraddingall', function (event, items) {
            console.info('After adding all files', items);
        });

        uploader.bind('beforeupload', function (event, item) {
            console.info('Before upload', item);
        });

        uploader.bind('progress', function (event, item, progress) {
            console.info('Progress: ' + progress, item);
        });

        uploader.bind('success', function (event, xhr, item, response) {
            // $scope.saveoffer();
            console.info('Success', xhr, item, response);
            $scope.$broadcast('uploadCompleted', item);
        });

        uploader.bind('cancel', function (event, xhr, item) {
            console.info('Cancel', xhr, item);
        });

        uploader.bind('error', function (event, xhr, item, response) {
            console.info('Error', xhr, item, response);
        });

        uploader.bind('complete', function (event, xhr, item, response) {
            console.info('Complete', xhr, item, response);
        });

        uploader.bind('progressall', function (event, progress) {
            console.info('Total progress: ' + progress);
        });

        uploader.bind('completeall', function (event, items) {

            console.info('Complete all', items);
            window.location = "/tickets";
        });

        $scope.load = function () {
            $http.get('http://parle-ss-api-secure.herokuapp.com/api/v1/containers/cbizorg/files').success(function (data) {
                console.log(data);

            });
        };

        $scope.delete = function (index, id) {
            $http.delete('http://parle-ss-api-secure.herokuapp.com/api/v1/containers/cbizorg/files/' + encodeURIComponent(id)).success(function (data, status, headers) {
                $scope.files.splice(index, 1);
            });
        };



        $scope.$on('uploadCompleted', function (event) {
            console.log('uploadCompleted event received');

            $scope.load();
        });

    //////////////////////////////////////////IMAGE UPLOAD///////////////////////////////////
    });