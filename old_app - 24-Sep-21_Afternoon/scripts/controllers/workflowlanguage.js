'use strict';

angular.module('secondarySalesApp')
    .controller('WorkflowLanguageCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $routeParams, $location, $timeout) {

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/workflowlanguage/create' || $location.path() === '/workflowlanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflowlanguage/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflowlanguage/create' || $location.path() === '/workflowlanguage/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflowlanguage/create' || $location.path() === '/workflowlanguage/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/workflowlanguage" || $window.sessionStorage.prviousLocation != "partials/workflowlanguage") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        $scope.workflow = {
            workflowid: '',
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            enableforroles: []
        };

        /************************************* INDEX ***************************************************/

        Restangular.all('workflowlanguages?filter[where][deleteflag]=false').getList().then(function (wflow) {
            $scope.workflowheaders = wflow;
            angular.forEach($scope.workflowheaders, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (role) {
            $scope.roles = role;
        });
    
    $scope.languageDisable = true;

       Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
           Restangular.one("roles/"+$window.sessionStorage.roleId).get().then(function(role){
             $scope.languages = langs;
               if(role.isLanguageUser){
                    $scope.workflow.language = $window.sessionStorage.language;
                   $scope.languageDisable = true;
               }else{
                   $scope.languageDisable = false;
               }
           });
           
        });


        $scope.validatestring = "";

        $scope.Save = function (clicked) {
            if ($scope.workflow.language == '' || $scope.workflow.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.workflow.member == '' || $scope.workflow.member == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Member';

            } else if ($scope.workflow.workflowLabel == '' || $scope.workflow.workflowLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Workflow';

            } else if ($scope.workflow.category == '' || $scope.workflow.category == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Category';

            } else if ($scope.workflow.followup == '' || $scope.workflow.followup == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Follow Up';

            } else if ($scope.workflow.comments == '' || $scope.workflow.comments == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Comments';

            } else if ($scope.workflow.enableforroles == '' || $scope.workflow.enableforroles == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Enable for roles';

            } else if ($scope.workflow.assignedTo == '' || $scope.workflow.assignedTo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Assigned To';

            } else if ($scope.workflow.status == '' || $scope.workflow.status == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status';

            } else if ($scope.workflow.priority == '' || $scope.workflow.priority == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Priority';

            } else if ($scope.workflow.saveLabel == '' || $scope.workflow.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Save';

            } else if ($scope.workflow.updateLabel == '' || $scope.workflow.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Update';

            } else if ($scope.workflow.cancelLabel == '' || $scope.workflow.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.all('workflowlanguages').post($scope.workflow).then(function (resp) {
                    window.location = '/workflowlanguage';
                });
            }
        };

        $scope.Update = function (clicked) {
            if ($scope.workflow.language == '' || $scope.workflow.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            } else if ($scope.workflow.member == '' || $scope.workflow.member == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Member';

            } else if ($scope.workflow.workflowLabel == '' || $scope.workflow.workflowLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Workflow';

            } else if ($scope.workflow.category == '' || $scope.workflow.category == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Category';

            } else if ($scope.workflow.followup == '' || $scope.workflow.followup == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Follow Up';

            } else if ($scope.workflow.comments == '' || $scope.workflow.comments == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Comments';

            } else if ($scope.workflow.enableforroles == '' || $scope.workflow.enableforroles == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Enable for roles';

            } else if ($scope.workflow.assignedTo == '' || $scope.workflow.assignedTo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Assigned To';

            } else if ($scope.workflow.status == '' || $scope.workflow.status == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status';

            } else if ($scope.workflow.priority == '' || $scope.workflow.priority == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Priority';

            } else if ($scope.workflow.saveLabel == '' || $scope.workflow.saveLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Save';

            } else if ($scope.workflow.updateLabel == '' || $scope.workflow.updateLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Update';

            } else if ($scope.workflow.cancelLabel == '' || $scope.workflow.cancelLabel == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Cancel';

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('workflowlanguages', $routeParams.id).customPUT($scope.workflow).then(function (resp) {
                    window.location = '/workflowlanguage';
                });
            }
        };

        if ($routeParams.id) {

            $scope.message = 'Workflow has been updated!';

            Restangular.one('workflowlanguages', $routeParams.id).get().then(function (wflow) {
                $scope.original = wflow;
                $scope.workflow = Restangular.copy($scope.original);
                //                $scope.roles = Restangular.all('roles?filter[where][deleteflag]=false').getList().$object;
                //
                //                $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
            });
        } else {
            $scope.message = 'Workflow has been created!';
        }


        /************************************* Delete ***************************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('workflowlanguages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.getLanguage = function (language) {
            return Restangular.one('languagedefinitions/findOne?filter[where][deleteflag]=false&filter[where][id]='+language).get().$object;
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


    });
