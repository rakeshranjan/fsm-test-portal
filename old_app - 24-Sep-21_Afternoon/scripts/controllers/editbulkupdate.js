'use strict';

angular.module('secondarySalesApp')
	.controller('BulkUPEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/

		$scope.addedtodos = [];
		$scope.newtodo = {};


		$scope.showForm = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/bulkupdates/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};
		/*************************************************************************************/



		$scope.bulkupdate = {
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date()
		};

		$scope.reportincident = {};
		$scope.event = {};
		$scope.submitbulkupdates = Restangular.all('bulkupdates').getList().$object;
		$scope.bulkupdatefollowups = Restangular.all('bulkupdatefollowups').getList().$object;
		$scope.submittodos = Restangular.all('todos').getList().$object;


		/*$scope.getFollwUp = function (id) {
			return Restangular.one('bulkupdatefollowups', id).get().$object;
		};*/

		$scope.getMemberName = function (partnerId) {
			return Restangular.all('beneficiaries?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
		};

		$scope.getFollwUp = function (followUpId) {
			return Restangular.all('bulkupdatefollowups?filter={"where":{"id":{"inq":[' + followUpId + ']}}}').getList().$object;
		};



		$scope.bulkupdates = Restangular.all('bulkupdates').getList().$object;

		/************************************* Current& Future Date***********************************************************/
		var today = new Date();

		//$scope.newtodo.datetime = $filter('date')(new Date(), 'dd-MMMM-yyyy');
		$scope.bulkupdate.datetime = $filter('date')(new Date(), 'MMM d, y h:mm:ss a');
		$scope.bulkupdate.followupstatus = 'open';

		var newdate = new Date();

		$scope.today = function () {
			/*
				newdate.setDate(newdate.getDate() + 7, 'dd-MMMM-yyyy');

				var dd = newdate.getDate();
				var mm = newdate.getMonth() + 1;
				var y = newdate.getFullYear();
				$scope.bulkupdate.datetime = new Date();

				var sevendays = new Date();
				sevendays.setDate(sevendays.getDate() + 7);
				$scope.newtodo = {
					datetime: sevendays
				};*/
		};
		$scope.today();
		// $scope.presenttoday = new Date();
		// $scope.dtmax = new Date();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();
		$scope.picker = {};
		$scope.followdt = {};



		$scope.opendob = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepickerdob' + index).focus();
			});
			$scope.picker.dobopened = true;
		};

		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepickerfollowup' + index).focus();
			});
			//$scope.follow.followupopened = true;
			$scope.followdt.followupopened = true;

		};



		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.monthOptions = {
			formatYear: 'yyyy',
			startingDay: 1,
			minMode: 'month'
		};
		$scope.mode = 'month';



		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		$scope.monthformat = $scope.monthformats[0];

		//Datepicker settings end

		/************************************************ RouteParam END *******************************/

		if ($routeParams.id) {
			Restangular.one('bulkupdates', $routeParams.id).get().then(function (bulkupdate) {
				$scope.original = bulkupdate;
				$scope.modalInstanceLoad.close();
				$scope.bulkupdate = Restangular.copy($scope.original);

				if (bulkupdate.followup != null) {
					$scope.bulkupdate.follow = bulkupdate.followup.split(",");
				} else {
					$scope.bulkupdate.follow = [];
				}

				if (bulkupdate.attendeeslist != null) {
					$scope.selectedattendeeslist = bulkupdate.attendeeslist.split(",");
				} else {
					$scope.selectedattendeeslist = [];
				}
/*
				$scope.getMemberName1 = function (partnerId) {
					return Restangular.all('beneficiaries?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
				};*/

				/*
				if ($window.sessionStorage.roleId == 5) {
					Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
						$scope.comemberid = comember.id;
						$scope.part = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=null').getList().then(function (partner) {
							$scope.partners = partner;
							angular.forEach($scope.partners, function (member, index) {
								member.index = index;
								member.enabled = false;
							});
						});

					});
				} else {
					Restangular.all('beneficiaries?filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null').getList().then(function (partner) {
						$scope.partners = partner;
						angular.forEach($scope.partners, function (member, index) {
							member.index = index;
							member.enabled = false;
						});
					});
				}*/

				if ($window.sessionStorage.roleId == 5) {
					Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
						$scope.comemberid = comember.id;
						$scope.part = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=null').getList().then(function (part) {
					
							$scope.bulkpartners = part;
							console.log();
							angular.forEach($scope.bulkpartners, function (member, index) {
								member.index = index + 1;
								if ($scope.selectedattendeeslist.indexOf(member.id.toString()) == -1) {
									member.enabled = false;
								} else {
									member.enabled = true;
								}
							});
						});

					});
				} else {
					Restangular.all('beneficiaries?filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null').getList().then(function (part) {
				
						$scope.bulkpartners = part;
						angular.forEach($scope.bulkpartners, function (member, index) {
							member.index = index + 1;
							if ($scope.selectedattendeeslist.indexOf(member.id.toString()) == -1) {
								member.enabled = false;
							} else {
								member.enabled = true;
							}
						});
					});
				}



			});


		}
		/****************************************************************************************/
		$scope.showModal1 = false;
		$scope.toggleModal1 = function () {

			$scope.SaveScheme = function () {
				$scope.newArray = [];
				$scope.bulkpartners = [];
				$scope.partnerid = [];

				$scope.bulkupdate.attendees = [];
				for (var i = 0; i < $scope.partners.length; i++) {
					if ($scope.partners[i].enabled == true) {
						$scope.newArray.push($scope.partners[i]);
						$scope.partnerid.push($scope.partners[i].id);
						//$scope.attendeestodo.push($scope.partners[i].id);
					} else if ($scope.partners[i].enabled == false) {
						$scope.partners[i].enabled = false;

					}
				}
				$scope.attendeestodo = $scope.partnerid;
				console.log('$scope.attendeestodo', $scope.attendeeattendeestodo);


				$scope.bulkpartners = $scope.newArray;
				$scope.showModal1 = !$scope.showModal1;
			};
			$scope.showModal1 = !$scope.showModal1;
		};



		$scope.CancelReport = function () {
			$scope.showModal1 = !$scope.showModal1;
		};





		/************************************************* INDEX *******************************************/
		$scope.searchbulkupdate = $scope.name;


		$scope.zn = Restangular.all('bulkupdates?filter[where][deleteflag]=null' + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (zn) {
			$scope.Printbulkupdates = zn;
			angular.forEach($scope.Printbulkupdates, function (member, index) {
				member.index = index + 1;
				member.memberlist = $scope.getMemberName(member.memberlist);
				//member.memberlist1 = $scope.getMemberName1(member.memberlist);
				member.followup = $scope.getFollwUp(member.followup);
			});
		});

		/********************************************** SAVE *******************************************/

		$scope.bulkupdatedataModal = false;
		$scope.message = 'Bulk Update has been updated!';
		$scope.modalTitle = 'Thank You';

		$scope.bulkupdate.follow = [];

		$scope.Savebulkupdate = function () {
			$scope.bulkupdatedataModal = !$scope.bulkupdatedataModal;
			$scope.bulkupdate.followup = null;
			$scope.bulkupdate.memberlist = null;

			var checkedValue = null;
			var inputElements = document.getElementsByClassName('eventCheckbox');
			//var inputElements = document.getElementsById('mytable');
			for (var i = 0; inputElements[i]; ++i) {
				if (inputElements[i].checked) {
					if (checkedValue == null) {
						checkedValue = inputElements[i].value;
					} else {
						checkedValue = checkedValue + ',' + inputElements[i].value;
					}
				}
			}

			$scope.bulkupdate.memberlist = checkedValue;
			console.log('$scope.bulkupdate.memberlist', $scope.bulkupdate.memberlist);

			for (var i = 0; i < $scope.bulkupdate.follow.length; i++) {
				if (i == 0) {
					$scope.bulkupdate.followup = $scope.bulkupdate.follow[i];
				} else {
					$scope.bulkupdate.followup = $scope.bulkupdate.followup + ',' + $scope.bulkupdate.follow[i];
				}
			}

			$scope.submitbulkupdates.post($scope.bulkupdate).then(function () {
				console.log('bulkupdate Saved', $scope.bulkupdate);
				$scope.SaveToDo();
				//window.location = '/bulkupdates';
				console.log('Save ToDo');
			});
		};

		/************************************************* MOdal ******************************/

		$scope.newtodo.status = 1;
		$scope.newtodo.todotype = 33;
		$scope.newtodo.state = $window.sessionStorage.zoneId;
		$scope.newtodo.district = $window.sessionStorage.salesAreaId;
		$scope.newtodo.facility = $window.sessionStorage.coorgId
		$scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId
		$scope.newtodo.lastmodifiedtime = new Date();



		/*
		$scope.showfollowupModal = false;
		$scope.$watch('bulkupdate.follow', function (newValue, oldValue) {
			$scope.oldFollowUp = oldValue;
			$scope.newFollow = newValue;
			if (newValue === oldValue) {
				return;
			} else {
				$scope.SavetodoFollow = function () {
					$scope.attendeespurpose = newValue;
					console.log('$scope.attendeespurpose', $scope.attendeespurpose);
					$scope.addedtodos.push($scope.newtodo);
					console.log('$scope.addedtodos', $scope.addedtodos);
					$scope.showfollowupModal = !$scope.showfollowupModal;
				};
				var array3 = newValue.filter(function (obj) {
					return oldValue.indexOf(obj) == -1;
				});
				if (array3.length > 0) {
					console.log('unique', array3);
					for (var i = 0; i < $scope.bulkupdatefollowups.length; i++) {
						if ($scope.bulkupdatefollowups[i].id == array3[0]) {
							$scope.printpurpose = $scope.bulkupdatefollowups[i].name;
						}
					}
				}

				if (oldValue != undefined) {
					if (oldValue.length < newValue.length) {
						$scope.showfollowupModal = !$scope.showfollowupModal;
					}
				} else {
					$scope.showfollowupModal = !$scope.showfollowupModal;
				}
			};
		});


		$scope.CancelFollow = function () {
			$scope.bulkupdate.follow = $scope.oldFollowUp;
			console.log('$scope.bulkupdate.follow', $scope.bulkupdate.follow);
			$scope.showfollowupModal = !$scope.showfollowupModal;
		};
*/



		$scope.todocount = 0;
		$scope.todocount1 = 0;
		$scope.SaveToDo = function () {
			$scope.addedtodocount = 0;
			$scope.addedtodocount1 = 0;
			for (var i = 0; i < $scope.addedtodos.length; i++) {
				$scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount];
				$scope.addedtodos[i].purpose = $scope.attendeespurpose[$scope.todocount1];
				console.log('$scope.addedtodos[i].purpose', $scope.addedtodos[i].purpose);
				$scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
					$scope.addedtodocount++;
					$scope.addedtodocount1++;
					if ($scope.addedtodocount >= $scope.addedtodos.length && $scope.addedtodocount1 >= $scope.addedtodos.length) {
						$scope.todocount++;
						$scope.todocount1++;
						if ($scope.todocount < $scope.attendeestodo.length && $scope.todocount1 < $scope.attendeespurpose.length) {
							$scope.SaveToDo();
						} else if ($scope.SaveToDo.length <= $scope.addedtodos.length) {
							//$route.reload();
							window.location = '/bulkupdates';
						}
					}
				});

			}
		};


		/**************************************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatebulkupdate = function () {
			document.getElementById('name').style.border = "";
			if ($scope.bulkupdate.name == '' || $scope.bulkupdate.name == null) {
				$scope.bulkupdate.name = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your bulkupdate name';
				document.getElementById('name').style.border = "1px solid #ff0000";

			}
			if ($scope.validatestring != '') {
				alert($scope.validatestring);
				$scope.validatestring = '';
			} else {
				$scope.submitbulkupdates.customPUT($scope.bulkupdate).then(function () {
					//  console.log('bulkupdate Saved');
					window.location = '/bulkupdates';
				});
			}


		};
		/******************************************* DELETE *******************************************/
		$scope.Delete = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				$scope.item = [{
					deleteflag: 'yes'
            }]
				Restangular.one('bulkupdates/' + id).customPUT($scope.item[0]).then(function () {
					$route.reload();
				});

			} else {

			}
		}

		/*
				$scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
					// console.log('zipmaster', zipmaster);
					$scope.zipmasters = zipmaster;
					angular.forEach($scope.zipmasters, function (member, index) {

						member.total = null;
						member.loader = null;
					});
				});
		*/


		$scope.RemoveMember = function (index) {
			$scope.bulkpartners.splice(index, 1);
			$scope.$watch('partner.enabled', function (newValue, oldValue) {
				console.log('partner.enabled', newValue);
				if (newValue === oldValue) {
					$scope.partners[index].enabled = false;
					return;
				} else {
					for (var i = 0; i < $scope.bulkpartners.length; i++) {
						$scope.partners[i].enabled = false;
					}
				}
			});
		};
		/**************************************************************** MOdal For FollowUp***************************/
		/* $scope.prtnrs = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null').getList().then(function (partner) {
		     $scope.partners = partner;
		     angular.forEach($scope.partners, function (member, index) {
		         member.index = index;
		         member.enabled = false;
		     });
		 });*/



	});
