'use strict';

angular.module('secondarySalesApp')
    .controller('SchemeMastersCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $modal) {

        $scope.showForm = function () {
            var visible = $location.path() === '/schemes/create' || $location.path() === '/schemes/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/schemes/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/schemes/create' || $location.path() === '/schemes/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/schemes/create' || $location.path() === '/schemes/' + $routeParams.id;
            return visible;
        };
        /***********************************************************************************/
        $scope.ShowAllDetails = false;
        $scope.ShowAllDetailsDiv = false;
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute1 == null || $window.sessionStorage.myRoute1 == undefined) {
            $window.sessionStorage.myRoute1 = null;
            $window.sessionStorage.myRoute1_currentPage = 1;
            $window.sessionStorage.myRoute1_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute1_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute1_currentPage;
            $scope.pillarId = $window.sessionStorage.myRoute1;
        }

        if ($window.sessionStorage.prviousLocation != "partials/schemes") {
            $window.sessionStorage.myRoute1 = '';
            $window.sessionStorage.myRoute1_currentPage = 1;
            $window.sessionStorage.myRoute1_currentPagesize = 25;
        }


        $scope.currentpage = $window.sessionStorage.myRoute1_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute1_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute1_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute1_currentPagesize = mypage;
        };

        /*.................................... INDEX ................................................*/
        $scope.st = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znRes) {
            $scope.states = znRes;
            $scope.stateId = $window.sessionStorage.myRoute1;
        });

        //$scope.schememasters = {};
        //$scope.stateId = '';
        $scope.zonalid = '';
        $scope.$watch('stateId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {

                return;
            } else {
                $scope.topfive = false;
                $scope.zonalid = +newValue;
                $window.sessionStorage.myRoute1 = newValue;
                //$window.sessionStorage.myRoute = newValue;
                $scope.con = Restangular.all('schemes?filter[where][deleteflag]=false&filter[where][state]=' + newValue).getList().then(function (con) {
                    $scope.Displayschememasters = con;
                    console.log('$scope.Displayschememasters', $scope.Displayschememasters);
                    angular.forEach($scope.Displayschememasters, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        $scope.$watch('topfive', function (newValue, oldValue) {
            //console.log('$scope.stateId',$scope.stateId);
            if ($scope.stateId == '' || $scope.stateId == undefined) {
                return;
            } else {
                //console.log(newValue);
                if (newValue == true) {
                    // $scope.stateId = +newValue;
                    //$window.sessionStorage.myRoute = newValue;
                    $scope.con1 = Restangular.all('schemes?filter[where][deleteflag]=false&filter[where][and][0][topscheme][lt]=6&filter[where][and][1][topscheme][gt]=0&filter[where][state]=' + $scope.stateId + '&filter[order]=topscheme%20ASC').getList().then(function (con1) {
                        $scope.Displayschememasters = con1;
                        angular.forEach($scope.Displayschememasters, function (member, index) {
                            member.index = index + 1;
                        });
                    });

                } else {
                    $scope.con2 = Restangular.all('schemes?filter[where][deleteflag]=false&filter[where][state]=' + $scope.stateId).getList().then(function (con2) {
                        $scope.Displayschememasters = con2;
                        angular.forEach($scope.Displayschememasters, function (member, index) {
                            member.index = index + 1;
                        });
                    });

                }
            }
        });




        $scope.schememaster = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            deleteflag: false
        };


        $scope.genders = Restangular.all('genders').getList().$object;
        $scope.educations = Restangular.all('educations').getList().$object;
        $scope.submitschememasters = Restangular.all('schemes').getList().$object;
        $scope.schemecategories = Restangular.all('schemecategories').getList().$object;
        $scope.agegroups = Restangular.all('agegroups').getList().$object;
        $scope.healthstatuses = Restangular.all('healthstatuses').getList().$object;
        $scope.minoritystatuses = Restangular.all('minoritystatuses').getList().$object;
        $scope.socialstatuses = Restangular.all('socialstatuses').getList().$object;
        $scope.occupationstatuses = Restangular.all('occupationstatuses?filter[where][deleteflag]=false').getList().$object;
        $scope.incomestatuses = Restangular.all('incomestatuses').getList().$object;
        $scope.locationtypes = Restangular.all('locationtypes').getList().$object;

        /******************************* INDEX *****************************************/

        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };
        $scope.showDetails = function () {
            $scope.ShowAllDetails = !$scope.ShowAllDetails;
        };

        if ($routeParams.id) {
            $scope.ShowAllDetailsDiv = true;
            $scope.message = 'Scheme has been Updated!';
            Restangular.one('schemes', $routeParams.id).get().then(function (scheme) {
                $scope.original = scheme;
                $scope.schememaster = Restangular.copy($scope.original);
                console.log('$scope.schememaster.categ', $scope.schememaster.categ);
                $scope.schememaster.categ = scheme.category.split(",");
                $scope.schememaster.agegrp = scheme.agegroup.split(",");
                if ($scope.schememaster.topscheme == 0) {
                    $scope.schememaster.topscheme = "";
                }

                Restangular.all('allschemes?filter[where][scheme_id]=' + $scope.schememaster.id).getList().then(function (allscheme) {
                    $scope.alloriginal = allscheme[0];
                    if (allscheme.length > 0) {
                        $scope.schememasterall = Restangular.copy($scope.alloriginal);
                    }
                });
            });
        } else {
            $scope.message = 'Scheme has been Created!';
        }

        /*************************************** DELETE *******************************/

        $scope.DeleteFlag = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('schemes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        $scope.validatestring = '';
        $scope.schememaster.categ = [];
        $scope.schememaster.agegrp = [];
        $scope.Save = function () {
            $scope.schememaster.category = null;
            $scope.schememaster.agegroup = null;
            //console.log('$scope.schememaster.categ.length',$scope.schememaster.categ.length);
            //console.log('$scope.schememaster.categ',$scope.schememaster.categ);
            //console.log('$scope.schememaster.agegrp.length',$scope.schememaster.agegrp.length);

            if ($scope.schememaster.state == '' || $scope.schememaster.state == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a State or Union Territory';
            } else if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.localname == '' || $scope.schememaster.localname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme local name';
            } else if ($scope.schememaster.categ.length == 0) {
                $scope.validatestring = $scope.validatestring + 'Please select category';
            } else if ($scope.schememaster.agegrp.length == 0) {
                $scope.validatestring = $scope.validatestring + 'Please select age group';
            } else if ($scope.schememaster.hnname == '' || $scope.schememaster.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in hindi';
            } else if ($scope.schememaster.knname == '' || $scope.schememaster.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in kannada';
            } else if ($scope.schememaster.taname == '' || $scope.schememaster.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in tamil';
            } else if ($scope.schememaster.tename == '' || $scope.schememaster.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in telugu';
            } else if ($scope.schememaster.mrname == '' || $scope.schememaster.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in marathi';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.schememasterdataModal = !$scope.schememasterdataModal;
                for (var i = 0; i < $scope.schememaster.categ.length; i++) {
                    if (i == 0) {
                        $scope.schememaster.category = $scope.schememaster.categ[i];
                    } else {
                        $scope.schememaster.category = $scope.schememaster.category + ',' + $scope.schememaster.categ[i];
                    }
                }

                for (var i = 0; i < $scope.schememaster.agegrp.length; i++) {
                    if (i == 0) {
                        $scope.schememaster.agegroup = $scope.schememaster.agegrp[i];
                    } else {
                        $scope.schememaster.agegroup = $scope.schememaster.agegroup + ',' + $scope.schememaster.agegrp[i];
                    }
                }

                $scope.submitschememasters.post($scope.schememaster).then(function () {
                    console.log('$scope.schememaster', $scope.schememaster);
                    window.location = '/schemes';
                });
            }
        };


        $scope.Update = function () {
            $scope.schememaster.category = null;
            $scope.schememaster.agegroup = null;

            if ($scope.schememaster.state == '' || $scope.schememaster.state == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a State or Union Territory';
            } else if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.localname == '' || $scope.schememaster.localname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme local name';
            } else if ($scope.schememaster.categ.length == 0) {
                $scope.validatestring = $scope.validatestring + 'Please select category';
            } else if ($scope.schememaster.agegrp.length == 0) {
                $scope.validatestring = $scope.validatestring + 'Please select age group';
            } else if ($scope.schememaster.hnname == '' || $scope.schememaster.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in hindi';
            } else if ($scope.schememaster.knname == '' || $scope.schememaster.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in kannada';
            } else if ($scope.schememaster.taname == '' || $scope.schememaster.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in tamil';
            } else if ($scope.schememaster.tename == '' || $scope.schememaster.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in telugu';
            } else if ($scope.schememaster.mrname == '' || $scope.schememaster.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name in marathi';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.schememasterdataModal = !$scope.schememasterdataModal;
                for (var i = 0; i < $scope.schememaster.categ.length; i++) {
                    if (i == 0) {
                        $scope.schememaster.category = $scope.schememaster.categ[i];
                    } else {
                        $scope.schememaster.category = $scope.schememaster.category + ',' + $scope.schememaster.categ[i];
                    }
                }
                $scope.validatestring = $scope.validatestring + 'Please select category';
                for (var i = 0; i < $scope.schememaster.agegrp.length; i++) {
                    if (i == 0) {
                        $scope.schememaster.agegroup = $scope.schememaster.agegrp[i];
                    } else {
                        $scope.schememaster.agegroup = $scope.schememaster.agegroup + ',' + $scope.schememaster.agegrp[i];
                    }
                }

                $scope.submitschememasters.customPUT($scope.schememaster).then(function () {
                    console.log('$scope.schememaster', $scope.schememaster);
                    if ($scope.alterTopScheme == true) {
                        $scope.schemeToAlter.topscheme = null;
                        $scope.submitschememasters.customPUT($scope.schemeToAlter).then(function () {
                            window.location = '/schemes';
                        });
                    } else {
                        window.location = '/schemes';
                    }
                });
            }
        }
        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.$watch('schememaster.topscheme', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.alterTopScheme = false;
                if ($scope.schememaster.state == undefined || $scope.schememaster.state == '') {
                    alert('Select State');
                    $scope.schememaster.topscheme = '';
                } else {
                    Restangular.all('schemes?filter[where][state]=' + $scope.schememaster.state + '&filter[where][topscheme]=' + newValue).getList().then(function (topscheme) {
                        console.log('topscheme', topscheme);
                        if (topscheme.length > 0 && topscheme[0].id != $scope.schememaster.id) {
                            $scope.openSp();
                            $scope.schememastertopscheme = oldValue;
                            $scope.schemeToAlter = topscheme[0];
                        }
                    });
                }
            }
        });


        ///////////////////////////////////////Modal////////////////////////////////////////
        $scope.openSp = function () {
            $scope.modalSP = $modal.open({
                animation: true,
                templateUrl: 'template/sp.html',
                scope: $scope,
                backdrop: 'static'

            });
        };
        $scope.alterTopScheme = false;
        $scope.SaveSP = function () {
            $scope.alterTopScheme = true;
            $scope.modalSP.close();
        };

        $scope.okSp = function () {
            $scope.schememaster.topscheme = $scope.schememastertopscheme;
            $scope.modalSP.close();
        };


        ///////////////////////////////////////Modal////////////////////////////////////////
    });
