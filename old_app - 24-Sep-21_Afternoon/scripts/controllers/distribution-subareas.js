'use strict';

angular.module('secondarySalesApp')
    .controller('DistributionSubareasCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/distribution-subareas/create' || $location.path() === '/distribution-subareas/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/distribution-subareas/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/distribution-subareas/create' || $location.path() === '/distribution-subareas/' + $routeParams.id;
            return visible;
        };

      $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/distribution-subareas/create' || $location.path() === '/distribution-subareas/' + $routeParams.id;
            return visible;
        };

    
       $scope.salesArea = {
            zoneId: ''    
        }
    
    $scope.distributionArea = {
            zoneId: '',
            salesAreaId:''
        };
       
    $scope.distributionSubarea = {
            zoneId: '',
            salesAreaId:'',
            distributionAreaId:''
        }
    
        /*********/
        $scope.searchName = $scope.name;
    
     $scope.zones = Restangular.all('zones').getList().$object;
     $scope.salesAreas = Restangular.all('sales-areas?').getList().$object;
     $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
    
       
    

        $scope.getZone = function (zoneId) {
            return Restangular.one('zones', zoneId).get().$object;
        };

        $scope.getSalesArea = function (salesAreaId) {
            return Restangular.one('sales-areas', salesAreaId).get().$object;
        };

        $scope.getDistributionArea = function (distributionAreaId) {
            return Restangular.one('distribution-areas', distributionAreaId).get().$object;
        };

        $scope.$watch('distributionSubarea.zoneId', function (newValue, oldValue) {
          
             $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]='+newValue).getList().$object;
              $scope.salesAreas = this.salesAreas;
              console.log('this.salesAreas',this.salesAreas)
        });

      
        $scope.$watch('distributionSubarea.salesAreaId', function (newValue, oldValue) {
            $scope.distributionAreas = Restangular.all('distribution-areas?filter[where][salesAreaId]='+newValue).getList().$object;
        });

        /*********/

    $scope.Sal = Restangular.all('distribution-subareas').getList().then(function (Sal) {
        $scope.distributionSubareas = Sal;
        angular.forEach($scope.distributionSubareas, function (member, index) {
            member.index = index + 1;
        });
    });

    
/*****************************************************************************/
        if ($routeParams.id) {
            Restangular.one('distribution-subareas', $routeParams.id).get().then(function (distributionSubarea) {
                $scope.original = distributionSubarea;
                $scope.distributionSubarea = Restangular.copy($scope.original);
            });
        }

    
    
     
////////////////////////////////////////////////////////CREATE////////////////////////////////////////////

      $scope.validatestring = '';
        $scope.Save = function () {
                document.getElementById('name').style.border = "";
                document.getElementById('zoneId').style.border = "";
                document.getElementById('salesAreaId').style.border = "";
            
         if ($scope.distributionSubarea.name  == '' || $scope.distributionSubarea.name  == null) {
                $scope.distributionSubarea.name  = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your Distribution-Sub-Area';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } else if(  $scope.distributionSubarea.zoneId == '' || $scope.distributionSubarea.zoneId  == null) {
               $scope.distributionSubarea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Zone';
                document.getElementById('zoneId').style.border = "1px solid #ff0000";
        
          } else if(  $scope.distributionSubarea.salesAreaId == '' || $scope.distributionSubarea.salesAreaId  == null) {
               $scope.distributionSubarea.salesAreaId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Sales-Area';
                document.getElementById('salesAreaId').style.border = "1px solid #ff0000";
          }
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.distributionsubareas.post($scope.distributionSubarea).then(function () {
                         console.log('distribution-subareas saved');
                         window.location = '/distribution-subareas';
                    });
                }


        };
/***************************************UPDATE*******************************************************************/
    
    $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
                document.getElementById('zoneId').style.border = "";
                document.getElementById('salesAreaId').style.border = "";
            
         if ($scope.distributionSubarea.name  == '' || $scope.distributionSubarea.name  == null) {
                $scope.distributionSubarea.name  = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your Distribution-Sub-Area';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } else if(  $scope.distributionSubarea.zoneId == '' || $scope.distributionSubarea.zoneId  == null) {
               $scope.distributionSubarea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Zone';
                document.getElementById('zoneId').style.border = "1px solid #ff0000";
        
          } else if(  $scope.distributionSubarea.salesAreaId == '' || $scope.distributionSubarea.salesAreaId  == null) {
               $scope.distributionSubarea.salesAreaId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Sales-Area';
                document.getElementById('salesAreaId').style.border = "1px solid #ff0000";
          }
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.distributionsubareas.customPUT($scope.distributionSubarea).then(function () {
                         console.log('distribution-subareas saved');
                         window.location = '/distribution-subareas';
                    });
                }


        };
    
/*********************************************Calling*******************************************/
    
        $scope.zoneId = '';
        $scope.salesareaId = '';
        $scope.salesid = '';
        $scope.distid = '';
        $scope.distributionareaId = '';
        $scope.zonalid = '';

        $scope.$watch('zoneId', function (newValue, oldValue) {
             if (newValue === oldValue || newValue == '') {
                return;
            } else {
            $scope.salesareas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]='+'null' ).getList().$object;
     $scope.sal = Restangular.all('distribution-subareas?filter[where][zoneId]=' + newValue).getList().then( function (sal) {
            $scope.distributionSubareas = sal;
            angular.forEach($scope.distributionSubareas, function (member, index) {
            member.index = index + 1;
        });}); 
            
            $scope.zonalid = +newValue;
            }
        });
        
          $scope.$watch('salesareaId', function (newValue, oldValue) {
               if (newValue === oldValue || newValue == '') {
                return;
            } else {
    $scope.distributionareas = Restangular.all('distribution-areas?filter[where][salesAreaId]=' + newValue ).getList().$object;
             
        $scope.sal = Restangular.all('distribution-subareas?filter[where][zoneId]=' +   $scope.zonalid +
            '&filter[where][salesAreaId]=' + newValue).getList().then( function (sal) {
            $scope.distributionSubareas = sal;
            
                 angular.forEach($scope.distributionSubareas, function (member, index) {
            member.index = index + 1;
        });});   
            $scope.salesid = +newValue;
            }
        });
    
    
     $scope.$watch('distributionareaId', function (newValue, oldValue) {
          if (newValue === oldValue || newValue == '') {
                return;
            } else {
               $scope.sal = Restangular.all('distribution-subareas?filter[where][zoneId]=' +   $scope.zonalid +
            '&filter[where][salesAreaId]=' +$scope.salesid +'&filter[where][distributionAreaId]=' + newValue).getList().then( function (sal) {
            $scope.distributionSubareas = sal;
            
                 angular.forEach($scope.distributionSubareas, function (member, index) {
            member.index = index + 1;
        });});       
                
            }
        });
    
/**************************************************************************************DELETE***************************/
     
      $scope.updateFlag = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('distribution-subareas/' + id).remove($scope.distributionSubarea).then(function () {
                $route.reload();
            });

        } else {

        }

    }

});