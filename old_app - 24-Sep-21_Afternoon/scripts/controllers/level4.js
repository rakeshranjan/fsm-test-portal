'use strict';

angular.module('secondarySalesApp')
    .controller('Level4Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/level4/create' || $location.path() === '/level4/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/level4/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/level4/create' || $location.path() === '/level4/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/level4/create' || $location.path() === '/level4/' + $routeParams.id;
            return visible;
        };

        Restangular.all('leveldefinitions?filter[where][deleteflag]=false').getList().then(function (leveldefinition) {
            $scope.leveldefinitions = leveldefinition;

            if ($routeParams.id) {
                $scope.message = leveldefinition[3].name + ' has been Updated!';
            } else {
                $scope.message = leveldefinition[3].name + ' has been Created!';
            }

            angular.forEach($scope.leveldefinitions, function (data, index) {
                data.index = index;
                data.visible = true;
            });
        });

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/level4") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        Restangular.all('languagedefinitions?filter[where][deleteflag]=false').getList().then(function (langs) {
            $scope.languages = langs;
        });

        Restangular.all('levelones?filter[where][deleteflag]=false').getList().then(function (l1) {
            $scope.levelones = l1;
        });

        $scope.$watch('level1', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwos = l2;
                });
            }
        });

        $scope.$watch('level2', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined || newValue == null || $routeParams.id) {
                return;
            } else {
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (l3) {
                    $scope.levelthrees = l3;
                });
            }
        });

        Restangular.all('levelfours?filter[where][deleteflag]=false').getList().then(function (lvl4) {
            $scope.levelfours = lvl4;

            angular.forEach($scope.levelfours, function (member, index) {
                member.index = index + 1;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $scope.level1Id = '';
        $scope.level2Id = '';
        $scope.level3Id = '';

        $scope.$watch('level1Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('leveltwos?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (l2) {
                    $scope.leveltwolist = l2;
                });
                
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][leveloneid]=' + newValue).getList().then(function (lvl4) {
                    $scope.levelfours = lvl4;

                    angular.forEach($scope.levelfours, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });


        $scope.$watch('level2Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                
                Restangular.all('levelthrees?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (l3) {
                    $scope.levelthreelist = l3;
                });
                
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][leveltwoid]=' + newValue).getList().then(function (lvl4) {
                    $scope.levelfours = lvl4;

                    angular.forEach($scope.levelfours, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        $scope.$watch('level3Id', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined || newValue == null || newValue == '') {
                return;
            } else {
                Restangular.all('levelfours?filter[where][deleteflag]=false' + '&filter[where][levelthreeid]=' + newValue).getList().then(function (lvl4) {
                    $scope.levelfours = lvl4;

                    angular.forEach($scope.levelfours, function (member, index) {
                        member.index = index + 1;

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });


        /************************ end ********************************************************/

        $scope.DisplayLevelfours = [{
            name: '',
            disableLang: false,
            disableAdd: false,
            disableRemove: false,
            deleteflag: false,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedrole: $window.sessionStorage.roleId,
            lastmodifiedtime: new Date(),
            createdby: $window.sessionStorage.userId,
            createdtime: new Date(),
            createdrole: $window.sessionStorage.roleId,
            langdark: 'images/Lgrey.png'
        }];

        $scope.Add = function (index) {

            $scope.myobj = {};

            var idVal = index + 1;

            $scope.DisplayLevelfours.push({
                name: '',
                disableLang: false,
                disableAdd: false,
                disableRemove: false,
                deleteflag: false,
                lastmodifiedby: $window.sessionStorage.userId,
                lastmodifiedrole: $window.sessionStorage.roleId,
                lastmodifiedtime: new Date(),
                createdby: $window.sessionStorage.userId,
                createdtime: new Date(),
                createdrole: $window.sessionStorage.roleId,
                langdark: 'images/Lgrey.png'
            });
        };

        $scope.myobj = {};

        $scope.Remove = function (index) {
            var indexVal = index - 1;
            $scope.DisplayLevelfours.splice(indexVal, 1);
        };

        $scope.levelChange = function (index, name) {
            if (name == '' || name == null) {
                $scope.DisplayLevelfours[index].disableLang = false;
                $scope.DisplayLevelfours[index].disableAdd = false;
                $scope.DisplayLevelfours[index].disableRemove = false;
                $scope.DisplayLevelfours[index].langdark = 'images/Lgrey.png';
            } else {
                $scope.DisplayLevelfours[index].disableLang = true;
                $scope.DisplayLevelfours[index].disableAdd = false;
                $scope.DisplayLevelfours[index].disableRemove = false;
                $scope.DisplayLevelfours[index].langdark = 'images/Ldark.png';
            }
        };

        $scope.langModel = false;

        $scope.Lang = function (index, name) {

            $scope.lastIndex = index;

            angular.forEach($scope.languages, function (data) {
                data.lang = name;
                // console.log(data);
            });

            $scope.langModel = true;
        };

        $scope.SaveLang = function () {

            $scope.DisplayLevelfours[$scope.lastIndex].disableAdd = true;
            $scope.DisplayLevelfours[$scope.lastIndex].disableRemove = true;

            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.DisplayLevelfours[$scope.lastIndex]["lang" + data.inx] = data.lang;
                console.log($scope.DisplayLevelfours);;
            });

            $scope.langModel = false;
        };


        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('levelfours/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function (clicked) {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.saveLevel4();
        };

        var saveCount = 0;

        $scope.saveLevel4 = function () {

            if (saveCount < $scope.DisplayLevelfours.length) {

                if ($scope.DisplayLevelfours[saveCount].name == '' || $scope.DisplayLevelfours[saveCount].name == null) {
                    saveCount++;
                    $scope.saveLevel4();
                } else {

                    $scope.DisplayLevelfours[saveCount].leveloneid = $scope.level1;
                    $scope.DisplayLevelfours[saveCount].leveltwoid = $scope.level2;
                    $scope.DisplayLevelfours[saveCount].levelthreeid = $scope.level3;

                    Restangular.all('levelfours').post($scope.DisplayLevelfours[saveCount]).then(function (resp) {
                        saveCount++;
                        $scope.saveLevel4();
                    });
                }

            } else {
                window.location = '/level4-list';
            }
        };

        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {

            if ($scope.levelfour.name == '' || $scope.levelfour.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter' + $scope.leveldefinitions[3].name + 'Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.levelfour.lastmodifiedby = $window.sessionStorage.userId;
                $scope.levelfour.lastmodifiedtime = new Date();
                $scope.levelfour.lastmodifiedrole = $window.sessionStorage.roleId;

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                Restangular.one('levelfours', $routeParams.id).customPUT($scope.levelfour).then(function (resp) {
                    window.location = '/level4-list';
                });
            }
        };

        $scope.getLevel1 = function (leveloneid) {
            return Restangular.one('levelones', leveloneid).get().$object;
        };

        $scope.getLevel2 = function (leveltwoid) {
            return Restangular.one('leveltwos', leveltwoid).get().$object;
        };

        $scope.getLevel3 = function (levelthreeid) {
            return Restangular.one('levelthrees', levelthreeid).get().$object;
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.EditLang = function () {
            $scope.langModel = true;
            $scope.myobj = {};

            var uCount = 1;
            var mCount = 0;
            var myVal = 'lang';

            for (var key in $scope.levelfour) {
                if ($scope.levelfour.hasOwnProperty(key)) {
                    var x = myVal + '' + uCount;
                    if (key == x) {
                        // console.log(x);

                        if ($scope.levelfour[key] != null) {
                            $scope.languages[mCount].lang = $scope.levelfour[key];
                            //  console.log($scope.leveldefinition[key]);
                            uCount++;
                            mCount++;
                        } else if ($scope.levelfour[key] == null) {
                            // console.log($scope.leveldefinition[key]);
                            $scope.languages[mCount].lang = $scope.levelfour.lang1;
                            uCount++;
                            mCount++;
                        }
                    }
                }
            }
        };

        $scope.UpdateLang = function () {
            angular.forEach($scope.languages, function (data, index) {
                data.inx = index + 1;
                $scope.levelfour["lang" + data.inx] = data.lang;
                // console.log($scope.leveldefinition);
            });

            $scope.langModel = false;
        };

        if ($routeParams.id) {
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('levelfours', $routeParams.id).get().then(function (levelfour) {
                $scope.original = levelfour;
                $scope.levelfour = Restangular.copy($scope.original);
            });

            Restangular.all('leveltwos?filter[where][deleteflag]=false').getList().then(function (l2) {
                $scope.leveltwos = l2;
            });

            Restangular.all('levelthrees?filter[where][deleteflag]=false').getList().then(function (l3) {
                $scope.levelthrees = l3;
            });
        }

    });
