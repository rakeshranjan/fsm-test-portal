'use strict';

angular.module('secondarySalesApp')
	.controller('EmployeesCreateCtrl', function ($scope, Restangular, $window, $filter, $timeout, $location) {

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
		$scope.modalTitle = 'Thank You';
		$scope.message = 'Facility has been created'
		$scope.heading = 'Facility Create';
		$scope.Created = false;
		$scope.Updated = true;
		$scope.fwcodedisable = false;
		$scope.fwnamedisable = false;
		$scope.fwstatedisable = false;
		$scope.fwdistrictdisable = false;

		$scope.employee = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false,
			directintervention: false
		};

		$scope.typologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;
		$scope.areaofoperations = Restangular.all('areaofoperations?filter[where][deleteflag]=false').getList().$object;
		$scope.employeestatuses = Restangular.all('employeestatuses?filter[where][deleteflag]=false').getList().$object;
		$scope.facilitytypes = Restangular.all('facilitytypes?filter[where][deleteflag]=false').getList().$object;
		$scope.tiratings = Restangular.all('tiratings').getList().$object;

		$scope.Rating = function (no) {

			$scope.tifacilities = [];
			for (var i = 0; i < no; i++) {
				$scope.tifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});

			}
			console.log('$scope.tifacilities', $scope.tifacilities);
		}

		$scope.ProjectImp = function (noo) {
			$scope.nontifacilities = [];
			for (var i = 0; i < noo; i++) {
				$scope.nontifacilities.push({
					rating: '',
					facilityid: '',
					state: '',
					district: '',
					deleteflag: false
				});
			}
		}



		//Datepicker settings start
		$scope.today = function () {
			$scope.dt = $filter('date')(new Date(), 'y-MM-dd');
		};
		$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		$scope.dtmax = new Date();
		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();
		$scope.picker = {};
		$scope.RegistrationRenewal = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.RegistrationRenewal.opened = true;
		};

		$scope.RegistrationRenewal2 = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.RegistrationRenewal2.opened = true;
		};

		$scope.LastAGBM = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.LastAGBM.opened = true;
		};

		$scope.LastElection = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();
			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.LastElection.opened = true;
		};


		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end

		$scope.$watch('employee.stateId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.districts = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				$scope.zonalid = newValue;
			}
		});

		$scope.states = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
		/***************************** SAVE **********************************************************/
		console.log('$scope.employee.typolog', $scope.employee.typolog);
		$scope.showValidation = false;
		$scope.showUniqueValidation = false;
		$scope.validatestring = '';
		$scope.Save = function () {
			var re = /\S+@\S+\.\S+/;
			document.getElementById('salesCode').style.border = "";
			document.getElementById('firstName').style.border = "";

			 if ($scope.employee.salesCode == '' || $scope.employee.salesCode == null) {
				$scope.employee.salesCode = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your sales code';
				//document.getElementById('salesCode').style.border = "1px solid #ff0000";
				 $scope.TabName =  'BASIC';
				 
			} else if ($scope.employee.firstName == '' || $scope.employee.firstName == null) {
				$scope.validatestring = $scope.validatestring + 'Plese enter your first name';
				$scope.TabName =  'BASIC';
				
			} else if ($scope.employee.facilitytype == '' || $scope.employee.facilitytype == null) {
				$scope.validatestring = $scope.validatestring + 'Please select Facility Type';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.areaofoperation == '' || $scope.employee.areaofoperation == null) {
				$scope.validatestring = $scope.validatestring + 'Please select Area of Operation';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.nooftowncover == '' || $scope.employee.nooftowncover == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter No of Towns Covered';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofmandal == '' || $scope.employee.noofmandal == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter No of Mandals/Talukas/Blocks Covered';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofincome == '' || $scope.employee.noofincome == null) {
				$scope.validatestring = $scope.validatestring + 'No of Income Generating Activities Undertaken by the CO';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.nooftis == '' || $scope.employee.nooftis == null || $scope.employee.nooftis == NaN || $scope.employee.nooftis == 0) {
				$scope.validatestring = $scope.validatestring + 'No of TIs Implemented by the facility';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.noofnonti == '' || $scope.employee.noofnonti == null || $scope.employee.noofnonti == 0) {
				$scope.validatestring = $scope.validatestring + 'No of Non TI and Non Avahan Projects Implemented by the CO';
				$scope.TabName =  'NON STATUTORY';
				
			} else if ($scope.employee.registrationno == '' || $scope.employee.registrationno == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.registrationdate == '' || $scope.employee.registrationdate == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Renewal Date';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.pancard == '' || $scope.employee.pancard == null) {
				$scope.validatestring = $scope.validatestring + 'Enter PAN Card Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.astatus == '' || $scope.employee.astatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select 12 A Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.aregistrationno == '' || $scope.employee.aregistrationno == null && $scope.employee.astatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.gstatus == '' || $scope.employee.gstatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select 80 G Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.gregistrationno == '' || $scope.employee.gregistrationno == null && $scope.employee.gstatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.frcastatus == '' || $scope.employee.frcastatus == null) {
				$scope.validatestring = $scope.validatestring + 'Select FCRA Status';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.frcaregistrationno == '' || $scope.employee.frcaregistrationno == null && $scope.employee.frcastatus == 4) {
				$scope.validatestring = $scope.validatestring + 'Enter Registration Number';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.dateoflastabgm == '' || $scope.employee.dateoflastabgm == null) {
				$scope.validatestring = $scope.validatestring + 'Select Date of Last AGBM';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.registrationrenewaldate == '' || $scope.employee.registrationrenewaldate == null) {
				$scope.validatestring = $scope.validatestring + 'Select Registration Renewal Date';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.externalaudit == '' || $scope.employee.externalaudit == null) {
				$scope.validatestring = $scope.validatestring + 'Select External Audit Completed';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.auditreport == '' || $scope.employee.auditreport == null) {
				$scope.validatestring = $scope.validatestring + 'Select Audit Report Submitted';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.actionplan == '' || $scope.employee.actionplan == null) {
				$scope.validatestring = $scope.validatestring + 'Select Board has discussed Audit Report and prepared Action plan';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.electiondate == '' || $scope.employee.electiondate == null) {
				$scope.validatestring = $scope.validatestring + 'Select Date of Last Elections';
				$scope.TabName =  'STATUTORY';
				
			} else if ($scope.employee.electionbody == '' || $scope.employee.electionbody == null) {
				$scope.validatestring = $scope.validatestring + 'Enter Total No.of Members in the Elected Body( No of board members)';
				$scope.TabName =  'STATUTORY';
			}
			if ($scope.employee.typolog != undefined) {
				for (var i = 0; i < $scope.employee.typolog.length; i++) {
					if (i == 0) {
						$scope.employee.typology = $scope.employee.typolog[i];
					} else {
						$scope.employee.typology = $scope.employee.typology + ',' +
							$scope.employee.typolog[i];
					}
				}
			} else {
				$scope.employee.typology = null;
				//$scope.validatestring = $scope.validatestring + 'Please Select Typology';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring2 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				Restangular.all('employees').post($scope.employee).then(function (saveResponse) {
					$scope.SaveNONTISIMP(saveResponse.stateId, saveResponse.district, saveResponse.id);
				}, function (response) {
					$scope.showValidation = !$scope.showValidation;
					if (response.data.error.constraint == "unique_salescode") {
						$scope.validatestring1 = 'Facility Code' + ' ' + $scope.employee.salesCode + ' ' + 'Already Exists';
					} else {
						$scope.validatestring1 = response.data.error.detail;
					}
					console.error(response);
				});
			}
		};



		$scope.SaveTISIMP = function (st, dt, id) {
			for (var i = 0; i < $scope.tifacilities.length; i++) {
				$scope.item = {
					facilityid: id,
					state: st,
					district: dt,
					rating: $scope.tifacilities[i].rating,
					deleteflag: false
				}
				Restangular.all('tifacilities').post($scope.item).then(function (resp) {
					console.log('tifacilities', resp);
				});
			};
		};
		$scope.count = 0;
		$scope.SaveNONTISIMP = function (st, dt, id) {
			$scope.SaveTISIMP(st, dt, id);

			for (var i = 0; i < $scope.nontifacilities.length; i++) {
				$scope.itemnonti = {
					facilityid: id,
					state: st,
					district: dt,
					rating: $scope.nontifacilities[i].rating,
					deleteflag: false
				}
				Restangular.all('nontifacilities').post($scope.itemnonti).then(function (resp) {
					console.log('nontifacilities', resp);
				});
				$scope.count++;
				//console.log('$scope.coun++', $scope.coun);
				if ($scope.count == $scope.nontifacilities.length) {
					$location.path('/employees');
					//$scope.loc();
				}
			};
			console.log('$scope.count', $scope.count);

		};

		$scope.toggleValidation = function () {
			$scope.showUniqueValidation = !$scope.showUniqueValidation;
		};

		$scope.$watch('employee.astatus', function (newValue, oldValue) {
			//console.log('astatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.aRegistration = true;
			} else {
				$scope.aRegistration = false;
			}
		});

		$scope.$watch('employee.gstatus', function (newValue, oldValue) {
			//console.log('gstatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.gRegistration = true;
			} else {
				$scope.gRegistration = false;
			}
		});

		$scope.$watch('employee.frcastatus', function (newValue, oldValue) {
			//console.log('fcrastatus', newValue);
			if (newValue == oldValue) {
				return;
			} else if (newValue != 4) {
				$scope.fRegistration = true;
			} else {
				$scope.fRegistration = false;
			}
		});

	});
